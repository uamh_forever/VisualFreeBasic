Type Class_CheckBox Extends Class_Control
   Declare Property Value() As Boolean                 '返回/设置选中状态。{=.True.False}
   Declare Property Value(ByVal nCheckState As BOOLEAN)
   Declare Property Caption() As CWSTR                '返回/设置控件中的文本
   Declare Property Caption(ByVal sText As CWSTR)
    Declare Property Style() As Long      '返回/设置控件边界的外观和行为。{=.0 - 标准.1 - 按钮}
    Declare Property Style(bValue As Long)    
    Declare Property TextAlign() As Long      '返回/设置将在控件上显示的文本的对齐方式。{=.0 - 左对齐.1 - 居中.2 - 右对齐.3 - 中左对齐.4 - 置中.5 - 中右对齐.6 - 下左对齐.7 - 下居中.8 - 下右对齐}
    Declare Property TextAlign(bValue As Long)    
    Declare Property Alignment() As Long      '返回/设置文本在控件的左边或右边{=.0 - 文本在左边.1 - 文本在右边}
    Declare Property Alignment(bValue As Long)    
    Declare Property Multiline() As Boolean                 '返回/设置控件是否可以多行显示文本。{=.True.False}
    Declare Property Multiline(ByVal bValue As Boolean)    
End Type

Property Class_CheckBox.Caption() As CWStr
   Return AfxGetWindowText(hWndControl)
End Property
Property Class_CheckBox.Caption(ByVal sText As CWStr)
   AfxSetWindowText hWndControl, sText
End Property

Property Class_CheckBox.Value() As Boolean               '
   If SendMessage(hWndControl, BM_GETCHECK, 0, 0) = BST_CHECKED Then
      Return True
   Else
      Return False
   End If
End Property
Property Class_CheckBox.Value(ByVal nCheckState As Boolean)
   If nCheckState Then
      SendMessage hWndControl, BM_SETCHECK, BST_CHECKED, 0
   Else
      SendMessage hWndControl, BM_SETCHECK, BST_UNCHECKED, 0
   End If
End Property
Property Class_CheckBox.Style() As Long
   Return  iif((AfxGetWindowStyle(hWndControl) and BS_PUSHLIKE) <>0,1,0)
End Property
Property Class_CheckBox.Style(bValue As Long)
   Select Case bValue
      Case 0  '
         AfxRemoveWindowStyle hWndControl, BS_PUSHLIKE
      Case 1  '
         AfxAddWindowStyle hWndControl, BS_PUSHLIKE
   End Select
End Property
Property Class_CheckBox.TextAlign() As Long
   Dim nBS_LEFT as Long = AfxGetWindowStyle(hWndControl) and BS_LEFT
   Dim nBS_CENTER as Long = AfxGetWindowStyle(hWndControl) and BS_CENTER
   Dim nBS_RIGHT as Long = AfxGetWindowStyle(hWndControl) and BS_RIGHT
   Dim nBS_TOP as Long = AfxGetWindowStyle(hWndControl) and BS_TOP
   Dim nBS_VCENTER as Long = AfxGetWindowStyle(hWndControl) and BS_VCENTER
   Dim nBS_BOTTOM as Long = AfxGetWindowStyle(hWndControl) and BS_BOTTOM
   if nBS_LEFT <> 0 and nBS_TOP <> 0 then Return 0
   if nBS_CENTER <> 0 and nBS_TOP <> 0 then Return 1
   if nBS_RIGHT <> 0 and nBS_TOP <> 0 then Return 2
   if nBS_LEFT <> 0 and nBS_VCENTER <> 0 then Return 3
   if nBS_CENTER <> 0 and nBS_VCENTER <> 0 then Return 4
   if nBS_RIGHT <> 0 and nBS_VCENTER <> 0 then Return 5
   if nBS_LEFT <> 0 and nBS_BOTTOM <> 0 then Return 6
   if nBS_CENTER <> 0 and nBS_BOTTOM <> 0 then Return 7
   if nBS_RIGHT <> 0 and nBS_BOTTOM <> 0 then Return 8
   
   
End Property
Property Class_CheckBox.TextAlign(bValue As Long)
   Select Case bValue
      Case 0
         AfxRemoveWindowStyle hWndControl, BS_CENTER
         AfxRemoveWindowStyle hWndControl, BS_RIGHT
         AfxRemoveWindowStyle hWndControl, BS_VCENTER
         AfxRemoveWindowStyle hWndControl, BS_BOTTOM
         AfxAddWindowStyle hWndControl, BS_LEFT
         AfxAddWindowStyle hWndControl, BS_TOP
      Case 1
         AfxRemoveWindowStyle hWndControl, BS_LEFT
         AfxRemoveWindowStyle hWndControl, BS_RIGHT
         AfxRemoveWindowStyle hWndControl, BS_VCENTER
         AfxRemoveWindowStyle hWndControl, BS_BOTTOM
         AfxAddWindowStyle hWndControl, BS_CENTER
         AfxAddWindowStyle hWndControl, BS_TOP
      Case 2
         AfxRemoveWindowStyle hWndControl, BS_LEFT
         AfxRemoveWindowStyle hWndControl, BS_CENTER
         AfxRemoveWindowStyle hWndControl, BS_VCENTER
         AfxRemoveWindowStyle hWndControl, BS_BOTTOM
         AfxAddWindowStyle hWndControl, BS_TOP
         AfxAddWindowStyle hWndControl, BS_RIGHT
      Case 3
         AfxRemoveWindowStyle hWndControl, BS_CENTER
         AfxRemoveWindowStyle hWndControl, BS_RIGHT
         AfxRemoveWindowStyle hWndControl, BS_TOP
         AfxRemoveWindowStyle hWndControl, BS_BOTTOM
         AfxAddWindowStyle hWndControl, BS_LEFT
         AfxAddWindowStyle hWndControl, BS_VCENTER
      Case 4
         AfxRemoveWindowStyle hWndControl, BS_LEFT
         AfxRemoveWindowStyle hWndControl, BS_RIGHT
         AfxRemoveWindowStyle hWndControl, BS_TOP
         AfxRemoveWindowStyle hWndControl, BS_BOTTOM
         AfxAddWindowStyle hWndControl, BS_CENTER
         AfxAddWindowStyle hWndControl, BS_VCENTER
      Case 5
         AfxRemoveWindowStyle hWndControl, BS_LEFT
         AfxRemoveWindowStyle hWndControl, BS_CENTER
         AfxRemoveWindowStyle hWndControl, BS_TOP
         AfxRemoveWindowStyle hWndControl, BS_BOTTOM
         AfxAddWindowStyle hWndControl, BS_RIGHT
         AfxAddWindowStyle hWndControl, BS_VCENTER
      Case 6
         AfxRemoveWindowStyle hWndControl, BS_CENTER
         AfxRemoveWindowStyle hWndControl, BS_RIGHT
         AfxRemoveWindowStyle hWndControl, BS_TOP
         AfxRemoveWindowStyle hWndControl, BS_VCENTER
         AfxAddWindowStyle hWndControl, BS_LEFT
         AfxAddWindowStyle hWndControl, BS_BOTTOM
      Case 7
         AfxRemoveWindowStyle hWndControl, BS_LEFT
         AfxRemoveWindowStyle hWndControl, BS_RIGHT
         AfxRemoveWindowStyle hWndControl, BS_TOP
         AfxRemoveWindowStyle hWndControl, BS_VCENTER
         AfxAddWindowStyle hWndControl, BS_CENTER
         AfxAddWindowStyle hWndControl, BS_BOTTOM
      Case 8
         AfxRemoveWindowStyle hWndControl, BS_LEFT
         AfxRemoveWindowStyle hWndControl, BS_CENTER
         AfxRemoveWindowStyle hWndControl, BS_TOP
         AfxRemoveWindowStyle hWndControl, BS_VCENTER
         AfxAddWindowStyle hWndControl, BS_RIGHT
         AfxAddWindowStyle hWndControl, BS_BOTTOM
   End Select
End Property
Property Class_CheckBox.Alignment() As Long
   Return  iif((AfxGetWindowStyle(hWndControl) and BS_LEFTTEXT) <>0,1,0)
End Property
Property Class_CheckBox.Alignment(bValue As Long)
   Select Case bValue
      Case 0  '
         AfxRemoveWindowStyle hWndControl, BS_LEFTTEXT
      Case 1  '
         AfxAddWindowStyle hWndControl, BS_LEFTTEXT
   End Select
End Property
Property Class_CheckBox.Multiline() As Boolean
   Return  (AfxGetWindowStyle(hWndControl) and BS_MULTILINE) <>0
End Property
Property Class_CheckBox.Multiline(bValue As Boolean)
   if bValue then 
       AfxAddWindowStyle hWndControl, BS_MULTILINE
   Else
       AfxRemoveWindowStyle hWndControl, BS_MULTILINE
   end if 
End Property











