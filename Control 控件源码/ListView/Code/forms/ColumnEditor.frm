﻿#VisualFreeBasic_Form#  Version=5.5.6
Locked=0

[Form]
Name=ColumnEditor
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=ListView 列表头编辑器
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=424
Height=272
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=400
MinHeight=250
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=名称：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=17
Top=26
Width=45
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=False
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=57
Top=24
Width=242
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=False
Visible=True
MaxLength=5
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=56
Top=58
Width=41
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[ListView]
Name=ListView1
Index=-1
Style=1 - 报表视图
BStyle=3 - 凹边框
Theme=False
ImageNormalS=无图像列表控件
ImageSmallS=无图像列表控件
SingleSel=False
ShowSel=True
Sort=0 - 不排序
SortColumn=True
NoScroll=False
AutoArrange=False
EditLabels=False
OwnerData=False
AlignTop=False
AlignLeft=False
OwnDraw=False
ColumnHeader=True
ColumnEditor=
NoSortHeader=False
BorderSelect=False
Check=False
FlatScrollBars=False
FullRowSelect=False
GridLines=False
HeaderDragDrop=False
InfoTip=False
LabelTip=False
MultiWorkAreas=False
OneClickActivate=False
Regional=False
SimpleSelect=False
SubItemImages=False
TrackSelect=False
TwoClickActivate=False
UnderlineCold=False
UnderlineHot=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
TextBkColor=SYS,5
Font=微软雅黑,9
Left=10
Top=112
Width=387
Height=82
Layout=5 - 宽度和高度
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=宽度：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=17
Top=60
Width=45
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=预示：（点击下面的列，就可以修改它）
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=17
Top=90
Width=252
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=索引：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=17
Top=3
Width=227
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=确定
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=271
Top=204
Width=61
Height=26
Layout=9 - 右边和底部
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=339
Top=204
Width=61
Height=26
Layout=9 - 右边和底部
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=新增
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=330
Top=12
Width=61
Height=26
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=删除
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=330
Top=48
Width=61
Height=26
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line1
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无箭头
ArrowStartH=0 - 无箭头
ArrowEndW=0 - 无箭头
ArrowEndH=0 - 无箭头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=3
Top=199
Width=403
Height=1
Layout=8 - 底部和宽度
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=0
Caption=<-
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=322
Top=82
Width=35
Height=24
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=1
Caption=->
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=365
Top=82
Width=35
Height=24
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=对齐：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=127
Top=60
Width=45
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[ComboBox]
Name=Combo1
Index=-1
Style=0 - 标签和下拉框
Custom=<Sel>左对齐|0|右对齐|0|中对齐|0
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=False
AutoHscroll=True
MaxLength=0
Uppercase=False
Lowercase=False
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=167
Top=56
Width=64
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label6
Index=-1
Style=0 - 无边框
Caption=图标：
Enabled=True
Visible=False
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=20
Top=293
Width=45
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=...
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=False
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=272
Top=290
Width=29
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text4
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=False
Visible=False
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=True
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=58
Top=292
Width=209
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label7
Index=-1
Style=0 - 无边框
Caption=注意：设置列图像属性后这里的图标将无法显示
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=7
Top=209
Width=261
Height=20
Layout=7 - 跟随底部
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Timer]
Name=Timer1
Index=-1
Interval=500
Enabled=True
Left=267
Top=59
Tag=


[AllCode]
Type customTAB_type 
   minzi As CWSTR    '名称
   tuxian As CWSTR   '图像
   chuanko As CWSTR  '绑定的窗口
   xuan As Long      '选择项
   iImage As Long   '用于ColumnEditor 
End Type    
Dim Shared customTABm() As customTAB_type ,customTABxg As Long 
Dim Shared ColumnEditorI As Long 
Dim Shared xiugai As Long  '修改标记，是不是被修改选项

Sub ColumnEditor_Shown(hWndForm As hWnd, UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   Me.UserData(0) = UserData
   Dim As Rect rc,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr,UserData)
   GetWindowRect(st->hWndForm , @rc2)
   GetWindowRect(st->hWndList , @rc)
   if rc.Left + Me.Width > rc2.right Then rc.Left = rc2.right - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top = rc2.bottom - Me.Height
   Me.Move rc.Left, rc.top   
   
   Dim aa As String = *st->value
   Erase customTABm
   ColumnEditorI = -1    
   Dim FLY_hPicture As HIMAGELIST = ImageList_Create(AfxScaleX(16), AfxScaleY(16), ILC_MASK Or ILC_COLOR32, 2, 10)
   if Len(aa) Then
      aa = YF_Replace(aa, "\|", Chr(0))
      Dim cc() As String
      Dim u As Long = vbSplit(aa, "|", cc()) -1
      Dim i As Long, xu As Long, ci As Long = -1
      
      if u > 0 Then
         for i = 0 To u -4 Step 4
            ci += 1
            ReDim Preserve customTABm(ci)
            customTABm(ci).minzi = Utf8toStr(YF_Replace(cc(i), Chr(0), "|"))     ' 名称
            if i + 1 <= u Then
'               customTABm(ci).tuxian = Utf8toStr(YF_Replace(cc(i + 1), Chr(0), "|"))  '   图像
'               if Len(customTABm(ci).tuxian) Then
'                  Dim FLY_hIcon As HICON = ImgFileToIcon(customTABm(ci).tuxian)
'                  if FLY_hIcon Then
'                     DestroyIcon FLY_hIcon
'                     customTABm(ci).iImage = ImageList_GetImageCount(FLY_hPicture) '-1
'                  End if
'               End if
            End if
            if i + 2 <= u Then customTABm(ci).chuanko = ValInt(cc(i + 2))  ' 对齐
            if i + 3 <= u Then customTABm(ci).xuan = ValInt(cc(i + 3))
            ListView1.AddColumn customTABm(ci).minzi, AfxScaleX(valint(customTABm(ci).chuanko)),AfxScaleX(customTABm(ci).xuan) ', customTABm(ci).iImage
         Next
         ColumnEditorI = 0
      End if
   end if
   Dim pNMV As NM_LISTVIEW
   pNMV.iSubItem = ColumnEditorI
   ColumnEditor_ListView1_LVN_ColumnClick 0, 0, pNMV
   'Label4.Caption = "索引：0 " & UBound(customTABm) + 1
   
   customTABxg = 0
   xiugai = 0
End Sub
        
Sub ColumnEditor_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Me.Close 
   
End Sub

Function ColumnEditor_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   if xiugai Then
      
      Select Case MessageBox(hWndForm, vfb_LangString("选项被修改，你需要退出前保存吗？"), "VisualFreeBasic", _
               MB_YESNOCANCEL Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Case IDYES
            Command1.Click 
            Return True
         Case IDNO
            
         Case IDCANCEL
            Return True
      End Select
   End if 
   Function = FALSE ' 如果想阻止窗口关闭，则应返回 TRUE 。
   Dim FLY_hPicture As HIMAGELIST =ListView1.ImageList(LVSIL_GROUPHEADER) 
   if FLY_hPicture Then ImageList_Destroy FLY_hPicture
End Function

Sub ColumnEditor_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim ss As String 
   if UBound(customTABm) > -1 Then 
      Dim i As Long
      for i = 0 To UBound(customTABm)
          ss &= YF_Replace(customTABm(i).minzi.UTF8,"|","\|") & "|"  & YF_Replace(customTABm(i).tuxian.UTF8,"|","\|") & "|"  & customTABm(i).chuanko.UTF8 & "|"  & customTABm(i).xuan & "|" 
      Next        
   End if 
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   st->Rvalue = ss   
   xiugai =0 
   Me.Close   
End Sub

Sub ColumnEditor_ListView1_LVN_ColumnClick(hWndForm As hWnd, hWndControl As hWnd, pNMV As NM_LISTVIEW)  '一个列被点击了
   'pNMV.iSubItem  当前被点击列的索引。lpNMV.iItem = -1 其它成员均为零 
   customTABxg = 1
   ColumnEditorI = pNMV.iSubItem 
   if ColumnEditorI= -1 Then 
      Text1.Enabled = False 
      Text2.Enabled = False 
      Text4.Enabled = False 
      Combo1.Enabled = False 
      Command4.Enabled = False 
      Command7.Enabled = False 
      Command5(0).Enabled = False 
      Command5(1).Enabled = False 
      Label4.Caption = "索引：-1 （请点击预示中的列表名）"
      Text1.Text = ""
      Text2.Text = ""
      Text4.Text = ""
   Else
      Text1.Enabled = True 
      Text2.Enabled = True 
      Text4.Enabled = True 
      Combo1.Enabled = True 
      Command4.Enabled = True 
      Command7.Enabled = True 
      Command5(0).Enabled = ColumnEditorI >0 
      Command5(1).Enabled = ColumnEditorI < ListView1.ColumnCount-1 
      Label4.Caption = vfb_LangString("索引：") & ColumnEditorI & "  ( 0 - " & ListView1.ColumnCount -1 & " )"
      Text1.Text = customTABm(ColumnEditorI).minzi 
      Text2.Text = customTABm(ColumnEditorI).xuan 
      Text4.Text = customTABm(ColumnEditorI).tuxian
      Combo1.ListIndex =ValInt(customTABm(ColumnEditorI).chuanko)  
      
   End if  
   customTABxg = 0 
End Sub

Sub ColumnEditor_Text1_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if customTABxg Then Return
   if ColumnEditorI < 0 Or ColumnEditorI > UBound(customTABm) Then Return 
   customTABm(ColumnEditorI).minzi  = Text1.Text 
   ListView1.ColumnText(ColumnEditorI) = customTABm(ColumnEditorI).minzi 
   xiugai = 1
End Sub

Sub ColumnEditor_Text2_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if customTABxg Then Return
   if ColumnEditorI < 0 Or ColumnEditorI > UBound(customTABm) Then Return 
   customTABm(ColumnEditorI).xuan = ValInt(Text2.Text)
   ListView1.ColumnWidth(ColumnEditorI) = AfxScaleX(customTABm(ColumnEditorI).xuan) 
   xiugai = 1
End Sub

Sub ColumnEditor_Combo1_CBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '列表框中更改当前选择时
   if customTABxg Then Return
   if ColumnEditorI < 0 Or ColumnEditorI > UBound(customTABm) Then Return 
   customTABm(ColumnEditorI).chuanko = Combo1.ListIndex 
   ListView1.ColumnAlignment(ColumnEditorI) = Combo1.ListIndex 
   xiugai = 1

End Sub

Sub ColumnEditor_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim i As Long = UBound(customTABm) + 1
   ReDim Preserve customTABm(i)
   customTABm(i).minzi = "Column" & i + 1
   customTABm(i).chuanko = 0
   customTABm(i).xuan = 100
   customTABm(i).tuxian = ""
   if ListView1.ItemCount = 0 Then 
      for ii As Long =1 To 10
         ListView1.AddItem "ListView"
      Next 
   End if  
   
   
   Dim pNMV As NM_LISTVIEW
   pNMV.iSubItem = i
   ListView1.AddColumn customTABm(i).minzi
   ColumnEditor_ListView1_LVN_ColumnClick 0, 0, pNMV
   SendMessage(ListView1.hWnd, LVM_REDRAWITEMS, 0, ListView1.ColumnCount)
End Sub

Sub ColumnEditor_Timer1_WM_Timer(hWndForm As hWnd, wTimerID As Long)  '定时器
   if customTABxg Then Return
   Dim u As Long = UBound(customTABm)
   if u>-1 Then 
      Dim i As Long ,w As Long 
      for i = 0 To u 
         w = AfxUnscaleX(ListView1.ColumnWidth(i))     
         if w <> customTABm(i).xuan Then 
            customTABm(i).xuan = w
            xiugai = 1 
            if ColumnEditorI = i Then 
               customTABxg = 1
               Text2.Text = w
                customTABxg = 0
            End if 
         End if 
      Next 
   End if 
End Sub

Sub ColumnEditor_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
'   if customTABxg Then Return
'   if ColumnEditorI < 0 Or ColumnEditorI > UBound(customTABm) Then Return
'   Dim aa As String = customTABm(ColumnEditorI).tuxian
'   Dim bb As String = GetImgForm(aa)  '获取图像文件名称
'   if aa <> bb Then
'      xiugai = 1
'      customTABm(ColumnEditorI).tuxian = bb
'      customTABm(ColumnEditorI).iImage =-1
'      Text2.Text = customTABm(ColumnEditorI).tuxian
'      Dim FLY_hPicture As HIMAGELIST = ListView1.ImageList(LVSIL_GROUPHEADER)
'      if Len(customTABm(ColumnEditorI).tuxian) Then
'         Dim FLY_hIcon As HICON = ImgFileToIcon(customTABm(ColumnEditorI).tuxian)
'         if FLY_hIcon Then
'            ImageList_AddIcon(FLY_hPicture, FLY_hIcon)
'            DestroyIcon FLY_hIcon
'            customTABm(ColumnEditorI).iImage = ImageList_GetImageCount(FLY_hPicture) -1
'         End if
'      End if
'      ListView1.ColumnImage(ColumnEditorI)= customTABm(ColumnEditorI).iImage
'      
'   End if
End Sub

Sub ColumnEditor_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if customTABxg Then Return
   if ColumnEditorI < 0 Or ColumnEditorI > UBound(customTABm) Then Return      
   Select Case MessageBox(hWndForm, vfb_LangString("真的要删除吗？"), "VisualFreeBasic", _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON2 Or MB_APPLMODAL)
      Case IDYES
      Case IDNO
         Return
   End Select
   ListView1.DeleteColumn ColumnEditorI
   Dim u As Long = UBound(customTABm)
   if u > 0 Then
      Dim ii As Long
      for ii = ColumnEditorI To u -1
         customTABm(ii) = customTABm(ii + 1)
      Next
      ReDim Preserve customTABm(u -1)
   Else
      Erase customTABm
   End If
   ColumnEditorI = -1
   Dim pNMV As NM_LISTVIEW
   pNMV.iSubItem = ColumnEditorI
   ColumnEditor_ListView1_LVN_ColumnClick 0, 0, pNMV   
   xiugai = 1   
   
End Sub

Sub ColumnEditor_Command5_BN_Clicked(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd)  '单击
   if customTABxg Then Return
   if ColumnEditorI < 0 Or ColumnEditorI > UBound(customTABm) Then Return
   if ControlIndex = 0 Then
      if ColumnEditorI < 1 Then Return
      Swap customTABm(ColumnEditorI), customTABm(ColumnEditorI -1)
      ListView1.DeleteColumn ColumnEditorI -1
      ListView1.InsertColumn ColumnEditorI,customTABm(ColumnEditorI).minzi,AfxScaleX(valint(customTABm(ColumnEditorI).chuanko)), customTABm(ColumnEditorI).xuan
      ColumnEditorI -=1
      
   Else
      if ColumnEditorI > ListView1.ColumnCount -2 Then Return
      Swap customTABm(ColumnEditorI), customTABm(ColumnEditorI +1)
      ListView1.DeleteColumn ColumnEditorI+1 
      ListView1.InsertColumn ColumnEditorI,customTABm(ColumnEditorI).minzi,AfxScaleX(valint(customTABm(ColumnEditorI).chuanko)), customTABm(ColumnEditorI).xuan
      ColumnEditorI +=1      
   End if
   Dim pNMV As NM_LISTVIEW
   pNMV.iSubItem = ColumnEditorI
   ColumnEditor_ListView1_LVN_ColumnClick 0, 0, pNMV   
   xiugai = 1    
End Sub



















