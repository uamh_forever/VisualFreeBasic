﻿
'这里是控件主模块 ==================
'  VisualFreeBasic 控件工作流程
'VFB启动时扫描控件文件夹里的每个文件夹，每个文件夹其中一个为 控件DLL，也就是本工程生产的DLL
'第1步：动态加载DLL
'第2步：检查DLL输出函数，都存在继续下一步，否则认为不是 控件DLL 就卸载DLL
'第3步：调用  initialization  来确定协议版本，不符合的就VFB会提示需要多少版本的。并且卸载DLL
'第4步：调用  SetControlProperty 由 DLL来加载控件属性
'当打开一个工程用到本控件时，调用 Edit_AddControls 加载控件到 窗口编辑器
'用户修改或加载控件后，调用 Edit_SetControlProperty 来设置 窗口编辑器中控件 属性
'若控件有特殊属性需要在线处理，调用 Edit_ControlPropertyAlter 来设置，此时由本DLL负责处理，比如 菜单编辑
'当虚拟控件需要绘画时，调用 Edit_OnPaint 来画控件，实体控件是系统负责画，就不需要了。
'编译软件时，调用 Compile_ExplainControl 解析控件，把控件属性翻译成具体代码。
'而控件类是提供给代码调用的。


Function initialization() As Long Export '初始化
                        '当 VFB5 主软件加载本DLL后，主动调用一下此函数，可以在此做初始化代码
   SetFunctionAddress() '设置函数地址
   Function = 7 '返回协议版本号，不同协议（发生接口变化）不能通用，会发生崩溃等问题，因此VFB主软件会判断此版本号，不匹配就不使用本控件。
End Function
Function SetControlProperty(ByRef ColTool As ColToolType) As Long Export '设置控件工具属性。
   '处理多国语言 -----  如果本控件不支持多国语言，可以删除
   Dim op     As pezi Ptr     = GetOpAPP()
   Dim ExeApp As APP_TYPE Ptr = GetExeAPP()
   vfb_LoadLanguage(ExeApp->Path & "Languages\" & op->Languages & "\Languages.txt" ,App.EXEName)
   
   '设置控件基本属性 --------------
   ColTool.sName     = "ListView"                 'Name      控件名称，必须英文
   ColTool.sTips     = vfb_LangString("列表视图") 'Tips      鼠标停留在控件图标上提示的内容
   ColTool.uName     = UCase(ColTool.sName)
   ColTool.sVale     = &HE66F                          'Ico       字体图标的字符值，字体文件在：VisualFreeBasic5\Settings\iconfont.ttf  如果有 *.ico 文件则优先显示图标文件，而不是字体图标。
   ColTool.Feature   = 2                               'Feature   特征 =0 不使用 =1 主窗口(只能有1个，且永远在第一个) =2 普通控件（有窗口句柄） =3 虚拟控件有界面（无句柄） =4 虚拟控件无界面（组件）
   ColTool.ClassFile = "ClsListView.inc"               'ClassFile 控件类文件名
   ColTool.Only      = 0                               'Only      是否是唯一的，就是一个窗口只能有1个此控件
   ColTool.GROUP     = vfb_LangString("扩展控件,列表") 'Group     分组名称，2个中文到4个最佳，属于多个分组，用英文豆号分割
   
   '设置控件的属性（窗口编辑器里的属性和选项，写代码的属性是控件类负责，代码提示则在帮助里修改。）和事件（代码编辑时可选的事件）
   '从配置文件中读取属性和事件，必须保证和DLL同文件下的 Attribute.ini Event.ini 配置文件正常
   '若不想用配置文件，也可以直接用代码赋值配置。
   'language <>0支持多国语言，会去VFB主语言文件里读取语言，修改配置里的文字。
   If AttributeOREvent(ColTool ,True) Then Return -1 '返回 -1 表示发生问题，VFB将会直接退出。
   
   Function = 21 '返回 排序号，控件在 IDE 中控件列表的先后位置，必须从2开始，主窗口 Form=0  Pointer=1 ，其它 2--n  从小到大排列
End Function


Function Edit_AddControls(cw As CWindow Ptr,hParent AS HWND,IDC As ULong ,Caption As CWSTR, x As Long, y As Long, w As Long, h As Long,WndProc As Any Ptr) As HWND Export '增加1个控件 
   '编辑：窗口刚被打开，需要新建，返回新建后的控件窗口句柄
   'cw      基于CWindow创建，这是当前窗口的 CWindow 指针
   'hParent 父窗口句柄
   'IDC     控件IDC号
   'Caption 控件标题
   'xywh    位置
   'WndProc 主窗口处理消息的函数地址(窗口消息回调函数)
   
   Function = cw->AddControl("LISTVIEW", hParent, IDC, Caption, x, y, w, h,WS_CHILD oR WS_CLIPSIBLINGS Or WS_VISIBLE Or WS_CLIPCHILDREN Or WS_TABSTOP Or LVS_REPORT Or LVS_SHOWSELALWAYS Or LVS_SHAREIMAGELISTS Or LVS_AUTOARRANGE Or LVS_EDITLABELS Or LVS_ALIGNTOP , , , WndProc)

End Function

Function Edit_SetControlProperty(ByRef Control As clsControl, ByRef ColTool As ColToolType, ki As Long) As Long Export '设置控件属性
   '编辑：新创建控件、修改控件属性后，都调用1次
   'Control  窗口中的控件
   'ColTool  当前控件配置和属性
   'ki       被修改的属性索引，=0为全部
   Dim hWndControl As hWnd = Control.nHwnd
   Dim vv As String, cvv As CWSTR, i As Long
   
   For i = 1 To ColTool.plU
      vv = Control.pValue(i) '值是 Utf8 格式
      cvv.UTF8 = YF_Replace(vv, Chr(3, 1), vbCrLf)
      '先设置通用部分
      Select Case ColTool.ProList(i).uName
         Case "CAPTION"
            if ColTool.uName <> "STATUSBAR" Then
               SetWindowTextW hWndControl, cvv.vptr
            End if
            Control.Caption = YF_Replace(vv, Chr(3, 1), vbCrLf)
         Case "ICON"
            Dim pa As String = GetProRunFile(0,4)
            Dim svv As String, fvv As Long = InStr(vv, "|")
            if fvv = 0 Then svv = vv Else svv = Left(vv, fvv -1)
            Dim hIcon As HICON = LoadImage(Null, pa & "images\" & Utf8toStr(svv), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE Or LR_LOADFROMFILE)
            If hIcon Then
               hIcon = AfxSetWindowIcon(hWndControl, ICON_SMALL, hIcon)
               If hIcon Then DestroyIcon(hIcon)
            End If
         Case "LEFT"
            If ki = i Then  '只有控件才设置，主窗口不设置
               Control.nLeft = ValInt(vv)
               FF_Control_SetLoc hWndControl, AfxScaleX(Control.nLeft), AfxScaleY(Control.nTop)
            End If
         Case "TOP"   '只有控件才设置，主窗口不设置
            If ki = i Then
               Control.nTop = ValInt(vv)
               FF_Control_SetLoc hWndControl, AfxScaleX(Control.nLeft), AfxScaleY(Control.nTop)
            End If
         Case "WIDTH"
            If ki = i And ColTool.Feature <> 4 Then
               Control.nWidth = ValInt(vv)
               FF_Control_SetSize hWndControl, AfxScaleX(Control.nWidth), AfxScaleY(Control.nHeight)
            End If
         Case "HEIGHT"
            If ki = i And ColTool.Feature <> 4 Then
               Control.nHeight = ValInt(vv)
               FF_Control_SetSize hWndControl, AfxScaleX(Control.nWidth), AfxScaleY(Control.nHeight)
            End If
         Case "CHILD"
         Case "MOUSEPOINTER"
         Case "FORECOLOR"
            Control.ForeColor = GetColorText(vv)
            Dim cc As Long = GetCodeColorGDI(Control.ForeColor)
            if cc = -1 then cc = GetSysColor(COLOR_WINDOWTEXT)            
            SendMessage(hWndControl ,LVM_SETTEXTCOLOR ,0 ,cc)
         Case "BACKCOLOR"
            Control.BackColor = GetColorText(vv)
            Dim cc As Long = GetCodeColorGDI(Control.BackColor)
            if cc = -1 then cc = CLR_NONE              
            SendMessage(hWndControl ,LVM_SETBKCOLOR ,0 ,cc)
         Case "TEXTBKCOLOR"
            Dim cc As Long = GetCodeColorGDI(GetColorText(vv))
            if cc = -1 then cc = CLR_NONE 
            SendMessage(hWndControl,LVM_SETTEXTBKCOLOR,0,cc)           
         Case "TAG"
         Case "TAB"
         Case "ACCEPTFILES"
         Case "INDEX"
         Case "FONT"
            Dim tFont As HFONT = GetWinFontLog(vv)
            SendMessage hWndControl, WM_SETFONT, Cast(wParam, tFont), True
            Control.Font = vv
         Case "TOOLTIPBALLOON"
         Case "TOOLTIP"
            '==============     以上是公共设置，下面是每个控件私有设置    =================
         Case "STYLE"  '\样式\2\显示样式\1 - 报表模式\0 - 图标模式LVS_ICON,1 - 报表模式LVS_REPORT,2 - 小图标模式LVS_SMALLICON,3 - 列表模式LVS_LIST
            Select Case ValInt(vv)
               Case 0  '左对齐
                  AfxRemoveWindowStyle hWndControl, LVS_REPORT
                  AfxRemoveWindowStyle hWndControl, LVS_SMALLICON
                  AfxRemoveWindowStyle hWndControl, LVS_LIST
                  AfxAddWindowStyle hWndControl, LVS_ICON
               Case 1  '居中
                  AfxRemoveWindowStyle hWndControl, LVS_ICON
                  AfxRemoveWindowStyle hWndControl, LVS_SMALLICON
                  AfxRemoveWindowStyle hWndControl, LVS_LIST
                  AfxAddWindowStyle hWndControl, LVS_REPORT
               Case 2  '右对齐
                  AfxRemoveWindowStyle hWndControl, LVS_ICON
                  AfxRemoveWindowStyle hWndControl, LVS_REPORT
                  AfxRemoveWindowStyle hWndControl, LVS_LIST
                  AfxAddWindowStyle hWndControl, LVS_SMALLICON
               Case 3
                  AfxRemoveWindowStyle hWndControl, LVS_ICON
                  AfxRemoveWindowStyle hWndControl, LVS_REPORT
                  AfxRemoveWindowStyle hWndControl, LVS_SMALLICON
                  AfxAddWindowStyle hWndControl, LVS_LIST
            End Select
            
         Case "COLUMNEDITOR"
            FF_ListView_DeleteAllItems hWndControl
            FF_ListView_DeleteAllColumns hWndControl
            if Len(vv) Then
               Dim aa As String = YF_Replace(vv, "\|", Chr(0))
               Dim cc() As String
               Dim u As Long = vbSplit(aa, "|", cc()) -1
               Dim i As Long, xu As Long, ci As Long = -1, cu As Long
               
               if u > 0 Then
                  for i = 0 To u -4 Step 4
                     ci += 1
                     Dim ss As CWSTR = Utf8toStr(YF_Replace(cc(i), Chr(0), "|"))     ' 名称
                     if i + 1 <= u Then
                     End if
                     if i + 2 <= u Then xu = ValInt(cc(i + 2))  ' 对齐
                     if i + 3 <= u Then cu = ValInt(cc(i + 3))
                     FF_ListView_InsertColumn hWndControl, ci, ss, xu, AfxScaleX(cu)
                  Next
               End if
            Else
               FF_ListView_InsertColumn hWndControl, 0, "Column 0", LVCFMT_LEFT, AfxScaleX(100)
               FF_ListView_InsertColumn hWndControl, 1, "Column 1", LVCFMT_LEFT, AfxScaleX(100)
               FF_ListView_InsertColumn hWndControl, 2, "Column 2", LVCFMT_LEFT, AfxScaleX(100)
               
            end if
            For oi As Long = 0 To 6
               FF_ListView_InsertItem hWndControl, oi, 0, Control.nName
               FF_ListView_SetItemText hWndControl, oi, 1, "Col=1," & "Item=" & oi
               FF_ListView_SetItemText hWndControl, oi, 2, "Col=2," & "Item=" & oi
               FF_ListView_SetItemText hWndControl, oi, 3, "Col=3," & "Item=" & oi
            Next
            
            
         Case "BSTYLE"  '\边框\2\指示控件边界的外观和行为。\3 - 凹边框\0 - 无边框,1 - 凹凸边框,2 - 半边框,3 - 凹边框,4 - 凸边框
            Select Case ValUInt(vv)
               Case 0  '无边框
                  AfxRemoveWindowStyle hWndControl, WS_BORDER
                  AfxRemoveWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
               Case 1  '细边框
                  AfxAddWindowStyle hWndControl, WS_BORDER
                  AfxRemoveWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
               Case 2  '半边框
                  AfxRemoveWindowStyle hWndControl, WS_BORDER
                  AfxRemoveWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxAddWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
               Case 3  '凹边框
                  AfxRemoveWindowStyle hWndControl, WS_BORDER
                  AfxAddWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
               Case 4 ' 凸边框
                  AfxRemoveWindowStyle hWndControl, WS_BORDER
                  AfxRemoveWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxAddWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
            End Select
            
         Case "SINGLESEL"  '\单选\2\一次只能选择一个项目。默认情况下，可以选择多个项目。\FALSE\TRUE,FALSE
         Case "SHOWSEL"  '\显示选择\2\即使控件没有焦点，也会始终显示选择内容（如果有的话）。\TRUE\TRUE,FALSE
         Case "SORT"  '\排序\2\项目索引根据项目文本排序。\3 - 凹边框\0 - 不排序,1 - 升序,2 - 降序
            Select Case ValUInt(vv)
               Case 0  '无边框
                  AfxRemoveWindowStyle hWndControl, LVS_SORTASCENDING
                  AfxRemoveWindowStyle hWndControl, LVS_SORTDESCENDING
               Case 1  '细边框
                  AfxRemoveWindowStyle hWndControl, LVS_SORTDESCENDING
                  AfxAddWindowStyle hWndControl, LVS_SORTASCENDING
               Case 2  '半边框
                  AfxRemoveWindowStyle hWndControl, LVS_SORTASCENDING
                  AfxAddWindowStyle hWndControl, LVS_SORTDESCENDING
            End Select
         Case "NOSCROLL"  '\禁用滚动\2\滚动功能被禁用。所有项目必须在客户区域内。此样式与LVS_LIST或样式不兼容。\FALSE\TRUE,FALSE
         Case "AUTOARRANGE"  '\排列图标\2\图标模式和小图标模式时自动排列图标\FALSE\TRUE,FALSE
         Case "EDITLABELS"  '\排列图标\2\项目文本可以在控件中当前位置进行编辑。父窗口必须处理LVN_ENDLABELEDIT通知消息。\FALSE\TRUE,FALSE
         Case "OWNERDATA"  '\虚拟表\2\创建一个虚拟列表视图控件\FALSE\TRUE,FALSE
         Case "ALIGNTOP"  '\顶部对齐\2\项目与图标和小图标视图中列表视图控件的顶部对齐。\FALSE\TRUE,FALSE
         Case "ALIGNLEFT"  '\左对齐\2\项目在图标和小图标视图中左对齐。\FALSE\TRUE,FALSE
         Case "OWNDRAW"  '\自绘控件\2\由自己写代码绘制，列表视图控件发送一个WM_DRAWITEM消息来绘制每个项目\FALSE\TRUE,FALSE
            'If UCase(vv) = "TRUE" Then AfxAddWindowStyle hWndControl, LVS_OWNERDRAWFIXED Else AfxRemoveWindowStyle hWndControl, LVS_OWNERDRAWFIXED
         Case "COLUMNHEADER"  '\标题栏\2\列标题不显示在报告视图中。默认情况下，列在报表视图中具有标题。\TRUE\TRUE,FALSE
            If UCase(vv) <> "TRUE" Then AfxAddWindowStyle hWndControl, LVS_NOCOLUMNHEADER Else AfxRemoveWindowStyle hWndControl, LVS_NOCOLUMNHEADER
         Case "NOSORTHEADER"  '\禁用列表\2\禁用报表模式的列表头点击\FALSE\TRUE,FALSE
         Case "BORDERSELECT"  '\突出项目\2\当列表选择项目，改变该项目而不是突出项目边框的颜色。\FALSE\TRUE,FALSE
         Case "CHECK"  '\复选框\2\在一个LISTVIEW使用复选框。\FALSE\TRUE,FALSE
            SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_CHECKBOXES, IIf(UCase(vv) = "TRUE", LVS_EX_CHECKBOXES, 0))
         Case "FLATSCROLLBARS"  '\水平滚动\2\LISTVIEW使用水平滚动条。\FALSE\TRUE,FALSE
         Case "FULLROWSELECT"  '\选择整行\2\设置LISTVIEW 视图LVS_REPORT模式时选择整个行。\FALSE\TRUE,FALSE
         Case "GRIDLINES"  '\网格线\2\在LISTVIEW 视图LVS_REPORT模式时项目和子项目显示网格线\FALSE\TRUE,FALSE
            SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_GRIDLINES, IIf(UCase(vv) = "TRUE", LVS_EX_GRIDLINES, 0))
         Case "HEADERDRAGDROP"  '\拖放排序\2\LVS_REPORT视图模式下LISTVIEW控件启用列拖和拖放重新排序。\FALSE\TRUE,FALSE
         Case "INFOTIP"  '\提示通知\2\显示工具提示之前先发送LVN_GET消息通知消息窗口。\FALSE\TRUE,FALSE
         Case "LABELTIP"  '\展开标签\2\如果 LISTVIEW 当前的视图模式缺乏工具提示文本，将展开任何隐藏部分的标签。\FALSE\TRUE,FALSE
         Case "MULTIWORKAREAS"  '\作区排列\2\如果LISTVIEW有 LVS_AUTOARRANGE 样式设置，它不会定义一个或多个工作区自动排列图标。\FALSE\TRUE,FALSE
         Case "ONECLICKACTIVATE"  '\点击通知\2\当用户点击一个项目时发送一个LVN_ITEMACTIVATE 通知消息到窗口。这种风格也使LISTVIEW控件有热跟踪。\FALSE\TRUE,FALSE
         Case "REGIONAL"  '\区域\2\设置LISTVIEW（与LVS_ICON风格）窗口区域只包括项目的图标和文本使用SETWINDOWRGN。\FALSE\TRUE,FALSE
         Case "SIMPLESELECT"  '\简单选择\2\在图标视图中，移动LISTVIEW的状态图像右上方的大图标显示。\FALSE\TRUE,FALSE
         Case "SUBITEMIMAGES"  '\子项图像\2\允许列表视图（含LVS_REPORT风格）为子项显示图像。\FALSE\TRUE,FALSE
         Case "TRACKSELECT"  '\热点追踪\2\启用LISTVIEW控件中热点追踪选择。当光标停留在项目上一段时间（通过设置LVM_SETHOVERTIME）项目被自动选择。\FALSE\TRUE,FALSE
         Case "TWOCLICKACTIVATE"  '\双击通知\2\当用户双击一个项目，LISTVIEW将发送一个 LVN_ITEMACTIVATE消息通知父窗口。\FALSE\TRUE,FALSE
         Case "UNDERLINECOLD"  '\非热显示\2\那些可被引起激活的非热的项目，以带有下划线的文本显示。需要TWOCLICKACTIVATE属性被设置为TRUE。\FALSE\TRUE,FALSE
         Case "UNDERLINEHOT"  '\热点显示\2\那些可被引起激活的热点项目，以带下划线文本显示。需要ONECLICKACTIVATE或TWOCLICKACTIVATE属性被设置为TRUE。\FALSE\TRUE,FALSE
      End Select
   Next
   Function = 0
End Function

Function Edit_ControlPropertyAlter(hWndForm As hWnd, hWndList As hWnd, nType As Long, value As String, default As String, AllList As String, nName As String, FomName As String) As Long Export  ' 控件属性修改
   '编辑：用户点击窗口属性，修改属性时，1--6由EXE处理，7 或其它由本DLL处理
   'hWndForm   EXE 主窗口句柄
   'hWndList   控件属性显示窗口句柄（是List控件）Dim z As ZString Ptr = Cast(Any Ptr ,FF_ListBox_GetItemData(aa.hWndList,Index)) '当前属性值
   'nType      类型，由 Attribute.ini 里设置，7 或其它由本DLL处理
   'value      当前的值
   'default    默认值，由 Attribute.ini 里设置
   'AllList    所有值，由 Attribute.ini 里设置
   Select Case nType  '这里根据需要编写
      Case 100
         Dim aa As StyleFormType
         aa.hWndForm = hWndForm
         aa.hWndList = hWndList
         aa.nType = nType
         aa.value = @value
         aa.default = @default
         aa.AllList = @AllList
         aa.Rvalue = value
         aa.nName = nName : aa.FomName = FomName '当前被编辑的控件名和窗口名
         ColumnEditor.Show hWndForm, True, Cast(Integer, @aa)
         value = aa.Rvalue
         Function = len(value)
         
   End Select
End Function
Function Edit_OnPaint(gg As yGDI, Control As clsControl, ColTool As ColToolType, WinCc As Long, nFile As String) as Long Export '描绘控件
   '编辑：当被刷新窗口，需要重绘控件时，窗口和实控件由系统绘画，不需要我们在这里处理，虚拟控件必须由此画出来。
   'gg    目标， 画在这个缓冲里。
   'Control  窗口中的控件
   'ColTool  当前控件配置和属性
   'WinCc    主窗口底色，不是本控件底色
   'nFile    当前工程主文件名，带文件夹，用来提取路径用。
   '返回非0  将会立即结束描绘操作，就是在此之后的控件就不会画了。按照最底层的控件先画。
   
   
   Function = 0
End Function
Function Compile_ExplainControl(Control As clsControl ,ColTool As ColToolType ,ProWinCode As String ,ussl() As String ,ByRef IDC As Long ,DECLARESdim As String ,Form_clName as String ,nFile As String) as Long Export '解释控件，制造创建控件和事件的代码
   '编译：解释控件 ，注意：编译处理字符全部为 UTF8 编码。Control和ColTool里的是 A字符。
   'Control      窗口中的控件
   'ColTool      当前控件配置和属性
   'ProWinCode   处理后的窗口代码，最初由窗口加载窗口模板处理，然后分发给其它控件。填充处理
   'ussl()       已特殊处理过的用户写的窗口代码，主要用来识辨事件
   'IDC          控件IDC，每个控件唯一，VFB自动累计1，我们代码也可以累计
   'DECLARESdim  全局变量定义，整个工程的定义都在此处
   'Form_clName  主窗口类名，最初由窗口设置，方便后面控件使用。
   'nFile        窗口文件名，用在事件调用注释，出错时可以提示源文件地方，避免提示临时文件。
   
   
   '创建控件 ------------------------------
   Dim ii As Long
   Dim As String clClName ,clName ,clStyle ,clExStyle ,clPro
   
   Dim As Long clType '为了解释代码里用，>=100 为虚拟控件  100=LABEL 1=TEXT
   clName = StrToUtf8(Control.nName)
   If Control.Index > -1 Then clName &= "(" & Control.Index & ")"
   clClName  = "LISTVIEW"
   clType    = 0
   clStyle   = "WS_CHILD,WS_VISIBLE,WS_CLIPCHILDREN,WS_CLIPSIBLINGS,WS_TABSTOP,LVS_SHAREIMAGELISTS"
   clExStyle = ""
   For ii = 1 To ColTool.plU
      if ExplainControlPublic(Form_clName ,Control ,clName ,ii ,ColTool.ProList(ii).uName ,clType ,clStyle ,clExStyle ,clPro ,ProWinCode) Then '处理公共部分，已处理返回0，未处理返回非0
         Select Case ColTool.ProList(ii).uName
               'Case "NAME"  '名称\1\用来代码中识别对象的名称
               'Case "INDEX"  '数组索引\0\控件数组中的控件位置的索引数字。值小于零表示不是控件数组
               'Case "CAPTION"  '文本\1\显示的文本\Label\
               'Case "TEXT"  '文本\1\显示的文本\Label\
               'Case "ENABLED"  '允许\2\创建控件时最初是否允许操作。\True\True,False
               'Case "VISIBLE"  '显示\2\创建控件时最初是显示或隐藏。\True\True,False
               'Case "FORECOLOR"  '文字色\3\用于在对象中显示文本和图形的前景色。\SYS,8\
               'Case "BACKCOLOR"  '背景色\3\用于在对象中显示文本和图形的背景色。\SYS,15\
               'Case "FONT"  '字体\4\用于此对象的文本字体。\微软雅黑,9,0\
               'Case "LEFT"  '位置X\0\左边缘和父窗口的左边缘之间的距离。自动响应DPI缩放\0\
               'Case "TOP"  '位置Y\0\内部上边缘和父窗口的顶部边缘之间的距离。自动响应DPI缩放\0\
               'Case "WIDTH"  '宽度\0\窗口宽度，100%DPI时的像素单位，自动响应DPI缩放。\100\
               'Case "HEIGHT"  '高度\0\窗口高度，100%DPI时的像素单位，自动响应DPI缩放。\20\
               'Case "LAYOUT"
               'Case "MOUSEPOINTER"  '鼠标指针\2\鼠标在窗口上的形状\0 - 默认\0 - 默认,1 - 后台运行,2 - 标准箭头,3 - 十字光标,4 - 箭头和问号,5 - 文本工字光标,6 - 不可用禁止圈,7 - 移动,8 - 双箭头↙↗,9 - 双箭头↑↓,10 - 双箭头向↖↘,11 - 双箭头←→,12 - 垂直箭头,13 - 沙漏,14 - 手型
               'Case "TAG"  '附加\1\私有自定义文本与控件关联。\\
               'Case "TAB"  '导航\2\当用户按下TAB键时可以接收键盘焦点。\False\True,False
               'Case "TOOLTIP"  '提示\1\一个提示，当鼠标光标悬停在控件时显示它。\\
               'Case "TOOLTIPBALLOON"  '气球样式\2\一个气球样式显示工具提示。\False\True,False
               'Case "ACCEPTFILES"  '拖放\2\窗口是否接受拖放文件。\False\True,False
               '==============     以上是公共设置，下面是每个控件私有设置    =================
            Case "STYLE" '\样式\2\显示样式\1 - 报表模式\0 - 图标模式LVS_ICON,1 - 报表模式LVS_REPORT,2 - 小图标模式LVS_SMALLICON,3 - 列表模式LVS_LIST
               Select Case ValInt(Control.pValue(ii))
                  Case 0 '
                     clStyle = TextAddWindowStyle(clStyle ,"LVS_ICON")
                  Case 1 '
                     clStyle = TextAddWindowStyle(clStyle ,"LVS_REPORT")
                  Case 2 '
                     clStyle = TextAddWindowStyle(clStyle ,"LVS_SMALLICON")
                  Case 3
                     clStyle = TextAddWindowStyle(clStyle ,"LVS_LIST")
               End Select
            Case "BSTYLE" '\边框\2\指示控件边界的外观和行为。\3 - 凹边框\0 - 无边框,1 - 凹凸边框,2 - 半边框,3 - 凹边框,4 - 凸边框
               Select Case ValUInt(Control.pValue(ii))
                  Case 0 '无边框
                  Case 1 '细边框
                     clStyle = TextAddWindowStyle(clStyle ,"WS_BORDER")
                  Case 2 '半边框
                     clExStyle = TextAddWindowStyle(clExStyle ,"WS_EX_STATICEDGE")
                  Case 3 '凹边框
                     clExStyle = TextAddWindowStyle(clExStyle ,"WS_EX_CLIENTEDGE")
                  Case 4 ' 凸边框
                     clExStyle = TextAddWindowStyle(clExStyle ,"WS_EX_DLGMODALFRAME")
               End Select
            Case "COLUMNEDITOR"
               if Len(Control.pValue(ii)) Then
                  Dim aa   As String = YF_Replace(Control.pValue(ii) ,"\|" ,Chr(0))
                  Dim cc() As String
                  Dim u    As Long = vbSplit(aa ,"|" ,cc()) -1
                  Dim i    As Long ,xu As Long ,ci As Long = -1 ,cu As Long
                  
                  if u > 0 Then
                     for i = 0 To u -4 Step 4
                        ci += 1
                        Dim ss As String = YF_Replace(cc(i) ,Chr(0) ,"|") ' 名称
                        if i + 1 <= u Then
                        End if
                        if i + 2 <= u        Then xu = ValInt(cc(i + 2)) ' 对齐
                        if i + 3 <= u        Then cu = ValInt(cc(i + 3))
                        If IsMultiLanguage() Then 'IsMultiLanguage 后面加()才表示使用函数，不然就是函数指针。
                           clPro &= "      This." & clName & ".InsertColumn " & ci & ", vfb_LangString(""" & YF_Replace(ss ,Chr(34) ,Chr(34 ,34)) & """)," & xu & ", AfxScaleX(" & cu & ")" & vbCrLf
                        Else
                           clPro &= "      This." & clName & ".InsertColumn " & ci & ", """ & YF_Replace(ss ,Chr(34) ,Chr(34 ,34)) & """," & xu & ", AfxScaleX(" & cu & ")" & vbCrLf
                        End if
                     Next
                  End if
               end if
            Case "IMAGENORMALS"
               Dim bb As String = Utf8toStr(Control.pValue(ii))
               if CheckIfTheControlExists(bb ,"IMAGELIST") Then
                  Insert_code(ProWinCode ,"'[Create control end]" ,"      This." & clName & ".ImageList(LVSIL_NORMAL) = This." & Control.pValue(ii) & ".GethImageList")
               end if
            Case "IMAGESMALLS"
               Dim bb As String = Utf8toStr(Control.pValue(ii))
               if CheckIfTheControlExists(bb ,"IMAGELIST") Then
                  Insert_code(ProWinCode ,"'[Create control end]" ,"      This." & clName & ".ImageList(LVSIL_SMALL) = This." & Control.pValue(ii) & ".GethImageList")
               end if
            Case "SINGLESEL" '\单选\2\一次只能选择一个项目。默认情况下，可以选择多个项目。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_SINGLESEL")
            Case "SHOWSEL" '\显示选择\2\即使控件没有焦点，也会始终显示选择内容（如果有的话）。\TRUE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_SHOWSELALWAYS")
            Case "SORT" '\排序\2\项目索引根据项目文本排序。\3 - \0 - 不排序,1 - 升序,2 - 降序
               Select Case ValUInt(Control.pValue(ii))
                  Case 0 '
                  Case 1 '
                     clStyle = TextAddWindowStyle(clStyle ,"LVS_SORTASCENDING")
                  Case 2 '
                     clStyle = TextAddWindowStyle(clStyle ,"LVS_SORTDESCENDING")
               End Select
            Case "SORTCOLUMN"
               If UCase(Control.pValue(ii)) = "TRUE" Then
                  Insert_code(ProWinCode ,"'[CONTROLS_NOTIFY]" , _
                     "         If (FLY_pNotify->idFrom = " & IDC         & ") And (FLY_pNotify->Code = LVN_COLUMNCLICK) Then" & vbCrLf & _
                     "             "                       & Form_clName & "." & clName & ".hWndForm = hWndForm"                 & vbCrLf & _
                     "             "                       & Form_clName & "." & clName & ".ColumnToSort(Cast(Any Ptr, lParam))" & vbCrLf & _
                     "         End If")
               End if
            Case "NOSCROLL" '\禁用滚动\2\滚动功能被禁用。所有项目必须在客户区域内。此样式与LVS_LIST或样式不兼容。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_NOSCROLL")
            Case "AUTOARRANGE" '\排列图标\2\图标模式和小图标模式时自动排列图标\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_AUTOARRANGE")
            Case "EDITLABELS" '\排列图标\2\项目文本可以在控件中当前位置进行编辑。父窗口必须处理LVN_ENDLABELEDIT通知消息。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_EDITLABELS")
            Case "OWNERDATA" '\虚拟表\2\创建一个虚拟列表视图控件\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_OWNERDATA")
            Case "ALIGNTOP" '\顶部对齐\2\项目与图标和小图标视图中列表视图控件的顶部对齐。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_ALIGNTOP")
            Case "ALIGNLEFT" '\左对齐\2\项目在图标和小图标视图中左对齐。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_ALIGNLEFT")
            Case "OWNDRAW" '\自绘控件\2\由自己写代码绘制，列表视图控件发送一个WM_DRAWITEM消息来绘制每个项目\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_OWNERDRAWFIXED")
            Case "COLUMNHEADER" '\标题栏\2\列标题不显示在报告视图中。默认情况下，列在报表视图中具有标题。\TRUE\TRUE,FALSE
               If UCase(Control.pValue(ii)) <> "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_NOCOLUMNHEADER")
            Case "NOSORTHEADER" '\禁用列表\2\禁用报表模式的列表头点击\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clStyle = TextAddWindowStyle(clStyle ,"LVS_NOSORTHEADER")
            Case "BORDERSELECT" '\突出项目\2\当列表选择项目，改变该项目而不是突出项目边框的颜色。\FALSE\TRUE,FALSE
               If UCase(Control.PVALUE(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_BORDERSELECT, LVS_EX_BORDERSELECT)" & vbCrLf
            Case "CHECK" '\复选框\2\在一个LISTVIEW使用复选框。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_CHECKBOXES, LVS_EX_CHECKBOXES)" & vbCrLf
            Case "FLATSCROLLBARS" '\水平滚动\2\LISTVIEW使用水平滚动条。\FALSE\TRUE,FALSE
               If UCase(Control.PVALUE(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_FLATSB, LVS_EX_FLATSB)" & vbCrLf
            Case "FULLROWSELECT" '\选择整行\2\设置LISTVIEW 视图LVS_REPORT模式时选择整个行。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT)" & vbCrLf
            Case "GRIDLINES" '\网格线\2\在LISTVIEW 视图LVS_REPORT模式时项目和子项目显示网格线\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_GRIDLINES, LVS_EX_GRIDLINES)" & vbCrLf
            Case "HEADERDRAGDROP" '\拖放排序\2\LVS_REPORT视图模式下LISTVIEW控件启用列拖和拖放重新排序。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_HEADERDRAGDROP, LVS_EX_HEADERDRAGDROP)" & vbCrLf
            Case "INFOTIP" '\提示通知\2\显示工具提示之前先发送LVN_GET消息通知消息窗口。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_INFOTIP, LVS_EX_INFOTIP)" & vbCrLf
            Case "LABELTIP" '\展开标签\2\如果 LISTVIEW 当前的视图模式缺乏工具提示文本，将展开任何隐藏部分的标签。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_LABELTIP, LVS_EX_LABELTIP)" & vbCrLf
            Case "MULTIWORKAREAS" '\作区排列\2\如果LISTVIEW有 LVS_AUTOARRANGE 样式设置，它不会定义一个或多个工作区自动排列图标。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_MULTIWORKAREAS, LVS_EX_MULTIWORKAREAS)" & vbCrLf
            Case "ONECLICKACTIVATE" '\点击通知\2\当用户点击一个项目时发送一个LVN_ITEMACTIVATE 通知消息到窗口。这种风格也使LISTVIEW控件有热跟踪。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_ONECLICKACTIVATE, LVS_EX_ONECLICKACTIVATE)" & vbCrLf
            Case "REGIONAL" '\区域\2\设置LISTVIEW（与LVS_ICON风格）窗口区域只包括项目的图标和文本使用SETWINDOWRGN。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_REGIONAL, LVS_EX_REGIONAL)" & vbCrLf
            Case "SIMPLESELECT" '\简单选择\2\在图标视图中，移动LISTVIEW的状态图像右上方的大图标显示。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_SIMPLESELECT, LVS_EX_SIMPLESELECT)" & vbCrLf
            Case "SUBITEMIMAGES" '\子项图像\2\允许列表视图（含LVS_REPORT风格）为子项显示图像。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_SUBITEMIMAGES, LVS_EX_SUBITEMIMAGES)" & vbCrLf
            Case "TRACKSELECT" '\热点追踪\2\启用LISTVIEW控件中热点追踪选择。当光标停留在项目上一段时间（通过设置LVM_SETHOVERTIME）项目被自动选择。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_TRACKSELECT, LVS_EX_TRACKSELECT)" & vbCrLf
            Case "TWOCLICKACTIVATE" '\双击通知\2\当用户双击一个项目，LISTVIEW将发送一个 LVN_ITEMACTIVATE消息通知父窗口。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_TWOCLICKACTIVATE, LVS_EX_TWOCLICKACTIVATE)" & vbCrLf
            Case "UNDERLINECOLD" '\非热显示\2\那些可被引起激活的非热的项目，以带有下划线的文本显示。需要TWOCLICKACTIVATE属性被设置为TRUE。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_UNDERLINECOLD, LVS_EX_UNDERLINECOLD)" & vbCrLf
            Case "UNDERLINEHOT" '\热点显示\2\那些可被引起激活的热点项目，以带下划线文本显示。需要ONECLICKACTIVATE或TWOCLICKACTIVATE属性被设置为TRUE。\FALSE\TRUE,FALSE
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SNDMSG(hWndControl, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_UNDERLINEHOT, LVS_EX_UNDERLINEHOT)" & vbCrLf
            Case "THEME"
               If UCase(Control.pValue(ii)) = "TRUE" Then clPro &= "      SetWindowTheme(hWndControl,""Explorer"", NULL)" & vbCrLf
         End Select
      End If
   Next
   
   
   Dim CONTROL_CODExx As String
   If Len(clExStyle) = 0 Then clExStyle = "0"
   If Len(clStyle) = 0   Then clStyle   = "0"
   '真实控件========
   Dim CaptionTxt As String = GetTextToOutText(Control.Caption) '为编译输出文本转换输出文本，可能是多国语言，转换为多国语言字符
   CONTROL_CODExx &= "   hWndControl = pWindow->AddControl(""" & clClName & """, hWnd, " & IDC & ", " & CaptionTxt & ", " & _
      Control.nLeft          & ", "        & Control.nTop       & ", " & Control.nWidth & ", " & Control.nHeight & "," & YF_Replace(clStyle ,"," ," Or ") & " ," & YF_Replace(clExStyle ,"," ," Or ") & _
      " , , Cast(Any Ptr, @" & Form_clName & "_CODEPROCEDURE))" & vbCrLf
   CONTROL_CODExx &= "   If hWndControl Then " & vbCrLf
   CONTROL_CODExx &= "      Dim fp As FormControlsPro_TYPE ptr = new FormControlsPro_TYPE" & vbCrLf
   CONTROL_CODExx &= "      vfb_Set_Control_Ptr(hWndControl,fp)"                           & vbCrLf
   CONTROL_CODExx &= "      fp->hWndParent = hWnd"                                         & vbCrLf
   CONTROL_CODExx &= "      fp->Index = "                                                  & Control.Index & vbCrLf
   CONTROL_CODExx &= "      fp->IDC = "                                                    & IDC           & vbCrLf
   CONTROL_CODExx &= "      fp->nText = "                                                  & CaptionTxt    & vbCrLf
   '   CONTROL_CODExx &= "      fp->ControlType = " & clType & vbCrLf
   CONTROL_CODExx &= "      This." & clName & ".hWnd = hWndControl " & vbCrLf '真实控件========
   CONTROL_CODExx &= "      This." & clName & ".IDC ="               & IDC & vbCrLf
   
   CONTROL_CODExx &= clPro
   CONTROL_CODExx &= "   End IF" & vbCrLf
   Insert_code(ProWinCode ,"'[Create control]" ,CONTROL_CODExx)
   
   '事件处理 ------------------------------
   Dim LeaveHoverI As Long
   '控件事件
   For ii = 1 To ColTool.elU
      Dim sim As String '事件函数名组合
      sim = " " & UCase(Form_clName & "_" & StrToUtf8(Control.nName & "_" & ColTool.EveList(ii).sName)) & "("
      Dim ff As Long
      for fi As Long = 0 To UBound(ussl)
         If left(ussl(fi) ,1) <> "'" AndAlso InStr(ussl(fi) ,sim) > 0 Then
            ff = fi + 1
            Exit for
         End If
      Next
      If ff > 0 Then
         if IsEventComparison(Control ,ColTool ,ii ,ff ,nFile ,ussl(ff -1) ,Form_clName) Then Return 3 '检查事件是不是正确
         Select Case ColTool.EveList(ii).tMsg
            Case "LVN_BEGINDRAG" ,"LVN_BEGINLABELEDIT" ,"LVN_BEGINRDRAG" ,"LVN_COLUMNCLICK" ,"LVN_DELETEALLITEMS" ,"LVN_DELETEITEM" ,"LVN_ITEMACTIVATE" , _
                  "LVN_ENDLABELEDIT" ,"LVN_GETDISPINFO" ,"LVN_INSERTITEM" ,"LVN_ITEMCHANGED" ,"LVN_ITEMCHANGING" ,"LVN_KEYDOWN"
               Dim CONTROLS_NOTIFY As String = "         If (FLY_pNotify->idFrom = " & IDC & ") And (FLY_pNotify->Code = " & ColTool.EveList(ii).tMsg & ") Then" & vbCrLf
               If Right(ColTool.EveList(ii).Param ,1) = ")" Then '是SUB
                  CONTROLS_NOTIFY &= "             " & sim
               Else
                  CONTROLS_NOTIFY &= "              tLResult = " & sim
               end if
               If Control.Index > -1 Then CONTROLS_NOTIFY &= Control.Index & ","
               CONTROLS_NOTIFY &= ColTool.EveList(ii).gCall & "  " & nFile & ff -1 & "]" & vbCrLf
               If Right(ColTool.EveList(ii).Param ,1) <> ")" Then CONTROLS_NOTIFY &= "            If tLResult Then Return tLResult" & vbCrLf
               CONTROLS_NOTIFY &= "         End If"
               Insert_code(ProWinCode ,"'[CONTROLS_NOTIFY]" ,CONTROLS_NOTIFY)
            Case "CUSTOM"
               dim CALL_CONTROL_CUSTOM As String = "    If IDC = " & IDC & " Then  ' " & clName & vbCrLf
               CALL_CONTROL_CUSTOM &= "       tLResult = " & sim
               If Control.Index > -1 Then CALL_CONTROL_CUSTOM &= Control.Index & ","
               CALL_CONTROL_CUSTOM &= ColTool.EveList(ii).gCall                 & "  " & nFile & ff -1 & "]" & vbCrLf
               CALL_CONTROL_CUSTOM &= "       If tLResult Then Return tLResult" & vbCrLf
               CALL_CONTROL_CUSTOM &= "    End If"                              & vbCrLf
               Insert_code(ProWinCode ,"'[CALL_CONTROL_CUSTOM]" ,CALL_CONTROL_CUSTOM)
            Case "OWNERDRAW"
               ProWinCode = YF_Replace(ProWinCode ,"'{FORM_WM_DRAWITEM}" ,"      Case WM_DRAWITEM" & vbCrLf & "         Dim lpdis As DRAWITEMSTRUCT Ptr = Cast(Any Ptr, lParam)")
               Dim FORM_WM_DRAWITEM As String = "         If Cast(Long, wParam) = " & IDC & "  Then ' " & clName & vbCrLf
               FORM_WM_DRAWITEM &= "           tLResult = " & sim
               If Control.Index > -1 Then FORM_WM_DRAWITEM &= Control.Index & ","
               FORM_WM_DRAWITEM &= ColTool.EveList(ii).gCall                     & "  " & nFile & ff -1 & "]" & vbCrLf
               FORM_WM_DRAWITEM &= "           If tLResult Then Return tLResult" & vbCrLf
               FORM_WM_DRAWITEM &= "         End If"                             & vbCrLf
               Insert_code(ProWinCode ,"'[FORM_WM_DRAWITEM]" ,FORM_WM_DRAWITEM)
            Case Else
               If ColTool.EveList(ii).tMsg = "WM_MOUSEHOVER" Then LeaveHoverI Or= 1
               If ColTool.EveList(ii).tMsg = "WM_MOUSELEAVE" Then LeaveHoverI Or= 10
               Dim ca    As String = "      Case "         & ColTool.EveList(ii).tMsg & " ''' "
               Dim other As String = "          If IDC = " & IDC                      & " Then  ' " & clName & vbCrLf
               If Right(ColTool.EveList(ii).Param ,1) <> ")" Then '这是函数
                  other &= "          tLResult = " & sim
                  If Control.Index > -1 Then other &= Control.Index & ","
                  other &= ColTool.EveList(ii).gCall                    & "  " & nFile & ff -1 & "]" & vbCrLf
                  other &= "          If tLResult Then Return tLResult" & vbCrLf
               Else '这是过程
                  other &= "             " & sim
                  If Control.Index > -1 Then other &= Control.Index & ","
                  other &= ColTool.EveList(ii).gCall & "  " & nFile & ff -1 & "]" & vbCrLf
               End If
               other &= "          End If" & vbCrLf
               ff    = InStr(ProWinCode ,ca)
               If ff = 0 Then '不存在
                  Insert_code(ProWinCode ,"'[CONTROL_CASE_OTHER]" ,ca & vbCrLf & other)
               Else '已经有了
                  ProWinCode = Left(ProWinCode ,ff + Len(ca) -1) & vbCrLf & other & Mid(ProWinCode ,ff + Len(ca))
               End If
         End Select
      End If
   Next
   
   If LeaveHoverI > 0 Then
      Dim CONTROL_LEAVEHOVER As String = "          If wMsg = WM_MouseMove AndAlso IDC = " & IDC & " Then  ' " & clName & vbCrLf
      CONTROL_LEAVEHOVER &= "             Dim entTrack As tagTRACKMOUSEEVENT"           & vbCrLf
      CONTROL_LEAVEHOVER &= "             entTrack.cbSize = SizeOf(tagTRACKMOUSEEVENT)" & vbCrLf
      If LeaveHoverI = 11 Then
         CONTROL_LEAVEHOVER &= "             entTrack.dwFlags = TME_LEAVE Or TME_HOVER" & vbCrLf
      ElseIf LeaveHoverI = 10 Then
         CONTROL_LEAVEHOVER &= "             entTrack.dwFlags = TME_LEAVE " & vbCrLf
      Else
         CONTROL_LEAVEHOVER &= "             entTrack.dwFlags =  TME_HOVER" & vbCrLf
      End If
      CONTROL_LEAVEHOVER &= "             entTrack.hwndTrack = hWndControl"     & vbCrLf
      CONTROL_LEAVEHOVER &= "             entTrack.dwHoverTime = HOVER_DEFAULT" & vbCrLf
      CONTROL_LEAVEHOVER &= "             TrackMouseEvent @entTrack"            & vbCrLf
      CONTROL_LEAVEHOVER &= "          End IF"                                  & vbCrLf
      Insert_code(ProWinCode ,"'[CONTROL_LEAVEHOVER]" ,CONTROL_LEAVEHOVER)
   End If
   
   '成功返回0，失败非0
   Function = 0
End Function























