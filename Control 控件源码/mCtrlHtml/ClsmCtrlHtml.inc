'控件类
#include Once "..\mCtrlPublic\html.bi"
Type Class_mCtrlHtml Extends Class_Control
    Declare Function GotoUrl(Url As CWSTR) As Long  '显示网址 （BOOL）TRUE成功，FALSE否则。
    Declare Function SetTagContents(ID As CWSTR , CodeHtml As CWSTR) As Long  '使用给定属性"id"（Unicode变体）设置HTML标记的内容。 （BOOL）TRUE成功，FALSE否则。
    Declare Function GoBack() As Long  '后退网页（BOOL）TRUE成功，FALSE否则。
    Declare Function GoForward() As Long  '前进网页（BOOL）TRUE成功，FALSE否则。
    Declare Function IsBack() As Long  '是否可以后退网页 （BOOL）TRUE成功，FALSE否则。
    Declare Function IsForward() As Long  '是否可以前进网页（BOOL）TRUE成功，FALSE否则。
    Declare Function CallScriptFunc(FunName As CWSTR,p1 As CWSTR="",p2 As CWSTR="",p3 As CWSTR="",p4 As CWSTR="" ) As CWSTR  '在HTML页面中调用脚本函数。p1--p4为参数（BOOL）TRUE成功，FALSE否则。
    Declare Function CallScriptFuncEX(f As MC_HMCALLSCRIPTFUNCEX) As HRESULT  '更多功能的在HTML页面中调用脚本函数。（S_OK如果调用成功），否则返回HRESULT错误代码。

End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Function Class_mCtrlHtml.GotoUrl(Url As CWSTR) As Long  
  Function = SendMessage( hWndControl , MC_HM_GOTOURLW , 0 ,Cast(lParam, Url.vptr)  ) 
End Function

Function Class_mCtrlHtml.SetTagContents(ID As CWSTR , CodeHtml As CWSTR) As Long 
  Function = SendMessage( hWndControl , MC_HM_SETTAGCONTENTSW , Cast(wParam, ID.vptr)  ,Cast(lParam, CodeHtml.vptr)  ) 
End Function

Function Class_mCtrlHtml.GoBack() As Long  
  Function = SendMessage( hWndControl , MC_HM_GOBACK , True ,0  ) 
End Function
Function Class_mCtrlHtml.GoForward() As Long  
  Function = SendMessage( hWndControl , MC_HM_GOBACK , False ,0  ) 
End Function

Function Class_mCtrlHtml.IsBack() As Long  
  Function = SendMessage( hWndControl , MC_HM_CANBACK , True ,0  ) 
End Function
Function Class_mCtrlHtml.IsForward() As Long  
  Function = SendMessage( hWndControl , MC_HM_CANBACK , False ,0  ) 
End Function
Function Class_mCtrlHtml.CallScriptFunc(FunName As CWSTR, p1 As CWSTR = "", p2 As CWSTR = "", p3 As CWSTR = "", p4 As CWSTR = "") As CWSTR
   Dim pp As MC_HMCALLSCRIPTFUNCW
   pp.cbSize = SizeOf(MC_HMCALLSCRIPTFUNCW)
   Dim by(10244) As UByte 
   pp.pszRet = Cast(WString Ptr,@by(0))
   pp.iRet = 10240
   if Len(p1) Then 
      pp.cArgs =1
      pp.pszArg1 = p1.vptr 
      if Len(p2) Then 
         pp.cArgs =2
         pp.pszArg2 = p2.vptr 
         if Len(p3) Then 
            pp.cArgs = 3
            pp.pszArg3 = p3.vptr 
            if Len(p4) Then 
               pp.cArgs =4
               pp.pszArg4 = p4.vptr 
            end if 
         end if 
      end if 
   end if 
   if SendMessage(hWndControl, MC_HM_CALLSCRIPTFUNCW,Cast(wParam, FunName.vptr), 0) Then
       Return  *Cptr(WString Ptr,@by(0))
   End if
End Function
Function Class_mCtrlHtml.CallScriptFuncEX(f As MC_HMCALLSCRIPTFUNCEX) As HRESULT  
  Function = SendMessage( hWndControl , MC_HM_CALLSCRIPTFUNCEX , 0 , Cast(lParam, @f)  ) 
End Function






