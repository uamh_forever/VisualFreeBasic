Type Class_Custom Extends Class_Control

   Declare Property Caption() As CWStr                '返回/设置控件中的文本
   Declare Property Caption(ByVal sText As CWStr)
   
End Type

Property Class_Custom.Caption() As CWStr
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_Custom.Caption(ByVal sText As CWStr)
  AfxSetWindowText  hWndControl, sText
End Property