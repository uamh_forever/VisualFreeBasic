﻿'这里是控件主模块 ==================
'  VisualFreeBasic 控件工作流程
'VFB启动时扫描控件文件夹里的每个文件夹，每个文件夹其中一个为 控件DLL，也就是本工程生产的DLL
'第1步：动态加载DLL
'第2步：检查DLL输出函数，都存在继续下一步，否则认为不是 控件DLL 就卸载DLL
'第3步：调用  initialization  来确定协议版本，不符合的就VFB会提示需要多少版本的。并且卸载DLL
'第4步：调用  SetControlProperty 由 DLL来加载控件属性
'当打开一个工程用到本控件时，调用 Edit_AddControls 加载控件到 窗口编辑器
'用户修改或加载控件后，调用 Edit_SetControlProperty 来设置 窗口编辑器中控件 属性
'若控件有特殊属性需要在线处理，调用 Edit_ControlPropertyAlter 来设置，此时由本DLL负责处理，比如 菜单编辑
'当虚拟控件需要绘画时，调用 Edit_OnPaint 来画控件，实体控件是系统负责画，就不需要了。
'编译软件时，调用 Compile_ExplainControl 解析控件，把控件属性翻译成具体代码。
'而控件类是提供给代码调用的。


Function initialization() As Long Export '初始化
                        '当 VFB5 主软件加载本DLL后，主动调用一下此函数，可以在此做初始化代码
   SetFunctionAddress() '设置函数地址
   Function = 7 '返回协议版本号，不同协议（发生接口变化）不能通用，会发生崩溃等问题，因此VFB主软件会判断此版本号，不匹配就不使用本控件。
End Function
Function SetControlProperty(ByRef ColTool As ColToolType) As Long Export '设置控件工具属性。
   '处理多国语言 -----  如果本控件不支持多国语言，可以删除
   Dim op     As pezi Ptr     = GetOpAPP()
   Dim ExeApp As APP_TYPE Ptr = GetExeAPP()
   vfb_LoadLanguage(ExeApp->Path & "Languages\" & op->Languages & "\Languages.txt" ,App.EXEName)
   'vfb_LangString 为支持多国语言，工程属性里选上 多国语言，编译后把 Languages.txt 内容复制到VFB的语言文件里即可。
   '设置控件基本属性 --------------
   ColTool.sName     = "YFproTab" 'Name      控件名称，必须英文
   ColTool.sTips     = vfb_LangString("加强版标签控件") 'Tips      鼠标停留在控件图标上提示的内容
   ColTool.uName     = UCase(ColTool.sName)
   ColTool.sVale     = &HF13D                          'Ico       字体图标的字符值，字体文件在：VisualFreeBasic5\Settings\iconfont.ttf  如果有 *.ico 文件则优先显示图标文件，而不是字体图标。
   ColTool.Feature   = 2                               'Feature   特征 =0 不使用 =1 主窗口(只能有1个，且永远在第一个) =2 普通控件（有窗口句柄） =3 虚拟控件有界面（无句柄） =4 虚拟控件无界面（组件）
   ColTool.ClassFile = "ClsYFproTab.inc"                 'ClassFile 控件类文件名
   ColTool.Only      = 0                               'Only      是否是唯一的，就是一个窗口只能有1个此控件
   ColTool.GROUP     = vfb_LangString("扩展控件,勇芳系列") 'Group     分组名称，2个中文到4个最佳，属于多个分组，用英文豆号分割
   
   '设置控件的属性（窗口编辑器里的属性和选项，写代码的属性是控件类负责，代码提示则在帮助里修改。）和事件（代码编辑时可选的事件）
   '从配置文件中读取属性和事件，必须保证和DLL同文件下的 Attribute.ini Event.ini 配置文件正常
   '若不想用配置文件，也可以直接用代码赋值配置。
   'language <>0支持多国语言，会去VFB主语言文件里读取语言，修改配置里的文字。
   If AttributeOREvent(ColTool ,True) Then Return -1 '返回 -1 表示发生问题，VFB将会直接退出。
   
   Function = 56  '返回 排序号，控件在 IDE 中控件列表的先后位置，必须从2开始，主窗口 Form=0  Pointer=1 ，其它 2--n  从小到大排列
   
End Function
Function Edit_AddControls(cw As CWindow Ptr,hParent As hWnd,IDC As ULong ,Caption As CWSTR, x As Long, y As Long, w As Long, h As Long,WNDPROC As Any Ptr) As hWnd Export '增加1个控件 
   '编辑：窗口刚被打开，需要新建，返回新建后的控件窗口句柄
   'cw      基于CWindow创建，这是当前窗口的 CWindow 指针
   'hParent 父窗口句柄
   'IDC     控件IDC号
   'Caption 控件标题
   'xywh    位置
   'WndProc 主窗口处理消息的函数地址(窗口消息回调函数)

   Function = cw->AddControl("LABEL", hParent, IDC, Caption, x, y, w, h,WS_CHILD Or WS_VISIBLE oR WS_CLIPSIBLINGS OR SS_LEFT OR WS_GROUP OR SS_OWNERDRAW , , , WndProc)

End Function

Function Edit_SetControlProperty(ByRef Control As clsControl, ByRef ColTool As ColToolType, ki As Long) As Long Export '设置控件属性
   '编辑：新创建控件、修改控件属性后，都调用1次
   'Control  窗口中的控件
   'ColTool  当前控件配置和属性
   'ki       被修改的属性索引，=0为全部
   Dim hWndControl As hWnd = Control.nHwnd
   Dim vv As String, cvv As CWSTR, i As Long
   
   For i = 1 To ColTool.plU
      vv = Control.pValue(i) '值是 Utf8 格式
      cvv.UTF8 = YF_Replace(vv, Chr(3, 1), vbCrLf)
      '先设置通用部分
      Select Case ColTool.ProList(i).uName
         Case "CAPTION"
            if ColTool.uName <> "STATUSBAR" Then
               SetWindowTextW hWndControl, cvv.vptr
            End if
            Control.Caption = YF_Replace(vv, Chr(3, 1), vbCrLf)
         Case "ICON"
            Dim pa As String = GetProRunFile(0,4)
            Dim svv As String, fvv As Long = InStr(vv, "|")
            if fvv = 0 Then svv = vv Else svv = Left(vv, fvv -1)
            Dim hIcon As HICON = LoadImage(Null, pa & "images\" & Utf8toStr(svv), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE Or LR_LOADFROMFILE)
            If hIcon Then
               hIcon = AfxSetWindowIcon(hWndControl, ICON_SMALL, hIcon)
               If hIcon Then DestroyIcon(hIcon)
            End If
         Case "LEFT"
            If ki = i Then  '只有控件才设置，主窗口不设置
               Control.nLeft = ValInt(vv)
               FF_Control_SetLoc hWndControl, AfxScaleX(Control.nLeft), AfxScaleY(Control.nTop)
            End If
         Case "TOP"   '只有控件才设置，主窗口不设置
            If ki = i Then
               Control.nTop = ValInt(vv)
               FF_Control_SetLoc hWndControl, AfxScaleX(Control.nLeft), AfxScaleY(Control.nTop)
            End If
         Case "WIDTH"
            If ki = i And ColTool.Feature <> 4 Then
               Control.nWidth = ValInt(vv)
               FF_Control_SetSize hWndControl, AfxScaleX(Control.nWidth), AfxScaleY(Control.nHeight)
            End If
         Case "HEIGHT"
            If ki = i And ColTool.Feature <> 4 Then
               Control.nHeight = ValInt(vv)
               FF_Control_SetSize hWndControl, AfxScaleX(Control.nWidth), AfxScaleY(Control.nHeight)
            End If
         Case "CHILD"
         Case "MOUSEPOINTER"
         Case "FORECOLOR"
            Control.ForeColor = GetColorText(vv)
         Case "BACKCOLOR"
            Control.BackColor = GetColorText(vv)
         Case "TAG"
         Case "TAB"
         Case "ACCEPTFILES"
         Case "INDEX"
         Case "FONT"
            Dim tFont As HFONT = GetWinFontLog(vv)
            SendMessage hWndControl, WM_SETFONT, Cast(wParam, tFont), True
            Control.Font = vv
         Case "TOOLTIPBALLOON"
         Case "TOOLTIP"
            '==============     以上是公共设置，下面是每个控件私有设置    =================
         Case "NAME" '\样式\2\指示控件边界的外观和行为。
            SetWindowTextW hWndControl, cvv.vptr
         Case "STYLE"
            Select Case ValUInt(vv)
               Case 0  '无边框
                  AfxRemoveWindowStyle hWndControl, WS_BORDER
                  AfxRemoveWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
               Case 1  '细边框
                  AfxAddWindowStyle hWndControl, WS_BORDER
                  AfxRemoveWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
               Case 2  '半边框
                  AfxRemoveWindowStyle hWndControl, WS_BORDER
                  AfxRemoveWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxAddWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
               Case 3  '凹边框
                  AfxRemoveWindowStyle hWndControl, WS_BORDER
                  AfxAddWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
               Case 4 ' 凸边框
                  AfxRemoveWindowStyle hWndControl, WS_BORDER
                  AfxRemoveWindowExStyle hWndControl, WS_EX_CLIENTEDGE
                  AfxRemoveWindowExStyle hWndControl, WS_EX_STATICEDGE
                  AfxAddWindowExStyle hWndControl, WS_EX_DLGMODALFRAME
            End Select
      End Select
   Next
   Function = 0
End Function

Function Edit_ControlPropertyAlter(hWndForm As hWnd, hWndList As hWnd, nType As Long, value As String, default As String, AllList As String, nName As String, FomName As String) As Long Export  ' 控件属性修改
   '编辑：用户点击窗口属性，修改属性时，1--6由EXE处理，7 或其它由本DLL处理
   'hWndForm   EXE 主窗口句柄
   'hWndList   控件属性显示窗口句柄（是List控件）
   'nType      类型，由 Attribute.ini 里设置，7 或其它由本DLL处理
   'value      当前的值
   'default    默认值，由 Attribute.ini 里设置
   'AllList    所有值，由 Attribute.ini 里设置
   Select Case nType  '这里根据需要编写
      Case 100
         Dim aa As StyleFormType
         'aa.hWndForm = hWndForm
         'aa.hWndList = hWndList
         'aa.nType = nType
         'aa.value = @value
         'aa.default = @default
         'aa.AllList = @AllList
         'aa.Rvalue = value
         'aa.nName = nName : aa.FomName = FomName '当前被编辑的控件名和窗口名
         'StyleForm.Show hWndForm, True, Cast(Integer, @aa)
         value = aa.Rvalue
         Function = len(value)
         
   End Select
End Function
Function Edit_OnPaint(gg As yGDI ,Control As clsControl ,ColTool As ColToolType ,WinCc As Long ,nFile As String) As Long Export '描绘控件
   '编辑：当被刷新窗口，需要重绘控件时，窗口和实控件由系统绘画，不需要我们在这里处理，虚拟控件必须由此画出来。
   'gg    目标， 画在这个缓冲里。
   'Control  窗口中的控件
   'ColTool  当前控件配置和属性
   'WinCc    主窗口底色，不是本控件底色
   'nFile    当前工程主文件名，带文件夹，用来提取路径用。
   '返回非0  将会立即结束描绘操作，就是在此之后的控件就不会画了。按照最底层的控件先画。
   Dim cBack       As Long = &HDA8A22 '背景色
   Dim cWord       As Long = &HE0E0E0 '字色
   Dim cSelBack    As Long = &H3B3B3B '选择背景色
   Dim cSelWord    As Long = &HE0E0E0 '选择色
   Dim cMove       As Long
   Dim mTabH       As Long = 28 '标签区高度
   Dim vAddButton  As Long = -1 '是否显示新增按钮
   Dim vDownButton As Long = -1 '是否显示下拉按钮
   Dim ii          As Long
   Dim SelStyle    As Long ,vLeftIndent As Long
   For ii = 1 To ColTool.plU
      Select Case ColTool.ProList(ii).uName
         Case "TABH"
            mTabH = ValInt(Control.PVALUE(ii))
         Case "ADDBUTTON"
            vAddButton = IIf(UCase(Control.PVALUE(ii)) = "TRUE" , -1 ,0)
         Case "DOWNBUTTON"
            vDownButton = IIf(UCase(Control.PVALUE(ii)) = "TRUE" , -1 ,0)
         Case "WORD"
            cWord = GetCodeColorGDI(GetColorText(Control.PVALUE(ii)) ,0)
         Case "BACK"
            cBack = GetCodeColorGDI(GetColorText(Control.PVALUE(ii)) ,0)
         Case "SELBACK"
            cSelBack = GetCodeColorGDI(GetColorText(Control.PVALUE(ii)) ,0)
         Case "SELWORD"
            cSelWord = GetCodeColorGDI(GetColorText(Control.PVALUE(ii)) ,0)
         Case "MOVE"
            cMove = GetCodeColorGDI(GetColorText(Control.PVALUE(ii)) ,0)
         Case "SELSTYLE"
            SelStyle = ValInt(Control.PVALUE(ii))
         Case "LEFTINDENT"
            vLeftIndent = ValInt(Control.PVALUE(ii))
      End Select
   Next
   gg.Cls cBack
   
   Dim TabDatavName(5) As String ,TabDatavIco(5) As Long
   TabDatavName(0) = "主页"
   TabDatavIco(0)  = 61656
   TabDatavName(1) = "标签A"
   TabDatavIco(1)  = 61660
   TabDatavName(2) = "标签B"
   TabDatavIco(2)  = 61652
   TabDatavName(3) = "标签c"
   TabDatavName(4) = "标签d"
   TabDatavName(5) = "标签e"
   Dim u  As Long = 5
   Dim ww As Long = AfxUnscaleX(gg.m_Width) ,hh As Long = AfxUnscaleY(gg.m_Height)
   Dim i  As Long ,x As Single ,y As Long ,aw As Single ,bw As Single ,tw As Long = ww
   If mTabH < 10 Then mTabH = 10
   y = hh - mTabH
   'Print @gg,  ww,hh
   x = vLeftIndent
   gg.SetColor cWord
   gg.Pen 1 ,cWord
   gg.Brush
   Dim index As Long = 1
   If vAddButton  Then tw -= 25
   If vDownButton Then tw -= 15
   
   bw = tw / (u + 1) '平均宽度，当有标签超过平均，就限制为平均宽度
   If bw < 25 Then bw = 25
   For i = 0 To u
      aw = gg.GetTextWidth(TabDatavName(i))
      if TabDatavIco(i) Then aw += 20
      aw += 15 '这里是标签2边空边
      If aw > bw Then aw = bw
      If i = index Then
         gg.SetColor cSelWord
         DepictSelection(gg ,x ,y ,aw ,mTabH ,ColorGdiToGDIplue(cSelBack) ,SelStyle ,cMove)
      ElseIf i = 3 Then
         gg.SetColor cWord
         DepictSelection(gg ,x ,y ,aw ,mTabH ,ColorGdiToGDIplue(cMove) ,SelStyle ,cMove)
      End If
      Dim vx As Long = 7
      If TabDatavIco(i) Then vx = 25
      
      gg.DrawTextS x + vx ,y ,aw - vx ,mTabH ,TabDatavName(i) ,DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX Or DT_WORD_ELLIPSIS
      If TabDatavIco(i) Then
         gg.Font "iconfont" ,12
         gg.DrawTextS x + 5 ,y ,20 ,mTabH ,WChr(TabDatavIco(i)) ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE
         gg.Font
      End If
      
      x += aw
      If i = Index Then
         gg.SetColor cWord
         gg.Pen 1 ,cWord
         gg.Brush
      Else
         If i + 1 <> index And i < u Then '分割线
            gg.GpPen 1      ,ColorGdiToGDIplue(cWord ,128)
            gg.gpDrawLine x ,y + (mTabH / 2 -6) ,x ,y + (mTabH / 2 + 6)
         End If
      End If
      
   Next
   
   x += 6
   If vAddButton Then '是否显示新增按钮
      gg.gpPen 2 ,ColorGdiToGDIplue(cWord)
      gg.gpDrawLine x + 2 ,y + mTabH / 2 ,x + 12 ,y + mTabH / 2
      gg.gpDrawLine x + 7 ,y + (mTabH / 2 -5) ,x + 7 ,y + (mTabH / 2 + 5)
   End If
   If vDownButton Then '是否显示下拉按钮
      gg.gpPen 0 ,0
      gg.GpBrush ColorGdiToGDIplue(cWord)
      
      Dim POINTS(2) As GpPointF
      POINTS(0).x = ww -13
      POINTS(0).y = y  + (mTabH -13)
      POINTS(1).x = ww - 2
      POINTS(1).y = y  + (mTabH -13)
      POINTS(2).x = ww -7.5
      POINTS(2).y = y + (mTabH -4)
      gg.GpDrawPolygon POINTS()
   End If
   
   Function = 1
End Function
Sub DepictSelection(gg As yGDI ,x As Long ,y As Long ,w As Long ,h As Long ,cc As Long ,mSelStyle As Long,cMove As Long ) '描绘选择
   Dim i As Long ,cl As Long
   Select Case mSelStyle
      Case 1
         gg.GpPathNew
         gg.GpPathArc x -24 ,y + (h -23) ,24 ,24 ,90 , -90
         gg.GpPathArc x ,y  ,16 ,16 ,180 ,90
         gg.GpPathArc x + w -16     ,y   ,16 ,16 ,270 ,90
         gg.GpPathArc x + w ,y + (h -23)     ,24 ,24  ,180 , -90
         gg.gpBrush 0
         gg.gpPen 0 ,0
         gg.gpBrush cc
         gg.GpPathDraw
      Case 2
         gg.GpPathNew
         gg.GpPathArc x ,y ,10 ,10 ,180 ,90
         gg.GpPathArc x  + w -10   ,y   ,10 ,10 ,270 ,90
         gg.GpPathLine x + w ,y + h         ,x  ,y + h
         gg.GpPathLine x ,y + h ,x ,y + 5
         gg.gpBrush 0
         For i = 6 To 2 Step -2
            cl = RGBA(0 ,0 ,0 ,(9 - i) * 2 + 20)
            gg.gpPen i ,cl
            gg.GpPathDraw
         Next
         gg.gpPen 0 ,0
         gg.gpBrush cc
         gg.GpPathDraw
      Case 3
         gg.GpPathNew
         gg.GpPathArc x ,y ,10 ,10 ,180 ,90
         gg.GpPathArc x  + w -10   ,y   ,10 ,10 ,270 ,90
         gg.GpPathLine x + w ,y + h         ,x  ,y + h
         gg.GpPathLine x ,y + h ,x ,y + 5
         gg.gpBrush 0
         gg.gpPen 0 ,0
         gg.gpBrush cc
         gg.GpPathDraw
      Case 4
         gg.GpPathNew
         gg.GpPathArc x ,y ,10 ,10 ,180 ,90
         gg.GpPathArc x  + w -10   ,y   ,10 ,10 ,270 ,90
         gg.GpPathLine x + w ,y + h         ,x  ,y + h
         gg.GpPathLine x ,y + h ,x ,y + 5
         gg.gpPen 0 ,0
         gg.gpBrush ColorGdiToGDIplue(cMove)
         gg.GpPathDraw
         gg.gpBrush cc
         gg.GpDrawFrame x,y+h-4,w,4
      Case 5
         gg.GpPathNew
         gg.GpPathArc x ,y ,10 ,10 ,180 ,90
         gg.GpPathArc x  + w -10   ,y   ,10 ,10 ,270 ,90
         gg.GpPathLine x + w ,y + h         ,x  ,y + h
         gg.GpPathLine x ,y + h ,x ,y + 5
         gg.gpPen 0 ,0
         gg.gpBrush ColorGdiToGDIplue(cMove)
         gg.GpPathDraw
         gg.gpBrush cc
         gg.GpDrawFrame x+4,y,w-8,3   
      Case Else
         gg.GpPathNew
         gg.GpPathArc x -18 ,y + (h -17) ,18 ,18 ,90 , -90
         gg.GpPathArc x ,y  ,16 ,16 ,180 ,90
         gg.GpPathArc x + w -16     ,y   ,16 ,16 ,270 ,90
         gg.GpPathArc x + w ,y + (h -17)     ,18 ,18  ,180 , -90
         gg.gpBrush 0
         For i = 6 To 2 Step -2
            cl = RGBA(0 ,0 ,0 ,(9 - i) * 2 + 20)
            gg.gpPen i ,cl
            gg.GpPathDraw
         Next
         gg.gpPen 0 ,0
         gg.gpBrush cc
         gg.GpPathDraw
   End Select
   
   
End Sub

Function Compile_ExplainControl(Control As clsControl ,ColTool As ColToolType ,ProWinCode As String ,ussl() As String ,ByRef IDC As Long ,DECLARESdim As String ,Form_clName As String ,nFile As String) As Long Export '解释控件，制造创建控件和事件的代码
   '编译：解释控件 ，注意：编译处理字符全部为 UTF8 编码。Control和ColTool里的是 A字符。
   'Control      窗口中的控件
   'ColTool      当前控件配置和属性
   'ProWinCode   处理后的窗口代码，最初由窗口加载窗口模板处理，然后分发给其它控件。填充处理
   'ussl()       已特殊处理过的用户写的窗口代码，主要用来识辨事件
   'IDC          控件IDC，每个控件唯一，VFB自动累计1，我们代码也可以累计
   'DECLARESdim  全局变量定义，整个工程的定义都在此处
   'Form_clName  主窗口类名，最初由窗口设置，方便后面控件使用。
   'nFile        窗口文件名，用在事件调用注释，出错时可以提示源文件地方，避免提示临时文件。
   
   
   '创建控件 ------------------------------
   Dim ii As Long
   Dim As String clClName ,clName ,clStyle ,clExStyle ,clPro
   
   Dim As Long clType '为了解释代码里用，>=100 为虚拟控件  100=LABEL 1=TEXT
   clName = StrToUtf8(Control.nName)
   If Control.Index > -1 Then clName &= "(" & Control.Index & ")"
   clClName  = "LABEL"
   clType    = 0
   clStyle   = "WS_CHILD,WS_VISIBLE,SS_NOTIFY,WS_CLIPCHILDREN,WS_CLIPSIBLINGS"
   clExStyle = ""
   For ii = 1 To ColTool.plU
      if ExplainControlPublic(Form_clName ,Control ,clName ,ii ,ColTool.ProList(ii).uName ,clType ,clStyle ,clExStyle ,clPro ,ProWinCode) Then '处理公共部分，已处理返回0，未处理返回非0
         Select Case ColTool.ProList(ii).uName
               'Case "NAME"  '名称\1\用来代码中识别对象的名称
               'Case "INDEX"  '数组索引\0\控件数组中的控件位置的索引数字。值小于零表示不是控件数组
               'Case "CAPTION"  '文本\1\显示的文本\Label\
               'Case "TEXT"  '文本\1\显示的文本\Label\
               'Case "ENABLED"  '允许\2\创建控件时最初是否允许操作。\True\True,False
               'Case "VISIBLE"  '显示\2\创建控件时最初是显示或隐藏。\True\True,False
               'Case "FORECOLOR"  '文字色\3\用于在对象中显示文本和图形的前景色。\SYS,8\
               'Case "BACKCOLOR"  '背景色\3\用于在对象中显示文本和图形的背景色。\SYS,15\
               'Case "FONT"  '字体\4\用于此对象的文本字体。\微软雅黑,9,0\
               'Case "LEFT"  '位置X\0\左边缘和父窗口的左边缘之间的距离。自动响应DPI缩放\0\
               'Case "TOP"  '位置Y\0\内部上边缘和父窗口的顶部边缘之间的距离。自动响应DPI缩放\0\
               'Case "WIDTH"  '宽度\0\窗口宽度，100%DPI时的像素单位，自动响应DPI缩放。\100\
               'Case "HEIGHT"  '高度\0\窗口高度，100%DPI时的像素单位，自动响应DPI缩放。\20\
               'Case "LAYOUT"
               'Case "MOUSEPOINTER"  '鼠标指针\2\鼠标在窗口上的形状\0 - 默认\0 - 默认,1 - 后台运行,2 - 标准箭头,3 - 十字光标,4 - 箭头和问号,5 - 文本工字光标,6 - 不可用禁止圈,7 - 移动,8 - 双箭头↙↗,9 - 双箭头↑↓,10 - 双箭头向↖↘,11 - 双箭头←→,12 - 垂直箭头,13 - 沙漏,14 - 手型
               'Case "TAG"  '附加\1\私有自定义文本与控件关联。\\
               'Case "TAB"  '导航\2\当用户按下TAB键时可以接收键盘焦点。\False\True,False
               'Case "TOOLTIP"  '提示\1\一个提示，当鼠标光标悬停在控件时显示它。\\
               'Case "TOOLTIPBALLOON"  '气球样式\2\一个气球样式显示工具提示。\False\True,False
               'Case "ACCEPTFILES"  '拖放\2\窗口是否接受拖放文件。\False\True,False
               '==============     以上是公共设置，下面是每个控件私有设置    =================
            Case "STYLE" '\样式\2\指示控件边界的外观和行为。\3 - 凹边框\0 - 无边框,1 - 细边框,2 - 半边框,3 - 凹边框,4 - 凸边框
               Select Case ValInt(Control.pValue(ii))
                  Case 0 '无边框
                     clStyle   = TextRemoveWindowStyle(clStyle ,"WS_BORDER")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_CLIENTEDGE")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_STATICEDGE")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_DLGMODALFRAME")
                  Case 1 '细边框
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_CLIENTEDGE")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_STATICEDGE")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_DLGMODALFRAME")
                     clStyle   = TextAddWindowStyle(clStyle ,"WS_BORDER")
                  Case 2 '半边框
                     clStyle   = TextRemoveWindowStyle(clStyle ,"WS_BORDER")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_CLIENTEDGE")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_DLGMODALFRAME")
                     clExStyle = TextAddWindowStyle(clExStyle ,"WS_EX_STATICEDGE")
                  Case 3 '凹边框
                     clStyle   = TextRemoveWindowStyle(clStyle ,"WS_BORDER")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_STATICEDGE")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_DLGMODALFRAME")
                     clExStyle = TextAddWindowStyle(clExStyle ,"WS_EX_CLIENTEDGE")
                  Case 4 ' 凸边框
                     clStyle   = TextRemoveWindowStyle(clStyle ,"WS_BORDER")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_CLIENTEDGE")
                     clExStyle = TextRemoveWindowStyle(clExStyle ,"WS_EX_STATICEDGE")
                     clExStyle = TextAddWindowStyle(clExStyle ,"WS_EX_DLGMODALFRAME")
               End Select
            Case "TABH"
               clPro &= "      This." & clName & ".mTabH = " & ValInt(Control.PVALUE(ii)) & vbCrLf
            Case "ADDBUTTON"
               clPro &= "      This." & clName & ".vAddButton = " & Control.PVALUE(ii) & vbCrLf
            Case "DOWNBUTTON"
               clPro &= "      This." & clName & ".vDownButton = " & Control.PVALUE(ii) & vbCrLf
            Case "WORD"
               clPro &= "      This." & clName & ".cWord = GetCodeColorGDI(&H" & Hex(GetColorText(Control.PVALUE(ii)) ,8) & ",GetSysColor(COLOR_BTNTEXT))" & vbCrLf
            Case "BACK"
               clPro &= "      This." & clName & ".cBack = GetCodeColorGDI(&H" & Hex(GetColorText(Control.PVALUE(ii)) ,8) & ",GetSysColor(COLOR_BTNTEXT))" & vbCrLf
            Case "MOVE"
               clPro &= "      This." & clName & ".cMove = GetCodeColorGDI(&H" & Hex(GetColorText(Control.PVALUE(ii)) ,8) & ",GetSysColor(COLOR_BTNTEXT))" & vbCrLf
            Case "SELBACK"
               clPro &= "      This." & clName & ".cSelBack = GetCodeColorGDI(&H" & Hex(GetColorText(Control.PVALUE(ii)) ,8) & ",GetSysColor(COLOR_BTNTEXT))" & vbCrLf
            Case "SELWORD"
               clPro &= "      This." & clName & ".cSelWord = GetCodeColorGDI(&H" & Hex(GetColorText(Control.PVALUE(ii)) ,8) & ",GetSysColor(COLOR_BTNTEXT))" & vbCrLf
            Case "SELSTYLE"
               clPro &= "      This." & clName & ".SelStyle = " & ValInt(Control.PVALUE(ii)) & vbCrLf 
            Case "LEFTINDENT"
               clPro &= "      This." & clName & ".vLeftIndent = " & ValInt(Control.PVALUE(ii)) & vbCrLf 
         End Select
      End If
   Next
   
   Insert_code(ProWinCode ,"'[CALL_CONTROL_DESTROY]" , _  '控件窗口销毁事件，用于真实控件，处理清理工作
      "               Case " & IDC & vbCrLf & _
      "                  " & Form_clName & "." & clName & ".HWnd= hWndControl" & vbCrLf & _
      "                  " & Form_clName & "." & clName & ".UnLoadControl() " & vbCrLf)

   
   Dim CONTROL_CODExx As String
   If Len(clExStyle) = 0 Then clExStyle = "0"
   If Len(clStyle) = 0   Then clStyle   = "0"
   '真实控件========
   CONTROL_CODExx &= "   hWndControl = pWindow->AddControl(""" & clClName & """, hWnd, " & IDC & ", """ & YF_Replace(Control.Caption ,Chr(34) ,Chr(34 ,34)) & """, " & _
      Control.nLeft          & ", "        & Control.nTop       & ", " & Control.nWidth & ", " & Control.nHeight & "," & YF_Replace(clStyle ,"," ," Or ") & " ," & YF_Replace(clExStyle ,"," ," Or ") & _
      " , , Cast(Any Ptr, @" & Form_clName & "_CODEPROCEDURE))" & vbCrLf
   CONTROL_CODExx &= "   If hWndControl Then " & vbCrLf
   CONTROL_CODExx &= "      Dim fp As FormControlsPro_TYPE ptr = new FormControlsPro_TYPE" & vbCrLf
   CONTROL_CODExx &= "      vfb_Set_Control_Ptr(hWndControl,fp)" & vbCrLf
   CONTROL_CODExx &= "      fp->hWndParent = hWnd"               & vbCrLf
   CONTROL_CODExx &= "      fp->Index = "                        & Control.Index & vbCrLf
   CONTROL_CODExx &= "      fp->IDC = "                          & IDC           & vbCrLf
   CONTROL_CODExx &= "      fp->nText = """                      & YF_Replace(Control.Caption ,Chr(34) ,Chr(34 ,34)) & """" & vbCrLf
   '   CONTROL_CODExx &= "      fp->ControlType = " & clType & vbCrLf
   CONTROL_CODExx &= "      This." & clName & ".hWnd = hWndControl " & vbCrLf '真实控件========
   CONTROL_CODExx &= "      This." & clName & ".IDC ="               & IDC & vbCrLf
   
   CONTROL_CODExx &= clPro
   CONTROL_CODExx &= "   End IF" & vbCrLf
   Insert_code(ProWinCode ,"'[Create control]" ,CONTROL_CODExx)
   
   '事件处理 ------------------------------
   '增加 控件内事件处理
   Dim CALL_CONTROL_CUSTOM As String = "    If IDC = " & IDC & " Then  ' " & clName & vbCrLf
   CALL_CONTROL_CUSTOM &= "       " & Form_clName & "." & clName
   CALL_CONTROL_CUSTOM &= ".hWnd=hWndControl"  & vbCrLf
   CALL_CONTROL_CUSTOM &= "       tLResult = " & Form_clName & "." & clName
   CALL_CONTROL_CUSTOM &= ".MsgProcedure(hWndControl,wMsg, wParam, lParam)" & vbCrLf
   CALL_CONTROL_CUSTOM &= "       If tLResult Then Return tLResult"         & vbCrLf
   CALL_CONTROL_CUSTOM &= "    End If" & vbCrLf
   Insert_code(ProWinCode ,"'[CALL_CONTROL_CUSTOM]" ,CALL_CONTROL_CUSTOM)
   
   Dim LeaveHoverI As Long
   '控件事件
   For ii = 1 To ColTool.elU
      Dim sim As String '事件函数名组合
      sim = " " & UCase(Form_clName & "_" & StrToUtf8(Control.nName & "_" & ColTool.EveList(ii).sName)) & "("
      Dim ff As Long
      for fi As Long = 0 To UBound(ussl)
         If left(ussl(fi) ,1) <> "'" AndAlso InStr(ussl(fi) ,sim) > 0 Then
            ff = fi + 1
            Exit for
         End If
      Next
      If ff > 0 Then
         if IsEventComparison(Control ,ColTool ,ii ,ff ,nFile ,ussl(ff -1) ,Form_clName) Then Return 3 '检查事件是不是正确
         Select Case ColTool.EveList(ii).tMsg
            Case "STN_CLICKED" ,"STN_DBLCLK" ,"STN_DISABLE" ,"STN_ENABLE"
               ProWinCode = YF_Replace(ProWinCode ,"'{CONTROL_WM_COMMAND}" ,"         Dim As Long IDC =LoWord(wParam) ,CODE = HiWord(wParam)")
               Dim CONTROL_WM_COMMAND As String = "          If IDC = " & IDC & " And CODE = " & ColTool.EveList(ii).tMsg & " Then  ' " & clName & vbCrLf
               If Right(ColTool.EveList(ii).Param ,1) = ")" Then '是SUB
                  CONTROL_WM_COMMAND &= "             " & sim
               Else
                  CONTROL_WM_COMMAND &= "              tLResult = " & sim
               end if
               If Control.Index > -1 Then CONTROL_WM_COMMAND &= Control.Index & ","
               CONTROL_WM_COMMAND &= ColTool.EveList(ii).gCall & "  " & nFile & ff -1 & "]" & vbCrLf
               If Right(ColTool.EveList(ii).Param ,1) <> ")" Then CONTROL_WM_COMMAND &= "            If tLResult Then Return tLResult" & vbCrLf
               CONTROL_WM_COMMAND &= "           End If"
               Insert_code(ProWinCode ,"'[CONTROL_WM_COMMAND]" ,CONTROL_WM_COMMAND)
               
            Case "CUSTOM"
               dim CALL_CONTROL_CUSTOM As String = "    If IDC = " & IDC & " Then  ' " & clName & vbCrLf
               CALL_CONTROL_CUSTOM &= "       tLResult = " & sim
               If Control.Index > -1 Then CALL_CONTROL_CUSTOM &= Control.Index & ","
               CALL_CONTROL_CUSTOM &= ColTool.EveList(ii).gCall                 & "  " & nFile & ff -1 & "]" & vbCrLf
               CALL_CONTROL_CUSTOM &= "       If tLResult Then Return tLResult" & vbCrLf
               CALL_CONTROL_CUSTOM &= "    End If" & vbCrLf
               Insert_code(ProWinCode ,"'[CALL_CONTROL_CUSTOM]" ,CALL_CONTROL_CUSTOM)
               
            Case Else
               If ColTool.EveList(ii).tMsg = "WM_MOUSEHOVER" Then LeaveHoverI Or= 1
               If ColTool.EveList(ii).tMsg = "WM_MOUSELEAVE" Then LeaveHoverI Or= 10
               Dim ca    As String = "      Case "         & ColTool.EveList(ii).tMsg & " ''' "
               Dim other As String = "          If IDC = " & IDC                      & " Then  ' " & clName & vbCrLf
               If Right(ColTool.EveList(ii).Param ,1) <> ")" Then '这是函数
                  other &= "          tLResult = " & sim
                  If Control.Index > -1 Then other &= Control.Index & ","
                  other &= ColTool.EveList(ii).gCall                    & "  " & nFile & ff -1 & "]" & vbCrLf
                  other &= "          If tLResult Then Return tLResult" & vbCrLf
               Else '这是过程
                  other &= "             " & sim
                  If Control.Index > -1 Then other &= Control.Index & ","
                  other &= ColTool.EveList(ii).gCall & "  " & nFile & ff -1 & "]" & vbCrLf
               End If
               other &= "          End If" & vbCrLf
               ff    = InStr(ProWinCode ,ca)
               If ff = 0 Then '不存在
                  Insert_code(ProWinCode ,"'[CONTROL_CASE_OTHER]" ,ca & vbCrLf & other)
               Else '已经有了
                  ProWinCode = Left(ProWinCode ,ff + Len(ca) -1) & vbCrLf & other & Mid(ProWinCode ,ff + Len(ca))
               End If
         End Select
      End If
   Next
   
   If LeaveHoverI > 0 Then
      dim CONTROL_LEAVEHOVER As String = "          If wMsg = WM_MouseMove AndAlso IDC = " & IDC & " Then  ' " & clName & vbCrLf
      CONTROL_LEAVEHOVER &= "             Dim entTrack As tagTRACKMOUSEEVENT"           & vbCrLf
      CONTROL_LEAVEHOVER &= "             entTrack.cbSize = SizeOf(tagTRACKMOUSEEVENT)" & vbCrLf
      If LeaveHoverI = 11 Then
         CONTROL_LEAVEHOVER &= "             entTrack.dwFlags = TME_LEAVE Or TME_HOVER" & vbCrLf
      ElseIf LeaveHoverI = 10 Then
         CONTROL_LEAVEHOVER &= "             entTrack.dwFlags = TME_LEAVE " & vbCrLf
      Else
         CONTROL_LEAVEHOVER &= "             entTrack.dwFlags =  TME_HOVER" & vbCrLf
      End If
      CONTROL_LEAVEHOVER &= "             entTrack.hwndTrack = hWndControl"     & vbCrLf
      CONTROL_LEAVEHOVER &= "             entTrack.dwHoverTime = HOVER_DEFAULT" & vbCrLf
      CONTROL_LEAVEHOVER &= "             TrackMouseEvent @entTrack"            & vbCrLf
      CONTROL_LEAVEHOVER &= "          End IF"                                  & vbCrLf
      Insert_code(ProWinCode ,"'[CONTROL_LEAVEHOVER]" ,CONTROL_LEAVEHOVER)
   End If
   
   '成功返回0，失败非0
   Function = 0
End Function


'Function GetCodeColorGDI(coColor As Long) As Long  '把控件特殊颜色值，转换为 GDI 色  ,返回-1 为不使用或默认
'  If (&H00FFFFFF And coColor) = &H7F7F7F Then
'      Dim f As Long = Cast(UInteger, (&HFF000000 And coColor)) Shr 24
'      If f=25 Then  Return -1   '不使用或默认值 
'      If f < 31 Then 
'          Return GetSysColor(f)  
'      End If  
'  End If
'  Function = (&H00FFFFFF And coColor) '去掉 A 通道
'End Function
'Function GetCodeColorGDIplue(coColor As Long) As Long  '把控件特殊颜色值，转换为 GDI+ 色  ,返回0 为不使用或默认
'  Dim tColor As Long = coColor 
'  If (&H00FFFFFF And coColor) = &H7F7F7F Then
'      Dim f As Long = Cast(UInteger, (&HFF000000 And coColor)) Shr 24
'      If f = 25 Then Return 0  ' 不使用或默认值 
'      If f < 31 Then 
'          tColor = GetSysColor(f) Or &HFF000000 '增加 A通道，不透明，不然是全透明  
'      End If  
'  End If 
'  '因为保存的是GDI 的颜色，GDI+ 需要调换
'  Dim As UInteger c1 =(&H00FF0000 And tColor),c2 = (&H000000FF And tColor) ,c3 =(&HFF00FF00 And tColor)
'  c1 Shr= 16
'  c2 Shl= 16 
'  Function = c1 Or c2 Or c3  
'End Function
























