Type Class_Label Extends Class_Virtual '属于虚拟控件，是直接画在窗口上的 ，所有私有属性记录，保存在主窗口中，
   
   Declare Property Caption() As CWSTR                 '返回/设置控件中的文本
   Declare Property Caption(sText As CWSTR)
   Declare Property TextAlign() As Long      '返回/设置控件上显示的文本的对齐方式。{=.0 - 左对齐.1 - 居中.2 - 右对齐.3 - 中左对齐.4 - 置中.5 - 中右对齐.6 - 下左对齐.7 - 下居中.8 - 下右对齐}
   Declare Property TextAlign(bValue As Long)
   Declare Property Style() As Long      '返回/设置指示控件边界的外观和行为。{=.0 - 无边框.1 - 细边框}
   Declare Property Style(bValue As Long)
   Declare Property ForeColor() As Long       '返回/设置前景色：设置用：BGR(r, g, b)  
   Declare Property ForeColor(nColor As Long)
   Declare Property BackColor() As Long      '返回/设置背景色：设置用：BGR(r, g, b)  
   Declare Property BackColor(nColor As Long)
   Declare Property ForeColorGDIplue() As ARGB       '返回/设置前景色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a) 
   Declare Property ForeColorGDIplue(GDIplue_Color As ARGB)
   Declare Property BackColorGDIplue() As ARGB      '返回/设置背景色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a) 
   Declare Property BackColorGDIplue(GDIplue_Color As ARGB)
   Declare Property Font() As String          '返回/设置用于在控件中绘制文本的字体，格式为：字体,字号,加粗,斜体,下划线,删除线  中间用英文豆号分割，可以省略参数 默认为：宋体,9,0,0,0,0  自动响应系统DPI创建字体大小。
   Declare Property Font(nFont As String)     '
   Declare Property Prefix() As Boolean                 '处理前缀字符，通常将助记符前缀＆解释为下划线{=.True.False}
   Declare Property Prefix(ByVal bValue As Boolean)
   Declare Property Ellipsis() As Boolean                 '截断所有不适合大小的单词并添加省略号。{=.True.False}
   Declare Property Ellipsis(ByVal bValue As Boolean)
   Declare Sub Drawing(gg As yGDI ,hWndForm As hWnd ,nBackColor As Long) '描绘控件，窗口需要描绘，通知类把控件画到 gg 中。（** VFB系统自用）

End Type

Property Class_Label.Caption() As CWSTR
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      Return fp->nText
   End If
End Property
Property Class_Label.Caption(sText As CWSTR)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->nText = sText
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Label.TextAlign() As Long ' "LABEL"  样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      Return (fp->Style And &H00FF0000) Shr 16
   End If
End Property
Property Class_Label.TextAlign(bValue As Long )
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy>8 then sy=8
      fp->Style = (fp->Style And &HFF00FFFF) Or (sy Shl 16)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Label.Style() As Long
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then  ' "LABEL"  样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
      Return  ((fp->Style And &HFF000000) Shr 24)  
   End If
End Property
Property Class_Label.Style(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy>255 then sy=255
      fp->Style = (fp->Style And &H00FFFFFF) Or (sy Shl 24)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Label.ForeColor() As Long         'GDI 前景色返回
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->ForeColor)
      If cc = -1 Then cc = GetSysColor(COLOR_BTNTEXT)    
      Return cc
   End If
End Property
Property Class_Label.ForeColor(ByVal nColor As Long)   'GDI 前景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->ForeColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明 
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Label.BackColor() As Long          'GDI 背景色返回
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->BackColor)
      If cc = -1 Then cc = GetSysColor(COLOR_BTNFACE)      
      Return cc
   End If
End Property
Property Class_Label.BackColor(ByVal nColor As Long)    'GDI 背景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->BackColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明 
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Label.ForeColorGDIplue() As ARGB   'GDI+ 前景色返回
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->ForeColor)
      if cc = 0 then
         cc = GetSysColor(COLOR_BTNTEXT)
         Dim b As UByte Ptr = Cast(Any Ptr, @cc ) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0], b[1], b[2], &HFF)  
      End if
      Return cc
   End If
End Property
Property Class_Label.ForeColorGDIplue(ByVal nColor As ARGB) 'GDI+ 前景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr, @nColor )
      Dim c As Long = BGRA(b[2], b[1], b[0], b[3])  'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->ForeColor = c
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Label.BackColorGDIplue() As ARGB     'GDI+ 背景色返回
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->BackColor)
      if cc = 0 then
         cc = GetSysColor(COLOR_BTNFACE)
         Dim b As UByte Ptr = Cast(Any Ptr, @cc ) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0], b[1], b[2], &HFF)  
      End if
      Return cc
   End If
End Property
Property Class_Label.BackColorGDIplue(ByVal nColor As ARGB)   'GDI+ 背景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr, @nColor )
      Dim c As Long = BGRA(b[2], b[1], b[0], b[3])  'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->BackColor = c
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Label.Font() As String
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then   
      Return fp->nFont  
   End If
End Property
Property Class_Label.Font(nFont As String)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->nFont = nFont
      AfxRedrawWindow m_hWndParent
   end if
End Property
Property Class_Label.Prefix() As Boolean 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then    ' "LABEL"  样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
      Return (fp->Style and &H4 )<>0
   End If
End Property
Property Class_Label.Prefix(ByVal bValue As Boolean)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      If bValue Then fp->Style Or= &H4 Else fp->Style And= Not &H4 
      AfxRedrawWindow m_hWndParent
   end if
End Property
Property Class_Label.Ellipsis() As Boolean 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then   
      Return (fp->Style and &H8 )<>0
   End If
End Property
Property Class_Label.Ellipsis(ByVal bValue As Boolean)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      If bValue Then fp->Style Or= &H8 Else fp->Style And= Not &H8
      AfxRedrawWindow m_hWndParent
   end if
End Property
Sub Class_Label.Drawing(gg As yGDI, hWndForm As hWnd, nBackColor As Long)
   m_hWndParent = hWndForm
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp = 0 then return
   '样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
   if (fp->Style and &H2) = 0 Then Return
   dim cc As Long = GetCodeColorGDIplue(fp->BackColor)
   gg.gpBrush cc
   
   cc = GetCodeColorGDIplue(fp->ForeColor)
   If ((fp->Style And &HFF000000) Shr 24) = 1 Then
      gg.GpPen 1, cc
   Else
      gg.GpPen 0
   End If
   gg.gpDrawFrame fp->nLeft, fp->nTop, fp->nWidth, fp->nHeight
   
   cc = GetCodeColorGDI(fp->ForeColor)
   If cc <> -1 Then gg.SetColor cc
   
   'Windows使用带有dt_wordbreak和dt_expandtabs参数的drawtext绘制带有ss_left、cc_center和ss_right样式的标签。其他样式必须使用dt_单线，而不是换行。
   Dim As Long lWrapMode = DT_SINGLELINE
   Dim As Long lFormat
   
   Select Case (fp->Style And &H00FF0000) Shr 16
      Case 7 '下居中
         lFormat = DT_CENTER Or DT_BOTTOM
      Case 6 '下左对齐
         lFormat = DT_LEFT Or DT_BOTTOM
      Case 8 ' 下右对齐
         lFormat = DT_RIGHT Or DT_BOTTOM
      Case 4 '置中
         lFormat = DT_CENTER Or DT_VCENTER
      Case 3 '中左对齐
         lFormat = DT_LEFT Or DT_VCENTER
      Case 5 '中右对齐
         lFormat = DT_RIGHT Or DT_VCENTER
      Case 1 '居中
         lFormat = DT_CENTER Or DT_TOP
         lWrapMode = DT_WORDBREAK
      Case 0 ' 左对齐
         lFormat = DT_LEFT Or DT_TOP
         lWrapMode = DT_WORDBREAK
      Case 2 '右对齐
         lFormat = DT_RIGHT Or DT_TOP
         lWrapMode = DT_WORDBREAK
   End Select
   If (fp->Style And &H4) = 0 Then lFormat Or= DT_NOPREFIX
   If (fp->Style And &H8) Then lFormat Or= DT_WORD_ELLIPSIS
   lFormat = lFormat Or lWrapMode Or DT_EXPANDTABS
   Dim oFont As HGDIOBJ = SelectObject(gg.m_Dc, gFLY_GetFontHandles(fp->nFont))
   gg.DrawTexts fp->nLeft, fp->nTop, fp->nWidth, fp->nHeight, fp->nText, lFormat
   SelectObject(gg.m_Dc, oFont)
End Sub



