'控件类
#include Once "..\mCtrlPublic\grid.bi"
Type Class_mCtrlGrid Extends Class_Control
   Declare Property Table() As MC_HTABLE         '返回/设置 关联的 mcTable表 句柄
   Declare Property Table(mt As MC_HTABLE)
   Declare Function GetColumnCount() As Long  '获取附加到控件的表中的列数。
   Declare Function GetRowCount() As Long  '获取连接到控件的表中的行数。
   Declare Function ReSize(Column As Long, Row As Long) As Long  '调整附加到控件的表的大小。（BOOL）TRUE成功，FALSE失败。
   Declare Sub TableClear(v As Long)   '清除单元格 v=0 全部|v=1 单元格|v=2 列标题|v=4 行单元格 或任意组合
   Declare Function SetCell(Column As Long, Row As Long, zText As CWSTR, DataValue as Integer = 0, AlignH As Long = 0, AlignV As Long = 0) As Long  '设置表格单元格内容。列或行为 MC_TABLE_HEADER 时设置标题（BOOL）TRUE成功，FALSE失败。   {5.MC_TCF_ALIGNDEFAULT 默认值.MC_TCF_ALIGNLEFT 左对齐.MC_TCF_ALIGNCENTER 水平居中.MC_TCF_ALIGNRIGHT 右对齐}   {6.MC_TCF_ALIGNVDEFAULT 默认值.MC_TCF_ALIGNTOP 对齐到顶部.MC_TCF_ALIGNVCENTER 垂直居中.MC_TCF_ALIGNBOTTOM 右对齐}
   Declare Function GetCellText(Column As Long, Row As Long) As CWSTR  '获取表格单元格文字
   Declare Function GetCellData(Column As Long, Row As Long) As Integer  '获取表格单元格用户数据
   Declare Function GetCellAlign(Column As Long, Row As Long) As ULong  '获取表格单元格对齐标记
   Declare Function SetGeometry(ColumnHeaderHeight As Long, RowHeaderWidth As Long, DefColumnWidth As Long, DefRowHeight As Long, PaddingHorz As Long, PaddingVert As Long) As Long '设置网格的几何形状。（BOOL）TRUE成功，FALSE失败。参数：列标题单元格的高度,行标题单元格的宽度,常规内容单元格的默认宽度,常规内容单元格的默认高度,单元格中的水平填充,单元格中的垂直填充
   Declare Function GetGeometry(gg As MC_GGEOMETRY) As Long   '获取网格的几何形状。（BOOL）TRUE成功，FALSE失败
   Declare Function RedrawCells(Left_Column As Long, Top_Row As Long, Right_Column As Long, Bottom_Row As Long) As Long   '重绘单元格区域，。（BOOL）TRUE成功，FALSE失败
   Declare Function SetColumnWidth(Column As Long, w as Long) As Long '设置指定列的宽度。（以像素为单位）。（BOOL）TRUE成功，FALSE失败
   Declare Function GetColumnWidth(Column As Long) As Long '获取指定列的宽度。（以像素为单位）。失败，则返回值为-1
   Declare Function SetRowHeight(Row As Long, h as Long) As Long '设置指定行的高度。（以像素为单位）。（BOOL）TRUE成功，FALSE失败
   Declare Function GetRowHeight(Row As Long) As Long '获取指定行的高度。（以像素为单位）。失败，则返回值为-1
   Declare Function HitTest(x As Long, y As Long, flags As UInteger = 0) As POINT '命中测试，返回 单元格的列和行，=-1 为失败
   Declare Function GetCellRect(Column As Long, Row As Long) As RECT '获取单元格矩形。
   Declare Function EnsureVisible(Column As Long, Row As Long) As Long '确保该单元格可见。如果不可见，则控件滚动以使该单元可见。（BOOL）TRUE成功，FALSE否则。
   Declare Function SetFocusedCell(Column As Long, Row As Long) As Long '设置单元格焦点（BOOL）TRUE成功，FALSE否则。
   Declare Sub GetFocusedCell(ByRef Column As Long, ByRef Row As Long)  '获取单元格焦点  （BOOL）TRUE成功，FALSE否则。
   Declare Function SetSelection(g() As MC_GRECT) As Long '设置选择。（BOOL）TRUE成功，FALSE否则。
   Declare Function GetSelection(g() As MC_GRECT) As Long  '获取选择。返回选择矩形数量
   Declare Function GetEditControl() As .hWnd  '获取是不是在编辑中。成功返回 编辑控件窗口句柄，失败为 FALSE 。
   Declare Function EditLabel(Column As Long, Row As Long) As .hWnd '开始编辑指定单元格。成功返回 编辑控件窗口句柄，失败为 FALSE 。
   Declare Sub EditLabelCancel()  '结束单元格编辑操作（如果正在进行）。如果正在进行任何编辑操作，则该消息将导致发送通知MC_GN_ENDLABELEDIT。
   
   
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_mCtrlGrid.Table() As MC_HTABLE         '返回/设置 关联的图标句柄，必须具有SS_ICON样式控件才有效。
  Return Cast(MC_HTABLE, SendMessage(hWndControl, MC_GM_GETTABLE, 0, 0)) 
End Property
Property Class_mCtrlGrid.Table(mt As MC_HTABLE)
  PostMessage(hWndControl, MC_GM_SETTABLE,0, Cast(wParam, mt))
End Property
Function Class_mCtrlGrid.GetColumnCount() As Long 
  Function = SendMessage( hWndControl , MC_GM_GETCOLUMNCOUNT , 0 , 0 ) 
End Function
Function Class_mCtrlGrid.GetRowCount() As Long 
   Function = SendMessage(hWndControl, MC_GM_GETROWCOUNT, 0, 0) 
End Function 
Function Class_mCtrlGrid.ReSize(Column As Long ,Row As Long ) As Long
   Function = SendMessage( hWndControl , MC_GM_RESIZE ,MAKEWPARAM(Column,Row) , 0 ) 
End Function 
Sub Class_mCtrlGrid.TableClear(v As Long)
   SendMessage( hWndControl , MC_GM_CLEAR ,v , 0 ) 
End Sub 
Function Class_mCtrlGrid.SetCell(Column As Long, Row As Long, zText As CWSTR, DataValue as Integer = 0, AlignH As Long = 0, AlignV As Long = 0) As Long
   Dim t As MC_TABLECELLW
   t.fMask = MC_TCMF_TEXT Or MC_TCMF_PARAM Or MC_TCMF_FLAGS
   t.pszText = zText.vptr 
   t.cchTextMax = Len(zText)
   t.lParam = DataValue 
   t.dwFlags = AlignH Or AlignV
   Function = SendMessage(hWndControl, MC_GM_SETCELLW, MAKEWPARAM(Column, Row),  Cast(lParam,@t))
End Function
Function Class_mCtrlGrid.GetCellText(Column As Long, Row As Long) As CWSTR
   if Column+1> This.GetColumnCount Or Row+1 > This.GetRowCount Then Return ""
   Dim t As MC_TABLECELLW
   t.fMask = MC_TCMF_TEXT 
   Static tt As WString * 261
   memset(@tt ,0, 261*2)
   t.pszText = @tt
   t.cchTextMax = 260
   if SendMessage(hWndControl, MC_GM_GETCELLW, MAKEWPARAM(Column, Row), Cast(lParam,@t)) Then 
       Return tt
   End if 
End Function
Function Class_mCtrlGrid.GetCellData(Column As Long, Row As Long) As Integer
   if Column+1> This.GetColumnCount Or Row+1 > This.GetRowCount Then Return 0
   Dim t As MC_TABLECELLW
   t.fMask = MC_TCMF_PARAM 
   if SendMessage(hWndControl, MC_GM_GETCELLW, MAKEWPARAM(Column, Row), Cast(lParam,@t)) Then 
       Return t.lParam
   End if 
End Function
Function Class_mCtrlGrid.GetCellAlign(Column As Long, Row As Long) As ULong 
   if Column+1> This.GetColumnCount Or Row+1 > This.GetRowCount Then Return 0
   Dim t As MC_TABLECELLW
   t.fMask =  MC_TCMF_FLAGS
   if SendMessage(hWndControl, MC_GM_GETCELLW, MAKEWPARAM(Column, Row), Cast(lParam,@t)) Then 
       Return t.dwFlags
   End if 
End Function

Function Class_mCtrlGrid.SetGeometry(ColumnHeaderHeight As Long, RowHeaderWidth As Long, DefColumnWidth As Long, DefRowHeight As Long, PaddingHorz As Long, PaddingVert As Long) As Long
   Dim gg As MC_GGEOMETRY
   gg.fMask = MC_GGF_COLUMNHEADERHEIGHT Or MC_GGF_ROWHEADERWIDTH Or MC_GGF_DEFCOLUMNWIDTH Or MC_GGF_DEFROWHEIGHT Or MC_GGF_PADDINGHORZ Or MC_GGF_PADDINGVERT
   gg.wColumnHeaderHeight = ColumnHeaderHeight
   gg.wRowHeaderWidth = RowHeaderWidth
   gg.wDefColumnWidth = DefColumnWidth
   gg.wDefRowHeight = DefRowHeight
   gg.wPaddingHorz = PaddingHorz
   gg.wPaddingVert = PaddingVert
   Function = SendMessage(hWndControl, MC_GM_SETGEOMETRY, 0, Cast(lParam,@gg)) 
End Function
Function Class_mCtrlGrid.GetGeometry(gg As MC_GGEOMETRY) As Long 
   gg.fMask = MC_GGF_COLUMNHEADERHEIGHT Or MC_GGF_ROWHEADERWIDTH Or MC_GGF_DEFCOLUMNWIDTH Or MC_GGF_DEFROWHEIGHT Or MC_GGF_PADDINGHORZ Or MC_GGF_PADDINGVERT
   Function = SendMessage(hWndControl, MC_GM_GETGEOMETRY, 0, Cast(lParam,@gg)) 
End Function
Function Class_mCtrlGrid.RedrawCells(Left_Column As Long ,Top_Row As Long,Right_Column As Long ,Bottom_Row As Long) As Long
   Function = SendMessage(hWndControl, MC_GM_REDRAWCELLS, MAKEWPARAM(Left_Column,Top_Row),MAKELPARAM(Right_Column,Bottom_Row)) 
End Function
Function Class_mCtrlGrid.SetColumnWidth(Column As Long ,w as Long) As Long
   Function = SendMessage(hWndControl, MC_GM_SETCOLUMNWIDTH,Column,w) 
End Function
Function Class_mCtrlGrid.GetColumnWidth(Column As Long) As Long
   Function = GET_X_LPARAM ( SendMessage(hWndControl, MC_GM_GETCOLUMNWIDTH, Column, 0) )
End Function
Function Class_mCtrlGrid.SetRowHeight(Row As Long,h as Long) As Long
   Function = SendMessage(hWndControl, MC_GM_SETROWHEIGHT,Row,h) 
End Function
Function Class_mCtrlGrid.GetRowHeight(Row As Long) As Long
   Function = GET_X_LPARAM ( SendMessage(hWndControl, MC_GM_GETROWHEIGHT, Row, 0) )
End Function

Function Class_mCtrlGrid.HitTest(x As Long, y As Long, flags As UInteger = 0) As POINT 
   Dim tt As MC_GHITTESTINFO   
   tt.flags = flags 
   tt.pt.x = x
   tt.pt.y = y 
   Dim rr As LRESULT  = SendMessage(hWndControl, MC_GM_HITTEST, 0, Cast(lParam, @tt)) 
   Dim rt As POINT 
   if rr = -1 Then 
      rt.x = -1
      rt.y = -1
   Else
      rt.x = GET_X_LPARAM(rr)
      rt.y = GET_Y_LPARAM(rr)
   End if   
   Return rt
End Function
Function Class_mCtrlGrid.GetCellRect(Column As Long, Row As Long) As RECT
   Dim r As RECT 
   SendMessage(hWndControl, MC_GM_GETCELLRECT, MAKEWPARAM(Column, Row), Cast(lParam, @r))  
   Return r 
End Function
Function Class_mCtrlGrid.EnsureVisible(Column As Long ,Row As Long) As Long

  Function = SendMessage(hWndControl, MC_GM_ENSUREVISIBLE, MAKEWPARAM(Column, Row), 0)  

End Function
Function Class_mCtrlGrid.SetFocusedCell(Column As Long ,Row As Long) As Long

  Function = SendMessage(hWndControl, MC_GM_SETFOCUSEDCELL, MAKEWPARAM(Column, Row), 0)  

End Function

Sub Class_mCtrlGrid.GetFocusedCell(ByRef Column As Long ,ByRef Row As Long)

  Dim rr As  LRESULT  = SendMessage(hWndControl, MC_GM_GETFOCUSEDCELL, 0, 0)  
  Column = GET_X_LPARAM(rr)
  Row =GET_Y_LPARAM(rr)
End Sub

Function Class_mCtrlGrid.SetSelection(g() As MC_GRECT) As Long
   Dim mg As MC_GSELECTION
   mg.uDataCount = UBound(g) - LBound(g) + 1
   if mg.uDataCount < 1 Then Return 0
   mg.rcData = @g(LBound(g))
   Function = SendMessage(hWndControl, MC_GM_SETSELECTION, 0, Cast(lParam, @mg))
   
End Function

Function Class_mCtrlGrid.GetSelection(g() As MC_GRECT) As Long
   Dim u As Long = SendMessage(hWndControl, MC_GM_GETSELECTION, 0, 0) 
   if u = 0 Then Return 0
   ReDim g(u -1)
   Dim mg As MC_GSELECTION
   mg.uDataCount = u
   mg.rcData =@g(0)
  Function = SendMessage(hWndControl, MC_GM_GETSELECTION, 0, Cast(lParam, @mg))  

End Function

Function Class_mCtrlGrid.GetEditControl() As .hWnd

  Function = Cast(.hWnd, SendMessage(hWndControl, MC_GM_GETEDITCONTROL, 0,0)  )

End Function

Function Class_mCtrlGrid.EditLabel(Column As Long ,Row As Long) As .hWnd 

  Function = Cast(.hWnd, SendMessage(hWndControl, MC_GM_EDITLABEL, MAKEWPARAM(Column, Row),0)  )

End Function

Sub  Class_mCtrlGrid.EditLabelCancel()

  SendMessage(hWndControl, MC_GM_CANCELEDITLABEL, 0,0) 

End Sub 










