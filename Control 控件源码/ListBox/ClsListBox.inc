Type Class_ListBox Extends Class_Control
     
Protected : 
    m_ReDraw As BOOLEAN  = True '修改列表时是否允许刷新
Public :    
    Declare Sub Clear()                                  '删除组合框中所有项目。
    Declare Function AddItem(sText As CWSTR,DataValue As Integer =0 ) As Long  '新增项目，返回新添加的索引（从0开始）,失败返回CB_ERR
    Declare Function InsertItem(nIndex As Long, TheText As CWSTR) As Long  '插入项目，返回新添加的索引（从0开始）,失败返回CB_ERR
    Declare Property List(nIndex As Long)  As CWSTR        '返回/设置项目文本
    Declare Property List(nIndex As Long, sText As CWSTR)
    Declare Function ListCount() As Long  '检索框列表框中的项目数。
    Declare Property ListIndex() As Long                  '返回/设置项目
    Declare Property ListIndex(nIndex As Long)
    Declare Property ItemData(nIndex As Long)  As Integer         '返回/设置指定项的 附加数据 lParam
    Declare Property ItemData(nIndex As Long, nValue As Integer)
    Declare Property ReDraw()  As BOOLEAN         '设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
    Declare Property ReDraw(nValue As Boolean)    '修改项目时是否允许刷新，（注意：多开窗口中的控件，多窗口用同1个控件类，返回此值可能不正确）
    Declare Property ItemHeight() As Long  '返回/设置每行的高度，单位像素，如：AfxScaleY(15)，可变行高自绘控件时无效
    Declare Property ItemHeight(nValue As Long) 
    Declare Property TopIndex() As Long  '返回/设置 列表框中第一个可见项目(从零开始的索引)。
    Declare Property TopIndex(nIndex As Long) 
    Declare Property Selected(nIndex As Long)  As Boolean         '返回/设置 多选模式下项目是否被选择。{=.True.False}
    Declare Property Selected(nIndex As Long,nValue As Boolean)    '
    Declare Function SelCount() As Long  '获取多选模式下选定项目的总数。
    
    Declare Function Text() As CWSTR  '返回当前选择的文本，无选择为空文本。
    Declare Function RemoveItem(nIndex As Long) As Long  '删除一个项目，返回剩余项目数
    Declare Function FindString(indexStart As Long, lpszFind As CWSTR) As Long  '搜索以指定字符串中的字符开头的项目。如果搜索不成功，则为CB_ERR。
    Declare Function FindStringExact(indexStart As Long, lpszFind As CWSTR) As Long  '查找包含指定字符串在组合框中的第一个项目。。如果搜索不成功，则为CB_ERR。
    Declare Function FindItemData(indexStart As Long, nData As Integer) As Long  '查找包含指定项目数据在组合框中的第一个项目。。如果搜索不成功，则为CB_ERR。
End Type


Sub Class_ListBox.Clear() '删除组合框中所有项目。
  SendMessage hWndControl, LB_RESETCONTENT, 0, 0
End Sub
Function Class_ListBox.AddItem(sText As CWSTR , DataValue As Integer = 0) As Long  '新增项目，返回新添加的索引（从0开始）
   dim nIndex as long = SendMessageW(hWndControl, LB_ADDSTRING, 0, Cast(lParam, sText.vptr ))
   If DataValue Then SendMessage(hWndControl, LB_SETITEMDATA, nIndex, DataValue)
   Function = nIndex
End Function
Function Class_ListBox.InsertItem(nIndex As Long, TheText As CWStr) As Long  '插入项目，返回新添加的索引（从0开始）
  Function = SendMessageW( hWndControl, LB_INSERTSTRING, nIndex, Cast(lParam, TheText.vptr))
End Function
Property Class_ListBox.List(nIndex As Long) As CWStr        '返回/设置项目文本
   Dim nBufferSize As Long
   Dim nBuffer As String
   '检查窗口句柄是否有效
   If IsWindow(hWndControl) Then
      '得到的文本的长度
      nBufferSize = SendMessageW(hWndControl, LB_GETTEXTLEN, nIndex, 0)
      If (nBufferSize = 0) Or (nBufferSize = LB_ERR) Then
         Return ""
      End If
      nBufferSize = nBufferSize + 1
      nBuffer = String(nBufferSize * 2, 0)
      If SendMessageW(hWndControl, LB_GETTEXT, nIndex, Cast(lParam, StrPtr(nBuffer))) = LB_ERR Then
         Return ""
      Else
         Return *CPtr(WString Ptr, StrPtr(nBuffer))
      End If
   Else
      Return ""
   End If
End Property
Property Class_ListBox.List(nIndex As Long, sText As CWStr)
   If IsWindow(hWndControl) Then
      Dim i As Long = SendMessage(hWndControl, LB_GETCURSEL, 0, 0)
      Dim nValue As Integer = SendMessage( hWndControl, LB_GETITEMDATA, nIndex, 0)
      SendMessage(hWndControl, LB_DELETESTRING, nIndex, 0)
      SendMessageW(hWndControl, LB_INSERTSTRING, nIndex, Cast(lParam, sText.vptr))
      SendMessage( hWndControl, LB_SETITEMDATA, nIndex, nValue)
      If i<>-1 Then SendMessage(hWndControl, LB_SETCURSEL, i, 0)
   End If
End Property
Function Class_ListBox.ListCount() As Long  '检索组合框列表框中的项目数。
  Function = SendMessage( hWndControl, LB_GETCOUNT, 0, 0)
End Function
Property Class_ListBox.ListIndex() As Long                  '返回/设置项目
    Property = SendMessage( hWndControl, LB_GETCURSEL, 0, 0)
End Property
Property Class_ListBox.ListIndex(nIndex As Long)
    SendMessage( hWndControl, LB_SETCURSEL, nIndex, 0)
End Property
Property Class_ListBox.ItemData(nIndex As Long)  As Integer         '返回/设置指定项目在组合框中关联的 32 位值
    Property = SendMessage( hWndControl, LB_GETITEMDATA, nIndex, 0)
End Property
Property Class_ListBox.ItemData(nIndex As Long, nValue As Integer)
    SendMessage( hWndControl, LB_SETITEMDATA, nIndex, nValue)
End Property
Property Class_ListBox.TopIndex() As Long  '检索组合框的列表框部分中第一个可见项目的从零开始的索引。
    Return SendMessage( hWndControl, LB_GETTOPINDEX, 0, 0)
End Property
Property Class_ListBox.TopIndex(nIndex As Long)  
     SendMessage( hWndControl, LB_SETTOPINDEX, nIndex, 0)
End Property
Function Class_ListBox.FindString(indexStart As Long, lpszFind As CWStr) As Long  '搜索以指定字符串中的字符开头的项目
  Function = SendMessageW( hWndControl, LB_FINDSTRING, indexStart, Cast(lParam, lpszFind.vptr))
End Function
Function Class_ListBox.FindStringExact(indexStart As Long, lpszFind As CWStr) As Long  '查找包含指定字符串在组合框中的第一个项目。
  Function = SendMessageW( hWndControl, LB_FINDSTRINGEXACT, indexStart, Cast(lParam, lpszFind.vptr))
End Function
Function Class_ListBox.FindItemData(indexStart As Long, nData As Integer) As Long  '查找包含指定项目数据在组合框中的第一个项目。
  Function = SendMessage( hWndControl, LB_FINDSTRING, indexStart, nData)
End Function
Property Class_ListBox.ReDraw()  As Boolean         '返回/设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
    Property = m_ReDraw '无需返回
End Property
Property Class_ListBox.ReDraw(nValue As Boolean)
 m_ReDraw = nValue
    SendMessage( hWndControl, WM_SETREDRAW, nValue, 0)
End Property
Property Class_ListBox.ItemHeight() As Long  '返回/设置每行的高度，单位像素，如：AfxScaleY(15)
    Property = SendMessage( hWndControl, LB_GETITEMHEIGHT, 0, 0)
End Property
Property Class_ListBox.ItemHeight(nValue As Long) 
    SendMessage( hWndControl, LB_SETITEMHEIGHT, 0, nValue)
End Property
Function Class_ListBox.RemoveItem(nIndex As Long) As Long  '删除一个项目，返回剩余项目数
  Function = SendMessage( hWndControl, LB_DELETESTRING, nIndex, 0)
End Function
Function Class_ListBox.Text() As CWSTR  '
   Dim i As Long = This.ListIndex
   If i = -1 Then Return ""
   Return This.List(i)
End Function
Function Class_ListBox.SelCount() As Long  '获取多选模式下选定项目的总数。
  Function = SendMessage( hWndControl, LB_GETSELCOUNT, 0, 0)
End Function
Property Class_ListBox.Selected(nIndex As Long)  As Boolean
   Property = SendMessage(hWndControl ,LB_GETSEL ,Cast(wParam ,nIndex) ,0)
End Property
Property Class_ListBox.Selected(nIndex As Long ,nValue As Boolean)
   SendMessage(hWndControl ,LB_SETSEL ,Cast(WINBOOL ,nValue) ,Cast(lParam ,nIndex))
End Property







