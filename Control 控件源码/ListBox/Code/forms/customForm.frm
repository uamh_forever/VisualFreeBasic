﻿#VisualFreeBasic_Form#  Version=5.5.7
Locked=0

[Form]
Name=customForm
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=自定义数据编辑，一行为1条数据项目。
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=414
Height=226
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=400
MinHeight=200
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Scintilla]
Name=Scintilla1
Explain=
Help=
Index=-1
Style=3 - 凹边框
Enabled=True
Visible=True
Left=6
Top=6
Width=384
Height=137
Layout=5 - 宽度和高度
Tag=
Tab=True

[Button]
Name=Command1
Index=-1
Caption=确定
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=289
Top=151
Width=46
Height=30
Layout=9 - 右边和底部
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=344
Top=151
Width=46
Height=30
Layout=9 - 右边和底部
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[PopupMenu]
Name=PopupMenu1
Index=-1
Menu=撤销PopupMenu1_撤销0-10重做PopupMenu1_重做0-10-PopupMenu1_10-10剪切PopupMenu1_剪切0-10复制PopupMenu1_复制0-10粘贴PopupMenu1_粘贴0-10删除PopupMenu1_删除0-10-PopupMenu1_20-10全选PopupMenu1_全选0-10清空PopupMenu1_清空0-10
Left=94
Top=271
Tag=

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=附加数值行尾加“|1234”，其中"|" 为分割符号。行首为“<Sel>”是默认选择项，区分大小写。
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=7
Top=147
Width=288
Height=37
Layout=8 - 底部和宽度
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]

Dim Shared xiugai As Long  '修改标记，是不是被修改选项
Sub customForm_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。

   sci_Properties Scintilla1.pSci
   Me.UserData(0) = UserData
   Dim As Rect rc,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr,UserData)
   GetWindowRect(st->hWndForm , @rc2)
   GetWindowRect(st->hWndList , @rc)
   if rc.Left + Me.Width > rc2.right Then rc.Left = rc2.right - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top = rc2.bottom - Me.Height
   Me.Move rc.Left, rc.top   
   
   Dim aa As String = *st->value 
   if Len(aa) Then
      aa = YF_Replace(aa, "\|", Chr(0))
      Dim t As Long 
      for i As Long = 0 To Len(aa) -1
         if aa[i] = 124 Then 
            if t = 0 Then 
               t = 1
            Else
               aa[i] = 13
               t = 0
            End if 
         End if   
      Next
      aa = YF_Replace(aa, Chr(13), vbCrLf )
      aa = YF_Replace(aa, "|0" & vbCrLf, vbCrLf)
      aa = YF_Replace(aa, Chr(0), "|")
      if Right(aa,2)="|0" Then aa=Left(aa,Len(aa)-2) 
      Scintilla1.Text = aa 
   end if
   xiugai =0 
End Sub


Sub customForm_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim aa As String = Scintilla1.Text 
   if Len(aa) Then
      Dim cc() As String
      '转换分割符合 |
      Dim u As Long = vbSplit(aa, vbCrLf, cc()), i As Long, f As Long
      for i = 0 To u -1
         f = InStrRev(cc(i), "|")
         if f > 0 Then
            if AfxIsNumeric(Mid(cc(i), f + 1)) = 0 Then
               cc(i) = YF_Replace(cc(i), "|", "\|")
               f = 0
            Else
               cc(i) = YF_Replace(Left(cc(i), f -1), "|", "\|") & Mid(cc(i), f)
            End if
         End if
         if f = 0 Then cc(i) &= "|0"
      Next
      aa = FF_Join(cc(), "|")
   End if
   Dim st As StyleFormType Ptr = Cast(Any Ptr,Me.UserData(0))
   st->Rvalue = aa
   xiugai =0
   Me.Close
End Sub

Sub customForm_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Me.Close
End Sub

Function customForm_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   if xiugai Then
      
      Select Case MessageBox(hWndForm, vfb_LangString("选项被修改，你需要退出前保存吗？"), "VisualFreeBasic", _
               MB_YESNOCANCEL Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Case IDYES
            Command1.Click 
            Return True
         Case IDNO
            
         Case IDCANCEL
            Return True
      End Select
   End if 
   Function = FALSE ' 如果想阻止窗口关闭，则应返回 TRUE 。
End Function

Sub customForm_Shown(hWndForm As hWnd, UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   xiugai = 0
End Sub
Sub sci_Properties(pSci As Any Ptr) '给编辑器设置属性
   
   Dim rxRatio As Single = AfxScaleX(1)  'DPI比率
   Dim ryRatio As Single = AfxScaleY(1)
   Dim nPixels As Long  ' 行号宽度
   Dim bitsNeeded As Long  '需要的位
   Dim tStr as ZString * 1024
   Dim zz As String = "Microsoft YaHei"
   SciMsg(pSci, SCI_STYLESETFONT, STYLE_DEFAULT, Cast(lParam, StrPtr(zz)))
   SciMsg(pSci, SCI_STYLESETSIZE, STYLE_DEFAULT, 9)
'   SciMsg(pSci, SCI_STYLESETCHARACTERSET, STYLE_DEFAULT, GetFontCharSetID(op.EditorFontCharSet))
   
   ''
   ''  边距0：行号（默认为宽度0）
   tStr = "_9999"
   nPixels = SciMsg(pSci, SCI_TEXTWIDTH, 0, Cast(lParam, @tStr))
   SciMsg(pSci, SCI_SETMARGINTYPEN, 0, SC_MARGIN_NUMBER)
   SciMsg(pSci, SCI_SETMARGINWIDTHN, 0, nPixels)
   SciMsg(pSci, SCI_SETMARGINWIDTHN, 1, 0)
   SciMsg(pSci, SCI_SETMARGINWIDTHN, 2, 0)
   ''
   ''  显示缩进参考线
   SciMsg(pSci, SCI_SETINDENTATIONGUIDES, False, 0)
   
   SciMsg(pSci, SCI_SETHSCROLLBAR, False, 0)   '禁用水平滚动
   
   ''  标识要在单词中使用的字符
   tStr = "~_:\abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
   SciMsg(pSci, SCI_SETWORDCHARS, 0, Cast(lParam, @tStr))
   
   ''  Unicode（UTF-8编码）
   SciMsg(pSci, SCI_SETCODEPAGE, SC_CP_UTF8, 0)
   
'   SciMsg(pSci, SCI_SETEXTRAASCENT, 2, 2) '字符上空位
'   SciMsg(pSci, SCI_SETEXTRADESCENT, 2, 2) '字符下空位   
   
   SciMsg(pSci, SCI_SETLEXER, SCLEX_VB, 0)
    SciMsg(pSci, SCI_USEPOPUP, False, 0)
   '设置FreeBASIC关键字
'   If Len(op.FBKeywordsC) Then
'      SciMsg(pSci, SCI_SETKEYWORDS, 0, Cast(lParam, StrPtr(op.FBKeywordsC)))
'   End If
'   If Len(op.APIKeywordsC) Then
'      SciMsg(pSci, SCI_SETKEYWORDS, 1, Cast(lParam, StrPtr(op.APIKeywordsC)))
'   End If     
'   If Len(op.LibKeywordsC) Then
'      SciMsg(pSci, SCI_SETKEYWORDS, 2, Cast(lParam, StrPtr(op.LibKeywordsC)))
'   End If     
   '禁止折叠程序和功能
   tStr = "fold"
   SciMsg(pSci, SCI_SETPROPERTY, Cast(wParam, @tStr), Cast(lParam, @"0"))
   
   
End Sub

Sub customForm_Scintilla1_WM_ContextMenu(hWndForm As hWnd, hWndControl As hWnd, xPos As Long, yPos As Long)  '鼠标右键单击
   Dim P as Point
   GetCursorPos @p   
   dim id As Long = TrackPopupMenu( PopupMenu1.hMenu , TPM_RETURNCMD Or TPM_NONOTIFY, p.x, p.y, 0, hWndForm, Null) '比方说弹出菜单
   select case id
      Case PopupMenu1_撤销 ' 撤销
         Scintilla1.SendMessage(SCI_UNDO, 0, 0)
      Case PopupMenu1_重做 ' 重做
         Scintilla1.SendMessage(SCI_REDO, 0, 0)
      Case PopupMenu1_剪切 ' 剪切
         Scintilla1.SendMessage(SCI_CUT, 0, 0)
      Case PopupMenu1_复制 ' 复制
         Scintilla1.SendMessage(SCI_COPY, 0, 0)
      Case PopupMenu1_粘贴 ' 粘贴
         Scintilla1.SendMessage(SCI_PASTE, 0, 0)
      Case PopupMenu1_删除 ' 删除
         Scintilla1.SendMessage(SCI_DELETERANGE, 0, 0)
      Case PopupMenu1_全选 ' 全选
         Scintilla1.SendMessage(SCI_SELECTALL, 0, 0)
      Case PopupMenu1_清空 ' 清空
         Scintilla1.SendMessage(SCI_CLEARALL, 0, 0)
   End Select
End Sub

Sub customForm_Scintilla1_SCN_CharAdded(hWndForm As hWnd, hWndControl As hWnd, sN As SCNOTIFICATION )  '输入普通文本字符
   '当用户键入输入到文本中的普通文本字符（而不是命令字符）时发送。容器可以使用它来决定显示呼叫提示或自动完成列表。
   'pNSC.ch  '字符码
   xiugai =1
End Sub

Sub customForm_Scintilla1_SCN_Updateui(hWndForm As hWnd, hWndControl As hWnd, sN As SCNOTIFICATION )  '文本或样式已更改
   'sN 为 Scintilla 事件信息的结构 详细见：https://www.scintilla.org/ScintillaDoc.html#Notifications
   'sN.updated  更改  
   ''          SC_UPDATE_CONTENT    =1  内容，样式或标记可能已更改。    
   ''          SC_UPDATE_SELECTION  =2  选择可能已更改。
   ''          SC_UPDATE_V_SCROLL   =4  可能已垂直滚动。
   ''          SC_UPDATE_H_SCROLL   =8  可能已水平滚动。
 xiugai =1
End Sub









