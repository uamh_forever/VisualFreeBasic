﻿#VisualFreeBasic_Form#  Version=5.5.6
Locked=0

[Form]
Name=StausForm
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_BORDER,WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP
Style=3 - 常规窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=状态栏设置
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=382
Height=331
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=8
Top=5
Width=136
Height=115
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=文本：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=11
Top=126
Width=36
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=提示：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=11
Top=153
Width=36
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=47
Top=125
Width=325
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=46
Top=151
Width=326
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=样式：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=11
Top=211
Width=36
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=对齐：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=11
Top=183
Width=36
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=宽度：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=11
Top=238
Width=36
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[ComboBox]
Name=Combo1
Index=-1
Style=0 - 标签和下拉框
Custom=
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=False
AutoHscroll=False
MaxLength=0
Uppercase=False
Lowercase=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=46
Top=178
Width=95
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ComboBox]
Name=Combo2
Index=-1
Style=0 - 标签和下拉框
Custom=
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=False
AutoHscroll=False
MaxLength=0
Uppercase=False
Lowercase=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=46
Top=209
Width=95
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text3
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=0
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=46
Top=240
Width=95
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=新增
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=214
Top=94
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=插入
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=269
Top=94
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=324
Top=94
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=▲
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=151
Top=94
Width=21
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=▼
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=176
Top=94
Width=21
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line1
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=8
Top=268
Width=363
Height=3
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=确定
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=239
Top=273
Width=62
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=308
Top=273
Width=62
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Image]
Name=Image1
Index=-1
Picture=
Stretch=0 - 自动适应
Percent=0
GrayScale=False
Enabled=True
Visible=True
Left=174
Top=193
Width=30
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame1
Index=-1
Caption=图标
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9
Left=161
Top=173
Width=127
Height=61
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command8
Index=-1
Caption=选择
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=234
Top=194
Width=43
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label6
Index=-1
Style=0 - 无边框
Caption=单位像素，设0为自动，支持高DPI
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=147
Top=243
Width=222
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label7
Index=-1
Style=0 - 无边框
Caption=选择自绘，在自绘事件中写代码自绘
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=8
Top=279
Width=215
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]
Type StausList_t
   zText As String    'utf8 格式
   zTipText As String  'utf8 格式
   alignment As Long 
   uType As Long 
   nWidth as Long 
   nIco As String    'utf8 格式
End Type 
Dim Shared StausList() As StausList_t, StausZ As Long, StausX As Long ' Z 正在设置  X 被修改

Sub StausForm_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   Me.UserData(0) = UserData
   Dim As Rect rc,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr,UserData)
   GetWindowRect(st->hWndForm , @rc2)
   GetWindowRect(st->hWndList , @rc)
   if rc.Left + Me.Width > rc2.right Then rc.Left = rc2.right - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top = rc2.bottom - Me.Height
   Me.Move rc.Left, rc.top   
   
End Sub
Sub StausForm_Shown(hWndForm As hWnd ,UserData As Integer) '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   Combo1.AddItem vfb_LangString("左对齐")
   Combo1.AddItem vfb_LangString("居中")
   Combo1.AddItem vfb_LangString("右对齐")
   Combo2.AddItem vfb_LangString("无边框")
   Combo2.AddItem vfb_LangString("凹边框")
   Combo2.AddItem vfb_LangString("凸边框")
   Combo2.AddItem vfb_LangString("自绘")
   Combo1.ListIndex = 0
   Combo2.ListIndex = 0
   
   Dim st As StyleFormType Ptr = Cast(Any Ptr ,UserData)
   Dim u  As Long ,i As Long
   Dim ss As String = trim( *st->value)
   if Len(ss) = 0 Then
      Erase StausList
   Else
      Dim el() As String
      u = vbSplit(ss ,chr(1) ,el())
      ReDim StausList(u -1)
      Dim si As Long = -1
      for i = 0 To u -1
         el(i) = Trim(el(i))
         if Len(el(i)) Then
            Dim sl() As String
            Dim uu   As Long = vbSplit(el(i) ,Chr(2) ,sl())
            if uu > 5 Then
               si                      += 1
               StausList(si).zText     = sl(0) 'utf8 格式
               StausList(si).zTipText  = sl(1) 'utf8 格式
               StausList(si).alignment = valint(sl(2))
               StausList(si).uType     = valint(sl(3))
               StausList(si).nWidth    = valint(sl(4))
               StausList(si).nIco      = sl(5) 'utf8 格式
               List1.AddItem StringToCWSTR("窗格" & si)
            End if
         End if
      Next
      if si = -1 Then
         Erase StausList
      Else
         if UBound(StausList) <> si Then ReDim Preserve StausList(si)
      End if
   End if
   List1.ListIndex = List1.ListCount -1
   StausForm_List1_LBN_SelChange 0 ,0
   StausZ = 0
   StausX = 0
End Sub

Sub StausForm_List1_LBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '选择了列表
   Dim si As Long = List1.ListIndex
   if si = -1 Then
      Text1.Enabled = False
      Text2.Enabled = False
      Combo1.Enabled = False
      Combo2.Enabled = False
      Text3.Enabled = False
      Command8.Enabled = False
      Command3.Enabled = False
      Command2.Enabled = False
      Command4.Enabled = False
      Command5.Enabled = False
   Else
      StausZ = 1
      Text1.Enabled = True
      Text2.Enabled = True
      Combo1.Enabled = True
      Combo2.Enabled = True
      Text3.Enabled = True
      Command8.Enabled = True
      Command3.Enabled = True
      Command2.Enabled = True
      Command4.Enabled = si > 0
      Command5.Enabled = si < List1.ListCount -1
      Text1.Text = Utf8toStr(StausList(si).zText)     'utf8 格式
      Text2.Text = Utf8toStr(StausList(si).zTipText)  'utf8 格式
      Combo1.ListIndex = StausList(si).alignment
      Combo2.ListIndex = StausList(si).uType
      Text3.Text = WStr(StausList(si).nWidth)
      Dim pa As String = GetProRunFile(0,4)
      Dim bb As String = StausList(si).nIco
      Dim ffi As Long = InStr(bb, "|")
      if ffi > 0 Then bb = left(bb, ffi -1)
      Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      StausZ = 0
   End if
   
End Sub

Sub StausForm_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Me.Close 
End Sub

Sub StausForm_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(StausList) + 1
   ReDim Preserve StausList(u)
   StausList(u).uType = 1
   List1.ListIndex  =  List1.AddItem ("窗格" & u)
   StausForm_List1_LBN_SelChange 0, 0
   StausX =1 
End Sub

Function StausForm_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   if StausX <> 0 Then
      Select Case MessageBox(hWndForm, vfb_LangString("状态栏已经被修改，你要保存修改吗？"), "VisualFreeBasic", _
               MB_YESNOCANCEL Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Case IDYES
            SaveStaus() '保存数据
         Case IDNO
         Case IDCANCEL
            Return 1
      End Select
   End if      
  
   Function = 0 '根据自己需要修改
End Function

Sub SaveStaus() '保存数据
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))

   Dim ss As String
   Dim i As Long
   if UBound(StausList) > -1 Then
      for i = 0 To UBound(StausList)
         if Len(ss) > 0 Then ss &= chr(1)
         ss &= StausList(i).zText & chr(2) & _    'utf8 格式
            StausList(i).zTipText & chr(2) & _   'utf8 格式
            StausList(i).alignment & chr(2) & _
            StausList(i).uType & chr(2) & _
            StausList(i).nWidth & chr(2) & _
            StausList(i).nIco     'utf8 格式
      Next
   End if

  st->Rvalue = ss
   StausX =0 
End Sub

Sub StausForm_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if StausX <> 0 Then SaveStaus() '保存数据
   Me.Close 
End Sub

Sub StausForm_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(StausList) + 1
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ReDim Preserve StausList(u)
   List1.AddItem(vfb_LangString("窗格") & u)
   Dim i As Long
   for i = u To si + 1 Step -1
      StausList(i) = StausList(i -1)
   Next
   StausList(si).zText = ""    'utf8 格式
   StausList(si).zTipText = ""  'utf8 格式
   StausList(si).alignment = 0
   StausList(si).uType = 1
   StausList(si).nWidth = 0
   StausList(si).nIco = ""    'utf8 格式
   List1.ListIndex = si
   StausForm_List1_LBN_SelChange 0, 0
   StausX =1
End Sub

Sub StausForm_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   
   Select Case MessageBox(hWndForm, vfb_LangString("你真的要删除吗？"), "VisualFreeBasic", _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON2 Or MB_APPLMODAL)
      Case IDYES
      Case IDNO
         Return
   End Select
   
   Dim u As Long = ubound(StausList)
   if u = 0 Then
      Erase StausList
      List1.Clear
      u -= 1
   Else
      Dim si As Long = List1.ListIndex
      if si = -1 Then Return
      if si < u Then
         for i As Long = si To u -1
            StausList(i) = StausList(i + 1)
         Next
      End if
      u -= 1
      if u = -1 Then
         Erase StausList
      Else
         ReDim Preserve StausList(u)
      End if
   End if
   List1.Clear
   if u > -1 Then
      for i As Long = 0 To u
         List1.AddItem StringToCWSTR(vfb_LangString("窗格") & i)
      Next
   End if
   StausForm_List1_LBN_SelChange 0, 0
   StausX = 1
End Sub

Sub StausForm_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(StausList)
   Dim si As Long = List1.ListIndex
   if si <0 Then Return
   Swap StausList(si), StausList(si -1)
   List1.ListIndex = si -1
   StausForm_List1_LBN_SelChange 0, 0
   StausX =1
End Sub

Sub StausForm_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(StausList)
   Dim si As Long = List1.ListIndex
   if si = -1 Or si >= u Then Return
   Swap StausList(si), StausList(si + 1)
   List1.ListIndex = si + 1
   StausForm_List1_LBN_SelChange 0, 0
   StausX = 1
End Sub

Sub StausForm_Text1_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if StausZ<>0 Then Return 
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   StausList(si).zText = CWSTRtoUTF8(Text1.Text)
   StausX = 1
End Sub

Sub StausForm_Text2_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if StausZ<>0 Then Return 
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   StausList(si).zTipText = CWSTRtoUTF8(Text2.Text)
   StausX = 1
End Sub

Sub StausForm_Text3_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if StausZ<>0 Then Return 
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   Dim cc As CWSTR = Text3.Text
   StausList(si).nWidth = cc.ValInt
   StausX = 1
End Sub

Sub StausForm_Combo1_CBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '列表框中更改当前选择时
   if StausZ<>0 Then Return 
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   StausList(si).alignment = Combo1.ListIndex 
   StausX = 1
End Sub

Sub StausForm_Combo2_CBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '列表框中更改当前选择时
   if StausZ<>0 Then Return 
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   StausList(si).uType = Combo2.ListIndex 
   StausX = 1
End Sub

Sub StausForm_Command8_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   Dim bb As String = StrToUtf8(GetImgForm(Utf8toStr(StausList(si).nIco),0))  '获取图像文件名称
   If bb <> StausList(si).nIco Then
      StausList(si).nIco = bb
      Dim ffi As Long = InStr(bb, "|")
      if ffi > 0 Then bb = left(bb, ffi -1)
      Dim pa As String = GetProRunFile(0,4)
      Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      StausX = 1
   End If
End Sub






















