Type Class_WinHook
Private : 
   m_hWndForm As HWnd '控件句柄
   m_IDC As Long     '控件IDC
   Declare Function GetFP() As FormControlsPro_TYPE ptr '返回控件保存数据的指针（一般是类自己用）
Public : 
   Declare Property hWndForm() As.hWnd         '返回/设置控件所在的窗口句柄，主要用于多开同一个窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
   Declare Property hWndForm(ByVal hWndParent As.hWnd) '获取控件所在的窗口句柄（父窗口）
   Declare Property IDC() As Long             '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Property Tag() As CWSTR                       '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWSTR)
   Declare Property UserData(idx AS LONG) As Integer      '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx AS LONG ,bValue As Integer)   
   '{代码不提示}
   Declare Property sHook(idHook As Integer) As HHOOK   '返回/设置 钩子句柄（一般由VFB自动控制），应由 SetWindowsHookEx 返回，以便于 UnhookWindowsHookEx 释放。{1.WH_KEYBOARD 键盘钩子.WH_KEYBOARD_LL 底层键盘钩子.WH_MOUSE 鼠标钩子.WH_MOUSE_LL 底层鼠标钩子.WH_CALLWNDPROC 消息处理前.WH_CALLWNDPROCRET 消息处理完.WH_CBT CBT应用通知.WH_DEBUG 调试.WH_FOREGROUNDIDLE 即将变为空闲.WH_GETMESSAGE 消息队列.WH_JOURNALPLAYBACK 输入消息前.WH_JOURNALRECORD 输入消息后.WH_MSGFILTER 输入事件.WH_SHELL 外壳通知.WH_SYSMSGFILTER 系统输入事件}
   Declare Property sHook(idHook As Integer ,vHOOK As HHOOK )   
   Declare Sub UnAllHook()  '卸载所有的钩子 一般是在关闭窗口后，控件自己调用此来关闭。
   
End Type
Sub Class_WinHook.UnAllHook()  '卸载所有的钩子 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then  ' 
      if len(fp->nData) > 100 Then 
         Dim tHOOK As HHOOK Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         Dim i As Long 
         For i=0 To 15
           If tHOOK[i] Then UnhookWindowsHookEx tHOOK[i]
        Next  
      End if 
   End If 
End Sub

Function Class_WinHook.GetFP() As FormControlsPro_TYPE Ptr '返回自己控件的指针
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(m_hWndForm)
   While fp
      if fp->IDC = m_IDC Then Return fp
      fp = fp->VrControls
   Wend
End Function
Property Class_WinHook.hWndForm() As .hWnd         '返回/设置控件所在的窗口句柄
      Return m_hWndForm
End Property
Property Class_WinHook.hWndForm(ByVal hWndParent As .hWnd)
   m_hWndForm = hWndParent
End Property

Property Class_WinHook.IDC() As Long                  '返回/设置两次调用 Timer 控件的 Timer 事件间隔的毫秒数。
   Return m_IDC
End Property
Property Class_WinHook.IDC(ByVal NewIDC As Long)
   m_IDC  =NewIDC
End Property
Property Class_WinHook.sHook(idHook As Integer) As HHOOK
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then  '
      if len(fp->nData) > 100 And idHook < 15 And idHook > -2 Then
         Dim tHOOK As HHOOK Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         Return tHOOK[idHook + 1]
      End if
   End If
End Property
Property Class_WinHook.sHook(idHook As Integer, vHOOK As HHOOK)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then  '
      if len(fp->nData) < 100 Then fp->nData = String(150, 0)
      if idHook < 15 And idHook > -2 Then
         Dim tHOOK As HHOOK Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         tHOOK[idHook + 1] = vHOOK
      End if
   End If
End Property
Property Class_WinHook.UserData(idx AS LONG) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return 0
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      Return fp->UserData(idx)
   End If   

End Property
Property Class_WinHook.UserData(idx AS LONG, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      fp->UserData(idx) = bValue
   End If    
End Property


