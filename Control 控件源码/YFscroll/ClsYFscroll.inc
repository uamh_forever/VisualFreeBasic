'控件类
Type Class_YFscroll Extends Class_Control
   Private : '私有区
   mColor   As Long '鼠标移动的颜色，GDI 颜色
   kMouse   As Long '鼠标在那个按钮上，'向上或左=1  向下或右=2 滑块=3 滑块前或上=4 滑块后或下=5
   kPress   As Long '鼠标是不是按下
   dragX    As Long '拖动时的鼠标位置
   dragY    As Long '拖动时的鼠标位置
   dragNewX As Long '拖动时的鼠标位置
   dragNewY As Long '拖动时的鼠标位置
   dragV    As Long '拖动时当前值
   Declare Sub TriggerEvent(fp As FormControlsPro_TYPE Ptr ,aa As Long) ' 触发事件
   Declare Function GetAn(fp As FormControlsPro_TYPE Ptr ,xPos As Long ,yPos As Long) As Long '根据鼠标位置，返回按钮，0表示不在按钮上，1开始在按钮上。
   Declare Sub TabPainting(vhWnd As .hWnd) ' 绘画（控件自用）
   Public : '公共区
   Frame As Long '是否显示框线 {=.True.False}
   Declare Property Value() As Long '返回/设置滚动值
   Declare Property Value(ByVal bValue As Long) '
   Declare Property nMax() As Long '返回/设置滚动条最大值
   Declare Property nMax(ByVal posMax As Long) '
   Declare Property nMin() As Long '返回/设置滚动最小值
   Declare Property nMin(ByVal posMin As Long) '
   Declare Property nPage() As Long '返回/设置页大小，比如可显示行数，影响滑块显示大小。=0 为自动调整。
   Declare Property nPage(ByVal bPage As Long) '
   Declare Property MoveColor() As Long '返回/赋值 鼠标移动的颜色，GDI 颜色
   Declare Property MoveColor(bValue As Long) '给属性
   Declare Property SmallChange() As Long '返回/设置用户单击滚动条剪头时，滚动条 Value 属性改变的数量
   Declare Property SmallChange(ByVal bValue As Long) '
   Declare Property LargeChange() As Long '返回/设置用户单击滚动条区域时，滚动条 Value 属性改变的数量
   Declare Property LargeChange(ByVal bValue As Long) '
   '{代码不提示}
   Declare Function MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult '消息处理（控件自用）
   '{代码可提示}
End Type
'私有变量保存
'fp->CtlData(0)  滚动条当前值
'fp->CtlData(1)  滚动条最大值
'fp->CtlData(2)  滚动最小值
'fp->CtlData(3)  剪头滚动值
'fp->CtlData(4)  区域滚动值
'fp->CtlData(5)  页大小

Function Class_YFscroll.MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult
   Dim fp As FormControlsPro_TYPE Ptr
   Select Case wMsg
      Case WM_PAINT
         This.TabPainting(vhWnd)
         Return True
      Case WM_USER + 200 '预防多线程操作控件，需要发消息处理
      Case WM_SIZE
         FF_Redraw vhWnd
         'Return jj
      Case WM_MOUSEMOVE
         '移出控件侦察
         Dim entTrack As tagTRACKMOUSEEVENT
         entTrack.cbSize      = SizeOf(tagTRACKMOUSEEVENT)
         entTrack.dwFlags     = TME_LEAVE
         entTrack.hwndTrack   = vhWnd
         entTrack.dwHoverTime = HOVER_DEFAULT
         TrackMouseEvent @entTrack
         '处理移动
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(fp ,xPos ,yPos)
         Dim bb As Long = MouseFlags = 1
         If (dragX > 0 Or dragY > 0) And bb <> 0 Then
            dragNewX = xPos
            dragNewY = yPos
            FF_Redraw vhWnd
         Else
            If aa <> kMouse Or kPress <> bb Then
               kMouse = aa
               kPress = bb
               FF_Redraw vhWnd
            End If
         End If
      Case WM_MOUSELEAVE '处理移出
         kMouse   = 0
         kPress   = 0
         dragX    = 0
         dragNewX = 0
         dragY    = 0
         dragNewY = 0
         FF_Redraw vhWnd
      Case WM_LBUTTONDOWN ,WM_LBUTTONDBLCLK
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(fp ,xPos ,yPos)
         Dim bb As Long = True
         ReleaseCapture '取消鼠标绑定控件
         SetCapture vhWnd '绑定鼠标控件，让鼠标按下拖动到控件外面也能有鼠标移动消息
         
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            If aa = 3 Then
               dragNewX = xPos
               dragX    = xPos
               dragNewY = yPos
               dragY    = yPos
               dragV = fp->CtlData(0) - fp->CtlData(2)
               SendMessage(hWndControl ,WM_USER + 101 ,fp->CtlData(0) ,4)  '滑块开始
            Else
               This.TriggerEvent(fp ,aa)
               SetTimer(vhWnd ,101 ,500 ,NULL)
            End If
            FF_Redraw vhWnd
         End If
      Case WM_LBUTTONUP
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         ReleaseCapture '取消鼠标绑定控件
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(fp ,xPos ,yPos)
         Dim bb As Long
         KillTimer(vhWnd ,101)
         KillTimer(vhWnd ,102)
         If dragX>0 Or dragY>0 Then 
            SendMessage(hWndControl ,WM_USER + 101 ,fp->CtlData(0) ,6)  '滑块结束
         End If 
         dragX    = 0
         dragNewX = 0
         dragY    = 0
         dragNewY = 0
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            
            FF_Redraw vhWnd
         End If
      Case WM_TIMER
         Select Case nwParam
            Case 101
               SetTimer(vhWnd ,102 ,50 ,NULL)
            Case 102
               fp = vfb_Get_Control_Ptr(vhWnd)
               If fp = 0 Then Return 0
               This.TriggerEvent(fp ,kMouse)
               FF_Redraw vhWnd
         End Select
      'Case WM_NOTIFY
      '   '为了工具提示时，鼠标指针挡住了提示文字，因此需要我们自己调整显示位置。
      '   fp = vfb_Get_Control_Ptr(vhWnd)
      '   If fp = 0 Then Return 0
      '   If fp->ToolTipBalloon = 0 Then
      '      Dim nn As NMHDR Ptr = Cast(Any Ptr ,nlParam)
      '      Select Case nn->code
      '         Case TTN_SHOW '工具提示时，设置显示位置
      '            Dim A As Point
      '            GetCursorPos @a
      '            SetWindowPos(nn->hwndFrom ,NULL ,A.x ,A.y + GetSystemMetrics(SM_CYCURSOR) ,0 ,0 ,SWP_NOSIZE Or SWP_NOZORDER Or SWP_NOACTIVATE)
      '            Return True
      '      End Select
      '   End If
      Case WM_ERASEBKGND
         Return True '防止擦除背景
   End Select
   Function = 0
End Function
Sub Class_YFscroll.TriggerEvent(fp As FormControlsPro_TYPE Ptr ,aa As Long) '触发事件
   Dim vv          As Long = fp->CtlData(0)
   Dim nScrollCode As Long
   Select Case aa '向上或左=1  向下或右=2 滑块=3 滑块前或上=4 滑块后或下=5
      Case 1 '触发按钮事件
         vv          -= fp->CtlData(3)
         nScrollCode = 0
      Case 2
         vv          += fp->CtlData(3)
         nScrollCode = 1
      Case 3
         Return
      Case 4
         vv          -= fp->CtlData(4)
         nScrollCode = 2
      Case 5
         vv          += fp->CtlData(4)
         nScrollCode = 3
   End Select
   If vv < fp->CtlData(2) Then vv = fp->CtlData(2)
   If vv > fp->CtlData(1) Then vv = fp->CtlData(1)
   If vv <> fp->CtlData(0) Then
      If SendMessage(hWndControl ,WM_USER + 102 ,fp->CtlData(0) ,vv) = 0 Then
         fp->CtlData(0) = vv
         SendMessage(hWndControl ,WM_USER + 101 ,vv ,nScrollCode)
      End If
   end if
End Sub
Sub Class_YFscroll.TabPainting(vhWnd As .hWnd) ' 绘画
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return
   Dim BColor As Long = GetCodeColorGDI(fp->BackColor) '背景色
   Dim fColor As Long = GetCodeColorGDI(fp->ForeColor) '前景色
   
   Dim gg  As yGDI   = yGDI(vhWnd ,BColor ,True) 'WM_PAINT事件里必须用这行初始化 yGDI
   Dim ww  As Long   = gg.m_Width ,hh As Long = gg.m_Height
   Dim dpi As Single = gg.Dpi
   Dim jt  As Long   = 16 *dpi '箭头宽度或高度
   Dim hk  As Long '滑块宽度或高度
   Dim zz  As Long = fp->CtlData(1) - fp->CtlData(2) '总数量
   Dim vv  As Long = fp->CtlData(0) - fp->CtlData(2) '当前值
   Dim tt  As Long
   Dim zt  As Single '滑块占空比
   If fp->CtlData(5) = 0 Then
      '计算一下控件位置能够容纳多少个最小滑块尺寸
      If ww > hh Then
         hk = ww / jt
      Else
         hk = hh / jt
      End If
      If zz < hk Then
         zt = 1 / (zz + 1) '当总数量少于容纳数量，用直接倒数
      Else
         zt = 1 / 30
      End If
   Else
      zt = fp->CtlData(5) / (zz+fp->CtlData(5)) '有页大小时，采用页和总数比例
      If zt > 1 Then zt = 1
   End If
   
   gg.dpi = 1
   ''画滚动条
   gg.Pen IIf(Frame,1,0) ,fColor
   '滚动条外框
   gg.Brush BColor
   gg.DrawFrame 0 ,0 ,ww ,hh
   'fp->CtlData(0)  滚动条当前值
   'fp->CtlData(1)  滚动条最大值
   'fp->CtlData(2)  滚动最小值
   'fp->CtlData(5)  页大小
   
   If ww > hh Then
      '水平
      If ww < 5 Or hh < 2 Then Return
      If ww < jt * 2      Then jt = 0
      hk = (ww - jt * 2) *zt
      If hk <= 16 *dpi Then hk = 16 *dpi
      If hk > ww       Then hk = ww
   Else
      '垂直
      If hh < 5 Or ww < 2 Then Return
      If hh < jt * 2      Then jt = 0
      hk = (hh - jt * 2) *zt
      If hk <= 16 *dpi Then hk = 16 *dpi
      If hk > hh       Then hk = hh
   End If
   gg.GpPen 0 ',ColorGdiToGDIplue(fColor,255)
   
   Dim PO(2) As GpPointF
   'kMouse As Long '鼠标在那个按钮上，'向上或左=1  向下或右=2 滑块=3 滑块前或上=4 滑块后或下=5
   'kPress As Long '鼠标是不是按下
   'PrintA Hex(mColor)
   If ww > hh Then
      '水平
      If jt Then
         'PrintA kMouse,kPress
         gg.DrawFrame 0 ,0 ,jt ,hh
         hh-=1
         PO(0).X = 4 *dpi  : PO(0).Y = hh / 2
         PO(1).X = 10 *dpi : PO(1).Y = hh / 2 - 4 * dpi : If PO(1).Y < 0  Then PO(1).Y = 0
         PO(2).X = 10 *dpi : PO(2).Y = hh / 2 + 4 * dpi : If PO(2).Y > hh Then PO(2).Y = hh
           hh +=1
         gg.GpBrush ColorGdiToGDIplue(IIf(kMouse = 1 ,mColor ,fColor) ,IIf(kPress ,100 ,255))
         gg.gpDrawPolygon PO()
         gg.DrawFrame ww - jt ,0 ,jt ,hh
         hh-=1 
         PO(0).X = ww - 11 *dpi : PO(0).Y = hh / 2 - 4 * dpi : If PO(0).Y < 0 Then PO(0).Y = 0
         PO(1).X = ww -5 *dpi  : PO(1).Y = hh / 2
         PO(2).X = ww -11 *dpi : PO(2).Y = hh / 2 + 4 * dpi : If PO(2).Y > hh Then PO(2).Y = hh
           hh+=1
         gg.GpBrush ColorGdiToGDIplue(IIf(kMouse = 2 ,mColor ,fColor) ,IIf(kPress ,100 ,255))
         gg.gpDrawPolygon PO()
      End If
      gg.Brush IIf(kMouse = 3 ,mColor ,fColor)
      If dragX > 0 Then
         If zz = 0 Then '除数不能为0，需要判断一下
            tt = jt
         Else
            tt = (ww - jt * 2 - hk) / zz * dragV + jt
         End If
         tt += (dragNewX - dragX)
         If tt < jt             Then tt = jt
         If tt > (ww - jt - hk) Then tt = (ww - jt - hk)
      Else
         If zz = 0 Then '除数不能为0，需要判断一下
            tt = jt
         Else
            tt = (ww - jt * 2 - hk) / zz * vv + jt
         End If
      End If
      gg.DrawFrame tt ,0 ,hk ,hh
      If dragX > 0 Then '从tt的值反算出 vv 的值
         tt -= jt
         vv = tt / ((ww - jt * 2 - hk) / zz)
         vv += fp->CtlData(2)
         If vv <> fp->CtlData(0) Then
            If SendMessage(hWndControl ,WM_USER + 102 ,fp->CtlData(0) ,vv) = 0 Then
               fp->CtlData(0) = vv
               SendMessage(hWndControl ,WM_USER + 101 ,vv ,5)
            End If
         End If
      End If
   Else
      '垂直
      If jt Then
         gg.DrawFrame 0 ,0 ,ww ,jt
          ww-=1 
         PO(0).X = ww / 2 : PO(0).Y = 4 *dpi
         PO(1).X = ww / 2 + 4 *dpi : PO(1).Y = 10 *dpi : If PO(1).X > ww Then PO(1).X = ww
         PO(2).X = ww / 2 -4 *dpi : PO(2).Y = 10 * dpi : If PO(1).X < 0 Then PO(1).X = 0
            ww+=1
         gg.GpBrush ColorGdiToGDIplue(IIf(kMouse = 1 ,mColor ,fColor) ,IIf(kPress ,100 ,255))
         gg.gpDrawPolygon PO()
         gg.DrawFrame 0 ,hh - jt ,ww ,jt
         ww-=1 
         PO(0).X = ww       / 2 -4 *dpi : PO(0).Y = hh -11 *dpi : If PO(0).X < 0 Then PO(0).X = 0
         PO(1).X = ww       / 2 + 4 *dpi : PO(1).Y = hh -11 *dpi : If PO(1).x > ww Then PO(1).x = ww
         PO(2).X = ww / 2 : PO(2).Y = hh -5 * dpi
              ww+=1
         gg.GpBrush ColorGdiToGDIplue(IIf(kMouse = 2 ,mColor ,fColor) ,IIf(kPress ,100 ,255))
         gg.gpDrawPolygon PO()
      End If

      gg.Brush IIf(kMouse = 3 ,mColor ,fColor)
      If dragY > 0 Then
         If zz = 0 Then '除数不能为0，需要判断一下
            tt = jt
         Else
            tt = (hh - jt * 2 - hk) / zz * dragV + jt
         End If
         tt += (dragNewY - dragY)
         If tt < jt             Then tt = jt
         If tt > (hh - jt - hk) Then tt = (hh - jt - hk)
      Else
         If zz = 0 Then '除数不能为0，需要判断一下
            tt = jt
         Else
            tt = (hh - jt * 2 - hk) / zz * vv + jt
         End If
      End If
      gg.DrawFrame 0 ,tt ,ww ,hk
      If dragX > 0 Then '从tt的值反算出 vv 的值
         tt -= jt
         vv = tt / ((hh - jt * 2 - hk) / zz)
         vv += fp->CtlData(2)
         If vv <> fp->CtlData(0) Then
            If SendMessage(hWndControl ,WM_USER + 102 ,fp->CtlData(0) ,vv) = 0 Then
               fp->CtlData(0) = vv
               SendMessage(hWndControl ,WM_USER + 101 ,vv ,5)
            End If
         End If
      End If
   End If
End Sub
   
Property Class_YFscroll.Value() As Long '返回/设置滚动值
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return 0
   Property = fp->CtlData(0)
   
End Property
Property Class_YFscroll.Value(ByVal bValue As Long)     '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return
   If bValue < fp->CtlData(2) Then bValue = fp->CtlData(2)
   If bValue > fp->CtlData(1) Then bValue = fp->CtlData(1)
   fp->CtlData(0) = bValue
   FF_Redraw hWndControl '刷新控件
End Property
Property Class_YFscroll.nPage() As Long
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return 0
   Property = fp->CtlData(5)
End Property
Property Class_YFscroll.nPage(ByVal bPage As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0             Then Return
   fp->CtlData(5) = bPage
   FF_Redraw hWndControl '刷新控件
End Property
Property Class_YFscroll.nMax() As Long                 '返回/设置滚动条最大值
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0             Then Return 0
   Property = fp->CtlData(1)
End Property
'fp->CtlData(0)  滚动条当前值
'fp->CtlData(1)  滚动条最大值
'fp->CtlData(2)  滚动最小值
'fp->CtlData(3)  剪头滚动值
'fp->CtlData(4)  区域滚动值
'fp->CtlData(5)  页大小
Property Class_YFscroll.nMax(ByVal posMax As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0                  Then Return
   If posMax < fp->CtlData(2) Then fp->CtlData(1) = fp->CtlData(2) Else fp->CtlData(1) = posMax
   If fp->CtlData(0)>fp->CtlData(1) Then fp->CtlData(0)=fp->CtlData(1)
   
   FF_Redraw hWndControl '刷新控件
End Property
Property Class_YFscroll.nMin() As Long '返回/设置滚动最小值
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return 0
   Property = fp->CtlData(2)
End Property
Property Class_YFscroll.nMin(ByVal posMin As Long) '
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0                  Then Return
   If posMin > fp->CtlData(1) Then fp->CtlData(2) = fp->CtlData(1) Else fp->CtlData(2) = posMin
   If fp->CtlData(0)<fp->CtlData(2) Then fp->CtlData(0)=fp->CtlData(2)
   FF_Redraw hWndControl '刷新控件   
End Property
Property Class_YFscroll.SmallChange() As Long
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return 0
   Property = fp->CtlData(3)
End Property
Property Class_YFscroll.SmallChange(ByVal bValue As Long) '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp = 0 then Return
   fp->CtlData(3) = bValue
End Property
Property Class_YFscroll.LargeChange() As Long
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return 0
   Property = fp->CtlData(4)
End Property
Property Class_YFscroll.LargeChange(ByVal bValue As Long) '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return
   fp->CtlData(4) = bValue
End Property
Property Class_YFscroll.MoveColor() As Long '返回属性
   Property = mColor
End Property
Property Class_YFscroll.MoveColor(bValue As Long)'给属性赋值 
      mColor = bValue
   If IsWindow (hWndControl) Then FF_Redraw hWndControl '刷新控件   
End Property
Function Class_YFscroll.GetAn(fp As FormControlsPro_TYPE Ptr ,xPos As Long ,yPos As Long) As Long
   '向上或左=1  向下或右=2 滑块=3 滑块前或上=4 滑块后或下=5
   Dim rc As Rect
   GetClientRect(hWndControl ,@rc)
   Dim As Long ww = (rc.Right - rc.Left) ,hh = (rc.Bottom - rc.Top)
   If xPos > ww Or yPos > hh Or xPos < 0 Or yPos < 0 Then Return 0
   Dim dpi As Single = AfxScaleX(1)
   Dim jt  As Long   = 16 *dpi '箭头宽度或高度
   Dim hk  As Long '滑块宽度或高度
   Dim zz  As Long = fp->CtlData(1) - fp->CtlData(2) '总数量
   Dim vv  As Long = fp->CtlData(0) - fp->CtlData(2) '当前值
   Dim zt  As Single '滑块占空比
   If fp->CtlData(5) = 0  Then
      '计算一下控件位置能够容纳多少个最小滑块尺寸
      If ww > hh Then
         hk = ww / jt
      Else
         hk = hh / jt
      End If
      If zz < hk Then
         zt = 1 / (zz + 1) '当总数量少于容纳数量，用直接倒数
      Else
         zt = 1 / 30
      End If
   Else
      zt = fp->CtlData(5) / (zz+fp->CtlData(5)) '有页大小时，采用页和总数比例
      If zt > 1 Then zt = 1
   End If
   If ww > hh Then
      '水平
      If ww < 5 Or hh < 2 Then Return 0
      If ww < jt * 2      Then jt = 0
      hk = (ww - jt * 2) *zt
      If hk <= 16 *dpi Then hk = 16 *dpi
      If hk > ww       Then hk = ww
   Else
      '垂直
      If hh < 5 Or ww < 2 Then Return 0
      If hh < jt * 2      Then jt = 0
      hk = (hh - jt * 2) *zt
      If hk <= 16 *dpi Then hk = 16 *dpi
      If hk > hh       Then hk = hh
   End If
   '向上或左=1  向下或右=2 滑块=3 滑块前或上=4 滑块后或下=5
   If ww > hh Then
      '水平
      If jt Then
         If xPos <= jt      Then Return 1
         If xPos >= ww - jt Then Return 2
      End If
      If zz = 0 Then '除数不能为0，需要判断一下
         If xPos <= hk + jt Then Return 3
         Return 5
      Else
         vv = (ww - jt * 2 - hk) / zz * vv + jt
         If xPos <= vv     Then Return 4
         If xPos > vv + hk Then Return 5
         Return 3
      End If
   Else
      '垂直
      If jt Then
         If yPos <= jt      Then Return 1
         If yPos >= hh - jt Then Return 2
      End If
      If zz = 0 Then '除数不能为0，需要判断一下
         If yPos <= hk + jt Then Return 3
         Return 5
      Else
         vv = (hh - jt * 2 - hk) / zz * vv + jt
         If yPos <= vv     Then Return 4
         If yPos > vv + hk Then Return 5
         Return 3
      End If
   End If
End Function

