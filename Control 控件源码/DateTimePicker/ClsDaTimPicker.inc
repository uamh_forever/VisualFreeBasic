Type Class_DateTimePicker Extends Class_Control
 Protected : 
   Declare Function DateStrToSys(bValue As String) As SYSTEMTIME  '把字符串时间转换为系统格式时间
Public :   

   Declare Property DateTime() As SYSTEMTIME           '返回/设置时间
   Declare Property DateTime(ByVal bValue As SYSTEMTIME)
   Declare Property MaxDate() As SYSTEMTIME           '返回/设置允许最大时间。
   Declare Property MaxDate(ByVal bValue As SYSTEMTIME)
   Declare Property MinDate() As SYSTEMTIME           '返回/设置允许最小时间。
   Declare Property MinDate(ByVal bValue As SYSTEMTIME)
   Declare Property DateTimeStr() As String            '返回/设置时间，字符格式时间，如：2019-01-01 12:12:12
   Declare Property DateTimeStr( bValue As String)
   Declare Property MaxDateStr() As String           '返回/设置允许最大时间，字符格式时间，如：2019-01-01 12:12:12
   Declare Property MaxDateStr( bValue As String)
   Declare Property MinDateStr() As String           '返回/设置允许最小时间，字符格式时间，如：2019-01-01 12:12:12
   Declare Property MinDateStr( bValue As String)
   Declare Function FormatString(sz As CWSTR) As Boolean '设置显示格式字符串，格式如：yyyy-mm-dd hh:mm:ss
   Declare Property MonthCalStyle() As ULong            '返回/设置控件的样式，详细见WINFBX
   Declare Property MonthCalStyle(ByVal dwStyle As ULong)
   Declare Property MonthCalColor(iColor As Long) As ULong      '返回/设置部分颜色，(视觉样式时无效){1.MCSC_BACKGROUND 月份之间显示的背景颜色.MCSC_MONTHBK 月份内显示的背景颜色.MCSC_TEXT 月内显示文字的颜色.MCSC_TITLEBK 标题中显示的背景颜色.MCSC_TITLETEXT 标题中用于显示文本的颜色.MCSC_TRAILINGTEXT 标题日和尾日文本的颜色}
   Declare Property MonthCalColor(iColor As Long, clr As ULong)
End Type

Property Class_DateTimePicker.DateTime() As SYSTEMTIME           '返回/设置时间
   Dim st As SYSTEMTIME
   DateTime_GetSystemtime(hWndControl, @st)
   Return st
End Property
Property Class_DateTimePicker.DateTime(ByVal bValue As SYSTEMTIME)
  DateTime_SetSystemtime(hWndControl, GDT_VALID, @bValue)
End Property
Property Class_DateTimePicker.MaxDate() As SYSTEMTIME           '返回/设置允许最大时间。
  Dim st(1) As SYSTEMTIME
  DateTime_GetSystemtime(hWndControl, @st(0))
  Return st(1)
End Property
Property Class_DateTimePicker.MaxDate(ByVal bValue As SYSTEMTIME)
  Dim st(1) As SYSTEMTIME
  st(1) = bValue
  DateTime_SetSystemtime(hWndControl, GDTR_MAX, @bValue)
End Property
Property Class_DateTimePicker.MinDate() As SYSTEMTIME           '返回/设置允许最小时间。
  Dim st(1) As SYSTEMTIME
  DateTime_GetSystemtime(hWndControl, @st(0))
  Return st(0)
End Property
Property Class_DateTimePicker.MinDate(ByVal bValue As SYSTEMTIME)
  Dim st(1) As SYSTEMTIME
  st(0) = bValue
  DateTime_SetSystemtime(hWndControl, GDTR_MIN, @bValue)
End Property
Function Class_DateTimePicker.FormatString(sz As CWSTR) As Boolean '设置显示格式字符串
  Return Cast(Boolean, SendMessageW(hWndControl, DTM_SETFORMAT, 0, Cast(lParam, sz.vptr )))
End Function
Property Class_DateTimePicker.MonthCalStyle() As ULong            '返回/设置控件的样式，详细见WINFBX
  Return SendMessage(hWndControl, DTM_FIRST + 12, 0, 0)
End Property
Property Class_DateTimePicker.MonthCalStyle(ByVal dwStyle As ULong)
  SendMessage(hWndControl, DTM_FIRST + 11, 0, Cast(lParam, dwStyle))
End Property
Property Class_DateTimePicker.MonthCalColor(iColor As Long) As ULong      '返回/设置部分颜色
  Return SendMessage(hWndControl, DTM_GETMCCOLOR, iColor, 0)
End Property
Property Class_DateTimePicker.MonthCalColor(iColor As Long, clr As ULong)
  SendMessage(hWndControl, DTM_SETMCCOLOR, iColor, clr)
End Property
Function Class_DateTimePicker.DateStrToSys(bValue As String) As SYSTEMTIME
   Dim As Long  I,a,b,c
   Dim As String y,m,d,h,mm,s
   Dim As SYSTEMTIME tt
   if len(bValue) = 0 then 
      tt.wYear = Year(Now) : tt.wMonth =Month (Now) : tt.wDay = Day(Now)
      tt.wHour = Hour(Now) : tt.wMinute = Minute(Now) : tt.wSecond = Second(Now)
      return tt
   end if
   
   b = -1
   For i = 0 To Len(bValue) -1
      c = bValue[i]
      If c < 48 or c > 57 Then  '不是数字
         If a = 0 Then  '中间不管分割多少字符
            a = 1
            b += 1
         End If
      Else
         If b = -1 Then b = 0
         Select Case b
            Case 0
               y &= Chr(c)
            Case 1
               m &= Chr(c)
            Case 2
               d &= Chr(c)
            Case 3
               h &= Chr(c)
            Case 4
               mm &= Chr(c)
            Case 5
               s &= Chr(c)
            Case Else
               Exit For
         End Select
         a = 0
      End If
   Next
   tt.wYear = ValInt(y) :tt.wMonth = ValInt(M) : tt.wDay = ValInt(D)
   tt.wHour  = ValInt(h) :tt.wMinute  = ValInt(mm) : tt.wSecond = ValInt(s)
   If tt.wYear < 100 Then tt.wYear = 100
   If tt.wYear > 9999 Then tt.wYear = 9999
   If tt.wMonth < 1 Then tt.wMonth = 1
   If tt.wMonth > 12 Then tt.wMonth = 12
   If tt.wDay < 1 Then tt.wDay = 1
   If tt.wDay > 31 Then tt.wDay = 31
   if tt.wHour > 23 then tt.wHour = 23
   if tt.wMinute > 59 then tt.wMinute = 59
   if tt.wSecond > 59 then tt.wSecond = 59
   Function =tt 
End Function
Property Class_DateTimePicker.DateTimeStr() As String            '返回/设置时间
   Dim tt As SYSTEMTIME
   DateTime_GetSystemtime(hWndControl, @tt)
   Return tt.wYear & "-" & tt.wMonth & "-" & tt.wDay & " " & tt.wHour & ":" & tt.wMinute & ":" & tt.wSecond
End Property
Property Class_DateTimePicker.DateTimeStr(bValue As String)
   Dim tt As SYSTEMTIME = DateStrToSys(bValue)  DateTime_SetSystemtime(hWndControl, GDT_VALID, @tt)
End Property
Property Class_DateTimePicker.MaxDateStr() As String           '返回/设置允许最大时间。
  Dim st(1) As SYSTEMTIME
  DateTime_GetSystemtime(hWndControl, @st(0))
  Return st(1).wYear & "-" & st(1).wMonth & "-" & st(1).wDay & " " & st(1).wHour & ":" & st(1).wMinute & ":" & st(1).wSecond
End Property
Property Class_DateTimePicker.MaxDateStr( bValue As String)
  Dim tt As SYSTEMTIME = DateStrToSys(bValue)
  DateTime_SetSystemtime(hWndControl, GDTR_MAX, @tt)
End Property
Property Class_DateTimePicker.MinDateStr() As String             '返回/设置允许最小时间。
  Dim st(1) As SYSTEMTIME
  DateTime_GetSystemtime(hWndControl, @st(0))
  Return st(0).wYear & "-" & st(0).wMonth & "-" & st(0).wDay & " " & st(0).wHour & ":" & st(0).wMinute & ":" & st(0).wSecond
End Property
Property Class_DateTimePicker.MinDateStr( bValue As String)
  Dim tt As SYSTEMTIME = DateStrToSys(bValue)
  DateTime_SetSystemtime(hWndControl, GDTR_MIN, @tt)
End Property
