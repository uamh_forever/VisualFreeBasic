#define UNICODE
#INCLUDE Once "Afx/CVAR.inc"
#INCLUDE Once "Afx/CAxHost/CWebCtx.inc"
'USING Afx
Type Class_WebBrowser
   Private : 
   m_hWndForm  As HWnd '控件句柄
   hWndControl As HWND
   m_IDC       As Long '控件IDC
   Declare Function GetFP() As FormControlsPro_TYPE Ptr '返回控件保存数据的指针（一般是类自己用）
   Public : 
   Declare Property hWndForm() As .hWnd '返回/设置控件所在的窗口句柄，主要用于多开同一个窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
   Declare Property hWndForm(ByVal hWndParent As .hWnd) '获取控件所在的窗口句柄（父窗口）
   Declare Property IDC() As Long '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Property Enabled() As Boolean '返回/设置控件是否允许操作。{=.True.False}
   Declare Property Enabled(ByVal bValue As Boolean)
   Declare Property Visible() As Boolean '显示或隐藏控件。{=.True.False}
   Declare Property Visible(ByVal bValue As Boolean)
   Declare Property Tag() As CWSTR '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWSTR)
   Declare Property Left() As Long '返回/设置相对于父窗口的 X（像素）
   Declare Property Left(ByVal nLeft As Long)
   Declare Property Top() As Long '返回/设置相对于父窗口的 Y（像素）
   Declare Property Top(ByVal nTop As Long)
   Declare Property Width() As Long '返回/设置控件宽度（像素）
   Declare Property Width(ByVal nWidth As Long)
   Declare Property Height() As Long '返回/设置控件高度（像素）
   Declare Property Height(ByVal nHeight As Long)
   Declare Sub Move(ByVal nLeft As Long = -1 ,ByVal nTop As Long = -1 ,ByVal nWidth As Long = -1 ,ByVal nHeight As Long = -1) '设置窗口位置和大小，高度、宽度=-1时不修改(全部为-1时更新位置大小)。注：虚拟控件自动感知DPI，应为100%DPI时的数值
   Declare Sub Size(ByVal nWidth As Long ,ByVal nHeight As Long) '设置控件高度、宽度
   Declare Function Advise()     As HRESULT              '在事件连接点对象和客户端的接收器之间建立连接。
   Declare Function AxHostPtr()  As CAxHost Ptr          '返回指向 CAxHost 类的指针。
   Declare Function BrowserPtr() As Afx_IWebBrowser2 Ptr '返回 指针，指向Hosting浏览器控件的 IWebBrowser2 接口的直接指针 .
   Declare Property Busy()       As BOOLEAN              '布尔值，用来检测，Cwebbrowser对象是否正在进行导航或下载。
   Declare Function CtrlID()     As Long                 '返回容器窗口的标识符 .
   Declare Property Document()   As IHTMLDocument2 Ptr   '检索活动文档的自动化对象（如果有的话）。
   Declare Function ExecWB(ByVal cmdID As OLECMDID ,ByVal cmdexecopt As OLECMDEXECOPT ,ByVal pvaIn As VARIANT Ptr = NULL ,ByVal pvaOut As VARIANT Ptr = NULL) As HRESULT '对 OLE 对象执行命令, 并使用 IOleCommandTarget 接口返回命令执行的状态。
   Declare Function Find()               As HRESULT                        '激活 "查找" 对话框。
   Declare Function GetActiveElementId() As CWSTR                          '检索活动元素的 ID (当父文档具有焦点时, 该对象具有焦点)。
   Declare Function GetBodyInnerHtml()   As CWSTR                          '返回开始标签和结束标签之间的文本和html标签的字符串。
   Declare Function GetBodyInnerText()   As CWSTR                          '返回一个字符串，在开始标签和结束标签之间不包含HTML标签的字符串
   Declare Function GoBack()             As HRESULT                        '后退浏览历史列表中的一个项目。
   Declare Function GetElementInnerHtmlById(ByRef cwsId As CWSTR) As CWSTR '检索对象的开始标签和结束标签之间的HTML
   Declare Function GetElementValueById(ByRef cwsId As CWSTR) As CWSTR   '检索并返回指定属性的属性值。
   Declare Function GoForward()       As HRESULT                           '前进，浏览在历史列表中向前的一个项目
   Declare Function GoHome()          As HRESULT                           '主页，导航到当前的主页或开始页面
   Declare FUNCTION GoSearch()        AS HRESULT                           '导航到当前的搜索引擎页面
   Declare Function hWindow()         As hWnd                              '返回OLE容器托管窗口的句柄
   Declare Function InternetOptions() As HRESULT                           '激活Internet选项对话框
   Declare Property LocationName()    As CWSTR                             '检索Microsoft Internet Explorer当前显示的资源的名称
   Declare PROPERTY LocationURL()     AS CWSTR                             '检索Microsoft Internet Explorer当前显示的资源的URL
   Declare Function Navigate(ByVal pwszUrl As Wstring Ptr ,ByVal FLAGS As VARIANT Ptr = NULL ,ByVal TargetFrameName As VARIANT Ptr = NULL ,ByVal PostData As VARIANT Ptr = NULL ,ByVal Headers As VARIANT Ptr = NULL) As HRESULT ' 导航
   Declare Function Navigate(ByVal vUrl As VARIANT Ptr ,ByVal FLAGS As VARIANT Ptr = NULL ,ByVal TargetFrameName As VARIANT Ptr = NULL ,ByVal PostData As VARIANT Ptr = NULL ,ByVal Headers As VARIANT Ptr = NULL) As HRESULT ' 导航
   Declare Function PageProperties()                       As HRESULT '激活属性对话框
   Declare Function PageSetup()                            As HRESULT '激活页面设置对话框
   Declare FUNCTION PrintPage()                            AS HRESULT '激活打印对话框
   Declare FUNCTION PrintPreview()                         AS HRESULT '激活打印预览对话框
   Declare Function QueryStatusWB(ByVal cmdID As OLECMDID) As OLECMDF ' 使用IOleCommandTarget接口的QueryStatus方法查询OLE对象的命令状态
   Declare Property ReadyState() As tagREADYSTATE                     '检索对象的就绪状态
   Declare Function Refresh() As HRESULT                              '刷新，重新加载在对象中显示的当前文件
   Declare Function Refresh2(ByVal nLevel As RefreshConstants) As HRESULT '刷新，重新加载在对象中显示的当前文件 .  和 Refresh不同， 此方法包含一个指定刷新级别的参数
   Declare Property RegisterAsBrowser() As BOOLEAN '设置或检索一个值，该值指示对象是否已注册为目标名称解析的顶级浏览器
   Declare PROPERTY RegisterAsBrowser(BYVAL bRegister AS BOOLEAN) '
   Declare Property RegisterAsDropTarget() As BOOLEAN '设置或检索一个值，该值表示对象是否被注册为导航拖放目标
   Declare Property RegisterAsDropTarget(ByVal bRegister As BOOLEAN) '
   Declare Function SaveAs() As HRESULT ' 激活保存对话框
   Declare Function SetElementFocusById(ByRef cwsId As CWSTR) As HRESULT '设置开始标签和结束标签之间的HTML。
   Declare Function SetElementInnerHtmlById(ByRef cwsId As CWSTR ,ByRef cwsHtml As CWSTR) As HRESULT '设置开始标签和结束标签之间的HTML
   Declare Function SetElementValueById(ByRef cwsId As CWSTR ,cValue As CWSTR)    As HRESULT '设置指定的ID标识符的属性的值
   Declare Function SetFocus()     As HRESULT '设置hosted文档的焦点（通常是一个html页面）。
   Declare Function SetUIHandler() As HRESULT '设置IDocHostUIHandler接口，用来自定义WebBrowser
   Declare Function ShowSource()   As HRESULT '在记事本显示页面的源代码
   Declare Property Silent(ByVal bSilent As VARIANT_BOOL) '设置或获取一个值，该值指示webbrowser对象是否可以显示对话框
   Declare Property Silent()   As VARIANT_BOOL '
   Declare FUNCTION Stop()     AS HRESULT      '取消任何挂起的导航或下载操作，并停止任何动态页面元素，如背景声音和动画
   Declare Function Unadvise() As HRESULT      '释放事件连接
   Declare Function WaitForPageLoad(ByVal dbTimeout As Double = 10)                As tagREADYSTATE '等待页面完全下载或超时过期。
   Declare Function WriteHtml(ByRef cwsHtml As CWSTR ,ByVal cr As BOOLEAN = False) As HRESULT ' 将一个或多个HTML字符串写入文档。
   Declare Property UserData(idx As Long) As Integer '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx As Long ,bValue As Integer)
   Declare Function RunJs (code As CWSTR,language As CWSTR="JavaScript")As Long '执行JS代码或其它语言，成功返回0，失败返回错误码
   '{代码不提示}
   Declare Function Create(pWindow As CWindow Ptr ,cID As LONG_PTR ,x As Long ,y As Long ,nWidth As Long ,nHeight As Long ,dwStyle As DWORD ,dwExStyle As DWORD) As CWebCtx Ptr '创建 IE 浏览器控件，若已创建，只返回旧指针，不新创建。 由VFB内部处理，用户不要搞。
   Declare Sub UnWebBrowser()  ' 当关闭窗口后需要卸载操作，有控件自动执行
   '{代码可提示}
End Type

Sub Class_WebBrowser.UnWebBrowser()  ' 当关闭窗口后需要卸载操作，有控件自动执行
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) < 16 Then Return 
      Dim pcwb As any Ptr Ptr= Cast(Any Ptr,StrPtr(fp->nData))
      If *pcwb Then 
         Dim cwb As CWebCtx Ptr =*pcwb
         Delete cwb  
      End if 
      fp->nData=""
   End If 
End Sub

Function Class_WebBrowser.GetFP() As FormControlsPro_TYPE Ptr '返回自己控件的指针
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(m_hWndForm)
   While fp
      if fp->IDC = m_IDC Then Return fp
      fp = fp->VrControls
   Wend
   
End Function
Property Class_WebBrowser.hWndForm() As .hWnd         '返回/设置控件所在的窗口句柄
      Return m_hWndForm
End Property
Property Class_WebBrowser.hWndForm(ByVal hWndParent As .hWnd)
   m_hWndForm = hWndParent
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            hWndControl = cwb->hWindow
         End if
      End if
   End If   
End Property
Property Class_WebBrowser.IDC() As Long     
   Return m_IDC
End Property
Property Class_WebBrowser.IDC(ByVal NewIDC As Long)
   m_IDC  =NewIDC
End Property
Property Class_WebBrowser.Tag() As CWSTR 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return fp->Tag
   End If
End Property
Property Class_WebBrowser.Tag(ByVal sText As CWSTR )
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->Tag = sText
   End If
End Property
Property Class_WebBrowser.Enabled() As Boolean                 '使能
  Return IsWindowEnabled(hWndControl)
End Property
Property Class_WebBrowser.Enabled(ByVal bValue As Boolean)
  EnableWindow(hWndControl, bValue)
End Property
Property Class_WebBrowser.Left() As Long                '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
  Return rc.Left
End Property
Property Class_WebBrowser.Left(ByVal nLeft As Long)
   If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl    ,@rc          '获得窗体大小
      MapWindowPoints HWND_DESKTOP ,GetParent(hWndControl) ,Cast(LPPOINT ,@rc) ,2
      SetWindowPos(hWndControl ,0 ,nLeft ,rc.top ,0 ,0 ,SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nLeft = AfxUnscaleX(nLeft)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Property Class_WebBrowser.Top() As Long     '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc     '获得窗体大小
  MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
  Return rc.Top
End Property
Property Class_WebBrowser.Top(ByVal nTop As Long)
   If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl    ,@rc          '获得窗体大小
      MapWindowPoints HWND_DESKTOP ,GetParent(hWndControl) ,Cast(LPPOINT ,@rc) ,2
      SetWindowPos(hWndControl ,0 ,rc.Left ,nTop ,0 ,0 ,SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nTop = AfxUnscaleY(nTop)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Property Class_WebBrowser.Height() As Long                  '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  Return  rc.Bottom - rc.Top
End Property
Property Class_WebBrowser.Height(ByVal nHeight As Long)
  If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl, @rc          '获得窗体大小
      SetWindowPos(hWndControl, 0, 0, 0, rc.Right - rc.Left, nHeight, SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nHeight = AfxUnscaleY(nHeight)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if      
  End If
End Property
Property Class_WebBrowser.Width() As Long
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  Return rc.Right - rc.Left
End Property
Property Class_WebBrowser.Width(ByVal nWidth As Long)
   If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl ,@rc          '获得窗体大小
      SetWindowPos(hWndControl ,0 ,0 ,0 ,nWidth ,rc.Bottom - rc.Top ,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nWidth = AfxUnscaleX(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Property Class_WebBrowser.Visible() As Boolean                 '可见
  Return IsWindowVisible(hWndControl)
End Property
Property Class_WebBrowser.Visible(ByVal bValue As Boolean)
  If bValue Then
      ShowWindow(hWndControl, SW_SHOW)
  Else
      ShowWindow(hWndControl, SW_HIDE)
  End If
End Property
Sub Class_WebBrowser.Move(ByVal nLeft As Long ,ByVal nTop As Long ,ByVal nWidth As Long ,ByVal nHeight As Long)
   if nLeft = -1 And nTop = -1 And nWidth = -1 And nHeight = -1 Then
      Dim fp As FormControlsPro_TYPE ptr = GetFP()
      if fp = 0 then Return
      nLeft   = AfxscaleX(fp->nLeft)
      nTop    = AfxscaleY(fp->nTop)
      nWidth  = AfxscaleX(fp->nWidth)
      nHeight = AfxscaleY(fp->nHeight)
   Else
      Dim rc As Rect
      GetWindowRect hWndControl ,@rc          '获得窗体大小
      If nWidth <= 0  Then nWidth  = rc.Right  - rc.Left
      If nHeight <= 0 Then nHeight = rc.Bottom - rc.Top
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nLeft   = AfxUnscaleX(nLeft)
         fp->nTop    = AfxUnscaleY(nTop)
         fp->nWidth  = AfxUnscaleY(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
         fp->nHeight = AfxUnscaleY(nHeight)
      end if
   end if
   SetWindowPos(hWndControl ,0 ,nLeft ,nTop ,nWidth ,nHeight ,SWP_NOZORDER Or SWP_NOACTIVATE)
End Sub
Sub Class_WebBrowser.Size(ByVal nWidth As Long ,ByVal nHeight As Long)
   If IsWindow(hWndControl) Then   
      SetWindowPos(hWndControl ,0 ,0 ,0 ,nWidth ,nHeight ,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nWidth  = AfxUnscaleY(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
         fp->nHeight = AfxUnscaleY(nHeight)
      end if      
   End if 
End Sub
Function Class_WebBrowser.Create(pWindow AS CWindow Ptr, cID AS LONG_PTR, x AS LONG, y AS LONG, nWidth AS LONG, nHeight AS LONG, dwStyle AS DWORD, dwExStyle AS DWORD) As CWebCtx Ptr
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) < 16 Then fp->nData = String(16, 0)
      Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
      if *pcwb Then
         Dim cwb as CWebCtx Ptr = *pcwb
         Return cwb
      End if
      Dim cwb as CWebCtx Ptr = new CWebCtx(pWindow, cID, x, y, nWidth, nHeight, dwStyle, dwExStyle)
      hWndControl = cwb->hWindow
       *pcwb = cwb
      Function = cwb
   End If
End Function
Function Class_WebBrowser.Advise() AS HRESULT '在事件连接点对象和客户端的接收器之间建立连接。
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Advise
         End if
      End if
   End If
End Function
Function Class_WebBrowser.AxHostPtr() AS CAxHost PTR '返回指向 CAxHost 类的指针。
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->AxHostPtr
         End if
      End if
   End If
End Function
Function Class_WebBrowser.BrowserPtr() AS Afx_IWebBrowser2 PTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->BrowserPtr
         End if
      End if
   End If
End Function
PROPERTY Class_WebBrowser.Busy() AS BOOLEAN
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Busy
         End if
      End if
   End If
End PROPERTY
Function Class_WebBrowser.CtrlID() AS LONG
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->CtrlID
         End if
      End if
   End If
End Function
Property Class_WebBrowser.Document() As IHTMLDocument2 Ptr
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Document
         End if
      End if
   End If
End PROPERTY
Function Class_WebBrowser.ExecWB(BYVAL cmdID AS OLECMDID, BYVAL cmdexecopt AS OLECMDEXECOPT, BYVAL pvaIn AS VARIANT PTR = NULL, BYVAL pvaOut AS VARIANT PTR = NULL) AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >= 16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->ExecWB(cmdID, cmdexecopt, pvaIn, pvaOut)
         End if
      End if
   End If
End Function
Function Class_WebBrowser.Find() AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Find
         End if
      End if
   End If
End Function
Function Class_WebBrowser.GetActiveElementId() AS CWSTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->GetActiveElementId
         End if
      End if
   End If
End Function
Function Class_WebBrowser.GetBodyInnerHtml() AS CWSTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->GetBodyInnerHtml
         End if
      End if
   End If
End Function
Function Class_WebBrowser.GetBodyInnerText() AS CWSTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->GetBodyInnerText
         End if
      End if
   End If
End Function
Function Class_WebBrowser.GoBack() AS HRESULT
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         If *pcwb Then
            Dim cwb As CWebCtx Ptr = *pcwb
            Return cwb->GoBack
         End if
      End If
   End If
End Function
Function Class_WebBrowser.GetElementInnerHtmlById(BYREF cwsId AS CWSTR) AS CWSTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp Then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->GetElementInnerHtmlById(cwsId)
         End if
      End if
   End If
End Function
Function Class_WebBrowser.GetElementValueById(ByRef cwsId As CWSTR) As CWSTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >= 16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr ,StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb As CWebCtx Ptr = *pcwb
            Dim v   As VARIANT     = cwb->GetElementValueById(cwsId)
            Return *CPtr(Wstring Ptr ,v.pbVal)
         End If
      End If
   End If
End Function
Function Class_WebBrowser.GoForward() AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->GoForward
         End if
      End if
   End If
End Function
Function Class_WebBrowser.GoHome() AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->GoHome
         End if
      End if
   End If
End Function
Function Class_WebBrowser.GoSearch() AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->GoSearch
         End if
      End if
   End If
End Function
Function Class_WebBrowser.hWindow () AS HWND
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->hWindow
         End if
      End if
   End If
End Function
Function Class_WebBrowser.InternetOptions () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->InternetOptions
         End if
      End if
   End If
End Function
PROPERTY Class_WebBrowser.LocationName () AS CWSTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->LocationName
         End if
      End if
   End If
End PROPERTY
PROPERTY Class_WebBrowser.LocationURL () AS CWSTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->LocationURL
         End if
      End if
   End If
End PROPERTY
Function Class_WebBrowser.Navigate(ByVal pwszUrl As Wstring Ptr,ByVal FLAGS As VARIANT Ptr = NULL,ByVal TargetFrameName As VARIANT Ptr = NULL,ByVal PostData As VARIANT Ptr = NULL,ByVal Headers As VARIANT Ptr = NULL) As HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Navigate(pwszUrl,Flags,TargetFrameName,PostData,Headers)
         End if
      End if
   End If
End Function
Function Class_WebBrowser.Navigate (ByVal vUrl As VARIANT Ptr,ByVal FLAGS As VARIANT Ptr = NULL,ByVal TargetFrameName As VARIANT Ptr = NULL,ByVal PostData As VARIANT Ptr = NULL,ByVal Headers As VARIANT Ptr = NULL) As HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As Any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Navigate(vUrl,FLAGS,TargetFrameName,PostData,Headers)
         End if
      End if
   End If
End Function
Function Class_WebBrowser.PageProperties () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->PageProperties
         End if
      End if
   End If
End Function
Function Class_WebBrowser.PageSetup () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->PageSetup
         End if
      End if
   End If
End Function
Function Class_WebBrowser.PrintPage () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->PrintPage
         End if
      End if
   End If
End Function
Function Class_WebBrowser.PrintPreview () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->PrintPreview
         End if
      End if
   End If
End Function
Function Class_WebBrowser.QueryStatusWB (BYVAL cmdID AS OLECMDID) AS OLECMDF
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->QueryStatusWB(cmdID)
         End if
      End if
   End If
End Function
PROPERTY Class_WebBrowser.ReadyState () AS tagREADYSTATE
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->ReadyState
         End if
      End if
   End If
End PROPERTY
Function Class_WebBrowser.Refresh () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Refresh
         End if
      End if
   End If
End Function
Function Class_WebBrowser.Refresh2 (BYVAL nLevel AS RefreshConstants) AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Refresh2(nLevel)
         End if
      End if
   End If
End Function
PROPERTY Class_WebBrowser.RegisterAsBrowser () AS BOOLEAN
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->RegisterAsBrowser
         End if
      End if
   End If
End PROPERTY
PROPERTY Class_WebBrowser.RegisterAsBrowser (BYVAL bRegister AS BOOLEAN)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            cwb->RegisterAsBrowser = bRegister
         End if
      End if
   End If
End PROPERTY
PROPERTY Class_WebBrowser.RegisterAsDropTarget () AS BOOLEAN
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->RegisterAsDropTarget
         End if
      End if
   End If
End PROPERTY
PROPERTY Class_WebBrowser.RegisterAsDropTarget (BYVAL bRegister AS BOOLEAN)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            cwb->RegisterAsDropTarget = bRegister
         End if
      End if
   End If
End PROPERTY
Function Class_WebBrowser.SaveAs () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->SaveAs
         End if
      End if
   End If
End Function
Function Class_WebBrowser.SetElementFocusById (BYREF cwsId AS CWSTR) AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->SetElementFocusById(cwsId)
         End if
      End if
   End If
End Function
Function Class_WebBrowser.SetElementInnerHtmlById (BYREF cwsId AS CWSTR,BYREF cwsHtml AS CWSTR ) AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As Any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         If *pcwb Then
            Dim cwb As CWebCtx Ptr = *pcwb
            Return cwb->SetElementInnerHtmlById(cwsId,cwsHtml)
         End If
      End If
   End If
End Function
Function Class_WebBrowser.SetElementValueById(ByRef cwsId As CWSTR ,cValue As CWSTR) As HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >= 16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr ,StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb    As CWebCtx Ptr = *pcwb
            Dim vValue As VARIANT 
            vValue.bstrVal = cValue.BSTR 
            vValue.vt =8
            Return cwb->SetElementValueById(cwsId ,vValue)
         End if
      End If
   End If
End Function
Function Class_WebBrowser.SetFocus () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->SetFocus
         End if
      End if
   End If
End Function
Function Class_WebBrowser.SetUIHandler () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->SetUIHandler
         End if
      End if
   End If
End Function
Function Class_WebBrowser.ShowSource () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->ShowSource
         End if
      End if
   End If
End Function
PROPERTY Class_WebBrowser.Silent(BYVAL bSilent AS VARIANT_BOOL)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >= 16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            cwb->Silent = bSilent
         End if
      End if
   End If
End PROPERTY
PROPERTY Class_WebBrowser.Silent() AS VARIANT_BOOL
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >= 16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Silent
         End if
      End if
   End If
End PROPERTY
Function Class_WebBrowser.Stop () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Stop
         End if
      End if
   End If
End Function
Function Class_WebBrowser.Unadvise () AS HRESULT
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->Unadvise
         End if
      End if
   End If
End Function
Function Class_WebBrowser.WaitForPageLoad (BYVAL dbTimeout AS DOUBLE = 10 ) AS tagREADYSTATE
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->WaitForPageLoad(dbTimeout)
         End if
      End if
   End If
End Function
Function Class_WebBrowser.WriteHtml (BYREF cwsHtml AS CWSTR,BYVAL cr AS BOOLEAN = FALSE ) AS HRESULT
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      if Len(fp->nData) >=16 Then
         Dim pcwb As any Ptr Ptr = Cast(Any Ptr, StrPtr(fp->nData))
         if *pcwb Then
            Dim cwb as CWebCtx Ptr = *pcwb
            Return cwb->WriteHtml(cwsHtml,cr)
         End if
      End if
   End If
End Function
Property Class_WebBrowser.UserData(idx As Long) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return 0
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      Return fp->UserData(idx)
   End If
   
End Property
Property Class_WebBrowser.UserData(idx AS LONG, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp Then
      fp->UserData(idx) = bValue
   End If
End Property
Function Class_WebBrowser.RunJs(code As CWSTR ,language As CWSTR="JavaScript") As  Long  
   Dim pwindow As IHTMLWindow2 Ptr ,pvarRet As VARIANT
   Dim doc     As IHTMLDocument2 Ptr = This.Document
   doc->lpVtbl->get_parentWindow(doc ,@pwindow)
   Return pwindow->lpVtbl->execScript(pwindow ,code ,language ,@pvarRet)
End Function























