Type Class_Picture Extends Class_Control
    Declare Property Icon() As hIcon         '返回/设置 关联的图标句柄，必须具有SS_ICON样式控件才有效。
    Declare Property Icon(nIcon As hIcon)
    Declare Property Image() As HBITMAP         '返回/设置 关联的位图BMP句柄，必须具有SS_BITMAP样式控件才有效。
    Declare Property Image(nBitmap As hBitmap)
    Declare Sub DeleteIcon()   '删除与静态控件关联的图标
    Declare Sub DeleteImage()  '删除与静态控件关联的位图
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_Picture.Icon() As hIcon         '返回/设置 关联的图标句柄，必须具有SS_ICON样式控件才有效。
  Return Cast(hIcon, SendMessage(hWndControl, STM_GETICON, 0, 0)) 
End Property
Property Class_Picture.Icon(nIcon As hIcon)
  PostMessage(hWndControl, STM_SETICON, CPtr(wParam, nIcon), 0)
End Property
Property Class_Picture.Image() As hBitmap         '返回/设置 关联的位图BMP句柄，必须具有SS_BITMAP样式控件才有效。
  Return Cast(hBitmap, SendMessage(hWndControl, STM_GETIMAGE, 0, 0))
End Property
Property Class_Picture.Image(nBitmap As hBitmap)
  PostMessage(hWndControl, STM_SETIMAGE, CPtr(wParam, nBitmap), 0)
End Property
Sub Class_Picture.DeleteIcon()
  Static_DeleteIcon hWndControl
End Sub
Sub Class_Picture.DeleteImage()
  Static_DeleteImage hWndControl,IMAGE_BITMAP
End Sub
