'控件类
#include Once "..\mCtrlPublic\expand.bi"
Type Class_mCtrlExpand Extends Class_Control
    Declare Sub SetCollapsedSize(w As Long ,h As Long)   '指定折叠状态的父级客户区的大小。
    Declare Sub GetCollapsedSize(ByRef w As Long ,ByRef h As Long)   '获取折叠状态的父级客户区的大小。
    Declare Sub SetExpandedSize(w As Long ,h As Long)   '指定展开状态的父级客户区的大小。
    Declare Sub GetExpandedSize(ByRef w As Long ,ByRef h As Long)   '获取展开状态的父级客户区的大小。
    Declare Property Expand() As Long               '返回/设置 当前状态(展开=TRUE或折叠=FALSE){=.TRUE.FALSE}
    Declare Property Expand(v As Long )

End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Sub Class_mCtrlExpand.SetCollapsedSize(w As Long ,h As Long) 
  SendMessage(hWndControl, MC_EXM_SETCOLLAPSEDSIZE, 0, MAKELPARAM(w,h) ) 
End Sub
Sub Class_mCtrlExpand.GetCollapsedSize(ByRef w As Long, ByRef h As Long)  
   Dim r As LRESULT = SendMessage(hWndControl, MC_EXM_GETCOLLAPSEDSIZE, 0, 0) 
   w=GET_X_LPARAM(r)   h=GET_Y_LPARAM(r) 
End Sub
Sub Class_mCtrlExpand.SetExpandedSize(w As Long ,h As Long) 
  SendMessage(hWndControl, MC_EXM_SETEXPANDEDSIZE, 0, MAKELPARAM(w,h) ) 
End Sub
Sub Class_mCtrlExpand.GetExpandedSize(ByRef w As Long, ByRef h As Long)  
   Dim r As LRESULT = SendMessage(hWndControl, MC_EXM_GETEXPANDEDSIZE, 0, 0) 
   w=GET_X_LPARAM(r)
   h=GET_Y_LPARAM(r) 
End Sub
Property Class_mCtrlExpand.Expand() As Long
  Return SendMessage(hWndControl, MC_EXM_ISEXPANDED, 0, 0) 
End Property
Property Class_mCtrlExpand.Expand(v As Long )
  SendMessage(hWndControl, MC_EXM_EXPAND, v, MC_EXE_NOANIMATE) 
End Property




