Type Class_Image  Extends Class_Virtual '属于虚拟控件，是直接画在窗口上的 ，所有私有属性记录，保存在主窗口中，
    Declare Property Picture() As CWSTR        '返回/设置在控件中显示的图像，1：是文件（带路径全称的文件），就读文件显示 2：图像库里的资源名称，显示资源图像
    Declare Property Picture(sText As CWSTR )
    Declare Property Stretch() As Long      '返回/设置是否调整图像大小来适应控件大小{=.0 - 自动适应.1 - 原始大小.2 - 宽度适应.3 - 高度适应.4 - 宽度高度调整}
    Declare Property Stretch(bValue As Long) 
    Declare Property Percent() As Long                 '调光百分比（1-99）
    Declare Property PERCENT(ByVal bValue As Long)
    Declare Property GrayScale() As Boolean                 '转换为灰度{=.True.False}
    Declare Property GrayScale(ByVal bValue As Boolean)
    Declare Sub Drawing(gg as yGDI, hWndForm As hWnd, nBackColor As Long)  '描绘控件，窗口需要描绘，通知类把控件画到 gg 中。（** VFB系统自用）
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_Image.Picture() As CWSTR
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      Return fp->nText
   End If
End Property
Property Class_Image.Picture(sText As CWSTR)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->nText = sText
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Image.Stretch() As Long  ' 样式结构：&H12345678  12调光   6适应 78 选项 &H1=允许 &H2=显示 &H4= 灰度
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H00000F00) Shr 8
   End If
End Property
Property Class_Image.Stretch(bValue As Long )
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 4 then sy = 4
      fp->Style = (fp->Style And &HFFFFF0FF) Or (sy Shl 8)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Image.Percent() As Long  ' 样式结构：&H12345678  12调光   6适应 78 选项 &H1=允许 &H2=显示 &H4= 灰度
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &HFF000000) Shr 24
   End If
End Property
Property Class_Image.PERCENT(bValue As Long )
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 4 then sy = 4
      fp->Style = (fp->Style And &H00FFFFFF) Or (sy Shl 24)   
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Image.GrayScale() As Boolean  ' 样式结构：&H12345678  12调光   6适应 78 选项 &H1=允许 &H2=显示 &H4= 灰度
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style and &H4 )<>0
   End If
End Property
Property Class_Image.GrayScale(bValue As Boolean )
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      If bValue Then fp->Style Or= &H4 Else fp->Style And= Not &H4  
      AfxRedrawWindow m_hWndParent
   End If
End Property
Sub Class_Image.Drawing(gg As yGDI ,hWndForm As hWnd ,nBackColor As Long)
   m_hWndParent = hWndForm
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp = 0                  then return
   if (fp->Style and &H2) = 0 Then Return
   Dim As Long nStretch = (fp->Style And &H00000F00) Shr 8 ,g = (fp->Style And &H4) ,s = ((fp->Style And &HFF000000) Shr 24)
   If Len(fp->nText) = 0 Then
      
   Else
      Dim nBmp As Wstring * 260 = fp->nText
      Dim As Long x ,y ,w ,h ,ww ,hh ,iw ,ih
      Dim nico As HICON   ,gp As Long
      if Mid(nBmp ,2 ,1) = ":" Then
         gg.LoadImgFile nBmp ,s ,g ,nBackColor
      Else
         If .Left(nBmp ,7) = "BITMAP_" Then
            gg.LoadbmpRes App.HINSTANCE ,nBmp ,s ,g
         ElseIf .Left(nBmp ,5) = "ICON_" Or nBmp = "AAAAA_APPICON" Then
            nico = LoadImageW(App.HINSTANCE ,nBmp ,IMAGE_ICON ,0 ,0 ,LR_DEFAULTCOLOR) '加了LR_SHARED 图标无需销毁
         Else
            gg.gpLoadImgRes(App.HINSTANCE ,nBmp)
            gp = 1
         End If
         
      End if
      ww = fp->nWidth
      hh = fp->nHeight
      If gp <> 0 Then
         GdipGetImageWidth(gg.nGpimage ,@iw) '获得图像宽和高'
         GdipGetImageHeight(gg.nGpimage ,@ih)
      elseif nico then
         Dim p As ICONINFO
         GetIconInfo nico ,@p
         Dim bm As BITMAP
         If GetObject(p.hbmColor ,SizeOf(BITMAP) ,@bm) Then
            iw = bm.bmWidth  '/ gg.m_DpiX
            ih = bm.bmHeight '/ gg.m_DpiY
         End If
         DeleteObject p.hbmColor
         DeleteObject p.hbmMask
      else
         iw = gg.m_ImgWidth  '/ gg.m_DpiX
         ih = gg.m_ImgHeight '/ gg.m_DpiY
      End If
      x = fp->nLeft : y = fp->nTop
      Select Case nStretch
         Case 0
            w = ww
            h = (ww / iw) * ih
            If h > hh Then
               w = (hh / ih) *iw
               h = hh
            End If
            x += (ww - w) / 2 : y += (hh - h) / 2
         Case 1
            x += (ww - iw) / 2 : y += (hh - ih) / 2 : w = iw : h = ih
         Case 2
            w = ww
            h = (ww / iw) * ih
            x += (ww - w) / 2 : y += (hh - h) / 2
         Case 3
            w = (hh / ih) *iw
            h = hh
            x += (ww - w) / 2 : y += (hh - h) / 2
         Case 4
            w = ww
            h = hh
      End Select
      If gp <> 0 Then
         gg.GpDrawCopyImg x ,y ,w ,h ,0 ,0 ,iw ,ih
      ElseIf nico Then '是图标
         'DrawIcon gg.m_Dc ,10,10 ,nico
         DrawIconEx gg.m_Dc ,AfxScaleX(x) ,AfxScaleY(y) ,nico ,AfxScaleX(w) ,AfxScaleY(h) ,0 ,NULL ,DI_NORMAL
         DeleteObject nico
      Else
         gg.DrawCopyImg x ,y ,w ,h ,0 ,0 ,iw ,ih
      end if
   End If
End Sub











