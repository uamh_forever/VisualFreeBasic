Type Class_UpDown Extends Class_Control

    Declare Property Value() As Long                 '返回/设置当前值
    Declare Property Value(ByVal bValue As Long)
    Declare Property Buddy() As .HWnd                 '返回/设置伙伴窗口
    Declare Property Buddy(ByVal hwndBuddy As .hWnd)
    Declare Property MaxValue() As Long                 '返回/设置最大值
    Declare Property MaxValue(ByVal bValue As Long)
    Declare Property MinValue() As Long                 '返回/设置最小值
    Declare Property MinValue(ByVal bValue As Long)
    Declare Property BuddyBase() As Long                 '返回/设置基值，确定伙伴窗口显示的十进制或十六进制数字编号。
    Declare Property BuddyBase(ByVal nBase As Long)
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------


Property Class_UpDown.Value() As Long                 '返回/设置当前值
  Return SendMessage(hWndControl, UDM_GETPOS32, 0, 0)
End Property
Property Class_UpDown.Value(ByVal bValue As Long)
  SendMessage(hWndControl, UDM_SETPOS32, 0, bValue)
End Property
Property Class_UpDown.Buddy() As .HWnd                 '返回/设置伙伴窗口
  Return Cast(.HWnd, SendMessage(hWndControl, UDM_GETBUDDY, 0, 0) )
End Property
Property Class_UpDown.Buddy(ByVal hwndBuddy As .HWnd)
  SendMessage(hWndControl, UDM_SETBUDDY, Cast(wParam,hwndBuddy), 0)
End Property
Property Class_UpDown.MaxValue() As Long                 '返回/设置最大值
   Dim As Long pLow,pHigh 
  SendMessage(hWndControl, UDM_GETRANGE32, Cast(wParam, @pLow),Cast(lParam, @pHigh))
  Return pHigh
End Property
Property Class_UpDown.MaxValue(ByVal bValue As Long)
   Dim As Long pLow,pHigh 
  SendMessage(hWndControl, UDM_GETRANGE32, Cast(wParam, @pLow),Cast(lParam, @pHigh))
  SendMessage hWndControl, UDM_SETRANGE32, pLow, bValue
End Property
Property Class_UpDown.MinValue() As Long                 '返回/设置最小值
   Dim As Long pLow,pHigh 
  SendMessage(hWndControl, UDM_GETRANGE32,  Cast(wParam, @pLow),Cast(lParam, @pHigh))
  Return pLow
End Property
Property Class_UpDown.MinValue(ByVal bValue As Long)
   Dim As Long pLow,pHigh 
  SendMessage(hWndControl, UDM_GETRANGE32,  Cast(wParam, @pLow),Cast(lParam, @pHigh))
  SendMessage hWndControl, UDM_SETRANGE32, bValue, pHigh
End Property
Property Class_UpDown.BuddyBase() As Long                 '返回/设置基值，确定伙伴窗口显示的十进制或十六进制数字编号。
  Return SendMessage(hWndControl, UDM_GETBASE, 0, 0)
End Property
Property Class_UpDown.BuddyBase(ByVal nBase As Long)
  SendMessage(hWndControl, UDM_SETBASE, nBase, 0)
End Property

