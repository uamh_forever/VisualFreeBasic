Type Class_TextBox Extends Class_Control
    Declare Property Text() As CWStr                '返回/设置控件中的文本
    Declare Property Text(ByVal sText As CWSTR)
    Declare Function GetLineCount() As Long '检索一个多行文本框中的行数。如果没有文本存在，则返回值为 1。
    Declare Function GetLine(nLine As Long) As CWStr '获取TextBox 控件某行的文本 。行的索引从零开始。
    Declare Property PasswordChar() As String                 '返回/设置编辑控件的密码字符。当设置密码字符时，将显示该字符而不是用户键入的字符。
    Declare Property PasswordChar(nPassword As String)
    Declare Sub SelClear() '清除（删除）当前所选内容。
    Declare Sub SelCopy() ' 复制当前所选内容，并放置在剪贴板。
    Declare Sub SelCut() ' 剪切当前所选内容，并放置在剪贴板。
    Declare Sub Paste() ' 粘贴剪贴板中的到当前位置，有选择将替换选择。
    Declare Sub ReplaceText(TheText As CWStr) ' 当前所选内容替换成指定的文本。
    Declare Sub Scroll(nScrollFlag As Long) ' 滚动多行文本框中的文本。,{1.SB_LINEDOWN 滚动下移一行。.SB_LINEUP 滚动一行。.SB_PAGEDOWN 滚动下移一页。.SB_PAGEUP 滚动一页。}
    Declare Property MarginLeft() As Long '返回/设置 左侧的边距
    Declare Property MarginLeft(nLeftMargin As Long)
    Declare Property MarginRight() As Long '返回/设置 右侧的边距
    Declare Property MarginRight(nRightMargin As Long)
    Declare Property Modify() As Boolean '返回/设置文本框已被修改标志。{=.True.False}
    Declare Property Modify(nFlagValue As Boolean)
    Declare Property SelStart() As Long '返回/设置当前所选内容在文本框中的起始字符位置。1个中文算1个字符
    Declare Property SelStart(Value As Long)
    Declare Property SelEnd() As Long '返回/设置文本框中当前所选内容的结束字符位置。1个中文算1个字符
    Declare Property SelEnd(Value As Long)
    Declare Function SetSel(nStartPos As Long, nEndPos As Long) As Long ' 设置选择某个范围的字符(当前所选字符)。1个中文算1个字符
    Declare Function TextLength() As Long '返回文本的长度
    Declare Property MaxLength() As Long '返回/设置可以在控件中输入最多的字符数。零为默认值。默认限制是32,767个字符
    Declare Property MaxLength(nValue As Long)    
    Declare Property Locked() As Boolean '返回/设置是否锁住内容，上锁后用户不可以修改，代码还是可以修改的{=.True 上锁，不可以编辑.False 没锁，可以编辑}
    Declare Property Locked(nValue As Boolean) 
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Property Class_TextBox.Text() As CWSTR
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_TextBox.Text(ByVal sText As CWStr)
  AfxSetWindowText  hWndControl, sText
End Property
Function Class_TextBox.GetLineCount() As Long '检索一个多行文本框中的行数。如果没有文本存在，则返回值为 1。
  Function = SendMessage(hWndControl, EM_GETLINECOUNT, 0, 0)
End Function
Function Class_TextBox.GetLine(nLine As Long) As CWStr '获取TextBox 控件某行的文本 。行的索引从零开始。
   Dim nBufferSize As Long ,LineIndex As Long 
   Dim nBuffer() As Byte
   If IsWindow(hWndControl) Then
      LineIndex = SendMessageW(hWndControl, EM_LINEINDEX, nLine, 0)   '获取多行编辑控件中指定行的第一个字符的字符索引。
      nBufferSize = SendMessageW(hWndControl, EM_LINELENGTH, LineIndex, 0) '检索编辑控件中一行的长度（以字符为单位）。
      If nBufferSize = 0 Then Return ""
      ReDim nBuffer(nBufferSize * 2 + 4)
       *CPtr(UShort Ptr, @nBuffer(0)) = nBufferSize
      SendMessageW(hWndControl, EM_GETLINE, nLine, Cast(lParam, @nBuffer(0)))
      Return *CPtr(WString Ptr, @nBuffer(0))
   Else
      Return ""
   End If
End Function
Property Class_TextBox.MaxLength() As Long                 '返回/设置当前的文本长度限制，以字符为单位。
  Return SendMessage(hWndControl, EM_GETLIMITTEXT, 0, 0)
End Property
Property Class_TextBox.MaxLength(MaxCharacters As Long)
  SendMessage hWndControl, EM_SETLIMITTEXT, MaxCharacters, 0
End Property
Property Class_TextBox.PasswordChar() As String                 '设置或删除编辑控件的密码字符。
  Return Chr(SendMessage(hWndControl, EM_GETPASSWORDCHAR, 0, 0))
End Property
Property Class_TextBox.PasswordChar(nPassword As String)
  If IsWindow(hWndControl) Then
      Dim aa As Long
      If Len(nPassword) = 0 Then aa = 0 Else aa = Asc(nPassword)
      SendMessage(hWndControl, EM_SETPASSWORDCHAR, aa, 0)
  End If
End Property
Sub Class_TextBox.SelClear() '清除（删除）当前所选内容。
  SendMessage hWndControl, WM_CLEAR, 0, 0
End Sub
Sub Class_TextBox.SelCopy() ' 复制当前所选内容，并放置在剪贴板。
  SendMessage hWndControl, WM_COPY, 0, 0
End Sub
Sub Class_TextBox.SelCut() ' 剪切当前所选内容，并放置在剪贴板。
  SendMessage hWndControl, WM_CUT, 0, 0
End Sub
Sub Class_TextBox.Paste() ' 粘贴剪贴板中的到当前位置，有选择将替换选择。
  SendMessage hWndControl, WM_PASTE, 0, 0
End Sub
Sub Class_TextBox.ReplaceText(TheText As CWSTR) ' 当前所选内容替换成指定的文本。
  SendMessage hWndControl, EM_REPLACESEL, True, Cast(lParam, TheText.vptr)
End Sub
Sub Class_TextBox.Scroll(nScrollFlag As Long) ' 滚动多行文本框中的文本。
  SendMessage hWndControl, EM_SCROLL, nScrollFlag, 0
End Sub
Property Class_TextBox.Modify() As BOOLEAN '返回/设置文本框已被修改标志。{=.True.False}
  Property = SendMessage(hWndControl, EM_GETMODIFY, 0, 0)
End Property
Property Class_TextBox.Modify(nFlagValue As Boolean)
    SendMessage(hWndControl, EM_SETMODIFY, nFlagValue, 0)
End Property
Property Class_TextBox.MarginLeft() As Long '返回/设置 左侧的边距
    Property = LoWord(SendMessage(hWndControl, EM_GETMARGINS, 0, 0))
End Property
Property Class_TextBox.MarginLeft(nLeftMargin As Long)
  SendMessage hWndControl, EM_SETMARGINS, EC_LEFTMARGIN , nLeftMargin
End Property
Property Class_TextBox.MarginRight() As Long '返回/设置 右侧的边距
  Property = HiWord(SendMessage(hWndControl, EM_GETMARGINS, 0, 0))
End Property
Property Class_TextBox.MarginRight(nRightMargin As Long)
    SendMessage hWndControl, EM_SETMARGINS, EC_RIGHTMARGIN,nRightMargin
End Property
Property Class_TextBox.SelStart() As Long '检索当前所选内容在文本框中的起始字符位置。
    Dim nStartPos As Long, nEndPos As Long
    SendMessage(hWndControl, EM_GETSEL, Cast(wParam,@nStartPos),Cast(lParam,@nEndPos))
    Property = nStartPos
End Property
Property Class_TextBox.SelStart(Value As Long)
  This.SetSel Value, This.SelEnd
End Property
Property Class_TextBox.SelEnd() As Long '检索文本框中当前所选内容的结束字符位置。
    Dim nStartPos As Long, nEndPos As Long
    SendMessage(hWndControl, EM_GETSEL, Cast(wParam,@nStartPos),Cast(lParam,@nEndPos))
  Property = nEndPos
End Property
Property Class_TextBox.SelEnd(Value As Long)
    This.SetSel This.SelStart, Value
End Property
Function Class_TextBox.SetSel(nStartPos As Long, nEndPos As Long) As Long ' 设置选择某个范围的字符。
    Function = SendMessage( hWndControl, EM_SETSEL, nStartPos, nEndPos)
End Function
Function Class_TextBox.TextLength() As Long '返回文本的长度
    Function = GetWindowTextLength(hWndControl)
End Function
Property Class_TextBox.Locked() As Boolean
    return  (AfxGetWindowStyle(hWndControl) and ES_READONLY) <>0
End Property
Property Class_TextBox.Locked(nValue As Boolean)
       SendMessage(hWndControl, EM_SETREADONLY, nValue ,0)
End Property