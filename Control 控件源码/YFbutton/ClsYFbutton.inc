Type Class_YFbutton Extends Class_Virtual '属于虚拟控件，是直接画在窗口上的 ，所有私有属性记录，保存在主窗口中，
   Private : '私有区
   mRounded        As Long '
   mIco            As Long '字体图标值，=0不显示图标， 字体名是“iconfont”，由主软件先加载字体
   mIcoColor       As Long =-1 '字体图标值颜色 保存的是 GDIplue（GDI+）颜色，带透明度 A
   mFrameColor     As Long '边框颜色,保存的是 GDIplue（GDI+）颜色，带透明度 A
   mMouseMoveColor As Long '鼠标移动时显示底色,保存的是 GDIplue（GDI+）颜色，带透明度 A
   mMouseDownColor As Long '鼠标按下时显示底色,保存的是 GDIplue（GDI+）颜色，带透明度 A
   mMouse          As Long '鼠标状态，0=什么都没发生 1=鼠标移动到控件上面 2=鼠标在控件上按下
   Public : '公共区
   Declare Property Caption() As CWSTR '返回/设置控件中的文本
   Declare Property Caption(sText As CWSTR)
   Declare Property TextAlign() As Long '返回/设置控件上显示的文本的对齐方式。{=.0 - 左对齐.1 - 居中.2 - 右对齐.3 - 中左对齐.4 - 置中.5 - 中右对齐.6 - 下左对齐.7 - 下居中.8 - 下右对齐}
   Declare Property TextAlign(bValue As Long)
   Declare Property Style() As Long '返回/设置指示控件边界的外观和行为。{=.0 - 无边框.1 - 细边框}
   Declare Property Style(bValue As Long)
   Declare Property ForeColor() As Long '返回/设置前景色：设置用：BGR(r, g, b)
   Declare Property ForeColor(nColor As Long)
   Declare Property BackColor() As Long '返回/设置背景色：设置用：BGR(r, g, b)
   Declare Property BackColor(nColor As Long)
   Declare Property ForeColorGDIplue() As ARGB '返回/设置前景色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)
   Declare Property ForeColorGDIplue(GDIplue_Color As ARGB)
   Declare Property BackColorGDIplue() As ARGB '返回/设置背景色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)
   Declare Property BackColorGDIplue(GDIplue_Color As ARGB)
   Declare Property Font() As String '返回/设置用于在控件中绘制文本的字体，格式为：字体,字号,加粗,斜体,下划线,删除线  中间用英文豆号分割，可以省略参数 默认为：宋体,9,0,0,0,0  自动响应系统DPI创建字体大小。
   Declare Property Font(nFont As String) '
   Declare Property Ico() As Long '返回/设置 字体图标值，=0不显示图标， 字体名是“iconfont”，由主软件先加载字体
   Declare Property Ico(bValue As Long)
   Declare Sub Click() '模拟用户点击了这个按钮
   Declare Property Rounded() As Long '返回/设置 圆角大小
   Declare Property Rounded(bValue As Long) '
   Declare Property FrameColorGDIplue() As ARGB '返回/设置边框颜色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)
   Declare Property FrameColorGDIplue(GDIplue_Color As ARGB)
   Declare Property FrameColor() As Long '返回/设置边框颜色：设置用：BGR(r, g, b)
   Declare Property FrameColor(nColor As Long)
   Declare Property MouseMoveColorGDIplue() As ARGB '返回/设置鼠标移动时显示底色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)
   Declare Property MouseMoveColorGDIplue(GDIplue_Color As ARGB)
   Declare Property MouseMoveColor() As Long '返回/设置鼠标移动时显示底色：设置用：BGR(r, g, b)
   Declare Property MouseMoveColor(nColor As Long)
   Declare Property MouseDownColorGDIplue() As ARGB '返回/设置鼠标按下时显示底色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)
   Declare Property MouseDownColorGDIplue(GDIplue_Color As ARGB)
   Declare Property MouseDownColor() As Long '返回/设置鼠标按下时显示底色：设置用：BGR(r, g, b)
   Declare Property MouseDownColor(nColor As Long)
   Declare Property IcoColorGDIplue() As ARGB '返回/设置字体图标值颜色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)
   Declare Property IcoColorGDIplue(GDIplue_Color As ARGB)
   Declare Property IcoColor() As Long '返回/设置字体图标值颜色：设置用：BGR(r, g, b)
   Declare Property IcoColor(nColor As Long)   
   '{代码不提示}
   Declare Function MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult '消息处理（控件自用）
   Declare Sub Drawing(gg As yGDI ,hWndForm As .hWnd ,nBackColor As Long) '描绘控件，窗口需要描绘，通知类把控件画到 gg 中。（** VFB系统自用）
End Type


Function Class_YFbutton.MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      '鼠标的坐标转换为相对控件
      Dim As Long xPos = GET_X_LPARAM(nlParam) - AfxScaleX(fp->nLeft) ,yPos = GET_Y_LPARAM(nlParam) - AfxScaleY(fp->nTop)
      Dim As Long w    = AfxScaleX(fp->nWidth) ,h = AfxScaleY(fp->nHeight)
      Dim MouseFlags As Long = Cast(Long ,nwParam)
      'MouseFlags  MK_CONTROL   MK_LBUTTON     MK_MBUTTON     MK_RBUTTON    MK_SHIFT     MK_XBUTTON1       MK_XBUTTON2
      ''           CTRL键按下   鼠标左键按下   鼠标中键按下   鼠标右键按下  SHIFT键按下  第一个X按钮按下   第二个X按钮按下
      '检查什么键按下用  If (MouseFlags And MK_CONTROL)<>0 Then CTRL键按下
      Select Case wMsg '为了快速处理，只有些鼠标消息，且鼠标在自己控件上才会有调用，其它的不会进来
         Case WM_TIMER
            '用于鼠标移出控件侦察
            Dim p As Point
            GetCursorPos @p
            Dim rc As Rect
            GetClientRect(vhWnd ,@rc)
            MapWindowPoints vhWnd ,HWND_DESKTOP ,Cast(LPPOINT ,Varptr(rc)) ,2
            xPos = AfxScaleX(fp->nLeft)
            yPos = AfxScaleY(fp->nTop)
            If p.x < rc.Left + xPos Or p.y < rc.top + yPos Or p.x > rc.Left + xPos + w Or p.y > rc.top + yPos + h Then
               KillTimer(vhWnd ,m_IDC)
               mMouse = 0
               FF_Redraw vhWnd
            end if
         Case WM_LBUTTONDOWN  ,WM_LBUTTONDBLCLK
            If mMouse <> 2 Then
               mMouse = 2
               FF_Redraw vhWnd
            End If
         Case WM_LBUTTONUP
            If mMouse = 2 Then
               '自己控件按下然后放开，完成单击
               SendMessage(vhWnd ,WM_LBUTTONDOWN,WM_USER + 101 ,nlParam)
            End If
            If mMouse <> 1 Then
               mMouse = 1
               FF_Redraw vhWnd
            End If
         Case WM_LBUTTONDBLCLK
         Case WM_RBUTTONDOWN
         Case WM_RBUTTONUP
         Case WM_RBUTTONDBLCLK
         Case WM_MOUSEMOVE
            KillTimer(vhWnd ,m_IDC) '用于鼠标移出控件侦察
            SetTimer(vhWnd ,m_IDC ,50 ,NULL)
            Dim aa As Long = 1
            '预防从控件外面按下拖动到控件中，拖动到控件中不会有任何反应
            If MouseFlags = 1 And mMouse = 2 Then
               aa = 2
            Else
               If MouseFlags <> 0 Then aa = 0
            End If
            If mMouse <> aa Then
               mMouse = aa
               FF_Redraw vhWnd
            End If
         Case WM_CONTEXTMENU
         Case WM_MOUSEWHEEL '滚轮时
            Dim WheelDelta As Long = GET_WHEEL_DELTA_WPARAM(nwParam)
            'WheelDelta 为旋转的距离，以倍数或分度表示，通常为120（具体看WIN系统设置），正值 120 表示向前旋转，负值 -120 表示向后旋转
            MouseFlags = GET_KEYSTATE_WPARAM(nwParam)
      End Select
   End If
   Function = 0
End Function
Property Class_YFbutton.Caption() As CWSTR
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      Return fp->nText
   End If
End Property
Property Class_YFbutton.Caption(sText As CWSTR)
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      fp->nText = sText
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_YFbutton.TextAlign() As Long ' "LABEL"  样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return(fp->Style And &H00FF0000) Shr 16
   End If
End Property
Property Class_YFbutton.TextAlign(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 8 then sy = 8
      fp->Style = (fp->Style And &HFF00FFFF) Or (sy Shl 16)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_YFbutton.Style() As Long
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then ' "LABEL"  样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
      Return((fp->Style And &HFF000000) Shr 24)
   End If
End Property
Property Class_YFbutton.Style(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 255 then sy = 255
      fp->Style = (fp->Style And &H00FFFFFF) Or (sy Shl 24)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_YFbutton.ForeColor() As Long 'GDI 前景色返回
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->ForeColor)
      if cc = -1 then cc = GetSysColor(COLOR_BTNTEXT)
      Return cc
   End If
End Property
Property Class_YFbutton.ForeColor(ByVal nColor As Long) 'GDI 前景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->ForeColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_YFbutton.BackColor() As Long 'GDI 背景色返回
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->BackColor)
      if cc = -1 then cc = GetSysColor(COLOR_BTNFACE)
      Return cc
   End If
End Property
Property Class_YFbutton.BackColor(ByVal nColor As Long) 'GDI 背景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->BackColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_YFbutton.ForeColorGDIplue() As ARGB 'GDI+ 前景色返回
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->ForeColor)
      if cc = 0 then
         cc = GetSysColor(COLOR_BTNTEXT)
         Dim b As UByte Ptr = Cast(Any Ptr ,@cc) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0] ,b[1] ,b[2] ,&HFF)
      End if
      Return cc
   End If
End Property
Property Class_YFbutton.ForeColorGDIplue(ByVal nColor As ARGB) 'GDI+ 前景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr ,@nColor)
      Dim c As Long      = BGRA(b[2] ,b[1] ,b[0] ,b[3]) 'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->ForeColor = c
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_YFbutton.BackColorGDIplue() As ARGB 'GDI+ 背景色返回
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->BackColor)
      If cc = 0 Then
         cc = GetSysColor(COLOR_BTNFACE)
         Dim b As UByte Ptr = Cast(Any Ptr ,@cc) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0] ,b[1] ,b[2] ,&HFF)
      End if
      Return cc
   End If
End Property
Property Class_YFbutton.BackColorGDIplue(ByVal nColor As ARGB) 'GDI+ 背景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr ,@nColor)
      Dim c As Long      = BGRA(b[2] ,b[1] ,b[0] ,b[3]) 'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->BackColor = c
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_YFbutton.Font() As String
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return fp->nFont
   End If
End Property
Property Class_YFbutton.Font(nFont As String)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->nFont = nFont
      AfxRedrawWindow m_hWndParent
   end if
End Property
Property Class_YFbutton.Rounded() As Long '返回属性
   Return mRounded
End Property
Property Class_YFbutton.Rounded(bValue As Long) '给属性赋值
   mRounded = bValue
   AfxRedrawWindow m_hWndParent
End Property
Sub Class_YFbutton.Drawing(gg As yGDI ,hWndForm As .hWnd ,nBackColor As Long)
   m_hWndParent = hWndForm
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp = 0 then return
   '样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
   if (fp->Style and &H2) = 0 Then Return
   Dim cc As Long 'mMouse          As Long '鼠标状态，0=什么都没发生 1=鼠标移动到控件上面 2=鼠标在控件上按下
   select case mMouse
      Case 0
         cc = GetCodeColorGDIplue(fp->BackColor)
      Case 1
         cc = mMouseMoveColor
      Case 2
         cc = mMouseDownColor
   End Select
   gg.gpBrush cc
   If ((fp->Style And &HFF000000) Shr 24) = 1 Then
      gg.GpPen 1 ,mFrameColor
   Else
      gg.GpPen 0
   End If
   gg.GpDrawCircleFrame fp->nLeft ,fp->nTop ,fp->nWidth ,fp->nHeight ,mRounded ,mRounded
   
   cc = GetCodeColorGDI(fp->ForeColor)
   If cc <> -1 Then gg.SetColor cc
   
   'Windows使用带有dt_wordbreak和dt_expandtabs参数的drawtext绘制带有ss_left、cc_center和ss_right样式的标签。其他样式必须使用dt_单线，而不是换行。
   Dim dpi As Single = gg.Dpi
   Dim As Long lWrapMode = DT_SINGLELINE
   Dim As Long lFormat ,tx ,ty ,ww = fp->nWidth *dpi ,hh = fp->nHeight *dpi
   Dim oFont As HGDIOBJ = SelectObject(gg.m_Dc ,gFLY_GetFontHandles(fp->nFont)) '用完必须马上还原原来的，不然会这个字体被 yGDI 销毁，后面就无法使用这个字体了
   Dim twh As SIZE
   GetTextExtentPoint32W gg.m_Dc ,fp->nText ,Len(fp->nText) ,@twh
   gg.dpi = 1
   
   Dim ff As Long ,fz As Single '算出图标字体大小的比例
   ff = InStr(fp->nFont ,",")
   If ff Then
      Dim ss As String = Mid(fp->nFont ,ff + 1)
      ff = InStr(ss ,",")
      If ff Then
         fz = ValInt(.Left(ss, ff -1))
      Else
         fz = ValInt(ss)
      End If
      fz= fz/9
   Else
      fz =1
   End If
   If mIco Then '有图标
      twh.cx += 18*dpi * fz 
   End If 
   
   If twh.cx > ww - 4 *dpi Then twh.cx = ww -4 *dpi
   If twh.CY > hh -4 *dpi  Then twh.CY = hh -4 *dpi
   lFormat = DT_CENTER Or DT_VCENTER
   Select Case (fp->Style And &H00FF0000) Shr 16
      Case 7 '下居中
         'lFormat = DT_CENTER Or DT_BOTTOM
         tx = (ww - twh.cx) / 2
         ty = hh            - twh.CY -2 *dpi
      Case 6 '下左对齐
         'lFormat = DT_LEFT Or DT_BOTTOM
         tx = 2 *dpi
         ty = hh - twh.CY -2 *dpi
      Case 8 ' 下右对齐
         'lFormat = DT_RIGHT Or DT_BOTTOM
         tx = ww - twh.cx -2 *dpi
         ty = hh - twh.CY -2 *dpi
      Case 4 '置中
         'lFormat = DT_CENTER Or DT_VCENTER
         tx = (ww - twh.cx) / 2
         ty = (hh - twh.CY) / 2
      Case 3 '中左对齐
         'lFormat = DT_LEFT Or DT_VCENTER
         tx = 2 *dpi
         ty = (hh - twh.CY) / 2
      Case 5 '中右对齐
         'lFormat = DT_RIGHT Or DT_VCENTER
         tx = ww - twh.cx -2 *dpi
         ty = (hh - twh.CY) / 2
      Case 1 '居中
         'lFormat   = DT_CENTER Or DT_TOP
         'lWrapMode = DT_WORDBREAK
         tx = (ww - twh.cx) / 2
         ty=2 *dpi
      Case 0 ' 左对齐
         'lFormat   = DT_LEFT Or DT_TOP
         'lWrapMode = DT_WORDBREAK
         tx = 2 *dpi
         ty=2 *dpi         
      Case 2 '右对齐
         'lFormat   = DT_RIGHT Or DT_TOP
         'lWrapMode = DT_WORDBREAK
         tx = ww - twh.cx -2 *dpi
         ty=2 *dpi
   End Select
   lFormat Or= DT_NOPREFIX
   lFormat Or= DT_WORD_ELLIPSIS
   lFormat = lFormat Or lWrapMode Or DT_EXPANDTABS
   If mIco Then '有图标
      tx += 18 *dpi * fz 
      twh.cx -=18 *dpi * fz 
   End If   
   gg.DrawTexts fp->nLeft *dpi + tx ,fp->nTop *dpi + ty ,twh.cx ,twh.CY ,fp->nText ,lFormat
   SelectObject(gg.m_Dc ,oFont) '用完必须马上还原原来的，不然会这个字体被 yGDI 销毁，后面就无法使用这个字体了
   If mIco Then '有图标
      tx -= 18 *dpi * fz 
      If mIcoColor <> -1 Then 
         gg.SetColor mIcoColor
      end If 
      gg.Font "iconfont" ,11 *dpi * fz
      gg.DrawTexts fp->nLeft *dpi + tx ,fp->nTop *dpi + ty ,18*dpi * fz  ,twh.CY ,WChr(mIco) ,lFormat
      
   End If   
   
   gg.dpi = dpi '必须还原，不然后会影响到后面虚拟控件
   
   
End Sub
Property Class_YFbutton.FrameColor() As Long 'GDI 前景色返回
   Dim b As UByte Ptr = Cast(Any Ptr ,@mFrameColor) 'GDI+颜色到GDI颜色转换
   Return BGR(b[2] ,b[1] ,b[0])
End Property
Property Class_YFbutton.FrameColor(ByVal nColor As Long) 'GDI
   Dim b As UByte Ptr = Cast(Any Ptr ,@nColor) 'GDI颜色到GDI+颜色转换
   mFrameColor = RGBA(b[0] ,b[1] ,b[2] ,&HFF)
   AfxRedrawWindow m_hWndParent
End Property
Property Class_YFbutton.FrameColorGDIplue() As ARGB '返回/设置边框颜色
   Return mFrameColor
End Property
Property Class_YFbutton.FrameColorGDIplue(GDIplue_Color As ARGB)
   mFrameColor = GDIplue_Color
   AfxRedrawWindow m_hWndParent
End Property
Property Class_YFbutton.MouseMoveColor() As Long 'GDI 前景色返回
   Dim b As UByte Ptr = Cast(Any Ptr ,@mMouseMoveColor) 'GDI+颜色到GDI颜色转换
   Return BGR(b[2] ,b[1] ,b[0])
End Property
Property Class_YFbutton.MouseMoveColor(ByVal nColor As Long) 'GDI
   Dim b As UByte Ptr = Cast(Any Ptr ,@nColor) 'GDI颜色到GDI+颜色转换
   mMouseMoveColor = RGBA(b[0] ,b[1] ,b[2] ,&HFF)
End Property
Property Class_YFbutton.MouseMoveColorGDIplue() As ARGB '返回/设置边框颜色
   Return mMouseMoveColor
End Property
Property Class_YFbutton.MouseMoveColorGDIplue(GDIplue_Color As ARGB)
   mMouseMoveColor = GDIplue_Color
End Property
Property Class_YFbutton.MouseDownColor() As Long 'GDI 前景色返回
   Dim b As UByte Ptr = Cast(Any Ptr ,@mMouseDownColor) 'GDI+颜色到GDI颜色转换
   Return BGR(b[2] ,b[1] ,b[0])
End Property
Property Class_YFbutton.MouseDownColor(ByVal nColor As Long) 'GDI
   Dim b As UByte Ptr = Cast(Any Ptr ,@nColor) 'GDI颜色到GDI+颜色转换
   mMouseDownColor = RGBA(b[0] ,b[1] ,b[2] ,&HFF)
End Property
Property Class_YFbutton.MouseDownColorGDIplue() As ARGB '返回/设置边框颜色
   Return mMouseDownColor
End Property
Property Class_YFbutton.MouseDownColorGDIplue(GDIplue_Color As ARGB)
   mMouseDownColor = GDIplue_Color
End Property
Property Class_YFbutton.Ico() As Long '返回属性 
    Return mIco
End Property
Property Class_YFbutton.Ico(bValue As Long) '给属性赋值
   mIco = bValue
   AfxRedrawWindow m_hWndParent
End Property
Property Class_YFbutton.IcoColor() As Long 'GDI 前景色返回
   Dim b As UByte Ptr = Cast(Any Ptr ,@mIcoColor) 'GDI+颜色到GDI颜色转换
   Return BGR(b[2] ,b[1] ,b[0])
End Property
Property Class_YFbutton.IcoColor(ByVal nColor As Long) 'GDI
   Dim b As UByte Ptr = Cast(Any Ptr ,@nColor) 'GDI颜色到GDI+颜色转换
   mIcoColor = RGBA(b[0] ,b[1] ,b[2] ,&HFF)
   AfxRedrawWindow m_hWndParent
End Property
Property Class_YFbutton.IcoColorGDIplue() As ARGB '返回/设置边框颜色
   Return mIcoColor
End Property
Property Class_YFbutton.IcoColorGDIplue(GDIplue_Color As ARGB)
   mIcoColor = GDIplue_Color
   AfxRedrawWindow m_hWndParent
End Property
Sub Class_YFbutton.Click() '模拟用户点击了这个按钮
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      '鼠标的坐标转换为相对控件
      Dim As Long xPos = AfxScaleX(fp->nLeft+1) ,yPos = AfxScaleY(fp->nTop+1)   
      SendMessage(m_hWndParent ,WM_LBUTTONUP ,WM_USER + 101 ,MAKELPARAM(xPos ,yPos))
   End If 
End Sub

