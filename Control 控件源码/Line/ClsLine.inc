Type Class_Line   Extends Class_Virtual '属于虚拟控件，是直接画在窗口上的 ，所有私有属性记录，保存在主窗口中，

    Declare Property Style() As Long      '返回/设置设置线条样式{=.0 - 顶部.1 - 左边.2 - 右边.3 - 底部.4 - 上斜.5 - 下斜}
    Declare Property Style(bValue As Long) 
    Declare Property BorderColor() As Long        '返回/设置线条色彩： RGB 颜色值(用 BGR(r, g, b) 获取) 
    Declare Property BorderColor(nColor As Long)
    Declare Property BorderColorGDIplue() As ARGB          '返回/设置线条色彩：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)  
    Declare Property BorderColorGDIplue(GDIplue_Color As ARGB)
    Declare Property BorderWidth() As Long      '返回/设置边框宽度，0 到 15  ，0为不显示
    Declare Property BorderWidth(bValue As Long) 
    Declare Property ArrowStartW() As Long      '返回/设置线条前面的剪头的宽度，0 到 15  ，0为不显示
    Declare Property ArrowStartW(bValue As Long) 
    Declare Property ArrowStartH() As Long      '返回/设置线条前面的剪头的高度，0 到 15  ，0为不显示
    Declare Property ArrowStartH(bValue As Long) 
    Declare Property ArrowEndW() As Long      '返回/设置线条后面的剪头的宽度，0 到 15  ，0为不显示
    Declare Property ArrowEndW(bValue As Long) 
    Declare Property ArrowEndH() As Long      '返回/设置线条后面的剪头的高度，0 到 15  ，0为不显示
    Declare Property ArrowEndH(bValue As Long) 
    Declare Sub Drawing(gg as yGDI,hWndForm As hWnd,nBackColor As Long)  '描绘控件，窗口需要描绘，通知类把控件画到 gg 中。（** VFB系统自用）
End Type
 '----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_Line.Style() As Long  '样式结构：&H12345678  1主样式 2线条 3前W 4前H 5后W 6后H   78 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &HF0000000) Shr 28
   End If
End Property
Property Class_Line.Style(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 5 then sy = 5
      fp->Style = (fp->Style And &H0FFFFFFF) Or (sy Shl 28)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Line.BorderColor() As Long    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->ForeColor)
      if cc = -1 then cc = GetSysColor(COLOR_BTNTEXT)    
      Return cc      
   End If
End Property
Property Class_Line.BorderColor(ByVal nColor As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->ForeColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明       
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Line.BorderColorGDIplue() As ARGB    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->ForeColor)
      if cc = 0 then
         cc = GetSysColor(COLOR_BTNTEXT)
         Dim b As UByte Ptr = Cast(Any Ptr, @cc ) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0], b[1], b[2], &HFF)  
      End if
      Return cc      
   End If
End Property
Property Class_Line.BorderColorGDIplue(ByVal nColor As ARGB)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr, @nColor )
      Dim c As Long = BGRA(b[2], b[1], b[0], b[3])  'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->ForeColor = c      
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Line.BorderWidth() As Long  '样式结构：&H12345678  1主样式 2线条 3前W 4前H 5后W 6后H   78 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H0F000000) Shr 24
   End If
End Property
Property Class_Line.BorderWidth(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 15 then sy = 15
      fp->Style = (fp->Style And  &HF0FFFFFF) Or (sy Shl 24)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Line.ArrowStartW() As Long  '样式结构：&H12345678  1主样式 2线条 3前W 4前H 5后W 6后H   78 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H00F00000) Shr 20
   End If
End Property
Property Class_Line.ArrowStartW(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 15 then sy = 15
      fp->Style = (fp->Style And  &HFF0FFFFF) Or (sy Shl 20)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Line.ArrowStartH() As Long  '样式结构：&H12345678  1主样式 2线条 3前W 4前H 5后W 6后H   78 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H000F0000) Shr 16
   End If
End Property
Property Class_Line.ArrowStartH(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 15 then sy = 15
      fp->Style = (fp->Style And  &HFFF0FFFF) Or (sy Shl 16)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Line.ArrowEndW() As Long  '样式结构：&H12345678  1主样式 2线条 3前W 4前H 5后W 6后H   78 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H0000F000) Shr 12
   End If
End Property
Property Class_Line.ArrowEndW(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 15 then sy = 15
      fp->Style = (fp->Style And  &HFFFF0FFF) Or (sy Shl 12)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Line.ArrowEndH() As Long  '样式结构：&H12345678  1主样式 2线条 3前W 4前H 5后W 6后H   78 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H00000F00) Shr 8
   End If
End Property
Property Class_Line.ArrowEndH(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 15 then sy = 15
      fp->Style = (fp->Style And  &HFFFFF0FF) Or (sy Shl 8)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Sub Class_Line.Drawing(gg as yGDI, hWndForm As hWnd, nBackColor As Long)
   
   m_hWndParent = hWndForm
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp = 0 then return 
   if (fp->Style and &H2 ) =0 Then Return 
   Dim As Long nStyles = (fp->Style And &HF0000000) Shr 28,nBorderWidth = (fp->Style And &H0F000000) Shr 24 , _
      nArrowStartW = (fp->Style And &H00F00000) Shr 20, nArrowStartH = (fp->Style And &H000F0000) Shr 16, _
      nArrowEndW = (fp->Style And &H0000F000) Shr 12, nArrowEndH = (fp->Style And &H00000F00) Shr 8, x, y, w, h, x2, y2
   gg.GpPen nBorderWidth, GetCodeColorGDIplue(fp->ForeColor)
   x = fp->nLeft : y = fp->nTop : w = fp->nWidth : h = fp->nHeight
   Select Case nStyles   '0 - 顶部,1 - 左边,2 - 右边,3 - 底部,4 - 上斜,5 - 下斜
      Case 0
         x2 = x + w : y2 = y
      Case 1
         x2 = x : y2 = y + h
      Case 2
         x = x + w : x2 = x : y2 = y + h
      Case 3
         y = y + h : x2 = x + w : y2 = y
      Case 4
         x2 = x + w : y2 = y + h
      Case 5
         y = y + h : x2 = x + w : y2 = y - h
   End Select
   gg.GpArrowCap nArrowEndW, nArrowEndH, True, nArrowStartW, nArrowStartH
   gg.GpDrawLine x, y, x2, y2
End Sub







