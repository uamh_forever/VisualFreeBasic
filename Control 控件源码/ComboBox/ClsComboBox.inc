Type Class_ComboBox Extends Class_Control
Protected : 
   nMaxCharacters As Long  '控件句柄
Public :    
   Declare Property Text() As CWSTR                 '返回/设置控件中的文本
   Declare Property Text(ByVal sText As CWSTR)
   Declare Sub Clear()                                  '删除组合框中所有项目。
   Declare Function AddItem(sText As CWSTR,DataValue as Integer =0) As Long  '新增项目，返回新添加的索引（从0开始）,失败返回CB_ERR
   Declare Function InsertItem(nIndex As Long, TheText As CWSTR) As Long  '插入项目，返回新添加的索引（从0开始）,失败返回CB_ERR
   Declare Property List(nIndex As Long) As CWSTR        '返回/设置项目文本
   Declare Property List(nIndex As Long, sText As CWSTR)
   Declare Function ListCount() As Long  '检索组合框列表框中的项目数。
   Declare Property ListIndex() As Long                  '返回/设置项目
   Declare Property ListIndex(nIndex As Long)
   Declare Property ItemData(nIndex As Long) As Integer  '返回/设置指定项目在组合框中关联的值。
   Declare Property ItemData(nIndex As Long, nValue As Integer)
   Declare Function RemoveItem(nIndex As Long) As Long  '删除一个项目，返回剩余项目数
   Declare Function GetTopIndex() As Long  '检索组合框的列表框部分中第一个可见项目的从零开始的索引。
   Declare Function FindString(indexStart As Long, lpszFind As CWSTR) As Long  '搜索以指定字符串中的字符开头的项目。如果搜索不成功，则为CB_ERR。
   Declare Function FindStringExact(indexStart As Long, lpszFind As CWSTR) As Long  '查找包含指定字符串在组合框中的第一个项目。。如果搜索不成功，则为CB_ERR。
   Declare Function FindItemData(indexStart As Long, nData As Integer) As Long  '查找包含指定项目数据在组合框中的第一个项目。。如果搜索不成功，则为CB_ERR。
   Declare Property ReDraw() As Boolean         '设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
   Declare Property ReDraw(nValue As Boolean)    '返回不使用
   Declare Property ItemHeight() As Long  '返回/设置每行的高度，单位像素，如：AfxScaleY(15)，可变行高自绘控件时无效
   Declare Property ItemHeight(nValue As Long)
   Declare Property LabelHeight() As Long  '返回/设置标签的高度，单位像素，如：AfxScaleY(15)，可变行高自绘控件时无效
   Declare Property LabelHeight(nValue As Long)
   Declare Property MaxLength() As Long '返回/设置可以在控件中输入最多的字符数。零为默认值。默认限制是32,767个字符
   Declare Property MaxLength(nValue As Long)
   
End Type

Property Class_ComboBox.Text() As CWSTR
   Return AfxGetWindowText(hWndControl)
End Property
Property Class_ComboBox.Text(ByVal sText As CWSTR)
   AfxSetWindowText hWndControl, sText
End Property
Sub Class_ComboBox.Clear() '删除组合框中所有项目。
   SendMessage hWndControl, CB_RESETCONTENT, 0, 0
End Sub
Function Class_ComboBox.AddItem(sText As CWSTR, DataValue as Integer = 0) As Long  '新增项目，返回新添加的索引（从0开始）
  Dim nIndex As Long = SendMessageW(hWndControl, CB_ADDSTRING, 0, Cast(lParam, sText.vptr))
  if DataValue then SendMessage(hWndControl, CB_SETITEMDATA, nIndex, DataValue)
  Function = nIndex
End Function
Function Class_ComboBox.InsertItem(nIndex As Long, TheText As CWSTR) As Long  '插入项目，返回新添加的索引（从0开始）
   Function = SendMessageW(hWndControl, CB_INSERTSTRING, nIndex, Cast(lParam, TheText.vptr ))
End Function
Property Class_ComboBox.MaxLength() As Long                 '返回/设置当前的文本长度限制，以字符为单位。
  Return nMaxCharacters
End Property
Property Class_ComboBox.MaxLength(MaxCharacters As Long)
  nMaxCharacters = MaxCharacters
  SendMessage hWndControl, CB_LIMITTEXT, MaxCharacters, 0
End Property
Property Class_ComboBox.List(nIndex As Long) As CWSTR        '返回/设置项目文本
   Dim nBufferSize As Long
   Dim nBuffer() As UByte
   '检查窗口句柄是否有效
   If IsWindow(hWndControl) Then
      '得到的文本的长度
      nBufferSize = SendMessage(hWndControl, CB_GETLBTEXTLEN, nIndex, 0)
      If (nBufferSize = 0) Or (nBufferSize = CB_ERR) Then
         Return ""
      End If
      ReDim nBuffer(nBufferSize * 2 + 4)
      ''发消息给控件
      If SendMessageW(hWndControl, CB_GETLBTEXT, nIndex, Cast(lParam, @nBuffer(0))) = CB_ERR Then
         Return ""
      Else
         'Remove the Null
         'nBuffer = RTrim(nBuffer, Chr(0))
         Return *CPtr(WString Ptr,@nBuffer(0))
      End If
   End If
End Property
Property Class_ComboBox.List(nIndex As Long, sText As CWSTR)
   If IsWindow(hWndControl) Then
      Dim i As Long = SendMessage(hWndControl, CB_GETCURSEL, 0, 0)
      Dim nValue As Integer = SendMessage(hWndControl, CB_GETITEMDATA, nIndex, 0)
      SendMessageW(hWndControl, CB_DELETESTRING, nIndex, 0)
      SendMessageW(hWndControl, CB_INSERTSTRING, nIndex, Cast(lParam, sText.vptr))
      SendMessage(hWndControl, CB_SETITEMDATA, nIndex, nValue)
      if i<>-1 Then SendMessage(hWndControl, CB_SETCURSEL, i, 0)
   End If
End Property
Function Class_ComboBox.ListCount() As Long  '检索组合框列表框中的项目数。
   Function = SendMessage(hWndControl, CB_GETCOUNT, 0, 0)
End Function
Property Class_ComboBox.ListIndex() As Long                  '返回/设置项目
   Property = SendMessage(hWndControl, CB_GETCURSEL, 0, 0)
End Property
Property Class_ComboBox.ListIndex(nIndex As Long)
   SendMessage(hWndControl, CB_SETCURSEL, nIndex, 0)
End Property
Property Class_ComboBox.ItemData(nIndex As Long) As Integer         '返回/设置指定项目在组合框中关联的 32 位值
   Property = SendMessage(hWndControl, CB_GETITEMDATA, nIndex, 0)
End Property
Property Class_ComboBox.ItemData(nIndex As Long, nValue As Integer)
   SendMessage(hWndControl, CB_SETITEMDATA, nIndex, nValue)
End Property
Function Class_ComboBox.GetTopIndex() As Long  '检索组合框的列表框部分中第一个可见项目的从零开始的索引。
   Function = SendMessage(hWndControl, CB_GETTOPINDEX, 0, 0)
End Function
Function Class_ComboBox.FindString(indexStart As Long, lpszFind As CWSTR) As Long  '搜索以指定字符串中的字符开头的项目
   Function = SendMessageW(hWndControl, CB_FINDSTRING, indexStart, Cast(lParam, lpszFind.vptr ))
End Function
Function Class_ComboBox.FindStringExact(indexStart As Long, lpszFind As CWSTR) As Long  '查找包含指定字符串在组合框中的第一个项目。
   Function = SendMessageW(hWndControl, CB_FINDSTRINGEXACT, indexStart, Cast(lParam, lpszFind.vptr))
End Function
Function Class_ComboBox.FindItemData(indexStart As Long, nData As Integer) As Long  '查找包含指定项目数据在组合框中的第一个项目。
   Function = SendMessage(hWndControl, CB_FINDSTRING, indexStart, nData)
End Function
Property Class_ComboBox.ReDraw() As Boolean         '返回/设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
   Property = False '无需返回
End Property
Property Class_ComboBox.ReDraw(nValue As Boolean)
   SendMessage(hWndControl, WM_SETREDRAW, nValue, 0)
End Property
Property Class_ComboBox.ItemHeight() As Long  '返回/设置每行的高度，单位像素，如：AfxScaleY(15)
   Property = SendMessage(hWndControl, CB_GETITEMHEIGHT, 0, 0)
End Property
Property Class_ComboBox.ItemHeight(nValue As Long)
   SendMessage(hWndControl, CB_SETITEMHEIGHT, 0, nValue)
End Property
Function Class_ComboBox.RemoveItem(nIndex As Long) As Long  '删除一个项目，返回剩余项目数
   Function = SendMessage(hWndControl, CB_DELETESTRING, nIndex, 0)
End Function
Property Class_ComboBox.LabelHeight() As Long  
   Property = SendMessage(hWndControl, CB_GETITEMHEIGHT, -1, 0)
End Property
Property Class_ComboBox.LabelHeight(nValue As Long)
   SendMessage(hWndControl, CB_SETITEMHEIGHT, -1, nValue)
End Property