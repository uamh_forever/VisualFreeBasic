
Type Class_Menu
   Private : 
   sWndParent As hWnd
   m_IDC      As Long
   Declare Function GetFP() As FormControlsPro_TYPE ptr '返回控件保存数据的指针（一般是类自己用）
   Declare Function AddIconToMenuItem(ByVal nMenu As .HMENU ,ByVal uItem As ULong ,ByVal fByPosition As BOOLEAN ,ByVal HICON As HICON ,ByVal fAutoDestroy As BOOLEAN = True ,ByVal phbmp As HBITMAP Ptr = NULL) As BOOLEAN '将图标关联到菜单
   Declare Sub TextOutImag(yMenu As .HMENU ,cID As UInteger ,tFont As HFONT ,nText As Long ,hColor As Long ,ByVal uFlags As UINT = MF_BYCOMMAND ,ByVal phbmp As HBITMAP Ptr = NULL) '
   Public : 
   Declare Property HMENU() As .HMENU '返回/设置 菜单的主句柄，就是根级别的菜单句柄 ,创建新菜单 =CreatePopupMenu()
   Declare Property hMenu(hMenuNew As .hMenu)
   Declare Property hWndForm() As hWnd '返回/设置控件所在的窗口句柄，主要用于多开窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
   Declare Property hWndForm(ByVal hWndParent As hWnd) '获取控件所在的窗口句柄
   Declare Property Text(idItem As Long) As CWSTR '返回/设置菜单项的文本，字符个数最多50个，多余将会截断
   Declare Property Text(idItem As Long ,sText As CWSTR)
   Declare Property Enabled(idItem As Long) As BOOLEAN '返回/设置菜单项是否允许操作。{=.True.False}
   Declare Property Enabled(idItem As Long ,bValue As BOOLEAN)
   Declare Property Check(idItem As Long) As BOOLEAN '返回/设置菜单项是否选中。{=.True.False}
   Declare Property Check(idItem As Long ,bValue As BOOLEAN)
   Declare Function DeleteItem(idItem As Long) As BOOLEAN '删除一个菜单项，成功返回True， 如果菜单项有子菜单，则此函数将销毁子菜单的句柄，并释放子菜单使用的内存。{=.True.False}
   Declare Sub DeleteAll() '删除所有菜单项并释放菜单使用的内存，以及清理控件数据类，而DeleteItem只删除菜单项不清理控件数据类。
   Declare Function PopupMenu(hWndParent As hWnd = 0 ,x As Long = 0 ,y As Long = 0) As Long '在当前鼠标位置，弹出此菜单，hWndParent 是接收菜单事件的窗口。返回点击的菜单项标识符。
   Declare Function AddMenu(fidItem As Long ,sText As CWSTR ,idItem As Long ,nBmp As String = "" ,nEnabled As BOOLEAN = True ,nCheck As BOOLEAN = False ,nDraw As BOOLEAN = False ,nKey As String = "") As Long '在fidItem 下增加1个菜单项，fidItem=0 为根菜单，文本字符个数最多50个，多余将会截断 等于"-"是分割线，idItem必须>0 而且不能重复，成功返回True，失败返回False{=.True.False}
   Declare Property IDC() As Long '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Function GetItemhMenu(idItem As Long)                         As .HMENU '返回当前菜单项所属于的菜单句柄，由于菜单是树叉结构，子菜单有自己的句柄。失败方法0
   Declare Function GetItemSubhMenu(idItem As Long ,bNew As BOOLEAN = 0) As .HMENU '返回当前菜单项子菜单的句柄。无子菜单返回0。当无子菜单时 bNew=True 将会创建个子菜单。  {2.True.False}
   Declare Property UserData(idx As Long) As Integer '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx As Long ,bValue As Integer)
   '{代码不提示}
   Declare Sub UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
End Type

'菜单为虚拟控件，数据保存在  采用数据链  New Class_Menu_Data_s 产生
Type Class_Menu_Data_s
   zMenu    As .HMENU '菜单句柄，此条菜单是属于那个菜单句柄的，方便使用
   nPos     As Long   '位置，在菜单中的位置，从0开始，从上到下
   idItem   As ULong  '是菜单的 WID
   nEnabled As Byte   '
   nCheck   As Byte
   nDraw    As Byte
   Bmph     As HBITMAP '需要卸载时销毁
   BmpT     As ZString * 31 '在图像管理器中的名称 IMAGE_***  zString 格式，保存31个英文字符，超过截断
   nText    As Wstring * 51 '显示的文字，wString格式 最多50个字符，超过截断，英文 “-”为分割线
   nKey     As ZString * 16 '快捷键保存15个英文字符
   xmds     As Class_Menu_Data_s Ptr '下一个数据链，=0表示没了
End Type
'fp->CtlData(0)   'hMenu 菜单总句柄
'fp->CtlData(1)  '第一个数据链


'菜单API
'CreateMenu 创建一个菜单
'CreatePopupMenu 创建一个下拉菜单
'DeleteMenu  从指定菜单中删除项目
'RemoveMenu 删除菜单项或从指定菜单分离子菜单。
'DestroyMenu 销毁指定的菜单并释放该菜单占用的所有内存。
'GetMenu  检索分配给指定窗口的菜单的句柄。
'GetSubMenu   检索由指定菜单项激活的下拉菜单或子菜单的句柄。
'SetMenu   将新菜单分配给指定的窗口。
'AppendMenuA   将新项目追加到指定菜单栏，下拉菜单，子菜单或快捷菜单的末尾。
'InsertMenuItem 在菜单中的指定位置插入新菜单项。
'InsertMenu     将新菜单项插入菜单，将其他项下移菜单。
'ModifyMenu   更改现有菜单项。
'SetMenuItemBitmaps
'GetMenuBarInfo    检索有关指定菜单栏的信息。
'DrawMenuBar  重新绘制指定窗口的菜单栏
'GetMenuItemInfo 检索有关菜单项的信息。

Sub Class_Menu.UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      Dim sMenu As .HMENU = Cast(.HMENU ,fp->CtlData(0))
      fp->CtlData(0) = 0
      If sMenu Then
         DestroyMenu sMenu
         sMenu = 0
      End If
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->Bmph Then DeleteObject met->Bmph
         med = met->xmds
         Delete met
      Loop
      fp->CtlData(1) = 0
   End If
End Sub

Property Class_Menu.HMENU() As .HMENU                    '句柄
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sMenu As .HMENU = Cast(.HMENU ,fp->CtlData(0))
      Return sMenu
   End if
End Property
Property Class_Menu.HMENU(ByVal hWndNew As .HMENU)        '句柄
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp Then
      Dim sMenu As .HMENU = Cast(.HMENU ,fp->CtlData(0))
      If sMenu Then
         DestroyMenu sMenu
         sMenu = 0
      End If
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->Bmph Then DeleteObject met->Bmph
         med = met->xmds
         Delete met
      Loop   
      fp->CtlData(1)=0   
      fp->CtlData(0) =Cast(Integer ,hWndNew)
   End If
End Property
Property Class_Menu.hWndForm() As.hWnd         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  Return sWndParent
End Property
Property Class_Menu.hWndForm(ByVal hWndParent As .hWnd)         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
'  hWndControl = GetMenu(hWndParent)
  sWndParent = hWndParent
End Property
Function Class_Menu.GetFP() As FormControlsPro_TYPE ptr '返回自己控件的指针
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(sWndParent)
   While fp
      If fp->IDC = m_IDC Then Return fp      
      fp = fp->VrControls
   Wend

End Function
Property Class_Menu.IDC() As Long    '返回/设置控件IDC，控件标识符，
      Return m_IDC
End Property
Property Class_Menu.IDC(NewIDC As Long)
   m_IDC = NewIDC
End Property
Property Class_Menu.Text(idItem As Long) As CWSTR    '按ID方式:返回/设置菜单项的文本
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp Then
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->idItem=idItem Then Return met->nText
         med = met->xmds
      Loop      
   End If 
   
End Property
Property Class_Menu.Text(idItem As Long, sText As CWSTR)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp Then
      If fp->CtlData(0) =0 Then Return 
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->idItem = idItem Then 
            met->nText = sText
            Dim mType As MENUITEMINFOW
            mType.cbSize = SizeOf(mType)
            mType.fMask = MIIM_STRING
            mType.dwTypeData = Cast(LPWSTR, sText.vptr)
            mType.cch = Len(sText)
            SetMenuItemInfoW met->zMenu ,met->nPos ,True ,Varptr(mType)     
            Exit Do        
         End If      
         med = met->xmds
      Loop       
   End if
End Property

Property Class_Menu.Enabled(idItem As Long) As Boolean     '按ID方式:返回/设置菜单项是否允许操作。{=.True.False}
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->idItem=idItem Then Return met->nEnabled
         med = met->xmds
      Loop 
   End if 
End Property
Property Class_Menu.Enabled(idItem As Long, bValue As Boolean)
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      If fp->CtlData(0) =0 Then Return 
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->idItem = idItem Then 
            met->nEnabled = bValue
            Dim mType As MENUITEMINFO
            mType.cbSize = SizeOf(mType)
            mType.fMask = MIIM_STATE
            mType.fState = IIf(bValue, MFS_ENABLED, MFS_DISABLED)
            If met->nCheck Then mType.fState Or=MFS_CHECKED
            'MFS_ENABLED, MFS_GRAYED, MFS_DISABLED, MFS_CHECKED, MFS_UNCHECKED
            'MFS_DEFAULT, MFS_HILITE, MFS_UNHILITE
            SetMenuItemInfo(met->zMenu, met->nPos, True, Varptr(mType))   
            Exit Do        
         End If      
         med = met->xmds
      Loop  
   End if
End Property

Property Class_Menu.Check(idItem As Long) As BOOLEAN     '按ID方式:返回/设置菜单项是否选中。{=.True.False}
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->idItem=idItem Then Return met->nCheck
         med = met->xmds
      Loop  
   End if 
End Property
Property Class_Menu.Check(idItem As Long, bValue As BOOLEAN)
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      If fp->CtlData(0) =0 Then Return 
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->idItem = idItem Then 
            met->nCheck = bValue
            Dim mType as MenuItemInfo
            mType.cbSize = SizeOf(mType)
            mType.fMask = MIIM_STATE
            mType.fState = IIf(bValue, MFS_CHECKED, MFS_UNCHECKED)
            If met->nEnabled = 0 Then mType.fState Or= MFS_DISABLED
            'MFS_ENABLED, MFS_GRAYED, MFS_DISABLED, MFS_CHECKED, MFS_UNCHECKED
            'MFS_DEFAULT, MFS_HILITE, MFS_UNHILITE
            SetMenuItemInfo(met->zMenu, met->nPos, True, Varptr(mType))   
            Exit Do        
         End If      
         med = met->xmds
      Loop 
   End If
End Property
Function Class_Menu.PopupMenu(hWndParent As hWnd = 0 ,x As Long = 0 ,y As Long = 0) As Long '在当前鼠标位置，弹出此菜单，hWndParent 是接收菜单事件的窗口。
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      If fp->CtlData(0) = 0 Then Return 0
      Dim sMenu As .HMENU = Cast(.HMENU ,fp->CtlData(0))
      if x = 0 and y = 0 then
         Dim P As Point
         GetCursorPos @p
         x = P.x
         y = P.y + AfxScaleY(10)
      End if
      if hWndParent = 0 Then hWndParent = sWndParent
      Dim id As Long = TrackPopupMenu(sMenu ,TPM_RETURNCMD Or TPM_NONOTIFY ,x ,y ,0 ,hWndParent ,NULL) '比方说弹出菜单
      PostMessage(hWndParent ,WM_Command ,id ,0) '默认为 PostMessageW
      Function = id
   End if
End Function
Function Class_Menu.GetItemSubhMenu(idItem As Long, bNew As BOOLEAN = 0 ) As .HMENU   '返回当前菜单项子菜单的句柄。无子菜单返回0。当无子菜单时 bNew=True 将会创建个子菜单。  {2.True.False}
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      Dim sMenu As .HMENU = Cast(.HMENU ,fp->CtlData(0))
      Dim zMenu As .HMENU
      Dim i As Long, u As Long
      If idItem = 0 Then
         If sMenu = 0 Then 
            sMenu = CreatePopupMenu
            fp->CtlData(0) = Cast(Integer ,sMenu)
         End If 
         zMenu = sMenu
      Else
         Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
         Dim met As Class_Menu_Data_s Ptr
         Do
            If med = 0 Then Exit Do
            met = med
            If met->idItem = idItem Then 
               zMenu = GetSubMenu(met->zMenu, met->nPos)
               If zMenu = 0 And bNew<>0 Then
                  zMenu = CreatePopupMenu
                  Dim uFlags As ULong = MF_BYPOSITION Or MF_POPUP
                  If met->nCheck Then uFlags Or= MF_CHECKED
                  If met->nEnabled Then uFlags Or= MF_ENABLED Else uFlags Or= MF_DISABLED
                  If met->nDraw Then uFlags Or= MF_OWNERDRAW
                  ModifyMenuW met->zMenu, met->nPos, uFlags, Cast(UINT_PTR, zMenu), met->nText
                  If met->Bmph Then SetMenuItemBitmaps(zMenu, met->nPos, MF_BYPOSITION, met->Bmph, NULL)
               End if
               Exit Do                
            End If      
            med = met->xmds
         Loop  
      End If
      Function = zMenu
   End if 
End Function

Function Class_Menu.AddMenu(fidItem As Long ,stext As CWSTR ,idItem As Long ,nBmp As String = "" ,nEnabled As BOOLEAN = True ,nCheck As BOOLEAN = False ,nDraw As BOOLEAN = False ,nKey As String = "") As Long
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp = 0 Then Return False
   Dim sMenu As .HMENU = Cast(.HMENU ,fp->CtlData(0))
   If sMenu = 0 Then
      sMenu = CreatePopupMenu
      fp->CtlData(0)=Cast(Integer ,sMenu)
   End If 
   Dim zMenu As .HMENU
   Dim i     As Long ,u As Long
   Dim med   As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
   Dim met   As Class_Menu_Data_s Ptr
   If idItem = 0 Then Return False
   '检查 idItem 是否重复，不可以有重复
   Do
      If med = 0 Then Exit Do
      met = med
      If met->idItem = idItem Then
         Return False
         Exit Do
      End If
      med = met->xmds
   Loop
   
   med = Cast(Any Ptr ,fp->CtlData(1))
   '添加菜单项目 ------------------
   If fidItem = 0 Then
      zMenu = sMenu
   Else
      Do
         If med = 0 Then Exit Do
         met = med
         If met->idItem = fidItem Then
            zMenu = GetSubMenu(met->zMenu ,met->nPos)
            If zMenu = 0 Then
               zMenu = CreatePopupMenu
               Dim uFlags As ULong = MF_BYPOSITION Or MF_POPUP
               If met->nCheck   Then uFlags Or= MF_CHECKED
               If met->nEnabled Then uFlags Or= MF_ENABLED Else uFlags Or= MF_DISABLED
               If met->nDraw    Then uFlags Or= MF_OWNERDRAW
               ModifyMenuW met->zMenu ,met->nPos ,uFlags ,Cast(UINT_PTR ,zMenu) ,met->nText
               If met->Bmph Then SetMenuItemBitmaps(zMenu ,met->nPos ,MF_BYPOSITION ,met->Bmph ,NULL)
            End If
            Exit Do
         End If
         med = met->xmds
      Loop
   End If
   '新增菜单数据 ---------------
   med = Cast(Any Ptr ,fp->CtlData(1))
   met = 0
   If med = 0 Then
      met            = New Class_Menu_Data_s
      fp->CtlData(1) = Cast(Integer ,met)
   Else
      Do
         If med->xmds = 0 Then
            met       = New Class_Menu_Data_s
            med->xmds = met
            Exit Do
         End If
         met = med
         med = met->xmds
         met = 0
      Loop
   End If
   If met = 0 Then Return False '虽然理论上不可能发生，但怕发生意外。
   med = Cast(Any Ptr ,fp->CtlData(1))
   '统计使用个数，求出位置
   u = 0
   Do
      If med = 0            Then Exit Do
      If med->zMenu = zMenu Then u += 1
      Dim med2 As Class_Menu_Data_s Ptr = med
      med = med2->xmds
   Loop
   met->zMenu    = zMenu
   met->nPos     = u
   met->idItem   = idItem
   met->nEnabled = nEnabled
   met->nCheck   = nCheck
   met->nDraw    = nDraw
   met->nText    = stext
   met->nKey     = nKey
   met->BmpT     = nBmp
   Dim uFlags As ULong = MF_BYPOSITION
   If stext = "-"   Then uFlags Or= MF_SEPARATOR Else uFlags Or= MF_STRING
   If met->nCheck   Then uFlags Or= MF_CHECKED
   If met->nEnabled Then uFlags Or= MF_ENABLED Else uFlags Or= MF_DISABLED
   If met->nDraw Then uFlags Or= MF_OWNERDRAW
   If Len(nKey) Then
      Function = AppendMenuW(zMenu ,uFlags ,idItem ,met->nText & WChr(9) & nKey)
   Else
      Function = AppendMenuW(zMenu ,uFlags ,idItem ,met->nText)
   End If
   If Len(nBmp) Then
      Dim nIcon As HICON
      Dim ffi   As Long = InStr(nBmp ,"|")
      Dim tBmp  As String
      If ffi > 0 Then tBmp = Mid(nBmp ,ffi + 1) Else tBmp = nBmp
      If Left(tBmp ,7) = "RCDATA_" Then
         nIcon = AfxGdipIconFromRes(App.HINSTANCE ,tBmp)
      Elseif Left(tBmp ,7) = "BITMAP_" Then
         met->Bmph = LoadImageA(App.HINSTANCE ,tBmp ,IMAGE_BITMAP ,GetSystemMetrics(SM_CXSMICON) ,GetSystemMetrics(SM_CYSMICON) ,LR_DEFAULTCOLOR)
         SetMenuItemBitmaps(zMenu ,u ,MF_BYPOSITION ,met->Bmph ,NULL)
      ElseIf Left(tBmp ,5) = "ICON_" Or tBmp = "AAAAA_APPICON" Then
         nIcon = LoadImageA(App.HINSTANCE ,tBmp ,IMAGE_ICON ,GetSystemMetrics(SM_CXSMICON) ,GetSystemMetrics(SM_CYSMICON) ,LR_DEFAULTCOLOR Or LR_VGACOLOR) ' LoadIcon(app.hInstance, tBmp) '从资源里加载图标
      ElseIf UCase(Left(nBmp ,ffi -1)) = "ICONFONT" Then
         Dim sFon As CWSTR ,sC As Long ,sZ As Long
         ffi = InStr(tBmp ,"|")
         if ffi = 0 Then
            sFon = tBmp
         Else
            sFon = left(tBmp ,ffi -1)
            tBmp = Mid(tBmp ,ffi + 1)
            ffi  = InStr(tBmp ,"|")
            If ffi = 0 Then
               sC = ValInt(tBmp)
            Else
               sC = ValInt(left(tBmp ,ffi -1))
               sZ = ValInt(Mid(tBmp ,ffi + 1))
            End if
         End if
         Dim tFont As HFONT ,nIsVista As Long
         Dim vi    as OSVERSIONINFO
         vi.dwOsVersionInfoSize = SizeOf(OSVERSIONINFO)
         GetVersionEx @vi
         If vi.dwPlatformId = VER_PLATFORM_WIN32_NT Then
            If vi.dwMajorVersion > 5 Then
               nIsVista = -1
            End If
         End If
         If nIsVista Then
            If AfxScaleX(1) = 1 Then
               tFont = AfxCreateFont(sFon ,12 , -1)
            Else
               tFont = AfxCreateFont(sFon ,11 , -1)
            End If
         Else
            tFont = AfxCreateFont(sFon ,10 , -1)
         End If
         TextOutImag(zMenu ,u ,tFont ,sZ ,sC ,True ,@met->Bmph)
         DeleteObject tFont
      End If
      If nIcon Then AddIconToMenuItem(zMenu ,u ,True ,nIcon ,True ,@met->Bmph)
   End If
   '获取快捷键
   If Len(nKey) Then
      Dim pWindow As CWindow Ptr = AfxCWindowPtr(sWndParent)
      If pWindow->AccelHandle Then
         u = CopyAcceleratorTableW(pWindow->AccelHandle ,NULL ,0)
      Else
         u = 0
      End if
      ReDim cAccel(u) As ACCEL
      If u > 0 Then CopyAcceleratorTableW(pWindow->AccelHandle ,@cAccel(0) ,u)
      Dim eKey  As Long
      Dim fVirt As Long   = FNOINVERT Or FVIRTKEY
      Dim uzz   As String = UCase(nKey)
      Dim f     As Long   = InStr(uzz ,"CTRL+")
      If f > 0 Then
         fVirt Or= FCONTROL
         uzz   = Mid(uzz ,f + 5)
      End If
      f = InStr(uzz ,"SHIFT+")
      If f > 0 Then
         fVirt Or= FSHIFT
         uzz   = Mid(uzz ,f + 6)
      End If
      f = InStr(uzz ,"ALT+")
      If f > 0 Then
         fVirt Or= FALT
         uzz   = Mid(uzz ,f + 4)
      End If
      Select Case uzz
         Case "F1"  : eKey = VK_F1
         Case "F2"  : eKey = VK_F2
         Case "F3"  : eKey = VK_F3
         Case "F4"  : eKey = VK_F4
         Case "F5"  : eKey = VK_F5
         Case "F6"  : eKey = VK_F6
         Case "F7"  : eKey = VK_F7
         Case "F8"  : eKey = VK_F8
         Case "F9"  : eKey = VK_F9
         Case "F10" : eKey = VK_F10
         Case "F11" : eKey = VK_F11
         Case "F12" : eKey = VK_F12
         Case "0"   : eKey = VK_0
         Case "1"   : eKey = VK_1
         Case "2"   : eKey = VK_2
         Case "3"   : eKey = VK_3
         Case "4"   : eKey = VK_4
         Case "5"   : eKey = VK_5
         Case "6"   : eKey = VK_6
         Case "7"   : eKey = VK_7
         Case "8"   : eKey = VK_8
         Case "9"   : eKey = VK_9
         Case "A"   : eKey = VK_A
         Case "B"   : eKey = VK_B
         Case "C"   : eKey = VK_C
         Case "D"   : eKey = VK_D
         Case "E"   : eKey = VK_E
         Case "F"   : eKey = VK_F
         Case "G"   : eKey = VK_G
         Case "H"   : eKey = VK_H
         Case "I"   : eKey = VK_I
         Case "J"   : eKey = VK_J
         Case "K"   : eKey = VK_K
         Case "L"   : eKey = VK_L
         Case "M"   : eKey = VK_M
         Case "N"   : eKey = VK_N
         Case "O"   : eKey = VK_O
         Case "P"   : eKey = VK_P
         Case "Q"   : eKey = VK_Q
         Case "R"   : eKey = VK_R
         Case "S"   : eKey = VK_S
         Case "T"   : eKey = VK_T
         Case "U"   : eKey = VK_U
         Case "V"   : eKey = VK_V
         Case "W"   : eKey = VK_W
         Case "X"   : eKey = VK_X
         Case "Y"   : eKey = VK_Y
         Case "Z"   : eKey = VK_Z
      End Select
      If eKey > 0 Then
         cAccel(u).fVirt = fVirt
         cAccel(u).key   = eKey
         cAccel(u).cmd   = idItem
         If pWindow->AccelHandle Then DestroyAcceleratorTable pWindow->AccelHandle
         pWindow->AccelHandle = CreateAcceleratorTableW(@cAccel(0) ,u + 1)
      End If
   End If
End Function
Function Class_Menu.AddIconToMenuItem(ByVal nMenu As .HMENU, ByVal uItem As ULong, ByVal fByPosition As BOOLEAN, ByVal HICON As HICON, ByVal fAutoDestroy As BOOLEAN = True, ByVal phbmp As HBITMAP Ptr = NULL) As BOOLEAN
   '给菜单加图标，成功返回值为非零，失败，则返回值为零
   'nMenu  [in]处理包含菜单项目的菜单。
   'uItem    [in]获取信息的菜单项的标识符或位置。此参数的含义取决于fByPosition 的值。
   'fByPosition  [in]项目的含义.如果此参数为FALSE，则项目是一个菜单项标识符。否则，这是一个菜单项位置。
   'hIcon        [in]将图标的句柄添加到菜单中。
   'fAutoDestroy   [in]TRUE（默认值）或FALSE。如果为TRUE，AddIconToMenuItem在返回之前销毁图标。
   'phbmp          [out]图标转换为位图后的位图句柄，调用者负责销毁。如果是NULL，不输出句柄。
   
   If nMenu = NULL Or hIcon = NULL Then Return False
   DIM hbmp AS HBITMAP, sizIcon AS SIZE, rcIcon AS RECT
   sizIcon.cx = GetSystemMetrics(SM_CXSMICON)
   sizIcon.cy = GetSystemMetrics(SM_CYSMICON)
   SetRect(@rcIcon, 0, 0, sizIcon.cx, sizIcon.cy)
   '创建内存DC =============
   DIM hdcDest AS HDC = CreateCompatibleDC(NULL)
   IF hdcDest = NULL THEN RETURN FALSE
   '创建32位位图  ====================
   DIM bmi AS BITMAPINFO
   bmi.bmiHeader.biSize = SIZEOF(BITMAPINFOHEADER)
   bmi.bmiHeader.biPlanes = 1
   bmi.bmiHeader.biCompression = BI_RGB
   bmi.bmiHeader.biWidth = sizIcon.cx
   bmi.bmiHeader.biHeight = sizIcon.cy
   bmi.bmiHeader.biBitCount = 24
   hbmp = CreateDIBSection(hdcDest, @bmi, DIB_RGB_COLORS, NULL, NULL, 0)
   'hbmp = CreateCompatibleBitmap(hdcDest, sizIcon.cx, sizIcon.cy)
   '把位图于DC关联 ===============
   DIM hbmpOld AS HBITMAP = CAST(HBITMAP, SelectObject(hdcDest, hbmp))
   Dim rc As Rect
   rc.bottom = 32 : rc.right = 32
   FillRect hdcDest, @rc, GetSysColorBrush(COLOR_MENU)
   IF hbmpOld THEN
      DrawIconEx(hdcDest, 0, 0, hIcon, sizIcon.cx, sizIcon.cy, 0, NULL, DI_NORMAL)
      'dim p as ICONINFO
      'GetIconInfo hIcon, @p
      'Dim bm As BITMAP
      'If GetObject(p.hbmColor, SizeOf(BITMAP), @bm) Then
      ''iw = bm.bmWidth '/ gg.m_DpiX
      ''ih = bm.bmHeight '/ gg.m_DpiY
      'End If
      ''      DrawState hdcDest, 0, 0, p.hbmMask, 0, 0, 0, sizIcon.cx, sizIcon.cy, DST_BITMAP
      'DrawState hdcDest, 0, 0, p.hbmColor, 0, 0, 0, sizIcon.cx, sizIcon.cy, DST_BITMAP
      'DeleteObject p.hbmColor
      'DeleteObject p.hbmMask
      SelectObject(hdcDest, hbmpOld)
   END IF
   DeleteDC(hdcDest)
   '把位图于菜单关联
   FUNCTION = SetMenuItemBitmaps(nMenu, uItem, IIf(fByPosition, MF_BYPOSITION, MF_BYCOMMAND), hbmp, null)
   If fAutoDestroy Then DestroyIcon(HICON)
   IF phbmp THEN *phbmp = hbmp
   
END FUNCTION
Sub Class_Menu.TextOutImag(yMenu As .HMENU, cID As UInteger, tFont As HFONT, nText As Long, hColor As Long, ByVal uFlags As UINT = MF_BYCOMMAND, ByVal phbmp As HBITMAP Ptr = NULL)  '
   Dim U As Long, i As Long, ai As Long
   Dim As Long w, h
   ai = u + 1
   w = GetSystemMetrics(SM_CXMENUCHECK)
   h = GetSystemMetrics(SM_CYMENUCHECK)
   Dim nDC As hDC, memDC As hDC, memBM As HBITMAP, rc As Rect, oFont As HFONT
   
   nDC = GetDC(0)
   '// 创建一个兼容的位图
   memBM = CreateCompatibleBitmap(nDC, w, h)
   '// 创建兼容的设备上下文
   memDC = CreateCompatibleDC(nDC)
   '// 将位图选择到兼容的设备上下文中
   SelectObject(memDC, memBM)
   SetBkMode memDC, 1  '设置这个后，画上的字是透明的
   SetRect @rc, 0, 0, w, h
   FillRect memDC, @rc, GetSysColorBrush(COLOR_MENU)     ' 画背景，填充底色
   SetTextColor memDC, hColor      ' 文本颜色
   oFont = SelectObject(memDC, tFont)
   DrawTextW memDC, Cast(LPCWSTR, @nText), 1, @rc, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE
   
   ReleaseDC 0, nDC
   DeleteDC memDC
   If oFont Then DeleteObject oFont
   
   SetMenuItemBitmaps(yMenu, cID, uFlags,memBM, null)
   IF phbmp THEN *phbmp = memBM
End Sub

Function Class_Menu.GetItemhMenu(idItem As Long) As .hMenu
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim med As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
      Dim met As Class_Menu_Data_s Ptr
      Do
         If med = 0 Then Exit Do
         met = med
         If met->idItem=idItem Then Return met->zMenu
         med = met->xmds
      Loop 
   End If
End Function
Property Class_Menu.UserData(idx AS LONG) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return  0
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      Return fp->UserData(idx)
   End If   

End Property
Property Class_Menu.UserData(idx AS LONG, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      fp->UserData(idx) = bValue
   End If    
End Property
Function Class_Menu.DeleteItem(idItem As Long) As BOOLEAN '删除这个菜单，成功返回True。{=.True.False}
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp = 0 Then Return False
   Dim med   As Class_Menu_Data_s Ptr = Cast(Any Ptr ,fp->CtlData(1))
   Dim met   As Class_Menu_Data_s Ptr
   Dim zMenu As .HMENU ,nPos As Long
   '寻找这个菜单项的菜单句柄
   Do
      If med = 0 Then Exit Do
      met = med
      If met->idItem = idItem Then
         zMenu = met->zMenu
         met->zMenu=0
         nPos = met->nPos
         If met->Bmph Then
            DeleteObject met->Bmph
            met->Bmph = 0
         End If 
         Exit Do
      End If
      med = met->xmds
   Loop
   If zMenu = 0 Then Return False
   '将其它位置在后面的菜单项，位置减去1
   med  = Cast(Any Ptr ,fp->CtlData(1))
   Do
      If med = 0 Then Exit Do
      met = med
      If zMenu = med->zMenu Then
         If med->nPos>nPos Then med->nPos -=1
      End If
      med = met->xmds
   Loop   
   Function= DeleteMenu( zMenu,nPos,MF_BYPOSITION)
End Function
Sub Class_Menu.DeleteAll()
   This.UnLoadControl()
   if IsWindow(sWndParent) then
      Dim pWindow As CWindow Ptr = AfxCWindowPtr(sWndParent)
      If pWindow Then
         If pWindow->AccelHandle Then DestroyAcceleratorTable pWindow->AccelHandle
      End If
   End If
End Sub
      '
      'If pWindow->AccelHandle Then
       '