﻿#VisualFreeBasic_Form#  Version=5.5.6
Locked=0

[Form]
Name=customTAB
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP
Style=3 - 常规窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=选项卡控件的属性
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=475
Height=293
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=9
Top=9
Width=160
Height=188
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Line]
Name=Line1
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无箭头
ArrowStartH=0 - 无箭头
ArrowEndW=0 - 无箭头
ArrowEndH=0 - 无箭头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=2
Top=230
Width=463
Height=3
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=确定
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=335
Top=235
Width=55
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=407
Top=235
Width=55
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=0
Caption=▲
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=176
Top=43
Width=25
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=1
Caption=▼
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=176
Top=91
Width=25
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=新增
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=11
Top=201
Width=36
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=插入
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=54
Top=201
Width=36
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=131
Top=201
Width=36
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=...
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=423
Top=93
Width=29
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=标题：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=217
Top=12
Width=205
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=217
Top=31
Width=238
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=图标：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=217
Top=98
Width=187
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=True
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=217
Top=122
Width=235
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=绑定子窗口：（窗口需要设置为子窗口）
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=217
Top=147
Width=244
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[ComboBox]
Name=Combo1
Index=-1
Style=0 - 标签和下拉框
Custom=
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=False
AutoHscroll=True
MaxLength=0
Uppercase=False
Lowercase=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=217
Top=166
Width=239
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=设为当前选定的选项卡
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=217
Top=200
Width=211
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=标签数量：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=12
Top=238
Width=155
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame1
Index=-1
Caption=
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=206
Top=-5
Width=256
Height=227
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=附加数据（数值）：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=217
Top=61
Width=115
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text3
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=335
Top=59
Width=118
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False


[AllCode]
Type customTAB_type 
   minzi As CWSTR    '名称
   tuxian As CWSTR   '图像
   chuanko As CWSTR  '绑定的窗口
   xuan As Long      '选择项
   ItemData As Long   '用于ColumnEditor 
End Type    
Dim Shared customTABm() As customTAB_type ,customTABxg As Long ,xiugai As Long 

Sub customTAB_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   Me.UserData(0) = UserData
   Dim As Rect rc,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr,UserData)
   GetWindowRect(st->hWndForm , @rc2)
   GetWindowRect(st->hWndList , @rc)
   if rc.Left + Me.Width > rc2.right Then rc.Left = rc2.right - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top = rc2.bottom - Me.Height
   Me.Move rc.Left, rc.top   
   
   Dim aa As String = *st->value
   Erase customTABm
   if Len(aa) Then
      aa = YF_Replace(aa, "\|", Chr(0))
      Dim cc() As String
      Dim u As Long = vbSplit(aa, "|", cc()) -1
      Dim i As Long, xu As Long, ci As Long = -1
      i = ValInt(cc(0))
      if i > 0 Then
         if i > 999 Then i = 999
         ReDim customTABm(i)
         if u > 0 Then
            xu = ValInt(cc(1))
            for i = 2 To u -4 Step 4
               ci += 1
               customTABm(ci).minzi = Utf8toStr(YF_Replace(cc(i), Chr(0), "|"))
               if i + 1 > u Then Exit for
               customTABm(ci).tuxian = Utf8toStr(YF_Replace(cc(i + 1), Chr(0), "|"))
               if i + 2 > u Then Exit for
               customTABm(ci).chuanko = Utf8toStr(YF_Replace(cc(i + 2), Chr(0), "|"))
               if i + 3 > u Then Exit for
               customTABm(ci).ItemData = ValInt(cc(i + 3))
               customTABm(ci).xuan = xu = ci
            Next
         End if
         if ci = -1 Then
            Erase customTABm
         Else
            if ci < UBound(customTABm) Then ReDim Preserve customTABm(ci)
            for i = 0 To UBound(customTABm)
               List1.AddItem customTABm(i).minzi
            Next
         End if
         List1.ListIndex =xu 
      end if
   end if
   Label4.Caption = vfb_LangString("标签数量：") & UBound(customTABm) + 1
   Combo1.AddItem vfb_LangString("不绑定窗口")
   CurProSetChildWindow(Combo1.hWnd) '让主窗口填充子窗口属性的窗口。

   customTAB_List1_LBN_SelChange 0, 0
   customTABxg =0 
   xiugai = 0
End Sub

Sub customTAB_List1_LBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '选择了列表
   Dim i As Long = List1.ListIndex
   customTABxg = 1
   if i = -1 Then
      Command5.Enabled = False
      Command6.Enabled = False
      Command3(0).Enabled = False
      Command3(1).Enabled = False
      Text1.Enabled = False
      Text2.Enabled = False
      Text3.Enabled = False
      Command7.Enabled = False
      Combo1.Enabled = False
      Check1.Enabled = False
   Else
      Command5.Enabled = True
      Command6.Enabled = True
      Command3(0).Enabled = i > 0
      Command3(1).Enabled = i < UBound(customTABm)
      Text1.Enabled = True
      Text2.Enabled = True
      Text3.Enabled = True
      Command7.Enabled = True
      Combo1.Enabled = True
      Check1.Enabled = True
      Text1.Text = customTABm(i).minzi
      Text2.Text = customTABm(i).tuxian
      Text3.Text = customTABm(i).ItemData
      Check1.Value = customTABm(i).xuan
      Dim j As Long
      if Len(customTABm(i).chuanko) Then
         for ii As Long = 1 To Combo1.ListCount -1
            if customTABm(i).chuanko = Combo1.List(ii) Then
               j = ii
               Exit for
            end if
         Next
      End if
      Combo1.ListIndex = j
   End if
   customTABxg = 0
End Sub

Sub customTAB_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Me.Close
End Sub

Sub customTAB_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim ss As String = UBound(customTABm) + 1 & "|"
   if UBound(customTABm) > -1 Then 
      Dim i As Long, j As Long 
      for i = 0 To UBound(customTABm)
         if customTABm(i).xuan Then 
            j = i
            Exit for 
         End if       Next 
      ss &= j & "|"
      for i = 0 To UBound(customTABm)
          ss &= YF_Replace(customTABm(i).minzi.UTF8,"|","\|") & "|"  & YF_Replace(customTABm(i).tuxian.UTF8,"|","\|") & "|"  & YF_Replace(customTABm(i).chuanko.UTF8,"|","\|") & "|" & customTABm(i).ItemData &  "|" 
      Next        
   End if 
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   st->Rvalue = ss   
   xiugai =0 
   Me.Close
End Sub

Function customTAB_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   if xiugai Then
      
      Select Case MessageBox(hWndForm, vfb_LangString("选项被修改，你需要退出前保存吗？"), "VisualFreeBasic", _
               MB_YESNOCANCEL Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Case IDYES
            Command1.Click 
            Return True
         Case IDNO
            
         Case IDCANCEL
            Return True
      End Select
   End if 
   Function = FALSE ' 如果想阻止窗口关闭，则应返回 TRUE 。
End Function

Sub customTAB_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim i As Long = UBound(customTABm) + 1
   ReDim Preserve customTABm(i)
   customTABm(i).minzi = "Tab" & i + 1
   i = List1.AddItem(customTABm(i).minzi)
   List1.ListIndex = i
   customTAB_List1_LBN_SelChange 0, 0
End Sub

Sub customTAB_Text1_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if customTABxg Then Return 
   Dim i As Long = List1.ListIndex
   if i = -1 Then Return 
   customTABm(i).minzi = Text1.Text 
   List1.List(i)=customTABm(i).minzi
   xiugai =1
End Sub

Sub customTAB_Combo1_CBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '列表框中更改当前选择时
   if customTABxg Then Return
   Dim i As Long = List1.ListIndex
   if i = -1 Then Return 
   Dim ci As Long = Combo1.ListIndex
   if ci =0 Then 
      customTABm(i).chuanko = ""
   Else  
      customTABm(i).chuanko = Combo1.List(ci )
   End if 

   xiugai =1   
End Sub

Sub customTAB_Check1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if customTABxg Then Return
   Dim i As Long = List1.ListIndex
   if i = -1 Then Return 
   Dim ii As Long 
   for ii = 0 To UBound(customTABm)
      customTABm(ii).xuan = 0
   Next 
   customTABm(i).xuan =1
   xiugai =1   
End Sub

Sub customTAB_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if customTABxg Then Return
   Dim i As Long = List1.ListIndex
   if i = -1 Then Return 
   Dim aa As String = customTABm(i).tuxian 
   Dim bb As String = GetImgForm(aa,0)  '获取图像文件名称
   if aa <> bb Then 
      xiugai = 1   
      customTABm(i).tuxian = bb 
      Text2.Text =customTABm(i).tuxian
   End if 
   
End Sub


Sub customTAB_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if customTABxg Then Return 
   Dim i As Long = List1.ListIndex
   if i = -1 Then Return 
   Dim ii As Long = UBound(customTABm) + 1
   ReDim Preserve customTABm(ii)
   for ii = UBound(customTABm) To i + 1 Step -1
      customTABm(ii) = customTABm(ii -1)     
   Next 
   customTABm(i).minzi = "Tab" & UBound(customTABm) + 1
   customTABm(i).chuanko = ""
   customTABm(i).tuxian = ""
   customTABm(i).xuan = 0
   List1.Clear
   for ii = 0 To UBound(customTABm)
      List1.AddItem customTABm(ii).minzi  
   Next 
   List1.ListIndex = i
   customTAB_List1_LBN_SelChange 0, 0  
   xiugai =1
End Sub

Sub customTAB_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if customTABxg Then Return
   
   Select Case MessageBox(hWndForm, vfb_LangString("真的要删除吗？"), "VisualFreeBasic", _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON2 Or MB_APPLMODAL)
      Case IDYES
      Case IDNO
         Return
   End Select
   
   Dim i As Long = List1.ListIndex
   if i = -1 Then Return
   List1.RemoveItem i
   Dim u As Long = UBound(customTABm)
   if u > 0 Then
      Dim ii As Long
      for ii = i To u -1
         customTABm(ii) = customTABm(ii + 1)
      Next
      ReDim Preserve customTABm(u -1)
   Else
      Erase customTABm
   End if
   customTAB_List1_LBN_SelChange 0, 0
   xiugai = 1
End Sub

Sub customTAB_Text3_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if customTABxg Then Return 
   Dim i As Long = List1.ListIndex
   if i = -1 Then Return 
   customTABm(i).ItemData =ValInt(Text3.Text)

   xiugai =1
End Sub

Sub customTAB_Command3_BN_Clicked(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim aa As Long = List1.ListIndex, bb As Long
   if aa = -1 Then Return
   if ControlIndex = 0 Then
      if aa < 1 Then Return
      bb = aa -1
   Else
      if aa >= List1.ListCount -1 Then Return
      bb = aa + 1
   End if
   Swap customTABm(aa), customTABm(bb)
   List1.List(aa)=customTABm(aa).minzi 
   List1.List(bb)=customTABm(bb).minzi 
   List1.ListIndex = bb
   
   
End Sub




















