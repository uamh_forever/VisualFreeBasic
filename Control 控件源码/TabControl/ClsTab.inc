Type Class_TabControl Extends Class_Control
   Declare Function AddTab(TabText As CWSTR, lParam As lParam = 0,iImage As Long =-1) As Long  '添加或在选项卡(文字,附加数,图像索引)，如果成功，则返回新选项卡的索引，否则返回-1。
   Declare Function InsertTab(Index As Long, TabText As CWSTR, lParam As lParam = 0,iImage As Long =-1) As Long    '添加或在选项卡(插入点,文字,附加数,图像索引)，如果成功，则返回新选项卡的索引，否则返回-1。
   Declare Property Text(Index As Long) As CWSTR                 '返回/设置标签名称（索引从0开始）
   Declare Property Text(Index As Long, TabText As CWSTR)
   Declare Property Focus() As Long                 '返回/设置具有焦点项的索引。具有焦点的项可能不同于所选项。
   Declare Property Focus(Index As Long)
   Declare Property Selected() As Long                 '返回/设置当前选定的选项卡，如果成功，返回其索引的所选的选项卡，如果没有选择则选项卡返回-1。
   Declare Property Selected(Index As Long)
   Declare Function GetCount() As Long                '返回选项卡数，如果不成功则为返回零。
   Declare Function RemoveAll() As Boolean               '删除所有选项卡，如果成功返回 TRUE 否则返回 FALSE。
   Declare Function remove(Index As Long) As Boolean    '删除选项卡，如果成功返回 TRUE 否则 FALSE 。
   Declare Sub SetItemSize(w As Long, h As Long)  '设置选项卡控件选项卡的高度和宽（用于TCM_SETITEMSIZE），W H 已支持高DPI
   Declare Sub SetPadding(w As Long, h As Long)  '设置每个标签的图标和标签中的标签控件的水平和垂直填充量，W H 已支持高DPI
   Declare Property ItemData(Index As Long) As lParam         '返回/设置附加数值
   Declare Property ItemData(Index As Long, nParam As lParam)
   Declare Property BindWindow(Index As Long) As .hWnd     '返回/设置选项卡绑定窗口，绑定后窗口自动响应大小和切换。
   Declare Property BindWindow(Index As Long, nWnd As .HWND)
   Declare Property ImageList() As hImageList                 '返回/设置与TabControl关联的ImageList的控件。(销毁控件后不会自动销毁ImageList)
   Declare Property ImageList(nImageList As hImageList)
   Declare Property ItemImage(Index As Long) As Long '返回/设置 TAB Index 索引的 图像索引
   Declare Property ItemImage(Index As Long, iImage As Long)  
   Declare Sub SetSize() '调整绑定窗口位置。 
End Type

Function Class_TabControl.AddTab(TabText As CWSTR, nlParam As lParam = 0,iImage As Long =-1) As Long    '添加或在选项卡，如果成功，则返回新选项卡的索引，否则返回-1。
  Function=TabCtrl_AddTab ( hWndControl,iImage,TabText.vptr,nlParam)
End Function
Function Class_TabControl.InsertTab (Index As Long ,TabText As CWSTR,lParam As lParam = 0,iImage As Long =-1) As Long    '添加或在选项卡，如果成功，则返回新选项卡的索引，否则返回-1。
  Function=TabCtrl_InsertTab (hWndControl,Index,iImage,TabText.vptr,lParam)
End Function
Property Class_TabControl.Text(Index As Long) As CWSTR                 '返回/设置标签名称（索引从0开始）
   Dim TB As TC_ITEMW
   Dim zT As wString Ptr 
   Dim TabName As wString * MAX_PATH
   Dim lResult As Long
      
   TB.Mask       = TCIF_TEXT       '  Set the item mask to return the Tab Name
   TB.cchTextMax = SizeOf(TabName)  '  Size the name buffer
   TB.pszText    = VarPtr(TabName)  '  Assign the buffer's address
   
   '  Send the message to the Tab Control
   lResult = SendMessageW( hWndControl, _          '  Handle To destination control
                          TCM_GETITEMW, _         '  Message ID
                          Index, _        '  WPARAM = Index of Tab Item
                          Cast(lParam, VarPtr(TB) ))            '  Pointer to Tab Text Value    
   
   Return TabName
End Property
Property Class_TabControl.Text(Index As Long , TabText As CWSTR)
   Dim TB As TC_ITEMW
   
   TB.Mask    = TCIF_TEXT        '  Set the item mask to return the Tab Name      
   TB.pszText = TabText.vptr  '  Assign the buffer's address
   
   '  Send the message to the Tab Control
    SendMessageW( hWndControl, _          '  Handle To destination control
                           TCM_SETITEMW, _         '  Message ID
                           Index, _        '  WPARAM = Index of Tab Item
                           Cast(lParam, VarPtr(TB)) )            '  Pointer to Tab Text Value 
End Property
Property Class_TabControl.Focus() As Long                 '返回/设置具有焦点项的索引。具有焦点的项可能不同于所选项。
  Return  SendMessage( hWndControl, TCM_GETCURFOCUS,0,0)
End Property
Property Class_TabControl.Focus(Index As Long )
  SendMessage( hWndControl, TCM_SETCURFOCUS,Index,0)
End Property
Property Class_TabControl.Selected() As Long                 '返回/设置当前选定的选项卡，如果成功，返回其索引的所选的选项卡，如果没有选择则选项卡返回-1。
   Return SendMessage(hWndControl, TCM_GETCURSEL, 0, 0)
End Property
Property Class_TabControl.Selected(Index As Long)
   ShowWindow This.BindWindow( This.Selected ), SW_HIDE
   SendMessage(hWndControl, TCM_SETCURSEL, Index, 0)
   ShowWindow This.BindWindow(Index ), SW_SHOW
End Property
Function Class_TabControl.GetCount () As Long                '返回选项卡数，如果不成功则为返回零。
  Return  SendMessage( hWndControl, TCM_GETITEMCOUNT,0,0)
End Function
Function Class_TabControl.RemoveAll() As Boolean '删除所有选项卡，如果成功返回 TRUE 否则返回 FALSE。
   Dim u As Long = This.GetCount
   If u Then
      Dim i As Long ,bWndForm As .hWnd
      For i = 0 To u -1
         bWndForm = This.BindWindow(i)
         If IsWindow(bWndForm) Then PostMessage bWndForm ,WM_CLOSE ,0 ,0  '关闭所有绑定窗口
      Next
   End If
   Return SendMessage(hWndControl ,TCM_DELETEALLITEMS ,0 ,0)
End Function
Function Class_TabControl.remove(Index As Long) As Boolean '删除选项卡，如果成功返回 TRUE 否则 FALSE 。
   If SendMessage(hWndControl ,TCM_DELETEITEM ,Index ,0) Then
      Dim bWndForm As .hWnd = This.BindWindow(Index)
      If IsWindow(bWndForm) Then PostMessage bWndForm ,WM_CLOSE ,0 ,0 '关闭当前绑定的窗口
      Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
      If fp Then '删除绑定窗口句柄
         Dim cb As Long = Len(Integer)
         If Index >= 0 And (Index + 1) *cb <= Len(fp->nData) Then
            fp->nData = .Left(fp->nData ,Index * cb) & Mid(fp->nData ,(Index + 1) * cb + 1)
         End If
      End If
      Function = True
   Else
      Function = False
   End If
End Function
Sub Class_TabControl.SetItemSize(w As Long,h As Long )
   TabCtrl_SetItemSize(hWndControl ,AfxScaleX(w) ,AfxScaleY(h))
   This.SetSize
End Sub
Sub Class_TabControl.SetPadding(w as Long, h as Long)
   TabCtrl_SetPadding(hWndControl, afxScaleX(w), afxScaleY(h))
End Sub
Property Class_TabControl.ItemData(Index As Long) As lParam
   Dim TB As TC_ITEMW
   
   TB.Mask = TCIF_PARAM           '设置掩码，返回选项卡名
   SendMessageW(hWndControl, TCM_GETITEM, Index, Cast(LPARAM, @TB))
   
   Return TB.lParam
End Property
Property Class_TabControl.ItemData(Index As Long, nParam As lParam)
   Dim TB As TC_ITEMW
   TB.Mask = TCIF_PARAM           '设置掩码，返回选项卡名
   TB.lParam = nParam
   SendMessageW(hWndControl, TCM_SETITEM, Index, Cast(LPARAM, @TB))
   End Property 
Property Class_TabControl.BindWindow(Index As Long) As .hWnd
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      if Index < 0 Or (Index + 1) *Len(Integer) > Len(fp->nData) Then Return 0
      Dim ss As .hWnd Ptr = Cast(.hWnd Ptr ,StrPtr(fp->nData))
      Return ss[Index]
   End If
End Property
Property Class_TabControl.BindWindow(Index As Long ,nWnd As .hWnd)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      If Index >= 0 And index < This.GetCount Then
         Dim ww As Long = (Index + 1) *Len(Integer)
         if ww > Len(fp->nData) Then fp->nData &= String(ww - Len(fp->nData) ,0)
         Dim ss As .hWnd Ptr = Cast(.hWnd Ptr ,StrPtr(fp->nData))
         ss[Index] = nWnd
      End if
   End If
   End Property 
   Property Class_TabControl.ImageList() As hImageList
      Return TabCtrl_GetImageList(hWndControl)
   End Property
Property Class_TabControl.ImageList(nImageList As hImageList)
    ' 附加ImageList到选项卡控件
    TabCtrl_SetImageList(hWndControl, nImageList)
End Property   
Property Class_TabControl.ItemImage(Index As Long) As Long
   Dim TB As TC_ITEMW
   
   TB.Mask = TCIF_IMAGE           '设置掩码，返回选项卡名
   SendMessageW(hWndControl, TCM_GETITEM, Index, Cast(LPARAM, @TB))
   
   Return TB.iImage
End Property
Property Class_TabControl.ItemImage(Index As Long, iImage As Long) 
   Dim TB As TC_ITEMW
   TB.Mask = TCIF_IMAGE           '设置掩码，返回选项卡名
   TB.iImage = iImage
   SendMessageW(hWndControl, TCM_SETITEM, Index, Cast(LPARAM, @TB))
End Property     
Sub Class_TabControl.SetSize() '调整绑定窗口位置。
   Dim u As Long = This.GetCount
   If u = 0 Then Return
   Dim i As Long
   Dim FLY_Rect As Rect
   GetWindowRect hWndControl, Varptr(FLY_Rect)
   TabCtrl_AdjustRect(hWndControl, 0, Varptr(FLY_Rect))
   MapWindowPoints HWND_DESKTOP, hWndControl, Cast(LPPOINT, Varptr(FLY_Rect)), 2

   For i = 0 To u -1
      Dim h As .hWnd = This.BindWindow(i)
      if h Then
         SetWindowPos h, HWND_TOP, FLY_Rect.Left, FLY_Rect.Top+1, FLY_Rect.Right - FLY_Rect.Left, FLY_Rect.Bottom - FLY_Rect.Top, 0
      End If
   Next
   
End Sub











