Type Class_Control  '控件通用属性
Protected : 
   hWndControl As .hWnd '控件句柄
   m_IDC As Long     '控件IDC
Public : 
   Declare Property Enabled() As BOOLEAN                 '返回/设置控件是否允许操作。{=.True.False}
   Declare Property Enabled(ByVal bValue As Boolean)
   Declare Property Visible() As Boolean                 '显示或隐藏控件。{=.True.False}
   Declare Property Visible(ByVal bValue As Boolean)
   Declare Property Tag() As CWSTR                       '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWSTR)
   Declare Property Left() As Long                   '返回/设置相对于父窗口的 X（像素）
   Declare Property Left(ByVal nLeft As Long)
   Declare Property Top() As Long                  '返回/设置相对于父窗口的 Y（像素）
   Declare Property Top(ByVal nTop As Long)
   Declare Property Width() As Long                '返回/设置控件宽度（像素）
   Declare Property Width(ByVal nWidth As Long)
   Declare Property Height() As Long               '返回/设置控件高度（像素）
   Declare Property Height(ByVal nHeight As Long)
   Declare Sub Move(ByVal nLeft As Long, ByVal nTop As Long, ByVal nWidth As Long = 0, ByVal nHeight As Long = 0) '设置窗口位置和大小，高度、宽度=0时不修改。
   Declare Sub Size(ByVal nWidth As Long , ByVal nHeight As Long ) '设置控件高度、宽度
   Declare Property ForeColor() As Long                  '返回/设置前景色：设置用：BGR(r, g, b)  
   Declare Property ForeColor(nColor As Long)
   Declare Property BackColor() As Long                  '返回/设置背景色：设置用：BGR(r, g, b) 
   Declare Property BackColor(nColor As Long)
   Declare Property MousePointer() As Long   '返回/设置鼠标形状。{=.0 - 默认.1 - 后台运行.2 - 标准箭头.3 - 十字光标.4 - 箭头和问号.5 - 文本工字光标.6 - 不可用禁止圈.7 - 移动.8 - 双箭头↙↗.9 - 双箭头↑↓.10 - 双箭头向↖↘.11 - 双箭头←→.12 - 垂直箭头.13 - 沙漏.14 - 手型}
   Declare Property MousePointer(bValue As Long) '
   Declare Property Cursor() As HCURSOR       '返回/设置鼠标形状的光标句柄，可用 LoadCursor 加载，VFB自动处理句柄，加载后不可以销毁句柄。
   Declare Property Cursor(bValue As HCURSOR)
   Declare Property Font() As String          '返回/设置用于在控件中绘制文本的字体，格式为：字体,字号,加粗,斜体,下划线,删除线  中间用英文豆号分割，可以省略参数 默认为：宋体,9,0,0,0,0  自动响应系统DPI创建字体大小。
   Declare Property Font(nFont As String)     '操作字体句柄用 WM_SETFONT WM_GETFONT 发消息给控件即可
   Declare Property IDC() As Long             '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Sub SetFocus()  ' 使控件获取键盘焦点
   Declare Function Kill() As Boolean  '从窗体中销毁控件。成功返回 True
   Declare Property hWnd() As.hWnd                    '返回/设置控件句柄
   Declare Property hWnd(ByVal hWndNew As.hWnd)
   Declare Property hWndForm() As.hWnd         '返回/设置控件所在的父窗口句柄，主要用于多开同一个窗口后，要使用控件前，必须先指定控件所在的父窗口句柄，才能正常使用控件。如果用 SetParent 指定新父，使用时也需要用新父句柄。
   Declare Property hWndForm(ByVal hWndParent As.hWnd) '获取控件所在的父窗口句柄
   Declare Property AcceptFiles() As Boolean  '返回/设置控件是否接受拖放文件。{=.True.False}
   Declare Property AcceptFiles(bValue As Boolean)
   Declare Property ToolTip() As CWSTR              '返回/设置控件的一个提示，当鼠标光标悬停在控件时显示它。
   Declare Property ToolTip(ByVal sText As CWSTR)
   Declare Property ToolTipBalloon() As Boolean  '返回/设置控件是否一个气球样式显示工具提示。。{=.True.False}
   Declare Property ToolTipBalloon(bValue As Boolean)
   Declare Property WindowsZ(hWndlnsertAfter As.hWnd) '控件Z顺序 在某个控件之上或是{=.HWND_BOTTOM 所有控件之后.HWND_TOP 所有窗口最前.某控件句柄 将置于此控件前}
   Declare Sub Refresh()  '刷新窗口
   Declare Property UserData(idx AS LONG) As Integer      '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx AS LONG, bValue As Integer)   
End Type

Property Class_Control.WindowsZ(ByVal hWndlnsertAfter As .hWnd)
      'HWND_BOTTOM,HWND_TOP,HWND_NOTOPMOST,HWND_TOPMOST,设置窗口Z位置:最后，最前，普通，置顶
   If IsWindow(hWndControl) Then     
      SetWindowPos(hWndControl ,hWndlnsertAfter ,0 ,0 ,0 ,0 ,SWP_NOSIZE Or SWP_NOMOVE Or SWP_SHOWWINDOW)
   End If 
End Property
Property Class_Control.Enabled() As Boolean                 '使能
  Return IsWindowEnabled(hWndControl)
End Property
Property Class_Control.Enabled(ByVal bValue As BOOLEAN)
   If IsWindow(hWndControl) Then EnableWindow(hWndControl, bValue)
End Property
Property Class_Control.Tag() As CWSTR 
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp  Then
      Return fp->Tag
   End If
End Property
Property Class_Control.Tag(ByVal sText As CWSTR)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp  Then
      fp->Tag = sText
   End If   
End Property
Property Class_Control.Left() As Long                '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
  Return rc.Left
End Property
Property Class_Control.Left(ByVal nLeft As Long)
   If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl    ,@rc          '获得窗体大小
      MapWindowPoints HWND_DESKTOP ,GetParent(hWndControl) ,Cast(LPPOINT ,@rc) ,2
      SetWindowPos(hWndControl ,0 ,nLeft ,rc.top ,0 ,0 ,SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nLeft = AfxUnscaleX(nLeft)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Property Class_Control.Top() As Long     '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc     '获得窗体大小
  MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
  Return rc.Top
End Property
Property Class_Control.Top(ByVal nTop As Long)
   If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl    ,@rc          '获得窗体大小
      MapWindowPoints HWND_DESKTOP ,GetParent(hWndControl) ,Cast(LPPOINT ,@rc) ,2
      SetWindowPos(hWndControl ,0 ,rc.Left ,nTop ,0 ,0 ,SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nTop = AfxUnscaleY(nTop)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Property Class_Control.Height() As Long                  '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  Return  rc.Bottom - rc.Top
End Property
Property Class_Control.Height(ByVal nHeight As Long)
  If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl, @rc          '获得窗体大小
      SetWindowPos(hWndControl ,0 ,0 ,0 ,rc.Right - rc.Left ,nHeight ,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nHeight = AfxUnscaleY(nHeight)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if      
  End If
End Property
Property Class_Control.Width() As Long
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  Return rc.Right - rc.Left
End Property
Property Class_Control.Width(ByVal nWidth As Long)
  If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl, @rc          '获得窗体大小
      SetWindowPos(hWndControl ,0 ,0 ,0 ,nWidth ,rc.Bottom - rc.Top ,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nWidth = AfxUnscaleX(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if       
  End If
End Property
Property Class_Control.Visible() As Boolean                 '可见
  Return IsWindowVisible(hWndControl)
End Property
Property Class_Control.Visible(ByVal bValue As Boolean)
   If IsWindow(hWndControl) Then
      If bValue Then
         ShowWindow(hWndControl ,SW_SHOW)
      Else
         ShowWindow(hWndControl ,SW_HIDE)
      End If
   End If
End Property
Sub Class_Control.Move(ByVal nLeft As Long ,ByVal nTop As Long ,ByVal nWidth As Long ,ByVal nHeight As Long)
   If IsWindow(hWndControl) Then
      Dim rc As Rect
      GetWindowRect hWndControl ,@rc          '获得窗体大小
      If nWidth <= 0  Then nWidth  = rc.Right  - rc.Left
      If nHeight <= 0 Then nHeight = rc.Bottom - rc.Top
      SetWindowPos(hWndControl ,0 ,nLeft ,nTop ,nWidth ,nHeight ,SWP_NOZORDER Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nLeft   = AfxUnscaleX(nLeft)
         fp->nTop    = AfxUnscaleY(nTop)
         fp->nWidth  = AfxUnscaleY(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
         fp->nHeight = AfxUnscaleY(nHeight)
      End If
   end if
End Sub
Sub Class_Control.Size(ByVal nWidth As Long ,ByVal nHeight As Long)
   If IsWindow(hWndControl) Then   
      SetWindowPos(hWndControl ,0 ,0 ,0 ,nWidth ,nHeight ,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->nWidth  = AfxUnscaleY(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
         fp->nHeight = AfxUnscaleY(nHeight)
      end if      
   End if 
End Sub
Property Class_Control.ForeColor() As Long                  '返回/设置前景色： RGB 颜色值(用 BGR(r, g, b) 获取)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->ForeColor)
      if cc = -1 then cc = GetSysColor(COLOR_BTNTEXT)      
      Return cc
   end if
End Property
Property Class_Control.ForeColor(nColor As Long)
   If IsWindow(hWndControl) Then
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->ForeColor = nColor
         Dim wszClassName as ZString * 260
         GetClassNameA hWndControl ,@wszClassName ,SizeOf(wszClassName)
         if wszClassName = "SysTreeView32" Then
            SendMessage(hWndControl ,TVM_SETTEXTCOLOR ,0 ,nColor)
         Elseif wszClassName = "SysListView32" Then
            SendMessage(hWndControl ,LVM_SETTEXTCOLOR ,0 ,nColor)
         End If
         InvalidateRect(hWndControl ,Null ,True)
         UpdateWindow(hWndControl)
      End If
   End If
End Property
Property Class_Control.BackColor() As Long                  '返回/设置背景色： RGB 颜色值(用 BGR(r, g, b) 获取)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim cc As Long = GetCodeColorGDI(fp->BackColor)
      If cc = -1 Then cc = GetSysColor(COLOR_BTNFACE)
      Return cc
   end if
End Property
Property Class_Control.BackColor(nColor As Long)
   If IsWindow(hWndControl) Then
      Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
      If fp Then
         fp->BackColor = nColor
         Dim wszClassName as ZString * 260
         GetClassNameA hWndControl ,@wszClassName ,SizeOf(wszClassName)
         If wszClassName = "SysTreeView32" Then
            SendMessage(hWndControl ,TVM_SETBKCOLOR ,0 ,nColor)
         ElseIf wszClassName = "SysListView32" Then
            SendMessage(hWndControl ,LVM_SETBKCOLOR ,0 ,nColor)
         ElseIf wszClassName = "RICHEDIT50W" Or wszClassName = "RICHEDIT" Then
            SendMessage(hWndControl ,EM_SETBKGNDCOLOR ,0 ,nColor)
         End If
         InvalidateRect(hWndControl ,NULL ,True)
         UpdateWindow(hWndControl)
      End If
   End If
End Property
Property Class_Control.Font() As String                   '返回/设置用于在控件中绘制文本的字体
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then
       Return fp->nFont 
   End If
End Property
Property Class_Control.Font(nFont As String)
   If IsWindow(hWndControl) Then
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      If fp Then
         fp->nFont = nFont
         '如果这是一个文本框，那么我们需要保存左右边距，以便在设置字体之后可以恢复它们。设置字体会重置为默认页边距。
         Dim wszClassName as ZString * 260 ,hh As Long
         GetClassNameA hWndControl ,@wszClassName ,SizeOf(wszClassName)
         Select Case wszClassName
            Case "Edit"
               hh = SendMessageA(hWndControl ,EM_GETMARGINS ,0 ,0)
            Case "ListBox" ,"ComboBox"
               hh = SendMessage(hWndControl ,LB_GETITEMHEIGHT ,0 ,0)
         End Select
         
         SendMessageW hWndControl ,WM_SETFONT ,Cast(wParam ,gFLY_GetFontHandles(nFont)) ,True
         
         Select Case wszClassName
            Case "Edit"
               SendMessageA hWndControl ,EM_SETMARGINS ,EC_LEFTMARGIN Or EC_RIGHTMARGIN ,hh
            Case "ListBox" ,"ComboBox"
               SendMessage(hWndControl ,LB_SETITEMHEIGHT ,0 ,hh)
         End Select
         
      End If
   End If
End Property
Sub Class_Control.SetFocus()  ' 获取键盘焦点
     .SetFocus hWndControl
End Sub
Function Class_Control.Kill() As Boolean '从窗体中销毁控件。成功返回 True
   If IsWindow(hWndControl) Then Return 0
   Return DestroyWindow(hWndControl)
End Function
Property Class_Control.hWnd() As.hWnd                    '句柄
  Return hWndControl
End Property
Property Class_Control.hWnd(ByVal hWndNew As.hWnd)        '句柄

  hWndControl = hWndNew
  'm_IDC = GetDlgCtrlID(hWndNew)
End Property
Property Class_Control.hWndForm() As .hWnd '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  If IsWindow(hWndControl) Then Return 0 
  Return GetParent(hWndControl)
End Property
Property Class_Control.hWndForm(ByVal hWndParent As .hWnd)         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  hWndControl = GetDlgItem(hWndParent,m_IDC)
End Property
Property Class_Control.AcceptFiles() As Boolean  '返回/设置窗口是否接受拖放文件。
  Return (AfxGetWindowExStyle(hWndControl) And WS_EX_ACCEPTFILES) =WS_EX_ACCEPTFILES
End Property
Property Class_Control.AcceptFiles(bValue As Boolean)
   If IsWindow(hWndControl) Then
      If bValue Then
         AfxAddWindowExStyle hWndControl ,WS_EX_ACCEPTFILES
      Else
         AfxRemoveWindowExStyle hWndControl ,WS_EX_ACCEPTFILES
      End If
   End If
End Property
Sub Class_Control.Refresh()
   If IsWindow(hWndControl) Then
      InvalidateRect(hWndControl ,NULL ,True)
      UpdateWindow(hWndControl)
   End If
End Sub
Property Class_Control.IDC() As Long
  Return m_IDC
End Property
Property Class_Control.IDC(NewIDC As Long)  
   m_IDC = NewIDC 
End property
Property Class_Control.MousePointer() As Long 
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then   
      Return fp->MousePointer  
   End If
End Property
Property Class_Control.MousePointer(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then
      fp->MousePointer   =bValue
      Select Case bValue
         Case 0 : fp->nCursor = Null
         Case 1 : fp->nCursor = LoadCursor(Null, IDC_APPSTARTING)  '1 - 后台运行
         Case 2 : fp->nCursor = LoadCursor(Null, IDC_ARROW)        '2 - 标准箭头
         Case 3 : fp->nCursor = LoadCursor(Null, IDC_CROSS)        '3 - 十字光标
         Case 4 : fp->nCursor = LoadCursor(Null, IDC_HELP)         '4 - 箭头和问号
         Case 5 : fp->nCursor = LoadCursor(Null, IDC_IBEAM)        '5 - 文本工字光标
         Case 6 : fp->nCursor = LoadCursor(Null, IDC_NO)           '6 - 不可用禁止圈
         Case 7 : fp->nCursor = LoadCursor(Null, IDC_SIZEALL)      '7 - 移动
         Case 8 : fp->nCursor = LoadCursor(Null, IDC_SIZENESW)     '8 - 双箭头↙↗
         Case 9 : fp->nCursor = LoadCursor(Null, IDC_SIZENS)       '9 - 双箭头↑↓
         Case 10 : fp->nCursor = LoadCursor(Null, IDC_SIZENWSE)    '10 - 双箭头向↖↘
         Case 11 : fp->nCursor = LoadCursor(Null, IDC_SIZEWE)      '11 - 双箭头←→
         Case 12 : fp->nCursor = LoadCursor(Null, IDC_UPARROW)     '12 - 垂直箭头
         Case 13 : fp->nCursor = LoadCursor(Null, IDC_WAIT)        '13 - 沙漏
         Case 14 : fp->nCursor = LoadCursor(Null, IDC_HAND)        '14 - 手型
         case Else
              fp->nCursor = NULL       
        End Select
   end if
End Property
Property Class_Control.Cursor() As HCURSOR  
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then   
      Return fp->nCursor  
   End If
End Property
Property Class_Control.Cursor(bValue As HCURSOR)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then
      if fp->nCursor then DestroyCursor fp->nCursor
      fp->nCursor   =bValue
   end if
End Property
Property Class_Control.ToolTip() As CWSTR              '返回/设置控件的一个提示，当鼠标光标悬停在控件时显示它。
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then   
      Return fp->ToolTip  
   End If
End Property
Property Class_Control.ToolTip(ByVal sText As CWSTR)
   If IsWindow(hWndControl) Then
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      If fp Then
         fp->ToolTip = sText
         If IsWindow(fp->ToolWnd) Then DestroyWindow fp->ToolWnd
         if Len(fp->ToolTip) then
            fp->ToolWnd = FF_AddTooltip(hWndControl ,fp->ToolTip ,fp->ToolTipBalloon)
         End If
      End If
   End If
End Property
Property Class_Control.ToolTipBalloon() As Boolean  '返回/设置控件是否一个气球样式显示工具提示。。{=.True.False}
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then   
      Return fp->ToolTipBalloon  
   End If
End Property
Property Class_Control.ToolTipBalloon(bValue As Boolean)
   If IsWindow(hWndControl) Then
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->ToolTipBalloon = bValue
         If fp->ToolWnd Then DestroyWindow fp->ToolWnd
         If Len( **fp->ToolTip) Then
            fp->ToolWnd = FF_AddTooltip(hWndControl ,fp->ToolTip ,fp->ToolTipBalloon)
         End If
      End If
   End If
End Property
Property Class_Control.UserData(idx AS LONG) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return 0
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp  Then
      Return fp->UserData(idx)
   End If   
End Property
Property Class_Control.UserData(idx AS LONG, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->UserData(idx) = bValue
   End If
End Property
