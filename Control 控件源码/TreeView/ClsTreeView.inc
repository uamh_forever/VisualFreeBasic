Type Class_TreeView Extends Class_Control
Protected : 
   m_ReDraw As Boolean = True '修改列表时是否允许刷新
Public : 
   Declare Function AddItem(hParent As HTREEITEM, TheText As CWSTR, clParam As Long = 0, iImage As Long = 0, iSelectedImage As Long = 0,Children As Long =0) As HTREEITEM   '新增项目：父项(TVI_ROOT 根节点)，文本，附加值，正常图像列索引，选中时图像列索引，返回创建的项目的句柄
   Declare Function InsertItem(hParent As HTREEITEM, After As HTREEITEM, TheText As CWSTR, clParam As Long = 0, iImage As Long = 0, iSelectedImage As Long = 0,Children As Long =0) As HTREEITEM   '插入项目：父项(TVI_ROOT 根节点)，项句柄或：{2.TVI_FIRST 开头.TVI_LAST 末尾.TVI_ROOT 根.TVI_SORT} 按照字母顺序， 文本，附加值，正常图像列索引，选中时图像列索引，返回创建的项目的句柄
   Declare Property Selection() As HTREEITEM                 '返回/设置选择指定的项，并将视图滚动到项。
   Declare Property Selection(ByVal hItem As HTREEITEM)
   Declare Property ImageNormal(hItem As HTREEITEM) As Long                 '返回/设置正常时显示图像索引。
   Declare Property ImageNormal(hItem As HTREEITEM, iImage As Long)
   Declare Property ImageSelected(hItem As HTREEITEM) As Long                 '返回/设置选中时显示图像索引。
   Declare Property ImageSelected(hItem As HTREEITEM, iImage As Long)
   Declare Property ItemData(hItem As HTREEITEM) As Integer                 '返回/设置指定项的 附加数据 lParam
   Declare Property ItemData(hItem As HTREEITEM, newlParam As Integer)
   Declare Property Text(hItem As HTREEITEM) As CWSTR                 '返回/设置项目的文本。
   Declare Property Text(hItem As HTREEITEM, TheText As CWSTR)
   Declare Function DeleteAllItems() As Boolean           '从树视图中删除所有项目。如果成功返回 TRUE 否则为 FALSE
   Declare Function DeleteItem(hItem As HTREEITEM) As Boolean           '从树视图中删除项节点。如果成功返回 TRUE 否则为 FALSE
   Declare Property Expand(hItem As HTREEITEM) As Boolean            '返回/设置 展开或折叠要显示/隐藏子节点的节点。如果成功返回 TRUE 否则为 FALSE {=.True.False}
   Declare Property Expand(hItem As HTREEITEM ,nExpand As Boolean)   
   Declare Sub ExpandEx(hItem As HTREEITEM ,nExpand As Long ) '更多功能， {2.TVE_COLLAPSE 折叠列表。.TVE_COLLAPSERESET 折叠列表并删除子项目。.TVE_EXPAND 展开列表.TVE_TOGGLE 切换折叠} 
   Declare Sub CollapseAllItems() '折叠所有项目列表。
   Declare Sub ExpandAllItems() '展开所有项目列表。
   Declare Function EnsureVisible(hItem As HTREEITEM) As Boolean   '确保树视图项目可见，展开父项目或滚动树视图控件。没有项目展开返回 TRUE 否则为 FALSE
   Declare Function GetCount() As Long   '检索树视图控件中全部项目总数。
   Declare Function GetVisibleCount() As Long    '可以完全可见的项目数，就是可以显示多少行。
   Declare Function GetChildCount(hItem As HTREEITEM) As Long      '获取指定项目子级别数量
   Declare Function GetRoot() As HTREEITEM           '返回最顶层的第一个项目，成功则返回该项目的句柄，否则返回NULL。
   Declare Function GetLevel(hItem As HTREEITEM) As Long           '返回项目的级别。根项被假定为零级。
   Declare Function GetChild(hItem As HTREEITEM) As HTREEITEM   '指定树视图项目的第一个子项目。 如果成功，则返回该项目的句柄，否则返回NULL。
   Declare Function GetFirstVisible() As HTREEITEM   '树视图控件窗口中检索第一个可见项目。 如果成功，则返回该项目的句柄，否则返回NULL。
   Declare Function GetLastVisible() As HTREEITEM   '树视图控件窗口中检索最后一个可见项目。 如果成功，则返回该项目的句柄，否则返回NULL。
   Declare Function GetNextSiblin(hItem As HTREEITEM) As HTREEITEM   '检索指定项目的下一个同级项目。 如果成功，则返回该项目的句柄，否则返回NULL。
   Declare Function GetNextVisible(hItem As HTREEITEM) As HTREEITEM   '检索指定项目后面的下一个可见项目 如果成功，则返回该项目的句柄，否则返回NULL。
   Declare Function GetParent(hItem As HTREEITEM) As HTREEITEM   '检索指定项目的父项目， 如果成功，则返回该项目的句柄，否则返回NULL。
   Declare Function GetPrevSiblin(hItem As HTREEITEM) As HTREEITEM   '检索指定项目的上一个同级项目， 如果成功，则返回该项目的句柄，否则返回NULL。
   Declare Function GetPrevVisible(hItem As HTREEITEM) As HTREEITEM   '检索指定项目的上一个可见项目， 如果成功，则返回该项目的句柄，否则返回NULL。
   Declare Function HitTest(x As Long, y As Long) As HTREEITEM   '检索指定坐标（像素单位）的项， 如果有返回该项目的句柄，否则返回NULL。
   Declare Property Checked(hItem As HTREEITEM) As Boolean   '返回/设置项目“已选中”或“未选中”。{=.True.False}
   Declare Property Checked(hItem As HTREEITEM, fCheck As Boolean)
   
   Declare Function SelectSetFirstVisible(hItem As HTREEITEM) As Boolean   '确保指定的项目可见，如果可能，指定的项目将成为控件窗口顶部的第一个可见项目。 如果成功则返回CTRUE，否则返回FALSE。
   Declare Function SortChildren(hItem As HTREEITEM, recurse As Boolean) As Boolean   '指定父项的子项进行排序。recurse {1.True.False} 排序所有子级项目，  如果成功则返回CTRUE，否则返回FALSE。
   
   Declare Property ImageList() As HIMAGELIST                 '返回/设置与TreeView关联的ImageList的控件。
   Declare Property ImageList(hImageList As hImageList)
   Declare Property ImageListState() As hImageList             '返回/设置与TreeView关联的状态图像列表。
   Declare Property ImageListState(hImageList As hImageList)
   
   Declare Property Indent() As Long                 '返回/设置树视图控件的缩进宽度（以像素为单位）。
   Declare Property Indent(widht As Long)
   Declare Property ItemHeight() As Long                 '返回/设置项目的高度（以像素为单位）。设置-1为恢复默认高度
   Declare Property ItemHeight(cyItem As Long)
   Declare Property ReDraw() As Boolean         '设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
   Declare Property ReDraw(nValue As Boolean)    '修改项目时是否允许刷新，（注意：多开窗口中的控件，多窗口用同1个控件类，返回此值可能不正确）
   Declare Property LineColor() As Long        '返回/设置线条颜色：设置用：BGR(r, g, b)  返回或设置 CLR_DEFAULT 表示系统默认
   Declare Property LineColor(nColor As Long)
   
End Type


Property Class_TreeView.Selection() As HTREEITEM                 '返回/设置选择指定的项，并将视图滚动到项。
  Return Cast(hTreeItem, TreeView_GetSelection(hWndControl))
End Property
Property Class_TreeView.Selection(ByVal hItem As hTreeItem)
  TreeView_SelectItem( hWndControl, hItem)
End Property
Property Class_TreeView.Indent() As Long                 '返回/设置树视图控件的缩进宽度（以像素为单位）。
  Return TreeView_GetIndent(hWndControl)
End Property
Property Class_TreeView.Indent(widht As Long)
  TreeView_SetIndent( hWndControl, widht)
End Property
Property Class_TreeView.ItemHeight() As Long                 '返回/设置项目的高度（以像素为单位）。
  Return TreeView_GetItemHeight(hWndControl)
End Property
Property Class_TreeView.ItemHeight(cyItem As Long)
  TreeView_SetItemHeight( hWndControl, cyItem)
End Property


Property Class_TreeView.ImageSelected(hItem As hTreeItem) As Long '返回/设置图像索引。
   Dim ti As TV_ITEM
   
   '检查窗口句柄是否有效
   If IsWindow(hWndControl) Then
      
      ti.hItem = hItem
      ti.mask  = TVIF_HANDLE Or TVIF_SELECTEDIMAGE
      
      If TreeView_GetItem(hWndControl ,VarPtr(ti)) = 0 Then
         Return 0
      Else
         Return ti.iSelectedImage
      End If
      
   End If
End Property
Property Class_TreeView.ImageSelected(hItem As hTreeItem ,iImage As Long)
   '检查窗口句柄是否有效
   If IsWindow(hWndControl) Then
      Dim ti As TV_ITEM
      ti.hItem = hItem
      ti.mask  = TVIF_HANDLE Or TVIF_SELECTEDIMAGE Or TVIF_IMAGE
      TreeView_GetItem(hWndControl ,VarPtr(ti))
      ti.iSelectedImage = iImage
      TreeView_SetItem(hWndControl ,VarPtr(ti))
   End If
End Property
Property Class_TreeView.ImageNormal(hItem As hTreeItem) As Long                 '返回/设置正常图像索引。
    Dim ti As TV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
      
        ti.hItem          = hItem                                        
        ti.mask           = TVIF_HANDLE Or TVIF_IMAGE 
      
        If TreeView_GetItem (hWndControl, VarPtr(ti) ) = 0 Then
           Return  0
        Else
           Return ti.iImage   
        End If   
    
    End If
End Property
Property Class_TreeView.ImageNormal(hItem As hTreeItem,iImage As Long)
      Dim ti As TV_ITEM
      ti.hItem = hItem
      ti.mask  = TVIF_HANDLE Or TVIF_SELECTEDIMAGE Or TVIF_IMAGE
      TreeView_GetItem(hWndControl ,VarPtr(ti))
      ti.iImage = iImage
      TreeView_SetItem(hWndControl ,VarPtr(ti))
End Property
    
Property Class_TreeView.ImageList() As hImageList                 '返回/设置与TreeView关联的ImageList的控件。
  Return TreeView_GetImageList (hWndControl, TVSIL_NORMAL)
End Property
Property Class_TreeView.ImageList(hImageList As hImageList)
  SNDMSG(hWndControl, TVM_SETIMAGELIST, TVSIL_NORMAL, Cast(.lParam, hImageList))
'  TreeView_SetImageList (hWndControl, Cast(wParam, hImageList), TVSIL_NORMAL)
End Property
Property Class_TreeView.ImageListState() As hImageList             '返回/设置与TreeView关联的状态图像列表。
  Return TreeView_GetImageList (hWndControl, TVSIL_STATE)
End Property
Property Class_TreeView.ImageListState(hImageList As hImageList)
  SNDMSG(hWndControl, TVM_SETIMAGELIST, TVSIL_STATE, Cast(.lParam, hImageList))
End Property


Property Class_TreeView.ItemData(hItem As hTreeItem) As Integer     '返回/设置指定项的 lParam
    Dim ti As TV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
  
       ti.hItem = hItem                                                 
       ti.mask  = TVIF_HANDLE Or TVIF_PARAM  
       
       TreeView_GetItem(hWndControl, VarPtr(ti))
  
       Return ti.lParam  
    End If
End Property
Property Class_TreeView.ItemData(hItem As hTreeItem,newlParam As Integer)
    Dim ti As TV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  

        ti.hItem  = hItem                                        
        ti.mask   = TVIF_PARAM
        ti.lParam = newlParam
      
        If TreeView_SetItem (hWndControl, VarPtr(ti) ) = 0 Then

        End If   
    
    End If
End Property
Property Class_TreeView.Text(hItem As hTreeItem) As CWSTR                 '返回/设置项目的文本。
    Dim ti      As TV_ITEMW
    Dim zText   As wString * MAX_PATH
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
    
        'Set up the item structure...
        ti.hItem      = hItem                                        
        ti.mask       = TVIF_TEXT                                    
        ti.cchTextMax = MAX_PATH
        ti.pszText    = @zText 

        'Query the tree...
        If SendMessageW( hWndControl , TVM_GETITEMW , 0,cast(lParam, @ti) )  Then     
           Return zText
        End If
    
    End If
End Property
Property Class_TreeView.Text(hItem As hTreeItem,TheText As CWSTR)
    Dim ti As TV_ITEMW

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
      
        ti.hItem      = hItem                                        
        ti.mask       = TVIF_TEXT                   
        ti.cchTextMax = Len(**TheText)
        ti.pszText    = TheText.vptr
      
        If SendMessageW (hWndControl,TVM_SETITEMW,0,cast(lParam,@ti) ) = 0 Then

        End If   
    
    End If
End Property
Function Class_TreeView.GetLevel (hItem As hTreeItem) As Long           '返回项目的级别。根项被假定为零级。 
    Dim hRoot  As hTreeItem
    Dim mLevel As Long
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        If hItem <> 0 Then
        
          'Determine the root's handle...
          hRoot = TreeView_GetRoot(hWndControl)
          
          'Walk back up the tree, towards the root...
          While ( hItem <> hRoot ) And ( hItem <> 0 )
      
            mLevel = mLevel + 1
            hItem = TreeView_GetNextItem( hWndControl, hItem, TVGN_PARENT )
      
          Wend
        
        End If
        
        Return  mLevel
    
    End If
End Function
Function Class_TreeView.GetChildCount (hItem As hTreeItem) As Long           '获取子级别数量 
    Dim hChild As hTreeItem
    Dim nCount As Long
  
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  

        hChild = TreeView_GetNextItem( hWndControl, hItem, TVGN_CHILD )
        While hChild <> 0
        
          nCount = nCount + 1
          hChild = TreeView_GetNextItem( hWndControl, hChild, TVGN_NEXT )
        
        Wend
  
        Return  nCount
    End If
End Function
Function Class_TreeView.DeleteAllItems () As Boolean           '从树视图中删除所有项目。如果成功返回 TRUE 否则为 FALSE 
    Function = SendMessage( hWndControl, TVM_DELETEITEM, 0, Cast(.lParam, TVI_ROOT))
End Function
Function Class_TreeView.DeleteItem (hItem As hTreeItem) As Boolean           '从树视图中删除项节点。如果成功返回 TRUE 否则为 FALSE 
    Function = SendMessage( hWndControl, TVM_DELETEITEM, 0, Cast(.lParam, hItem) )
End Function
Property Class_TreeView.Expand(hItem As hTreeItem) As Boolean
   Dim item As TV_ITEM
   item.hItem = hItem
   item.mask  = TVIF_STATE
   SendMessage hWndControl ,TVM_GETITEM ,0 ,Cast(.lParam ,@item)
   return cast(Boolean ,(item.state And TVIS_EXPANDED) = TVIS_EXPANDED)
End Property
Property Class_TreeView.Expand(hItem As hTreeItem ,nExpand As Boolean)
    If nExpand Then
        TreeView_Expand( hWndControl, hItem, TVE_EXPAND)
    Else
        TreeView_Expand( hWndControl, hItem, TVE_COLLAPSE)
    End If
End Property 
Sub Class_TreeView.ExpandEx(hItem As hTreeItem ,nExpand As Long)
   If (nExpand And TVE_COLLAPSERESET) <> 0 Then nExpand Or= TVE_COLLAPSE
   TreeView_Expand(hWndControl ,hItem ,TVE_EXPAND)
End Sub
Function Class_TreeView.AddItem (hParent As HTREEITEM,TheText As CWSTR ,clParam As Long =0,iImage As Long =0,iSelectedImage As Long =0,Children As Long =0) As HTREEITEM   '新增项目：父项(TVI_ROOT 根节点)，文本，附加值，正常图像列索引，选中时图像列索引 
    Function =This.InsertItem (hParent,Cast(HTREEITEM,TVI_LAST),TheText,clParam,iImage,iSelectedImage,Children)
End Function
Function Class_TreeView.InsertItem (hParent As HTREEITEM,After As HTREEITEM, TheText As CWSTR ,clParam As Long =0,iImage As Long =0,iSelectedImage As Long =0,Children As Long =0) As HTREEITEM   '插入项目：父项(TVI_ROOT 根节点)，项句柄或：TVI_FIRST 开头，TVI_LAST 末尾， TVI_ROOT 根，TVI_SORT 按照字母顺序， 文本，附加值，正常图像列索引，选中时图像列索引，返回创建的项目的句柄 
    Dim uInsert As TVINSERTSTRUCTW
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        uInsert.hInsertAfter            = After
        uInsert.item.mask               = TVIF_TEXT Or TVIF_IMAGE Or TVIF_SELECTEDIMAGE Or TVIF_PARAM Or iif(Children,TVIF_CHILDREN,0)
        uInsert.hParent                 = hParent
        uInsert.item.pszText            = TheText.vptr
        uInsert.item.iImage             = iImage 
        uInsert.item.iSelectedImage     = iSelectedImage
        uInsert.item.lParam             = clParam 
        uInsert.item.cChildren          = Children 
        Function = Cast(HTREEITEM, SendMessageW( hWndControl , TVM_INSERTITEMW ,0 , Cast(lParam, @uInsert) ) )
    End If
End Function
Sub Class_TreeView.CollapseAllItems () '折叠所有项目列表。 
   TreeView_CollapseAllItems hWndControl
End Sub
Function Class_TreeView.EnsureVisible (hItem As hTreeItem) As Boolean   '确保树视图项目可见，展开父项目或滚动树视图控件。没有项目展开返回 TRUE 否则为 FALSE 
   Function=TreeView_EnsureVisible (hWndControl,hItem)
End Function
Sub Class_TreeView.ExpandAllItems () '展开所有项目列表。
   TreeView_ExpandAllItems hWndControl
End Sub
Function Class_TreeView.GetChild (hItem As hTreeItem) As hTreeItem   '指定树视图项目的第一个子项目。 如果成功，则返回该项目的句柄，否则返回NULL。 
   Function=TreeView_GetChild (hWndControl,hItem)
End Function
Function Class_TreeView.GetCount () As Long   '检索树视图控件中项目总数。  
   Function=TreeView_GetCount (hWndControl)
End Function
Function Class_TreeView.GetFirstVisible () As hTreeItem   '树视图控件窗口中检索第一个可见项目。 如果成功，则返回该项目的句柄，否则返回NULL。
   Function=TreeView_GetFirstVisible (hWndControl)
End Function
Function Class_TreeView.GetLastVisible () As hTreeItem   '
   Function=TreeView_GetLastVisible (hWndControl)
End Function
Function Class_TreeView.GetNextSiblin(hItem As hTreeItem) As hTreeItem   '检索指定项目的下一个同级项目。 如果成功，则返回该项目的句柄，否则返回NULL。 
   Function=TreeView_GetNextSibling (hWndControl,hItem)
End Function
Function Class_TreeView.GetNextVisible(hItem As hTreeItem) As hTreeItem   '指定项目后面的下一个可见项目 如果成功，则返回该项目的句柄，否则返回NULL。 
   Function=TreeView_GetNextVisible (hWndControl,hItem)
End Function
Function Class_TreeView.GetParent(hItem As hTreeItem) As hTreeItem   '检索指定项目的父项目， 如果成功，则返回该项目的句柄，否则返回NULL。 
   Function=TreeView_GetParent (hWndControl,hItem)
End Function
Function Class_TreeView.GetPrevSiblin(hItem As hTreeItem) As hTreeItem   '检索指定项目的上一个同级项目， 如果成功，则返回该项目的句柄，否则返回NULL。 
   Function=TreeView_GetPrevSibling (hWndControl,hItem)
End Function
Function Class_TreeView.GetPrevVisible(hItem As hTreeItem) As hTreeItem   '检索指定项目的上一个可见项目， 如果成功，则返回该项目的句柄，否则返回NULL。 
   Function=TreeView_GetPrevVisible (hWndControl,hItem)
End Function
Function Class_TreeView.GetRoot () As hTreeItem           '返回最顶层或第一个项目 
   Function=TreeView_GetRoot (hWndControl)
End Function
Function Class_TreeView.GetVisibleCount() As Long    '可以完全可见的项目数，就是可以显示多少行。
   Function=TreeView_GetVisibleCount (hWndControl)
End Function
Function Class_TreeView.HitTest(x As Long ,y As Long ) As hTreeItem   '检索指定坐标（像素单位）的项， 如果有返回该项目的句柄，否则返回NULL。
   Dim aa As TVHITTESTINFO
   aa.pt.x =x
   aa.pt.y=y
   aa.flags =TVHT_NOWHERE
   aa.hItem =Null
   Function=TreeView_HitTest (hWndControl,@aa)
End Function

Function Class_TreeView.SelectSetFirstVisible (hItem As hTreeItem) As Boolean   '确保指定的项目可见，如果可能，指定的项目将成为控件窗口顶部的第一个可见项目。 如果成功则返回CTRUE，否则返回FALSE。
   Function=TreeView_SelectSetFirstVisible (hWndControl,hItem)
End Function
Function Class_TreeView.SortChildren (hItem As hTreeItem,recurse As Boolean) As Boolean   '指定父项的子项进行排序。recurse ={=.True.False} 排序所有子级项目，  如果成功则返回CTRUE，否则返回FALSE。
   Function=TreeView_SortChildren (hWndControl,hItem,recurse)
End Function
Property Class_TreeView.ReDraw()  As Boolean         '返回/设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
    Property = m_ReDraw  '无需返回
End Property
Property Class_TreeView.ReDraw(nValue As Boolean)
 m_ReDraw = nValue
 SendMessage(hWndControl, WM_SETREDRAW, nValue, 0)
End Property
Property Class_TreeView.Checked (hItem As hTreeItem) As Boolean   '返回/设置项目“已选中”或“未选中”。{=.True.False} 
    Property = TreeView_IsItemChecked (hWndControl,hItem)
End Property
Property Class_TreeView.Checked (hItem As hTreeItem,fCheck As Boolean ) 
    TreeView_SetCheckState (hWndControl,hItem,fCheck)
End Property
Property Class_TreeView.LineColor() As Long                  '返回/设置前景色： RGB 颜色值(用 BGR(r, g, b) 获取)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then
      if fp->CtlData(0) = CLR_DEFAULT Then Return CLR_DEFAULT
      Dim cc As Long = GetCodeColorGDI(fp->CtlData(0))
      if cc = -1 then cc = CLR_DEFAULT
      Return cc
   end if
End Property
Property Class_TreeView.LineColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then
      fp->CtlData(0) = nColor
      Dim cc As Long
      if nColor = -1 Then
         cc = CLR_DEFAULT
      Else
         cc = nColor
      End if
      SendMessage(hWndControl ,TVM_SETLINECOLOR ,0 ,cc)
      InvalidateRect(hWndControl ,Null ,True)
      UpdateWindow(hWndControl)
   End If
End Property
