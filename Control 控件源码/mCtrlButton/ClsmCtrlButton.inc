'控件类
#include Once "..\mCtrlPublic\button.bi"
Type Class_mCtrlButton Extends Class_Control
    Declare Property Caption() As CWStr                '返回/设置控件中的文本
    Declare Property Caption(ByVal sText As CWStr)
    Declare Sub Click() '模拟用户点击了这个按钮
    Declare Sub IcoRes(nResImg As String) '设置 图像资源名，就是在图像管理器中的名称， 空名称，将去掉图标
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_mCtrlButton.Caption() As CWStr
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_mCtrlButton.Caption(ByVal sText As CWStr)
  AfxSetWindowText  hWndControl, sText
End Property
Sub Class_mCtrlButton.Click() '模拟用户点击了这个按钮
  SendMessage(hWndControl, BM_CLICK, 0, 0)
End Sub
Sub Class_mCtrlButton.IcoRes(nResImg As String)
   If Len(nResImg) = 0 Then
      AfxRemoveWindowStyle hWndControl ,BS_ICON
      SendMessage(hWndControl ,BM_SETIMAGE ,IMAGE_ICON ,Cast(lParam ,0)) '默认为 SendMessageW
      Return
   End if
   Dim nIcon As HICON
   Dim hh    As Long = This.Height() -2
   If InStr(nResImg ,"BITMAP_") = 1 Then
      Dim nBmp As HBITMAP = LoadImageA(App.HINSTANCE ,nResImg ,IMAGE_BITMAP ,hh ,hh ,LR_DEFAULTCOLOR)
      Dim po   As ICONINFO
      po.fIcon    = TRUE
      po.hbmColor = nBmp
      po.hbmMask  = nBmp
      nIcon       = CreateIconIndirect(@po)
      DeleteObject nBmp
   ElseIf InStr(nResImg ,"ICON_") = 1 Or nResImg = "AAAAA_APPICON" Then
      nIcon = LoadImageA(App.HINSTANCE ,nResImg ,IMAGE_ICON ,hh ,hh ,LR_DEFAULTCOLOR) '从资源里加载图标
   Else
      nIcon = AfxGdipIconFromRes(App.hInstance ,nResImg)
   End if
   If nIcon Then
      if Len(AfxGetWindowText(hWndControl)) = 0 Then
         AfxAddWindowStyle hWndControl ,BS_ICON
      Else
         AfxRemoveWindowStyle hWndControl ,BS_ICON
      End if
      SendMessage(hWndControl ,BM_SETIMAGE ,IMAGE_ICON ,Cast(lParam ,nIcon)) '默认为 SendMessageW
   Else
      AfxRemoveWindowStyle hWndControl ,BS_ICON
      SendMessage(hWndControl ,BM_SETIMAGE ,IMAGE_ICON ,Cast(lParam ,0)) '默认为 SendMessageW
   End If
   
End Sub

