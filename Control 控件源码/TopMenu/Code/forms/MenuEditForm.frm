﻿#VisualFreeBasic_Form#  Version=5.5.6
Locked=0

[Form]
Name=MenuEditForm
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_BORDER,WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=菜单编辑器
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=841
Height=459
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=500
MinHeight=435
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=文字：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=570
Top=11
Width=39
Height=19
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=False
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=605
Top=8
Width=214
Height=24
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=名称：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=571
Top=56
Width=39
Height=19
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=False
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=605
Top=54
Width=211
Height=24
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=快捷键：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=570
Top=138
Width=56
Height=20
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[ComboBox]
Name=Combo1
Index=-1
Style=0 - 标签和下拉框
Custom=
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=False
AutoHscroll=False
MaxLength=0
Uppercase=False
Lowercase=False
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=622
Top=134
Width=170
Height=25
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=选中状态
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
Left=621
Top=167
Width=73
Height=16
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check2
Index=-1
Style=0 - 标准
Caption=可用状态
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
Left=621
Top=191
Width=77
Height=17
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Line]
Name=Line1
Index=0
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=572
Top=296
Width=245
Height=6
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=确定
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=693
Top=387
Width=55
Height=28
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line2
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=572
Top=376
Width=241
Height=7
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=761
Top=387
Width=55
Height=28
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=新增
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=572
Top=344
Width=47
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=删除
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=764
Top=344
Width=51
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=▲
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=572
Top=307
Width=26
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=▼
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=603
Top=307
Width=26
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=新增子菜单
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=680
Top=344
Width=75
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=图标：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=572
Top=242
Width=39
Height=19
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[CheckBox]
Name=Check3
Index=-1
Style=0 - 标准
Caption=自绘（在自绘事件中自己写代码）
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
Left=621
Top=216
Width=197
Height=17
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=输入1个英文字母 “-”减号，为分割线
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=606
Top=34
Width=231
Height=19
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label6
Index=-1
Style=0 - 无边框
Caption=代码中代表此菜单项(必须全工程唯一的)
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=604
Top=82
Width=231
Height=17
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command8
Index=-1
Caption=帮助
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=572
Top=387
Width=39
Height=28
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Image]
Name=Image1
Index=-1
Picture=
Stretch=0 - 自动适应
Percent=0
GrayScale=False
Enabled=True
Visible=True
Left=621
Top=241
Width=30
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command9
Index=-1
Caption=选择图像
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=671
Top=243
Width=68
Height=24
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command10
Index=-1
Caption=插入
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=625
Top=344
Width=47
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command11
Index=-1
Caption=←
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=641
Top=307
Width=26
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command12
Index=-1
Caption=→
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=672
Top=307
Width=26
Height=27
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command13
Index=-1
Caption=Select
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=626
Top=387
Width=55
Height=28
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command14
Index=-1
Caption=字体图标
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=745
Top=243
Width=68
Height=24
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label7
Index=-1
Style=0 - 无边框
Caption=
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=False
Ellipsis=False
Left=574
Top=270
Width=241
Height=19
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command15
Index=-1
Caption=自动产生名称
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=721
Top=99
Width=94
Height=24
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line1
Index=1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=572
Top=128
Width=245
Height=6
Layout=2 - 跟随右边
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[mCtrlTreeList]
Name=mCtrlTreeList1
Help=
Index=-1
Style=1 - 细边框
WindowTheme=True
ImageList=无图像列表控件
HasButton=True
HasLines=True
LinesRoot=True
GridLines=True
ShowSel=True
FullSel=True
NoHeight=False
DBuffer=True
ColHeader=True
HeaderDrop=True
SingleExpand=False
MultiSelect=False
NoTooltips=False
Enabled=True
Visible=True
Left=5
Top=5
Width=557
Height=410
Layout=5 - 宽度和高度
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
Type MenuEdit_t
   zText As String    'utf8 格式  菜单文字
   mnuText As String  'utf8 格式  代码里的名字
   nKeys As String    '快捷键
   nSel As Long       '选中
   Usable As Long      '可用状态
   nDraw as Long       '自绘
   nIco As String       'utf8 格式 图标
   tv As MC_HTREELISTITEM     '对应TV 
End Type
Dim Shared MenuEditList() As MenuEdit_t, MenuEditZ As Long, MenuEditX As Long ' Z 正在设置  X 被修改

'创建 [事件]   hWndForm=窗体句柄  UserData=可选的用户定义的值
Sub MenuEditForm_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
  Combo1.AddItem vfb_LangString("(无快捷键)")
  Dim i As Long
  For i = 65 To 90
      Combo1.AddItem StringToCWSTR("Ctrl+" & Chr(i))
  Next
  For i = 1 To 12
      Combo1.AddItem StringToCWSTR("F" & i)
  Next
  For i = 1 To 12
      Combo1.AddItem StringToCWSTR("Ctrl+F" & i)
  Next
  For i = 1 To 12
      Combo1.AddItem StringToCWSTR("Shift+F" & i)
  Next
  For i = 1 To 12
      Combo1.AddItem StringToCWSTR("Ctrl+Shift+F" & i)
  Next
  Combo1.AddItem "Ctrl+Insert"
  Combo1.AddItem "Shift+Insert"
  Combo1.AddItem "Delete"
  Combo1.AddItem "Shift+Delete"
  Combo1.AddItem "Alt+Back"
  Combo1.ListIndex = 0 

End Sub

Sub MenuEditForm_Command8_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   OpenHelp ("VisualFreeBasic.chm::/MenuEdit.htm")
End Sub

Sub MenuEditForm_Shown(hWndForm As hWnd, UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   Me.UserData(0) = UserData
   Dim As Rect rc,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr, UserData)
   GetWindowRect(st->hWndForm, @rc2)
   GetWindowRect(st->hWndList, @rc)
   if rc.Left + Me.Width > rc2.right Then rc.Left = rc2.right - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top = rc2.bottom - Me.Height
   Me.Move rc.Left, rc.top
   Dim u As Long, i As Long
   Dim ss As String = trim( *st->value)
   mCtrlTreeList1.AddColumn vfb_LangString("文字"), AfxScaleX(280)
   mCtrlTreeList1.AddColumn vfb_LangString("名称"), AfxScaleX(180)
   mCtrlTreeList1.AddColumn vfb_LangString("快捷键"), AfxScaleX(70)
   mCtrlTreeList1.AddColumn vfb_LangString("选中"), AfxScaleX(55),MC_TLFMT_CENTER
   mCtrlTreeList1.AddColumn vfb_LangString("可用"), AfxScaleX(55),MC_TLFMT_CENTER
   mCtrlTreeList1.AddColumn vfb_LangString("自绘"), AfxScaleX(55),MC_TLFMT_CENTER
   mCtrlTreeList1.AddColumn vfb_LangString("图像"), AfxScaleX(180)
   
   if Len(ss) = 0 Then
      Erase MenuEditList
   Else
      Dim el() As String, ftv(100) As MC_HTREELISTITEM, ftvi As Long
      u = vbSplit(ss, chr(1), el())
      ReDim MenuEditList(u -1)
      Dim si As Long = -1
      ftv(0) = MC_TLI_ROOT
      for i = 0 To u -1
         el(i) = Trim(el(i))
         if Len(el(i)) Then
            Select Case el(i)
               Case "{"
                  ftvi += 1
                  if ftvi > 99 Then ftvi = 99
               Case "}"
                  ftvi -= 1
                  if ftvi < 0 Then ftvi = 0
               Case Else
                  Dim sl() As String
                  Dim uu As Long = vbSplit(el(i), Chr(2), sl())
                  if uu > 6 Then
                     si += 1
                     MenuEditList(si).zText = sl(0)   'utf8 格式  菜单文字
                     MenuEditList(si).mnuText = sl(1)  'utf8 格式  代码里的名字
                     MenuEditList(si).nKeys = sl(2)    '快捷键
                     MenuEditList(si).nSel = ValInt(sl(3))       '选中
                     MenuEditList(si).Usable = ValInt(sl(4))      '可用状态
                     MenuEditList(si).nDraw = ValInt(sl(5))      '自绘
                     MenuEditList(si).nIco = sl(6)       '图标
                     Dim mws As CWSTR = UTF8toCWSTR(sl(0))
                     'MenuEditList(si).tv = TreeView1.AddItem(ftv(ftvi),mws , si)     '对应TV
                     Dim tv As MC_HTREELISTITEM = mCtrlTreeList1.AddItem(ftv(ftvi), mws, si)
                      MenuEditList(si).tv =tv 
                      mCtrlTreeList1.SetItemText(tv, 1,UTF8toCWSTR(sl(1)))
                      mCtrlTreeList1.SetItemText(tv, 2, sl(2))
                      mCtrlTreeList1.SetItemText(tv, 3, IIf(ValInt(sl(3)),vfb_LangString("是"),vfb_LangString("否")))
                      mCtrlTreeList1.SetItemText(tv, 4, IIf(ValInt(sl(4)),vfb_LangString("是"),vfb_LangString("否")))
                      mCtrlTreeList1.SetItemText(tv, 5, IIf(ValInt(sl(5)),vfb_LangString("是"),vfb_LangString("否")))
                      mCtrlTreeList1.SetItemText(tv, 6, sl(6))
                     ftv(ftvi + 1) =tv
                  End if
            End Select
         End if
      Next
      if si = -1 Then
         Erase MenuEditList
      Else
         if UBound(MenuEditList) <> si Then ReDim Preserve MenuEditList(si)
      End if
   End if
   'TreeView1.ExpandAllItems
   'TreeView1.Selection = TreeView1.GetRoot
   'SetMenuEdit TreeView1.Selection
   mCtrlTreeList1.ExpandAllItems 
   mCtrlTreeList1.Selection = mCtrlTreeList1.GetRoot 
   SetMenuEdit mCtrlTreeList1.Selection
   MenuEditZ = 0
   MenuEditX = 0
End Sub
Sub MenuEditForm_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    Me.Close 
End Sub

Function MenuEditForm_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   if MenuEditX <> 0 Then
      Select Case MessageBox(hWndForm, vfb_LangString("菜单已经被修改，你要保存修改吗？"), "VisualFreeBasic", _
               MB_YESNOCANCEL Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Case IDYES
            if SaveMenuEdit() <> 0 Then Return 1 '保存数据
         Case IDNO
         Case IDCANCEL
            Return 1
      End Select
   End if      

    Function = 0 '根据自己需要修改
End Function

Function SaveMenuEdit() As Long  '保存数据，成功返回0 ，失败非0
   Dim i As Long
   Dim u As Long = UBound(MenuEditList)
   if u > -1 Then
      for i = 0 To u
         if MenuEditList(i).mnuText = "" Then
               mCtrlTreeList1.Selection = MenuEditList(i).tv
               MessageBox(Me.hWnd, vfb_LangString("未设置名称，将无法使用菜单，必须设置。"), "VisualFreeBasic", _
                  MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
               mCtrlTreeList1.SetFocus
               Return 1
         End if
      Next
      for i = 0 To u
         Dim ts As String = MenuEditList(i).mnuText
         if Len(ts) > 0 Then
            for ii As Long = 0 To u
               if i <> ii Then
                  if ts = MenuEditList(ii).mnuText Then
                     mCtrlTreeList1.Selection = MenuEditList(i).tv
                     MessageBox(Me.hWnd, vfb_LangString("菜单文字1：") & Utf8toStr(MenuEditList(i).zText) & vbCrLf & vfb_LangString("菜单文字2：") & Utf8toStr(MenuEditList(ii).zText) & vbCrLf & _
                        "使用名称：" & Utf8toStr(MenuEditList(i).mnuText) & vbCrLf & vfb_LangString("两者的名称重复，名称必须是唯一的，不可以重复，请修改。"), "VisualFreeBasic", _
                        MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
                     text2.SetFocus
                     Return 2
                  End if
               End if
            Next
         End if
      Next
   End if
   
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   st->Rvalue  = GetMenuEdit(mCtrlTreeList1.GetRoot)
   MenuEditX = 0
End Function

Sub MenuEditForm_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if MenuEditX Then 
      if SaveMenuEdit() <> 0 Then Return  '保存数据
   End if 
   Me.Close
End Sub
Function GetMenuEdit(ByVal ztv As MC_HTREELISTITEM) As String
   Dim ss As String, pp As String, stv As MC_HTREELISTITEM
   Dim si As Long
   Do
      if ztv = 0 Then Exit Do
      si = mCtrlTreeList1.ItemData(ztv)
      if Len(ss) > 0 Then ss &= Chr(1)
      ss &= MenuEditList(si).zText & Chr(2) & _   'utf8 格式  菜单文字
         MenuEditList(si).mnuText & Chr(2) & _   'utf8 格式  代码里的名字
         MenuEditList(si).nKeys & Chr(2) & _     '快捷键
         MenuEditList(si).nSel & Chr(2) & _        '选中
         MenuEditList(si).Usable & Chr(2) & _       '可用状态
         MenuEditList(si).nDraw & Chr(2) & _       '自绘
         MenuEditList(si).nIco        '图标
      stv = mCtrlTreeList1.GetNextItem(ztv,MC_TLGN_CHILD )
      if stv Then
         pp = GetMenuEdit(stv)
         if Len(pp) Then ss &= Chr(1, 123, 1) & pp & Chr(1, 125)   '{ }
      End if
      ztv = mCtrlTreeList1.GetNextItem(ztv,MC_TLGN_NEXT )
   Loop
   Function = ss
End Function

Sub MenuEditForm_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim si As Long = ubound(MenuEditList) + 1
   ReDim Preserve MenuEditList(si)
   MenuEditList(si).zText = StrToUtf8(vfb_LangString("新菜单") & si)
   MenuEditList(si).mnuText = StrToUtf8( GetMenuDefault("") )
   MenuEditList(si).Usable = True
   Dim fvv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if fvv = 0 Then 
      fvv = MC_TLI_ROOT
   Else
      fvv = mCtrlTreeList1.GetNextItem(fvv,MC_TLGN_PARENT)      
   End if   
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.AddItem(fvv, vfb_LangString("新菜单") & si, si) 
   mCtrlTreeList1.Selection = vv
   MenuEditList(si).tv = vv 
   mCtrlTreeList1.SetItemText(vv, 1, GetMenuDefault(""))
   mCtrlTreeList1.SetItemText(vv, 2, MenuEditList(si).nKeys)
   mCtrlTreeList1.SetItemText(vv, 3, IIf(MenuEditList(si).nSel, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 4, IIf(MenuEditList(si).Usable, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 5, IIf(MenuEditList(si).nDraw, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 6,UTF8toCWSTR( MenuEditList(si).nIco))   
   
   SetMenuEdit(vv)
   MenuEditX =1
End Sub

Sub SetMenuEdit(vv As MC_HTREELISTITEM)
   
   if vv = 0 Then
      Text1.Enabled = False
      Text2.Enabled = False
      Combo1.Enabled = False
      Check1.Enabled = False
      Check2.Enabled = False
      Check3.Enabled = False
      Image1.Picture = ""
      Command9.Enabled = False
      Command14.Enabled = False
      Command5.Enabled = False
      Command6.Enabled = False
      Command4.Enabled = False
      Command7.Enabled = False
      Command11.Enabled = False
      Command12.Enabled = False
      Command15.Enabled =  False 
   Else
      Dim si As Long = mCtrlTreeList1.ItemData(vv)
      MenuEditZ = 1
      Text1.Enabled = True
      Text1.Text = Utf8toStr(MenuEditList(si).zText)
      Text2.Enabled = True
      Text2.Text = Utf8toStr(MenuEditList(si).mnuText)
      Combo1.Enabled = True
      Dim ss As CWSTR = Utf8toStr(MenuEditList(si).nKeys)
      Dim i As Long = Combo1.FindStringExact(0, ss)
      if i = -1 Then i = 0
      Combo1.ListIndex = i
      Check1.Enabled = True
      Check1.Value = MenuEditList(si).nSel
      Check2.Enabled = True
      Check2.Value = MenuEditList(si).Usable
      Check3.Enabled = True
      Check3.Value = MenuEditList(si).nDraw
      Dim pa As String = GetProRunFile(0,4)
      Dim bb As String = MenuEditList(si).nIco
      Dim ffi As Long = InStr(bb, "|")
      if ffi > 0 Then bb = left(bb, ffi -1)
      if bb = "iconfont" Then 
         Label7.Caption = Utf8toStr(MenuEditList(si).nIco)
         Image1.Picture =""
      Else
         Label7.Caption  =""
         Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      End if 
      Command9.Enabled = True
      Command14.Enabled = True
      Command5.Enabled = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_PREVIOUS ) <> 0
      Command6.Enabled = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_NEXT ) <> 0
      Command11.Enabled = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_PARENT ) <> 0
      Command12.Enabled = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_PREVIOUS ) <> 0
      Command4.Enabled = True
      Command7.Enabled = True
      Command15.Enabled = True
      MenuEditZ = 0
   end if
End Sub

Sub MenuEditForm_Command9_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   Dim bb As String = StrToUtf8(GetImgForm(Utf8toStr(MenuEditList(si).nIco),0))  '获取图像文件名称
   If bb <> MenuEditList(si).nIco Then
      MenuEditList(si).nIco = bb
      mCtrlTreeList1.SetItemText(vv, 6, UTF8toCWSTR(MenuEditList(si).nIco))         
      mCtrlTreeList1.Refresh 
      Dim ffi As Long = InStr(bb, "|")
      if ffi > 0 Then bb = left(bb, ffi -1)
      Dim pa As String = GetProRunFile(0,4)
      Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      Label7.Caption = ""
      MenuEditX = 1
   End If
End Sub

Sub MenuEditForm_Text1_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if MenuEditZ <> 0 Then Return 
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv =0 Then Return 
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   Dim tt As CWSTR = Text1.Text
   MenuEditList(si).zText = CWSTRtoUTF8(tt)
   mCtrlTreeList1.SetItemText(vv,0,tt)
   MenuEditX = 1
End Sub

Sub MenuEditForm_Text2_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if MenuEditZ <> 0 Then Return 
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv =0 Then Return 
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   Dim tt As CWSTR = Text2.Text
   MenuEditList(si).mnuText = CWSTRtoUTF8(tt)
   mCtrlTreeList1.SetItemText(vv, 1, tt)
   mCtrlTreeList1.Refresh 
   MenuEditX = 1
End Sub

Sub MenuEditForm_Combo1_CBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '列表框中更改当前选择时
   if MenuEditZ <> 0 Then Return
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   if Combo1.ListIndex = 0 Then
      MenuEditList(si).nKeys = ""
   Else
      Dim tt As CWSTR = Combo1.Text
      MenuEditList(si).nKeys = CWSTRtoUTF8(tt)
   End if
   mCtrlTreeList1.SetItemText(vv, 2, MenuEditList(si).nKeys)
   mCtrlTreeList1.Refresh 
   MenuEditX = 1
End Sub

Sub MenuEditForm_Check1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if MenuEditZ <> 0 Then Return
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   MenuEditList(si).nSel = Check1.Value  
   mCtrlTreeList1.SetItemText(vv, 3, IIf(MenuEditList(si).nSel, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.Refresh 
   MenuEditX = 1
End Sub

Sub MenuEditForm_Check2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if MenuEditZ <> 0 Then Return
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   MenuEditList(si).Usable = Check2.Value  
   mCtrlTreeList1.SetItemText(vv, 4, IIf(MenuEditList(si).Usable, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.Refresh 
   MenuEditX = 1
End Sub

Sub MenuEditForm_Check3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if MenuEditZ <> 0 Then Return
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   MenuEditList(si).nDraw = Check3.Value  
   mCtrlTreeList1.SetItemText(vv, 5, IIf(MenuEditList(si).nDraw, vfb_LangString("是"), vfb_LangString("否")))
   MenuEditX = 1
End Sub

Sub MenuEditForm_Text2_EN_SetFocus(hWndForm As hWnd, hWndControl As hWnd)  '得到输入焦点
   if Text2.TextLength = 0 Then
      Dim bb As String = GetMenuDefault(Text1.Text)
      Text2.Text = bb
      MenuEditX = 1
   End if
End Sub
Sub MenuEditForm_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim si As Long = ubound(MenuEditList) + 1
   ReDim Preserve MenuEditList(si)
   MenuEditList(si).zText = StrToUtf8(vfb_LangString("新子菜单") & si)
   MenuEditList(si).mnuText = StrToUtf8( GetMenuDefault("") )
   MenuEditList(si).Usable = True
   Dim fvv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if fvv = 0 Then    fvv = MC_TLI_ROOT
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.AddItem(fvv, StringToCWSTR(vfb_LangString("新子菜单") & si) , si) 
   mCtrlTreeList1.Selection = vv
   MenuEditList(si).tv = vv 
   mCtrlTreeList1.SetItemText(vv, 1, GetMenuDefault(""))
   mCtrlTreeList1.SetItemText(vv, 2, MenuEditList(si).nKeys)
   mCtrlTreeList1.SetItemText(vv, 3, IIf(MenuEditList(si).nSel, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 4, IIf(MenuEditList(si).Usable, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 5, IIf(MenuEditList(si).nDraw, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 6,UTF8toCWSTR( MenuEditList(si).nIco))      
   SetMenuEdit(vv)
   MenuEditX = 1
End Sub

Sub MenuEditForm_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim xvv As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_PREVIOUS ) 
   ChangeOfPosition(vv,xvv )  '交换位置
End Sub

Sub MenuEditForm_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim xvv As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_NEXT ) 
   ChangeOfPosition(vv,xvv )  '交换位置
End Sub

Sub MenuEditForm_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   Select Case MessageBox(hWndForm, vfb_LangString("你是不是真的要删除这个菜单？") & vbCrLf & _
            vfb_LangString("如果包含子菜单，那么将删除所有子菜单。"), "VisualFreeBasic", _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Case IDYES
         mCtrlTreeList1.DeleteItem vv
         SetMenuEdit 0
      Case IDNO
         Return 
   End Select
   '重新组织数组
   Dim Ltv() As MC_HTREELISTITEM '目录层次表
   Dim Ltvi As Long  '当前目录层次
   Dim tv As MC_HTREELISTITEM = mCtrlTreeList1.GetRoot '起始项目
   si = -1
   Dim  med() As MenuEdit_t
   Dim u As Long = UBound(MenuEditList)
   if u > -1 Then ReDim med(u)

   Do
      if tv = 0 Then Exit Do
      'tv 就是我们需要的，在这里可以写自己的代码了。
      dim ji As Long = mCtrlTreeList1.ItemData(tv)
      si += 1
      med(si) = MenuEditList(ji)
      med(si).tv = tv
      mCtrlTreeList1.ItemData(tv) = si
      Dim ztv As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(tv,MC_TLGN_CHILD ) '第一个子项目
      if ztv Then  '有子
         Ltvi += 1 '目录层次累计
         if Ltvi > UBound(Ltv) Then ReDim Preserve Ltv(Ltvi + 10)  '设置10层缓冲
         Ltv(Ltvi) = tv '备份
         tv = ztv
         Continue Do '继续下一个循环
      End if
      tv = mCtrlTreeList1.GetNextItem(tv,MC_TLGN_NEXT ) '下一个同级项目
      if tv = 0 Then
         Do
            if Ltvi = 0 Then Exit Do '最后1个了，没了。
            tv = Ltv(Ltvi)
            Ltvi -= 1 '目录层次恢复
            tv = mCtrlTreeList1.GetNextItem(tv,MC_TLGN_NEXT ) '下一个同级项目
            if tv Then Exit Do
         Loop
         if Ltvi = 0 And tv = 0 Then Exit Do '最后1个了，没了。
      End if
   Loop
   if si = -1 Then
      Erase MenuEditList
   Else
      ReDim MenuEditList(si)
      for i As Long = 0 To si
         MenuEditList(i) = med(i)
      Next
   End if
   MenuEditX = 1
   SetMenuEdit mCtrlTreeList1.Selection
   mCtrlTreeList1.SetFocus 
End Sub

Sub MenuEditForm_Command10_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim si As Long = ubound(MenuEditList) + 1
   ReDim Preserve MenuEditList(si)
   MenuEditList(si).zText = StrToUtf8(vfb_LangString("新菜单") & si)
   MenuEditList(si).mnuText = StrToUtf8( GetMenuDefault("") )
   MenuEditList(si).Usable = True
   Dim fvv As MC_HTREELISTITEM 
   Dim tv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if tv = 0 Then 
      fvv = MC_TLI_ROOT
   Else
      fvv = mCtrlTreeList1.GetNextItem(tv,MC_TLGN_PARENT )      
   End if   
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.InsertItem(fvv,tv, "新菜单" & si , si) 
   mCtrlTreeList1.Selection = vv
   MenuEditList(si).tv = vv 
   mCtrlTreeList1.SetItemText(vv, 1, GetMenuDefault(""))
   mCtrlTreeList1.SetItemText(vv, 2, MenuEditList(si).nKeys)
   mCtrlTreeList1.SetItemText(vv, 3, IIf(MenuEditList(si).nSel, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 4, IIf(MenuEditList(si).Usable, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 5, IIf(MenuEditList(si).nDraw, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(vv, 6, UTF8toCWSTR(MenuEditList(si).nIco))   
      
   SetMenuEdit(vv)
   mCtrlTreeList1.SetFocus 
   MenuEditX =1
End Sub

Sub ChangeOfPosition(tv1 As MC_HTREELISTITEM, tv2 As MC_HTREELISTITEM)  '交换位置
   Dim aa1 As CWSTR = mCtrlTreeList1.GetItemText(tv1, 0)
   Dim bb1 As Long = mCtrlTreeList1.ItemData(tv1)
   Dim ff1 As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(tv1, MC_TLGN_PARENT)
   Dim aa2 As CWSTR = mCtrlTreeList1.GetItemText(tv2, 0)
   Dim bb2 As Long = mCtrlTreeList1.ItemData(tv2)
   Dim ff2 As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(tv2, MC_TLGN_PARENT)
   
   Dim cc1 As MC_HTREELISTITEM = mCtrlTreeList1.InsertItem(ff2, tv2, aa1, bb1)
   MenuEditList(bb1).tv = cc1
   mCtrlTreeList1.SetItemText(cc1, 1, UTF8toCWSTR(MenuEditList(bb1).mnuText))
   mCtrlTreeList1.SetItemText(cc1, 2, MenuEditList(bb1).nKeys)
   mCtrlTreeList1.SetItemText(cc1, 3, IIf(MenuEditList(bb1).nSel, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 4, IIf(MenuEditList(bb1).Usable, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 5, IIf(MenuEditList(bb1).nDraw, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 6,UTF8toCWSTR( MenuEditList(bb1).nIco))
   
   Dim cc2 As MC_HTREELISTITEM = mCtrlTreeList1.InsertItem(ff1, tv1, aa2, bb2)
   MenuEditList(bb2).tv = cc2
   mCtrlTreeList1.SetItemText(cc2, 1, UTF8toCWSTR(MenuEditList(bb2).mnuText))
   mCtrlTreeList1.SetItemText(cc2, 2, MenuEditList(bb2).nKeys)
   mCtrlTreeList1.SetItemText(cc2, 3, IIf(MenuEditList(bb2).nSel, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc2, 4, IIf(MenuEditList(bb2).Usable, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc2, 5, IIf(MenuEditList(bb2).nDraw, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc2, 6,UTF8toCWSTR( MenuEditList(bb2).nIco))
   
   Dim stv As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(tv1, MC_TLGN_CHILD)
   if stv Then CopyTreeView(stv, cc1)
   
   stv = mCtrlTreeList1.GetNextItem(tv2, MC_TLGN_CHILD)
   if stv Then CopyTreeView(stv, cc2)
   
   mCtrlTreeList1.DeleteItem tv1
   mCtrlTreeList1.DeleteItem tv2
   
   mCtrlTreeList1.Selection = cc1
   mCtrlTreeList1.SetFocus
   MenuEditX = 1
End Sub
Sub CopyTreeView(byval tv1 As MC_HTREELISTITEM, byval tv2 As MC_HTREELISTITEM)
   Dim stv As MC_HTREELISTITEM
   Do
      if tv1 = 0 Then Exit Do
      Dim aa1 As CWSTR = mCtrlTreeList1.GetItemText(tv1,0)
      Dim bb1 As Long = mCtrlTreeList1.ItemData(tv1)
      Dim cc2 As MC_HTREELISTITEM = mCtrlTreeList1.AddItem(tv2, aa1, bb1)
      MenuEditList(bb1).tv = cc2
      mCtrlTreeList1.SetItemText(cc2, 1, UTF8toCWSTR(MenuEditList(bb1).mnuText))
      mCtrlTreeList1.SetItemText(cc2, 2, MenuEditList(bb1).nKeys)
      mCtrlTreeList1.SetItemText(cc2, 3, IIf(MenuEditList(bb1).nSel, vfb_LangString("是"), vfb_LangString("否")))
      mCtrlTreeList1.SetItemText(cc2, 4, IIf(MenuEditList(bb1).Usable, vfb_LangString("是"), vfb_LangString("否")))
      mCtrlTreeList1.SetItemText(cc2, 5, IIf(MenuEditList(bb1).nDraw, vfb_LangString("是"), vfb_LangString("否")))
      mCtrlTreeList1.SetItemText(cc2, 6, UTF8toCWSTR(MenuEditList(bb1).nIco))
      
      stv = mCtrlTreeList1.GetNextItem(tv1,MC_TLGN_CHILD )
      if stv Then CopyTreeView(stv, cc2)
      tv1 = mCtrlTreeList1.GetNextItem(tv1,MC_TLGN_NEXT )
   Loop
End Sub

Sub MenuEditForm_Command11_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim xvv As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_PARENT ) 
   if xvv = 0 Then Return
   Dim aa1 As CWSTR = mCtrlTreeList1.GetItemText(vv,0)
   Dim bb1 As Long = mCtrlTreeList1.ItemData(vv) 
   Dim cc1 As MC_HTREELISTITEM = mCtrlTreeList1.InsertItem(mCtrlTreeList1.GetNextItem(xvv,MC_TLGN_PARENT ),xvv , aa1, bb1)
   MenuEditList(bb1).tv = cc1 
   mCtrlTreeList1.SetItemText(cc1, 1, UTF8toCWSTR(MenuEditList(bb1).mnuText))
   mCtrlTreeList1.SetItemText(cc1, 2, MenuEditList(bb1).nKeys)
   mCtrlTreeList1.SetItemText(cc1, 3, IIf(MenuEditList(bb1).nSel, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 4, IIf(MenuEditList(bb1).Usable, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 5, IIf(MenuEditList(bb1).nDraw, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 6,UTF8toCWSTR( MenuEditList(bb1).nIco))   
   
   Dim stv As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_CHILD )
   if stv Then CopyTreeView(stv, cc1)   
   mCtrlTreeList1.DeleteItem vv
   
   mCtrlTreeList1.Selection = cc1 
   mCtrlTreeList1.SetFocus   
   MenuEditX = 1        
   
End Sub

Sub MenuEditForm_Command12_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim xvv As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_PREVIOUS ) 
   if xvv = 0 Then Return
   Dim aa1 As CWSTR = mCtrlTreeList1.GetItemText(vv,0)
   Dim bb1 As Long = mCtrlTreeList1.ItemData(vv) 
   Dim cc1 As MC_HTREELISTITEM = mCtrlTreeList1.AddItem(xvv , aa1, bb1)
   MenuEditList(bb1).tv = cc1 
   mCtrlTreeList1.SetItemText(cc1, 1, UTF8toCWSTR(MenuEditList(bb1).mnuText))
   mCtrlTreeList1.SetItemText(cc1, 2, MenuEditList(bb1).nKeys)
   mCtrlTreeList1.SetItemText(cc1, 3, IIf(MenuEditList(bb1).nSel, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 4, IIf(MenuEditList(bb1).Usable, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 5, IIf(MenuEditList(bb1).nDraw, vfb_LangString("是"), vfb_LangString("否")))
   mCtrlTreeList1.SetItemText(cc1, 6,UTF8toCWSTR( MenuEditList(bb1).nIco))     
   
   
   Dim stv As MC_HTREELISTITEM = mCtrlTreeList1.GetNextItem(vv,MC_TLGN_CHILD )
   if stv Then CopyTreeView(stv, cc1)   
   mCtrlTreeList1.DeleteItem vv
   
   mCtrlTreeList1.Selection = cc1 
   mCtrlTreeList1.SetFocus   
   MenuEditX = 1 
End Sub

Sub MenuEditForm_Command13_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim ss As String = "   Select Case wID" & vbCrLf
   Dim u As Long = ubound(MenuEditList)
   if u > -1 Then
      for i As Long = 0 To u
         if MenuEditList(i).zText = "{" Or MenuEditList(i).zText = "}" Or MenuEditList(i).zText = "-" Then
            
         Else
            ss &= "      Case " & Utf8toStr(MenuEditList(i).mnuText) & " ' " & Utf8toStr(MenuEditList(i).zText) & vbCrLf  & vbCrLf
         End if
      Next
   End if
   ss &= "   End Select"
   FF_ClipboardSetText ss
   
   MessageBox(hWndForm, StringToCWSTR( vfb_LangString("已经复制 Select 代码到系统剪切板。") & vbCrLf & _
      "将其粘贴到菜单事件中即可。"), "VisualFreeBasic", _
      MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
   
End Sub

Sub MenuEditForm_Command14_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
 
   Dim vv As MC_HTREELISTITEM = mCtrlTreeList1.Selection
   if vv = 0 Then Return
   Dim si As Long = mCtrlTreeList1.ItemData(vv)
   Dim bb As String = MenuEditList(si).nIco  '获取图像文件名称
   iconfontEdit.Show(hWndForm, True,Cast(Integer, @bb ))
   If bb <> MenuEditList(si).nIco Then
      MenuEditList(si).nIco = bb
      mCtrlTreeList1.SetItemText(vv, 6, UTF8toCWSTR(MenuEditList(si).nIco))  
      mCtrlTreeList1.Refresh        
      Image1.Picture = "" 
      Label7.Caption =  Utf8toStr(bb)
      MenuEditX = 1
   End If   
End Sub

Function GetMenuDefault(pp As String) As String '获取菜单默认名称
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   Dim bb As String = Trim(pp)
   Dim mm As String = st->FomName & "_" & st->nName & "_"
   Dim i As Long, aa As Long, cc As String, kk As Long
   if Len(bb) Then
      For i As Long = 1 To Len(bb)
         if InStr("~!@#$%^&*()+-={}[]\|:;""',.<>/? ", Mid(bb, i, 1)) Then Mid(bb, i, 1) = "_"
      Next
   End if
   aa = 1
   if Len(bb) = 0 Then
      mm &= "Menu"
      bb = Str(aa)
   ElseIf bb = "-" Then
      bb = Str(aa)
   Else
      mm &= bb
      bb = ""
   End if
   '检查有没重复的名称，有就后面增加数字
   Do
      cc = StrToUtf8(mm & bb)
      kk = 0
      for i = 0 To UBound(MenuEditList)
         if UCase(cc) = UCase(MenuEditList(i).mnuText) Then
            kk = 1
            Exit for
         End if
      Next
      if kk = 0 Then Exit Do
      aa += 1
      bb = Str(aa)
   Loop
   Function = Utf8toStr(cc)
  
End Function

Sub MenuEditForm_Command15_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim bb As String = Text1.Text 
   bb = GetMenuDefault(bb)
   Text2.Text =bb 
End Sub

Sub MenuEditForm_mCtrlTreeList1_MC_TLN_SelChanged(hWndForm As hWnd, hWndControl As hWnd, NM As MC_NMTREELIST)  '已经更改选择项
   '当不使用样式MC_TLS_MULTISELECT时，MC_NMTREELIST描述选择的变化方式：成员hItemOld和lParamOld描述旧选择，成员hItemNew和lParamNew指定新选择。                 
   '但是，当使用样式MC_TLS_MULTISELECT时，通知的行为将有所不同：它仅向控件发送一次通知，并且所有MC_NMTREELIST成员均设置为NULL。应用程序可以使用消息MC_TLM_GETNEXTITEM遍历当前选定的项目。
   '  NM.hItemOld   As MC_HTREELISTITEM  旧选择的句柄
   '  NM.lParamOld  As LPARAM            旧选择的数据
   '  NM.hItemNew   As MC_HTREELISTITEM  新选择的句柄
   '  NM.lParamNew  As LPARAM            新选择的数据
   SetMenuEdit NM.hItemNew
End Sub























































