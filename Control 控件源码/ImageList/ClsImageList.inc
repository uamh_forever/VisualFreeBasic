Type Class_ImageList
Private : 
   m_hWndForm As HWnd '控件句柄
   m_IDC      As Long '控件IDC
   Declare Function GetFP() As FormControlsPro_TYPE ptr '返回控件保存数据的指针（一般是类自己用）
Public : 

   Declare Property hWndForm() As.hWnd '返回/设置控件所在的窗口句柄，主要用于多开同一个窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
   Declare Property hWndForm(ByVal hWndParent As.hWnd) '获取控件所在的窗口句柄（父窗口）
   Declare Property IDC() As Long '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Property Tag() As CWSTR '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWSTR)
   Declare Sub SetSize(w As Long ,h As Long) '设置图像尺寸（像素），将会销毁原图像列表，重新创建，背景色需要重新设置才行。绑定其它控件，必须要重新绑定。
   Declare Function GetSizeW()                As Long       '获取图像尺寸（像素）
   Declare Function GetSizeH()                As Long       '获取图像尺寸（像素）
   Declare Function GethImageList()           As HIMAGELIST '获取图像列表句柄，给TV LV 绑定用
   Declare Function Add(ResImg As String)     As Long       '新增图像（需要资源名称），成功返回新图像的索引，否则返回-1。
   Declare Function AddFile(FileImg As CWSTR) As Long       '新增图像（从文件中打开），成功返回新图像的索引，否则返回-1。
   Declare Function AddIco(nIcon As HICON)    As Long       '直接提供ICO句柄新增，新增后ICO句柄可以销毁，本函数不销毁句柄，成功返回新图像的索引，否则返回-1。
   Declare Property BackColor()               As Long       '返回/设置背景色：设置用：BGR(r, g, b) 返回或设置 CLR_NONE 是 使用蒙版透明地绘制图像
   Declare Property BackColor(nColor As Long)
   Declare Property BackColorGDIplue() As ARGB '返回/设置背景色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)  返回或设置 CLR_NONE 是 使用蒙版透明地绘制图像
   Declare Property BackColorGDIplue(GDIplue_Color As ARGB)
   Declare Function Draw(DC As HDC ,i As Long ,x As Long ,y As Long) As Long    '在指定的设备上下文中绘制图像列表项。成功返回非零值，否则返回零。
   Declare Function GetIcon(i As Long)                               As HICON   '从图像列表中的图像创建一个图标。成功返回图像列表的句柄，否则返回NULL。由调用者负责使用DestroyIcon函数销毁图标。
   Declare Function remove(i As Long)                                As WINBOOL '从图像列表中的移除图标。成功返回非零值，否则返回零。删除图像时，将调整剩余图像的索引，以使图像索引始终范围从零到小于图像列表中图像的数量。例如，如果删除索引为0的图像，则图像1变为图像0，图像2变为图像1，依此类推。
   Declare Sub Clear()   '删除图像列表中所有图像。
   Declare Function GetImageCount()    As Long '返回图像数量。
   Declare Function BeginDrag(i As Long,x As Long ,y As Long)As WINBOOL '开始拖动图像。成功返回非零值，否则返回零。
   Declare Function DragEnter(hwndLock As hWnd,x As Long ,y As Long)As WINBOOL '在窗口内的指定位置显示拖动图像。成功返回非零值，否则返回零。
   Declare Function DragLeave(hwndLock As hWnd)As WINBOOL '解锁指定的窗口并隐藏拖动图像，允许更新窗口。成功返回非零值，否则返回零。
   Declare Function DragMove(x As Long ,y As Long)As WINBOOL '在拖放操作过程中移动正在拖动的图像。此函数通常在响应WM_MOUSEMOVE消息时被调用。成功返回非零值，否则返回零。
   Declare Function DragShowNolock(fShow As WINBOOL)As WINBOOL '显示或隐藏正在拖动的图像。成功返回非零值，否则返回零。
   Declare Sub EndDrag()   '结束拖动图像操作。
   
   
   Declare Property UserData(idx As Long) As Integer '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx As Long ,bValue As Integer)
   'fp->CtlData(0)  0=列表句柄  1=宽度 2=高度
   'Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
   'If zz =0 Then Return -1
   '{代码不提示}
   Declare Sub UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
End Type

Sub Class_ImageList.UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp Then
      'fp->CtlData()  0=列表句柄  1=宽度 2=高度
      Dim zz As HIMAGELIST = Cast(HIMAGELIST,fp->CtlData(0))
      If zz Then ImageList_Destroy zz
      fp->CtlData(0)=0
   End If    
End Sub


Function Class_ImageList.GetFP() As FormControlsPro_TYPE Ptr '返回自己控件的指针
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(m_hWndForm)
   While fp
      if fp->IDC = m_IDC Then Return fp      
      fp = fp->VrControls
   Wend

End Function
Property Class_ImageList.hWndForm() As .hWnd         '返回/设置控件所在的窗口句柄
      Return m_hWndForm
End Property
Property Class_ImageList.hWndForm(ByVal hWndParent As .hWnd)
   m_hWndForm = hWndParent
End Property
Property Class_ImageList.IDC() As Long     
   Return m_IDC
End Property
Property Class_ImageList.IDC(ByVal NewIDC As Long)
   m_IDC  =NewIDC
End Property
Sub Class_ImageList.Clear()
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz = 0 Then Return   
      ImageList_RemoveAll(zz)    
      'Dim u As Long = ImageList_GetImageCount(zz)
      'If u Then 
      '   Dim i As Long 
      '   For i = u -1 To 0 Step -1
      '      ImageList_Remove(zz ,i)
      '   Next     
      'End If 
   End If
End Sub
Sub Class_ImageList.SetSize(w As Long ,h As Long) '设置图像尺寸（像素），将会销毁原图像列表，重新创建。
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz     Then ImageList_Destroy zz
      If w = 0  Then w = 16
      if h = 0  Then h = 16
      zz = ImageList_Create(w ,h ,ILC_COLOR32 ,10 ,10)
      fp->CtlData(0) = Cast(Integer ,zz)
      fp->CtlData(1) = w
      fp->CtlData(2) = h
   End If
   
End Sub
Function Class_ImageList.GetSizeW() As Long  '获取图像尺寸（像素）
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      Return fp->CtlData(1) 
   End If
End Function
Function Class_ImageList.GetSizeH() As Long  '获取图像尺寸（像素）
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return fp->CtlData(2) 
   End If
End Function
Function Class_ImageList.GethImageList() As HIMAGELIST '获取图像列表句柄，给TV LV 绑定用
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp Then
      Function =Cast(HIMAGELIST, fp->CtlData(0)) 
   End If
End Function
Function Class_ImageList.Add(ResImg As String) As Long  '新增图像（需要资源名称），成功返回新图像的索引，否则返回-1。
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   Dim r As Long = -1
   if fp then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz =0 Then Return -1 
      Dim nIcon As HICON
      If Left(ResImg, 7) = "BITMAP_" Then
         Dim nBmp As HBITMAP = LoadImageA(App.HINSTANCE, ResImg, IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR)
         Dim po As ICONINFO
         po.fIcon = TRUE
         po.hbmColor = nBmp
         po.hbmMask = nBmp
         nIcon = CreateIconIndirect(@po)
         DeleteObject nBmp
      ElseIf Left(ResImg, 5) = "ICON_"  Or ResImg="AAAAA_APPICON" Then
         'nIcon = LoadImageA(app.hInstance, ResImg, IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR)  '从资源里加载图标
         nIcon = LoadIconA(App.HINSTANCE ,ResImg)
      Else
         nIcon = AfxGdipIconFromRes(App.HINSTANCE, ResImg)
      End if
      r = ImageList_AddIcon( zz, nIcon)
      DestroyIcon nIcon
   End If
   Function = r
End Function
Function Class_ImageList.AddFile(FileImg As CWSTR) As Long
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   Dim r  As Long                     = -1
   if fp then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz =0 Then Return -1 
      Dim nIcon As HICON
      If LCase(Right(FileImg, 4))=".cur" Or LCase(Right(FileImg, 4))=".ani" Then
          nIcon=LoadImage(NULL ,FileImg ,IMAGE_CURSOR ,This.GetSizeW() ,This.GetSizeH() ,LR_LOADFROMFILE)
      else
          nIcon = AfxGdipIconFromFile(FileImg)
      End If 
      If nIcon Then
         r = ImageList_AddIcon( zz ,nIcon)
         DestroyIcon nIcon
      Else
         r = -1
      End if
   End If
   Function = r
End Function
Function Class_ImageList.AddIco(nIcon As HICON) As Long
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   Dim r  As Long                     = -1
   If fp Then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz =0 Then Return -1 
      If nIcon Then
         r = ImageList_AddIcon( zz ,nIcon)
      Else
         r = -1
      End If
   End If
   Function = r
End Function
Property Class_ImageList.BackColor() As Long                  '返回/设置背景色： RGB 颜色值(用 BGR(r, g, b) 获取)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->BackColor)
      Return cc      
   end if
End Property
Property Class_ImageList.BackColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->BackColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz Then ImageList_SetBkColor zz ,nColor
   End If
End Property
Property Class_ImageList.BackColorGDIplue() As ARGB     'GDI+ 背景色返回
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->BackColor)
      If cc = 0 Then  cc = CLR_NONE
      Return cc
   End If
End Property
Property Class_ImageList.BackColorGDIplue(ByVal nColor As ARGB) 'GDI+ 背景色设置
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr ,@nColor)
      Dim c As Long      = BGRA(b[2] ,b[1] ,b[0] ,b[3]) 'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->BackColor = c
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz = 0 Then Return
      Dim cc As Long = GetCodeColorGDI(fp->BackColor)
      ImageList_SetBkColor zz ,cc 'CLR_NONE
   End If
End Property

Function Class_ImageList.Draw(DC AS HDC ,i AS LONG ,x AS LONG ,y AS LONG) As Long '在指定的设备上下文中绘制图像列表项。成功返回非零值，否则返回零。
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz = 0 Then Return 0
      Function = ImageList_Draw( zz ,i ,DC ,x ,y ,ILD_TRANSPARENT)
   End If
End Function
Function Class_ImageList.GetIcon(i As Long) As HICON
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz = 0 Then Return 0
      Function = ImageList_GetIcon(zz ,i ,ILD_TRANSPARENT)
   End If
End Function
Function Class_ImageList.remove(i As Long) As WINBOOL
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz = 0 Then Return 0
      Function = ImageList_Remove( zz ,i)
   End If
End Function
Property Class_ImageList.UserData(idx As Long) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return 0
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp  Then
      Return fp->UserData(idx)
   End If   

End Property
Property Class_ImageList.UserData(idx As Long, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      fp->UserData(idx) = bValue
   End If    
End Property
Function Class_ImageList.GetImageCount() As Long '返回图像数量。
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz = 0 Then Return 0
      Return ImageList_GetImageCount(zz)
   End If
   Function = 0
End Function
Function Class_ImageList.BeginDrag(i As Long ,x As Long ,y As Long)As WINBOOL '开始拖动图像。
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz = 0 Then Return 0
      Return ImageList_BeginDrag(zz ,i ,x ,y)
   End If
   Function = 0
End Function
Function Class_ImageList.DragEnter(hwndLock As hWnd,x As Long ,y As Long)As WINBOOL
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      Dim zz As HIMAGELIST = Cast(HIMAGELIST ,fp->CtlData(0))
      If zz = 0 Then Return 0
      Return ImageList_DragEnter(hwndLock ,x ,y)
   End If
   Function = 0
End Function
Function Class_ImageList.DragLeave(hwndLock As hWnd)As WINBOOL
   Return ImageList_DragLeave(hwndLock)
End Function
Function Class_ImageList.DragMove(x As Long ,y As Long)As WINBOOL
   Return ImageList_DragMove(x,y)
End Function
Function Class_ImageList.DragShowNolock(fShow As WINBOOL)As WINBOOL
   Return ImageList_DragShowNolock(fShow)
End Function
Sub Class_ImageList.EndDrag()
   ImageList_EndDrag()
End Sub 




