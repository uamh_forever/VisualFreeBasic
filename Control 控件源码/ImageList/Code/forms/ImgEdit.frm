﻿#VisualFreeBasic_Form#  Version=5.5.8
Locked=0

[Form]
Name=ImgEdit
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP
Style=3 - 常规窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=图像列表编辑
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=412
Height=468
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=1 - 固定行高自绘
ItemHeight=18
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=7
Top=6
Width=251
Height=426
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Frame]
Name=Frame1
Index=-1
Caption=1:1 图像预览
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9
Left=265
Top=7
Width=130
Height=108
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Image]
Name=Image1
Index=-1
Picture=
Stretch=0 - 自动适应
Percent=0
GrayScale=False
Enabled=True
Visible=True
Left=271
Top=26
Width=118
Height=84
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=选择图像
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=327
Top=257
Width=69
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line1
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无箭头
ArrowStartH=0 - 无箭头
ArrowEndW=0 - 无箭头
ArrowEndH=0 - 无箭头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=264
Top=391
Width=135
Height=4
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=确定
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=272
Top=401
Width=55
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=339
Top=401
Width=55
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=新增
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=265
Top=290
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=插入
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=265
Top=321
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=265
Top=355
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame2
Index=-1
Caption=放大 图像预览
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9
Left=266
Top=122
Width=130
Height=126
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Image]
Name=Image2
Index=-1
Picture=
Stretch=0 - 自动适应
Percent=0
GrayScale=False
Enabled=True
Visible=True
Left=275
Top=145
Width=114
Height=97
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=▲
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=266
Top=258
Width=21
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command8
Index=-1
Caption=▼
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=291
Top=258
Width=21
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]
Dim Shared ImgEditList() As String , ImgEditX As Long ' Z 正在设置  X 被修改

Sub ImgEdit_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    Me.Close 
End Sub

Sub ImgEdit_Shown(hWndForm As hWnd ,UserData As Integer) '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   Me.UserData(0) = UserData
   Dim As Rect rc ,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr ,UserData)
   GetWindowRect(st->hWndForm ,@rc2)
   GetWindowRect(st->hWndList ,@rc)
   if rc.Left + Me.Width > rc2.right  Then rc.Left = rc2.right  - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top  = rc2.bottom - Me.Height
   Me.Move rc.Left ,rc.top
   Dim z  As ZString Ptr = Cast(Any Ptr ,FF_ListBox_GetItemData(st->hWndList ,3))
   Dim ww As Long        = Val( *z)
   z = Cast(Any Ptr ,FF_ListBox_GetItemData(st->hWndList ,4))
   Dim hh As Long = Val( *z)
   If ww < 6   Then ww = 6
   If hh < 6   Then hh = 6
   If ww > 120 Then ww = 120
   If hh > 85  Then hh = 85
   
   Image1.Move AfxScaleX(331-ww/2),AfxScaleY(67-hh/2),AfxScaleX(ww),AfxScaleY(hh)
   
   
   Dim ss As String = *st->value
   if Len(ss) = 0 Then
      Erase ImgEditList
   Else
      Dim el() As String ,u As Long ,i As Long
      u = vbSplit(ss ,Chr(1) ,el())
      ReDim ImgEditList(u -1)
      Dim si As Long = -1
      for i = 0 To u -1
         ImgEditList(i) = el(i)
         List1.AddItem ""
      Next
      
   End if
   List1.ListIndex = List1.ListCount -1
   ImgEdit_List1_LBN_SelChange 0 ,0
   ImgEditX = 0
End Sub

Sub ImgEdit_List1_LBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '选择了列表
   Dim si As Long = List1.ListIndex
   if si = -1 Then
      Command1.Enabled = False
      Command5.Enabled = False
      Command6.Enabled = False
      Command7.Enabled = False
      Command8.Enabled = False
   Else
      Command1.Enabled = True
      Command5.Enabled = True
      Command6.Enabled = True
      Command7.Enabled = si > 0
      Command8.Enabled = si < List1.ListCount -1
      Dim pa As String =GetProRunFile(0,4)
      Dim bb As String = ImgEditList(si)
      Dim ffi As Long = InStr(bb, "|")
      if ffi > 0 Then bb = left(bb, ffi -1)
      Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      Image2.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
   End if
End Sub

Function ImgEdit_List1_OwnerDraw(hWndForm As hWnd, hWndControl As hWnd,lpdis As DRAWITEMSTRUCT ) As LResult  '自绘控件（需要设计时选择自绘属性）
   Dim rc As Rect
   If lpdis.itemID = &HFFFFFFFF Then Return 0 '如果列表为空 =-1
   rc = lpdis.rcItem  '当前行绘画范围，多余操作，就是嫌弃 lpdis.rcItem 太长，
   Select Case lpdis.itemAction
      Case ODA_DRAWENTIRE, ODA_SELECT '要绘画消息
         Dim ki As Long = lpdis.itemID  '也是多余操作，就是嫌弃 lpdis.itemID 太长，
         Dim gg As yGDI = yGDI(lpdis.hDC, 0, rc.Left, rc.top, rc.Right - rc.Left, rc.bottom - rc.top)
         Dim As Long w=AfxUnscaleX(rc.Right - rc.Left),h= AfxUnscaleX(rc.bottom - rc.top)
         gg.Pen 0, 0
         gg.Brush GetSysColor(COLOR_WINDOW)
         gg.DrawFrame 0, 0, w + 1, h + 1
         If (lpdis.itemState And ODS_SELECTED) = 0 Then                  ' 未选中
            gg.SetColor GetSysColor(COLOR_WINDOWTEXT)
         Else                                                             ' 处于选中状态
            gg.Pen 1, GetSysColor(COLOR_HIGHLIGHT)
            gg.Brush GetSysColor(COLOR_HIGHLIGHT)
            gg.DrawFrame 0, 0, w, h -1
            gg.SetColor GetSysColor(COLOR_HIGHLIGHTTEXT)
         End If
         gg.DrawTextS 0, 0, w, h,ki & " . " & ImgEditList(ki) , DT_SINGLELINE Or DT_LEFT Or DT_VCENTER
         Function = True '告诉系统，表示自己画了，不需要系统处理
   End Select
End Function

Sub ImgEdit_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   Dim bb As String = StrToUtf8(GetImgForm(Utf8toStr(ImgEditList(si)),0))  '获取图像文件名称
   If bb <> ImgEditList(si) Then
      ImgEditList(si) = bb
      Dim ffi As Long = InStr(bb, "|")
      if ffi > 0 Then bb = left(bb, ffi -1)
      Dim pa As String = GetProRunFile(0,4)
      Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      Image2.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      ImgEditX = 1
      List1.Refresh 
   End If
End Sub

Sub ImgEdit_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ImgEditList) + 1
   ReDim Preserve ImgEditList(u)

   List1.ListIndex = List1.AddItem("")
   ImgEdit_List1_LBN_SelChange 0, 0
   ImgEditX = 1
   List1.Refresh
   Command1.Click
End Sub

Sub ImgEdit_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  Dim u As Long = ubound(ImgEditList) + 1
   ReDim Preserve ImgEditList(u)

   Dim i As Long, si As Long 
   si = List1.ListIndex
   if si = -1 Then Return
   List1.ListIndex = List1.AddItem ("")
   for i = u To si + 1 Step -1
      ImgEditList(i) = ImgEditList(i -1)
   Next
   ImgEdit_List1_LBN_SelChange 0, 0
   ImgEditX = 1
   List1.Refresh
   Command1.Click
End Sub

Sub ImgEdit_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Select Case MessageBox(hWndForm,  vfb_LangString("你真的要删除吗？"), "VisualFreeBasic", _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON2 Or MB_APPLMODAL)
      Case IDYES
      Case IDNO
         Return
   End Select
   Dim u As Long = UBound(ImgEditList)
   if u = 0 Then
      Erase ImgEditList
      List1.Clear
   Else
      Dim si As Long = List1.ListIndex
      if si = -1 Then Return
      if si < u Then
         for i As Long = si To u -1
            ImgEditList(i) = ImgEditList(i + 1)
         Next
      End if
      u -= 1
      ReDim Preserve ImgEditList(u)
      List1.RemoveItem 0
   End if

   ImgEdit_List1_LBN_SelChange 0, 0
   ImgEditX = 1
   List1.Refresh
End Sub

Sub ImgEdit_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ImgEditList)
   Dim si As Long = List1.ListIndex
   if si <0 Then Return
   Swap ImgEditList(si), ImgEditList(si -1)
   List1.ListIndex = si -1
   ImgEdit_List1_LBN_SelChange 0, 0
   ImgEditX = 1
   List1.Refresh
End Sub

Sub ImgEdit_Command8_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ImgEditList)
   Dim si As Long = List1.ListIndex
   if si = -1 Or si >= u Then Return
   Swap ImgEditList(si), ImgEditList(si + 1)
   List1.ListIndex = si +1
   ImgEdit_List1_LBN_SelChange 0, 0
   ImgEditX = 1
   List1.Refresh
End Sub

Function ImgEdit_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   if ImgEditX <> 0 Then
      Select Case MessageBox(hWndForm,  vfb_LangString("图像列表已经被修改，你要保存修改吗？"), "VisualFreeBasic", _
               MB_YESNOCANCEL Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Case IDYES
             SaveImgEdit() '保存数据
         Case IDNO
         Case IDCANCEL
            Return 1
      End Select
   End if      

    Function = 0 '根据自己需要修改(函数必须有个返回，否则编译会警告)
End Function

Sub SaveImgEdit() '保存数据
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   Dim ss As String
   st->Rvalue = FF_Join(ImgEditList(),Chr(1))
    ImgEditX = 0
End Sub

Sub ImgEdit_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ImgEditX Then   SaveImgEdit '保存数据

   Me.Close  
End Sub
















