Type Class_Button Extends Class_Control
    Declare Property Caption() As CWStr                '返回/设置控件中的文本
    Declare Property Caption(ByVal sText As CWStr)
    Declare Sub Click() '模拟用户点击了这个按钮
    Declare Sub IcoRes(nResImg As String) '设置 图像资源名，就是在图像管理器中的名称， 空名称，将去掉图标    
End Type
'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_Button.Caption() As CWStr
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_Button.Caption(ByVal sText As CWStr)
  AfxSetWindowText  hWndControl, sText
End Property
Sub Class_Button.Click() '模拟用户点击了这个按钮
  SendMessage(hWndControl, BM_CLICK, 0, 0)
End Sub
Sub Class_Button.IcoRes(nResImg As String)

   If Len(nResImg) = 0 Then
      AfxRemoveWindowStyle hWndControl, BS_ICON
      SendMessage(hWndControl, BM_SETIMAGE, IMAGE_ICON, Cast(lParam, 0))   '默认为 SendMessageW
      Return
   End If
   Dim nIcon As HICON
   '目前只支持ICO，将来可能会支持所有的
   'If InStr(nResImg, "BITMAP_") = 1 Then
   '   Dim nBmp As HBITMAP = LoadImageA(App.HINSTANCE, nResImg, IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR)
   '   Dim po As ICONINFO
   '   po.fIcon = TRUE
   '   po.hbmColor = nBmp
   '   po.hbmMask = nBmp
   '   nIcon = CreateIconIndirect(@po)
   '   DeleteObject nBmp
   'Else
   If InStr(nResImg, "ICON_") = 1  Or nResImg="AAAAA_APPICON" Then
      nIcon = LoadImageA(App.HINSTANCE ,nResImg ,IMAGE_ICON ,0 ,0 , LR_DEFAULTSIZE Or LR_DEFAULTCOLOR) '从资源里加载图标
   Else
      nIcon = AfxGdipIconFromRes(App.HINSTANCE, nResImg)   
   End if
   If nIcon Then
      If Len(AfxGetWindowText(hWndControl)) = 0 Then  '当有文字时，BS_ICON 必须删除才能显示图标和文本
         AfxAddWindowStyle hWndControl, BS_ICON
      Else
         AfxRemoveWindowStyle hWndControl, BS_ICON
      End If
      SendMessage(hWndControl, BM_SETIMAGE, IMAGE_ICON, Cast(lParam, nIcon))   '默认为 SendMessageW
   Else
      AfxRemoveWindowStyle hWndControl, BS_ICON
      SendMessage(hWndControl, BM_SETIMAGE, IMAGE_ICON, Cast(lParam, 0))   '默认为 SendMessageW
   End If
   
End Sub