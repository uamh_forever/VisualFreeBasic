'控件类
#include Once "YFscroll\ClsYFscroll.inc"
Type YFListData '数据
   iconfont  As Integer '字体图标值，=0不显示图标，1-&HFFFF 表示字体图标 字体名是“iconfont”，由主软件先加载字体 ，其它值是图标句柄。
   sText     As Wstring Ptr ' 文本数据
   DataValue As Integer '用户附加数据
   ColorIcon As Long = -1 'GDI色 图标显示的颜色，-1时显示控件前景色
   ColorText As Long = -1 'GDI色 文字显示的颜色，-1时显示控件前景色
   ColorBack As Long = -1 'GDI色 背景色，-1时默认背景色
   Sel       As Long '是否被选择{=.True.False}
   
End Type
Type Class_YFList Extends Class_Control
   Private : '私有区
   kMouse      As Long    '鼠标在那个按钮上，0表示不在按钮上，1开始在按钮上。
   kPress      As Long    '鼠标是不是按下
   dragY       As Long    '拖动时的鼠标位置
   dragY2      As Long    '拖动时的鼠标位置
   dragYc      As Long    '拖动延续
   mItemHeight As Long    '行高
   mMoveColor  As Long    '鼠标在项目上移动时的底色
   mSelFore    As Long    '选择项目的文字色
   mSelBack    As Long    '选择项目的背景色
   mFocusLine  As Long    '是否显示焦点线
   mCheck      As Long    '是否显示复选框
   mGridLines  As Long    '是否显示网格线
   mLinesColor As Long    '网格线颜色
   EventProc   As Any Ptr '事件指针
   EventIndex  As Long    '控件数组
   Declare Function GetAn(fp As FormControlsPro_TYPE Ptr ,xPos As Long ,yPos As Long) As Long '根据鼠标位置，返回按钮，0表示不在按钮上，1开始在按钮上。
   Declare Sub TabPainting(vhWnd As .hWnd) ' 绘画（控件自用）
   Public : '公共区
   Declare Function AddItem(sText As CWSTR ,iconfont As Integer = 0 ,DataValue As Integer = 0 ,ColorIcon As Long = -1 ,ColorText As Long = -1 ,Sel As Long = 0,ColorBack As Long =-1)                      As Long '新增项目，返回新添加的索引（从0开始）,失败返回 -1
   Declare Function InsertItem(nIndex As Long ,TheText As CWSTR ,iconfont As Integer = 0 ,DataValue As Integer = 0 ,ColorIcon As Long = -1 ,ColorText As Long = -1 ,Sel As Long = 0,ColorBack As Long =-1) As Long '插入项目，返回新添加的索引（从0开始）,失败返回CB_ERR
   Declare Property List(nIndex As Long) As CWSTR '返回/设置项目文本
   Declare Property List(nIndex As Long ,sText As CWSTR)
   Declare Function ListCount() As Long '检索框列表框中的项目数。
   Declare Property ListIndex() As Long '返回/设置 当前项目
   Declare Property ListIndex(nIndex As Long)
   Declare Property Itemicon(nIndex As Long) As Integer  '返回/设置字体图标值，=0不显示图标，1-&HFFFF 表示字体图标（字体名是“iconfont”，由主软件先加载字体）其它值是图标句柄
   Declare Property Itemicon(nIndex As Long ,nValue As Integer)
   Declare Property ItemData(nIndex As Long) As Integer '返回/设置指定项的 附加数据 lParam
   Declare Property ItemData(nIndex As Long ,nValue As Integer)
   Declare Property ItemColorIcon(nIndex As Long) As Long '返回/设置 行的单独图标颜色  GDI色 图标显示的颜色，-1时显示控件前景色
   Declare Property ItemColorIcon(nIndex As Long ,nValue As Long)
   Declare Property ItemColorText(nIndex As Long) As Long '返回/设置 行的单独文字颜色  GDI色 图标显示的颜色，-1时显示控件前景色
   Declare Property ItemColorText(nIndex As Long ,nValue As Long)
   Declare Property ItemColorBack(nIndex As Long) As Long '返回/设置 行的单独背景色 GDI色 图标显示的颜色，-1时默认背景色 
   Declare Property ItemColorBack(nIndex As Long ,nValue As Long)
   Declare Property ItemSel(nIndex As Long) As Long '返回/设置是否被选择（多选模式）  {=.True.False}
   Declare Property ItemSel(nIndex As Long ,nValue As Long)
   Declare Property ItemHeight() As Long '返回/设置每行的高度，100%DPI时的像素单位，自动感知DPI缩放，数值为100%DPI。
   Declare Property ItemHeight(nValue As Long)
   Declare Property TopIndex() As Long '返回/设置 列表框中第一个可见项目(从零开始的索引)。
   Declare Property TopIndex(nIndex As Long)
   Declare Function SelCount()                 As Long '获取多选模式下选定项目的总数。
   Declare Function RemoveItem(nIndex As Long) As Long '删除一个项目，返回剩余项目数
   Declare Sub Clear() '删除组合框中所有项目。
   Declare Property MoveColor() As Long '鼠标在项目上移动时的底色
   Declare Property MoveColor(bValue As Long) '
   Declare Property ForeColor() As Long '返回/设置前景色：设置用：BGR(r, g, b)
   Declare Property ForeColor(nColor As Long)
   Declare Property BackColor() As Long '返回/设置背景色：设置用：BGR(r, g, b)
   Declare Property BackColor(nColor As Long)
   Declare Property SelFore() As Long '返回/设置选择项目的文字色：设置用：BGR(r, g, b)
   Declare Property SelFore(nColor As Long) '
   Declare Property SelBack() As Long '返回/设置选择项目的背景色：设置用：BGR(r, g, b)
   Declare Property SelBack(nColor As Long) '
   Declare Property ScrollFore() As Long '返回/设置滚动条的前景颜色：设置用：BGR(r, g, b)
   Declare Property ScrollFore(nColor As Long) '
   Declare Property ScrollBack() As Long '返回/设置滚动条的背景色：设置用：BGR(r, g, b)
   Declare Property ScrollBack(nColor As Long) '
   Declare Property ScrollMove() As Long '返回/设置鼠标在滚动条上显示的颜色：设置用：BGR(r, g, b)
   Declare Property ScrollMove(nColor As Long) '
   Declare Property FocusLine() As Boolean '返回/设置 当控件得到焦点时，显示焦点线框。 {=.True.False}
   Declare Property FocusLine(bValue As Boolean) '
   Declare Property Check() As Long '返回/设置 是否显示复选框。 {=.True.False}
   Declare Property Check(bValue As Long) '
   Declare Property GridLines() As Long ' 是否显示网格线。 {=.True.False}
   Declare Property GridLines(bValue As Long) '
   Declare Property LinesColor() As Long '返回/设置网格线的颜色色：设置用：BGR(r, g, b)
   Declare Property LinesColor(bValue As Long) '
   Declare Function GetTopY() As Long '获取顶部第一行显示的位置，像素单位
   
   
   '{代码不提示}
   Declare Function MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult '消息处理（控件自用）
   Vscroll As Class_YFscroll '垂直滚动条控件
   'Hscroll As Class_YFscroll '水平滚动条控件
   Declare Sub SetEventProc(Index As Long ,pProc As Any Ptr) '设置直接调用事件，不走消息，速度和效率快
   Declare Sub UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
End Type

Sub Class_YFList.UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0)) '当前
      If nList Then 
         If fp->CtlData(2) Then '清理文本数据内存
            For i As Long = 0 To fp->CtlData(2) -1
              If  nList[i].sText Then Deallocate nList[i].sText
            Next 
         End If 
         Delete[] nList
      End If 
      fp->CtlData(0) = 0  '列表数据组指针
      fp->CtlData(1) = 0  '列表数据组有多少个数据
      fp->CtlData(2) = 0  '列表数据组实体多少个
      fp->CtlData(3) = -1 '当前选择的列表
   End If   
End Sub

Function Class_YFList.MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult
   Dim fp As FormControlsPro_TYPE Ptr
   Select Case wMsg
      Case WM_PAINT
         This.TabPainting(vhWnd)
         Return True
      Case WM_USER + 200 '预防多线程操作控件，需要发消息处理
         '新增数据
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return -1
         Dim n As YFListData Ptr = Cast(Any Ptr ,nlParam)
         If n = 0 Then Return -1
         Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0))
         Dim u     As Long           = fp->CtlData(1) '多少数据
         Dim mu    As Long           = fp->CtlData(2) '实体大小
         Dim i     As Long ,k As Long = nwParam
         '需要的数据 超过实体大小
         If u + 1 > mu Then
            mu             += 100
            fp->CtlData(2) = mu
            Dim snList As YFListData Ptr = New YFListData[mu]
            If u > 0 Then
               For i = 0 To u -1
                  snList[i] = nList[i]
               Next
            End If
            If nList Then Delete[] nList
            nList          = snList
            fp->CtlData(0) = Cast(Integer ,nList)
         End If
         '是否是插入
         If k > -1 And k < u Then
            For i = u -1 To k Step -1
               Swap nList[i] ,nList[i + 1]
            Next
         Else
            k = u
         End If
         fp->CtlData(1) += 1
         nList[k]       = *n
         'fp->CtlData(3) = k
         KillTimer(vhWnd ,101)
         SetTimer(vhWnd ,101 ,200 ,NULL) '延迟来设置滚动条，避免大量添加数据时比较卡
         Return k
      Case WM_USER + 201 '预防多线程操作控件，需要发消息处理
         '获取数据
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim n As YFListData Ptr = Cast(Any Ptr ,nlParam)
         If n = 0 Then Return 0
         Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0))
         Dim u     As Long           = fp->CtlData(1) '多少数据
         Dim i     As Long           = nwParam
         If i >= 0 And i < u Then
             *n = nList[i]
         End If
         Return -1
      Case WM_USER + 202 '预防多线程操作控件，需要发消息处理
         '修改数据
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim n As YFListData Ptr = Cast(Any Ptr ,nlParam)
         If n = 0 Then Return 0
         Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0))
         Dim u     As Long           = fp->CtlData(1) '多少数据
         Dim i     As Long           = nwParam
         If i >= 0 And i < u Then
            '2=文本 4=图标 8=附加数据 16=图标颜色 32=文字颜色 64=选择 128=单独背景色
            If (n->Sel And 2) = 2 Then
               If nList[i].sText Then Deallocate nList[i].sText
               nList[i].sText = n->sText
            End If
            If (n->Sel And 4) = 4    Then nList[i].iconfont  = n->iconfont
            If (n->Sel And 5) = 8    Then nList[i].DataValue = n->DataValue
            If (n->Sel And 16) = 16  Then nList[i].ColorIcon = n->ColorIcon
            If (n->Sel And 32) = 32  Then nList[i].ColorText = n->ColorText
            If (n->Sel And 64) = 64  Then nList[i].Sel       = (n->Sel And 1) = 1
            If (n->Sel And 32) = 128 Then nList[i].ColorBack = n->ColorBack
         End If
         KillTimer(vhWnd ,103)
         SetTimer(vhWnd ,103 ,200 ,NULL) '延迟刷新
         Return -1
      Case WM_USER + 203 '预防多线程操作控件，需要发消息处理
         '获取选择数量
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim n As YFListData Ptr = Cast(Any Ptr ,nlParam)
         If n = 0 Then Return 0
         Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0))
         Dim u     As Long           = fp->CtlData(1) '多少数据
         Dim i     as Long ,jj as Long
         If u > 0 Then
            For i = 0 To u -1
               If nList[i].Sel Then jj += 1
            Next
         End If
         Return jj
      Case WM_USER + 204 '删除记录
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0))
         Dim u     As Long           = fp->CtlData(1) '多少数据
         Dim i     As Long ,jj As Long = nlParam
         If jj < 0 Then '全部清除， 实体数据，只增加不删除，直到销毁控件时才回收内存。
            fp->CtlData(1)        = 0
            fp->CtlData(3)        = -1
            This.Vscroll.hWndForm = vhWnd
            This.Vscroll.Value    = 0
            This.Vscroll.Visible  = 0
            This.Vscroll.nMax     = 0
            FF_Redraw vhWnd
            Return 0
         End If
         If jj >= u Then Return u
         If jj < u -1 Then
            For i = jj To u -2
               Swap nList[i] ,nList[i + 1]
            Next
         End If
         u              -= 1
         fp->CtlData(1) = u
         Dim rc As Rect
         GetClientRect(vhWnd ,@rc)
         Dim As Long ww = (rc.Right - rc.Left) ,hh = (rc.Bottom - rc.Top)
         Dim As Long ah = AfxScaleY(mItemHeight)
         This.Vscroll.hWndForm = vhWnd
         This.Vscroll.nMax = u *ah - hh + AfxScaleY(10)
         fp->CtlData(3)    = -1
         If u * ah <= hh Then '显示不够，启用滚动条
            This.Vscroll.Value   = 0
            This.Vscroll.Visible = 0
         Else
            
         End If
         This.Vscroll.Refresh
         KillTimer(vhWnd ,103)
         SetTimer(vhWnd ,103 ,200 ,NULL) '延迟刷新
         Return u
      Case WM_USER + 205 '预防多线程操作控件，需要发消息处理
         '多少数据
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Return fp->CtlData(1) '多少数据
      Case WM_USER + 206 '预防多线程操作控件，需要发消息处理
         '获取当前选择
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim u As Long = fp->CtlData(1) '多少数据
         If u < 1 Or fp->CtlData(3) >= u Then Return -1
         Return fp->CtlData(3) '
      Case WM_USER + 207 '预防多线程操作控件，需要发消息处理
         '设置当前选择
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         If nwParam < 0 Or nwParam >= fp->CtlData(1) Then nwParam = -1
         fp->CtlData(3) = nwParam
         Dim rc As Rect
         GetClientRect(vhWnd ,@rc)
         Dim As Long ww = (rc.Right - rc.Left) ,hh = (rc.Bottom - rc.Top)         
         Dim As Long ah = AfxScaleY(mItemHeight)
         Dim u As Long          = fp->CtlData(1) '多少数据
         KillTimer(vhWnd ,101) '预防新增项目后又设置选择，会跳动的情况
         If u * ah > hh Then '显示不够，启用滚动条
            This.Vscroll.hWndForm = vhWnd
            If This.Vscroll.Visible = 0 Then
               This.Vscroll.Visible     = True
               This.Vscroll.LargeChange = hh
               This.Vscroll.nPage       = hh
            End If
            This.Vscroll.nMax = u *ah - hh + AfxScaleY(10)
            Dim v  As Long = This.Vscroll.Value
            Dim aa As Long = nwParam
            If aa *ah - v < 1 Then
               This.Vscroll.Value = aa *ah - hh * 0.2
            Else
               If (aa + 1) *ah > v + hh Then
                  This.Vscroll.Value = (aa + 1) *ah - hh * 0.8
               End If
            End If
            This.Vscroll.Refresh
         End If
         
         FF_Redraw vhWnd
      Case WM_TIMER
         Select Case nwParam
            Case 101 '延迟处理
               KillTimer(vhWnd ,101)
               fp = vfb_Get_Control_Ptr(vhWnd)
               If fp = 0 Then Return -1
               Dim u  As Long = fp->CtlData(1) '多少数据
               Dim rc As Rect
               GetClientRect(vhWnd ,@rc)
               Dim As Long ww = (rc.Right - rc.Left) ,hh = (rc.Bottom - rc.Top)
               Dim As Long ah = AfxScaleY(mItemHeight)
               If u * ah > hh Then '显示不够，启用滚动条
                  This.Vscroll.hWndForm = vhWnd
                  If This.Vscroll.Visible = 0 Then
                     This.Vscroll.Visible     = True
                     This.Vscroll.LargeChange = hh
                     This.Vscroll.nPage       = hh
                  End If
                  This.Vscroll.nMax = u *ah - hh + AfxScaleY(10)
                  This.Vscroll.Move ww - AfxScaleX(10) ,0 ,AfxScaleX(10) ,hh
                  'This.Vscroll.Value = This.Vscroll.nMax
                  This.Vscroll.Refresh
               Else
                  This.Vscroll.hWndForm = vhWnd
                  If This.Vscroll.Visible Then
                     This.Vscroll.Visible = 0
                     This.Vscroll.Value   = 0
                     This.Vscroll.nMax    = 0
                  End If
               End If
               FF_Redraw vhWnd
            Case 102 '实现快速拖动后，鼠标放开还能滑行一段距离
               If dragYc = 0 Then
                  KillTimer(vhWnd ,102)
               Else
                  This.Vscroll.hWndForm = vhWnd
                  Dim v As Long = This.Vscroll.Value
                  If dragYc > 0 Then
                     v      += dragYc * AfxScaleX(8)
                     dragYc -= 1
                  Else
                     v      += dragYc * AfxScaleX(8)
                     dragYc += 1
                     
                  End If
                  This.Vscroll.Value = v
                  FF_Redraw vhWnd
               End If
            Case 103 '延迟刷新，大量添加数据时，加快速度
               KillTimer(vhWnd ,103)
               FF_Redraw vhWnd
         End Select
      Case WM_SIZE
         Dim ww As Long = LoWord(nlParam) ,hh As Long = HiWord(nlParam)
         This.Vscroll.hWndForm = vhWnd
         This.Vscroll.Move ww - AfxScaleX(10) ,0 ,AfxScaleX(10) ,hh
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0))
         Dim u     As Long           = fp->CtlData(1) '多少数据
         Dim ah    As Long           = AfxScaleY(mItemHeight)
         This.Vscroll.nMax        = u *ah - hh + AfxScaleY(10)
         This.Vscroll.Visible     = u *ah > hh
         This.Vscroll.LargeChange = hh
         This.Vscroll.nPage       = hh
         This.Vscroll.Refresh
         
      Case WM_USER + 301 '滚动条值改变
         FF_Redraw vhWnd
      Case WM_MOUSEWHEEL
         Dim WheelDelta As Long = GET_WHEEL_DELTA_WPARAM(nwParam) ,MouseFlags As Long = GET_KEYSTATE_WPARAM(nwParam) ,xPos As Long = GET_X_LPARAM(nlParam) ,yPos As Long = GET_Y_LPARAM(nlParam)
         'WheelDelta 为旋转的距离，以倍数或分度表示，通常为120（具体看WIN系统设置），正值 120 表示向前旋转，负值 -120 表示向后旋转
         This.Vscroll.hWndForm = vhWnd
         If This.Vscroll.Visible = True Then
            Dim v As Long = This.Vscroll.Value
            v                  -= AfxScaleX(WheelDelta / 120 * 15)
            This.Vscroll.Value = v
            FF_Redraw vhWnd
         End If
      Case WM_SETFOCUS ,WM_KILLFOCUS
         If mFocusLine Then
            FF_Redraw vhWnd
            This.Vscroll.hWndForm = vhWnd
            This.Vscroll.Refresh
         End If
      Case WM_MOUSEMOVE
         '移出控件侦察
         Dim entTrack As tagTRACKMOUSEEVENT
         entTrack.cbSize      = SizeOf(tagTRACKMOUSEEVENT)
         entTrack.dwFlags     = TME_LEAVE
         entTrack.hwndTrack   = vhWnd
         entTrack.dwHoverTime = HOVER_DEFAULT
         TrackMouseEvent @entTrack
         '处理移动
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         This.Vscroll.hWndForm = vhWnd
         Dim aa As Long = GetAn(fp ,xPos ,yPos)
         Dim bb As Long = MouseFlags = 1
         If bb Then
            If dragY2 Then
               This.Vscroll.hWndForm = vhWnd
               If This.Vscroll.Visible = True Then
                  Dim v As Long = This.Vscroll.Value
                  dragYc             = dragY - yPos
                  v                  += dragYc
                  This.Vscroll.Value = v
                  dragY              = yPos
                  If Abs(dragY2 - dragY) > AfxScaleY(10) Or dragY2 = -9999 Then
                     '当拖动时，不应该显示鼠标在项目上移动的色彩
                     '但经常有人点击项目会打滑造成假拖动，因此设定 AfxScaleY(10) 的范围为不拖动
                     aa     = 0
                     dragY2 = -9999
                  End If
                  FF_Redraw vhWnd
               End If
            End If
         End If
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            FF_Redraw vhWnd
         End If
         
      Case WM_MOUSELEAVE '处理移出
         kMouse = 0
         kPress = 0
         dragY2 = 0
         FF_Redraw vhWnd
      Case WM_LBUTTONDOWN
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         This.Vscroll.hWndForm = vhWnd
         Dim aa As Long = GetAn(fp ,xPos ,yPos)
         Dim bb As Long = True
         ReleaseCapture '取消鼠标绑定控件
         SetCapture vhWnd '绑定鼠标控件，让鼠标按下拖动到控件外面也能有鼠标移动消息
         dragY  = yPos
         dragY2 = yPos
         dragYc = 0
         KillTimer(vhWnd ,102)
         If mFocusLine Then.SetFocus(vhWnd)
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            FF_Redraw vhWnd
         End If
      Case WM_LBUTTONUP
         
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         ReleaseCapture '取消鼠标绑定控件
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         If dragYc Then
            If Abs(dragYc) > AfxScaleY(1) Then '实现快速拖动后，鼠标放开还能滑行一段距离
               SetTimer(vhWnd ,102 ,20 ,NULL)
            End If
         End If
         dragY2                = 0
         dragY                 = 0
         This.Vscroll.hWndForm = vhWnd
         Dim aa As Long = GetAn(fp ,xPos ,yPos)
         Dim bb As Long
         If aa = kMouse Then
            If mCheck <> 0 And aa > 0 And xPos < AfxScaleX(16) Then
               Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0))
               Dim u     As Long           = fp->CtlData(1) '多少数据
               If aa <= u And nList <> 0 Then
                  nList[aa -1].Sel = nList[aa -1].Sel = 0
                  FF_Redraw vhWnd
                  SendMessage(vhWnd ,WM_USER + 103 ,aa -1 ,nList[aa -1].Sel)
               End If
            Else
               If SendMessage(vhWnd ,WM_USER + 102 ,fp->CtlData(3) ,aa -1) = 0 Then
                  fp->CtlData(3) = aa -1
                  If aa Then
                     This.Vscroll.hWndForm = vhWnd
                     Dim v As Long = This.Vscroll.Value ,ah As Long = AfxScaleY(mItemHeight)
                     If (aa -1) *ah - v < 1 Then
                        This.Vscroll.Value = (aa -1) *ah -1
                     Else
                        Dim rc As Rect
                        GetClientRect(vhWnd ,@rc)
                        Dim As Long ww = (rc.Right - rc.Left) ,hh = (rc.Bottom - rc.Top)
                        If (aa) *ah > v + hh Then
                           This.Vscroll.Value = (aa) *ah - hh
                        End If
                     End If
                  end If
                  SendMessage(vhWnd ,WM_USER + 101 ,0 ,aa -1)
               End If
            end If
            
         End If
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            
            FF_Redraw vhWnd
         End If
         
         'Case WM_NOTIFY
         '   '为了工具提示时，鼠标指针挡住了提示文字，因此需要我们自己调整显示位置。
         '   fp = vfb_Get_Control_Ptr(vhWnd)
         '   If fp = 0 Then Return 0
         '   If fp->ToolTipBalloon = 0 Then
         '      Dim nn As NMHDR Ptr = Cast(Any Ptr ,nlParam)
         '      Select Case nn->code
         '         Case TTN_SHOW '工具提示时，设置显示位置
         '            Dim A As Point
         '            GetCursorPos @a
         '            SetWindowPos(nn->hwndFrom ,NULL ,A.x ,A.y + GetSystemMetrics(SM_CYCURSOR) ,0 ,0 ,SWP_NOSIZE Or SWP_NOZORDER Or SWP_NOACTIVATE)
         '            Return True
         '      End Select
         '   End If
   End Select
   Function = 0
End Function
Sub Class_YFList.TabPainting(vhWnd As .hWnd) ' 绘画
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then
      Dim gg As yGDI = yGDI(vhWnd ,&HFFFFFF ,True) 'WM_PAINT事件里必须用这行初始化 yGDI
      Return
   end if
   Dim nList As YFListData Ptr = Cast(Any Ptr ,fp->CtlData(0)) '  '列表数据组指针
   If nList = 0 Then
      Dim gg As yGDI = yGDI(vhWnd ,GetCodeColorGDI(fp->BackColor) ,True) 'WM_PAINT事件里必须用这行初始化 yGDI
      Return
   end if
   Dim u As Long = fp->CtlData(1) '列表数据组有多少个数据
   If u = 0 Then
      Dim gg As yGDI = yGDI(vhWnd ,GetCodeColorGDI(fp->BackColor) ,True) 'WM_PAINT事件里必须用这行初始化 yGDI
      Return
   end if
   Dim sel As Long = fp->CtlData(3) '当前选择的列表
   Dim i As Long ,x As Long ,y As Long
   This.Vscroll.hWndForm = vhWnd
   y = This.Vscroll.Value
   'EventProc  As Any Ptr '事件指针
   'EventIndex As Long    '控件数组
   'PrintA Hex(EventProc)  ,EventIndex
   If EventProc Then
      If EventIndex = -1 Then
         Dim ee As Function(hF As .hWnd ,hC As .hWnd ,SY As Long ,IH As Long ,sU As Long ,nL As YFListData Ptr ,sI As Long ,kM As Long ,kP As Long) As LResult
         ee = EventProc
         If ee(GetParent(vhWnd) ,vhWnd , - y ,mItemHeight ,u ,nList ,sel ,kMouse -1 ,kPress) Then Return
      Else
         Dim ee As Function(ii As Long ,hF As .hWnd ,hC As .hWnd ,SY As Long ,IH As Long ,sU As Long ,nL As YFListData Ptr ,sI As Long ,kM As Long ,kP As Long) As LResult
         ee = EventProc
         If ee(fp->Index ,GetParent(vhWnd) ,vhWnd , - y ,mItemHeight ,u ,nList ,sel ,kMouse -1 ,kPress) Then Return
      End If
   End If
   
   Dim BColor As Long   = GetCodeColorGDI(fp->BackColor) '背景色
   Dim fColor As Long   = GetCodeColorGDI(fp->ForeColor) '前景色
   Dim gg     As yGDI   = yGDI(vhWnd ,BColor ,True)      'WM_PAINT事件里必须用这行初始化 yGDI
   Dim dpi    As Single = gg.Dpi
   Dim ww     As Long   = gg.m_Width ,hh As Long = gg.m_Height
   
   Dim ah As Long = AfxScaleY(mItemHeight) '行高
   Dim ai As Long '列表框中第一个可见项目
   
   If u * ah > hh Then ww -= 10 *dpi '有滚动条时
   x = 5 *dpi
   
   gg.SetColor fColor
   ai = Int(y / ah)
   y  = - y + ai *ah
   gg.Pen 0 ,0
   gg.Brush mMoveColor
   gg.dpi = 1
   Dim aw As Long = ww -8 *dpi '文字显示的宽度
   If mCheck Then
      x  += 17 *dpi
      aw -= 17 *dpi
      gg.gpPen 1.5 *dpi ,ColorGdiToGDIplue(fColor ,255)
      gg.GpBrush 0
   End If
   
   For i = ai To u -1
      If sel = i Then
         gg.Brush mSelBack
         gg.SetColor mSelFore
         If mCheck Then gg.gpPen 1.5 *dpi ,ColorGdiToGDIplue(mSelFore ,255)
      End If
      If i + 1 = kMouse Or sel = i Then
         gg.DrawFrame 0 ,y ,ww ,ah
      End If
      If mCheck Then
         gg.Brush
         gg.gpDrawFrame 4 *dpi ,ah / 2 -6 *dpi + y ,10 *dpi ,10 *dpi
         If nList[i].sel Then
            gg.GpDrawLine 4 *dpi ,ah / 2 -4 *dpi       + y ,(3 + 6) *dpi  ,ah / 2 + 2 *dpi + y
            gg.GpDrawLine(3 + 6) *dpi ,ah / 2 + 2 *dpi + y ,(5 + 12) *dpi ,ah / 2 -8 *dpi + y
         End If
         gg.Brush mMoveColor
      End If
      
      If nList[i].iconfont Then
         If nList[i].iconfont > 0 And nList[i].iconfont <= &HFFFF Then
            gg.Font "iconfont" ,12 *dpi '字体图标
            If nList[i].ColorIcon <> -1 Then gg.SetColor nList[i].ColorIcon
            gg.DrawTextS(x -2 *dpi ,y ,20 *dpi ,ah ,WChr(nList[i].iconfont) ,DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX Or DT_WORD_ELLIPSIS)
         Else '图标句柄
            DrawIconEx gg.m_Dc ,x -2 *dpi ,ah / 2 -9 *dpi + y ,Cast(HICON ,nList[i].iconfont) ,AfxScaleX(16) ,AfxScaleY(16) ,NULL ,NULL ,DI_NORMAL
            
         end if
         gg.Font ,9 *dpi
         If nList[i].ColorText <> -1 Then
            gg.SetColor nList[i].ColorText
         ElseIf nList[i].ColorIcon <> -1 Then
            If sel = i Then
               gg.SetColor mSelFore
            Else
               gg.SetColor fColor
            end If
         End If
         gg.DrawTextS(x + 18 *dpi ,y ,aw -20 *dpi ,ah ,*nList[i].sText ,DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX Or DT_WORD_ELLIPSIS)
         If nList[i].ColorText <> -1 Then gg.SetColor fColor
      Else
         If i + 1 = kMouse Or sel = i Then
            
         Else
            If nList[i].ColorBack <> -1 Then
               gg.Brush nList[i].ColorBack
               gg.DrawFrame 0 ,y ,ww ,ah
            End If
            If nList[i].ColorText <> -1 Then gg.SetColor nList[i].ColorText
         End If
         gg.DrawTextS(x ,y ,aw ,ah ,*nList[i].sText ,DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX Or DT_WORD_ELLIPSIS)
         If nList[i].ColorText <> -1 Then gg.SetColor fColor
         If nList[i].ColorBack <> -1 Then gg.Brush mMoveColor
      End If
      
      y += ah
      If sel = i Then
         gg.Brush mMoveColor
         gg.SetColor fColor
         If mCheck Then gg.gpPen 1.5 *dpi ,ColorGdiToGDIplue(fColor ,255)
      End If
      If mGridLines Then
         gg.Pen 1      ,mLinesColor
         gg.DrawLine 0 ,y -1 ,ww ,y -1
         gg.Pen 0      ,0
      End If
      If y > hh Then Exit For
   Next
   
   If mFocusLine Then
      If GetFocus() = vhWnd Then
         gg.Pen 1 ,mSelBack
         gg.Brush
         gg.DrawFrame 0 ,0 ,ww ,hh
      End If
   End If
   
End Sub
   
Function Class_YFList.GetAn(fp As FormControlsPro_TYPE Ptr ,xPos As Long ,yPos As Long) As Long
   '获取鼠标在什么按钮
   Dim u As Long = fp->CtlData(1) '列表数据组有多少个数据
   If u = 0 Then Return 0
   Dim y  As Long = This.Vscroll.Value
   Dim ah As Long = AfxScaleY ( mItemHeight) '行高
   Dim i  As Long = Int((yPos + y) / ah) + 1
   If i > u Then i = 0
   Function = i
End Function
Function Class_YFList.AddItem(sText As CWSTR ,iconfont As Integer = 0 ,DataValue As Integer = 0 ,ColorIcon As Long = -1 ,ColorText As Long = -1 ,Sel As Long = 0 ,ColorBack As Long = -1) As Long
   Dim aa As YFListData
   Dim sLen As Long = Len( **sText) * 2+1
   aa.sText = CAllocate(sLen)
   *aa.sText     = sText
   aa.iconfont  = iconfont
   aa.DataValue = DataValue
   aa.ColorIcon = ColorIcon
   aa.ColorText = ColorText
   aa.Sel       = Sel
   aa.ColorBack = ColorBack
   Function     = SendMessage(hWndControl ,WM_USER + 200 , -1 ,Cast(lParam ,@aa)) '预防多线程操作控件发生崩溃，发消息在集中在主线程中操作。
End Function
Function Class_YFList.InsertItem(nIndex As Long ,TheText As CWSTR ,iconfont As Integer = 0 ,DataValue As Integer = 0 ,ColorIcon As Long = -1 ,ColorText As Long = -1 ,Sel As Long = 0 ,ColorBack As Long = -1) As Long
   Dim aa As YFListData
   Dim sLen As Long = Len( **TheText) * 2+1
   aa.sText = CAllocate(sLen)
   *aa.sText     = TheText   
   aa.iconfont  = iconfont
   aa.DataValue = DataValue
   aa.ColorIcon = ColorIcon
   aa.ColorText = ColorText
   aa.Sel       = Sel
   aa.ColorBack = ColorBack
   Function     = SendMessage(hWndControl ,WM_USER + 200 ,nIndex ,Cast(lParam ,@aa)) '预防多线程操作控件发生崩溃，发消息在集中在主线程中操作。
End Function
Property Class_YFList.List(nIndex As Long) As CWSTR '返回/设置项目文本
   '检查窗口句柄是否有效
   If IsWindow(hWndControl) Then
      Dim aa As YFListData
      If SendMessage(hWndControl ,WM_USER + 201 ,nIndex ,Cast(lParam ,@aa)) Then
         Return *aa.sText
      Else
         Return ""
      End If
   Else
      Return ""
   End If
End Property
Property Class_YFList.List(nIndex As Long ,sText As CWSTR)
   If IsWindow(hWndControl) Then
      Dim aa As YFListData
      '2=文本 4=图标 8=附加数据 16=图标颜色 32=文字颜色 64=选择
      aa.Sel = 2
      Dim sLen As Long = Len( **sText) * 2 + 1
      aa.sText = CAllocate(sLen)
       *aa.sText = sText
      SendMessage(hWndControl ,WM_USER + 202 ,nIndex ,Cast(lParam ,@aa))
   End If
End Property
Property Class_YFList.Itemicon(nIndex As Long) As Integer '返回/设置字体图标值
   If IsWindow(hWndControl) = 0 Then Return 0  '检查窗口句柄是否有效
   Dim aa As YFListData
   If SendMessage(hWndControl ,WM_USER + 201 ,nIndex ,Cast(lParam ,@aa)) =0 Then Return 0
   Return aa.iconfont
End Property
Property Class_YFList.Itemicon(nIndex As Long ,nValue As Integer)
   If IsWindow(hWndControl)=0 Then Return
   Dim aa As YFListData
   aa.Sel      = 4 '2=文本 4=图标 8=附加数据 16=图标颜色 32=文字颜色 64=选择
   aa.iconfont = nValue
   SendMessage(hWndControl ,WM_USER + 202 ,nIndex ,Cast(lParam ,@aa))
End Property
Property Class_YFList.ItemData(nIndex As Long) As Integer '返回/设置指定项的
   If IsWindow(hWndControl) = 0 Then Return 0  '检查窗口句柄是否有效
   Dim aa As YFListData
   If SendMessage(hWndControl ,WM_USER + 201 ,nIndex ,Cast(lParam ,@aa)) =0 Then Return 0
   Return aa.DataValue
End Property
Property Class_YFList.ItemData(nIndex As Long ,nValue As Integer)
   If IsWindow(hWndControl)=0 Then Return
   Dim aa As YFListData
   aa.Sel      = 8 '2=文本 4=图标 8=附加数据 16=图标颜色 32=文字颜色 64=选择
   aa.DataValue = nValue
   SendMessage(hWndControl ,WM_USER + 202 ,nIndex ,Cast(lParam ,@aa))
End Property
Property Class_YFList.ItemColorIcon(nIndex As Long) As Long '返回/设置图标颜色
   If IsWindow(hWndControl) = 0 Then Return 0  '检查窗口句柄是否有效
   Dim aa As YFListData
   If SendMessage(hWndControl ,WM_USER + 201 ,nIndex ,Cast(lParam ,@aa)) =0 Then Return 0
   Return aa.ColorIcon
End Property
Property Class_YFList.ItemColorIcon(nIndex As Long ,nValue As Long)
   If IsWindow(hWndControl)=0 Then Return
   Dim aa As YFListData
   aa.Sel      = 16 '2=文本 4=图标 8=附加数据 16=图标颜色 32=文字颜色 64=选择
   aa.ColorIcon = nValue
   SendMessage(hWndControl ,WM_USER + 202 ,nIndex ,Cast(lParam ,@aa))
End Property
Property Class_YFList.ItemColorText(nIndex As Long) As Long '返回/设置文字颜色
   If IsWindow(hWndControl) = 0 Then Return 0  '检查窗口句柄是否有效
   Dim aa As YFListData
   If SendMessage(hWndControl ,WM_USER + 201 ,nIndex ,Cast(lParam ,@aa)) =0 Then Return 0
   Return aa.ColorText
End Property
Property Class_YFList.ItemColorText(nIndex As Long ,nValue As Long)
   If IsWindow(hWndControl)=0 Then Return
   Dim aa As YFListData
   aa.Sel      = 32 '2=文本 4=图标 8=附加数据 16=图标颜色 32=文字颜色 64=选择
   aa.ColorText = nValue
   SendMessage(hWndControl ,WM_USER + 202 ,nIndex ,Cast(lParam ,@aa))
End Property
Property Class_YFList.ItemColorBack(nIndex As Long) As Long '返回/设置 行的单独背景色 GDI色 图标显示的颜色，-1时默认背景色 
   If IsWindow(hWndControl) = 0 Then Return 0  '检查窗口句柄是否有效
   Dim aa As YFListData
   If SendMessage(hWndControl ,WM_USER + 201 ,nIndex ,Cast(lParam ,@aa)) =0 Then Return 0
   Return aa.ColorBack
End Property
Property Class_YFList.ItemColorBack(nIndex As Long ,nValue As Long)
   If IsWindow(hWndControl) = 0 Then Return
   Dim aa As YFListData
   aa.Sel       = 128 '2=文本 4=图标 8=附加数据 16=图标颜色 32=文字颜色 64=选择 128=单独背景色
   aa.ColorBack = nValue
   SendMessage(hWndControl ,WM_USER + 202 ,nIndex ,Cast(lParam ,@aa))
End Property


Property Class_YFList.ItemSel(nIndex As Long) As Long '返回/设置是否被选择（多选模式）
   If IsWindow(hWndControl) = 0 Then Return 0  '检查窗口句柄是否有效
   Dim aa As YFListData
   If SendMessage(hWndControl ,WM_USER + 201 ,nIndex ,Cast(lParam ,@aa)) =0 Then Return 0
   Return aa.Sel
End Property
Property Class_YFList.ItemSel(nIndex As Long ,nValue As Long)
   If IsWindow(hWndControl)=0 Then Return
   Dim aa As YFListData
   aa.Sel = 64 '2=文本 4=图标 8=附加数据 16=图标颜色 32=文字颜色 64=选择
   If nValue Then aa.Sel =65
   SendMessage(hWndControl ,WM_USER + 202 ,nIndex ,Cast(lParam ,@aa))
End Property
Property Class_YFList.ItemHeight() As Long '返回属性
   If IsWindow(hWndControl)=0 Then Return 0
   Return mItemHeight
End Property
Property Class_YFList.ItemHeight(nValue As Long) '给属性赋值
   If IsWindow(hWndControl) = 0 Then Return
   mItemHeight = nValue
   Dim rc As Rect
   GetClientRect(hWndControl ,@rc)
   Dim As Long ww = (rc.Right - rc.Left) ,hh = (rc.Bottom - rc.Top)
   This.Vscroll.hWndForm = hWndControl
   This.Vscroll.nMin     = 0
   'This.Vscroll.Frame =True
   This.Vscroll.SmallChange = AfxScaleY(3)
   This.Vscroll.LargeChange = hh
   This.Vscroll.nPage       = hh
   This.Vscroll.Move ww - AfxScaleX(10) ,0 ,AfxScaleX(10) ,hh
   This.Vscroll.Refresh
   
End Property
Property Class_YFList.TopIndex() As Long  '返回/设置 列表框中第一个可见项目(从零开始的索引)。
   If IsWindow(hWndControl)=0 Then Return 0
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return 0
   Dim ah As Long = AfxScaleY ( mItemHeight ) '行高
   Dim ai As Long 
   This.Vscroll.hWndForm = hWndControl
   ai = Int(This.Vscroll.Value / ah)   
   Return ai
End Property
Property Class_YFList.TopIndex(nIndex As Long) 
   If IsWindow(hWndControl)=0 Then Return
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return
   Dim ah As Long = AfxScaleY(mItemHeight) '行高
   This.Vscroll.hWndForm = hWndControl
   This.Vscroll.Value = nIndex * ah 
   FF_Redraw hWndControl
End Property
Function Class_YFList.SelCount()As Long '
   If IsWindow(hWndControl)=0 Then Return 0
   Function=SendMessage(hWndControl ,WM_USER + 203 ,0 ,0)
End Function
Function Class_YFList.RemoveItem(nIndex As Long) As Long '删除一个项目，返回剩余项目数
   If IsWindow(hWndControl)=0 Then Return 0
   Function=SendMessage(hWndControl ,WM_USER + 204 ,0 ,nIndex)
End Function
Sub Class_YFList.Clear()                                  '删除组合框中所有项目。
   If IsWindow(hWndControl)=0 Then Return 
   SendMessage(hWndControl ,WM_USER + 204 ,0 ,-1)
End Sub
Property Class_YFList.MoveColor() As Long '返回属性 
   Return mMoveColor
End Property
Property Class_YFList.MoveColor(bValue As Long) '给属性赋值
   mMoveColor = bValue
   FF_Redraw hWndControl '刷新控件
   
End Property
Property Class_YFList.BackColor() As Long                  '返回/设置背景色： RGB 颜色值(用 BGR(r, g, b) 获取)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim cc As Long = GetCodeColorGDI(fp->BackColor)
      If cc = -1 Then cc = GetSysColor(COLOR_BTNFACE)
      Return cc
   End If
End Property
Property Class_YFList.BackColor(nColor As Long)
   If IsWindow(hWndControl) Then
      Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
      If fp Then
         fp->BackColor = nColor
         FF_Redraw hWndControl '刷新控件
      End If
   End If
End Property
Property Class_YFList.ForeColor() As Long                  '返回/设置前景色： RGB 颜色值(用 BGR(r, g, b) 获取)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->ForeColor)
      if cc = -1 then cc = GetSysColor(COLOR_BTNTEXT)      
      Return cc
   end if
End Property
Property Class_YFList.ForeColor(nColor As Long)
   If IsWindow(hWndControl) Then
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
      if fp then
         fp->ForeColor = nColor
         FF_Redraw hWndControl '刷新控件
      End If
   End If
End Property
Property Class_YFList.SelFore() As Long '返回属性 
    Return mSelFore
End Property
Property Class_YFList.SelFore(nColor As Long) '给属性赋值
   mSelFore = nColor
   FF_Redraw hWndControl
End Property
Property Class_YFList.SelBack() As Long '返回属性 
    Return mSelBack
End Property
Property Class_YFList.SelBack(nColor As Long) '给属性赋值
   mSelBack = nColor
   FF_Redraw hWndControl
End Property
Property Class_YFList.ScrollFore() As Long '返回属性 
    Return This.Vscroll.ForeColor
End Property
Property Class_YFList.ScrollFore(nColor As Long)'给属性赋值 
      This.Vscroll.ForeColor = nColor
      'This.Hscroll.ForeColor = nColor
End Property
Property Class_YFList.ScrollBack() As Long '返回属性 
    Return This.Vscroll.BackColor
End Property
Property Class_YFList.ScrollBack(nColor As Long)'给属性赋值 
      This.Vscroll.BackColor = nColor
      'This.Hscroll.BackColor = nColor
End Property
Property Class_YFList.ScrollMove() As Long '返回属性 
    Return This.Vscroll.MoveColor
End Property
Property Class_YFList.ScrollMove(nColor As Long)'给属性赋值 
      This.Vscroll.MoveColor = nColor
      'This.Hscroll.MoveColor = nColor
End Property
Property Class_YFList.FocusLine() As Boolean '返回属性 
   Return mFocusLine
End Property
Property Class_YFList.FocusLine(bValue As Boolean) '给属性赋值
   mFocusLine = bValue
   FF_Redraw hWndControl
End Property
Property Class_YFList.Check() As Long '返回/设置 是否显示复选框 
   Return mCheck
End Property
Property Class_YFList.Check(bValue As Long) '
   mCheck = bValue
   FF_Redraw hWndControl
End Property
Property Class_YFList.GridLines() As Long '返回属性 
     Return mGridLines
End Property
Property Class_YFList.GridLines(bValue As Long) '给属性赋值
   mGridLines = bValue
   FF_Redraw hWndControl
End Property
Property Class_YFList.LinesColor() As Long '返回属性 
   Return  mLinesColor
End Property
Property Class_YFList.LinesColor(bValue As Long) '给属性赋值
   mLinesColor = bValue
   FF_Redraw hWndControl
End Property
Sub Class_YFList.SetEventProc(Index As Long ,pProc As Any Ptr) '
   EventIndex = Index '用来表示控件数组，没数组就是-1
   EventProc =pProc
End Sub
Function Class_YFList.ListCount() As Long  '检索组合框列表框中的项目数。
  Function = SendMessage( hWndControl, WM_USER + 205, 0, 0)
End Function
Property Class_YFList.ListIndex() As Long                  '返回/设置项目
    Property = SendMessage( hWndControl, WM_USER + 206, 0, 0)
End Property
Property Class_YFList.ListIndex(nIndex As Long)
    SendMessage( hWndControl, WM_USER + 207, nIndex, 0)
End Property
Function Class_YFList.GetTopY() As Long 
   This.Vscroll.hWndForm = hWndControl
   Dim y As Long = This.Vscroll.Value
   Function = - y
   Dim ai As Long  = Int(  y / AfxScaleY(mItemHeight))
   If ai < 0 Then ai = 0
   Function = -y + ai *AfxScaleY(mItemHeight)
   
End Function


