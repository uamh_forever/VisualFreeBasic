﻿#VisualFreeBasic_Form#  Version=5.5.5
Locked=0

[Form]
Name=StyleForm
ClassStyle=CS_DBLCLKS,CS_HREDRAW,CS_VREDRAW
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_MAXIMIZEBOX,WS_MINIMIZEBOX,WS_BORDER,WS_CAPTION,WS_SYSMENU,WS_EX_TOOLWINDOW,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP
Style=4 - 工具窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=样式设置
StartPosition=0 - 手动
WindowState=0 - 正常
Enabled=True
Repeat=True
Left=0
Top=0
Width=222
Height=531
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=1 - 固定行高自绘
ItemHeight=18
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=6
Top=26
Width=205
Height=384
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=5
Top=414
Width=205
Height=87
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=默认值
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=6
Top=2
Width=56
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=还原
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=70
Top=2
Width=51
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]

Dim Shared Style_acc() As String, Style_Sel() As Long
'创建 [事件]   hWndForm=窗体句柄  UserData=可选的用户定义的值
Sub StyleForm_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   Me.UserData(0) = UserData
   Dim As Rect rc,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr,UserData)
   GetWindowRect(st->hWndForm , @rc2)
   GetWindowRect(st->hWndList , @rc)
   if rc.Left + Me.Width > rc2.right Then rc.Left = rc2.right - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top = rc2.bottom - Me.Height
   Me.Move rc.Left, rc.top
   
   Dim i As Long, u As Long

   Dim vv As String = "," & *st->value & ","
   
   u = vbSplit(*st->AllList , ",", Style_acc())
   If u = 0 Then Return
   ReDim Style_Sel(u -1)
   For i = 0 To u -1
      Style_acc(i) = Trim(Style_acc(i))
      List1.AddItem ""
      Style_Sel(i) = InStr(vv, "," & Style_acc(i) & ",") > 0
   Next
   
End Sub


'释放鼠标左键 [事件]   ControlIndex=控件数组索引  hWndForm=窗体句柄  hWndControl=控件句柄  MouseFlags=按下的虚拟键  xPos=光标的x坐标  yPos=光标的y坐标
Sub StyleForm_List1_WM_LButtonUp(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '释放鼠标左键
   If xPos < AfxScaleX(20) Then
      Dim a As Long = List1.ListIndex
      If a = -1 Then Exit Sub
      Style_Sel(a) = Style_Sel(a) = 0
      List1.Refresh
   End If
   
End Sub

'自定义 [事件]   ControlIndex=控件数组索引  hWndForm=窗体句柄  hWndControl=控件句柄  wMsg=消息类型  wParam=第一个消息参数  lParam=第二个消息参数
Function StyleForm_List1_Custom(hWndForm As hWnd, hWndControl As hWnd, wMsg As UInteger, wParam As wParam, lParam As lParam) As LResult  '自定义消息（全部消息），在其它事件后处理，返回非0，终止系统处理此消息。
   Select Case wMsg
      Case WM_VSCROLL
         
      Case WM_MOUSELEAVE   '鼠标出窗口
         'If AnNiuID <> 0 Then
         'AnNiuID = 0
         'AnNiuZ = 0
         'FF_Redraw(hWndForm) '重绘控件
         'End If
      Case WM_NCHITTEST        '启用鼠标出窗口检查
         'Dim entTrack As tagTRACKMOUSEEVENT
         'entTrack.cbSize = SizeOf(tagTRACKMOUSEEVENT)
         'entTrack.dwFlags = TME_LEAVE
         'entTrack.hwndTrack = hWndForm
         'entTrack.dwHoverTime = HOVER_DEFAULT
         'TrackMouseEvent @entTrack
      Case WM_ERASEBKGND
         
         Return True   '防止擦除背景，不加这个会闪的。
   End Select
   
   Function = 0
End Function

'选择了列表 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idListBox=ListBox控件的标识符
Sub StyleForm_List1_LBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '选择了列表
   Dim a As Long = List1.ListIndex
   If a <> -1 Then
      Dim library As Any Ptr = GetModuleHandle(null) 'EXE 模块句柄
      Dim GetStyleHelp As Function(sName As String) As String = DyLibSymbol(library, "GETSTYLEHELP") '获取样式值 UTF8      
      Label1.Caption =Utf8toStr( GetStyleHelp(Style_acc(a)) )
   End If
   
End Sub

'关闭 [事件]   hWndForm=窗体句柄
Function StyleForm_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))

   Dim i As Long, u As Long
   
   Dim vv As String 
   u = UBound(Style_acc)
   If u = -1 Then Return 0
   For i = 0 To u
      If Style_Sel(i) Then vv &= "," & Style_acc(i)
   Next
   st->Rvalue =  Mid(vv, 2)
   Function = 0
End Function

Sub StyleForm_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))

   Dim vv As String = "," & *st->default & ","
   
   For i As Long  = 0 To UBound(Style_Sel)
      Style_Sel(i) = InStr(vv, "," & Style_acc(i) & ",") > 0
   Next
   List1.Refresh 
End Sub

Sub StyleForm_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   Dim vv As String = "," & *st->value  & ","
   
   For i As Long = 0 To UBound(Style_Sel)
      Style_Sel(i) = InStr(vv, "," & Style_acc(i) & ",") > 0
   Next
   List1.Refresh 
End Sub

Function StyleForm_List1_OwnerDraw(hWndForm As hWnd, hWndControl As hWnd, lpdis As DRAWITEMSTRUCT) As LResult  '自绘控件（需要设计时选择自绘属性）
   'lpdis.hDC         设备上下文的句柄
   'lpdis.rcItem      一个矩形，要绘制的控件的边界。
   'lpdis.itemAction  绘图操作: ODA_DRAWENTIRE 需要绘制整个控件, ODA_FOCUS 失去或获得焦点, ODA_SELECT 选择状态已更改
   ''                 例： if (lpdis.itemAction And ODA_DRAWENTIRE)<>0  Then
   'lpdis.itemState   视觉状态  详细说明，选中它 DRAWITEMSTRUCT 然后按 F1 键盘
   Dim rc As Rect
   rc = lpdis.rcItem  '当前行绘画范围
   Select Case lpdis.itemAction
      Case ODA_DRAWENTIRE, ODA_SELECT '要绘画消息
         Dim ki As Long = lpdis.itemID
         If ki = -1 Then Return 0  '如果列表为空 =-1
         Dim gg As yGDI = yGDI(lpdis.hDC, 0, rc.Left, rc.top, rc.Right - rc.Left, rc.bottom - rc.top)
         Dim As Long w=AfxUnscaleX(rc.Right - rc.Left) + 1 , h = AfxUnscaleY(rc.bottom - rc.top)+1 '宽度和高度为 未被DPi缩放的尺寸，因为 yGDI支持DPI缩放
         gg.Pen 0, 0 '不需要轮廓线
         If (lpdis.itemState And ODS_SELECTED) = 0 Then                  ' 未选中
            gg.Brush GetSysColor(COLOR_WINDOW)     ' 画背景，填充底色
            gg.SetColor GetSysColor(COLOR_WINDOWTEXT)        ' 文本颜色
         Else                                                             ' 处于选中状态
            gg.Brush GetSysColor(COLOR_HIGHLIGHT)  ' 画背景，填充底色
            gg.SetColor GetSysColor(COLOR_HIGHLIGHTTEXT)   ' 文本颜色
         End If
         gg.DrawFrame 22, 0, w, h
         gg.DrawTexts 22, 0, w -28, h, Style_acc(ki), DT_SINGLELINE Or DT_LEFT Or DT_VCENTER
         If ki = List1.ListCount -1 Then '最后面不会刷新背景
            DrawFrame(lpdis.hDC, 0, rc.bottom, rc.Right, List1.Height, GetSysColor(COLOR_WINDOW))
         End If
         gg.Font "iconfont", 13
         
         gg.Brush GetSysColor(COLOR_WINDOW)
         gg.DrawFrame 0, 0, 23, h
         If Style_Sel(ki) Then
            gg.SetColor GetSysColor(COLOR_HIGHLIGHT)
            gg.DrawTextS 2, 0, 18, 18, WChr(&HE66A), DT_CENTER Or DT_VCENTER Or DT_SINGLELINE
         Else
            gg.SetColor GetSysColor(COLOR_WINDOWTEXT)
            gg.DrawTextS 2, 0, 18, 18, WChr(&HE67B), DT_CENTER Or DT_VCENTER Or DT_SINGLELINE
         End If
         
         Function = True '告诉系统，表示自己画了，不需要系统处理
   End Select
   Function = FALSE ' 如果处理了此事件，则应返回TRUE。
End Function




