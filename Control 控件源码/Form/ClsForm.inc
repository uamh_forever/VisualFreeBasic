Type Class_Form
Protected : 
   m_Repeat As Boolean     '是否可以多开同一个窗口
Public :    
   hWnd As .hWnd               '返回/设置窗口句柄，多开同一个窗口时，使用前必须指定窗口句柄。
   Declare Sub Hide()                                  '隐藏一个 MDIForm 或 Form 对象，但不卸载它。
   Declare Property Caption() As CWSTR                '设置对象的标题栏中或图标下面的文本。
   Declare Property Caption(ByVal sText As CWSTR)
   Declare Property Enabled() As Boolean                 '返回/设置窗口是否允许操作。{=.True.False}
   Declare Property Enabled(ByVal bValue As Boolean)
   Declare Property Visible() As Boolean                 '显示或隐藏窗口{=.True.False}
   Declare Property Visible(ByVal bValue As Boolean)
   Declare Property Tag() As CWStr                       '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWStr)
   Declare Property Left() As Long                   '返回/设置相对于父窗口的 X（像素）
   Declare Property Left(ByVal nLeft As Long)
   Declare Property Top() As Long                  '返回/设置相对于父窗口的 Y（像素）
   Declare Property Top(ByVal nTop As Long)
   Declare Property Width() As Long                '返回/设置窗口宽度（像素）
   Declare Property Width(ByVal nWidth As Long)
   Declare Property Height() As Long               '返回/设置窗口高度（像素）
   Declare Property Height(ByVal nHeight As Long)
   Declare Sub Move(ByVal nLeft As Long, ByVal nTop As Long, ByVal nWidth As Long = 0, ByVal nHeight As Long = 0) '设置窗口位置和大小，高度、宽度=0时不修改。
   Declare Sub SIZE(ByVal nWidth As Long , ByVal nHeight As Long ) '设置窗口高度、宽度 时不修改。
   Declare Property WindowState() As Integer             '{=.0'正常.1'最小化.2'最大化}
   Declare Property WindowState(ByVal bValue As Integer)
   Declare Function ScaleWidth() As Long                '返回/设置对象内部的W（像素）
   Declare Function ScaleHeight() As Long               '返回/设置对象内部的H（像素）
   Declare Sub Refresh()  '刷新窗口
   Declare Sub CenterWindow(ByVal hWndP As.hWnd = NULL) '窗口居中，默认在屏幕上，有句柄为在其窗口上居中
   Declare Sub SetForegroundWindow() '置于前台并激活窗口。
   Declare Property hWndParent() As.hWnd    '返回/设置父窗口句柄
   Declare Property hWndParent(ByVal hWndNewParent As.hWnd)    '指定一个窗口的新父，返回原来的父句柄
   Declare Property WindowsZ() As Boolean  '窗口Z顺序 设置在某个窗口之上或是{=.HWND_BOTTOM 普通层所有窗口最后.HWND_TOP 普通层所有窗口最前.HWND_NOTOPMOST 普通层窗口.HWND_TOPMOST 顶层窗口(置顶).某窗口句柄 将置于此窗口前}
   Declare Property WindowsZ(hWndlnsertAfter As.hWnd) '返回是否前置
   Declare Function FlashWindow() As Boolean '激活窗口并且闪耀窗口
   Declare Sub Close() '关闭本窗口，如果是软件第一个窗口，将会关闭整个软件。
   Declare Property hWndForm() As.hWnd        '返回/设置本窗口句柄，主要用于多开同一个窗口后，必须先指定窗口句柄，才能正常使用。
   Declare Property hWndForm(hForm As.hWnd)   '与 hWnd 属性相同，是为了和控件属性保持一致。
   Declare Property Icon() As hIcon     '返回/设置本窗口图标，设置后的图标句柄不可销毁。
   Declare Property Icon(Ico As hIcon)  '设置为NULL，则删除图标。
   Declare Sub MoveWin() '用来拖动窗口，鼠标点下可以拖动窗口，放在窗口的鼠标按下事件里即可。
   Declare Function ClassName() As CWSTR     '获取窗口类名
   Declare Property UserData(idx AS LONG) As Integer      '返回/设置用户数据，idx索引号，范围为0至99。就是1个窗口可以存放100个数值。
   Declare Property UserData(idx AS LONG, bValue As Integer)
   Declare Property MousePass() As Boolean      '返回/设置窗口是否鼠标穿透效果（只能看到而无法用鼠标点到）{=.True.False}
   Declare Property MousePass(bValue As Boolean)
   Declare Property TransPer() As Long      '返回/设置窗口透明度，百分比(0--100)，0%不透明 100%全透明
   Declare Property TransPer(bValue As Long)
   Declare Property TransColor() As Long  '返回/设置窗口透明颜色，RGB颜色值(用 BGR(r, g, b) 获取)，窗口上的此颜色将 100%全透明 （半透明实现不了）注：设为-1 为取消透明色
   Declare Property TransColor(bValue As Long)
   Declare Property AcceptFiles() As Boolean  '返回/设置窗口是否接受拖放文件。{=.True.False}
   Declare Property AcceptFiles(bValue As Boolean)
   Declare Function WindowPlacementSave(FileIni As CWSTR , nName As CWSTR) As Boolean   '在INI文件里保存窗口位置、大小。成功返回 True (通常在窗口销毁事件里)
   Declare Sub WindowPlacementLoad(FileIni As CWSTR , nName As CWSTR)  '从INI文件里加载窗口位置、大小来恢复窗口。 (通常在窗口加载事件里)
   Declare Property MousePointer() As Long   '返回/设置鼠标形状。{=.0 - 默认.1 - 后台运行.2 - 标准箭头.3 - 十字光标.4 - 箭头和问号.5 - 文本工字光标.6 - 不可用禁止圈.7 - 移动.8 - 双箭头↙↗.9 - 双箭头↑↓.10 - 双箭头向↖↘.11 - 双箭头←→.12 - 垂直箭头.13 - 沙漏.14 - 手型}
   Declare Property MousePointer(bValue As Long) '
   Declare Property Cursor() As HCURSOR       '返回/设置鼠标形状的光标句柄，可用 LoadCursor 加载，VFB自动处理句柄，加载后不可以销毁句柄。
   Declare Property Cursor(bValue As HCURSOR)
   Declare Property BackColor() As Long           '返回/设置背景色：特殊合成色，设置用：BGR (r, g, b) 
   Declare Property BackColor(nColor As Long)
   Declare Property ToolTip() As CWSTR              '返回/设置控件的一个提示，当鼠标光标悬停在控件时显示它。
   Declare Property ToolTip(ByVal sText As CWSTR)
   Declare Property ToolTipBalloon() As Boolean  '返回/设置控件是否一个气球样式显示工具提示。。{=.True.False}
   Declare Property ToolTipBalloon(bValue As Boolean)
   Declare Sub SetFocus()  ' 使窗口获取键盘焦点，窗口中有焦点的控件，会将焦点转移到此控件。
   Declare Property Repeat() As Boolean               '多开窗口， 窗口是否可以同时开启很多个{=.True.False}
   Declare Property Repeat(ByVal bValue As Boolean)   
'   Declare Function AnchorControl (BYVAL hwndCtl AS HWND, BYVAL nAnchorMode AS LONG) AS BOOLEAN
End Type
'发现 Constructor Destructor 缺点很多，就取消了。 
'Constructor Class_Form
'   '注意：由于窗口类是永久的，全局变量，刚开软件时才只执行1次。
'   '此时还没有初始化窗口，没有实物窗口等各中相关数据
'End Constructor
'Destructor Class_Form
'   '注意：由于窗口类是永久的，全局变量，关闭软件后会执行1次
'   '此时窗口早被销毁，已经没有实物窗口等各中相关数据，需要执行与窗口内容相关，请在窗口销毁事件里处理
'End Destructor

Property Class_Form.hWndForm() As .hWnd                    '句柄
   Return hWnd
End Property
Property Class_Form.hWndForm(ByVal hForm As .hWnd)        '句柄
   hWnd = hForm
End Property
Sub Class_Form.Hide()
  if IsWindow(hWnd) Then ShowWindow(hWnd, SW_HIDE)
End Sub
Property Class_Form.Caption() As CWStr
   Return AfxGetWindowText(hWnd)
End Property
Property Class_Form.Caption(ByVal sText As CWStr)
   if IsWindow(hWnd) Then AfxSetWindowText hWnd, **sText
End Property
Property Class_Form.Enabled() As Boolean                 '使能
   Return IsWindowEnabled(hWnd)
End Property
Property Class_Form.Enabled(ByVal bValue As Boolean)
  if IsWindow(hWnd) Then  EnableWindow(hWnd, bValue)
End Property
Property Class_Form.Repeat() As Boolean               
   Return m_Repeat
End Property
Property Class_Form.Repeat(ByVal bValue As Boolean)
   m_Repeat = bValue 
End Property
Sub Class_Form.SetFocus()  ' 获取键盘焦点
   if IsWindow(hWnd) Then   .SetFocus hWnd
End Sub
Property Class_Form.Tag() As CWSTR
   Dim fp As FormControlsPro_TYPE ptr =vfb_Get_Control_Ptr(hWnd)
   If fp Then
      Return fp->Tag
   End If
End Property
Property Class_Form.Tag(ByVal sText As CWSTR)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
   If fp Then
      fp->Tag = sText
   End If
End Property
Property Class_Form.Left() As Long                '
   Dim rc As Rect
   if IsWindow(hWnd) Then
      GetWindowRect hWnd           ,@rc          '获得窗体大小
      MapWindowPoints HWND_DESKTOP ,.GetParent(hWnd) ,Cast(LPPOINT ,@rc) ,2
   end if
   Return rc.Left
End Property
Property Class_Form.Left(ByVal nLeft As Long)
   If IsWindow(hWnd) Then
      Dim rc As Rect
      GetWindowRect hWnd, @rc          '获得窗体大小
      MapWindowPoints HWND_DESKTOP, .GetParent(hWnd), Cast(LPPOINT, @rc), 2
      SetWindowPos(hWnd, 0, nLeft, rc.top, 0, 0, SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
      if fp then
         fp->nLeft = AfxUnscaleX(nLeft)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Property Class_Form.Top() As Long     '
   Dim rc As Rect
   if IsWindow(hWnd) Then
      GetWindowRect hWnd           ,@rc     '获得窗体大小
      MapWindowPoints HWND_DESKTOP ,.GetParent(hWnd) ,Cast(LPPOINT ,@rc) ,2
   End if
   Return rc.Top
End Property
Property Class_Form.Top(ByVal nTop As Long)
   If IsWindow(hWnd) Then
      Dim rc As Rect
      GetWindowRect hWnd           ,@rc          '获得窗体大小
      MapWindowPoints HWND_DESKTOP ,.GetParent(hWnd) ,Cast(LPPOINT ,@rc) ,2
      SetWindowPos(hWnd ,0 ,rc.Left ,nTop ,0 ,0 ,SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
      if fp then
         fp->nTop = AfxUnscaleY(nTop)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Property Class_Form.Height() As Long                  '
   Dim rc As Rect
   if IsWindow(hWnd) Then GetWindowRect hWnd ,@rc          '获得窗体大小
   Return rc.Bottom - rc.Top
End Property
Property Class_Form.Height(ByVal nHeight As Long)
   If IsWindow(hWnd) Then
      Dim rc As Rect
      GetWindowRect hWnd ,@rc          '获得窗体大小
      SetWindowPos(hWnd ,0 ,0 ,0 ,rc.Right - rc.Left ,nHeight ,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
      if fp then
         fp->nHeight = AfxUnscaleY(nHeight)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Property Class_Form.Width() As Long
   Dim rc As Rect
   if IsWindow(hWnd) Then GetWindowRect hWnd ,@rc          '获得窗体大小
   Return rc.Right - rc.Left
End Property
Property Class_Form.Width(ByVal nWidth As Long)
   If IsWindow(hWnd) Then
      Dim rc As Rect
      GetWindowRect hWnd ,@rc          '获得窗体大小
      SetWindowPos(hWnd ,0 ,0 ,0 ,nWidth ,rc.Bottom - rc.Top ,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
      if fp then
         fp->nWidth = AfxUnscaleX(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
      end if
   End If
End Property
Sub Class_Form.Move(ByVal nLeft As Long ,ByVal nTop As Long ,ByVal nWidth As Long = 0 ,ByVal nHeight As Long = 0)
   If IsWindow(hWnd) Then
      Dim rc As Rect
      GetWindowRect hWnd ,@rc          '获得窗体大小
      If nWidth <= 0  Then nWidth  = rc.Right  - rc.Left
      If nHeight <= 0 Then nHeight = rc.Bottom - rc.Top
      SetWindowPos(hWnd ,0 ,nLeft ,nTop ,nWidth ,nHeight ,SWP_NOZORDER Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
      if fp then
         fp->nLeft   = AfxUnscaleX(nLeft)
         fp->nTop    = AfxUnscaleY(nTop)
         fp->nWidth  = AfxUnscaleY(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
         fp->nHeight = AfxUnscaleY(nHeight)
      end if
   end if
End Sub
Sub Class_Form.Size(ByVal nWidth As Long , ByVal nHeight As Long )
   If IsWindow(hWnd) Then   
      SetWindowPos(hWnd ,0 ,0 ,0 ,nWidth ,nHeight ,SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
      if fp then
         fp->nWidth  = AfxUnscaleY(nWidth)  'VFB 内部记录的是响应DPI的数值，就是永远是 DPI=100%时的数值
         fp->nHeight = AfxUnscaleY(nHeight)
      end if      
   End if    
End Sub
Property Class_Form.Visible() As Boolean                 '可见
   Return IsWindowVisible(hWnd)
End Property
Property Class_Form.Visible(ByVal bValue As Boolean)
   if IsWindow(hWnd) Then
      If bValue Then
         If (GetWindowLongPtr(hWnd ,GWL_EXSTYLE) And WS_EX_NOACTIVATE) Then
            ShowWindow(hWnd ,SW_SHOWNOACTIVATE)
         Else
            ShowWindow(hWnd ,SW_SHOW)
         End if
      Else
         ShowWindow(hWnd ,SW_HIDE)
      End If
   end if
End Property
Property Class_Form.WindowState() As Integer
   Dim m_WndState As Integer
   if IsWindow(hWnd) Then
      If IsIconic(hWnd) Then      '最小化
         m_WndState = 1
      ElseIf IsZoomed(hWnd) Then  '最大化
         m_WndState = 2
      Else
         m_WndState = 0           '正常
      End If
   end if
   Return m_WndState
End Property
Property Class_Form.WindowState(ByVal iTxt As Integer)
   if IsWindow(hWnd) Then
      Select Case iTxt
         Case 0
            ShowWindow(hWnd ,SW_RESTORE)  '用原来的大小和位置显示一个窗口，同时令其进入活动状态
         Case 1
            ShowWindow(hWnd ,SW_MINIMIZE) '//最小化
         Case 2
            ShowWindow(hWnd ,SW_MAXIMIZE) '//最大化
         Case Else
      End Select
   End if
End Property
Function Class_Form.ScaleHeight() As Long               '
   Dim rc As Rect
   if IsWindow(hWnd) Then GetClientRect(hWnd, @rc)     '获得客户区大小
   Return rc.Bottom - rc.Top
End Function
Function Class_Form.ScaleWidth() As Long                   '
   Dim rc As Rect
   if IsWindow(hWnd) Then GetClientRect(hWnd, @rc)     '获得客户区大小
   Return rc.Right - rc.Left
End Function
'Property Class_Form.WindowStyle() As DWord    '返回/设置窗口样式
'Return AfxGetWindowStyle(m_hWindow)
'End Property
'Property Class_Form.WindowStyle(ByVal dwStyle  As DWord) As DWord
'Return AfxSetWindowStyle(m_hWindow,dwStyle)
'End Property
'Property Class_Form.WindowExStyle() As DWord               '返回/设置窗口扩展样式
'Return AfxGetWindowExStyle(m_hWindow)
'End Property
'Property Class_Form.WindowExStyle(ByVal dwStyle As DWord) As DWord
'Return AfxSetWindowExStyle(m_hWindow,dwStyle)
'End Property
Sub Class_Form.Refresh()
   if IsWindow(hWnd) Then 
   InvalidateRect(hWnd, Null, True)
   UpdateWindow(hWnd)
   end if 
End Sub
Sub Class_Form.CenterWindow(ByVal hWndP As .hWnd = NULL) '窗口居中，默认在屏幕上，有句柄为在其窗口上居中
   If IsWindow(hWnd) Then AfxCenterWindow(hWnd, hWndP)
End Sub
Sub Class_Form.SetForegroundWindow() '置于前台并激活窗口。
   if IsWindow(hWnd) Then.SetForegroundWindow(hWnd)
End Sub
Property Class_Form.hWndParent() As.hWnd '获取父窗口句柄
   If IsWindow(hWnd) Then
      Dim ff As .hWnd = GetAncestor(hWnd ,GA_PARENT)
      If ff = GetDesktopWindow Then ff = 0
      Property = ff
   End If
End Property
Property Class_Form.hWndParent(ByVal hWndNewParent As.hWnd)     '指定一个窗口的新父，返回原来的父句柄
   If IsWindow(hWnd) Then SetParent(hWnd ,hWndNewParent)
   This.Move 0 ,0
End Property
Property Class_Form.WindowsZ() As Boolean
   'HWND_BOTTOM,HWND_TOP,HWND_NOTOPMOST,HWND_TOPMOST,设置窗口Z位置:最后，最前，普通，置顶
   If IsWindow(hWnd) Then Return(AfxGetWindowExStyle(hWnd) And WS_EX_TOPMOST) = WS_EX_TOPMOST
End Property
Property Class_Form.WindowsZ(ByVal hWndlnsertAfter As .hWnd)
   'HWND_BOTTOM,HWND_TOP,HWND_NOTOPMOST,HWND_TOPMOST,设置窗口Z位置:最后，最前，普通，置顶
   If IsWindow(hWnd) Then SetWindowPos(hWnd, hWndlnsertAfter, 0, 0, 0, 0, SWP_NOSIZE Or SWP_NOMOVE Or SWP_SHOWWINDOW)
End Property
Function Class_Form.FlashWindow() As Boolean '激活窗口并且闪耀窗口
   If IsWindow(hWnd) Then
      Dim aa As FLASHWINFO
      aa.cbSize    = SizeOf(FLASHWINFO)
      aa.hWnd      = hWnd
      aa.dwFlags   = FLASHW_ALL
      aa.uCount    = 5
      aa.dwTimeout = 100
      OpenIcon hWnd
      .SetForegroundWindow hWnd
      Function = FlashWindowEx(@aa)
   end if
End Function
Sub Class_Form.Close() '关闭本窗口，如果是软件第一个窗口，将会关闭整个软件。
  if IsWindow(hWnd) Then  PostMessage hWnd, WM_CLOSE, 0, 0
End Sub
Property Class_Form.Icon() As hIcon        '返回/设置本窗口小图标(普通图标)，设置后的图标句柄不可销毁，系统在ALT + TAB对话框中显示大图标，在窗口标题中显示小图标。
   if IsWindow(hWnd) Then
      Dim aa As HICON
      aa = Cast(hicon ,SendMessage(hWnd ,WM_GETICON ,ICON_SMALL ,0))
      '没有显式设置图标的窗口（使用WM_SETICON）使用已注册窗口类的图标，在这种情况下，DefWindowProc将为WM_GETICON消息返回0 。
      '如果向窗口发送WM_GETICON消息返回0，则接下来尝试调用该窗口的GetClassLongPtr函数。如果返回0，则尝试LoadIcon函数。
      If aa = 0 Then aa = Cast(hicon ,GetClassLongPtr(hWnd ,GCLP_HICON))
      Return aa
   end if
End Property
Property Class_Form.Icon(Ico As hIcon)  '设置为NULL，则删除图标。
   if IsWindow(hWnd) Then
      Dim aa As HICON = Cast(hicon ,SendMessage(hWnd ,WM_SETICON ,ICON_SMALL ,Cast(lParam ,Ico)))
      If aa <> 0 Then DeleteObject Cast(HGDIOBJ ,aa)
      aa = Cast(hicon ,SendMessage(hWnd ,WM_SETICON ,ICON_BIG ,Cast(lParam ,Ico)))
      If aa <> 0 Then DeleteObject Cast(HGDIOBJ ,aa)
   End if
End Property
Sub Class_Form.MoveWin() '用来拖动窗口，鼠标点下可以拖动窗口，放在窗口的鼠标按下事件里即可。
   if IsWindow(hWnd) Then 
      ReleaseCapture
      SendMessage hWnd ,WM_NCLBUTTONDOWN ,HTCAPTION ,Null
   End if 
End Sub
Function Class_Form.ClassName() As CWSTR '获取窗口类名
   Dim wszClassName As wString * 260
   If IsWindow(hWnd) Then
      GetClassNameW hWnd ,@wszClassName ,SizeOf(wszClassName)
      Return wszClassName
   End If
End Function
Property Class_Form.UserData(idx As Long) As Integer '返回/设置用户数据，就是1个窗口可以存放1个数值。
   If idx < 0 Or idx > 99 Then Return 0
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWnd)
   If fp Then
      Return fp->UserData(idx)
   End If
   
End Property
Property Class_Form.UserData(idx As Long ,bValue As Integer)
   If idx < 0 Or idx > 99 Then Return
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWnd)
   If fp Then
      fp->UserData(idx) = bValue
   End If
End Property
Property Class_Form.MousePass() As Boolean      '返回/设置窗口鼠标穿透效果（只能看到而无法用鼠标点到）{=.True.False}
   If IsWindow(hWnd) Then Return (AfxGetWindowExStyle(hWnd) And WS_EX_TRANSPARENT) = WS_EX_TRANSPARENT
End Property
Property Class_Form.MousePass(bValue As Boolean)
   If IsWindow(hWnd) Then
      Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWnd)
      If fp Then
         If bValue Then
            AfxAddWindowExStyle hWnd ,WS_EX_TRANSPARENT
            AfxAddWindowExStyle hWnd ,WS_EX_LAYERED
         Else
            AfxRemoveWindowExStyle hWnd ,WS_EX_TRANSPARENT
            Dim cc As Long = GetCodeColorGDI(fp->TransColor)
            If fp->TransPer = 0 And cc = -1 Then
               AfxRemoveWindowExStyle hWnd ,WS_EX_LAYERED
            End If
         End If
      end if
   end if
End Property
Property Class_Form.TransPer() As Long      '返回/设置窗口透明度，百分比(0--100)，0%不透明 100%全透明
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWnd)
   If fp     Then Return fp->TransPer
End Property
Property Class_Form.TransPer(bValue As Long)
   If IsWindow(hWnd) Then
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
      if fp then
         fp->TransPer = bValue
         If fp->TransPer < 0   Then fp->TransPer = 0
         If fp->TransPer > 100 Then fp->TransPer = 100
         Dim cc As Long = GetCodeColorGDI(fp->TransColor)
         If fp->TransPer = 0 And cc = -1 And MousePass = 0 Then
            AfxRemoveWindowExStyle hWnd ,WS_EX_LAYERED
         Else
            AfxAddWindowExStyle hWnd ,WS_EX_LAYERED
            If fp->TransPer = 0 And cc <> -1 Then
               SetLayeredWindowAttributes hWnd ,cc ,0 ,LWA_COLORKEY
            ElseIf fp->TransPer > 0 Then
               If cc = -1 Then
                  SetLayeredWindowAttributes hWnd ,0 ,(100 - fp->TransPer) * 2.55 ,LWA_ALPHA
               Else
                  SetLayeredWindowAttributes hWnd ,cc ,(100 - fp->TransPer) * 2.55 ,LWA_ALPHA Or LWA_COLORKEY
               End If
            End If
         End If
      end if
   End if
End Property
Property Class_Form.TransColor() As Long      '返回/设置窗口透明颜色，窗口上的此颜色将 100%全透明 （半透明实现不了）
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      Return GetCodeColorGDI(fp->TransColor)
   end if
End Property
Property Class_Form.TransColor(bValue As Long)
   if IsWindow(hWnd) Then
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
      If fp Then
         fp->TransColor = bValue
         Dim cc As Long = bValue
         If fp->TransPer = 0 And cc = -1 And MousePass = 0 Then
            AfxRemoveWindowExStyle hWnd ,WS_EX_LAYERED
         Else
            AfxAddWindowExStyle hWnd ,WS_EX_LAYERED
            If cc = -1 And fp->TransPer > 0 Then
               SetLayeredWindowAttributes hWnd ,0 ,(100 - fp->TransPer) * 2.55 ,LWA_ALPHA
            ElseIf cc <> -1 Then
               If fp->TransPer = 0 Then
                  SetLayeredWindowAttributes hWnd ,cc ,0 ,LWA_COLORKEY
               Else
                  SetLayeredWindowAttributes hWnd ,cc ,(100 - fp->TransPer) * 2.55 ,LWA_ALPHA Or LWA_COLORKEY
               End If
            End If
         End If
      end if
   End if
End Property
Property Class_Form.AcceptFiles() As Boolean  '返回/设置窗口是否接受拖放文件。
   if IsWindow(hWnd) Then Return(AfxGetWindowExStyle(hWnd) And WS_EX_ACCEPTFILES) = WS_EX_ACCEPTFILES
End Property
Property Class_Form.AcceptFiles(bValue As Boolean)
   if IsWindow(hWnd) Then
      If bValue Then
         AfxAddWindowExStyle hWnd ,WS_EX_ACCEPTFILES
      Else
         AfxRemoveWindowExStyle hWnd ,WS_EX_ACCEPTFILES
      End If
   end if
End Property
Function Class_Form.WindowPlacementSave(FileIni As CWSTR ,nName As CWSTR) As Boolean '在INI文件里保存窗口位置、大小。
   '// 获取当前窗口位置
   if IsWindow(hWnd) Then
      Dim WinPla As WINDOWPLACEMENT
      WinPla.Length = SizeOf(WinPla)
      GetWindowPlacement(hWnd ,@WinPla)
      '// 将值保存在.ini文件中
      Dim bRes As BOOLEAN
      bRes = WritePrivateProfileStringW(nName ,"Left" ,wStr(WinPla.rcNormalPosition.Left) ,FileIni)
      bRes = WritePrivateProfileStringW(nName ,"Right" ,wStr(WinPla.rcNormalPosition.Right) ,FileIni)
      bRes = WritePrivateProfileStringW(nName ,"Top" ,wStr(WinPla.rcNormalPosition.Top) ,FileIni)
      bRes = WritePrivateProfileStringW(nName ,"Bottom" ,wStr(WinPla.rcNormalPosition.Bottom) ,FileIni)
      Dim aa As Long = (AfxGetWindowExStyle(hWnd) And WS_EX_TOPMOST) = WS_EX_TOPMOST
      bRes = WritePrivateProfileStringW(nName ,"TopMost" ,wStr(aa) ,FileIni)
      bRes = WritePrivateProfileStringW(nName ,"flags" ,wStr(WinPla.flags) ,FileIni)
      bRes = WritePrivateProfileStringW(nName ,"showCmd" ,wStr(WinPla.showCmd) ,FileIni)
      
      Return bRes
   end if
End Function
Sub Class_Form.WindowPlacementLoad(FileIni As CWSTR ,nName As CWSTR)  '从INI文件里加载窗口位置、大小来恢复窗口。
   '// 获取当前窗口位置
   if IsWindow(hWnd) Then
      Dim WinPla As WINDOWPLACEMENT
      WinPla.Length = SizeOf(WinPla)
      GetWindowPlacement(hWnd ,@WinPla)
      '// 读取.ini文件中保存的值
      Dim wszBuffer As wString * 260 ,dwChars As DWord
      dwChars = GetPrivateProfileStringW(nName ,"Left" ,Null ,@wszBuffer ,MAX_PATH ,FileIni)
      If dwChars Then WinPla.rcNormalPosition.Left = Val(wszBuffer)
      dwChars = GetPrivateProfileStringW(nName ,"Right" ,Null ,@wszBuffer ,MAX_PATH ,FileIni)
      If dwChars                           Then WinPla.rcNormalPosition.Right = Val(wszBuffer)
      If WinPla.rcNormalPosition.Right = 0 Then WinPla.rcNormalPosition.Right = 750
      dwChars = GetPrivateProfileStringW(nName ,"Top" ,Null ,@wszBuffer ,MAX_PATH ,FileIni)
      If dwChars Then WinPla.rcNormalPosition.Top = Val(wszBuffer)
      dwChars = GetPrivateProfileStringW(nName ,"Bottom" ,Null ,@wszBuffer ,MAX_PATH ,FileIni)
      If dwChars                            Then WinPla.rcNormalPosition.Bottom = Val(wszBuffer)
      If WinPla.rcNormalPosition.Bottom = 0 Then WinPla.rcNormalPosition.Bottom = 450
      dwChars = GetPrivateProfileStringW(nName ,"TopMost" ,Null ,@wszBuffer ,MAX_PATH ,FileIni)
      If dwChars Then
         If Val(wszBuffer) Then
            SetWindowPos hWnd ,HWND_TOPMOST ,0 ,0 ,0 ,0 ,&H2 Or &H1
         Else
            SetWindowPos hWnd ,HWND_NOTOPMOST ,0 ,0 ,0 ,0 ,&H2 Or &H1
         End If
      End If
      dwChars = GetPrivateProfileStringW(nName ,"flags" ,Null ,@wszBuffer ,MAX_PATH ,FileIni)
      If dwChars Then WinPla.flags = Val(wszBuffer)
      dwChars = GetPrivateProfileStringW(nName ,"showCmd" ,Null ,@wszBuffer ,MAX_PATH ,FileIni)
      If dwChars Then WinPla.showCmd = Val(wszBuffer)
      
      SetWindowPlacement hWnd ,@WinPla
      '// 桌面的实际尺寸
      Dim rcDeskTop As RECT
      SystemParametersInfo SPI_GETWORKAREA ,0 ,@rcDesktop ,0
      '// 确保对话框没有水平过大
      If WinPla.rcNormalPosition.Right - WinPla.rcNormalPosition.Left > rcDesktop.Right Then
         WinPla.rcNormalPosition.Left  = 0
         WinPla.rcNormalPosition.Right = rcDesktop.Right
      End If
      '// 确保对话框不是垂直超大的
      If WinPla.rcNormalPosition.Bottom - WinPla.rcNormalPosition.Top > rcDesktop.Bottom Then
         WinPla.rcNormalPosition.Top    = 0
         WinPla.rcNormalPosition.Bottom = rcDesktop.Bottom
      End If
      '// 确保对话框的左侧可见
      If WinPla.rcNormalPosition.Left < 0 Then
         WinPla.rcNormalPosition.Right = WinPla.rcNormalPosition.Right - WinPla.rcNormalPosition.Left
         WinPla.rcNormalPosition.Left  = 0
      End If
      '// 确保对话框的右侧可见
      If WinPla.rcNormalPosition.Right > rcDesktop.Right Then
         WinPla.rcNormalPosition.Left  = WinPla.rcNormalPosition.Left - (WinPla.rcNormalPosition.Right - rcDesktop.Right)
         WinPla.rcNormalPosition.Right = rcDesktop.Right
      End If
      '// 确保对话框的顶部可见
      If WinPla.rcNormalPosition.Top < 0 Then
         WinPla.rcNormalPosition.Bottom = WinPla.rcNormalPosition.Bottom - WinPla.rcNormalPosition.Top
         WinPla.rcNormalPosition.Top    = 0
      End If
      '// 确保对话框的底部可见
      If WinPla.rcNormalPosition.Bottom > rcDesktop.Bottom Then
         WinPla.rcNormalPosition.Top    = WinPla.rcNormalPosition.Top - (WinPla.rcNormalPosition.Bottom - rcDesktop.Bottom)
         WinPla.rcNormalPosition.Bottom = rcDesktop.Bottom
      End If
      '// 调整工作区域
      Dim rc As RECT
      SystemParametersInfo SPI_GETWORKAREA ,0 ,@rc ,0
      If WinPla.rcNormalPosition.Left = rc.Left And WinPla.rcNormalPosition.Right = rc.Right Then WinPla.rcNormalPosition.Right  = WinPla.rcNormalPosition.Right
      If WinPla.rcNormalPosition.Top = rc.Top And WinPla.rcNormalPosition.Bottom = rc.Bottom Then WinPla.rcNormalPosition.Bottom = WinPla.rcNormalPosition.Bottom
      '// 放置窗口
      SetWindowPlacement hWnd ,@WinPla
   end if
   
End Sub
Property Class_Form.MousePointer() As Long
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      Return fp->MousePointer
   End If
End Property
Property Class_Form.MousePointer(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      fp->MousePointer = bValue
      Select Case bValue
         Case 0  : fp->nCursor = Null
         Case 1  : fp->nCursor = LoadCursor(Null ,IDC_APPSTARTING)  '1 - 后台运行
         Case 2  : fp->nCursor = LoadCursor(Null ,IDC_ARROW)        '2 - 标准箭头
         Case 3  : fp->nCursor = LoadCursor(Null ,IDC_CROSS)        '3 - 十字光标
         Case 4  : fp->nCursor = LoadCursor(Null ,IDC_HELP)         '4 - 箭头和问号
         Case 5  : fp->nCursor = LoadCursor(Null ,IDC_IBEAM)        '5 - 文本工字光标
         Case 6  : fp->nCursor = LoadCursor(Null ,IDC_NO)           '6 - 不可用禁止圈
         Case 7  : fp->nCursor = LoadCursor(Null ,IDC_SIZEALL)      '7 - 移动
         Case 8  : fp->nCursor = LoadCursor(Null ,IDC_SIZENESW)     '8 - 双箭头↙↗
         Case 9  : fp->nCursor = LoadCursor(Null ,IDC_SIZENS)       '9 - 双箭头↑↓
         Case 10 : fp->nCursor = LoadCursor(Null ,IDC_SIZENWSE)     '10 - 双箭头向↖↘
         Case 11 : fp->nCursor = LoadCursor(Null ,IDC_SIZEWE)       '11 - 双箭头←→
         Case 12 : fp->nCursor = LoadCursor(Null ,IDC_UPARROW)      '12 - 垂直箭头
         Case 13 : fp->nCursor = LoadCursor(Null ,IDC_WAIT)         '13 - 沙漏
         Case 14 : fp->nCursor = LoadCursor(Null ,IDC_HAND)         '14 - 手型
         case Else
            fp->nCursor = NULL
      End Select
   end if
End Property
Property Class_Form.Cursor() As HCURSOR
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      Return fp->nCursor
   End If
End Property
Property Class_Form.Cursor(bValue As HCURSOR)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWnd)
   If fp Then
      fp->nCursor = bValue
   end if
End Property
Property Class_Form.BackColor() As Long                  '返回/设置背景色： RGB 颜色值(用 BGR(r, g, b) 获取)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->BackColor ,GetSysColor(COLOR_BTNFACE))
      Return cc
   End If
End Property
Property Class_Form.BackColor(nColor As Long)
   If IsWindow(hWnd) Then
      Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWnd)
      If fp Then
         fp->BackColor = nColor
         Dim pWindow As CWindow Ptr = AfxCWindowPtr(hWnd)
         if pWindow then
            if pWindow->Brush Then DeleteObject pWindow->Brush  '销毁以前的
            pWindow->Brush = CreateSolidBrush(nColor)
         End if
      End If
   end if
End Property
Property Class_Form.ToolTip() As CWSTR              '返回/设置控件的一个提示，当鼠标光标悬停在控件时显示它。
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      Return fp->ToolTip
   End If
End Property
Property Class_Form.ToolTip(ByVal sText As CWSTR)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      fp->ToolTip = sText
      If IsWindow(fp->ToolWnd) Then DestroyWindow fp->ToolWnd
      if Len( * *fp->ToolTip) then
        if IsWindow(hWnd) Then  fp->ToolWnd = FF_AddTooltip(hWnd ,fp->ToolTip ,fp->ToolTipBalloon)
      End If
   end if
End Property
Property Class_Form.ToolTipBalloon() As Boolean  '返回/设置控件是否一个气球样式显示工具提示。。{=.True.False}
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      Return fp->ToolTipBalloon
   End If
End Property
Property Class_Form.ToolTipBalloon(bValue As Boolean)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWnd)
   if fp then
      fp->ToolTipBalloon = bValue
      If fp->ToolWnd Then DestroyWindow fp->ToolWnd
      if Len( * *fp->ToolTip) then
        if IsWindow(hWnd) Then  fp->ToolWnd = FF_AddTooltip(hWnd ,fp->ToolTip ,fp->ToolTipBalloon)
      End If
   end if
End Property

