Type Class_Shape Extends Class_Virtual '属于虚拟控件，是直接画在窗口上的 ，所有私有属性记录，保存在主窗口中，

    Declare Property Style() As Long      '返回/设置指示控件边界的外观和行为。{=.0 - 矩形.1 - 圆形.2 - 圆角矩形.3 - 正三角形.4 - 倒三角形.5 - 左三角形.6 - 右三角形.7 - 菱形.8 - 五角形.9 - 内五角.10 - 六角形.11 - 六角形2}
    Declare Property Style(bValue As Long) 
    Declare Property BorderColor() As Long                  '返回/设置线条色彩：设置用：BGR(r, g, b)  
    Declare Property BorderColor(nColor As Long)
    Declare Property FillColor() As Long                  '返回/设置背景色： 设置用：BGR(r, g, b)  
    Declare Property FillColor(nColor As Long)
    Declare Property BorderColorGDIplue() As ARGB                  '返回/设置线条色彩： GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a) 
    Declare Property BorderColorGDIplue(GDIplue_Color As ARGB)
    Declare Property FillColorGDIplue() As ARGB                  '返回/设置背景色： GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a) 
    Declare Property FillColorGDIplue(GDIplue_Color As ARGB)
    Declare Property FillStyle() As Long      '返回/设置填充样式。{=.0 - 无填充.1 - 纯色.2 渐变-左右.3 渐变-上下.4 渐变-右左.5 渐变-下上.6 渐变-左上斜.7 渐变-左下斜.8 渐变-右上斜.9 渐变-右下斜}
    Declare Property FillStyle(bValue As Long) 
    Declare Property BorderWidth() As Long      '返回/设置边框宽度0到15
    Declare Property BorderWidth(bValue As Long) 
    Declare Sub Drawing(gg as yGDI,hWndForm As hWnd,nBackColor As Long)  '描绘控件，窗口需要描绘，通知类把控件画到 gg 中。（** VFB系统自用）    
End Type
'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_Shape.Style() As Long  '样式结构：&H12345678  12主样式 34填充 5宽度   678 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &HFF000000) Shr 24
   End If
End Property
Property Class_Shape.Style(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      If sy > 11 Then sy = 11
      fp->Style = (fp->Style And &H00FFFFFF) Or (sy Shl 24)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Shape.BorderColor() As Long    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->ForeColor)
      if cc = -1 then cc = GetSysColor(COLOR_BTNTEXT)    
      Return cc      
   End If
End Property
Property Class_Shape.BorderColor(ByVal nColor As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->ForeColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明 
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Shape.FillColor() As Long    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->BackColor)
      if cc = -1 then cc = GetSysColor(COLOR_BTNFACE)      
      Return cc      
   End If
End Property
Property Class_Shape.FillColor(ByVal nColor As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->BackColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明 
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Shape.BorderColorGDIplue() As ARGB    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->ForeColor)
      if cc = 0 then
         cc = GetSysColor(COLOR_BTNTEXT)
         Dim b As UByte Ptr = Cast(Any Ptr, @cc ) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0], b[1], b[2], &HFF)  
      End if
      Return cc      
   End If
End Property
Property Class_Shape.BorderColorGDIplue(ByVal nColor As ARGB)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr, @nColor )
      Dim c As Long = BGRA(b[2], b[1], b[0], b[3])  'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->ForeColor = c
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Shape.FillColorGDIplue() As ARGB    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->BackColor)
      if cc = 0 then
         cc = GetSysColor(COLOR_BTNFACE)
         Dim b As UByte Ptr = Cast(Any Ptr, @cc ) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0], b[1], b[2], &HFF)  
      End if
      Return cc      
   End If
End Property
Property Class_Shape.FillColorGDIplue(ByVal nColor As ARGB)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr, @nColor )
      Dim c As Long = BGRA(b[2], b[1], b[0], b[3])  'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->BackColor = c      
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Shape.FillStyle() As Long  '样式结构：&H12345678  12主样式 34填充 5宽度   678 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H00FF0000) Shr 16
   End If
End Property
Property Class_Shape.FillStyle(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 9 then sy = 9
      fp->Style = (fp->Style and &HFF00FFFF) Or (sy Shl 16)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Shape.BorderWidth() As Long  '样式结构：&H12345678  12主样式 34填充 5宽度   678 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H0000F000) Shr 12
   End If
End Property
Property Class_Shape.BorderWidth(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 15 then sy = 15
      fp->Style = (fp->Style and &HFFFF0FFF) Or (sy Shl 12)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Sub Class_Shape.Drawing(gg As yGDI ,hWndForm As hWnd ,nBackColor As Long)
   m_hWndParent = hWndForm
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp = 0                  then return
   if (fp->Style and &H2) = 0 Then Return
   Dim As Long nStyles = (fp->Style And &HFF000000) Shr 24 ,nFillStyle = (fp->Style And &H00FF0000) Shr 16 , _
      nBorderWidth = (fp->Style And &H0000F000) Shr 12 ,x ,y ,x2 ,y2 ,w ,h ,m ,cc
   cc = GetCodeColorGDIplue(fp->BackColor)
   If nFillStyle = 0 Then
      gg.GpBrush
   ElseIf nFillStyle = 1 Then
      gg.GpBrush cc
   Else
      Select Case nFillStyle
         Case 0 '0 - 无填充,1 - 纯色,2 渐变.左右,3 渐变.上下,4 渐变.右左,5 渐变.下上,6 渐变.左上斜,7 渐变.左下斜,7 渐变.右上斜,9 渐变.右下斜
         Case 1
         Case 2
            x = fp->nLeft : y = fp->nTop : x2 = fp->nLeft + fp->nWidth : y2 = fp->nTop
         Case 3
            x = fp->nLeft : y = fp->nTop : x2 = fp->nLeft : y2 = fp->nTop + fp->nHeight
         Case 4
            x2 = fp->nLeft : y2 = fp->nTop : x = fp->nLeft + fp->nWidth : y = fp->nTop
         Case 5
            x2 = fp->nLeft : y2 = fp->nTop : x = fp->nLeft : y = fp->nTop + fp->nHeight
         Case 6
            x = fp->nLeft : y = fp->nTop : x2 = fp->nLeft + fp->nWidth : y2 = fp->nTop + fp->nHeight
         Case 7
            x = fp->nLeft : y = fp->nTop + fp->nHeight : x2 = fp->nLeft + fp->nWidth : y2 = fp->nTop
         Case 8
            x2 = fp->nLeft : y2 = fp->nTop + fp->nHeight : x = fp->nLeft + fp->nWidth : y = fp->nTop
         Case 9
            x2 = fp->nLeft : y2 = fp->nTop : x = fp->nLeft + fp->nWidth : y = fp->nTop + fp->nHeight
      End Select
      gg.GpBrushGradient x ,y ,x2 ,y2 ,GetCodeColorGDIplue(fp->ForeColor) ,GetCodeColorGDIplue(fp->BackColor) ,WrapModeTileFlipX
   End If
   gg.GpPen nBorderWidth ,GetCodeColorGDIplue(fp->ForeColor)
   x = fp->nLeft : y = fp->nTop : w = fp->nWidth : h = fp->nHeight : m = 0
   Dim P() As GpPointF
   Select Case nStyles
      Case 0 '- 矩形,
         gg.GpDrawFrame x ,y ,w ,h
      Case 1 '- 圆形,
         gg.GpDrawEllipse x ,y ,w ,h
      Case 2 '- 圆角矩形,
         gg.GpDrawCircleFrame x ,y ,w ,h ,w * 0.5 ,h * 0.5
      Case 3 ' - 正三角形,
         ReDim p(2)
         p(0).x = x + w / 2 : p(0).y = y
         p(1).x = x : p(1).y = y + h
         p(2).x = x + w : p(2).y = y + h
      Case 4 '- 倒三角形,
         ReDim p(2)
         p(0).x = x : p(0).y = y
         p(1).x = x + w : p(1).y = y
         p(2).x = x + w / 2 : p(2).y = y + h
      Case 5 '-左三角形,
         ReDim p(2)
         p(0).x = x : p(0).y = y + h / 2
         p(1).x = x + w : p(1).y = y
         p(2).x = x + w : p(2).y = y + h
      Case 6 '- 右三角形,
         ReDim p(2)
         p(0).x = x : p(0).y = y
         p(1).x = x : p(1).y = y + h
         p(2).x = x + w : p(2).y = y + h / 2
      Case 7 '- 菱形,
         ReDim p(3)
         p(0).x = x + w * 0.5 : p(0).y = y
         p(1).x = x + w : p(1).y = y + h / 2
         p(2).x = x + w / 2 : p(2).y = y + h
         p(3).x = x : p(3).y = y + h / 2
      Case 8 ',8 - 五角形,9 内五角,10 - 六角形,11 - 六角形2,12 - 八角形
         ReDim p(4)
         p(0).x = x + w * 0.5 : p(0).y = y
         p(1).x = x + w : p(1).y = y + h       * 0.38231
         p(2).x = x + w * 0.80946 : p(2).y = y + h
         p(3).x = x + w * 0.19178 : p(3).y = y + h
         p(4).x = x : p(4).y = y + h * 0.38231
      Case 9
         ReDim p(4) : m = 1
         p(0).x = x + w * 0.5     : p(0).y = y
         p(1).x = x + w * 0.80946 : p(1).y = y + h
         p(2).x = x : p(2).y = y + h * 0.38231
         p(3).x = x + w : p(3).y = y + h       * 0.38231
         p(4).x = x + w * 0.19178 : p(4).y = y + h
      Case 10
         ReDim p(5)
         p(0).x = x + w * 0.2427 : p(0).y = y
         p(1).x = x + w * 0.7463 : p(1).y = y
         p(2).x = x + w : p(2).y = y + h      * 0.5
         p(3).x = x + w * 0.7463 : p(3).y = y + h
         p(4).x = x + w * 0.2427 : p(4).y = y + h
         p(5).x = x : p(5).y = y + h * 0.5
      Case 11
         ReDim p(5)
         p(0).x = x + w * 0.5 : p(0).y = y
         p(1).x = x + w : p(1).y = y + h   * 0.2427
         p(2).x = x + w : p(2).y = y + h   * 0.7463
         p(3).x = x + w * 0.5 : p(3).y = y + h
         p(4).x = x : p(4).y = y + h * 0.7463
         p(5).x = x : p(5).y = y + h * 0.2427
   End Select
   If nStyles > 2 Then gg.GpDrawPolygon P() ,m
End Sub




















