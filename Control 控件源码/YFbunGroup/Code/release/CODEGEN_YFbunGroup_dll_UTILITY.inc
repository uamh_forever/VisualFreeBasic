'-----------------------------------------------------------------------------
' 由 VisualFreeBasic 5.8.5 生成的源代码
' 生成时间：2023年01月01日 19时35分38秒
' 更多信息请访问 www.yfvb.com 
'-----------------------------------------------------------------------------

#define HSB(h, s, b, a) Cast(COLORREF,(Cast(ULong, Cast(UByte, (a))) Shl 24) Or ((Cast(ULong, Cast(UShort, (h))) And 511) Shl 14) Or ((Cast(ULong, Cast(UByte, (s))) And 127) Shl 7) Or (Cast(ULong, Cast(UByte, (b))) And 127))

Function gFLY_GetFontHandles(mFont As String) As HFONT '由字体字符，返回字体句柄
   Dim As Long i,u=UBound(gFLY_FontNames),aa =-1
   If u > -1 Then
      For i = 0 To u
         If gFLY_FontNames(i) = mFont Then
            Return gFLY_FontHandles(I)
         End If
      Next
   End If
   aa = u + 1
   ReDim Preserve gFLY_FontNames(aa), gFLY_FontHandles(aa)
   gFLY_FontNames(aa) = mFont
   Dim pvv(5) As String, ff as Long, zz as String = mFont
   for i = 0 to 5
      ff = instr(zz, ",")
      if ff = 0 then
         pvv(i) = Trim(zz)
         exit for
      end if
      pvv(i) = Trim(.left(zz, ff -1))
      zz = Mid(zz, ff + 1)
   next
   if valint(pvv(1)) = 0 then pvv(1) = "9"
   if len(pvv(0)) = 0 then pvv(0) = "SimSun" '宋体
   gFLY_FontHandles(aa) = AfxCreateFont(pvv(0), ValInt(pvv(1)), -1, IIf(ValInt(pvv(2)) = 0, FW_NORMAL, FW_BOLD), ValInt(pvv(3)), ValInt(pvv(4)), ValInt(pvv(5)))

   IF gFLY_FontHandles(aa) = 0 then  '创建失败
      aa -= 1
      ReDim Preserve gFLY_FontNames(aa), gFLY_FontHandles(aa)
      Return 0
   End If
   Function = gFLY_FontHandles(aa)
End Function
Function GetCodeColorGDI(coColor As Long, defaultColor As Long = -1) As Long  '把控件特殊颜色值，转换为 GDI 色  ,返回-1 为不使用或默认
   '格式：&H01020304  02.03.04为7F时，01值0到30是系统色（25为不使用）其它时候是A通道，GDI+需要 02 04 里值对调
  If (&H00FFFFFF And coColor) = &H7F7F7F Then
      Dim f As Long = Cast(UInteger, (&HFF000000 And coColor)) Shr 24
      If f = 25 Then Return defaultColor   '不使用或默认值 
      If f < 31 Then 
          Return GetSysColor(f)  
      End If  
  End If
  Function = (&H00FFFFFF And coColor) '去掉 A 通道
End Function
Function GetCodeColorGDIplue(coColor As Long, defaultColor As Long = 0) As Long  '把控件特殊颜色值，转换为 GDI+ 色  ,返回0 为不使用或默认
 '格式：&H01020304  02.03.04为7F时，01值0到30是系统色（25为不使用）其它时候是A通道，GDI+需要 02 04 里值对调  
  Dim tColor As Long = coColor 
  If (&H00FFFFFF And coColor) = &H7F7F7F Then
      Dim f As Long = Cast(UInteger, (&HFF000000 And coColor)) Shr 24
      If f = 25 Then Return defaultColor  ' 不使用或默认值 
      If f < 31 Then 
          tColor = GetSysColor(f) Or &HFF000000 '增加 A通道，不透明，不然是全透明  
      End If  
  End If 
  '因为保存的是GDI 的颜色，GDI+ 需要调换
  Dim As UInteger c1 =(&H00FF0000 And tColor),c2 = (&H000000FF And tColor) ,c3 =(&HFF00FF00 And tColor)
  c1 Shr= 16
  c2 Shl= 16 
  Function = c1 Or c2 Or c3  
End Function
Function FLY_ResourceToIcon(ByVal ResImg As String) As HICON '资源获取图标句柄
   Dim nIcon As HICON
   Dim ffi   As Long = InStr(ResImg ,"|")
   if ffi > 0 Then ResImg = Mid(ResImg ,ffi + 1)
   if Left(ResImg ,7) = "BITMAP_" Then
      Dim nBmp As HBITMAP = LoadImageA(app.hInstance ,ResImg ,IMAGE_BITMAP ,0 ,0 ,LR_DEFAULTCOLOR)
      Dim po   As ICONINFO
      po.fIcon    = TRUE
      po.hbmColor = nBmp
      po.hbmMask  = nBmp
      nIcon       = CreateIconIndirect(@po)
      DeleteObject nBmp
   Elseif Left(ResImg ,5) = "ICON_" Or ResImg = "AAAAA_APPICON" Then
      nIcon = LoadImageA(app.hInstance ,ResImg ,IMAGE_ICON ,0 ,0 ,LR_DEFAULTCOLOR) '从资源里加载图标
   Else 
      nIcon = AfxGdipIconFromRes(App.hInstance ,ResImg)
   End if
   Function = nIcon
End Function
' =====================================================================================
' 根据Jose Roca的代码
' 为窗口的整个客户区域创建标准工具提示。
' 参数:
' - hwnd = 窗口句柄
' - strTooltipText = 工具提示文本
' - bBalloon = 气球提示 (TRUE or FALSE)
' 返回值:
'   工具提示控件的句柄
' =====================================================================================
Function FF_AddTooltip(hWndForm AS HWND, strTooltipText AS wString, bBalloon AS Long, X as Long = 0, Y As Long = 0, W As Long = 0, H As Long = 0) As HWND
   
   IF hWndForm = 0 Then Exit Function
   
   Dim hwndTT AS HWND
   Dim dwStyle As Long
   
   dwStyle = WS_POPUP OR TTS_NOPREFIX OR TTS_ALWAYSTIP
   IF bBalloon THEN dwStyle = dwStyle OR TTS_BALLOON
   hwndTT = CreateWindowExW(WS_EX_TOPMOST, "tooltips_class32", "", dwStyle, 0, 0, 0, 0, 0, Cast(HMENU, Null), 0, ByVal Cast(LPVOID, Null))
   
   IF hwndTT = 0 THEN Exit Function
   'SetWindowPos(hwndTT, null, 100, 100, 0, 0,  SWP_NOSIZE Or SWP_NOZORDER Or SWP_NOACTIVATE)
   
   Dim tti AS TTTOOLINFOW
   tti.cbSize = SIZEOF(tti)
   tti.uFlags = TTF_SUBCLASS
   tti.hwnd = hWndForm
   tti.hinst = GetModuleHandle(BYVAL NULL)
   
   GetClientRect(hWndForm, Varptr(tti.rect))
   tti.rect.Left = x
   tti.rect.top = y
   if w > 0 then tti.rect.Right = x + w
   if h > 0 then tti.rect.bottom = y + h
   '// 字符串的长度不能超过80个字符，包括终止的空值
   tti.uId = 0
   Dim ff As Long = InStr(strTooltipText, WChr(13, 10) )
   if ff Then
      tti.lpszText =Cast(LPWSTR, Cast(UInteger, @strTooltipText) + ff * 2 + 2) 
      SendMessageW hwndTT, TTM_ADDTOOLW, 0, Cast(LPARAM, Varptr(tti))
      Dim nw As WString * 100 = Left(strTooltipText,ff-1)
      SendMessageW hwndTT, TTM_SETTITLEW, 0, Cast(LPARAM, @nw)
   Else
      tti.lpszText = @strTooltipText
      SendMessageW hwndTT, TTM_ADDTOOLW, 0, Cast(LPARAM, Varptr(tti))
   End if

   
   Function = hwndTT
   
End Function
Function FLY_DoMessagePump(pWindow AS CWindow Ptr ,ByVal ShowModalFlag As Long ,ByVal hWndForm As HWND ,ByVal hWndParent As HWND ,ByVal nFormShowState As Long ,ByVal IsMDIForm As Long) As HWND
   '如果这是一个MDI子窗体，那么它不能显示为模态。
   If (GetWindowLongPtr(hWndForm ,GWL_EXSTYLE) And WS_EX_MDICHILD) = WS_EX_MDICHILD Then ShowModalFlag = False
   If (GetWindowLongPtr(hWndForm ,GWL_EXSTYLE) And WS_EX_NOACTIVATE) = WS_EX_NOACTIVATE And nFormShowState = SW_SHOWNORMAL Then nFormShowState = SW_SHOWNOACTIVATE
   If ShowModalFlag = True Then '模式窗口，进入消息循环处理
      '确定活动控件的顶层窗口
      While (GetWindowLongPtr(hWndParent ,GWL_STYLE) And WS_CHILD) <> 0
         hWndParent = GetParent(hWndParent)
         If IsWindow(hWndParent) = 0 Then Exit While
         If (GetWindowLongPtr(hWndParent ,GWL_EXSTYLE) And WS_EX_MDICHILD) <> 0 Then Exit While
      Wend
      
      '为父窗体禁用鼠标和键盘输入
      If IsWindow(hWndParent) Then EnableWindow(hWndParent ,False)
      ShowWindow(hWndForm ,nFormShowState)
      UpdateWindow(hWndForm)
      '主消息循环：
      Dim uMsg        As MSG
      Dim zTempString As zString * MAX_PATH
      Dim hWndP       As HWND = pWindow->hWindow
      Dim hAccel      AS HACCEL
      Do While GetMessage(@uMsg ,Null ,0 ,0)
         If FF_PUMPHOOK(uMsg) = 0 Then
            hAccel = pWindow->AccelHandle
            '处理菜单命令的快捷键
            If (hAccel = 0) OrElse (TranslateAcceleratorW(hWndP ,hAccel ,@uMsg)) = 0 Then '发生崩溃
               If IsMDIForm = TRUE Then
                  If TranslateMDISysAccel(hWndP ,@uMsg) <> 0 Then Continue Do
               End If
               If IsDialogMessageW(hWndP ,@uMsg) = 0 Then
                  TranslateMessage @uMsg
                  DispatchMessage @uMsg
               End If
            End If
         end if
         
         If IsWindow(hWndForm) = FALSE Then Exit Do ' 如果窗口被销毁，则退出模态消息循环（重要）。
      Loop
      Function        = Cast(hWnd ,Cast(LONG_PTR ,App.ReturnValue))
      App.ReturnValue = 0
   Else
      ShowWindow hWndForm ,nFormShowState
      Function = hWndForm
   End If
End Function

Sub FLY_VFB_Layout_hWndForm(hWndForm As HWND) '处理控件布局
   DIM rcParent AS RECT ,rcChild AS RECT
   DIM x        AS LONG ,y       AS LONG ,xWidth AS LONG ,yHeight AS LONG
   GetClientRect(hWndForm ,@rcParent)
   rcParent.Right  = AfxUnscaleX(rcParent.Right)  ' 为自动响应DPI，全部调整为 100%DPI 时的数值
   rcParent.Bottom = AfxUnscaleY(rcParent.Bottom)
   '真实控件
   Dim zWnd  As HWND = GetWindow(hWndForm ,GW_CHILD)
   Dim nHDWP As HDWP = BeginDeferWindowPos(1) '同时更新控件位置
   While zWnd
      Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(zWnd)
      If fp <> 0 AndAlso fp->anchor > 0 Then
         GetWindowRect(zWnd ,@rcChild)
         FLY_VFB_Layout_Handle(fp ,rcParent.Right ,rcParent.Bottom ,AfxUnscaleX(rcChild.Right - rcChild.Left) ,AfxUnscaleY(rcChild.Bottom - rcChild.Top) ,x ,y ,xWidth ,yHeight)
         fp->nLeft   = x
         fp->nTop    = y
         fp->nWidth  = xWidth
         fp->nHeight = yHeight
         nHDWP       = DeferWindowPos(nHDWP ,zWnd ,0 ,AfxscaleX(x) ,AfxscaleY(y) ,AfxscaleX(xWidth) ,AfxscaleY(yHeight) ,SWP_NOZORDER Or SWP_NOACTIVATE)
      End If
      zWnd = GetWindow(zWnd ,GW_HWNDNEXT)
   Wend
   
   '虚拟控件
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndForm)
   While fp    '
      if fp->anchor > 0 Then
         if fp->CtrlFocus Then '非 CW创建的控件，因为不 fp 指针不内置入窗口，真实控件查不到
            GetWindowRect(fp->CtrlFocus ,@rcChild)
            FLY_VFB_Layout_Handle(fp ,rcParent.Right ,rcParent.Bottom ,AfxUnscaleX(rcChild.Right - rcChild.Left) ,AfxUnscaleY(rcChild.Bottom - rcChild.Top) ,x ,y ,xWidth ,yHeight)
            nHDWP = DeferWindowPos(nHDWP ,fp->CtrlFocus ,0 ,AfxscaleX(x) ,AfxscaleY(y) ,AfxscaleX(xWidth) ,AfxscaleY(yHeight) ,SWP_NOZORDER Or SWP_NOACTIVATE)
         Else
            FLY_VFB_Layout_Handle(fp ,rcParent.Right ,rcParent.Bottom ,fp->nWidth ,fp->nHeight ,x ,y ,xWidth ,yHeight)
         End if
         fp->nLeft   = x
         fp->nTop    = y
         fp->nWidth  = xWidth
         fp->nHeight = yHeight
      End if
      Dim sfp As FormControlsPro_TYPE ptr = fp->VrControls
      fp = sfp
   Wend
   EndDeferWindowPos nHDWP
End Sub

Sub FLY_VFB_Layout_Handle(fp As FormControlsPro_TYPE ptr, pWidth AS LONG, pHeight AS LONG, nWidth AS LONG, nHeight AS LONG, ByRef x AS LONG, ByRef y AS LONG, ByRef xWidth AS LONG, ByRef yHeight AS LONG)
  '处理控件布局 
   x = 0 : y = 0 : xWidth = 0 : yHeight = 0
   SELECT CASE fp->anchor
      CASE 0 'AFX_ANCHOR_NONE
         x = fp->nLeft
         y = fp->nTop
         xWidth = MAX(pWidth - fp->nLeft - fp->nRight, 0)
         yHeight = nHeight
      CASE 1 'AFX_ANCHOR_WIDTH
         x = fp->nLeft
         y = fp->nTop
         xWidth = MAX(pWidth - fp->nLeft - fp->nRight, 0)
         yHeight = nHeight
      CASE 2 'AFX_ANCHOR_RIGHT
         x = pWidth - nWidth - fp->nRight
         y = fp->nTop
         xWidth = nWidth
         yHeight = nHeight
      CASE 3 'AFX_ANCHOR_CENTER_HORZ
         x = (pWidth \ 2) + fp->centerX
         y = fp->nTop
         xWidth = nWidth
         yHeight = nHeight
      CASE 4 'AFX_ANCHOR_HEIGHT
         x = fp->nLeft
         y = fp->nTop
         xWidth = nWidth
         yHeight = MAX(pHeight - fp->nTop - fp->nBottom, 0)
      CASE 5 'AFX_ANCHOR_HEIGHT_WIDTH
         x = fp->nLeft
         y = fp->nTop
         xWidth = MAX(pWidth - fp->nLeft - fp->nRight, 0)
         yHeight = MAX(pHeight - fp->nTop - fp->nBottom, 0)
      CASE 6 'AFX_ANCHOR_HEIGHT_RIGHT
         x = pWidth - nWidth - fp->nRight
         y = fp->nTop
         xWidth = nWidth
         yHeight = MAX(pHeight - fp->nTop - fp->nBottom, 0)
      CASE 7 'AFX_ANCHOR_BOTTOM
         x = fp->nLeft
         y = pHeight - fp->nBottom - nHeight
         xWidth = nWidth
         yHeight = nHeight
      CASE 8 'AFX_ANCHOR_BOTTOM_WIDTH
         x = fp->nLeft
         y = pHeight - fp->nBottom - nHeight
         xWidth = MAX(pWidth - fp->nLeft - fp->nRight, 0)
         yHeight = nHeight
      CASE 9 'AFX_ANCHOR_BOTTOM_RIGHT
         x = pWidth - nWidth - fp->nRight
         y = pHeight - fp->nBottom - nHeight
         xWidth = nWidth
         yHeight = nHeight
      CASE 10 'AFX_ANCHOR_CENTER_HORZ_BOTTOM
         x = (pWidth \ 2) + fp->centerX
         y = pHeight - fp->nBottom - nHeight
         xWidth = nWidth
         yHeight = nHeight
      CASE 11 'AFX_ANCHOR_CENTER_VERT
         x = fp->nLeft
         y = (pHeight - nHeight) \ 2
         xWidth = nWidth
         yHeight = nHeight
      CASE 12 'AFX_ANCHOR_CENTER_VERT_RIGHT
         x = pWidth - nWidth - fp->nRight
         y = (pHeight - nHeight) \ 2
         xWidth = nWidth
         yHeight = nHeight
      CASE 13 'AFX_ANCHOR_CENTER
         x = (pWidth \ 2) + fp->centerX
         y = (pHeight \ 2) + fp->centerY
         xWidth = nWidth
         yHeight = nHeight
   END SELECT
End Sub





'[SourceDB:0-171133]
'兼容VB，返回 是数组的个数，一维数组下标从零开始，它包含指定数目的子字符串。
'vbSplit( SourceStr as String,delimeter as String, StrArray() as String) as Long
Function vbSplit(SourceStr as String, delimeter as String, StrArray() as String) as Long
   Dim as Integer f1,f2,u,umax,ld
   If SourceStr = "" Then
      Function = -1
      Exit Function
   End If
   ld = Len(delimeter)
   f1 = 1 - ld
   u = -1
   umax = 100  '加速数组操作
   ReDim StrArray(umax)
   Do
      f2 = InStr(f1 + ld, SourceStr, delimeter)
      u += 1
      If u > umax Then
         umax += 100
         ReDim Preserve StrArray(umax)
      End If
      If f2 = 0 Then '没有分割符合了
         StrArray(u) = Mid(SourceStr, f1 + ld)
         Exit Do
      End If
      StrArray(u) = Mid(SourceStr, f1 + ld, f2 - f1 - ld)
      f1 = f2
   Loop
   ReDim Preserve StrArray(u)
   
   Function = u + 1
End Function

'[SourceDB:0-171144]
'Utf8 转换成 String
Function Utf8toStr(Utf8str as String) as String
   Dim eLen As Integer = Len(Utf8str)
   If eLen = 0 Then Return ""
   Dim wsStr() As UByte  ' = CAllocate(eLen * 2 + 2)  '+2 是为了最后空出2个00 为宽字符结尾
   ReDim wsStr(eLen * 2 + 2) 
   UTFToWChar(UTF_ENCOD_UTF8, StrPtr(Utf8str), Cast(WString Ptr,@wsStr(0)), @eLen)
   Dim ZStr as String
   ZStr = String(eLen * 2 + 2, 0)
   Dim ZStrLen as Integer = WideCharToMultiByte(ansiStr_CodePage, 0, Cast(WString Ptr,@wsStr(0)), eLen, StrPtr(ZStr), eLen * 2 + 2, Null, Null)
   If ZStrLen Then Function = Left(ZStr, ZStrLen)
'   Deallocate wsStr

End Function
'[SourceDB:0-171141]
'普通字符 换成 UTF8字符
'返回包含UTF8字符格式的普通字符串（String 可以包含任何数据）
Function StrToUtf8(sStr as String) as String
   Dim LenStr As Integer = Len(sStr)
   If LenStr = 0 Then Return ""
   Dim bLen As Integer = LenStr * 2
   '转换为宽字符，这是必须的，大家都这么做，直接转换就不一样了
   Dim wsStr As String  = String(bLen + 2 ,0)
   Dim eLen  As Integer = MultiByteToWideChar(ansiStr_CodePage ,0 ,StrPtr(sStr) ,LenStr ,Cast(WString Ptr ,StrPtr(wsStr)) ,bLen)
   '由宽字符转换为 UTF8
   Dim dst     As String  = String(bLen ,0)      '为输出Utf8预留空间
   Dim UTF8Len as Integer = WideCharToMultiByte(CP_UTF8 ,0 ,Cast(WString Ptr ,StrPtr(wsStr)) ,eLen ,StrPtr(dst) ,bLen ,Null ,Null)
   If UTF8Len Then Function = Left(dst ,UTF8Len)
End Function






'[SourceDB:0-171263]
'存储到 Windows 剪贴板中的文本字符串。
'Dim sText As String = "This is my text to copy"
'FF_ClipboardSetText(sText)
Function FF_ClipboardSetText(ByRef x As String) As Integer
   Function = False
   Dim As HANDLE hText = Null
   Dim As UByte Ptr clipmem = Null
   Dim As Integer n = Len(x)
   If n > 0 Then
      hText = GlobalAlloc(GMEM_MOVEABLE Or GMEM_DDESHARE, n + 1)
      Sleep 15
      If (hText) Then
         clipmem = GlobalLock(hText)
         If clipmem Then
            CopyMemory(clipmem, Strptr(x), n)
         Else
            hText = Null
         End If
         If GlobalUnlock(hText) Then
            hText = Null
         End If
      End If
      If (hText) Then
         If OpenClipboard(Null) Then
            Sleep 15
            If EmptyClipboard() Then
               Sleep 15
               If SetClipboardData(CF_TEXT, hText) Then
                  Sleep 15
                  Function = True
               End If
            End If
            CloseClipboard()
         End If
      End If
   End If
End Function

'[SourceDB:0-171140]
'z字符转换为w字符  
Function StringToCWSTR(cc As String) As CWSTR   'z字符转换为w字符  
   Dim ww As String = StrToWStr(cc) & Chr(0,0)
   Return *CPtr(WString Ptr,StrPtr(ww))
End Function
'[SourceDB:0-171139]
'W字符转换为UTF8编码的Z字符  
Function CWSTRtoUTF8(cc As CWSTR  ) As String   
   Function = cc.UTF8  
End Function
'[SourceDB:0-171125]
' Dim Shared vfb_LangStringOld() As CWSTR , vfb_LangStringNew() As CWSTR   '已经搬到定义文件处
Function vfb_LoadLanguage(langFile As CWSTR ,nodeName As CWSTR = "") As Long '加载语言文件，成功返回 True 失败 False
   'nodeName  节点名称，作用于多EXE或DLL使用同个语言文件，可以节点区分读取，为空时，遇到到的第一个读取。
   Dim txt As String = GetFileStr(langFile)
   If Len(txt) = 0 Then Return False
   
   '处理速度 A文本总是比W文本快，转为 UTF8处理，来支持大字符集（火星文）
   If Len(txt) > 2 AndAlso txt[0] = &HEF AndAlso txt[1] = &HBB AndAlso txt[2] = &HBF Then
      txt = Mid(txt ,4)
   ElseIf Len(txt) > 1 AndAlso txt[0] = &HFF AndAlso txt[1] = &HFE Then
      txt = wStrToUtf8(Cast(Any Ptr ,Cast(UInteger ,StrPtr(txt)) + 2) ,Len(txt) / 2 -1)
   Else
      txt = StrToUtf8(txt)
   End If
   Dim i       As Long   ,f           As Long   ,uu    As Long ,ui As Long ,vi As Long
   Dim wlist() As String ,yTxt        As String ,oText As String
   Dim node    As String ,nodeNameUtf As String = UCase(Trim(nodeName.utf8)) ,nodeK As Long
   Dim u       As Long = vbSplit(txt ,vbCrLf ,wlist())
   'Node=  TotalText= txt_ out_ 未做容错处理，容错严重影响执行效率。除非用户乱改，都是软件对软件永不出错。
   For i = 0 To u -1
      If Left(wlist(i) ,5) = "Node=" Then ' ====== 读取节点
         f = InStr(wlist(i) ,"'")
         If f = 0 Then InStr(wlist(i) ," ")
         If f     Then node = Mid(wlist(i) ,6 ,f -6) Else node = Mid(wlist(i) ,6)
         node = UCase(Trim(node))
         If Len(nodeNameUtf) = 0 Then nodeNameUtf = node
         nodeK = nodeNameUtf = node
      Else
         If nodeK Then ' 自己节点才处理
            If uu = 0 Then
               If Left(wlist(i) ,10) = "TotalText=" Then ' 预先获得数量来声明数组，这样执行效率高。
                  f = InStr(wlist(i) ,"'")
                  If f Then wlist(i) = Mid(wlist(i) ,11 ,f -11) Else wlist(i) = Mid(wlist(i) ,11)
                  uu = ValInt(wlist(i))
                  ReDim vfb_LangStringOld(uu) ,vfb_LangStringNew(uu)
               End If
            Else
               Select Case Left(wlist(i) ,4)
                  Case "txt_"
                     f = InStr(wlist(i) ,"=")
                     If f Then
                        yTxt = Mid(wlist(i) ,f + 1) '先获取原文本，遇到没译文时就使用原文，这里不判断索引号，都是软件产生，永不出错，要遇到人为故意乱改就出错。
                     Else
                        yTxt = Mid(wlist(i) ,5) '这里永不会发生，除非人为乱改，属于容错处理
                     End If
                  Case "out_"
                     f = InStr(wlist(i) ,"=")
                     If f Then
                        oText = Mid(wlist(i) ,f + 1)
                        ui    = ValInt(Mid(wlist(i) ,5 ,f -5))
                     Else
                        ui    = 0 '这里永不会发生，除非人为乱改，属于容错处理
                        oText = Mid(wlist(i) ,5)
                     End If
                     If ui > uu Then
                        uu += 10 '容错处理
                        ReDim Preserve vfb_LangStringOld(uu) ,vfb_LangStringNew(uu)
                     End If
                     vfb_LangStringOld(ui) = UTF8toCWSTR(yTxt)
                     if len(oText ) then vfb_LangStringNew(ui) = UTF8toCWSTR(oText )
                     vi                    += 1
               End Select
            End If
         End If
      End If
   Next
   If UBound(vfb_LangStringOld) > ui Then ReDim Preserve vfb_LangStringOld(ui),vfb_LangStringNew(ui)
   Return vi > 0
End Function

Function vfb_LangString(nn As Wstring Ptr) As CWSTR
   Dim i As Long ,u As Long = UBound(vfb_LangStringOld)
   If u > 0 Then
      For i = 1 To u
         If *nn = vfb_LangStringOld(i) Then
            If Len(vfb_LangStringNew(i)) = 0 Then Return *nn
            Return vfb_LangStringNew(i)
         End If
      Next
   End If
   Return *nn
End Function
'[SourceDB:0-171135]
'替换
Function YF_Replace(Expression As String, Find As String, Replacewith As String) As String
   Dim As  Long  FF(),ff1,i,u,Lf,Le,Lr,Lu,t
   Dim outstr As String
   Dim As UByte Ptr pE,pF
   Dim As Any Ptr pU,pR
   Le = Len(Expression)
   Lf = Len(Find)
   Lr = Len(Replacewith)
   If Le = 0 Or Lf = 0 Then
      Return Expression
   Else
      '先找出替换数与位置----------------------------------
      u = -1
      ReDim ff(100)
      pE = StrPtr(Expression)
      pR = StrPtr(Replacewith)
      pF = StrPtr(Find)
      For i = 0 To Le - Lf
         If pe[i] = pf[0] Then
            If MemCmp(@pe[i], pf, lf) = 0 Then
               u += 1
               If u > UBound(ff) Then ReDim Preserve ff(u + 100)
               ff(u) = i
               i += lf -1
            End If
         End If
      Next
      If u = -1 Then
         Return Expression
      Else
         Lu = Le + (Lr - Lf) * (u + 1)  'Lu=Le - Lf * (u+1) + Lr * (u+1)
         outstr = String(Lu, 0)
         pU = StrPtr(outstr)
         t = 0
         For i = 0 To u
            Lu = FF(i) - t
            If Lu > 0 Then '复制原字符
               MemCpy(pU, pE, Lu)
               pU += Lu
            End If
            If Lr > 0 Then '复制替换字符
               MemCpy(pU, pR, Lr)
               pU += Lr
            End If
            pE += Lu + Lf
            t += Lu + Lf
         Next
         If t < le Then
            lu = le - t
            MemCpy(pU, pE, Lu)
         End If
         Return outstr
      End If
   End If
   
   
End Function

'[SourceDB:0-171007]
'location position
'   以像素为单位设置控件的左上角的位置。该位置是相对于父窗口客户区的左上角。
'   hWndControl: 窗体或控件的句柄 (例如  HWND_FORM1_COMMAND1)
'   nLeft:       Sets the left position of the window
'   nTop:        Sets the top position of the window
'   Returns:     0 if error, non-zero if successful
Function FF_Control_SetLoc( ByVal hWndControl as HWnd, _
                            ByVal nLeft as Long, _
                            ByVal nTop as Long _
                            ) as Integer
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       
       ' Set the positioning of the window
         Function = SetWindowPos( hWndControl, 0, nLeft, nTop, 0, 0,SWP_NOZORDER Or SWP_NOSIZE  Or SWP_NOACTIVATE)
         
    End If
       

End Function


'[SourceDB:0-171010]
'size
'   设置指定的控件或窗体的大小 （以像素为单位）。
'   hWndControl: 窗体或控件的句柄 (例如  HWND_FORM1_COMMAND1)
'   nWidth:      The new width of the window in pixels.
'   nHeight:     The new height of the window in pixels.
'   Returns:     0 if error, non-zero if successful
Function FF_Control_SetSize( ByVal hWndControl as HWnd, _
                             ByVal nWidth as Long, _
                             ByVal nHeight as Long _
                             ) as Long
    
    Dim rc as Rect
    
    ' This function works for both Forms and Controls.
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then
       
       ' Set the dimensions of the window
         Function = SetWindowPos( hWndControl, 0, 0, 0, nWidth, nHeight, _
                      SWP_NOZORDER Or SWP_NOMOVE or SWP_NOACTIVATE)
       
    End If
    
End Function


'[SourceDB:0-171147]
'宽字符 换成 UTF8字符
'宽字符是指针，返回包含UTF8字符格式的普通字符串
'WStrLen 是宽字符个数，不是字节长度
Function wStrToUtf8(WStrPtr as WString Ptr, WStrLen as UInteger=0) as  String
'WStrLen 是宽字符个数，不是字节长度
    Dim UTF8Mem as  ZString Ptr
    Dim ss as String  
    if WStrLen=0 then WStrLen=Len(*WStrPtr)
    If WStrPtr>0 And WStrLen>0 Then
        ss=String(WStrLen*3,0)
        UTF8Mem=StrPtr(ss)    
        Dim UTF8Len as Integer =WideCharToMultiByte(CP_UTF8, 0, WStrPtr, WStrLen, UTF8Mem, Len(ss), Null, Null)
        If UTF8Len Then Function=Left(ss,UTF8Len)
    EndIf
End Function
'[SourceDB:0-171244]
'在对话框中显示消息(兼容VB6)，等待用户单击按钮，并返回一个 Integer 告诉用户单击哪一个按钮。（选项太多，建议用【工具】菜单里【对话框编辑器】自动产生代码）
'按钮 MB_OK MB_OKCANCEL MB_YESNOCANCEL MB_YESNO MB_RETRYCANCEL MB_ABORTRETRYIGNORE MB_CANCELTRYCONTINUE
'图标 MB_ICONERROR MB_ICONQUESTION MB_ICONWARNING MB_ICONINFORMATION
'默认按钮 MB_DEFBUTTON1 MB_DEFBUTTON2 MB_DEFBUTTON3 MB_DEFBUTTON4
'模态对话框 MB_APPLMODAL MB_SYSTEMMODAL MB_TASKMODAL  
'帮助 MB_HELP
'返回 IDOK IDCANCEL IDYES IDNO IDCANCEL IDRETRY IDCANCEL IDABORT IDRETRY IDIGNORE IDTRYAGAIN IDCONTINUE
Function MsgBox Overload(CH_E68F90E7A4BAE69687E69CAC_ as CWSTR, CH_E98089E9A1B9_ As ULong = MB_OK, CH_E6A087E9A298E69687E69CAC_ As CWSTR = App.ProductName , CH_E788B6E7AA97E58FA3E58FA5E69F84_ As HWND = AfxGetHwndFromPID(GetCurrentProcessId)) as Integer
   'VB兼容模式
'   PrintA "VB兼容模式"
   Function = MessageBox(CH_E788B6E7AA97E58FA3E58FA5E69F84_, CH_E68F90E7A4BAE69687E69CAC_, CH_E6A087E9A298E69687E69CAC_, CH_E98089E9A1B9_)
End Function
Function MsgBox Overload(CH_E788B6E7AA97E58FA3E58FA5E69F84_ As HWND, CH_E68F90E7A4BAE69687E69CAC_ as CWSTR, CH_E6A087E9A298E69687E69CAC_ As CWSTR = App.ProductName , CH_E98089E9A1B9_ As ULong = MB_OK) as Integer
   'API 模式
'   PrintA "API 模式"
   Function = MessageBox(CH_E788B6E7AA97E58FA3E58FA5E69F84_, CH_E68F90E7A4BAE69687E69CAC_, CH_E6A087E9A298E69687E69CAC_, CH_E98089E9A1B9_)
End Function
Function MsgBox Overload(CH_E788B6E7AA97E58FA3E58FA5E69F84_ As HWND, CH_E68F90E7A4BAE69687E69CAC_ as CWSTR, CH_E98089E9A1B9_ As ULong ) as Integer
   'API 模式 ，标题默认
   Dim CH_E6A087E9A298E69687E69CAC_ As CWSTR = App.ProductName 
'   PrintA "API 模式 ，标题默认"
   Function = MessageBox(CH_E788B6E7AA97E58FA3E58FA5E69F84_, CH_E68F90E7A4BAE69687E69CAC_,CH_E6A087E9A298E69687E69CAC_, CH_E98089E9A1B9_)
End Function
Function MsgBox Overload( CH_E68F90E7A4BAE69687E69CAC_ as String, CH_E6A087E9A298E69687E69CAC_ As String,ByVal CH_E98089E9A1B9_ As Integer=0,ByVal  CH_E788B6E7AA97E58FA3E58FA5E69F84_ as Hwnd = 0 ) as Integer
   'WIN9库 模式 ，标题默认
   '   PrintA "WIN9库 模式"
   Function = MessageBox(CH_E788B6E7AA97E58FA3E58FA5E69F84_, StringToCWSTR(CH_E68F90E7A4BAE69687E69CAC_),StringToCWSTR(CH_E6A087E9A298E69687E69CAC_), CH_E98089E9A1B9_)
End Function


'[SourceDB:0-171143]
'UTF8编码的Z字符转换为W字符  
Function UTF8toCWSTR(UTF8 As String ) As CWSTR  
   Dim c As CWSTR 
   c.UTF8 =UTF8
   Return c 
End Function
'[SourceDB:0-171217]
'保存到文件,by是指任意数据 ，  成功返回0 ，失败返回非0

Function SaveFileStr(ByVal szFileName as CWSTR, by as String) as Integer
'   Dim as Integer  hFile,U
'   Dim szBuffer as String
'   hFile = Freefile
'   If AfxFileExists(szFileName) Then AfxDeleteFile szFileName
'   Open szFileName For Binary as #hFile
'   Function= Put( #hFile, 1, by )
'   Close #hFile
   Dim hFile As HANDLE = CreateFileW(szFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)
   if hFile = INVALID_HANDLE_VALUE Then Return 1
   dim bErrorFlag As WINBOOL, dwBytesWritten As uLong
   bErrorFlag = WriteFile(hFile ,StrPtr(by) ,Len(by) ,@dwBytesWritten ,NULL)
   CloseHandle(hFile)
   if bErrorFlag = FALSE Then Return 2
   
   Function =0 
End Function


'[SourceDB:0-171145]
 Function Utf8toWStr(ByRef Ansistr As Const String) As String
   Dim bb As String  
   Dim Ll As Long = Len(Ansistr) * 4
   Dim Dwlen As Long
   bb = String(Ll, 0)  
   Dwlen = MultiByteToWideChar(Cp_utf8, 0, StrPtr(Ansistr), Len(Ansistr), Cast(LPWSTR, StrPtr(bb)), Ll)
     'Dwlen 是宽字符个数，不是字节数
    If Dwlen Then 
        Function = Left(bb, Dwlen * 2) 
    End If

End Function
'[SourceDB:0-171094]
'''
'''  FF_JOIN
'''  返回一个包含所有数组中的字符串，每一个分隔符分隔。
'''
'''  If the delimiter is a null (zero-length) string then no separators are inserted 
'''  between the string sections. If the delimiter expression is the 3-byte value 
'''  of "," which may be expressed in your source code as the string literal ""","""
'''  or as Chr(34,44,34) then a leading and trailing double-quote is added to each 
'''  string section. This ensures that the returned string contains standard 
'''  comma-delimited quoted fields that can be easily parsed.
'''
'''  Function FF_Join( sArray() as String, _
'''                    ByRef sDelimiter as String _
'''                    ) as String        


''
''  FF_JOIN
''  Return a string consisting of all of the strings in an array, each 
''  separated by a delimiter.
''
''  If the delimiter is a null (zero-length) string then no separators are inserted 
''  between the string sections. If the delimiter expression is the 3-byte value 
''  of "," which may be expressed in your source code as the string literal ""","""
''  or as Chr(34,44,34) then a leading and trailing double-quote is added to each 
''  string section. This ensures that the returned string contains standard 
''  comma-delimited quoted fields that can be easily parsed.
''        

Function FF_Join( sArray() As String,  ByRef sDelimiter As String ) As String

   Dim s    As String  = ""     
   Dim lb   As Integer = LBound(sArray)
   Dim ub   As Integer = Ubound(sArray)
   Dim nLen As Integer = 0 
   Dim i    As Integer = 0
   Dim nPos As Integer = 0
   
   ' Determine the total size of the resulting string
   For i = lb To ub
      nLen = nLen + Len(sArray(i))
   Next
   
   ' Add room for the delimiters
   nLen = nLen + ( (ub-lb) * Len(sDelimiter) )
   If nLen = 0 Then Return ""
   
   ' Build the buffer to hold the resulting string
   s = Space(nLen)
   
   ' Copy the strings into the buffer
   Dim nLenMatch As Integer = Len(sDelimiter)
   nPos = 0
   For i = lb To ub
      nLen = Len(sArray(i))
      MemMove( Strptr(s)+nPos, Strptr(sArray(i)), nLen )
      nPos = nPos + nLen
      If i <> ub Then   ' add a delimiter 
         MemMove( Strptr(s)+nPos, Strptr(sDelimiter), nLenMatch )
         nPos = nPos + nLenMatch
      End If   
   Next

   ' If the special delimiter of DQ & COMMA & DQ was used then we
   ' also need to wrap the start and end of the string in DQ
   If sDelimiter = Chr(34,44,34) Then
      s = Chr(34) & s & Chr(34)
   End If
      
   Return s
    
End Function

'[SourceDB:0-171072]
'画线 DrawLine(ByVal hDc As Long , ByVal X1 As Long ,ByVal Y1 As Long ,ByVal X2 As Long ,ByVal Y2 As Long ,_
'             ByVal nPenStyle As Long, ByVal nWidth As Long, ByVal crColor As Long)
'需要支持高DPI，请代码中加 AfxScaleX(X), AfxScaleY(Y)  
'nPenStyle ------ Long，指定画笔样式，可以是下述常数之一
'PS_SOLID        画笔画出的是实线
'PS_DASH         画笔画出的是虚线（nWidth必须不大于1）
'PS_DOT          画笔画出的是点线（nWidth必须不大于1）
'PS_DASHDOT      画笔画出的是点划线（nWidth必须不大于1）
'PS_DASHDOTDOT   画笔画出的是点-点-划线（nWidth必须不大于1）
'PS_NULL         画笔不能画图
'PS_INSIDEFRAME  由椭圆、矩形、圆角矩形、饼图以及弦等生成的封闭对象框时，画线宽度向内扩展。如指定的准确RGB颜色不存在，就进行抖动处理
'颜色用 BGR(0, 0, 255) 创建  不能用 RGB
Sub DrawLine(ByVal hDC as hDC, ByVal X1 as Integer, ByVal Y1 as Integer, ByVal X2 as Integer, ByVal Y2 as Integer, _
      ByVal nPenStyle as Integer, ByVal nWidth as Integer, ByVal crColor as Long) '画线
   '线条类型 nPenStyle
   '实线 = 0
   '虚线 = 1
   '点线 = 2
   '点划线 = 3
   '双点划线 = 4
   '无线 = 5
   '内收实线 = 6
   Dim hOldPen as HGDIOBJ, hPen as HGDIOBJ, lpPoint as Point
   hPen = CreatePen(nPenStyle, nWidth, crColor) '创建新画笔
   hOldPen = SelectObject(hDC, hPen)            '画笔存入DC
   MoveToEx hDC, X1, Y1, @lpPoint             '设置起点
   LineTo hDC, X2, Y2                      '画到终点
   SelectObject hDC, hOldPen            '销毁
   DeleteObject hPen
   
End Sub
'[SourceDB:0-171071]
'画框 DrawFrame(DC,X1,Y1,X2,Y2,填充色彩,线条类型,线条色彩,线条宽)
'需要支持高DPI，请代码中加 AfxScaleX(X), AfxScaleY(Y)  
'nPenStyle ------ Long，指定画笔样式，可以是下述常数之一
'PS_SOLID        画笔画出的是实线
'PS_DASH         画笔画出的是虚线（nWidth必须不大于1）
'PS_DOT          画笔画出的是点线（nWidth必须不大于1）
'PS_DASHDOT      画笔画出的是点划线（nWidth必须不大于1）
'PS_DASHDOTDOT   画笔画出的是点-点-划线（nWidth必须不大于1）
'PS_NULL         画笔不能画图
'PS_INSIDEFRAME  由椭圆、矩形、圆角矩形、饼图以及弦等生成的封闭对象框时，画线宽度向内扩展。如指定的准确RGB颜色不存在，就进行抖动处理
'颜色用 BGR(0, 0, 255) 创建  不能用 RGB
Sub DrawFrame(DC as hDC, X1 as Long, Y1 as Long, X2 as Long, Y2 as Long, C as Long, L as Long = PS_NULL, cL as Long = 0, wL as Long = 1)
   '画框                                                                   填充色彩  线条类型     线条色彩
   
   Dim hBrush as HBRUSH, hPen as HPEN
   Dim hOldBrush as HGDIOBJ, hOldPen as HGDIOBJ
   
   If c <> -1 Then
      hBrush = CreateSolidBrush(c)
   Else
      hBrush = GetStockObject(HOLLOW_BRUSH) '创建空画笔，才能画出空心来
   End If
   hOldBrush = SelectObject(DC, hBrush)
   
   hPen = CreatePen(L, wl, cl)
   
   hOldPen = SelectObject(DC, hPen)
   
   Rectangle DC, X1, Y1, X2, Y2
   
   SelectObject DC, hOldPen
   
   DeleteObject hPen
   
   SelectObject DC, hOldBrush
   DeleteObject hBrush
   
   
End Sub
'[SourceDB:0-171069]
'圆角矩形 
' DrawCircleFrame(DC,X,Y,W,H,x圆角,y圆角,框线粗细,线条类型 ,边框色,填充色)
'需要支持高DPI，请代码中加 AfxScaleX(X), AfxScaleY(Y)  
'nPenStyle ------ Long，指定画笔样式，可以是下述常数之一
'PS_SOLID        画笔画出的是实线
'PS_DASH         画笔画出的是虚线（nWidth必须不大于1）
'PS_DOT          画笔画出的是点线（nWidth必须不大于1）
'PS_DASHDOT      画笔画出的是点划线（nWidth必须不大于1）
'PS_DASHDOTDOT   画笔画出的是点-点-划线（nWidth必须不大于1）
'PS_NULL         画笔不能画图
'PS_INSIDEFRAME  由椭圆、矩形、圆角矩形、饼图以及弦等生成的封闭对象框时，画线宽度向内扩展。如指定的准确RGB颜色不存在，就进行抖动处理
'颜色用 BGR(0, 0, 255) 创建  不能用 RGB
Sub DrawCircleFrame(ByVal sDC as hDC, ByVal X as Long, ByVal Y as Long, ByVal hWidth as Long, ByVal hHeight as Long, ByVal xCircle as Long, ByVal yCircle as Long, _
      ByVal LineWidth as Long, ByVal LineType as Long , ByVal bColor as Long, ByVal tColor as Long)'圆角矩形
   
   '(ByVal sDC As Long, ByVal X As Long, ByVal Y As Long, ByVal 宽度 As Long, ByVal 高度 As Long, X圆角 As Long, Y圆角 As Long, _
   'ByVal 框线粗细 As Long, ByVal 线条类型 As 线条类型_m, ByVal 边框色 As Long, ByVal 填充色 As Long) '圆角矩形
   '以支持高DPI，以DPI100%设置坐标就可以了
   
   Dim hBrush as HBRUSH, hPen as HPEN
   Dim hOldBrush as HGDIOBJ, hOldPen as HGDIOBJ
   
   If tColor <> -1 Then
      hBrush = CreateSolidBrush(tColor)
   Else
      hBrush = GetStockObject(HOLLOW_BRUSH) '创建空画笔，才能画出空心来
   End If
   hOldBrush = SelectObject(sDC, hBrush)
   
   hPen = CreatePen(LineType, LineWidth, bColor)
   
   hOldPen = SelectObject(sDC, hPen)
   
   RoundRect sDC, X, Y, X + hWidth, Y + hHeight, xCircle, yCircle
   
   SelectObject sDC, hOldPen
   
   DeleteObject hPen
   
   SelectObject sDC, hOldBrush
   DeleteObject hBrush
   
   '参数表
   '参数 类型及说明
   'sDC Long，用于绘图的设备场景
   'X1,Y1 Long，对矩形左上角位置进行说明的X，Y坐标
   'X2,Y2 Long，对矩形右下角位置进行说明的X，Y坐标
   'X3 Long，用于生成圆角效果的一个椭圆的宽度。取值范围从零（表示不加圆角），一直到矩形的宽度（全圆）
   'Y3 Long，用于生成圆角效果的一个椭圆的高度。取值范围从零（表示不加圆角），一直到矩形的高度（全圆）
   
   
End Sub
'[SourceDB:0-171070]
'描绘一个椭圆 
'DrawEllipse(Dc,X,Y,宽度 ,高度,线条类型,框线粗细,边框色,填充色)
'需要支持高DPI，请代码中加 AfxScaleX(X), AfxScaleY(Y)  
'nPenStyle ------ Long，指定画笔样式，可以是下述常数之一
'PS_SOLID        画笔画出的是实线
'PS_DASH         画笔画出的是虚线（nWidth必须不大于1）
'PS_DOT          画笔画出的是点线（nWidth必须不大于1）
'PS_DASHDOT      画笔画出的是点划线（nWidth必须不大于1）
'PS_DASHDOTDOT   画笔画出的是点-点-划线（nWidth必须不大于1）
'PS_NULL         画笔不能画图
'PS_INSIDEFRAME  由椭圆、矩形、圆角矩形、饼图以及弦等生成的封闭对象框时，画线宽度向内扩展。如指定的准确RGB颜色不存在，就进行抖动处理
'颜色用 BGR(0, 0, 255) 创建  不能用 RGB
Sub DrawEllipse(ByVal hDC as hDC, ByVal X as Integer, ByVal Y as Integer, ByVal hWidth as Integer, ByVal hHeight as Integer, _
      ByVal LineType as Integer ,ByVal LineWidth as Integer, ByVal bColor as  Integer , ByVal tColor as  Integer )'描绘一个椭圆
   
   '(ByVal hDc As Long, ByVal X As Long, ByVal Y As Long, ByVal 宽度 As Long, ByVal 高度 As Long, _
   'ByVal 框线粗细 As Long, ByVal 线条类型 As 线条类型_m, ByVal 边框色 As Long, ByVal 填充色 As Long) '圆角矩形
   
   
   Dim hBrush as HGDIOBJ, hPen as HGDIOBJ
   Dim hOldBrush as HGDIOBJ, hOldPen as HGDIOBJ
   
   If tColor <> -1 Then
      hBrush = CreateSolidBrush(tColor)
   Else
      hBrush = GetStockObject(HOLLOW_BRUSH)
   End If
   hOldBrush = SelectObject(hDC, hBrush)
   
   
   'SetBkMode hdc,TRANSPARENT
   '设置这个后，画上的字是透明的
   
   hPen = CreatePen(LineType, LineWidth, bColor)
   
   hOldPen = SelectObject(hDC, hPen)
   
   Ellipse hDC, X, Y, X + hWidth, Y + hHeight
   
   SelectObject hDC, hOldPen
   
   DeleteObject hPen
   
   SelectObject hDC, hOldBrush
   DeleteObject hBrush
   
   '参数表
   
   'X1，Y1 Long，约束矩形采用逻辑坐标的左上角位置
   'X2，Y2 Long，约束矩形采用逻辑坐标的右下角位置
   
   
End Sub

'[SourceDB:0-171078]
'获取文本显示宽度
Function GetTextWidth(ByVal DC as hDC, nText as String) as Long '获取文本显示宽度
   Dim s as SIZE
   GetTextExtentPoint32A DC, StrPtr(nText), Len(nText), @s
   Function = s.cx
End Function
'[SourceDB:0-171076]
'获取文本显示高度
Function GetTextHeight(ByVal DC as hDC, nText as String) as Long '获取文本显示高度
   Dim s as SIZE
   GetTextExtentPoint32A DC, StrPtr(nText), Len(nText), @s
   Function = s.cy
End Function
'[SourceDB:0-171142]
'普通字符 换成 宽字符 
'返回包含宽字符格式的普通字符串，使用还是要转换的
Function StrToWStr(sStr as  String ) as String
    Dim  as UInteger ZStrLen,WStrLen 
    Dim ss as String ,ff as Long
    Dim ZStrPtr as LPCCH  ,WStrMem as LPWSTR
    ZStrLen=Len(sStr)
    If ZStrLen>0 Then
        ZStrPtr= StrPtr(sStr) 
        WStrLen=ZStrLen*2
        ss=String(WStrLen,0)
        WStrMem= Cast( LPWSTR, StrPtr(ss)  )
        WStrLen =MultiByteToWideChar(ansiStr_CodePage, 0, ZStrPtr, ZStrLen, WStrMem, WStrLen)
        'WStrLen 是宽字符个数，不是字节数
        Function=Left(ss,WStrLen*2)
    End If   
End Function
'[SourceDB:0-171200]
Function GetFileStr(szFileName as CWSTR) as String '读取文件全部内容到字符串，返回的String应该算数据，而不一定就是A字符。
'   Dim h as Integer, txt as String
'   h = Freefile
'   Open szFileName For Binary as #h
'   If LOF(h) > 0 Then
'      txt = String(LOF(h), 0)
'      If Get(#h, 1, txt) <> 0 Then txt = ""
'   End If
'   
'   Close h
'   Function = txt
   Dim hFile As HANDLE = CreateFileW(szFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)
   Dim r As Long 
   if hFile = INVALID_HANDLE_VALUE Then Return ""
   dim bErrorFlag As WINBOOL, nFileSize As ULong  , txt As String
   if r = 0 Then 
      if GetFileSizeEx(hFile,Cast(Any Ptr,@nFileSize)) = 0 Then r = 1
   End if 
   if r=0 And nFileSize>0 Then 
      txt =String(nFileSize,0) 
      bErrorFlag = ReadFile(hFile, StrPtr(txt), Len(txt), @nFileSize, NULL)
      if bErrorFlag = FALSE Then txt = "" 
   End if 
   CloseHandle(hFile)   
   Function = txt
End Function
