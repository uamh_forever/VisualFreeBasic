﻿#VisualFreeBasic_Form#  Version=5.8.0
Locked=0

[Form]
Name=ToolEdit
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_BORDER,WS_CAPTION,WS_SYSMENU,WS_EX_TOOLWINDOW,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP
Style=4 - 工具窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=工具栏编辑器
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=707
Height=555
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option4
Index=0
Style=0 - 标准
Caption=默认
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=OptionGroup4
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=437
Top=383
Width=62
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text5
Index=0
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=-1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=442
Top=274
Width=73
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Frame]
Name=Frame1
Index=-1
Caption=按钮独立设置，-1 表示使用控件设定的默认值
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=379
Top=254
Width=309
Height=149
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option3
Index=0
Style=0 - 标准
Caption=左或上
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=OptionGroup3
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=420
Top=130
Width=121
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option2
Index=0
Style=0 - 标准
Caption=正常
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=OptionGroup2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=420
Top=105
Width=67
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=9
Top=9
Width=360
Height=501
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=样式：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=383
Top=58
Width=39
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option1
Index=0
Style=0 - 标准
Caption=普通按钮
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=OptionGroup1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=419
Top=58
Width=73
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=1
Style=0 - 标准
Caption=切换按钮
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=419
Top=81
Width=73
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=2
Style=0 - 标准
Caption=文本显示
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=530
Top=58
Width=96
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=名称：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=383
Top=15
Width=44
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=419
Top=11
Width=265
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label6
Index=-1
Style=0 - 无边框
Caption=代码中代表此按钮项(必须全工程唯一的名称)
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=419
Top=38
Width=261
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=文本：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=383
Top=161
Width=44
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=422
Top=157
Width=125
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=0
Style=0 - 无边框
Caption=提示：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=383
Top=193
Width=44
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text3
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=422
Top=189
Width=265
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label5
Index=0
Style=0 - 无边框
Caption=图像：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=383
Top=226
Width=39
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Image]
Name=Image1
Index=-1
Picture=
Stretch=0 - 自动适应
Percent=0
GrayScale=False
Enabled=True
Visible=True
Left=431
Top=222
Width=30
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command9
Index=-1
Caption=选择图像
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=469
Top=223
Width=68
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line1
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=378
Top=479
Width=314
Height=3
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=确定
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=568
Top=491
Width=55
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=635
Top=491
Width=55
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command13
Index=-1
Caption=Select
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=500
Top=491
Width=55
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=新增
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=381
Top=448
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=插入
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=439
Top=448
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=496
Top=448
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=▲
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=383
Top=419
Width=21
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=▼
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=408
Top=419
Width=21
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line2
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=377
Top=410
Width=312
Height=8
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command8
Index=-1
Caption=按顺序重置成默认名称
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=551
Top=447
Width=139
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option1
Index=3
Style=0 - 标准
Caption=分割线
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=530
Top=80
Width=73
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label7
Index=0
Style=0 - 无边框
Caption=状态：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=383
Top=104
Width=39
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option2
Index=1
Style=0 - 标准
Caption=选择
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=487
Top=106
Width=67
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option2
Index=2
Style=0 - 标准
Caption=无效
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=558
Top=106
Width=67
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option2
Index=3
Style=0 - 标准
Caption=隐藏
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=629
Top=106
Width=67
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text4
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=622
Top=222
Width=63
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label5
Index=1
Style=0 - 无边框
Caption=字体图标：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=560
Top=229
Width=81
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label7
Index=1
Style=0 - 无边框
Caption=排列：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=383
Top=129
Width=39
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option3
Index=1
Style=0 - 标准
Caption=右或下
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup3
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=545
Top=129
Width=121
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Line]
Name=Line3
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=377
Top=152
Width=312
Height=8
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label4
Index=1
Style=0 - 无边框
Caption=背景色：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=394
Top=276
Width=69
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label4
Index=2
Style=0 - 无边框
Caption=文字色：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=394
Top=302
Width=69
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text5
Index=1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=-1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=442
Top=300
Width=73
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=3
Style=0 - 无边框
Caption=选择背景色：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=528
Top=276
Width=106
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text5
Index=2
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=-1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=601
Top=274
Width=73
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text5
Index=3
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=-1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=601
Top=300
Width=73
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=4
Style=0 - 无边框
Caption=选择文字色：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=528
Top=302
Width=106
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text5
Index=4
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=-1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=442
Top=326
Width=73
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=5
Style=0 - 无边框
Caption=移动色：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=394
Top=328
Width=69
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text5
Index=5
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=-1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=443
Top=351
Width=73
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=6
Style=0 - 无边框
Caption=宽度：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=395
Top=353
Width=69
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label4
Index=7
Style=0 - 无边框
Caption=高度：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=527
Top=354
Width=69
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text5
Index=6
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=-1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=600
Top=351
Width=73
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=8
Style=0 - 无边框
Caption=圆角的直径：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=528
Top=327
Width=85
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text5
Index=7
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=-1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=600
Top=326
Width=73
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=9
Style=0 - 无边框
Caption=形状：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=396
Top=380
Width=59
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option4
Index=1
Style=0 - 标准
Caption=无框
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup4
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=501
Top=383
Width=62
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option4
Index=2
Style=0 - 标准
Caption=框线
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup4
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=564
Top=382
Width=62
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option4
Index=3
Style=0 - 标准
Caption=下划线
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup4
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=627
Top=383
Width=57
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
Type ToolEdit_t
   IDCname    As String 'utf8 格式  代码里的名字
   zText      As String 'utf8 格式  菜单文字
   nTips      As String 'utf8 格式  提示文字
   样式       As Long   '样式 0普通按钮 1多选按钮 2单选按钮 3分割线
   状态       As Long   '0 1 2 3 +10选择|+20无效|+30隐藏
   排列       As Long   ' 0左或上,1右或下
   nIco       As String 'utf8 格式 图标
   背景色     As Long
   文字色     As Long
   移动色     As Long
   选择背景色 As Long
   选择文字色 As Long
   圆角的直径 As Long
   宽度       As Long
   高度       As Long
   形状       As Long '-1 默认  =.0 - 无框.1 - 框线.2 - 下划线
End Type
Dim Shared ToolEditList() As ToolEdit_t ,ToolEditZ As Long ,ToolEditX As Long ' Z 正在设置  X 被修改

Sub ToolEdit_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    Me.Close 
End Sub
Sub ToolEdit_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   Me.UserData(0) = UserData
   Dim As Rect rc,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr, UserData)
   GetWindowRect(st->hWndForm, @rc2)
   GetWindowRect(st->hWndList, @rc)
   if rc.Left + Me.Width > rc2.right Then rc.Left = rc2.right - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top = rc2.bottom - Me.Height
   Me.Move rc.Left, rc.top
   
End Sub
Sub ToolEdit_Shown(hWndForm As hWnd ,UserData As Integer) '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   Dim st As StyleFormType Ptr = Cast(Any Ptr ,UserData)
   Dim u  As Long ,i As Long
   Dim ss As String = Trim( *st->value)
   If Len(ss) = 0 Then
      Erase ToolEditList
   Else
      Dim el() As String ,mcc(4) As String = {vfb_LangString("[按钮]") ,vfb_LangString("[切换]") ,vfb_LangString("[文本]") ,vfb_LangString("[分割]")}
      u = vbSplit(ss ,Chr(1) ,el())
      ReDim ToolEditList(u -1)
      Dim si As Long = -1
      for i = 0 To u -1
         el(i) = Trim(el(i))
         if Len(el(i)) Then
            Dim sl() As String
            Dim uu As Long = vbSplit(el(i) ,Chr(2) ,sl())
            If uu > 10 Then
               si                          += 1
               ToolEditList(si).IDCname    = sl(0)         ' utf8 格式  代码里的名字
               ToolEditList(si).zText      = sl(1)         '  utf8 格式  菜单文字
               ToolEditList(si).nTips      = sl(2)         'utf8 格式  提示文字
               ToolEditList(si).样式       = ValInt(sl(3)) '样式 0普通按钮 1多选按钮 2单选按钮 3分割线
               ToolEditList(si).状态       = ValInt(sl(4)) '0 1 2 3 +10选择|+20无效|+30隐藏
               ToolEditList(si).排列       = ValInt(sl(5)) ' 0左或上,1右或下
               ToolEditList(si).nIco       = sl(6)         'utf8 格式 图标
               ToolEditList(si).背景色     = ValInt(sl(7))
               ToolEditList(si).文字色     = ValInt(sl(8))
               ToolEditList(si).移动色     = ValInt(sl(9))
               ToolEditList(si).选择背景色 = ValInt(sl(10))
               ToolEditList(si).选择文字色 = ValInt(sl(11))
               ToolEditList(si).圆角的直径 = ValInt(sl(12))
               ToolEditList(si).宽度       = ValInt(sl(13))
               ToolEditList(si).高度       = ValInt(sl(14))
               ToolEditList(si).形状       = ValInt(sl(15)) '-1 默认  =.0 - 无框.1 - 框线.2 - 下划线
               If ToolEditList(si).样式 < 0 Or ToolEditList(si).样式 > 4 Then ToolEditList(si).样式 = 0
               List1.AddItem mcc(ToolEditList(si).样式) & " " & Utf8toStr(ToolEditList(si).IDCname)
            End If
         End If
      Next
      if si = -1 Then
         Erase ToolEditList
      Else
         If UBound(ToolEditList) <> si Then ReDim Preserve ToolEditList(si)
      End if
   End if
   List1.ListIndex = List1.ListCount -1
   ToolEdit_List1_LBN_SelChange 0 ,0
   ToolEditZ = 0
   ToolEditX = 0
End Sub

Function ToolEdit_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   If ToolEditX <> 0 Then
      Select Case MessageBox(hWndForm,  vfb_LangString("按钮已经被修改，你要保存修改吗？"), "VisualFreeBasic", _
               MB_YESNOCANCEL Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Case IDYES
             SaveToolEdit() '保存数据
         Case IDNO
         Case IDCANCEL
            Return 1
      End Select
   End if      

    Function = 0 '根据自己需要修改
End Function

Sub SaveToolEdit() ' 保存
   Dim st As StyleFormType Ptr = Cast(Any Ptr ,Me.UserData(0))
   
   Dim ss As String
   Dim i  As Long
   if UBound(ToolEditList) > -1 Then
      for i = 0 To UBound(ToolEditList)
         If Len(ss) > 0 Then ss &= Chr(1)
         ss &= ToolEditList(i).IDCname & Chr(2) & _ 'utf8 格式
            ToolEditList(i).zText      & chr(2) & _ 'utf8 格式
            ToolEditList(i).nTips      & Chr(2) & _
            ToolEditList(i).样式       & Chr(2) & _ '样式 0普通按钮 1多选按钮 2单选按钮 3分割线
            ToolEditList(i).状态       & Chr(2) & _ '0 1 2 3 +10选择|+20无效|+30隐藏
            ToolEditList(i).排列       & Chr(2) & _ ' 0左或上,1右或下
            ToolEditList(i).nIco       & Chr(2) & _ 'utf8 格式 图标
            ToolEditList(i).背景色     & Chr(2) & _
            ToolEditList(i).文字色     & Chr(2) & _
            ToolEditList(i).移动色     & Chr(2) & _
            ToolEditList(i).选择背景色 & Chr(2) & _
            ToolEditList(i).选择文字色 & Chr(2) & _
            ToolEditList(i).圆角的直径 & Chr(2) & _
            ToolEditList(i).宽度       & Chr(2) & _
            ToolEditList(i).高度       & Chr(2) & _
            ToolEditList(i).形状 '-1 默认  =.0 - 无框.1 - 框线.2 - 下划线
      Next
   End if
   st->Rvalue = ss
   ToolEditX  = 0
   
End sub

Sub ToolEdit_List1_LBN_SelChange(hWndForm As hWnd ,hWndControl As hWnd) '选择了列表
   Dim si As Long = List1.ListIndex
   if si = -1 Then
      Command9.Enabled = False
      Command6.Enabled = False
      Command7.Enabled = False
      Command4.Enabled = False
      Command5.Enabled = False
   Else
      ToolEditZ        = 1
      Command9.Enabled = True
      Command4.Enabled = True
      Command5.Enabled = True
      Command6.Enabled = si > 0
      Command7.Enabled = si < List1.ListCount -1
      Text1.Text       = Utf8toStr(ToolEditList(si).IDCname) 'utf8 格式
      Text2.Text       = Utf8toStr(ToolEditList(si).zText) 'utf8 格式
      Text3.Text       = Utf8toStr(ToolEditList(si).nTips) 'utf8 格式
      Dim i As Long
      For i = 0 To 3
         Option1(i).Value = i = ToolEditList(si).样式
      Next
      For i = 0 To 3
         Option2(i).Value = i = ToolEditList(si).状态 '0 1 2 3 +10选择|+20无效|+30隐藏
      Next
      For i = 0 To 1
         Option3(i).Value = i = ToolEditList(si).排列 ' 0左或上,1右或下
      Next
      Text5(0).Text = IIf(ToolEditList(si).背景色 = -1 ,"-1" ,"&H" & Hex(ToolEditList(si).背景色))
      Text5(1).Text = IIf(ToolEditList(si).文字色 = -1 ,"-1" ,"&H" & Hex(ToolEditList(si).文字色))
      Text5(2).Text = IIf(ToolEditList(si).选择背景色 = -1 ,"-1" ,"&H" & Hex(ToolEditList(si).选择背景色))
      Text5(3).Text = IIf(ToolEditList(si).选择文字色 = -1 ,"-1" ,"&H" & Hex(ToolEditList(si).选择文字色))
      Text5(4).Text = IIf(ToolEditList(si).移动色 = -1 ,"-1" ,"&H" & Hex(ToolEditList(si).移动色))
      Text5(5).Text = ToolEditList(si).宽度
      Text5(6).Text = ToolEditList(si).高度
      Text5(7).Text = ToolEditList(si).圆角的直径
      For i = 0 To 3
         Option4(i).Value = ((i -1) = ToolEditList(si).形状) '-1 默认  =.0 - 无框.1 - 框线.2 - 下划线
      Next
      If Left(ToolEditList(si).nIco ,4) = "ico=" Then
         Image1.Picture = ""
         Text4.Text     = Mid(ToolEditList(si).nIco ,5)
      Else
         Text4.Text = ""
         Dim pa As String = GetProRunFile(0 ,4)
         Dim bb As String = ToolEditList(si).nIco
         Image1.Picture = pa & "images\" & Utf8toStr(bb) 'utf8 格式
      End If
      ToolEditZ = 0
   End if
End Sub

Sub ToolEdit_Command9_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   Dim bb As String = StrToUtf8(GetImgForm(Utf8toStr(ToolEditList(si).nIco) ,0)) '获取图像文件名称
   If bb <> ToolEditList(si).nIco Then
      ToolEditList(si).nIco = bb
      Dim pa As String = GetProRunFile(0,4)
      Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      ToolEditX = 1
   End If
End Sub

Sub ToolEdit_Command3_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   Dim u As Long = UBound(ToolEditList) + 1
   ReDim Preserve ToolEditList(u)
   Dim st As StyleFormType Ptr = Cast(Any Ptr ,Me.UserData(0))
   Dim kk As Long              = u + 1 ,i As Long
   Dim bb As String            = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_" & kk
   if u > 0 Then '使用名词必须唯一
      Do
         bb = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_" & kk
         for i = 1 To u
            If bb = ToolEditList(i).IDCname Then
               bb = ""
               Exit for
            End if
         Next
         if Len(bb) Then Exit Do
         kk += 1
      Loop
   End if
   ToolEditList(u).样式       = 0  '样式 0普通按钮 1多选按钮 2单选按钮 3分割线
   ToolEditList(u).状态       = 0  '0 1 2 3 +10选择|+20无效|+30隐藏
   ToolEditList(u).排列       = 0  ' 0左或上,1右或下
   ToolEditList(u).nIco       = "" 'utf8 格式 图标
   ToolEditList(u).背景色     = -1
   ToolEditList(u).文字色     = -1
   ToolEditList(u).移动色     = -1
   ToolEditList(u).选择背景色 = -1
   ToolEditList(u).选择文字色 = -1
   ToolEditList(u).圆角的直径 = -1
   ToolEditList(u).宽度       = -1
   ToolEditList(u).高度       = -1
   ToolEditList(u).形状       = -1 '-1 默认  =.0 - 无框.1 - 框线.2 - 下划线
   ToolEditList(u).IDCname    = bb
   List1.ListIndex            = List1.AddItem(vfb_LangString("[按钮]") & " " & Utf8toStr(ToolEditList(u).IDCname))
   ToolEdit_List1_LBN_SelChange 0 ,0
   ToolEditX = 1
End Sub

Sub ToolEdit_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ToolEditList)
   Dim si As Long = List1.ListIndex
   if si <0 Then Return
   Swap ToolEditList(si), ToolEditList(si -1)
   Dim As CWSTR s1=List1.List(si),s2=List1.List(si-1)
   List1.List(si -1) = s1
   List1.List(si) =s2    
   List1.ListIndex = si -1
   ToolEdit_List1_LBN_SelChange 0, 0
   ToolEditX =1
End Sub

Sub ToolEdit_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ToolEditList)
   Dim si As Long = List1.ListIndex
   if si = -1 Or si >= u Then Return
   Swap ToolEditList(si), ToolEditList(si + 1)
   Dim As CWSTR s1=List1.List(si),s2=List1.List(si+1)
   List1.List(si +1) = s1
   List1.List(si) =s2    
   List1.ListIndex = si +1
   ToolEdit_List1_LBN_SelChange 0, 0
   ToolEditX =1   
End Sub

Sub ToolEdit_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ToolEditList) + 1
   
   ReDim Preserve ToolEditList(u)
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   Dim i As Long, kk As Long = u + 1
   Dim bb As String = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_" & kk
   if u > 0 Then  '使用名词必须唯一
      Do
         bb = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_" & kk
         for i = 1 To u
            if bb = ToolEditList(i).IDCname Then
               bb = ""
               Exit for
            End if
         Next
         if Len(bb) Then Exit Do
         kk += 1
      Loop
   End if
  Dim si As Long  = List1.ListIndex
   if si = -1 Then Return
   
   List1.ListIndex = List1.InsertItem(si,  vfb_LangString("[按钮]") & " " & Utf8toStr(bb))
   
   for i = u To si + 1 Step -1
      ToolEditList(i) = ToolEditList(i -1)
   Next
   ToolEditList(si).IDCname = bb
   ToolEditList(si).zText = ""   'utf8 格式  菜单文字
   ToolEditList(si).nTips = ""    'utf8 格式  提示文字
   ToolEditList(si).样式       = 0  '样式 0普通按钮 1多选按钮 2单选按钮 3分割线
   ToolEditList(si).状态       = 0  '0 1 2 3 +10选择|+20无效|+30隐藏
   ToolEditList(si).排列       = 0  ' 0左或上,1右或下
   ToolEditList(si).nIco       = "" 'utf8 格式 图标
   ToolEditList(si).背景色     = -1
   ToolEditList(si).文字色     = -1
   ToolEditList(si).移动色     = -1
   ToolEditList(si).选择背景色 = -1
   ToolEditList(si).选择文字色 = -1
   ToolEditList(si).圆角的直径 = -1
   ToolEditList(si).宽度       = -1
   ToolEditList(si).高度       = -1
   ToolEditList(si).形状       = -1 '-1 默认  =.0 - 无框.1 - 框线.2 - 下划线
   ToolEdit_List1_LBN_SelChange 0, 0
   ToolEditX = 1
End Sub

Sub ToolEdit_Command5_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   Select Case MessageBox(hWndForm ,vfb_LangString("你真的要删除吗？") ,"VisualFreeBasic" , _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON2 Or MB_APPLMODAL)
      Case IDYES
      Case IDNO
         Return
   End Select
   Dim u As Long = UBound(ToolEditList)
   If u = 0 Then
      Erase ToolEditList
      List1.Clear
      u = -1
   Else
      Dim si As Long = List1.ListIndex
      if si = -1 Then Return
      if si < u Then
         for i As Long = si To u -1
            ToolEditList(i) = ToolEditList(i + 1)
         Next
      End If
      u -= 1
      ReDim Preserve ToolEditList(u)
   End if
   List1.Clear
   Dim mcc(4) As String = {"[按钮] " ,"[切换] " ,"[文本] " ,"[分割] "}
   If u > -1 Then
      For i As Long = 0 To u
         List1.AddItem mcc(ToolEditList(i).样式) & " " & Utf8toStr(ToolEditList(i).IDCname)
      Next
   End If
   ToolEdit_List1_LBN_SelChange 0 ,0
   ToolEditX = 1
End Sub

Sub ToolEdit_Command13_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim ss As String = "   Select Case nID" & vbCrLf
   Dim u As Long = ubound(ToolEditList)
   if u > -1 Then
      For i As Long = 0 To u
            ss &= "      Case " & Utf8toStr(ToolEditList(i).IDCname) & " ' " & Utf8toStr(ToolEditList(i).zText) & vbCrLf  & vbCrLf

      Next
   End If
   ss &= "   End Select"
   FF_ClipboardSetText ss
       
   MessageBox(hWndForm, StringToCWSTR( "已经复制 Select 代码到系统剪切板。" & vbCrLf & _
      "将其粘贴到工具栏事件中即可。"), "VisualFreeBasic", _
      MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
End Sub

Sub ToolEdit_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   If ToolEditX Then   SaveToolEdit '保存数据

   Me.Close    
End Sub

Sub ToolEdit_Command8_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Select Case MessageBox(hWndForm, "你真的要重置名称吗？", "VisualFreeBasic", _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON2 Or MB_APPLMODAL)
      Case IDYES
      Case IDNO
         Return
   End Select
   Dim u As Long = ubound(ToolEditList)
   if u > -1 Then
      Dim mcc(4) As String = {vfb_LangString("[按钮]") ,vfb_LangString("[切换]") ,vfb_LangString("[文本]") ,vfb_LangString("[分割]")}
      Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
      Dim i As Long
      Dim bb As String = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_"
      for i As Long = 0 To u
         ToolEditList(i).IDCname = bb & i + 1
         List1.List(i) = mcc(ToolEditList(i).样式) & " " & Utf8toStr(ToolEditList(i).IDCname)
      Next
   End if
   ToolEdit_List1_LBN_SelChange 0, 0
End Sub

Sub ToolEdit_Text1_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).IDCname = CWSTRtoUTF8(Text1.Text)
   Dim mcc(4) As String = {vfb_LangString("[按钮]") ,vfb_LangString("[切换]") ,vfb_LangString("[文本]") ,vfb_LangString("[分割]")}
   List1.List(si) = mcc(ToolEditList(si).样式) & " " & Utf8toStr(ToolEditList(si).IDCname)
   ToolEditX = 1
End Sub

Sub ToolEdit_Option1_BN_Clicked(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).样式 = ControlIndex
   Dim mcc(4) As String = {vfb_LangString("[按钮]") ,vfb_LangString("[切换]") ,vfb_LangString("[文本]") ,vfb_LangString("[分割]")}
   List1.List(si) = mcc(ToolEditList(si).样式) & " " & Utf8toStr(ToolEditList(si).IDCname)   
   ToolEditX = 1
End Sub

Sub ToolEdit_Text2_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).zText = CWSTRtoUTF8(Text2.Text)
   ToolEditX = 1
End Sub

Sub ToolEdit_Text3_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).nTips = CWSTRtoUTF8(Text3.Text)
   ToolEditX = 1
End Sub

Sub ToolEdit_Text4_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   If ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   If si = -1 Then Return
   ToolEditList(si).nIco ="ico=&H" & Hex(ValInt(Text4.Text))
   ToolEditX = 1
End Sub

Sub ToolEdit_Option2_BN_Clicked(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   If ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   If si = -1 Then Return
   ToolEditList(si).状态 = ControlIndex
   ToolEditX = 1
End Sub

Sub ToolEdit_Option3_BN_Clicked(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   If ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).排列 = ControlIndex
   ToolEditX = 1
End Sub

Sub ToolEdit_Text5_EN_Change(ControlIndex As Long ,hWndForm As hWnd ,hWndControl As hWnd) '文本已经被修改（修改前用 EN_UPDATE
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   If ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   If si = -1 Or si>UBound(ToolEditList) Then Return
   Dim vv As Long = ValInt(Text5(ControlIndex).Text)
   Select Case ControlIndex
      Case 0 : ToolEditList(si).背景色     = vv
      Case 0 : ToolEditList(si).背景色     = vv
      Case 1 : ToolEditList(si).文字色     = vv
      Case 2 : ToolEditList(si).选择背景色 = vv
      Case 3 : ToolEditList(si).选择文字色 = vv
      Case 4 : ToolEditList(si).移动色     = vv
      Case 5 : ToolEditList(si).宽度       = vv
      Case 6 : ToolEditList(si).高度       = vv
      Case 7 : ToolEditList(si).圆角的直径 = vv
   End Select
   ToolEditX = 1
End Sub

Sub ToolEdit_Option4_BN_Clicked(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   If ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).形状 = ControlIndex -1
   ToolEditX = 1
End Sub

Sub ToolEdit_Text5_WM_LButtonUp(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '释放鼠标左键
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   'MouseFlags  MK_CONTROL   MK_LBUTTON     MK_MBUTTON     MK_RBUTTON    MK_SHIFT     MK_XBUTTON1       MK_XBUTTON2 
   ''           CTRL键按下   鼠标左键按下   鼠标中键按下   鼠标右键按下  SHIFT键按下  第一个X按钮按下   第二个X按钮按下
   '检查什么键按下用  If (MouseFlags And MK_CONTROL)<>0 Then CTRL键按下 
   'xPos yPos   当前鼠标位置，相对于控件。就是在控件里的坐标。
   If ControlIndex < 5 Then
      PostMessage( hWndForm , &H901 , ControlIndex , ControlIndex )  '默认为 PostMessageW

   End If   
End Sub

Function ToolEdit_Custom(hWndForm As hWnd ,wMsg As UInteger ,wParam As wParam ,lParam As lParam) As LResult '自定义消息（全部消息），在其它事件处理后才轮到本事件。
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'wMsg        消息值，0--&H400 由WIN系统定义，用户自定义是 WM_USER+** 定义。例 PostMessage( 窗口句柄 , Msg , wParam , lParam )
   'wParam      主消息参数，什么作用，由发送消息者定义
   'lParam      副消息参数，什么作用，由发送消息者定义
   Select Case wMsg
      Case &H901
         Dim i As Long = lParam
         If i<0 Or i>4 Then Return 0
         Dim cc As Long = OpenColorDialog(hWndForm ,Text5(i).Text.ValInt)
         If cc = -1 Then
            Text5(i).Text = "-1"
         Else
            Text5(i).Text = "&H" & Hex(cc)
         End If
         
   End Select
   Function = FALSE ' 若不想系统继续处理此消息，则应返回 TRUE （俗称吃掉消息）。
   
End Function






















