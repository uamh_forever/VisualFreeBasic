Type Class_IPControl Extends Class_Control
    Declare Property Address() As DWord              '返回/设置四个字段的合成值,FIRST_IPADDRESS 提取字段0值,SECOND_IPADDRESS 提取字段1值,THIRD_IPADDRESS 提取字段2值,FOURTH_IPADDRESS 提取字段3值
    Declare Property Address(dwAddr As DWord)        'MAKEIPADDRESS(b0, b1, b2, b3)
    Declare Sub SetAddress (b0 As UByte , b1 As UByte, b2 As UByte, b3 As UByte) '设置四个字段的地址值
    Declare Sub ClearAddress () '清除IP地址控件的内容
    Declare Function IsBlank () As Boolean  '如果所有字段都是空白，则返回非零值，否则返回零。
    Declare Sub SetFocusField (nField As Long ) '将键盘焦点设置到IP地址控件中的指定字段。
    Declare Function SetRange (nField As Long,loRange As UByte,hiRange As UByte) As Boolean  '设置IP地址控件中指定字段的有效范围(索引从0开始)。成功返回非零值，否则返回零。
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_IPControl.Address() As DWord              '返回/设置四个字段的合成值
  Dim aa As DWord 
  IPAddress_GetAddress hWndControl,@aa
  Return aa
End Property
Property Class_IPControl.Address(dwAddr As DWord)        'MAKEIPADDRESS(b0, b1, b2, b3)
  IPAddress_SetAddress hWndControl,dwAddr
End Property
Sub Class_IPControl.SetAddress (b0 As UByte , b1 As UByte, b2 As UByte, b3 As UByte) '设置四个字段的地址值
  Dim dwAddr As DWord
  dwAddr=MAKEIPADDRESS(b0, b1, b2, b3)
  IPAddress_SetAddress hWndControl,dwAddr
End Sub
Sub Class_IPControl.ClearAddress () '清除IP地址控件的内容
  IPAddress_ClearAddress hWndControl
End Sub
Function Class_IPControl.IsBlank () As Boolean  '如果所有字段都是空白，则返回非零值，否则返回零。
  Return SendMessage( hWndControl , IPM_ISBLANK , 0 , 0 )
End Function
Sub Class_IPControl.SetFocusField (nField As Long ) '将键盘焦点设置到IP地址控件中的指定字段。
  IPAddress_SetFocus hWndControl,nField
End Sub
Function Class_IPControl.SetRange (nField As Long,loRange As UByte,hiRange As UByte) As Boolean  '设置IP地址控件中指定字段的有效范围(索引从0开始)。成功返回非零值，否则返回零。
  Return IPAddress_SetRange( hWndControl,nField ,loRange ,hiRange )
End Function
