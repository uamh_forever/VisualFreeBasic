Type Class_Slider Extends Class_Control

    Declare Property Value () As Long                  '返回/设置滑块的当前逻辑位置
    Declare Property Value (ByVal bValue As Long )
    Declare Property MaxValue () As Long                  '返回/设置滑块的最大位置。
    Declare Property MaxValue (ByVal nMaxValue As Long )
    Declare Property MinValue () As Long                  '返回/设置滑块的最小位置。
    Declare Property MinValue (ByVal nMinValue As Long )
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_Slider.Value () As Long                  '返回/设置滑块的当前逻辑位置
  Return SendMessage (hWndControl, TBM_GETPOS, 0, 0)
End Property
Property Class_Slider.Value (ByVal bValue As Long )
  SendMessage hWndControl, TBM_SETPOS, True, bValue
End Property
Property Class_Slider.MaxValue () As Long                  '返回/设置滑块的最大位置。
  Return SendMessage (hWndControl, TBM_GETRANGEMAX, 0, 0)
End Property
Property Class_Slider.MaxValue (ByVal nMaxValue As Long )
  SendMessage hWndControl, TBM_SETRANGEMAX, True, nMaxValue
End Property
Property Class_Slider.MinValue () As Long                  '返回/设置滑块的最小位置。
  Return SendMessage (hWndControl, TBM_GETRANGEMIN, 0, 0)
End Property
Property Class_Slider.MinValue (ByVal nMinValue As Long )
  SendMessage hWndControl, TBM_SETRANGEMIN, True, nMinValue
End Property

