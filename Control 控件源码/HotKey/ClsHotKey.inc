Type Class_HotKey
   Private : 
   m_hWndForm As hWnd '控件句柄
   m_IDC      As Long '控件IDC
   m_ID(Any)  As Long ' HiWord()=虚拟键码,ID 是虚拟键码+100，LoWord()=组合键
   Declare Function GetFP() As FormControlsPro_TYPE Ptr '返回控件保存数据的指针（一般是类自己用）
   Public : 

   Declare Property hWndForm() As .hWnd '返回/设置控件所在的窗口句柄，主要用于多开同一个窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
   Declare Property hWndForm(ByVal hWndParent As .hWnd) '获取控件所在的窗口句柄（父窗口）
   Declare Property IDC() As Long '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Property Tag() As CWSTR '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWSTR)
   Declare Property UserData(idx As Long) As Integer '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx As Long ,bValue As Integer)
   Declare Function AddHotKey(vKey As Long ,fsModifiers As Long = 0) As Long ' 新增热键 (函数成功，则返回值非零。){1.&H30 0键.&H41 A键.&H70 F1键.&H60 数字键盘0键} {2.MOD_ALT ALT键.MOD_CONTROL CTRL键.MOD_SHIFT SHIFT键.MOD_WIN WINDOWS键}
   Declare Function IsHotKey(vKey As Long,fsModifiers As Long = 0 )As Long '判断热键是否已经存在，有返回非零 
   Declare Function KillHotKey(vKey As Long,fsModifiers As Long = 0 )As Long '删除热键，成功返回非零 
   
   '{代码不提示}
   Declare Sub UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
End Type


Declare Sub YF_GetKeyboardState_Server(ygks As Integer Ptr) '键盘状态获取服务线程

 'GetKeyboardState
'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Sub Class_HotKey.UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   Dim i As Long ,u As Long = UBound(m_ID) ,vKey As Long,fsModifiers As Long
   If u > -1 Then
      For i = 0 To u
         vKey = HiWord(m_ID(i))
         fsModifiers = LoWord(m_ID(i))
          UnregisterHotKey(m_hWndForm ,vKey + 100)
      Next
   End If
End Sub


Function Class_HotKey.GetFP() As FormControlsPro_TYPE Ptr '返回自己控件的指针
   Dim fp As FormControlsPro_TYPE ptr = GetPropW(m_hWndForm, "FCP_TYPE_PTR")
   if fp=0 Then fp = vfb_Get_Control_Ptr(m_hWndForm)
   While fp
      if fp->IDC = m_IDC Then Return fp      
      fp = fp->VrControls
   Wend

End Function
Property Class_HotKey.hWndForm() As .hWnd         '返回/设置控件所在的窗口句柄
      Return m_hWndForm
End Property
Property Class_HotKey.hWndForm(ByVal hWndParent As .hWnd)
   m_hWndForm = hWndParent
End Property

Property Class_HotKey.IDC() As Long                  '返回/设置两次调用 Timer 控件的 Timer 事件间隔的毫秒数。
   Return m_IDC
End Property
Property Class_HotKey.IDC(ByVal NewIDC As Long)
   m_IDC  =NewIDC
End Property
Property Class_HotKey.Tag() As CWSTR 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return fp->Tag
   End If
End Property
Property Class_HotKey.Tag(ByVal sText As CWSTR )
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp Then
      fp->Tag = sText
   End If
End Property
Property Class_HotKey.UserData(idx AS LONG) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return 0
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      Return fp->UserData(idx)
   End If   

End Property
Property Class_HotKey.UserData(idx As Long, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      fp->UserData(idx) = bValue
   End If    
End Property
Function Class_HotKey.AddHotKey(vKey As Long ,fsModifiers As Long = 0) As Long
   If vKey < 1 Or vKey > &HFE Then Return 0
   Dim jj As Long = MAKELONG(fsModifiers ,vKey)
   Dim i  As Long ,u As Long = UBound(m_ID)
   If u > -1 Then
      For i = 0 To u
         If m_ID(i) = jj Then Return 0 '已经新增
      Next
   End If
   Dim r As Long = RegisterHotKey(m_hWndForm ,vKey + 100 ,fsModifiers ,vKey)
   If r Then
      u += 1
      ReDim Preserve m_ID(u)
      m_ID(u) = jj
   End If
   Function = r
End Function
Function Class_HotKey.IsHotKey(vKey As Long,fsModifiers As Long = 0 ) As Long '
   If vKey < 1 Or vKey > &HFE Then Return 0
   Dim jj As Long = MAKELONG(fsModifiers ,vKey)
   Dim i  As Long ,u As Long = UBound(m_ID)
   If u > -1 Then
      For i = 0 To u
         If m_ID(i) = jj Then Return 1 '已经新增
      Next
   End If
   Function = 0
End Function
Function Class_HotKey.KillHotKey(vKey As Long,fsModifiers As Long = 0 ) As Long '
   If vKey < 1 Or vKey > &HFE Then Return 0
   Dim jj As Long = MAKELONG(fsModifiers ,vKey)
   Dim i  As Long ,u As Long = UBound(m_ID),ti As Long =-1
   If u > -1 Then
      For i = 0 To u
         If m_ID(i) = jj Then 
            ti = i
            Exit For 
         End If    
      Next
   End If
   If ti = -1 Then Return 0
   If ti < u Then 
      For i = ti To u -1
         m_ID(i)=m_ID(i+1)   
      Next 
   End If   
   If u=0 Then Erase m_ID Else ReDim Preserve m_ID(u-1)
   Function = UnregisterHotKey(m_hWndForm ,vKey + 100)
End Function

Sub YF_GetKeyboardState_Server(ygks As Integer Ptr) '键盘状态获取服务线程
   Dim vKeyList    As String
   Dim oldKeyState As String
   Dim i           As Long ,pp As NMHDR ,fsModifiers As Long
   vKeyList = Chr(1 ,2 ,3 ,4 ,5 ,6 ,8 ,9 ,&HC ,&HD) '需要获取的按键列表
   For i = &H13 To &H39
      vKeyList &= Chr(i)
   next
   For i = &H41 To &H5A
      vKeyList &= Chr(i)
   Next
   vKeyList &= Chr(&H5D)
   For i = &H5F To &H7B
      vKeyList &= Chr(i)
   Next
   vKeyList &= Chr(&H90 ,&H91)
   Dim u As Long = Len(vKeyList)
   oldKeyState = String(u ,0)
   u           -= 1
   For i = 0 To u
      oldKeyState[i] = HiByte(GetKeyState(vKeyList[i]))
   Next
   Dim newKeyState As String = oldKeyState
   pp.hwndFrom = Cast(hWnd ,ygks[1])
   pp.idFrom   = ygks[0]
   Delete[] ygks
   While IsWindow(pp.hwndFrom)
      'If GetKeyboardState(StrPtr(newKeyState)) Then '获取键盘所有信息  此API居然必须是自己线程才可以
      For i = 0 To u
         newKeyState[i] = HiByte(GetKeyState(vKeyList[i]))
      Next
      If oldKeyState <> newKeyState Then '键盘有改变
         For i = 0 To u
            If oldKeyState[i] <> newKeyState[i] Then
               oldKeyState[i] = newKeyState[i]
               fsModifiers = 0
               If HiByte(GetKeyState(VK_SHIFT))   Then fsModifiers Or= MOD_SHIFT
               If HiByte(GetKeyState(VK_CONTROL)) Then fsModifiers Or= MOD_CONTROL
               If HiByte(GetKeyState(VK_MENU))    Then fsModifiers Or= MOD_ALT
               If HiByte(GetKeyState(VK_LWIN)) <> 0 Or HiByte(GetKeyState(VK_RWIN)) <> 0 Then fsModifiers Or= MOD_WIN
               If newKeyState[i] Then fsModifiers Or= 16  '按键是按下还是放开
               pp.code = MAKELONG(vKeyList[i] ,fsModifiers)
               SendMessage(pp.hwndFrom ,WM_NOTIFY ,pp.idFrom ,Cast(lParam ,@pp))
            End If
         Next
      End If
      Sleep 20
   Wend   

End Sub



