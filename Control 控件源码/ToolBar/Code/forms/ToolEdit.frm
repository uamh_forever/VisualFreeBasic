﻿#VisualFreeBasic_Form#  Version=5.6.8
Locked=0

[Form]
Name=ToolEdit
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_BORDER,WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP
Style=3 - 常规窗口
Icon=confg_32.ico|ICON_CONFG_32
Caption=工具栏编辑器
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=564
Height=393
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
Left=9
Top=9
Width=218
Height=351
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=样式：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=241
Top=56
Width=39
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option1
Index=0
Style=0 - 标准
Caption=普通按钮
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=OptionGroup1
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=281
Top=56
Width=73
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=1
Style=0 - 标准
Caption=多选按钮
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup1
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=281
Top=79
Width=73
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=2
Style=0 - 标准
Caption=单选按钮
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup1
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=392
Top=78
Width=73
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=3
Style=0 - 标准
Caption=带下拉列表
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup1
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=392
Top=56
Width=96
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=名称：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=241
Top=13
Width=44
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=False
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=281
Top=9
Width=265
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label6
Index=-1
Style=0 - 无边框
Caption=代码中代表此按钮项(必须全工程唯一的名称)
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=281
Top=36
Width=261
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=文本：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=242
Top=159
Width=44
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=False
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=282
Top=155
Width=125
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=提示：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=242
Top=191
Width=44
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text3
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=282
Top=187
Width=265
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=图像：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=242
Top=224
Width=39
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Image]
Name=Image1
Index=-1
Picture=
Stretch=0 - 自动适应
Percent=0
GrayScale=False
Enabled=True
Visible=True
Left=291
Top=220
Width=30
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command9
Index=-1
Caption=选择图像
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=340
Top=224
Width=68
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line1
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=236
Top=321
Width=314
Height=3
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=确定
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=426
Top=333
Width=55
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=493
Top=333
Width=55
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command13
Index=-1
Caption=Select
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=358
Top=333
Width=55
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=新增
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=239
Top=290
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=插入
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=297
Top=290
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=354
Top=290
Width=47
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=▲
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=241
Top=261
Width=21
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=▼
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=266
Top=261
Width=21
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line2
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无剪头
ArrowStartH=0 - 无剪头
ArrowEndW=0 - 无剪头
ArrowEndH=0 - 无剪头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=238
Top=255
Width=312
Height=8
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command8
Index=-1
Caption=按顺序重置成默认名称
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=409
Top=289
Width=139
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=自动调整大小
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=281
Top=104
Width=107
Height=13
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check2
Index=-1
Style=0 - 标准
Caption=显示文本
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=392
Top=104
Width=107
Height=13
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=4
Style=0 - 标准
Caption=分割线
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup1
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=482
Top=79
Width=73
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label7
Index=-1
Style=0 - 无边框
Caption=状态：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=241
Top=126
Width=39
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[CheckBox]
Name=Check3
Index=-1
Style=0 - 标准
Caption=被选择
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=280
Top=129
Width=65
Height=14
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check4
Index=-1
Style=0 - 标准
Caption=可用
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=349
Top=129
Width=48
Height=14
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check5
Index=-1
Style=0 - 标准
Caption=隐藏
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=400
Top=129
Width=48
Height=14
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check6
Index=-1
Style=0 - 标准
Caption=省略号
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=456
Top=129
Width=64
Height=14
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
Type ToolEdit_t
   IDCname As String  'utf8 格式  代码里的名字
   zText As String    'utf8 格式  菜单文字
   nTips As String    'utf8 格式  提示文字
   tbStyle As Long    '样式 0普通按钮 1多选按钮 2单选按钮 3下拉列表  4分割线
   AutoSize As Long   ' 自动调整大小
   ShowText As Long   ' 显示文本
   xChecked As Long   ' 被选择
   xEnabled As Long   ' 可用
   xHidden As Long   ' 隐藏
   xEllipses As Long   ' 省略号
   nIco As String       'utf8 格式 图标
End Type
Dim Shared ToolEditList() As ToolEdit_t, ToolEditZ As Long, ToolEditX As Long ' Z 正在设置  X 被修改

Sub ToolEdit_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    Me.Close 
End Sub
Sub ToolEdit_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   Me.UserData(0) = UserData
   Dim As Rect rc,rc2
   Dim st As StyleFormType Ptr = Cast(Any Ptr, UserData)
   GetWindowRect(st->hWndForm, @rc2)
   GetWindowRect(st->hWndList, @rc)
   if rc.Left + Me.Width > rc2.right Then rc.Left = rc2.right - Me.Width
   if rc.top + Me.Height > rc2.bottom Then rc.top = rc2.bottom - Me.Height
   Me.Move rc.Left, rc.top
   
   
End Sub
Sub ToolEdit_Shown(hWndForm As hWnd ,UserData As Integer) '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   
   Dim st As StyleFormType Ptr = Cast(Any Ptr ,UserData)
   Dim u  As Long ,i As Long
   Dim ss As String = trim( *st->value)
   if Len(ss) = 0 Then
      Erase ToolEditList
   Else
      Dim el() As String ,mcc(4) As String = {vfb_LangString("[按钮]") ,vfb_LangString("[多选]") ,vfb_LangString("[单选]") ,vfb_LangString("[下拉]") ,vfb_LangString("[分割]")}
      u = vbSplit(ss ,Chr(1) ,el())
      ReDim ToolEditList(u -1)
      Dim si As Long = -1
      for i = 0 To u -1
         el(i) = Trim(el(i))
         if Len(el(i)) Then
            Dim sl() As String
            Dim uu   As Long = vbSplit(el(i) ,Chr(2) ,sl())
            If uu > 10 Then
               si                         += 1
               ToolEditList(si).IDCname   = sl(0)          ' utf8 格式  代码里的名字
               ToolEditList(si).zText     = sl(1)          '  utf8 格式  菜单文字
               ToolEditList(si).nTips     = sl(2)          'utf8 格式  提示文字
               ToolEditList(si).tbStyle   = valint(sl(3))  '样式 0按钮 1分割线 2下拉列表 3全下拉列表
               ToolEditList(si).nIco      = sl(4)          'utf8 格式 图标
               ToolEditList(si).AutoSize  = valint(sl(5))  ' 自动调整大小
               ToolEditList(si).ShowText  = valint(sl(6))  ' 显示文本
               ToolEditList(si).xChecked  = valint(sl(7))  '被选择
               ToolEditList(si).xEnabled  = valint(sl(8))  '可用
               ToolEditList(si).xHidden   = valint(sl(9))  '隐藏
               ToolEditList(si).xEllipses = valint(sl(10)) ' 省略号
               if ToolEditList(si).tbStyle < 0 Or ToolEditList(si).tbStyle > 4 Then ToolEditList(si).tbStyle = 0
               List1.AddItem mcc(ToolEditList(si).tbStyle) & " " & Utf8toStr(ToolEditList(si).IDCname)
            End If
         End If
      Next
      if si = -1 Then
         Erase ToolEditList
      Else
         if UBound(ToolEditList) <> si Then ReDim Preserve ToolEditList(si)
      End if
   End if
   List1.ListIndex = List1.ListCount -1
   ToolEdit_List1_LBN_SelChange 0 ,0
   ToolEditZ = 0
   ToolEditX = 0
End Sub

Function ToolEdit_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   if ToolEditX <> 0 Then
      Select Case MessageBox(hWndForm,  vfb_LangString("工具栏已经被修改，你要保存修改吗？"), "VisualFreeBasic", _
               MB_YESNOCANCEL Or MB_ICONQUESTION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Case IDYES
             SaveToolEdit() '保存数据
         Case IDNO
         Case IDCANCEL
            Return 1
      End Select
   End if      

    Function = 0 '根据自己需要修改
End Function

sub SaveToolEdit()    ' 保存
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))

   Dim ss As String
   Dim i As Long
   if UBound(ToolEditList) > -1 Then
      for i = 0 To UBound(ToolEditList)
         if Len(ss) > 0 Then ss &= chr(1)
         ss &= ToolEditList(i).IDCname & chr(2) & _    'utf8 格式
            ToolEditList(i).zText & chr(2) & _   'utf8 格式
            ToolEditList(i).nTips & chr(2) & _
            ToolEditList(i).tbStyle & chr(2) & _
            ToolEditList(i).nIco & chr(2) & _    'utf8 格式
            ToolEditList(i).AutoSize & chr(2) & _   ' 自动调整大小
            ToolEditList(i).ShowText & chr(2) & _     ' 显示文本
            ToolEditList(i).xChecked  & chr(2) & _    
            ToolEditList(i).xEnabled  & chr(2) & _    
            ToolEditList(i).xHidden  & chr(2) & _    
            ToolEditList(i).xEllipses   
      Next
   End if
   st->Rvalue = ss
   ToolEditX = 0
   
End sub

Sub ToolEdit_List1_LBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '选择了列表
   Dim si As Long = List1.ListIndex
   if si = -1 Then
      Text1.Enabled = False
      Text2.Enabled = False
      Text3.Enabled = False
      for ii As Long = 0 To 4
         Option1(ii).Enabled = False
      Next
      Check1.Enabled = False
      Check2.Enabled = False
      Check3.Enabled = False
      Check4.Enabled = False
      Check5.Enabled = False
      Check6.Enabled = False
      Command9.Enabled = False
      Command6.Enabled = False
      Command7.Enabled = False
      Command4.Enabled = False
      Command5.Enabled = False
   Else
      
      ToolEditZ = 1
      Text1.Enabled = True
      Text2.Enabled = True
      Text3.Enabled = True
      Command9.Enabled = True
      Command4.Enabled = True
      Command5.Enabled = True
      Command6.Enabled = si > 0
      Command7.Enabled = si < List1.ListCount -1
      Text1.Text = Utf8toStr(ToolEditList(si).IDCname)     'utf8 格式
      Text2.Text = Utf8toStr(ToolEditList(si).zText)  'utf8 格式
      Text3.Text = Utf8toStr(ToolEditList(si).nTips)  'utf8 格式
      for ii As Long = 0 To 4
         Option1(ii).Value = False
         Option1(ii).Enabled = True
      Next
      Check1.Enabled = True
      Check2.Enabled = True
      Check3.Enabled = True
      Check4.Enabled = True
      Check5.Enabled = True
      Check6.Enabled = True
      Option1(ToolEditList(si).tbStyle).Value = True
      Check1.Value = ToolEditList(si).AutoSize
      Check2.Value = ToolEditList(si).ShowText
      Check3.Value = ToolEditList(si).xChecked
      Check4.Value = ToolEditList(si).xEnabled
      Check5.Value = ToolEditList(si).xHidden
      Check6.Value = ToolEditList(si).xEllipses
      Dim pa As String = GetProRunFile(0,4)
      Dim bb As String = ToolEditList(si).nIco
      Dim ffi As Long = InStr(bb, "|")
      if ffi > 0 Then bb = left(bb, ffi -1)
      Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      ToolEditZ = 0
   End if
End Sub

Sub ToolEdit_Command9_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   Dim bb As String = StrToUtf8(GetImgForm(Utf8toStr(ToolEditList(si).nIco),0))  '获取图像文件名称
   If bb <> ToolEditList(si).nIco Then
      ToolEditList(si).nIco = bb
      Dim ffi As Long = InStr(bb, "|")
      if ffi > 0 Then bb = left(bb, ffi -1)
      Dim pa As String = GetProRunFile(0,4)
      Image1.Picture = pa & "images\" & Utf8toStr(bb)    'utf8 格式
      ToolEditX = 1
   End If
End Sub

Sub ToolEdit_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = UBound(ToolEditList) + 1
   ReDim Preserve ToolEditList(u)
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   Dim kk As Long = u + 1, i As Long
   Dim bb As String = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_" & kk
   if u > 0 Then  '使用名词必须唯一
      Do
         bb = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_" & kk
         for i = 1 To u
            If bb = ToolEditList(i).IDCname Then
               bb = ""
               Exit for
            End if
         Next
         if Len(bb) Then Exit Do
         kk += 1
      Loop
   End if
   ToolEditList(u).ShowText = 1
   ToolEditList(u).xEnabled = 1
   ToolEditList(u).IDCname = bb
   List1.ListIndex = List1.AddItem( vfb_LangString("[按钮]") & " " & Utf8toStr(ToolEditList(u).IDCname))
   ToolEdit_List1_LBN_SelChange 0, 0
   ToolEditX = 1
End Sub

Sub ToolEdit_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ToolEditList)
   Dim si As Long = List1.ListIndex
   if si <0 Then Return
   Swap ToolEditList(si), ToolEditList(si -1)
   Dim As CWSTR s1=List1.List(si),s2=List1.List(si-1)
   List1.List(si -1) = s1
   List1.List(si) =s2    
   List1.ListIndex = si -1
   ToolEdit_List1_LBN_SelChange 0, 0
   ToolEditX =1
End Sub

Sub ToolEdit_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ToolEditList)
   Dim si As Long = List1.ListIndex
   if si = -1 Or si >= u Then Return
   Swap ToolEditList(si), ToolEditList(si + 1)
   Dim As CWSTR s1=List1.List(si),s2=List1.List(si+1)
   List1.List(si +1) = s1
   List1.List(si) =s2    
   List1.ListIndex = si +1
   ToolEdit_List1_LBN_SelChange 0, 0
   ToolEditX =1   
End Sub

Sub ToolEdit_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim u As Long = ubound(ToolEditList) + 1
   
   ReDim Preserve ToolEditList(u)
   Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
   Dim i As Long, kk As Long = u + 1
   Dim bb As String = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_" & kk
   if u > 0 Then  '使用名词必须唯一
      Do
         bb = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_" & kk
         for i = 1 To u
            if bb = ToolEditList(i).IDCname Then
               bb = ""
               Exit for
            End if
         Next
         if Len(bb) Then Exit Do
         kk += 1
      Loop
   End if
  Dim si As Long  = List1.ListIndex
   if si = -1 Then Return
   
   List1.ListIndex = List1.InsertItem(si,  vfb_LangString("[按钮]") & " " & Utf8toStr(bb))
   
   for i = u To si + 1 Step -1
      ToolEditList(i) = ToolEditList(i -1)
   Next
   ToolEditList(si).IDCname = bb
   ToolEditList(si).zText = ""   'utf8 格式  菜单文字
   ToolEditList(si).nTips = ""    'utf8 格式  提示文字
   ToolEditList(si).tbStyle = 0    '样式 0按钮 1分割线 2下拉列表 3全下拉列表
   ToolEditList(si).nIco = ""       'utf8 格式 图标
   ToolEditList(si).AutoSize = 0
   ToolEditList(si).ShowText = 1
   ToolEditList(si).xChecked = 0
   ToolEditList(si).xEnabled = 1
   ToolEditList(si).xHidden = 0
   ToolEditList(si).xEllipses = 0
   'List1.ListIndex = si
   ToolEdit_List1_LBN_SelChange 0, 0
   ToolEditX = 1
End Sub

Sub ToolEdit_Command5_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   Select Case MessageBox(hWndForm ,vfb_LangString("你真的要删除吗？") ,"VisualFreeBasic" , _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON2 Or MB_APPLMODAL)
      Case IDYES
      Case IDNO
         Return
   End Select
   Dim u As Long = UBound(ToolEditList)
   If u = 0 Then
      Erase ToolEditList
      List1.Clear
      u = -1
   Else
      Dim si As Long = List1.ListIndex
      if si = -1 Then Return
      if si < u Then
         for i As Long = si To u -1
            ToolEditList(i) = ToolEditList(i + 1)
         Next
      End If
      u -= 1
      ReDim Preserve ToolEditList(u)
   End if
   List1.Clear
   Dim mcc(4) As String = {"[按钮] " ,"[多选] " ,"[单选] " ,"[下拉] " ,"[分割] "}
   If u > -1 Then
      For i As Long = 0 To u
         List1.AddItem mcc(ToolEditList(i).tbStyle) & " " & Utf8toStr(ToolEditList(i).IDCname)
      Next
   End If
   ToolEdit_List1_LBN_SelChange 0 ,0
   ToolEditX = 1
End Sub

Sub ToolEdit_Command13_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim ss As String = "   Select Case lpnm->hdr.idFrom" & vbCrLf
   Dim u As Long = ubound(ToolEditList)
   if u > -1 Then
      for i As Long = 0 To u
            ss &= "      Case " & Utf8toStr(ToolEditList(i).IDCname) & " ' " & Utf8toStr(ToolEditList(i).zText) & vbCrLf  & vbCrLf

      Next
   End if
   ss &= "   End Select"
   FF_ClipboardSetText ss
   
   MessageBox(hWndForm, StringToCWSTR( "已经复制 Select 代码到系统剪切板。" & vbCrLf & _
      "将其粘贴到工具栏事件中即可。"), "VisualFreeBasic", _
      MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
End Sub

Sub ToolEdit_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditX Then   SaveToolEdit '保存数据

   Me.Close    
End Sub

Sub ToolEdit_Command8_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Select Case MessageBox(hWndForm, "你真的要重置名称吗？", "VisualFreeBasic", _
            MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON2 Or MB_APPLMODAL)
      Case IDYES
      Case IDNO
         Return
   End Select
   Dim u As Long = ubound(ToolEditList)
   if u > -1 Then
      Dim mcc(4) As String = {vfb_LangString("[按钮]") ,vfb_LangString("[多选]") ,vfb_LangString("[单选]") ,vfb_LangString("[下拉]") ,vfb_LangString("[分割]")}
      Dim st As StyleFormType Ptr = Cast(Any Ptr, Me.UserData(0))
      Dim i As Long
      Dim bb As String = StrToUtf8(st->FomName & "_" & st->nName) & "_Button_"
      for i As Long = 0 To u
         ToolEditList(i).IDCname = bb & i + 1
         List1.List(i) = mcc(ToolEditList(i).tbStyle) & " " & Utf8toStr(ToolEditList(i).IDCname)
      Next
   End if
   ToolEdit_List1_LBN_SelChange 0, 0
End Sub

Sub ToolEdit_Text1_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).IDCname = CWSTRtoUTF8(Text1.Text)
   Dim mcc(4) As String = {vfb_LangString("[按钮]") ,vfb_LangString("[多选]") ,vfb_LangString("[单选]") ,vfb_LangString("[下拉]") ,vfb_LangString("[分割]")}
   List1.List(si) = mcc(ToolEditList(si).tbStyle) & " " & Utf8toStr(ToolEditList(si).IDCname)
   ToolEditX = 1
End Sub

Sub ToolEdit_Option1_BN_Clicked(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).tbStyle = ControlIndex
   Dim mcc(4) As String = {vfb_LangString("[按钮]") ,vfb_LangString("[多选]") ,vfb_LangString("[单选]") ,vfb_LangString("[下拉]") ,vfb_LangString("[分割]")}
   List1.List(si) = mcc(ToolEditList(si).tbStyle) & " " & Utf8toStr(ToolEditList(si).IDCname)   
   ToolEditX = 1
End Sub

Sub ToolEdit_Text2_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).zText = CWSTRtoUTF8(Text2.Text)
   ToolEditX = 1
End Sub

Sub ToolEdit_Text3_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).nTips = CWSTRtoUTF8(Text3.Text)
   ToolEditX = 1
End Sub

Sub ToolEdit_Check1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).AutoSize = Check1.Value 
   ToolEditX = 1
End Sub

Sub ToolEdit_Check2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).ShowText = Check2.Value 
   ToolEditX = 1
End Sub

Sub ToolEdit_Check3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).xChecked = Check3.Value 
   ToolEditX = 1
End Sub

Sub ToolEdit_Check4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).xEnabled = Check4.Value 
   ToolEditX = 1
End Sub

Sub ToolEdit_Check5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).xHidden = Check5.Value 
   ToolEditX = 1
End Sub

Sub ToolEdit_Check6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   if ToolEditZ <> 0 Then Return
   Dim si As Long = List1.ListIndex
   if si = -1 Then Return
   ToolEditList(si).xEllipses = Check6.Value 
   ToolEditX = 1
End Sub






























