Type Class_ToolBar
Private : 
   hWndControl As.HWnd '控件句柄
   m_IDC As Long     '控件IDC
Public : 
   Declare Property hWnd() As.hWnd                    '返回/设置控件句柄
   Declare Property HWnd(ByVal hWndNew As.HWnd)
   Declare Property hWndForm() As.HWnd         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
   Declare Property hWndForm(ByVal hWndParent As.HWnd)         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
   Declare Property ButtonText(idButton As uLong) As CWSTR          '返回/设置文本。idButton 按钮的命令标识符
   Declare Property ButtonText(idButton As uLong, nText As CWSTR)
   Declare Property ButtonTips(idButton As uLong) As CWSTR          '返回/设置提示文本。idButton 按钮的命令标识符
   Declare Property ButtonTips(idButton As uLong, nText As CWSTR)
   Declare Property ButtonBitmap(idButton As uLong) As Long          '返回/设置像列表中图像的从零开始的索引。idButton 按钮的命令标识符，设置为I_IMAGENONE，以指示该按钮没有图像
   Declare Property ButtonBitmap(idButton As uLong, iBitmap As Long)  '
   Declare Function ButtonCount() As Long               '返回按钮总数
   Declare Property ButtonCheck(idButton As uLong) As Boolean   '检查或取消选中工具栏中的给定按钮,idButton 按钮的命令标识符{=.True.False}
   Declare Property ButtonCheck(idButton As uLong, bValue As Boolean)
   Declare Property ButtonEnabled(idButton As uLong) As Boolean                 '返回/设置控件是否允许操作。idButton 按钮的命令标识符{=.True.False}
   Declare Property ButtonEnabled(idButton As uLong,ByVal bValue As Boolean)
   Declare Property ButtonVisible(idButton As uLong) As Boolean                 '显示或隐藏控件。idButton 按钮的命令标识符{=.True.False}
   Declare Property ButtonVisible(idButton As uLong, bValue As Boolean)
   Declare Property Enabled() As Boolean                 '返回/设置控件是否允许操作。{=.True.False}
   Declare Property Enabled(ByVal bValue As Boolean)
   Declare Property Visible() As Boolean                 '显示或隐藏控件。{=.True.False}
   Declare Property Visible(ByVal bValue As Boolean)
   Declare Property Tag() As CWSTR                      '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWSTR)
   Declare Function Left() As Long                   '返回相对于父窗口的 X（像素）
   Declare Function Top() As Long                  '返回相对于父窗口的 Y（像素）
   Declare Function Width() As Long                '返回控件宽度（像素）
   Declare Function Height() As Long               '返回控件高度（像素）
   Declare Property IDC() As Long             '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Sub SetFocus()  ' 使控件获取键盘焦点
   Declare Sub SetImgSize(imSize As Long )  ' 设置按钮图像尺寸，应该在新增按钮前设置。
   Declare Function AddButton(idCommand AS Long,ResImg As String ,zText As CWSTR, nTips As CWSTR,tbStyle As Long ,AutoSize As Long ,ShowText As Long,xChecked As Long, xEnabled As Long,xHidden As Long ,xEllipses As Long,dwData As lParam  =0) As Boolean  '向工具栏添加按钮。成功返回TRUE，否则返回FALSE。样式 {4.0 普通按钮.1 多选按钮.2 单选按钮.3 下拉列表.4 分割线}
   Declare Property UserData(idx AS LONG) As Integer      '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx As Long ,bValue As Integer)   
   
   '{代码不提示}
   Declare Sub UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
End Type


Type Class_ToolBar_Data
   idCommand AS Long     '按钮ID
   ResImg As zString *50  '资源名称
   idxBitmap AS Long      '资源在图像列表的索引
   nTips As WString *100  '提示内容
   
End Type 

Sub Class_ToolBar.UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   Dim siml As HIMAGELIST = Toolbar_GetImageList(hWndControl)
   If siml Then ImageList_Destroy siml
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
    If fp Then
       fp->nData=""
    end if    
End Sub

Property Class_ToolBar.hWnd() As.hWnd                    '句柄
  Return hWndControl
End Property
Property Class_ToolBar.HWnd(ByVal hWndNew As.HWnd)        '句柄
  hWndControl = hWndNew
  m_IDC = GetDlgCtrlID(hWndNew)
End Property
Property Class_ToolBar.IDC() As Long
  Return m_IDC
End Property
Property Class_ToolBar.IDC(NewIDC As Long)  
   m_IDC = NewIDC 
End property
Property Class_ToolBar.hWndForm() As.HWnd         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  Return GetParent(hWndControl)
End Property
Property Class_ToolBar.hWndForm(ByVal hWndParent As.HWnd)         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  hWndControl = GetDlgItem(hWndParent,m_IDC)
End Property
Property Class_ToolBar.Tag() As CWSTR
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Return fp->Tag
   End If
End Property
Property Class_ToolBar.Tag(ByVal sText As CWSTR)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->Tag = sText
   End If
End Property
Function Class_ToolBar.Left() As Long                '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
  Return rc.Left
End Function

Function Class_ToolBar.Top() As Long     '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc     '获得窗体大小
  MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
  Return rc.Top
End Function

Function Class_ToolBar.Height() As Long                  '
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  Return  rc.Bottom - rc.Top
End Function

Function Class_ToolBar.Width() As Long
  Dim rc As Rect
  GetWindowRect hWndControl, @rc          '获得窗体大小
  Return rc.Right - rc.Left
End Function

Property Class_ToolBar.Visible() As Boolean                 '可见
  Return IsWindowVisible(hWndControl)
End Property
Property Class_ToolBar.Visible(ByVal bValue As Boolean)
  If bValue Then
      ShowWindow(hWndControl, SW_SHOW)
  Else
      ShowWindow(hWndControl, SW_HIDE)
  End If
End Property
Property Class_ToolBar.Enabled() As Boolean                 '使能
  Return IsWindowEnabled(hWndControl)
End Property
Property Class_ToolBar.Enabled(ByVal bValue As Boolean)
  EnableWindow(hWndControl, bValue)
End Property
Property Class_ToolBar.ButtonText(idButton As uLong) As CWSTR                  '返回/设置文本。
   Dim nLen As Long = SendMessageW( hWndControl , TB_GETBUTTONTEXT , idButton , NULL )   '默认为 SendMessageW(hWndControl, idButton)
   if nLen=0 Then Return ""
   ReDim by(nLen * 2 + 2) As Byte 
   ToolBar_GetButtonText(hWndControl, idButton,Cast(WSTRING PTR,@by(0)))
   Return  *CPtr(WString Ptr ,@by(0))
End Property
Property Class_ToolBar.ButtonText(idButton As uLong, nText As CWSTR)
   Dim tb As TBBUTTONINFOW
   tb.cbSize = SizeOf(TBBUTTONINFOW)
   tb.dwMask = TBIF_COMMAND Or TBIF_TEXT
   tb.idCommand = idButton
   tb.pszText = nText.vptr
   tb.cchText = Len(nText) * 2
   ToolBar_SetButtonInfo (hWndControl, idButton,@tb)
   
End Property
Property Class_ToolBar.ButtonTips(idButton As uLong) As CWSTR                  '返回/设置文本。
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim u As Long = Len(fp->nData) / SizeOf(Class_ToolBar_Data)
      if u > 0 Then
         Dim i As Long ,td As Class_ToolBar_Data Ptr = Cast(Any Ptr ,StrPtr(fp->nData))
         For i = 0 To u -1
            if td[i].idCommand = idButton Then
               Return td[i].nTips
            End if
         Next
      End if
   End If
End Property
Property Class_ToolBar.ButtonTips(idButton As uLong ,nText As CWSTR)
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim u As Long = Len(fp->nData) / SizeOf(Class_ToolBar_Data)
      if u > 0 Then
         Dim i As Long ,td As Class_ToolBar_Data Ptr = Cast(Any Ptr ,StrPtr(fp->nData))
         For i = 0 To u -1
            if td[i].idCommand = idButton Then
               td[i].nTips = nText
               Exit For
            End if
         Next
      End if
   End If
End Property
Property Class_ToolBar.ButtonBitmap(idButton As uLong) As Long
  Return ToolBar_GetBitmap(hWndControl,idButton)
End Property
Property Class_ToolBar.ButtonBitmap(idButton As uLong, iBitmap As Long)
   Dim tb As TBBUTTONINFOW
   tb.cbSize = SizeOf(TBBUTTONINFOW)
   tb.dwMask = TBIF_COMMAND Or TBIF_IMAGE
   tb.idCommand = idButton
   tb.iImage = iBitmap
   ToolBar_SetButtonInfo (hWndControl, idButton,@tb)
End Property
Property Class_ToolBar.ButtonCheck(idButton As uLong) As Boolean
  Return ToolBar_IsButtonChecked(hWndControl, idButton)
End Property
Property Class_ToolBar.ButtonCheck(idButton As uLong, bValue As Boolean)
   ToolBar_CheckButton hWndControl, idButton, bValue
End Property

Function Class_ToolBar.ButtonCount() As Long
   Function=ToolBar_ButtonCount(hWndControl)
End Function
Property Class_ToolBar.ButtonEnabled(idButton As uLong) As Boolean 
   Return ToolBar_IsButtonEnabled(hWndControl, idButton)
End Property
Property Class_ToolBar.ButtonEnabled(idButton As uLong,ByVal bValue As Boolean)
   if bValue Then 
      ToolBar_EnableButton(hWndControl, idButton)
   Else
      ToolBar_DisableButton(hWndControl, idButton)
   End if 
End Property
Property Class_ToolBar.ButtonVisible(idButton As uLong) As Boolean 
   Return ToolBar_IsButtonHidden(hWndControl, idButton)
End Property
Property Class_ToolBar.ButtonVisible(idButton As uLong, bValue As Boolean)
   ToolBar_HideButton (hWndControl, idButton,bValue)
End Property
Sub Class_ToolBar.SetFocus()  ' 获取键盘焦点
     .SetFocus hWndControl
End Sub
Sub Class_ToolBar.SetImgSize(imSize As Long)  ' 设置按钮图像尺寸（像素单位），应该在新增按钮前设置。
   Dim siml As HIMAGELIST = ToolBar_GetImageList(hWndControl)
   if siml Then ImageList_Destroy siml
   siml = ImageList_Create(imSize, imSize, ILC_COLOR32, 10, 2)
   ImageList_SetBkColor siml,GetSysColor(COLOR_BTNFACE)
   SendMessage(hWndControl, TB_SETIMAGELIST, 0, cast(lParam, siml))   '默认为 SendMessageW
End Sub
Function Class_ToolBar.AddButton(idCommand As Long ,ResImg As String ,zText As CWSTR ,nTips As CWSTR ,tbStyle As Long ,AutoSize As Long ,ShowText As Long ,xChecked As Long ,xEnabled As Long ,xHidden As Long ,xEllipses As Long ,dwData As lParam = 0) As Boolean
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->nData &= String(SizeOf(Class_ToolBar_Data) ,0)
      Dim u  As Long                   = Len(fp->nData) / SizeOf(Class_ToolBar_Data)
      dim ti As Long                   = u -1 ,i As Long
      Dim td As Class_ToolBar_Data Ptr = Cast(Any Ptr ,StrPtr(fp->nData))
      td[ti].idCommand = idCommand
      td[ti].ResImg    = ResImg
      td[ti].nTips     = nTips
      td[ti].idxBitmap = -1
      if u > 1 Then
         For i = 0 To u -2
            if td[i].ResImg = ResImg Then
               td[ti].idxBitmap = td[i].idxBitmap
               Exit For
            end if
         Next
      End if
      if td[ti].idxBitmap = -1 Then
         Dim siml  As HIMAGELIST = ToolBar_GetImageList(hWndControl)
         Dim nIcon As HICON
         If .Left(ResImg ,7) = "BITMAP_" Then
            Dim nBmp As HBITMAP = LoadImageA(App.HINSTANCE ,ResImg ,IMAGE_BITMAP ,0 ,0 ,LR_DEFAULTCOLOR)
            Dim po   As ICONINFO
            po.fIcon    = TRUE
            po.hbmColor = nBmp
            po.hbmMask  = nBmp
            nIcon       = CreateIconIndirect(@po)
            DeleteObject nBmp
         ElseIf .Left(ResImg ,5) = "ICON_" Or ResImg = "AAAAA_APPICON" Then
            nIcon = LoadImageA(App.HINSTANCE ,ResImg ,IMAGE_ICON ,0 ,0 ,LR_DEFAULTCOLOR) '从资源里加载图标
         Else
            nIcon                  = AfxGdipIconFromRes(App.HINSTANCE ,ResImg)   
         End If
         td[ti].idxBitmap = ImageList_ReplaceIcon(siml , -1 ,nIcon)
         DestroyIcon nIcon
      End if
      Dim ppn As TBBUTTON ,ppm As WString * 20
      if tbStyle = 4 Then
         'Toolbar_AddSeparator hWndControl
         ppn.fsStyle = TBSTYLE_SEP
      Else
         Dim As ULong fsState ,fSStyle
         if xChecked <> 0  Then fsState Or= TBSTATE_CHECKED
         if xEnabled <> 0  Then fsState Or= TBSTATE_ENABLED
         if xHidden <> 0   Then fsState Or= TBSTATE_HIDDEN
         if xEllipses <> 0 Then fsState Or= TBSTATE_ELLIPSES
         Select Case tbStyle
            Case 0    : fSStyle = BTNS_BUTTON
            Case 1    : fSStyle = BTNS_CHECK
            Case 2    : fSStyle = BTNS_CHECKGROUP
            Case 3    : fSStyle = BTNS_DROPDOWN
            Case Else : fSStyle = BTNS_BUTTON
         End Select
         if AutoSize <> 0 Then fSStyle Or= BTNS_AUTOSIZE
         if ShowText <> 0 Then fSStyle Or= BTNS_SHOWTEXT
         'Function = Toolbar_AddButton(hWndControl, td[ti].idxBitmap, idCommand, fsState, fSStyle, 0, zText)
         ppn.iBitmap   = td[ti].idxBitmap
         ppn.idCommand = idCommand
         ppn.fsState   = fsState
         ppn.fsStyle   = fsStyle
         ppn.dwData    = dwData
         ppn.iString   = Cast(INT_PTR ,zText.vptr)
      End if
      Function = SendMessageW(hWndControl ,TB_ADDBUTTONS ,1 ,Cast(lParam ,@ppn)) '默认为 SendMessageW
   End If

End Function
Property Class_ToolBar.UserData(idx AS LONG) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return 0
   Dim fp As FormControlsPro_TYPE ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp  Then
      Return fp->UserData(idx)
   End If   

End Property
Property Class_ToolBar.UserData(idx AS LONG, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return 
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp  Then
      fp->UserData(idx) = bValue
   End If    
End Property

