'控件类
Type Class_YFsplitBar Extends Class_Control
   Private : '私有区
   TabMouse As Long '鼠标在那个按钮上，0表示不在按钮上，1开始在按钮上。
   TabPress As Long '鼠标是不是按下
   sPos     As Long '开始拖动时的位置
   'Declare Function Tab_GetAn(xPos As Long ,yPos As Long) As Long '根据鼠标位置，返回按钮，0表示不在按钮上，1开始在按钮上。
   Declare Sub TabPainting(vhWnd As .hWnd) ' 绘画（控件自用）
   Public : '公共区
   Declare Property Value() As Long '返回或设置当前值
   Declare Property Value(bValue As Long) '给属性赋值
   '{代码不提示}
   Declare Function MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult '消息处理（控件自用）
   '{代码可提示}
End Type

Function Class_YFsplitBar.MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult
   Dim fp As FormControlsPro_TYPE Ptr
   Select Case wMsg
      Case WM_PAINT
         This.TabPainting(vhWnd)
         Return True
      Case WM_USER + 200 '预防多线程操作控件，需要发消息处理
         
         'Return jj
      Case WM_MOUSEMOVE
         '移出控件侦察
         Dim entTrack As tagTRACKMOUSEEVENT
         entTrack.cbSize      = SizeOf(tagTRACKMOUSEEVENT)
         entTrack.dwFlags     = TME_LEAVE
         entTrack.hwndTrack   = vhWnd
         entTrack.dwHoverTime = HOVER_DEFAULT
         TrackMouseEvent @entTrack
         '处理移动
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         If TabPress Then
            If MouseFlags = 1 Then
               Dim ww As Long ,hh As Long ,nw As Long = AfxScaleX(fp->nWidth) ,nh As Long = AfxScaleY(fp->nHeight)
               Dim rc As Rect
               GetClientRect(GetParent(vhWnd) ,@rc)
               ww = (rc.Right - rc.Left)
               hh = (rc.Bottom - rc.Top)
               If fp->nWidth < fp->nHeight Then
                  Dim bb As Short = xPos
                  Dim aa As Long  = bb                   - sPos
                  Dim ee As Long  = AfxScaleX(fp->nLeft) + aa
                  If ee < 0       Then ee = 0
                  If ee > ww - nw Then ee = ww - nw
                  If SendMessage(vhWnd ,WM_USER + 102 ,AfxScaleX(fp->nLeft) ,ee) = 0 Then
                     SetWindowPos(vhWnd ,HWND_TOP ,ee ,AfxScaleY(fp->nTop) ,0 ,0 ,SWP_NOSIZE Or SWP_NOACTIVATE)
                     fp->nLeft = AfxUnscaleX(ee)
                  End If
               Else
                  Dim bb As Short = yPos
                  Dim aa As Long  = bb                  - sPos
                  Dim ee As Long  = AfxScaleY(fp->nTop) + aa
                  If ee < 0       Then ee = 0
                  If ee > hh - nh Then ee = hh - nh
                  If SendMessage(vhWnd ,WM_USER + 102 ,AfxScaleY(fp->nTop) ,ee) = 0 Then
                     SetWindowPos(vhWnd ,HWND_TOP ,AfxScaleX(fp->nLeft) ,ee ,0 ,0 ,SWP_NOSIZE Or SWP_NOACTIVATE)
                     fp->nTop = AfxUnscaleY(ee)
                  End If
               End If
            End If
         End If
         FF_Redraw vhWnd
      Case WM_MOUSELEAVE '处理移出
         TabPress = 0
         FF_Redraw vhWnd
      Case WM_LBUTTONDOWN
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         sPos = IIf(fp->nWidth < fp->nHeight ,xPos ,yPos)
         SetWindowPos(vhWnd ,HWND_TOP ,0 ,0 ,0 ,0 ,SWP_NOSIZE Or SWP_NOACTIVATE Or SWP_NOMOVE)
         SendMessage(vhWnd ,WM_USER + 101 ,0 ,AfxScaleX(IIf(fp->nWidth < fp->nHeight ,fp->nLeft ,fp->nTop))) ' 产生事件
         ReleaseCapture '取消鼠标绑定控件
         SetCapture vhWnd '绑定鼠标控件，让鼠标按下拖动到控件外面也能有鼠标移动消息
         TabPress = 1
         FF_Redraw vhWnd
         
      Case WM_LBUTTONUP
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         ReleaseCapture '取消鼠标绑定控件
         TabPress = 0
         SendMessage(vhWnd ,WM_USER + 103 ,0 ,AfxScaleY(IIf(fp->nWidth < fp->nHeight ,fp->nLeft ,fp->nTop))) ' 产生事件
         FF_Redraw vhWnd '刷新控件
         
      Case WM_SETCURSOR
         If LoWord(nlParam) = 1 Then '表示是窗口客户区
            fp = vfb_Get_Control_Ptr(vhWnd)
            If fp = 0 Then Return 0
            If fp->nWidth < fp->nHeight Then
               SetCursor(LoadCursor(NULL ,IDC_SIZEWE))
            Else
               SetCursor(LoadCursor(Null ,IDC_SIZENS))
            End If
            Return True ' *要注意的是，当设置了鼠标光标以后，您应该让函数返回 TRUE 以防止系统再作缺省处理。
         End If
      Case WM_NOTIFY
         '为了工具提示时，鼠标指针挡住了提示文字，因此需要我们自己调整显示位置。
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         If fp->ToolTipBalloon = 0 Then
            Dim nn As NMHDR Ptr = Cast(Any Ptr ,nlParam)
            Select Case nn->code
               Case TTN_SHOW '工具提示时，设置显示位置
                  Dim A As Point
                  GetCursorPos @a
                  SetWindowPos(nn->hwndFrom ,NULL ,A.x ,A.y + GetSystemMetrics(SM_CYCURSOR) ,0 ,0 ,SWP_NOSIZE Or SWP_NOZORDER Or SWP_NOACTIVATE)
                  Return True
            End Select
         End If
   End Select
   Function = 0
End Function
Sub Class_YFsplitBar.TabPainting(vhWnd As .hWnd) ' 绘画
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return
   Dim cc As Long =GetCodeColorGDI(IIf(TabPress,fp->ForeColor,fp->BackColor ))
   Dim gg As yGDI                     = yGDI(vhWnd ,cc ,True) 'WM_PAINT事件里必须用这行初始化 yGDI   
   
End Sub    
Property Class_YFsplitBar.Value() As Long '返回属性
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return 0
   If fp->nWidth < fp->nHeight Then
      Property = AfxScaleX(fp->nLeft)
   Else
      Property = AfxScaleY(fp->nTop)
   End If
   
End Property
Property Class_YFsplitBar.Value(bValue As Long) '给属性赋值
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return
   Dim ww As Long ,hh As Long ,nw As Long = AfxScaleX(fp->nWidth) ,nh As Long = AfxScaleY(fp->nHeight)
   Dim rc As Rect
   GetClientRect(GetParent(hWndControl) ,@rc)
   ww = (rc.Right - rc.Left)
   hh = (rc.Bottom - rc.Top)
   If fp->nWidth < fp->nHeight Then
      Dim ee As Long = bValue
      If ee < 0       Then ee = 0
      If ee > ww - nw Then ee = ww - nw
      If SendMessage(hWndControl ,WM_USER + 102 ,AfxScaleX(fp->nLeft) ,ee) = 0 Then
         SetWindowPos(hWndControl ,HWND_TOP ,ee ,AfxScaleY(fp->nTop) ,0 ,0 ,SWP_NOSIZE Or SWP_NOACTIVATE)
         fp->nLeft = AfxUnscaleX(ee)
      End If
   Else
      Dim ee As Long = bValue
      If ee < 0       Then ee = 0
      If ee > hh - nh Then ee = hh - nh
      If SendMessage(hWndControl ,WM_USER + 102 ,AfxScaleX(fp->nTop) ,ee) = 0 Then
         SetWindowPos(hWndControl ,HWND_TOP ,AfxScaleX(fp->nLeft) ,ee ,0 ,0 ,SWP_NOSIZE Or SWP_NOACTIVATE)
         fp->nTop = AfxUnscaleY(ee)
      End If
   End If
End Property
