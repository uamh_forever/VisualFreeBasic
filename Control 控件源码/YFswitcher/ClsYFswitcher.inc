'控件类
'注意： hWnd 成员存在 Class_Control 类里，因此使用 API的类型 hWnd 需要前面加个点 .hWnd

Type Class_YFswitcher Extends Class_Control
   Private : '私有区
   kMouse As Long '鼠标在那个按钮上，0表示不在按钮上，1开始在按钮上。
   kPress As Long '鼠标是不是按下
   Declare Function GetAn(xPos As Long ,yPos As Long) As Long '根据鼠标位置，返回按钮，0表示不在按钮上，1开始在按钮上。
   Declare Sub Painting(vhWnd As .hWnd) ' 绘画（控件自用）
   
   Public : '公共区
   Declare Property Value() As BOOLEAN '返回/设置 开启或关闭状态。{=.True.False}
   Declare Property Value(ByVal nCheckState As BOOLEAN)
   Declare Property Style() As Long '返回/设置开关画面，假如有图像，就显示图像，没图像按这里的类型用代码画。{=.0 - 标准.1 - 按钮}
   Declare Property Style(bValue As Long)
   Declare Property NoteON() As String '返回/设置 在控件开状态时显示的注释，有图像时不使用
   Declare Property NoteON(bValue As String)
   Declare Property NoteOFF() As String '返回/设置 在控件关状态时显示的注释，有图像时不使用
   Declare Property NoteOFF(bValue As String)
   Declare Property ImgON() As String '返回/设置 在控件开状态时显示的图像，1：是文件（带路径全称的文件），就读文件显示 2：图像库里的资源名称，显示资源图像
   Declare Property ImgON(bValue As String)
   Declare Property ImgOFF() As String '返回/设置 在控件关状态时显示的图像，1：是文件（带路径全称的文件），就读文件显示 2：图像库里的资源名称，显示资源图像
   Declare Property ImgOFF(bValue As String)
   Declare Property NoteONColor() As Long '返回/设置开状态的注释文字色：设置用：BGR(r, g, b)
   Declare Property NoteONColor(nColor As Long)
   Declare Property NoteOFFColor() As Long '返回/设置关状态的注释文字色：设置用：BGR(r, g, b)
   Declare Property NoteOFFColor(nColor As Long)
   Declare Property FillONColor() As Long '返回/设置开状态的填充色，-1时不使用：设置用：BGR(r, g, b)
   Declare Property FillONColor(nColor As Long)
   Declare Property FillOFFColor() As Long '返回/设置关状态的填充色，-1时不使用：设置用：BGR(r, g, b)
   Declare Property FillOFFColor(nColor As Long)
   Declare Property LineONColor() As Long '返回/设置开状态的线条色，-1时不使用：设置用：BGR(r, g, b)
   Declare Property LineONColor(nColor As Long)
   Declare Property LineOFFColor() As Long '返回/设置关状态的线条色，-1时不使用：设置用：BGR(r, g, b)
   Declare Property LineOFFColor(nColor As Long)
   Declare Property LumpONColor() As Long '返回/设置开状态的块状色，-1时不使用：设置用：BGR(r, g, b)
   Declare Property LumpONColor(nColor As Long)
   Declare Property LumpOFFColor() As Long '返回/设置关状态的块状色，-1时不使用：设置用：BGR(r, g, b)
   Declare Property LumpOFFColor(nColor As Long)
   
   '{代码不提示}
   Declare Function MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult '消息处理（控件自用）
   Declare Constructor '创建时
   Declare Sub UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   '{代码可提示}
End Type
'fp->CtlData(0)  0=关闭 非0=开启
'fp->nText  开图像|关图像|开注释|关注释
'fp->CtlData(1) 开状态的注释文字色
'fp->CtlData(2) 关状态的注释文字色
'fp->CtlData(3) 开状态的填充色
'fp->CtlData(4) 关状态的填充色
'fp->CtlData(5) 开状态的线条色
'fp->CtlData(6) 关状态的线条色
'fp->CtlData(7) 开状态的块状色
'fp->CtlData(8) 关状态的块状色

Declare Sub Class_YFswitcher_Animation(vhWnd As .hWnd )  '动画过程 
Declare Sub Class_YFswitcher_DrawAnImage(gg As yGDI ,sImg As String ,ww As Long ,hh As Long ,nBackColor As Long,transparency As Long ) ' 描绘图片

Constructor Class_YFswitcher
   '注意：由于窗口类是永久的，全局变量，因此这里从开软件到关软件只执行1次。
End Constructor
Sub Class_YFswitcher.UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      '1，每次窗口销毁  就执行1次
   End If
End Sub   

Function Class_YFswitcher.MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult
   Dim fp As FormControlsPro_TYPE Ptr
   Select Case wMsg
      Case WM_PAINT
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0         
         If SendMessage(vhWnd ,WM_USER + 103 ,fp->CtlData(0) ,IsWindowEnabled(vhWnd)) = 0 Then This.Painting(vhWnd)
         Return True
      Case WM_USER + 200 '预防多线程操作控件，需要发消息处理
         
         'Return jj
      Case WM_MOUSEMOVE
         '移出控件侦察
         Dim entTrack As tagTRACKMOUSEEVENT
         entTrack.cbSize      = SizeOf(tagTRACKMOUSEEVENT)
         entTrack.dwFlags     = TME_LEAVE
         entTrack.hwndTrack   = vhWnd
         entTrack.dwHoverTime = HOVER_DEFAULT
         TrackMouseEvent @entTrack
         '处理移动
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(xPos ,yPos)
         Dim bb As Long = MouseFlags = 1
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            FF_Redraw vhWnd
         End If
         
      Case WM_MOUSELEAVE '处理移出
         kMouse = 0
         kPress = 0
         FF_Redraw vhWnd
      Case WM_LBUTTONDOWN
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(xPos ,yPos)
         Dim bb As Long = True
         ReleaseCapture '取消鼠标绑定控件
         SetCapture vhWnd '绑定鼠标控件，让鼠标按下拖动到控件外面也能有鼠标移动消息
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            If SendMessage(vhWnd ,WM_USER + 102 ,0 ,fp->CtlData(0) = 0) = 0 Then '即将改变
               fp->CtlData(0) = fp->CtlData(0) = 0
               Threaddetach ThreadCreate(Cast(Any Ptr,@Class_YFswitcher_Animation),vhWnd) '经典调用方法
               SendMessage(vhWnd ,WM_USER + 101 ,0 ,fp->CtlData(0))
               'FF_Redraw vhWnd
            End If
         End If
      Case WM_LBUTTONUP
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         ReleaseCapture '取消鼠标绑定控件
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(xPos ,yPos)
         Dim bb As Long
         If aa = kMouse Then
            Select Case aa
               Case 1 '触发按钮事件
               Case 2
            End Select
         End If
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            
            'FF_Redraw vhWnd
         End If
         
      Case WM_NOTIFY
         '为了工具提示时，鼠标指针挡住了提示文字，因此需要我们自己调整显示位置。
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         If fp->ToolTipBalloon = 0 Then
            Dim nn As NMHDR Ptr = Cast(Any Ptr ,nlParam)
            Select Case nn->code
               Case TTN_SHOW '工具提示时，设置显示位置
                  Dim A As Point
                  GetCursorPos @a
                  SetWindowPos(nn->hwndFrom ,NULL ,A.x ,A.y + GetSystemMetrics(SM_CYCURSOR) ,0 ,0 ,SWP_NOSIZE Or SWP_NOZORDER Or SWP_NOACTIVATE)
                  Return True
            End Select
         End If
   End Select
   Function = 0
End Function
Sub Class_YFswitcher.Painting(vhWnd As .hWnd) ' 绘画
   
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(vhWnd)
   If fp = 0 Then Return
   Dim BColor As Long = GetCodeColorGDI(fp->BackColor) '背景色
   Dim fColor As Long = GetCodeColorGDI(fp->ForeColor) '前景色
   Dim vv     As Long = fp->CtlData(0)                 ' 0=关闭 非0=开启
   Dim en     As Long = IsWindowEnabled(vhWnd)
   Dim rr()   As String
   If vbSplit(fp->nText ,"|" ,rr()) <> 3 Then ReDim Preserve rr(3)
   Dim imgK      As String = rr(0)          '  开图像
   Dim imgG      As String = rr(1)          '  关图像
   Dim zhuK      As String = rr(2)          '  开注释
   Dim zhuG      As String = rr(3)          '  关注释
   Dim zhuKcolor As Long   = fp->CtlData(1) '开状态的注释文字色
   Dim zhuGcolor As Long   = fp->CtlData(2) '关状态的注释文字色
   Dim toumindu  As Long   = IIf(en ,&HFF ,&H4D)
   If kMouse Then
      If en Then
         toumindu = &HDd
         
      End If
   End If
   Dim tianKcolor As Long = ColorGdiToGDIplue(fp->CtlData(3) ,toumindu) '开状态的填充色
   Dim tianGcolor As Long = ColorGdiToGDIplue(fp->CtlData(4) ,toumindu) '关状态的填充色
   Dim xianKcolor As Long = ColorGdiToGDIplue(fp->CtlData(5) ,toumindu) '开状态的线条色
   Dim xianGcolor As Long = ColorGdiToGDIplue(fp->CtlData(6) ,toumindu) '关状态的线条色
   Dim kuanKcolor As Long = ColorGdiToGDIplue(fp->CtlData(7) ,toumindu) '开状态的块状色
   Dim kuanGcolor As Long = ColorGdiToGDIplue(fp->CtlData(8) ,toumindu) '关状态的块状色
   
   Dim gg  As yGDI   = yGDI(vhWnd ,BColor ,True) 'WM_PAINT事件里必须用这行初始化 yGDI
   Dim ww  As Long   = gg.m_Width -1 ,hh As Long = gg.m_Height -1
   Dim dpi As Single = gg.Dpi ,kk As Single
   gg.dpi = 1
   
   If vv <> 0 And Len(imgK) Then
      Class_YFswitcher_DrawAnImage(gg ,imgK ,ww + 1 ,hh + 1 ,BColor,toumindu)
   ElseIf vv = 0 And Len(imgG) Then
      Class_YFswitcher_DrawAnImage(gg ,imgG ,ww + 1 ,hh + 1 ,BColor,toumindu)
   Else
      Select Case fp->Style
         Case 0 ,1 ,4 ,5 '平面圆角  ---------------------------
            gg.GpBrush IIf(vv ,tianKcolor ,tianGcolor)
            gg.GpPen IIf(vv ,IIf(xianKcolor = -1 ,0 ,1) ,IIf(xianGcolor = -1 ,0 ,1)) ,IIf(vv ,xianKcolor ,xianGcolor)
            If ww >= hh Then
               '水平
               If fp->Style < 4 Then
                  gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,hh ,hh)
               Else
                  gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,3 ,3)
               End If
               kk = hh / 16
               If (vv <> 0 And Len(zhuK)) Or (vv = 0 And Len(zhuG)) Then
                  gg.SetColor IIf(vv ,zhuKcolor ,zhuGcolor)
                  gg.Font( ,hh * 0.6 / 96 * 72 ,True)
                  If vv Then
                     gg.DrawTextS(0 ,0 ,ww - (hh - kk) ,hh ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  Else
                     gg.DrawTextS(hh ,0 ,ww - (hh - kk) ,hh ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  End If
               End If
               gg.GpPen 0 ,0
               gg.GpBrush IIf(vv ,kuanKcolor ,kuanGcolor)
               If fp->Style < 4 Then
                  gg.GpDrawEllipse(IIf(vv ,ww - (hh - kk) ,kk) ,kk ,hh -2 *kk ,hh -2 *kk)
               Else
                  gg.GpDrawCircleFrame(IIf(vv ,ww - (hh - kk) ,kk) ,kk ,hh -2 *kk ,hh -2 *kk ,3 ,3)
               End If
               If fp->Style = 1 Or fp->Style = 5 Then
                  gg.GpBrush IIf(vv ,tianKcolor ,tianGcolor)
                  If fp->Style = 1 Then
                     gg.GpDrawEllipse(IIf(vv ,ww - (hh - kk * 4) ,kk * 4) ,kk * 4 ,hh -8 *kk ,hh -8 *kk)
                  Else
                     gg.GpDrawCircleFrame(IIf(vv ,ww - (hh - kk * 4) ,kk * 4) ,kk * 4 ,hh -8 *kk ,hh -8 *kk ,3 ,3)
                  end if
               End If
            Else
               '垂直
               If fp->Style < 4 Then
                  gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,ww ,ww)
               Else
                  gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,3 ,3)
               End If
               kk = ww / 16
               If (vv <> 0 And Len(zhuK)) Or (vv = 0 And Len(zhuG)) Then
                  gg.SetColor IIf(vv ,zhuKcolor ,zhuGcolor)
                  gg.Font( ,ww * 0.8 / Len(zhuK) / 96 * 72 ,True)
                  If vv Then
                     gg.DrawTextS(0 ,0 ,ww ,hh - (ww - kk) ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  Else
                     gg.DrawTextS(0 ,ww ,ww ,hh - (ww - kk) ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  End If
               End If
               gg.GpPen 0 ,0
               gg.GpBrush IIf(vv ,kuanKcolor ,kuanGcolor)
               If fp->Style < 4 Then
                  gg.GpDrawEllipse(kk ,IIf(vv ,hh - (ww - kk) ,kk) ,ww -2 *kk ,ww -2 *kk)
               else
                  gg.GpDrawCircleFrame(kk ,IIf(vv ,hh - (ww - kk) ,kk) ,ww -2 *kk ,ww -2 *kk ,3 ,3)
               End If
               If fp->Style = 1 Or fp->Style = 5 Then
                  gg.GpBrush IIf(vv ,tianKcolor ,tianGcolor)
                  If fp->Style = 1 Then
                     gg.GpDrawEllipse(kk * 4 ,IIf(vv ,hh - (ww - kk * 4) ,kk * 4) ,ww -8 *kk ,ww -8 *kk)
                  Else
                     gg.GpDrawCircleFrame(kk * 4 ,IIf(vv ,hh - (ww - kk * 4) ,kk * 4) ,ww -8 *kk ,ww -8 *kk ,3 ,3)
                  End If
               End If
            End If
         Case 2 ,3 '平面圆角  ---------------------------
            gg.GpBrush IIf(vv ,tianKcolor ,tianGcolor)
            gg.GpPen IIf(vv ,IIf(xianKcolor = -1 ,0 ,1) ,IIf(xianGcolor = -1 ,0 ,1)) ,IIf(vv ,xianKcolor ,xianGcolor)
            If ww >= hh Then
               '水平
               gg.GpDrawCircleFrame(1 ,hh * 0.2 ,ww -2 ,hh * 0.6 ,hh * 0.6 ,hh * 0.6)
               kk = hh / 16
               If (vv <> 0 And Len(zhuK)) Or (vv = 0 And Len(zhuG)) Then
                  gg.SetColor IIf(vv ,zhuKcolor ,zhuGcolor)
                  gg.Font( ,hh * 0.4 / 96 * 72 ,True)
                  If vv Then
                     gg.DrawTextS(0 ,0 ,ww - (hh - kk) ,hh ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  Else
                     gg.DrawTextS(hh ,0 ,ww - (hh - kk) ,hh ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  End If
               End If
               gg.GpPen 0 ,0
               gg.GpBrush IIf(vv ,kuanKcolor ,kuanGcolor)
               gg.GpDrawEllipse(IIf(vv ,ww - hh ,0) ,0 ,hh ,hh)
               If fp->Style = 3 Then
                  gg.GpBrush IIf(vv ,tianKcolor ,tianGcolor)
                  gg.GpDrawEllipse(IIf(vv ,ww - (hh - kk * 3) ,kk * 3) ,kk * 3 ,hh -6 *kk ,hh -6 *kk)
               End If
            Else
               '垂直
               gg.GpDrawCircleFrame(ww * 0.2 ,1 ,ww * 0.6 ,hh -2 ,ww * 0.6 ,ww * 0.6)
               kk = ww / 16
               If (vv <> 0 And Len(zhuK)) Or (vv = 0 And Len(zhuG)) Then
                  gg.SetColor IIf(vv ,zhuKcolor ,zhuGcolor)
                  gg.Font( ,ww * 0.3 / Len(zhuK) / 96 * 72 ,True)
                  If vv Then
                     gg.DrawTextS(0 ,0 ,ww ,hh - (ww - kk) ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  Else
                     gg.DrawTextS(0 ,ww ,ww ,hh - (ww - kk) ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  End If
               End If
               gg.GpPen 0 ,0
               gg.GpBrush IIf(vv ,kuanKcolor ,kuanGcolor)
               gg.GpDrawEllipse(0 ,IIf(vv ,hh - ww ,0) ,ww ,ww)
               If fp->Style = 3 Then
                  gg.GpBrush IIf(vv ,tianKcolor ,tianGcolor)
                  gg.GpDrawEllipse(kk * 3 ,IIf(vv ,hh - (ww - kk * 3) ,kk * 3) ,ww -6 *kk ,ww -6 *kk)
               End If
            End If
         Case 6 ,7
            gg.GpBrush tianGcolor
            gg.GpPen IIf(xianGcolor = -1 ,0 ,1),xianGcolor
            '水平
            If fp->Style = 6 Then
               gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,hh ,hh)
            Else
               gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,3 ,3)
            End If
            kk = hh / 16
            gg.GpPen IIf(xianKcolor = -1 ,0 ,1) ,xianKcolor 
            gg.GpBrush tianKcolor
            If vv Then
               If fp->Style = 6 Then
                  gg.GpDrawCircleFrame(ww/2 ,0 ,ww/2 ,hh ,hh ,hh,4 Or 8)
               Else
                  gg.GpDrawCircleFrame(ww/2 ,0 ,ww/2 ,hh ,3 ,3,4 Or 8)
               End If
            Else
                If fp->Style = 6 Then
                  gg.GpDrawCircleFrame(0 ,0 ,ww/2 ,hh ,hh ,hh,1 Or 2)
               Else
                  gg.GpDrawCircleFrame(0 ,0 ,ww/2 ,hh ,3 ,3,1 Or 2)
               End If              
            End If
            If Len(zhuK) Then
               gg.SetColor IIf(vv ,zhuKcolor ,zhuGcolor)
               gg.Font( ,hh * 0.6 / 96 * 72 ,True)
               gg.DrawTextS(ww /2,0 ,ww /2 ,hh ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
            End If
            If Len(zhuG) Then
               gg.SetColor IIf(vv = 0 ,zhuKcolor ,zhuGcolor)
               gg.Font( ,hh * 0.6 / 96 * 72 ,True)
               gg.DrawTextS(0  ,0 ,ww /2,hh ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
            End If
      End Select
   End If
   
End Sub
Sub Class_YFswitcher_Animation(vhWnd As .hWnd) '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(vhWnd)
   If fp <> 0 And IsWindowEnabled(vhWnd) <> 0 Then
      
      Dim BColor As Long = GetCodeColorGDI(fp->BackColor) '背景色
      Dim fColor As Long = GetCodeColorGDI(fp->ForeColor) '前景色
      Dim vv     As Long = fp->CtlData(0)                 ' 0=关闭 非0=开启
      Dim rr()   As String
      If vbSplit(fp->nText ,"|" ,rr()) <> 3 Then ReDim Preserve rr(3)
      Dim imgK       As String = rr(0)                             '  开图像
      Dim imgG       As String = rr(1)                             '  关图像
      Dim zhuK       As String = rr(2)                             '  开注释
      Dim zhuG       As String = rr(3)                             '  关注释
      Dim zhuKcolor  As Long   = fp->CtlData(1)                    '开状态的注释文字色
      Dim zhuGcolor  As Long   = fp->CtlData(2)                    '关状态的注释文字色
      Dim tianKcolor As Long   = ColorGdiToGDIplue(fp->CtlData(3)) '开状态的填充色
      Dim tianGcolor As Long   = ColorGdiToGDIplue(fp->CtlData(4)) '关状态的填充色
      Dim xianKcolor As Long   = ColorGdiToGDIplue(fp->CtlData(5)) '开状态的线条色
      Dim xianGcolor As Long   = ColorGdiToGDIplue(fp->CtlData(6)) '关状态的线条色
      Dim kuanKcolor As Long   = ColorGdiToGDIplue(fp->CtlData(7)) '开状态的块状色
      Dim kuanGcolor As Long   = ColorGdiToGDIplue(fp->CtlData(8)) '关状态的块状色
      If vv <> 0 And Len(imgK) Or vv = 0 And Len(imgG) Then
         FF_Redraw vhWnd
         Return
      End If
      Dim gg  As yGDI   = yGDI(vhWnd ,BColor) 'WM_PAINT事件里必须用这行初始化 yGDI
      Dim ww  As Long   = gg.m_Width -1 ,hh As Long = gg.m_Height -1
      Dim dpi As Single = gg.Dpi ,kk As Single ,i As Long ,xy As Single
      gg.dpi = 1
      For i = 0 To 9
         gg.Cls BColor
         Select Case fp->Style
            Case 0 ,1 ,4 ,5 '平面圆角  ---------------------------
               gg.GpBrush IIf(vv = 0 ,tianKcolor ,tianGcolor)
               gg.GpPen IIf(vv = 0 ,IIf(xianKcolor = -1 ,0 ,1) ,IIf(xianGcolor = -1 ,0 ,1)) ,IIf(vv = 0 ,xianKcolor ,xianGcolor)
               If ww >= hh Then
                  '水平
                  If fp->Style < 4 Then
                     gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,hh ,hh)
                  Else
                     gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,3 ,3)
                  End If
                  kk = hh / 16
                  If (vv = 0 And Len(zhuK)) Or (vv <> 0 And Len(zhuG)) Then
                     gg.SetColor IIf(vv = 0 ,zhuKcolor ,zhuGcolor)
                     gg.Font( ,hh * 0.6 / 96 * 72 ,True)
                     If vv = 0 Then
                        gg.DrawTextS(0 ,0 ,ww - (hh - kk) ,hh ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                     Else
                        gg.DrawTextS(hh ,0 ,ww - (hh - kk) ,hh ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                     End If
                  End If
                  gg.GpPen 0 ,0
                  gg.GpBrush IIf(vv = 0 ,kuanKcolor ,kuanGcolor)
                  If vv = 0 Then
                     xy = ((ww - (hh - kk)) - kk) / 10 * (9 - i) + kk
                  else
                     xy = ((ww - (hh - kk)) - kk) / 10 *i + kk
                  End If
                  If fp->Style < 4 Then
                     gg.GpDrawEllipse(xy ,kk ,hh -2 *kk ,hh -2 *kk)
                  Else
                     gg.GpDrawCircleFrame(xy ,kk ,hh -2 *kk ,hh -2 *kk ,3 ,3)
                  End If
                  If fp->Style = 1 Or fp->Style = 5 Then
                     gg.GpBrush IIf(vv = 0 ,tianKcolor ,tianGcolor)
                     If fp->Style = 1 Then
                        gg.GpDrawEllipse(xy + kk * 3 ,kk * 4 ,hh -8 *kk ,hh -8 *kk)
                     Else
                        gg.GpDrawCircleFrame(xy + kk * 3 ,kk * 4 ,hh -8 *kk ,hh -8 *kk ,3 ,3)
                     End If
                  End If
               Else
                  '垂直
                  If fp->Style < 4 Then
                     gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,ww ,ww)
                  Else
                     gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,3 ,3)
                  end if
                  kk = ww / 16
                  If (vv = 0 And Len(zhuK)) Or (vv <> 0 And Len(zhuG)) Then
                     gg.SetColor IIf(vv = 0 ,zhuKcolor ,zhuGcolor)
                     gg.Font( ,ww * 0.8 / Len(zhuK) / 96 * 72 ,True)
                     If vv = 0 Then
                        gg.DrawTextS(0 ,0 ,ww ,hh - (ww - kk) ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                     Else
                        gg.DrawTextS(0 ,ww ,ww ,hh - (ww - kk) ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                     End If
                  End If
                  gg.GpPen 0 ,0
                  gg.GpBrush IIf(vv = 0 ,kuanKcolor ,kuanGcolor)
                  If vv = 0 Then
                     xy = ((hh - (ww - kk)) - kk) / 10 * (9 - i) + kk
                  else
                     xy = ((hh - (ww - kk)) - kk) / 10 *i + kk
                  End If
                  If fp->Style < 4 Then
                     gg.GpDrawEllipse(kk ,xy ,ww -2 *kk ,ww -2 *kk)
                  Else
                     gg.GpDrawCircleFrame(kk ,xy ,ww -2 *kk ,ww -2 *kk ,3 ,3)
                  end if
                  If fp->Style = 1 Or fp->Style = 5 Then
                     gg.GpBrush IIf(vv = 0 ,tianKcolor ,tianGcolor)
                     If fp->Style = 1 Then
                        gg.GpDrawEllipse(kk * 4 ,xy + kk * 3 ,ww -8 *kk ,ww -8 *kk)
                     else
                        gg.GpDrawCircleFrame(kk * 4 ,xy + kk * 3 ,ww -8 *kk ,ww -8 *kk ,3 ,3)
                     End If
                  End If
               End If
            Case 2 ,3 '平面圆角  ---------------------------
               gg.GpBrush IIf(vv = 0 ,tianKcolor ,tianGcolor)
               gg.GpPen IIf(vv = 0 ,IIf(xianKcolor = -1 ,0 ,1) ,IIf(xianGcolor = -1 ,0 ,1)) ,IIf(vv = 0 ,xianKcolor ,xianGcolor)
               If ww >= hh Then
                  '水平
                  gg.GpDrawCircleFrame(1 ,hh * 0.2 ,ww -2 ,hh * 0.6 ,hh * 0.6 ,hh * 0.6)
                  kk = hh / 16
                  If (vv = 0 And Len(zhuK)) Or (vv <> 0 And Len(zhuG)) Then
                     gg.SetColor IIf(vv = 0 ,zhuKcolor ,zhuGcolor)
                     gg.Font( ,hh * 0.4 / 96 * 72 ,True)
                     If vv = 0 Then
                        gg.DrawTextS(0 ,0 ,ww - (hh - kk) ,hh ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                     Else
                        gg.DrawTextS(hh ,0 ,ww - (hh - kk) ,hh ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                     End If
                  End If
                  gg.GpPen 0 ,0
                  gg.GpBrush IIf(vv = 0 ,kuanKcolor ,kuanGcolor)
                  If vv = 0 Then
                     xy = (ww - hh) / 10 * (9 - i) + kk
                  else
                     xy = (ww - hh) / 10 *i + kk
                  End If
                  gg.GpDrawEllipse(xy ,0 ,hh ,hh)
                  If fp->Style = 3 Then
                     gg.GpBrush IIf(vv = 0 ,tianKcolor ,tianGcolor)
                     gg.GpDrawEllipse(xy + kk * 3 ,kk * 3 ,hh -6 *kk ,hh -6 *kk)
                  End If
               Else
                  '垂直
                  gg.GpDrawCircleFrame(ww * 0.2 ,1 ,ww * 0.6 ,hh -2 ,ww * 0.6 ,ww * 0.6)
                  kk = ww / 16
                  If (vv = 0 And Len(zhuK)) Or (vv <> 0 And Len(zhuG)) Then
                     gg.SetColor IIf(vv = 0 ,zhuKcolor ,zhuGcolor)
                     gg.Font( ,ww * 0.3 / Len(zhuK) / 96 * 72 ,True)
                     If vv = 0 Then
                        gg.DrawTextS(0 ,0 ,ww ,hh - (ww - kk) ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                     Else
                        gg.DrawTextS(0 ,ww ,ww ,hh - (ww - kk) ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                     End If
                  End If
                  gg.GpPen 0 ,0
                  gg.GpBrush IIf(vv = 0 ,kuanKcolor ,kuanGcolor)
                  If vv = 0 Then
                     xy = (hh - ww) / 10 * (9 - i) + kk
                  else
                     xy = (hh - ww) / 10 *i + kk
                  End If
                  gg.GpDrawEllipse(0 ,xy ,ww ,ww)
                  If fp->Style = 3 Then
                     gg.GpBrush IIf(vv = 0 ,tianKcolor ,tianGcolor)
                     gg.GpDrawEllipse(kk * 3 ,xy + kk * 3 ,ww -6 *kk ,ww -6 *kk)
                  End If
               End If
            Case 6 ,7
               gg.GpBrush tianGcolor
               gg.GpPen IIf(xianGcolor = -1 ,0 ,1) ,xianGcolor
               '水平
               If fp->Style = 6 Then
                  gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,hh ,hh)
               Else
                  gg.GpDrawCircleFrame(0 ,0 ,ww ,hh ,3 ,3)
               End If
               kk = hh / 16
               gg.GpPen IIf(xianKcolor = -1 ,0 ,1) ,xianKcolor
               gg.GpBrush tianKcolor
               If vv = 0 Then
                  xy = ww / 20 * (9 - i) 
               else
                  xy = ww / 20 *i 
               End If
               If vv Then
                  If fp->Style = 6 Then
                     gg.GpDrawCircleFrame(xy ,0 ,ww / 2 ,hh ,hh ,hh ,4 Or 8)
                  Else
                     gg.GpDrawCircleFrame(xy ,0 ,ww / 2 ,hh ,3 ,3 ,4 Or 8)
                  End If
                  If Len(zhuK) Then
                     gg.SetColor IIf(vv ,zhuKcolor ,zhuGcolor)
                     gg.Font( ,hh * 0.6 / 96 * 72 ,True)
                     gg.DrawTextS(xy ,0 ,ww / 2 ,hh ,zhuK ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  End If
               Else
                  If fp->Style = 6 Then
                     gg.GpDrawCircleFrame(xy ,0 ,ww / 2 ,hh ,hh ,hh ,1 Or 2)
                  Else
                     gg.GpDrawCircleFrame(xy ,0 ,ww / 2 ,hh ,3 ,3 ,1 Or 2)
                  End If
                  If Len(zhuG) Then
                     gg.SetColor IIf(vv = 0 ,zhuKcolor ,zhuGcolor)
                     gg.Font( ,hh * 0.6 / 96 * 72 ,True)
                     gg.DrawTextS(xy ,0 ,ww / 2 ,hh ,zhuG ,DT_CENTER Or DT_VCENTER Or DT_SINGLELINE Or DT_NOPREFIX)
                  End If
               End If
               
         End Select
         gg.Redraw
         Sleep 10
         
      Next
   End If
   FF_Redraw vhWnd
End Sub
Property Class_YFswitcher.Value() As BOOLEAN               '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then  ' "LABEL"  样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
      Return  fp->CtlData(0) 
   End If
End Property
Property Class_YFswitcher.Value(ByVal nCheckState As BOOLEAN)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(0)  = nCheckState
      AfxRedrawWindow hWndControl
   End If
End Property
Property Class_YFswitcher.Style() As Long
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then  ' "LABEL"  样式结构：&H01020304  01主样式 02对齐 03 04 选项 &H1=允许 &H2=显示 &H4=前缀 &H8=省略号
      Return  fp->Style 
   End If
End Property
Property Class_YFswitcher.Style(bValue As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->Style = bValue
      AfxRedrawWindow hWndControl
   End If
End Property
Property Class_YFswitcher.ImgON() As String '返回/设置 在控件开状态时显示的图像，1：是文件（带路径全称的文件），就读文件显示 2：图像库里的资源名称，显示资源图像
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then 
      Dim rr() As String 
      If vbSplit(fp->nText,"|",rr())<>3 Then ReDim Preserve rr(3)
      Return rr(0)
   End If
End Property
Property Class_YFswitcher.ImgON(bValue As String)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim rr() As String
      If vbSplit(fp->nText ,"|" ,rr()) <> 3 Then ReDim Preserve rr(3)
      rr(0)     = bValue
      fp->nText = FF_Join(rr() ,"|")
      AfxRedrawWindow hWndControl
   End If
End Property
Property Class_YFswitcher.ImgOFF() As String  '返回/设置 在控件关状态时显示的图像，1：是文件（带路径全称的文件），就读文件显示 2：图像库里的资源名称，显示资源图像
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then 
      Dim rr() As String 
      If vbSplit(fp->nText,"|",rr())<>3 Then ReDim Preserve rr(3)
      Return rr(1)
   End If
End Property
Property Class_YFswitcher.ImgOFF(bValue As String)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim rr() As String
      If vbSplit(fp->nText ,"|" ,rr()) <> 3 Then ReDim Preserve rr(3)
      rr(1)     = bValue
      fp->nText = FF_Join(rr() ,"|")
      AfxRedrawWindow hWndControl
   End If
End Property
Property Class_YFswitcher.NoteON() As String  '返回/设置 在控件开状态时显示的注释，有图像时不使用
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then 
      Dim rr() As String 
      If vbSplit(fp->nText,"|",rr())<>3 Then ReDim Preserve rr(3)
      Return rr(2)
   End If
End Property
Property Class_YFswitcher.NoteON(bValue As String)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim rr() As String
      If vbSplit(fp->nText ,"|" ,rr()) <> 3 Then ReDim Preserve rr(3)
      rr(2)     = bValue
      fp->nText = FF_Join(rr() ,"|")
      AfxRedrawWindow hWndControl
   End If
End Property
Property Class_YFswitcher.NoteOFF() As String  '返回/设置 在控件关状态时显示的注释，有图像时不使用
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then 
      Dim rr() As String 
      If vbSplit(fp->nText,"|",rr())<>3 Then ReDim Preserve rr(3)
      Return rr(3)
   End If
End Property
Property Class_YFswitcher.NoteOFF(bValue As String)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Dim rr() As String
      If vbSplit(fp->nText ,"|" ,rr()) <> 3 Then ReDim Preserve rr(3)
      rr(3)     = bValue
      fp->nText = FF_Join(rr() ,"|")
      AfxRedrawWindow hWndControl
   End If
End Property
Property Class_YFswitcher.NoteONColor() As Long '返回/设置开状态的注释文字色：设置用：BGR(r, g, b)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then  
      Return  fp->CtlData(1) 
   End If
End Property
Property Class_YFswitcher.NoteONColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(1)  = nColor
      AfxRedrawWindow hWndControl
   End If
End Property  
Property Class_YFswitcher.NoteOFFColor() As Long '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then  
      Return  fp->CtlData(2) 
   End If
End Property
Property Class_YFswitcher.NoteOFFColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(2)  = nColor
      AfxRedrawWindow hWndControl
   End If
End Property 
Property Class_YFswitcher.FillONColor() As Long '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then  
      Return  fp->CtlData(3) 
   End If
End Property
Property Class_YFswitcher.FillONColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(3)  = nColor
      AfxRedrawWindow hWndControl
   End If
End Property 
Property Class_YFswitcher.FillOFFColor() As Long '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then  
      Return  fp->CtlData(4) 
   End If
End Property
Property Class_YFswitcher.FillOFFColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(4)  = nColor
      AfxRedrawWindow hWndControl
   End If
End Property 
Property Class_YFswitcher.LineONColor() As Long '
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then  
      Return  fp->CtlData(5) 
   End If
End Property
Property Class_YFswitcher.LineONColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(5)  = nColor
      AfxRedrawWindow hWndControl
   End If
End Property 
Property Class_YFswitcher.LineOFFColor()  As Long 
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Return fp->CtlData(6) 
   End If
End Property 
Property Class_YFswitcher.LineOFFColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(6)  = nColor
      AfxRedrawWindow hWndControl
   End If
End Property 
Property Class_YFswitcher.LumpONColor()  As Long 
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Return fp->CtlData(7)  
   End If
End Property 
Property Class_YFswitcher.LumpONColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(7)  = nColor
      AfxRedrawWindow hWndControl
   End If
End Property 
Property Class_YFswitcher.LumpOFFColor() As Long
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      Return fp->CtlData(8)
   End If
End Property
Property Class_YFswitcher.LumpOFFColor(nColor As Long)
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      fp->CtlData(8) = nColor
      AfxRedrawWindow hWndControl
   End If
End Property
Sub Class_YFswitcher_DrawAnImage(gg As yGDI ,sImg As String ,ww As Long ,hh As Long ,nBackColor As Long ,transparency As Long) '
   Dim nBmp As Wstring * 260 = sImg
   Dim As Long iw ,ih
   Dim nico As HICON ,gp As Long
   If Mid(nBmp ,2 ,1) = ":" Then
      gg.LoadImgFile nBmp ,0 ,0 ,nBackColor
   Else
      If .Left(nBmp ,7) = "BITMAP_" Then
         gg.LoadbmpRes App.HINSTANCE ,nBmp
      ElseIf .Left(nBmp ,5) = "ICON_" Or nBmp = "AAAAA_APPICON" Then
         nico = LoadImageW(App.HINSTANCE ,nBmp ,IMAGE_ICON ,0 ,0 ,LR_DEFAULTCOLOR) '加了LR_SHARED 图标无需销毁
      Else
         gg.gpLoadImgRes(App.HINSTANCE ,nBmp)
         gp = 1
      End If
      
   End If
   If gp <> 0 Then
      GdipGetImageWidth(gg.nGpimage ,@iw) '获得图像宽和高'
      GdipGetImageHeight(gg.nGpimage ,@ih)
   ElseIf nico Then
      Dim p As ICONINFO
      GetIconInfo nico ,@p
      Dim bm As BITMAP
      If GetObject(p.hbmColor ,SizeOf(BITMAP) ,@bm) Then
         iw = bm.bmWidth  '/ gg.m_DpiX
         ih = bm.bmHeight '/ gg.m_DpiY
      End If
      DeleteObject p.hbmColor
      DeleteObject p.hbmMask
   else
      iw = gg.m_ImgWidth  '/ gg.m_DpiX
      ih = gg.m_ImgHeight '/ gg.m_DpiY
   End If
   If gp <> 0 Then
      gg.GpDrawCopyImg 0 ,0 ,ww ,hh ,0 ,0 ,iw ,ih
   ElseIf nico Then '是图标
      'DrawIcon gg.m_Dc ,10,10 ,nico
      DrawIconEx gg.m_Dc ,0 ,0 ,nico ,ww ,hh ,0 ,NULL ,DI_NORMAL
      DeleteObject nico
   Else
      gg.DrawCopyImg 0 ,0 ,ww ,hh ,0 ,0 ,iw ,ih
   End If
   If transparency < &HFF Then 
      gg.GpPen 0 ,0
      gg.GpBrush ColorGdiToGDIplue(nBackColor ,&HFF-transparency)
      gg.GpDrawFrame 0,0,ww,hh    
   end if 
End Sub
Function Class_YFswitcher.GetAn(xPos As Long ,yPos As Long) As Long
   '获取鼠标在什么按钮
   Function = xPos>0 And yPos>0
   
End Function 



