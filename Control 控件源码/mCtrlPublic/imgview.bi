#include Once "_common.bi"
Extern "C"     
    Declare Function mcImgView_Initialize MCTRL_API  Lib "mCtrl" Alias "mcImgView_Initialize"() As  BOOL
    Declare Sub mcImgView_Terminate MCTRL_API  Lib "mCtrl" Alias "mcImgView_Terminate"()
End Extern
mcImgView_Initialize

#define MC_IVS_TRANSPARENT           &H00000001
#define MC_IVS_REALSIZECONTROL       &H00000100
#define MC_IVS_REALSIZEIMAGE         &H00000200
#define MC_IVM_LOADRESOURCEW        (MC_IVM_FIRST + 0)
#define MC_IVM_LOADRESOURCEA        (MC_IVM_FIRST + 1)
#define MC_IVM_LOADFILEW            (MC_IVM_FIRST + 2)
#define MC_IVM_LOADFILEA            (MC_IVM_FIRST + 3)
#ifdef UNICODE
   #define MC_IVM_LOADRESOURCE   MC_IVM_LOADRESOURCEW
   #define MC_IVM_LOADFILE       MC_IVM_LOADFILEW
#else
   #define MC_IVM_LOADRESOURCE   MC_IVM_LOADRESOURCEA
   #define MC_IVM_LOADFILE       MC_IVM_LOADFILEA
#endif
