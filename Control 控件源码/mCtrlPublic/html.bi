
#include Once "_common.bi"

Extern "C"     
    Declare Function mcHtml_Initialize MCTRL_API  Lib "mCtrl" Alias "mcHtml_Initialize"() As  BOOL
    Declare Sub mcHtml_Terminate MCTRL_API  Lib "mCtrl" Alias "mcHtml_Terminate"()
End Extern
mcHtml_Initialize

Type MC_HMCALLSCRIPTFUNCW
     cbSize As UInteger  
     pszRet As LPWSTR
     iRet As   Integer 
     cArgs As UInteger 
     pszArg1 As  LPCWSTR
     iArg1 As       Integer 
     pszArg2 As  LPCWSTR 
     iArg2 As       Integer 
     pszArg3 As  LPCWSTR
     iArg3 As       Integer 
     pszArg4 As  LPCWSTR 
     iArg4 As  Integer 
End Type 

Type MC_HMCALLSCRIPTFUNCA
     cbSize As UInteger  
     pszRet As  LPCSTR
     iRet As   Integer 
     cArgs As UInteger 
     pszArg1 As   LPCSTR
     iArg1 As       Integer 
     pszArg2 As    LPCSTR
     iArg2 As       Integer 
     pszArg3 As   LPCSTR
     iArg3 As       Integer 
     pszArg4 As    LPCSTR
     iArg4 As  Integer 
End Type
Type MC_HMCALLSCRIPTFUNCEX
     cbSize As UInteger 
     pszFuncName As const LPCOLESTR
     lpvArgs As VARIANT Ptr 
     cArgs As UInteger 
     lpRet As VARIANT Ptr 
End Type 

#define MC_HM_GOTOURLW        (MC_HM_FIRST + 10)
#define MC_HM_GOTOURLA        (MC_HM_FIRST + 11)
#define MC_HM_SETTAGCONTENTSW (MC_HM_FIRST + 12)
#define MC_HM_SETTAGCONTENTSA (MC_HM_FIRST + 13)
#define MC_HM_GOBACK          (MC_HM_FIRST + 14)
#define MC_HM_CANBACK         (MC_HM_FIRST + 15)
#define MC_HM_CALLSCRIPTFUNCW    (MC_HM_FIRST + 16)
#define MC_HM_CALLSCRIPTFUNCA    (MC_HM_FIRST + 17)
#define MC_HM_CALLSCRIPTFUNCEX   (MC_HM_FIRST + 18)

Type MC_NMHTMLURLW
     hdr As NMHDR
    pszUrl As LPCWSTR
End Type 
Type MC_NMHTMLURLA
     hdr As NMHDR
    pszUrl As LPCSTR
End Type 

Type MC_NMHTMLPROGRESS
    hdr As NMHDR
    lProgress As Long 
    lProgressMax As Long 
End Type 

Type MC_NMHTMLTEXTW
     hdr As NMHDR
     pszText As LPCWSTR
End Type 

Type MC_NMHTMLTEXTA
    hdr As NMHDR 
    pszText As LPCSTR 
End Type 

Type MC_NMHTMLHISTORY
     hdr As NMHDR
     bCanBack As BOOL 
     bCanForward As BOOL 
End Type 

Type MC_NMHTTPERRORW
     hdr As NMHDR
     pszUrl As LPCWSTR
     iStatus As Integer 
End Type 

Type MC_NMHTTPERRORA 
    hdr As NMHDR
    pszUrl As LPCSTR
    iStatus As Integer 
End Type 

#define MC_HS_NOCONTEXTMENU      1
#define MC_HN_APPLINK            (MC_HN_FIRST + 0)

#define MC_HN_DOCUMENTCOMPLETE   (MC_HN_FIRST + 1)
#define MC_HN_PROGRESS           (MC_HN_FIRST + 2)
#define MC_HN_STATUSTEXT         (MC_HN_FIRST + 3)

#define MC_HN_TITLETEXT          (MC_HN_FIRST + 4)

#define MC_HN_HISTORY            (MC_HN_FIRST + 5)

#define MC_HN_NEWWINDOW          (MC_HN_FIRST + 6)

#define MC_HN_HTTPERROR          (MC_HN_FIRST + 7)

#define MC_HN_BEFORENAVIGATE     (MC_HN_FIRST + 8)

#ifdef UNICODE
   #define MC_HM_GOTOURL          MC_HM_GOTOURLW
   #define MC_HM_SETTAGCONTENTS   MC_HM_SETTAGCONTENTSW
   #define MC_HM_CALLSCRIPTFUNC   MC_HM_CALLSCRIPTFUNCW
   #define MC_NMHTMLURL           MC_NMHTMLURLW
   #define MC_NMHTMLTEXT          MC_NMHTMLTEXTW
   #define MC_NMHTTPERROR         MC_NMHTTPERRORW
   #define MC_HMCALLSCRIPTFUNC    MC_HMCALLSCRIPTFUNCW
#else
   #define MC_HM_GOTOURL          MC_HM_GOTOURLA
   #define MC_HM_SETTAGCONTENTS   MC_HM_SETTAGCONTENTSA
   #define MC_HM_CALLSCRIPTFUNC   MC_HM_CALLSCRIPTFUNCA
   #define MC_NMHTMLURL           MC_NMHTMLURLA
   #define MC_NMHTMLTEXT          MC_NMHTMLTEXTA
   #define MC_NMHTTPERROR         MC_NMHTTPERRORA
   #define MC_HMCALLSCRIPTFUNC    MC_HMCALLSCRIPTFUNCA
#endif

