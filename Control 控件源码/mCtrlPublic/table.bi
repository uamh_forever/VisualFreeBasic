#include Once "_common.bi"

Type MC_HTABLE As Any Ptr 


#define MC_TABLE_HEADER              &Hffff

#define MC_TCMF_TEXT                 &H00000001
#define MC_TCMF_PARAM                &H00000004
#define MC_TCMF_FLAGS                &H00000008

#define MC_TCF_ALIGNDEFAULT         &H00000000
#define MC_TCF_ALIGNLEFT            &H00000001
#define MC_TCF_ALIGNCENTER          &H00000003
#define MC_TCF_ALIGNRIGHT           &H00000002
#define MC_TCF_ALIGNVDEFAULT        &H00000000
#define MC_TCF_ALIGNTOP             &H00000004
#define MC_TCF_ALIGNVCENTER         &H0000000C
#define MC_TCF_ALIGNBOTTOM          &H00000008

#define MC_TCF_ALIGNMASKHORZ       (MC_TCF_ALIGNDEFAULT Or MC_TCF_ALIGNLEFT Or MC_TCF_ALIGNCENTER Or MC_TCF_ALIGNRIGHT)
#define MC_TCF_ALIGNMASKVERT       (MC_TCF_ALIGNVDEFAULT Or MC_TCF_ALIGNTOP Or MC_TCF_ALIGNVCENTER Or MC_TCF_ALIGNBOTTOM)

Type MC_TABLECELLW
     fMask As DWORD
    pszText As WString Ptr 
     cchTextMax As Integer 
     lParam As LPARAM
     dwFlags As DWORD
End Type 

Type MC_TABLECELLA
     fMask As DWORD
    pszText As zString Ptr 
     cchTextMax As Integer 
     lParam As LPARAM
     dwFlags As DWORD
End Type 

Extern "C"     
    Declare Function mcTable_Create MCTRL_API  Lib "mCtrl" Alias "mcTable_Create"(wColumnCount As WORD,wRowCount As WORD,dwReserved As DWORD) As MC_HTABLE
    Declare Sub mcTable_AddRef MCTRL_API  Lib "mCtrl" Alias "mcTable_AddRef"(hTable As MC_HTABLE)
    Declare Sub mcTable_Release MCTRL_API  Lib "mCtrl" Alias "mcTable_Release"(hTable As MC_HTABLE)
    Declare Function mcTable_ColumnCount MCTRL_API  Lib "mCtrl" Alias "mcTable_ColumnCount"(hTable As MC_HTABLE) As WORD
    Declare Function mcTable_RowCount MCTRL_API  Lib "mCtrl" Alias "mcTable_RowCount"(hTable As MC_HTABLE) As WORD
    Declare Function mcTable_Resize MCTRL_API  Lib "mCtrl" Alias "mcTable_Resize"(hTable As MC_HTABLE,wColumnCount As WORD,wRowCount As WORD) As BOOL
    Declare Sub mcTable_Clear MCTRL_API  Lib "mCtrl" Alias "mcTable_Clear"(hTable As MC_HTABLE,dwWhat As DWORD)
    Declare Function mcTable_SetCellW MCTRL_API  Lib "mCtrl" Alias "mcTable_SetCellW"(hTable As MC_HTABLE,wCol As WORD,wRow As WORD,pCell As MC_TABLECELLW Ptr ) As BOOL
    Declare Function mcTable_SetCellA MCTRL_API  Lib "mCtrl" Alias "mcTable_SetCellA"(hTable As MC_HTABLE,wCol As WORD,wRow As WORD,pCell As MC_TABLECELLA Ptr ) As BOOL
    Declare Function mcTable_GetCellW MCTRL_API  Lib "mCtrl" Alias "mcTable_GetCellW"(hTable As MC_HTABLE,wCol As WORD,wRow As WORD,pCell As MC_TABLECELLW Ptr ) As BOOL
    Declare Function mcTable_GetCellA MCTRL_API  Lib "mCtrl" Alias "mcTable_GetCellA"(hTable As MC_HTABLE,wCol As WORD,wRow As WORD,pCell As MC_TABLECELLA Ptr ) As BOOL
End Extern
#ifdef UNICODE
   #define MC_TABLECELL             MC_TABLECELLW
   #define mcTable_SetCell          mcTable_SetCellW
   #define mcTable_GetCell          mcTable_GetCellW
#else
   #define MC_TABLECELL             MC_TABLECELLA
   #define mcTable_SetCell          mcTable_SetCellA
   #define mcTable_GetCell          mcTable_GetCellA
#endif


