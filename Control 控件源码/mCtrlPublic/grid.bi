
#include Once "_common.bi"
#include Once "table.bi"

Extern "C"     
    Declare Function mcGrid_Initialize MCTRL_API  Lib "mCtrl" Alias "mcGrid_Initialize"() As  BOOL
    Declare Sub mcGrid_Terminate MCTRL_API  Lib "mCtrl" Alias "mcGrid_Terminate"()
End Extern
mcGrid_Initialize


#define MC_GS_NOTABLECREATE          &H0001

#define MC_GS_NOGRIDLINES            &H0002

#define MC_GS_DOUBLEBUFFER           &H0004

#define MC_GS_OWNERDATA              &H0008

#define MC_GS_RESIZABLECOLUMNS       &H0010
#define MC_GS_RESIZABLEROWS          &H0020

#define MC_GS_FOCUSEDCELL            &H0040

#define MC_GS_EDITLABELS             &H0080

#define MC_GS_NOSEL                  &H0000
#define MC_GS_SINGLESEL              &H0100
#define MC_GS_RECTSEL                &H0200
#define MC_GS_COMPLEXSEL             &H0300

#define MC_GS_SHOWSELALWAYS          &H0400

#define MC_GS_COLUMNHEADERNORMAL     &H0000
#define MC_GS_COLUMNHEADERNUMBERED   &H1000
#define MC_GS_COLUMNHEADERALPHABETIC &H2000
#define MC_GS_COLUMNHEADERNONE       &H3000
#define MC_GS_COLUMNHEADERMASK      (MC_GS_COLUMNHEADERNORMAL Or MC_GS_COLUMNHEADERNUMBERED Or MC_GS_COLUMNHEADERALPHABETIC Or MC_GS_COLUMNHEADERNONE)

#define MC_GS_ROWHEADERNORMAL        &H0000
#define MC_GS_ROWHEADERNUMBERED      &H4000
#define MC_GS_ROWHEADERALPHABETIC    &H8000
#define MC_GS_ROWHEADERNONE          &HC000
#define MC_GS_ROWHEADERMASK         (MC_GS_ROWHEADERNORMAL Or MC_GS_ROWHEADERNUMBERED or MC_GS_ROWHEADERALPHABETIC or MC_GS_ROWHEADERNONE)
#define MC_GGF_COLUMNHEADERHEIGHT     (1 Shl 0)
#define MC_GGF_ROWHEADERWIDTH         (1 Shl 1)
#define MC_GGF_DEFCOLUMNWIDTH         (1 Shl 2)
#define MC_GGF_DEFROWHEIGHT           (1 Shl 3)
#define MC_GGF_PADDINGHORZ            (1 Shl 4)
#define MC_GGF_PADDINGVERT            (1 Shl 5)

#define MC_GHT_NOWHERE              (1 Shl 0)
#define MC_GHT_ONCOLUMNHEADER       (1 Shl 1)
#define MC_GHT_ONROWHEADER          (1 Shl 2)
#define MC_GHT_ONHEADER             (MC_GHT_ONCOLUMNHEADER or MC_GHT_ONROWHEADER)
#define MC_GHT_ONNORMALCELL         (1 Shl 3)
#define MC_GHT_ONCELL               (MC_GHT_ONHEADER or MC_GHT_ONNORMALCELL)
#define MC_GHT_ONCOLUMNDIVIDER      (1 Shl 4)
#define MC_GHT_ONROWDIVIDER         (1 Shl 5)
#define MC_GHT_ONCOLUMNDIVOPEN      (1 Shl 6)
#define MC_GHT_ONROWDIVOPEN         (1 Shl 7)
#define MC_GHT_ABOVE                (1 Shl 8)
#define MC_GHT_BELOW                (1 Shl 9)
#define MC_GHT_TORIGHT              (1 Shl 10)
#define MC_GHT_TOLEFT               (1 Shl 11)

Type MC_GRECT
    wColumnFrom As WORD 
    wRowFrom As WORD
    wColumnTo As WORD
    wRowTo As WORD
End Type 

Type MC_GGEOMETRY
     fMask As DWORD
     wColumnHeaderHeight As WORD
     wRowHeaderWidth As WORD
     wDefColumnWidth As WORD
     wDefRowHeight As WORD
     wPaddingHorz As WORD
     wPaddingVert As WORD
End Type 

Type MC_GHITTESTINFO
    pt As POINT 
    flags As UInteger 
    wColumn As WORD
    wRow As WORD
End Type 


Type MC_GSELECTION
    rcExtents As MC_GRECT
    uDataCount As UInteger
    rcData As MC_GRECT Ptr 
End Type 

Type MC_NMGCACHEHINT
     hdr As NMHDR
     wColumnFrom As WORD
     wRowFrom As WORD
     wColumnTo As WORD
     wRowTo As WORD
End Type 

Type MC_NMGCUSTOMDRAW
     nmcd As MC_NMCUSTOMDRAW
     clrText As COLORREF
     clrTextBk As COLORREF
End Type 

type MC_NMGDISPINFOW
     hdr As NMHDR
     wColumn As WORD
     wRow As WORD
     cell As MC_TABLECELLW
End Type 

Type MC_NMGDISPINFOA
     hdr As NMHDR
     wColumn As WORD
     wRow As WORD
     cell As MC_TABLECELLA
End Type 

Type MC_NMGCOLROWSIZECHANGE
     hdr As NMHDR
     wColumnOrRow As WORD
     wWidthOrHeight As WORD
End Type 

Type MC_NMGFOCUSEDCELLCHANGE
     hdr As NMHDR
     wOldColumn As WORD
     wOldRow As WORD
     wNewColumn As WORD
     wNewRow As WORD
End Type 

Type MC_NMGSELECTIONCHANGE
     hdr As NMHDR
     oldSelection As MC_GSELECTION
     newSelection As MC_GSELECTION
End Type 

#define MC_GM_GETTABLE            (MC_GM_FIRST + 0)

#define MC_GM_SETTABLE            (MC_GM_FIRST + 1)

#define MC_GM_GETCOLUMNCOUNT      (MC_GM_FIRST + 2)

#define MC_GM_GETROWCOUNT         (MC_GM_FIRST + 3)

#define MC_GM_RESIZE              (MC_GM_FIRST + 4)

#define MC_GM_CLEAR               (MC_GM_FIRST + 5)

#define MC_GM_SETCELLW            (MC_GM_FIRST + 6)

#define MC_GM_SETCELLA            (MC_GM_FIRST + 7)

#define MC_GM_GETCELLW            (MC_GM_FIRST + 8)

#define MC_GM_GETCELLA            (MC_GM_FIRST + 9)

#define MC_GM_SETGEOMETRY         (MC_GM_FIRST + 10)

#define MC_GM_GETGEOMETRY         (MC_GM_FIRST + 11)

#define MC_GM_REDRAWCELLS         (MC_GM_FIRST + 12)

#define MC_GM_SETCOLUMNWIDTH      (MC_GM_FIRST + 13)

#define MC_GM_GETCOLUMNWIDTH      (MC_GM_FIRST + 14)

#define MC_GM_SETROWHEIGHT        (MC_GM_FIRST + 15)

#define MC_GM_GETROWHEIGHT        (MC_GM_FIRST + 16)

#define MC_GM_HITTEST             (MC_GM_FIRST + 17)

#define MC_GM_GETCELLRECT         (MC_GM_FIRST + 18)

#define MC_GM_ENSUREVISIBLE       (MC_GM_FIRST + 19)

#define MC_GM_SETFOCUSEDCELL      (MC_GM_FIRST + 20)

#define MC_GM_GETFOCUSEDCELL      (MC_GM_FIRST + 21)

#define MC_GM_SETSELECTION        (MC_GM_FIRST + 22)

#define MC_GM_GETSELECTION        (MC_GM_FIRST + 23)

#define MC_GM_GETEDITCONTROL      (MC_GM_FIRST + 24)

#define MC_GM_EDITLABEL           (MC_GM_FIRST + 25)

#define MC_GM_CANCELEDITLABEL     (MC_GM_FIRST + 26)

#define MC_GN_ODCACHEHINT         (MC_GN_FIRST + 0)

#define MC_GN_SETDISPINFOW        (MC_GN_FIRST + 1)

#define MC_GN_SETDISPINFOA        (MC_GN_FIRST + 2)

#define MC_GN_GETDISPINFOW        (MC_GN_FIRST + 3)

#define MC_GN_GETDISPINFOA        (MC_GN_FIRST + 4)

#define MC_GN_BEGINCOLUMNTRACK    (MC_GN_FIRST + 5)

#define MC_GN_ENDCOLUMNTRACK      (MC_GN_FIRST + 6)

#define MC_GN_BEGINROWTRACK       (MC_GN_FIRST + 7)

#define MC_GN_ENDROWTRACK         (MC_GN_FIRST + 8)

#define MC_GN_COLUMNWIDTHCHANGING (MC_GN_FIRST + 9)

#define MC_GN_COLUMNWIDTHCHANGED  (MC_GN_FIRST + 10)

#define MC_GN_ROWHEIGHTCHANGING   (MC_GN_FIRST + 11)

#define MC_GN_ROWHEIGHTCHANGED    (MC_GN_FIRST + 12)

#define MC_GN_FOCUSEDCELLCHANGING (MC_GN_FIRST + 13)

#define MC_GN_FOCUSEDCELLCHANGED  (MC_GN_FIRST + 14)

#define MC_GN_SELECTIONCHANGING   (MC_GN_FIRST + 15)

#define MC_GN_SELECTIONCHANGED    (MC_GN_FIRST + 16)

#define MC_GN_BEGINLABELEDITW     (MC_GN_FIRST + 17)

#define MC_GN_BEGINLABELEDITA     (MC_GN_FIRST + 18)

#define MC_GN_ENDLABELEDITW       (MC_GN_FIRST + 19)

#define MC_GN_ENDLABELEDITA       (MC_GN_FIRST + 20)

#ifdef UNICODE
   #define MC_WC_GRID              MC_WC_GRIDW
   #define MC_NMGDISPINFO          MC_NMGDISPINFOW
   #define MC_GM_SETCELL           MC_GM_SETCELLW
   #define MC_GM_GETCELL           MC_GM_GETCELLW
   #define MC_GN_SETDISPINFO       MC_GN_SETDISPINFOW
   #define MC_GN_GETDISPINFO       MC_GN_GETDISPINFOW
   #define MC_GN_BEGINLABELEDIT    MC_GN_BEGINLABELEDITW
   #define MC_GN_ENDLABELEDIT      MC_GN_ENDLABELEDITW
#else
   #define MC_WC_GRID              MC_WC_GRIDA
   #define MC_NMGDISPINFO          MC_NMGDISPINFOA
   #define MC_GM_SETCELL           MC_GM_SETCELLA
   #define MC_GM_GETCELL           MC_GM_GETCELLA
   #define MC_GN_SETDISPINFO       MC_GN_SETDISPINFOA
   #define MC_GN_GETDISPINFO       MC_GN_GETDISPINFOA
   #define MC_GN_BEGINLABELEDIT    MC_GN_BEGINLABELEDITA
   #define MC_GN_ENDLABELEDIT      MC_GN_ENDLABELEDITA
#endif

