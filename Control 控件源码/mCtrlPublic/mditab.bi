
#include Once "_common.bi"

Extern "C"     
    Declare Function mcMditab_Initialize MCTRL_API  Lib "mCtrl" Alias "mcMditab_Initialize"() As  BOOL
    Declare Sub mcMditab_Terminate MCTRL_API  Lib "mCtrl" Alias "mcMditab_Terminate"()
    Declare Function mcMditab_DefWindowProc MCTRL_API  Lib "mCtrl" Alias "mcMditab_DefWindowProc"(hwndMain As HWND,hwndMditab As HWND ,uMsg As UInteger,wParam as WPARAM ,lParam As  LPARAM ,plResult As LRESULT Ptr ) As  BOOL
    
End Extern
 mcMditab_Initialize
 
' *
' * @code
' * LRESULT AppWndProc(HWND hwndMain, UINT uMsg, WPARAM wParam, LPARAM lParam)
' * {
' *     LRESULT lResult;
' *
' *     // Give mcMditab_DefWindowProc() the first chance to handle the message
' *     // and if it handles it, return the result of its processing.
' *     if(mcMditab_DefWindowProc(hwndMain, hwndMditab, uMsg, wParam, lParam, &lResult))
' *         return lResult;
' *
' *     switch(uMsg) {
' *         // Normal message handling as in ordinary window procedure.
' *         ...
' *
' *         // Pass unhandled messages to DefWindowProc().
' *         default:
' *             return DefWindowProc(hwndMain, uMsg, wParam, lParam);
' *     }
' *
' * }
' * @endcode
' */

#define MC_MTS_CBONTOOLBAR           &H0000
#define MC_MTS_CBONEACHTAB           &H0001
#define MC_MTS_CBONACTIVETAB         &H0002
#define MC_MTS_CBNONE                &H0003
#define MC_MTS_CBMASK                &H0003

#define MC_MTS_TLBALWAYS             &H0000
#define MC_MTS_TLBONSCROLL           &H0004
#define MC_MTS_TLBNEVER              &H0008
#define MC_MTS_TLBMASK               &H000C

#define MC_MTS_SCROLLALWAYS          &H0010

#define MC_MTS_CLOSEONMCLICK         &H0020

#define MC_MTS_FOCUSONBUTTONDOWN     &H0040
#define MC_MTS_FOCUSNEVER            &H0080
#define MC_MTS_FOCUSMASK             &H00C0

#define MC_MTS_DOUBLEBUFFER          &H0100

#define MC_MTS_ANIMATE               &H0200

#define MC_MTS_EXTENDWINDOWFRAME     &H0400

#define MC_MTS_NOTOOLTIPS            &H0800

#define MC_MTS_DRAGDROP              &H1000

#define MC_MTS_ROUNDEDITEMS          &H2000

#define MC_MTIF_TEXT         (1 Shl 0)
#define MC_MTIF_IMAGE        (1 Shl 1)
#define MC_MTIF_PARAM        (1 Shl 2)


#define MC_MTHT_NOWHERE              (1 Shl 0)
#define MC_MTHT_ONITEMICON           (1 Shl 1)
#define MC_MTHT_ONITEMLABEL          (1 Shl 2)
#define MC_MTHT_ONITEMCLOSEBUTTON    (1 Shl 3)
#define MC_MTHT_ONITEM               (MC_MTHT_ONITEMICON Or MC_MTHT_ONITEMLABEL Or MC_MTHT_ONITEMCLOSEBUTTON)
#define MC_MTHT_ONLEFTSCROLLBUTTON   (1 Shl 4)
#define MC_MTHT_ONRIGHTSCROLLBUTTON  (1 Shl 5)
#define MC_MTHT_ONLISTBUTTON         (1 Shl 6)
#define MC_MTHT_ONCLOSEBUTTON        (1 Shl 7)
#define MC_MTHT_ONBUTTON             (MC_MTHT_ONLEFTSCROLLBUTTON Or MC_MTHT_ONRIGHTSCROLLBUTTON Or MC_MTHT_ONLISTBUTTON Or MC_MTHT_ONCLOSEBUTTON)
#define MC_MTHT_ABOVE                (1 Shl 8)
#define MC_MTHT_BELOW                (1 Shl 9)
#define MC_MTHT_TORIGHT              (1 Shl 10)
#define MC_MTHT_TOLEFT               (1 Shl 11)

Type MC_MTITEMW
     dwMask As DWORD
     pszText As LPWSTR
    cchTextMax As Integer 
    iImage As Integer 
    lParam As LPARAM
End Type 

Type MC_MTITEMA
      dwMask As DWORD
     pszText As LPSTR
    cchTextMax As Integer 
    iImage As Integer 
End Type 

Type MC_MTITEMWIDTH
    dwDefWidth As DWORD 
    dwMinWidth As DWORD 
End Type 

Type MC_MTHITTESTINFO
     pt As POINT
     flags As UInteger 
End Type 

Type MC_NMMTSELCHANGE
     hdr As NMHDR
     iItemOld As Integer 
     lParamOld As LPARAM
     iItemNew As Integer 
     lParamNew as LPARAM
End Type 

Type MC_NMMTDELETEITEM
     hdr As NMHDR
     iItem As Integer 
     lParam As LPARAM
End Type 

Type MC_NMMTCLOSEITEM
     hdr as NMHDR
     iItem As Integer 
     lParam As LPARAM
End type 

Type MC_NMMTDISPINFOW
     hdr As NMHDR
     iItem As UInteger 
     item As MC_MTITEMW
End Type 

Type MC_NMMTDISPINFOA
     hdr As NMHDR
     iItem As UInteger 
     item As MC_MTITEMA
End Type 


#define MC_MTM_GETITEMCOUNT       (MC_MTM_FIRST + 0)

#define MC_MTM_GETIMAGELIST       (MC_MTM_FIRST + 1)

#define MC_MTM_SETIMAGELIST       (MC_MTM_FIRST + 2)

#define MC_MTM_DELETEALLITEMS     (MC_MTM_FIRST + 3)

#define MC_MTM_INSERTITEMW        (MC_MTM_FIRST + 4)

#define MC_MTM_INSERTITEMA        (MC_MTM_FIRST + 5)

#define MC_MTM_SETITEMW           (MC_MTM_FIRST + 6)

#define MC_MTM_SETITEMA           (MC_MTM_FIRST + 7)

#define MC_MTM_GETITEMW           (MC_MTM_FIRST + 8)

#define MC_MTM_GETITEMA           (MC_MTM_FIRST + 9)

#define MC_MTM_DELETEITEM         (MC_MTM_FIRST + 10)

#define MC_MTM_HITTEST            (MC_MTM_FIRST + 11)

#define MC_MTM_SETCURSEL          (MC_MTM_FIRST + 12)

#define MC_MTM_GETCURSEL          (MC_MTM_FIRST + 13)

#define MC_MTM_CLOSEITEM          (MC_MTM_FIRST + 14)

#define MC_MTM_SETITEMWIDTH       (MC_MTM_FIRST + 15)

#define MC_MTM_GETITEMWIDTH       (MC_MTM_FIRST + 16)

#define MC_MTM_INITSTORAGE        (MC_MTM_FIRST + 17)

#define MC_MTM_GETITEMRECT        (MC_MTM_FIRST + 18)

#define MC_MTM_ENSUREVISIBLE      (MC_MTM_FIRST + 19)

#define MC_MTM_SETTOOLTIPS        (MC_MTM_FIRST + 20)

#define MC_MTM_GETTOOLTIPS        (MC_MTM_FIRST + 21)

#define MC_MTM_CANCELDRAGITEM     (MC_MTM_FIRST + 22)

#define MC_MTN_SELCHANGE          (MC_MTN_FIRST + 0)

#define MC_MTN_DELETEITEM         (MC_MTN_FIRST + 1)

#define MC_MTN_DELETEALLITEMS     (MC_MTN_FIRST + 2)

#define MC_MTN_CLOSEITEM          (MC_MTN_FIRST + 3)

#define MC_MTN_GETDISPINFOW       (MC_MTN_FIRST + 4)

#define MC_MTN_GETDISPINFOA       (MC_MTN_FIRST + 5)

#ifdef UNICODE
   #define MC_MTITEM             MC_MTITEMW
   #define MC_MTM_INSERTITEM     MC_MTM_INSERTITEMW
   #define MC_MTM_SETITEM        MC_MTM_SETITEMW
   #define MC_MTM_GETITEM        MC_MTM_GETITEMW
   #define MC_MTN_GETDISPINFO    MC_MTN_GETDISPINFOW
   #define MC_NMMTDISPINFO       MC_NMMTDISPINFOW

#else
   #define MC_MTITEM             MC_MTITEMA
   #define MC_MTM_INSERTITEM     MC_MTM_INSERTITEMA
   #define MC_MTM_SETITEM        MC_MTM_SETITEMA
   #define MC_MTM_GETITEM        MC_MTM_GETITEMA
   #define MC_MTN_GETDISPINFO    MC_MTN_GETDISPINFOA
   #define MC_NMMTDISPINFO       MC_NMMTDISPINFOA

#endif

