'控件类
#include Once "..\mCtrlPublic\imgview.bi"
Type Class_mCtrlImgView Extends Class_Control
    Declare Function LoadResource(ResourceName As CWSTR ,hInstance As HINSTANCE = NULL) As Long    '从资源加载图像  （BOOL）TRUE成功，FALSE否则。
    Declare Function LoadFile(FileName As CWSTR ) As Long    '从文件加载图像  （BOOL）TRUE成功，FALSE否则。
    Declare Function ClearImg( ) As Long    '清除图像  （BOOL）TRUE成功，FALSE否则。

End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Function Class_mCtrlImgView.LoadResource(ResourceName As CWSTR, HINSTANCE As HINSTANCE = NULL) As Long 
  if hInstance =0 Then hInstance= App.hInstance 
  Return SendMessage(hWndControl, MC_IVM_LOADRESOURCEW,Cast(wParam, hInstance)  ,Cast(lParam, ResourceName.vptr))
End Function

Function Class_mCtrlImgView.LoadFile(FileName As CWSTR) As Long 
   Dim pp As String = FileName  '发现不可以用 W字符，只能用A字符   Return SendMessage(hWndControl, MC_IVM_LOADFILEA,0 ,Cast(lParam, StrPtr(pp) ))
End Function

Function Class_mCtrlImgView.ClearImg( ) As Long 
  Return SendMessage(hWndControl, MC_IVM_LOADRESOURCEW,0  ,0)
End Function

