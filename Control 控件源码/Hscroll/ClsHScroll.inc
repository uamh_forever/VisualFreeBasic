#include Once "..\Vscroll\ClsVScroll.inc"
Type Class_HScroll Extends  Class_VScroll
'Protected : 
'   SChange As Long  
'   LChange As Long '大变\0\用户单击滚动条区域时，滚动条 Value 属性改变的数量\10\
'Public : 
'    Declare Property Value() As Long                 '返回/设置滚动值
'    Declare Property Value(ByVal bValue As Long)     '
'    Declare Property nMax() As Long                 '返回/设置滚动条最大值
'    Declare Property nMax(ByVal posMax As Long)     '
'    Declare Property nMin() As Long                 '返回/设置滚动最小值
'    Declare Property nMin(ByVal posMin As Long)     '
'    Declare Property SmallChange() As Long                 '返回/设置用户单击滚动条剪头时，滚动条 Value 属性改变的数量
'    Declare Property SmallChange(ByVal bValue As Long)     '
'    Declare Property LargeChange() As Long                 '返回/设置用户单击滚动条区域时，滚动条 Value 属性改变的数量
'    Declare Property LargeChange(ByVal bValue As Long)     '
'    Declare Function HandleScroll(hWndControl As hWnd, nScrollCode As Long, nPosition As Long) As long '处理滚动条消息，并且返回最新的当前位置值。由VFB系统调用然后发送给用户 Change 事件
End Type

''----------------------------------------------------------------------------------------------------------------------------------------------------------------
'
'Property Class_HScroll.Value() As Long                 '返回/设置滚动值
'  Return GetScrollPos(hWndControl, SB_CTL)
'End Property
'Property Class_HScroll.Value(ByVal bValue As Long)     '
'  SetScrollPos(hWndControl, SB_CTL, bValue, True)
'End Property
'Property Class_HScroll.nMax() As Long                 '返回/设置滚动条最大值
'  Dim lpposMin As Long, lpposMax As Long
'  GetScrollRange(hWndControl, SB_CTL, @lpposMin, @lpposMax)
'  Return lpposMax
'End Property
'Property Class_HScroll.nMax(ByVal posMax As Long)
'   Dim lpposMin As Long, lpposMax As Long
'   GetScrollRange(hWndControl, SB_CTL, @lpposMin, @lpposMax)
'   Dim ss As SCROLLINFO
'   ss.cbSize = SizeOf(SCROLLINFO)
'   ss.fMask = SIF_PAGE Or SIF_RANGE
'   ss.nMax = posMax
'   ss.nMin = lpposMin
'   ss.nPage = (posMax - lpposMin + 1) / 5
'   if ss.nPage = 0 Then ss.nPage = 1
'   SetScrollInfo hWndControl, SB_CTL, @ss, TRUE
'   'SendMessage(hWndControl, SBM_SETRANGEREDRAW, lpposMin, posMax)
'End Property
'Property Class_HScroll.nMin() As Long                 '返回/设置滚动最小值
'  Dim lpposMin As Long, lpposMax As Long
'  GetScrollRange(hWndControl, SB_CTL, @lpposMin, @lpposMax)
'  Return lpposMin
'End Property
'Property Class_HScroll.nMin(ByVal posMin As Long)     '
'   Dim lpposMin As Long, lpposMax As Long
'   GetScrollRange(hWndControl, SB_CTL, @lpposMin, @lpposMax)
'   Dim ss As SCROLLINFO
'   ss.cbSize = SizeOf(SCROLLINFO)
'   ss.fMask = SIF_PAGE Or SIF_RANGE
'   ss.nMax = lpposMax
'   ss.nMin = posMin
'   ss.nPage = (lpposMax - posMin + 1) / 5
'   if ss.nPage = 0 Then ss.nPage = 1
'   SetScrollInfo hWndControl, SB_CTL, @ss, TRUE
'   'SendMessage(hWndControl, SBM_SETRANGEREDRAW, posMin, lpposMax)
'End Property
'Property Class_HScroll.SmallChange() As Long            
'  Return SChange
'End Property
'Property Class_HScroll.SmallChange(ByVal bValue As Long)     '
'  SChange = bValue
'End Property
'Property Class_HScroll.LargeChange() As Long            
'  Return LChange
'End Property
'Property Class_HScroll.LargeChange(ByVal bValue As Long)     '
'  LChange = bValue
'End Property
'
'Function Class_HScroll.HandleScroll(hControl As hWnd, nScrollCode As Long, nPosition As Long) As Long   '
'   Dim As Long nPos,jMin, jMax
'   GetScrollRange(hControl, SB_CTL, @jMin, @jMax)
'   nPos = GetScrollPos(hControl, SB_CTL)
'
'   Select Case nScrollCode
'      Case SB_LINELEFT  '点击向←按钮
'         If nPos - SChange >= jMin Then nPos -= SChange
'         SetScrollPos(hControl, SB_CTL, nPos, 1)
'      Case SB_LINERIGHT '点击向→按钮
'         If nPos + SChange <= jMax Then nPos += SChange
'         SetScrollPos(hControl, SB_CTL, nPos, 1)
'      Case SB_PAGELEFT '点击滑块左边空白处
'         If nPos > jMin + LChange Then
'            nPos -= LChange
'         Else
'            nPos = jMin
'         End If
'         SetScrollPos(hControl, SB_CTL, nPos, 1)
'      Case SB_PAGERIGHT '点击滑块右边空白处
'         If nPos < jMax - LChange Then
'            nPos += LChange
'         Else
'            nPos = jMax
'         End If
'         SetScrollPos(hControl, SB_CTL, nPos, 1)
'      Case SB_THUMBPOSITION '托动滑块 鼠标放开后
'         
'      Case SB_THUMBTRACK '托动滑块 同时 nPosition 是托动后的位置
'         SetScrollPos(hControl, SB_CTL, nPosition, 1)
'         nPos = nPosition
'      Case SB_LEFT
'      Case SB_RIGHT
'      Case SB_ENDSCROLL '结束滚动  不管点了什么地方，只要放开鼠标，就是这个
'         
'      Case Else
'         
'   End Select
'   Function = nPos
'End Function
