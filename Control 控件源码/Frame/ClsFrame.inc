Type Class_Frame Extends Class_Virtual
   Declare Property Caption() As CWStr                '返回/设置控件中的文本
   Declare Property Caption(ByVal sText As CWStr)
   Declare Property TextAlign() As Long      '返回/设置0 - 左对齐,1 - 居中,2 - 右对齐
   Declare Property TextAlign(bValue As Long)
   Declare Property BackColor() As Long                  '返回/设置背景色：设置用：BGR(r, g, b)  
   Declare Property BackColor(nColor As Long)
   Declare Property ForeColor() As Long                  '返回/设置前景色：设置用：BGR(r, g, b)   
   Declare Property ForeColor(nColor As Long)
   Declare Property BorderColor() As Long                  '返回/设置用于周围的线条颜色：设置用：BGR(r, g, b)  
   Declare Property BorderColor(nColor As Long)
   Declare Property BackColorGDIplue() As ARGB                  '返回/设置背景色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)  
   Declare Property BackColorGDIplue(GDIplue_Color As ARGB)
   Declare Property ForeColorGDIplue() As ARGB                  '返回/设置前景色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)   
   Declare Property ForeColorGDIplue(GDIplue_Color As ARGB)
   Declare Property BorderColorGDIplue() As ARGB                  '返回/设置用于周围的线条颜色：GDIplue（GDI+）颜色，带透明度 A ，用：RGBA (r, g, b, a)  
   Declare Property BorderColorGDIplue(GDIplue_Color As ARGB)
   Declare Property Fillet() As Long      '返回/设置圆角半径 0--999
   Declare Property Fillet(bValue As Long)
   Declare Property BorderWidth() As Long      '返回/设置边框宽度0到15
   Declare Property BorderWidth(bValue As Long)
   Declare Property Font() As String          '返回/设置用于在控件中绘制文本的字体，格式为：字体,字号,加粗,斜体,下划线,删除线  中间用英文豆号分割，可以省略参数 默认为：宋体,9,0,0,0,0  自动响应系统DPI创建字体大小。
   Declare Property Font(nFont As String)     '
   Declare Sub Drawing(gg as yGDI, hWndForm As hWnd, nBackColor As Long)  '描绘控件，窗口需要描绘，通知类把控件画到 gg 中。（** VFB系统自用）
   Declare Property Visible() As Boolean                 '显示或隐藏控件。{=.True.False}
   Declare Property Visible(ByVal bValue As Boolean)   
   Declare Property Enabled() As Boolean                 '返回/设置控件是否允许操作。{=.True.False}
   Declare Property Enabled(ByVal bValue As Boolean)   
End Type

Property Class_Frame.Enabled() As Boolean
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then  ' 虚拟控件通用样式 样式结构：&H01020304  04 选项 &H1=允许 &H2=显示
      Return (fp->Style And &H1 )<>0
   End If
End Property
Property Class_Frame.Enabled(ByVal bValue As Boolean)
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      If bValue Then fp->Style Or= &H1 Else fp->Style And= Not &H1
      '同步所有在 Frame 内的控件
      Dim mjiu As Rect ,yjiu1 As Rect ,yjiu2 As Rect
      SetRect(@yjiu1 ,fp->nLeft ,fp->nTop ,fp->nLeft + fp->nWidth ,fp->nTop + fp->nHeight)
      '虚拟控件的情况
      Dim fp2 As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(m_hWndParent)
      Static sIDC() As Long '是否原来禁用，要是禁用的，下次启用时就不再启用
      Dim jj        As Long ,i As Long
      While fp2
         If fp2->IDC > 0 And fp2->IDC <> m_IDC Then
            SetRect(@yjiu2 ,fp2->nLeft ,fp2->nTop ,fp2->nLeft + fp2->nWidth ,fp2->nTop + fp2->nHeight)
            If IntersectRect(@mjiu ,@yjiu1 ,@yjiu2) Then
               If bValue Then
                  fp2->Style Or= &H1
                  If UBound(sIDC) > -1 Then '查找是否原来被禁用的，现在就不启用
                     For i = 0 To UBound(sIDC)
                        If sIDC(i) = fp2->IDC Then
                           fp2->Style And= Not &H1
                           Exit For
                        End If
                     Next
                  End If
               Else
                  If (fp->Style And &H1) = 0 Then '本来就是被禁用的
                     ReDim Preserve sIDC(jj)
                     sIDC(jj) = fp2->IDC
                     jj       += 1
                  Else
                     fp2->Style And= Not &H1
                  End If
               End If
            End If
         End If
         fp2 = fp2->VrControls
      Wend
      If jj = 0 Then Erase sIDC
      '真实控件情况
      Dim dpi  As Single = AfxScaleX(1)
      Dim mWin As hWnd
      Static sWin() As hWnd  '是否原来禁用，要是禁用的，下次启用时就不再启用
      jj = 0
      Do
         mWin = FindWindowEx(m_hWndParent ,mWin ,NULL ,NULL)
         If mWin = 0 Then Exit Do
         GetWindowRect(mWin ,@yjiu2)
         MapWindowPoints HWND_DESKTOP ,m_hWndParent ,Cast(LPPOINT ,Varptr(yjiu2)) ,2
         yjiu2.Left   /= dpi
         yjiu2.top    /= dpi
         yjiu2.Right  /= dpi
         yjiu2.bottom /= dpi
         If IntersectRect(@mjiu ,@yjiu1 ,@yjiu2) Then
            If bValue Then
               EnableWindow mWin ,True
               If UBound(sWin) > -1 Then '查找是否原来被禁用的，现在就不启用
                  For i = 0 To UBound(sWin)
                     If sWin(i) = mWin Then
                        EnableWindow mWin ,False
                        Exit For
                     End If
                  Next
               End If
            Else
               If IsWindowEnabled(mWin) = 0 Then   '原来就被禁用的，就先保存列表
                  ReDim Preserve sWin(jj)
                  sWin(jj) = mWin
                  jj       += 1
               Else
                  EnableWindow mWin ,False
               End If
            End If
         End If
      Loop
      If jj = 0 Then Erase sWin
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.Visible() As Boolean
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      Return (fp->Style and &H2 )<>0 
   End If
End Property
Property Class_Frame.Visible(ByVal bValue As Boolean)
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      If bValue Then fp->Style Or= &H2 Else fp->Style And= Not &H2
      '同步所有在 Frame 内的控件
      Dim mjiu As Rect ,yjiu1 As Rect ,yjiu2 As Rect
      SetRect(@yjiu1 ,fp->nLeft ,fp->nTop ,fp->nLeft + fp->nWidth ,fp->nTop + fp->nHeight)
      '虚拟控件的情况
      Dim fp2 As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(m_hWndParent)
      Static sIDC() As Long '是否原来不显示，要是不显示的，下次显示时就不再不显示
      Dim jj        As Long ,i As Long
      While fp2
         If fp2->IDC > 0 And fp2->IDC <> m_IDC Then
            SetRect(@yjiu2 ,fp2->nLeft ,fp2->nTop ,fp2->nLeft + fp2->nWidth ,fp2->nTop + fp2->nHeight)
            If IntersectRect(@mjiu ,@yjiu1 ,@yjiu2) Then
               If bValue Then
                  fp2->Style Or= &H2
                  If UBound(sIDC) > -1 Then '查找是否原来不显示的，现在就不显示
                     For i = 0 To UBound(sIDC)
                        If sIDC(i) = fp2->IDC Then
                           fp2->Style And= Not &H2
                           Exit For
                        End If
                     Next
                  End If
               Else
                  If (fp->Style And &H2) = 0 Then '本来就是不显示的
                     ReDim Preserve sIDC(jj)
                     sIDC(jj) = fp2->IDC
                     jj       += 1
                  Else
                     fp2->Style And= Not &H2
                  End If
               End If
            End If
         End If
         fp2 = fp2->VrControls
      Wend
      If jj = 0 Then Erase sIDC
      '真实控件情况
      Dim dpi  As Single = AfxScaleX(1)
      Dim mWin As hWnd
      Static sWin() As hWnd '是否原来不显示的，要是不显示的，下次显示时就不再显示
      jj = 0
      Do
         mWin = FindWindowEx(m_hWndParent ,mWin ,NULL ,NULL)
         If mWin = 0 Then Exit Do
         GetWindowRect(mWin ,@yjiu2)
         MapWindowPoints HWND_DESKTOP ,m_hWndParent ,Cast(LPPOINT ,Varptr(yjiu2)) ,2
         yjiu2.Left   /= dpi
         yjiu2.top    /= dpi
         yjiu2.Right  /= dpi
         yjiu2.bottom /= dpi
         If IntersectRect(@mjiu ,@yjiu1 ,@yjiu2) Then
            If bValue Then
               ShowWindow(mWin ,SW_SHOWNOACTIVATE)
               If UBound(sWin) > -1 Then '查找是否原来不显示的，现在就不显示
                  For i = 0 To UBound(sWin)
                     If sWin(i) = mWin Then
                        ShowWindow(mWin ,SW_HIDE)
                        Exit For
                     End If
                  Next
               End If
            Else
               If IsWindowVisible(mWin) = 0 Then '原来就被禁用的，就先保存列表
                  ReDim Preserve sWin(jj)
                  sWin(jj) = mWin
                  jj       += 1
               Else
                  ShowWindow(mWin ,SW_HIDE)
               End If
            End If
         End If
      Loop
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.Caption() As CWSTR
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return fp->nText
   end if
End Property
Property Class_Frame.Caption(ByVal sText As CWStr)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->nText = sText
      AfxRedrawWindow m_hWndParent
   end if
End Property
Property Class_Frame.TextAlign() As Long  '样式结构：&H12345678  12主样式 34填充 5宽度   678 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &HFF000000) Shr 24
   End If
End Property
Property Class_Frame.TextAlign(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 5 then sy = 5
      fp->Style = (fp->Style And &H00FFFFFF) Or (sy Shl 24)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.BackColor() As Long    
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->BackColor,GetSysColor(COLOR_BTNFACE))
   
      Return cc      
   End If
End Property
Property Class_Frame.BackColor(ByVal nColor As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->BackColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明 
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.ForeColor() As Long    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->ForeColor,GetSysColor(COLOR_BTNTEXT))
      Return cc      
   End If
End Property
Property Class_Frame.ForeColor(ByVal nColor As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->ForeColor = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明 
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.BorderColor() As Long    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDI(fp->CtlData(0),GetSysColor(COLOR_WINDOWTEXT))
      Return cc        
   End If
End Property
Property Class_Frame.BorderColor(ByVal nColor As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->CtlData(0) = nColor Or &HFF000000 '增加 A通道，不透明，不然是全透明       
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.BackColorGDIplue() As ARGB    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->BackColor)
      if cc = 0 then
         cc = GetSysColor(COLOR_BTNFACE)
         Dim b As UByte Ptr = Cast(Any Ptr, @cc ) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0], b[1], b[2], &HFF)  
      End if
      Return cc      
   End If
End Property
Property Class_Frame.BackColorGDIplue(ByVal nColor As ARGB)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr, @nColor )
      Dim c As Long = BGRA(b[2], b[1], b[0], b[3])  'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->BackColor = c
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.ForeColorGDIplue() As ARGB    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->ForeColor)
      if cc = 0 then
         cc = GetSysColor(COLOR_BTNTEXT)
         Dim b As UByte Ptr = Cast(Any Ptr, @cc ) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0], b[1], b[2], &HFF)  
      End if
      Return cc      
   End If
End Property
Property Class_Frame.ForeColorGDIplue(ByVal nColor As ARGB)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr =  Cast(Any Ptr, @nColor )
      Dim c As Long = BGRA(b[2], b[1], b[0], b[3])  'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）
      fp->ForeColor = c
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.BorderColorGDIplue() As ARGB    
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim cc As Long = GetCodeColorGDIplue(fp->CtlData(0))
      if cc = 0 then
         cc = GetSysColor(COLOR_WINDOWTEXT)
         Dim b As UByte Ptr = Cast(Any Ptr, @cc ) 'GDI颜色到GDI+颜色转换
         cc = RGBA(b[0], b[1], b[2], &HFF)  
      End if
      Return cc      
   End If
End Property
Property Class_Frame.BorderColorGDIplue(ByVal nColor As ARGB)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim b As UByte Ptr = Cast(Any Ptr, @nColor )
      Dim c As Long = BGRA(b[2], b[1], b[0], b[3])  'GDI+ 样式内存结构 BGRA  VFB 里内存结构 RGBA（主要为兼容GDI，去掉A就是GDI色）      
      fp->CtlData(0) = c
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.BorderWidth() As Long  '样式结构：&H12345678  12主样式 34填充 5宽度   678 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H0000F000) Shr 12
   End If
End Property
Property Class_Frame.BorderWidth(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 15 then sy = 15
      fp->Style = (fp->Style and &HFFFF0FFF) Or (sy Shl 12)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.Fillet() As Long  '样式结构：&H12345678  12主样式 34填充 5宽度   678 选项 &H1=允许 &H2=显示
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return (fp->Style And &H00FF0000) Shr 16
   End If
End Property
Property Class_Frame.Fillet(bValue As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Dim sy As UInteger = bValue
      if sy > 9 then sy = 9
      fp->Style = (fp->Style and &HFF00FFFF) Or (sy Shl 16)
      AfxRedrawWindow m_hWndParent
   End If
End Property
Property Class_Frame.Font() As String
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then   
      Return fp->nFont  
   End If
End Property
Property Class_Frame.Font(nFont As String)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->nFont = nFont
      AfxRedrawWindow m_hWndParent
   end if
End Property
Sub Class_Frame.Drawing(gg As yGDI, hWndForm As hWnd, nBackColor As Long)
   m_hWndParent = hWndForm
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp = 0 then return
   if (fp->Style and &H2 ) =0 Then Return 
   Dim As Long nStyles = (fp->Style And &HFF000000) Shr 24,nFillet = (fp->Style And &H00FF0000) Shr 16 , _
      nBorderWidth = (fp->Style And &H0000F000) Shr 12, x, y, w, h, zh, fc, bc
   x = fp->nLeft : y = fp->nTop : w = fp->nWidth : h = fp->nHeight
   SetBkMode gg.m_Dc, 0  '设置这个后，画上的字是透明的
   fc = GetCodeColorGDI(fp->ForeColor)
   If fc = -1 Then fc = GetSysColor(COLOR_WINDOWTEXT)
   bc = GetCodeColorGDI(fp->BackColor)
   If bc = -1 Then bc = nBackColor
   If bc = -1 Then bc = GetSysColor(COLOR_BTNFACE)
   gg.SetColor fc, bc

   Dim As Long lFormat
   Select Case nStyles
      Case 1 '居中
         lFormat = DT_CENTER Or DT_TOP
      Case 0 ' 左对齐
         lFormat = DT_LEFT Or DT_TOP
      Case 2 '右对齐
         lFormat = DT_RIGHT Or DT_TOP
   End Select
   lFormat = lFormat Or DT_SINGLELINE Or DT_EXPANDTABS
   Dim oFont As HGDIOBJ = SelectObject(gg.m_Dc ,gFLY_GetFontHandles(fp->nFont))
   Dim tt As CWSTR = WStr(" ") & fp->nText & " "
   If Len(fp->nText) Then zh = gg.DrawTextS(x + nFillet+nBorderWidth+1, y, w -20, h,tt, lFormat Or DT_CALCRECT) / AfxScaleY(1)
   
   gg.GpPen nBorderWidth, GetCodeColorGDIplue(fp->CtlData(0))
   gg.GpBrush GetCodeColorGDIplue(fp->BackColor)
   
   zh /= 2
   gg.GpDrawCircleFrame x, y + zh, w, h - zh, nFillet*2, nFillet*2
   
   If Len(fp->nText) Then gg.DrawTextS(x +  nFillet+nBorderWidth+1, y, w -20, h, tt, lFormat)
   SetBkMode gg.m_Dc, 1  '设置这个后，画上的字是透明的
   SelectObject(gg.m_Dc, oFont)
   
   
End Sub

