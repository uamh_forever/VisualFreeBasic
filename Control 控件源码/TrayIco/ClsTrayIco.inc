Type Class_TrayIco
Private : 
   m_hWndForm As hWnd '控件句柄
   m_IDC As Long '控件IDC
   m_Msg As uLong 
   m_Tip As CWSTR
   m_ResImg As String
   m_Ok As Boolean  '是不是创建了提示

   Declare Function GetFP() As FormControlsPro_TYPE ptr '返回控件保存数据的指针（一般是类自己用）
Public : 
   Declare Constructor
   Declare Property hWndForm() As.hWnd         '返回/设置控件所在的窗口句柄，主要用于多开同一个窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
   Declare Property hWndForm(ByVal hWndParent As.hWnd) '获取控件所在的窗口句柄（父窗口）
   Declare Property IDC() As Long             '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Property Tag() As CWSTR                       '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWSTR)
   Declare Function Create() As Boolean '创建托盘图标，如果成功，则返回TRUE，否则返回FALSE。{=.True.False}
   Declare Property CallMsg() As ULong             '返回/设置消息值，是：WM_User + 消息值，用于本控件事件值，万一与其它冲突，可以修改此值。不同控件必须不同值。
   Declare Property CallMsg(wMsg As ULong)
   Declare Property Tips() As CWSTR             '返回/设置 鼠标指向图标时提示的内容
   Declare Property Tips(nTip As CWSTR)
   Declare Property ResImg() As String      '返回/设置 图像资源名，就是在图像管理器中的名称
   Declare Property ResImg(nResImg As String)
   Declare Function Del() As Boolean '删除托盘图标，如果成功，则返回TRUE，否则返回FALSE。
   Declare Function Info(nTips As CWSTR,Title As CWSTR =WChr(0),TitleIcon As Long=0 ) As Boolean  '立即以气球样式显示提示，成功返回TRUE，否则返回FALSE {3.NIIF_NONE 没有图标.NIIF_INFO 信息图标.NIIF_WARNING 警告图标.NIIF_ERROR 错误图标.NIIF_USER 用户图标} {=.True.False}
   MsgBarRester As ULong '任务栏重建消息值，创建控件时向系统注册的来的。
   Declare Property UserData(idx AS LONG) As Integer      '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx AS LONG, bValue As Integer)   
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_TrayIco
   '注意：由于窗口类是永久的，全局变量，因此这里从开软件到关软件只执行1次。
   MsgBarRester = RegisterWindowMessageA("TaskBarCreated") '注册任务栏恢复消息。当系统崩溃或任务栏消失后重新恢复，发送给窗口消息。
End Constructor


Function Class_TrayIco.GetFP() As FormControlsPro_TYPE ptr '返回自己控件的指针
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(m_hWndForm)
   While fp
      if fp->IDC = m_IDC Then Return fp
      fp = fp->VrControls
   Wend
   
End Function
Property Class_TrayIco.hWndForm() As .hWnd         '返回/设置控件所在的窗口句柄
      Return m_hWndForm
End Property
Property Class_TrayIco.hWndForm(ByVal hWndParent As .hWnd)
   m_hWndForm = hWndParent
End Property
Property Class_TrayIco.CallMsg() As ULong      
      Return m_Msg
End Property
Property Class_TrayIco.CallMsg(wMsg As uLong)
   m_Msg = wMsg
   if m_Ok Then This.Create
End Property
Property Class_TrayIco.IDC() As Long   
   Return m_IDC
End Property
Property Class_TrayIco.IDC(ByVal NewIDC As Long)
   m_IDC = NewIDC
   if m_Ok Then This.Create
End Property
Property Class_TrayIco.Tips() As CWSTR 
   Return m_Tip
End Property
Property Class_TrayIco.Tips(nTip As CWSTR)
   m_Tip = nTip
   If m_Ok Then
      Dim ti As NOTIFYICONDATAW
      ti.cbSize = SizeOf(ti)
      ti.hWnd = m_hWndForm
      ti.uID = m_IDC '图标号，其它用什么都可以，只是取消时要记得用同样的号，就可以了。
      ti.uFlags = NIF_TIP
      ti.szTip = m_Tip '提示
      Shell_NotifyIconW NIM_MODIFY ,@ti
   End If
End Property
Function Class_TrayIco.Info(nTips As CWSTR, Title As CWSTR = WChr(0), TitleIcon As Long = 0) As Boolean
   if m_Ok = 0 Then This.Create
   Dim ti As NOTIFYICONDATAW
   ti.cbSize = SizeOf(ti)
   ti.HWnd = m_hWndForm
   ti.uID = m_IDC '图标号，其它用什么都可以，只是取消时要记得用同样的号，就可以了。
   '气球样式
   ti.uFlags = NIF_INFO
   ti.szInfo = nTips
   ti.szInfoTitle = Title
   ti.dwInfoFlags = TitleIcon
   ti.uTimeout = 5000
   Function = Shell_NotifyIconW(NIM_MODIFY, @ti)
End Function


Property Class_TrayIco.ResImg() As String 
   Return m_ResImg
End Property
Property Class_TrayIco.ResImg(nResImg As String)
   m_ResImg = nResImg
   If m_Ok Then
      Dim ti as NOTIFYICONDATAW
      ti.cbSize = SizeOf(NOTIFYICONDATAW)
      ti.HWnd   = m_hWndForm
      ti.uID    = m_IDC '图标号，其它用什么都可以，只是取消时要记得用同样的号，就可以了。
      ti.uFlags = NIF_ICON
      Dim nIcon As HICON ,nResImg As String = m_ResImg
      If InStr(nResImg ,"BITMAP_") = 1 Then
         Dim nBmp As HBITMAP = LoadImageA(app.hInstance ,ResImg ,IMAGE_BITMAP ,0 ,0 ,LR_DEFAULTCOLOR)
         Dim po   As ICONINFO
         po.fIcon    = TRUE
         po.hbmColor = nBmp
         po.hbmMask  = nBmp
         nIcon       = CreateIconIndirect(@po)
         DeleteObject nBmp
      ElseIf InStr(nResImg ,"ICON_") = 1 Or nResImg = "AAAAA_APPICON" Then
         nIcon = LoadImageA(App.HINSTANCE ,ResImg ,IMAGE_ICON ,0 ,0 ,LR_DEFAULTCOLOR) '从资源里加载图标
      Else
         nIcon = AfxGdipIconFromRes(App.hInstance ,ResImg)
      End if
      ti.HICON = nIcon
      Shell_NotifyIcon NIM_MODIFY ,@ti
      DestroyIcon ti.HICON
   End if
End Property
Function Class_TrayIco.Create() As Boolean '创建一个托盘图标，如果成功，则返回TRUE，否则返回FALSE。
   if m_Ok Then This.Del
   Dim ti as NOTIFYICONDATAW
   ti.cbSize = SizeOf(NOTIFYICONDATAW)
   ti.HWnd = m_hWndForm
   ti.uID = m_IDC '图标号，其它用什么都可以，只是取消时要记得用同样的号，就可以了。
   ti.uFlags = NIF_ICON Or NIF_MESSAGE Or NIF_TIP 
   ti.uCallbackMessage = WM_USER + m_Msg ' 向窗口发送的消息，可以自己定，只要与接受到的处理相同就可以了
   Dim nIcon As HICON, nResImg As String = m_ResImg
   If InStr(nResImg, "BITMAP_") = 1 Then
      Dim nBmp As HBITMAP = LoadImageA(app.hInstance, ResImg, IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR)
      Dim po As ICONINFO
      po.fIcon = True
      po.hbmColor = nBmp
      po.hbmMask = nBmp
      nIcon = CreateIconIndirect(@po)
      DeleteObject nBmp
   ElseIf InStr(nResImg, "ICON_") = 1 Or nResImg="AAAAA_APPICON"   Then
      nIcon = LoadImageA(App.HINSTANCE ,ResImg ,IMAGE_ICON ,0 ,0 ,LR_DEFAULTCOLOR) '从资源里加载图标
   Else
      nIcon = AfxGdipIconFromRes(App.HINSTANCE, ResImg)
   End if
   ti.HICON = nIcon
   ti.szTip = m_Tip '提示
   m_Ok = Shell_NotifyIconW(NIM_ADD ,@ti) ' '加入任务栏图标
   DestroyIcon nIcon '销毁一个图标并且释放该图标所占用的内存；
   Function = m_Ok
End Function
Function Class_TrayIco.Del() As Boolean '删除托盘图标，如果成功，则返回TRUE，否则返回FALSE。
   If m_Ok Then
      Dim ti As NOTIFYICONDATAW
      ti.cbSize = SizeOf(NOTIFYICONDATAW)
      ti.HWnd = m_hWndForm
      ti.uID = m_IDC '图标号，其它用什么都可以，只是取消时要记得用同样的号，就可以了。
      Function = Shell_NotifyIconW(NIM_DELETE, @ti)
      m_Ok = False
   Else
      Function = TRUE
   End if
End Function
Property Class_TrayIco.UserData(idx AS LONG) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return  0
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      Return fp->UserData(idx)
   End If   

End Property
Property Class_TrayIco.UserData(idx AS LONG, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      fp->UserData(idx) = bValue
   End If    
End Property
















