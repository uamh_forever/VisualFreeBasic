'控件类
#include Once "..\mCtrlPublic\chart.bi"
Type Class_mCtrlChart Extends Class_Control
   Declare Property Caption() As CWStr                '返回/设置 题目文本
   Declare Property Caption(ByVal sText As CWStr)
   Declare Function GetCount() As Long  '获取数据总数。
   Declare Function DeleteAll() As Long  '删除所有数据集。TRUE成功，FALSE失败。
   Declare Function DeleteItem(Index As Long) As Long  '删除单项数据。TRUE成功，FALSE失败。
   Declare Function Insert(Index As Long, nValue As Integer, nText As CWSTR = "", nColor As Long=-1) As Long  '插入1条数据。返回新数据集的索引（从0开始），失败返回-1。
   Declare Function AddItem(nValue As Integer, nText As CWSTR = "", nColor As Long=-1) As Long  '新增项目，返回新添加的索引（从0开始），失败返回-1。
   Declare Function InsertVirtual(Index As Long) As Long  '插入1条虚拟表数据。返回新数据集的索引（从0开始），失败返回-1。(虚拟表的数据由事件提供)
   Declare Function AddItemVirtual() As Long  '新增虚拟表，返回新添加的索引（从0开始），失败返回-1。(虚拟表的数据由事件提供)
   
   Declare Property Value(Index As Long) As Integer  '获取/设置 数据值，索引从0开始
   Declare Property Value(Index As Long, nValue As Integer)
   Declare Property Colour(Index As Long) As Long  '获取/设置 颜色值(GDI色)，索引从0开始 设置用：BGR(r, g, b)
   Declare Property Colour(Index As Long, nColor As Long)
   Declare Property Text(Index As Long) As CWStr                '返回/设置 数据中的文本
   Declare Property Text(Index As Long, sText As CWStr)
   Declare Property FactorExponent(n As Long) As Long   '返回/设置 因子指数v=-9到9（n=1主轴或 n=2辅轴） ,返回-666为失败时的值
   Declare Property FactorExponent(n As Long, v As Long)
   Declare Property AxisOffset(n As Long) As Long   '返回/设置 轴偏移量 （n=1主轴或 n=2辅轴）
   Declare Property AxisOffset(n As Long, v As Long)
   Declare Property AxisText(n As Long) As CWStr   '返回/设置 轴的图例文本 （n=1主轴或 n=2辅轴）
   Declare Property AxisText(n As Long, v As CWStr)
   Declare Property Style() As Long   '返回/设置 控件样式 {=.MC_CHS_PIE 饼形图.MC_CHS_SCATTER 散点图.MC_CHS_LINE 折线图.MC_CHS_STACKEDLINE 堆积折线图.MC_CHS_AREA 面积图.MC_CHS_STACKEDAREA 堆积面积图.MC_CHS_COLUMN 柱形图.MC_CHS_STACKEDCOLUMN 堆积柱形图.MC_CHS_BAR 条形图.MC_CHS_STACKEDBAR 堆积条形图}
   Declare Property Style(v As Long)   

End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Property Class_mCtrlChart.Caption() As CWStr
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_mCtrlChart.Caption(ByVal sText As CWStr)
  AfxSetWindowText  hWndControl, sText
End Property

Function Class_mCtrlChart.GetCount() As Long 
  Function = SendMessage( hWndControl , MC_CHM_GETDATASETCOUNT , 0 , 0 )   '默认为 SendMessageW
End Function
Function Class_mCtrlChart.DeleteAll() As Long 
  Function =SendMessage( hWndControl , MC_CHM_DELETEALLDATASETS , 0 , 0 )   '默认为 SendMessageW
End Function 
Function Class_mCtrlChart.Insert(Index As Long, nValue As Integer, nText As CWSTR="", nColor As Long=-1) As Long 
  Dim dataSet as MC_CHDATASET 
  dataSet.dwCount = 1
  dataSet.piValues =@nValue 
  Index = SendMessage(hWndControl, MC_CHM_INSERTDATASET, Index, Cast(LPARAM, @dataSet))
  SendMessage(hWndControl, MC_CHM_SETDATASETLEGENDW, Index, Cast(LPARAM, nText.vptr))
  if nColor<>-1 Then  SendMessage(hWndControl, MC_CHM_SETDATASETCOLOR, Index, nColor) 
  Function = Index
End Function 
Function Class_mCtrlChart.AddItem(nValue As Integer ,nText As CWSTR ="", nColor As Long=-1) As Long
  Dim index As Long = This.GetCount()
  Function = This.Insert(index,nValue ,nText,nColor)
End Function 
Function Class_mCtrlChart.InsertVirtual(Index As Long) As Long 
  Dim dataSet as MC_CHDATASET 
  dataSet.dwCount = 1
  dataSet.piValues =0
  Index = SendMessage(hWndControl, MC_CHM_INSERTDATASET, Index, Cast(LPARAM, @dataSet))
  Function = Index
End Function 
Function Class_mCtrlChart.AddItemVirtual() As Long
  Dim index As Long = This.GetCount()
  Function = This.InsertVirtual(index)
End Function 

Function Class_mCtrlChart.DeleteItem(Index As Long) As Long 
  Function =SendMessage( hWndControl , MC_CHM_DELETEDATASET ,Index , 0 )  
End Function 
Property Class_mCtrlChart.Value(Index As Long) As Integer
  Dim dataSet as MC_CHDATASET  ,Values As Integer 
  dataSet.dwCount = 1   
  dataSet.piValues = @Values
  Dim r As Long = SendMessage(hWndControl, MC_CHM_GETDATASET, Index, Cast(LPARAM, @dataSet))  
  if r <> -1 Then 
     Property = Values
  End if   
End Property 
Property Class_mCtrlChart.Value(Index As Long,nValue As Integer ) 
  Dim dataSet as MC_CHDATASET 
  dataSet.dwCount = 1 
  dataSet.piValues = @nValue  
  SendMessage(hWndControl, MC_CHM_SETDATASET, Index, Cast(LPARAM, @dataSet))  
End Property 
Property Class_mCtrlChart.Colour(Index As Long ) As Long
  Property = SendMessage(hWndControl, MC_CHM_GETDATASETCOLOR, Index, 0)  
End Property 
Property Class_mCtrlChart.Colour(Index As Long,nColor As Long )
  SendMessage(hWndControl, MC_CHM_SETDATASETCOLOR, Index, nColor)  
End Property 
Property Class_mCtrlChart.Text(Index As Long) As CWStr
   Dim wP As Long = MAKEWPARAM(Index, 1024)
   Dim ww As WString * 1025
   SendMessage(hWndControl, MC_CHM_GETDATASETLEGENDW, Index, Cast(LPARAM, @ww))
   Return ww
End Property
Property Class_mCtrlChart.Text(Index As Long, sText As CWStr)
   SendMessage(hWndControl, MC_CHM_SETDATASETLEGENDW, Index, Cast(LPARAM, sText.vptr))
End Property
Property Class_mCtrlChart.FactorExponent(n As Long ) As Long  
  Property = SendMessage(hWndControl, MC_CHM_GETFACTOREXPONENT, n,0)   
End Property
Property Class_mCtrlChart.FactorExponent(n As Long,v As Long  )
   SendMessage(hWndControl, MC_CHM_SETFACTOREXPONENT, n, v)
End Property
Property Class_mCtrlChart.AxisOffset(n As Long ) As Long  
  Property = SendMessage(hWndControl, MC_CHM_GETAXISOFFSET, n,0)   
End Property
Property Class_mCtrlChart.AxisOffset(n As Long,v As Long  )
   SendMessage(hWndControl, MC_CHM_SETAXISOFFSET, n, v)
End Property
Property Class_mCtrlChart.AxisText(n As Long) As CWStr
   Dim wP As Long = MAKEWPARAM(n, 1024)
   Dim ww As WString * 1025
   SendMessage(hWndControl, MC_CHM_GETAXISLEGENDW, n, Cast(LPARAM, @ww))
   Return ww
End Property
Property Class_mCtrlChart.AxisText(n As Long,v As CWStr)
   SendMessage(hWndControl, MC_CHM_SETAXISLEGENDW, n, Cast(LPARAM, v.vptr))
End Property
Property Class_mCtrlChart.Style() As Long
   Dim s As uLong = AfxGetWindowStyle(hWndControl)
   If (s And MC_CHS_SCATTER)       Then Return MC_CHS_SCATTER
   If (s And MC_CHS_LINE)          Then Return MC_CHS_LINE
   If (s And MC_CHS_STACKEDLINE)   Then Return MC_CHS_STACKEDLINE
   If (s And MC_CHS_AREA)          Then Return MC_CHS_AREA
   If (s And MC_CHS_STACKEDAREA)   Then Return MC_CHS_STACKEDAREA
   If (s And MC_CHS_COLUMN)        Then Return MC_CHS_COLUMN
   If (s And MC_CHS_STACKEDCOLUMN) Then Return MC_CHS_STACKEDCOLUMN
   If (s And MC_CHS_BAR)           Then Return MC_CHS_BAR
   If (s And MC_CHS_STACKEDBAR)    Then Return MC_CHS_STACKEDBAR
   Property = MC_CHS_PIE
End Property
Property Class_mCtrlChart.Style(v As Long)
   AfxRemoveWindowStyle hWndControl, 15
   AfxAddWindowStyle hWndControl ,v
End Property 







