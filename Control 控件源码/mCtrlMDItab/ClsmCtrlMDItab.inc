'控件类
#include Once "..\mCtrlPublic\mditab.bi"
Type Class_mCtrlMDItab Extends Class_Control
    Declare Function GetItemCount() As Long         '获取标签数
    Declare Function GetImageList() As HIMAGELIST         '获取绑定的图像列表控件句柄
    Declare Function SetImageList(new_ImageList As HIMAGELIST) As HIMAGELIST   '设置绑定图像列表控件句柄 ，返回上次使用的图像列表控件句柄
    Declare Function DeleteAllItems() As  Long  ' 删除所有标签项。（BOOL）TRUE成功，FALSE否则。
    Declare Function InsertItem(Index As Integer ,zText As CWSTR ,nImg As Long =0,UserData As Integer =0 ) As  Integer  ' 将插入选项卡 （int）新标签的索引，否则-1失败。
    Declare Function AddItem(zText As CWSTR ,nImg As Long =0,UserData As Integer =0 ) As  Integer  ' 新增选项卡 （int）新标签的索引，否则-1失败。
    Declare Property ItemText(Index As Integer) As CWStr              '返回/设置选项卡的文本
    Declare Property ItemText(Index As Integer, sText As CWStr)    
    Declare Property ItemImage(Index As Integer) As Long              '返回/设置选项卡的图像索引（绑定图像列表控件里的位置）
    Declare Property ItemImage(Index As Integer, nImg As Long )    
    Declare Property ItemData(Index As Integer) As Integer               '返回/设置选项卡的附加数据
    Declare Property ItemData(Index As Integer, UserData As Integer)    
    Declare Function DeleteItem(Index As Integer) As Long   ' 删除项目。将有MC_MTN_DELETEITEM事件发生，（BOOL）TRUE成功，FALSE否则。
    Declare Function HitTest(x As Long ,y As Long ) As Integer   ' 测试哪个选项卡（及其部分）放置在指定位置。（int）命中选项卡的索引，或-1。
    Declare Property Selection() As Integer   '返回/设置 当前选择选项卡索引。
    Declare Property Selection(Index As Integer)  
    Declare Function CloseItem(Index As Integer) As Long   ' 要求关闭项目。将有MC_MTN_CLOSEITEM事件发生，然后根据其返回值，是否删除该项目。（BOOL）TRUE成功，FALSE否则。
    Declare Function SetItemWidth(DefWidth As Long ,MinWidth As Long ) As Long   ' 为标签设置默认宽度和最小宽度。用 AfxScaleX(W)来响应系统DPI （BOOL）TRUE成功，FALSE否则。
    Declare Function GetItemDefWidth() As Long   ' 获取默认宽度
    Declare Function GetItemMinWidth() As Long   ' 获取最小宽度
    Declare Function InitStorage(ItemNumber As Long) As Long   ' 预分配内存，若添加很多项目时，避免多次重新分配内存来加快它的速度。（BOOL）TRUE成功，FALSE否则。
    Declare Function GetItemRect(Index As Integer,visible As Long = 0 ) As RECT  ' 获取项目矩形。visible=0 全部 visible<>0 仅获取可见部分
    Declare Function EnsureVisible(Index As Integer) As Long   ' 确保该项目可见。如果看不见，则控件滚动以使其可见。 （BOOL）TRUE成功，FALSE否则。
    Declare Function SetToolTips(Tips As .hWnd) As .hWnd   ' 将工具提示窗口与控件相关联 （HWND）工具提示窗口的句柄，或者NULL如果没有工具提示与控件关联。
    Declare Function GetToolTips() As .hWnd   ' 获取与控件关联的工具提示窗口句柄
    Declare Function CancelDragItem() As Long    ' 获取与控件关联的工具提示窗口句柄
    
    
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------

Function Class_mCtrlMDItab.GetItemCount() As Long  
  Return SendMessage(hWndControl, MC_MTM_GETITEMCOUNT, 0, 0)
End Function

Function Class_mCtrlMDItab.GetImageList() As HIMAGELIST 
  Return Cast(HIMAGELIST, SendMessage(hWndControl, MC_MTM_GETIMAGELIST, 0, 0) )
End Function

Function Class_mCtrlMDItab.SetImageList(new_ImageList As HIMAGELIST) As HIMAGELIST
  Return Cast(HIMAGELIST, SendMessage(hWndControl, MC_MTM_SETIMAGELIST, 0, Cast(lParam,new_ImageList)) )
End Function

Function Class_mCtrlMDItab.DeleteAllItems() As  Long 
  Return SendMessage(hWndControl, MC_MTM_DELETEALLITEMS, 0, 0)
End Function

Function Class_mCtrlMDItab.InsertItem(Index As Integer, zText As CWSTR, nImg As Long = 0, UserData As Integer=0) As Integer 
   Dim pp As MC_MTITEMW 
   pp.dwMask = MC_MTIF_TEXT Or MC_MTIF_IMAGE Or MC_MTIF_PARAM 
   pp.pszText = zText.vptr 
   pp.cchTextMax = Len(zText)
   pp.iImage =nImg 
   pp.lParam =UserData 
  Function = SendMessageW(hWndControl, MC_MTM_INSERTITEMW, Index, Cast(lParam,@pp))
End Function

Function Class_mCtrlMDItab.AddItem(zText As CWSTR ,nImg As Long =0,UserData As Integer =0 ) As  Integer
   Dim index As Integer = This.GetItemCount
  Function = This.InsertItem(index,zText ,nImg,UserData )
End Function

Property Class_mCtrlMDItab.ItemText(Index As Integer) As CWStr
   if index+1> This.GetItemCount Then Return ""
   Dim pp As MC_MTITEMW
   pp.dwMask = MC_MTIF_TEXT 
   Static tt As WString * 261
   memset(@tt ,0, 261*2)
   pp.pszText = @tt
   pp.cchTextMax = 260    
   if SendMessage(hWndControl, MC_MTM_GETITEMW, Index, Cast(lParam, @pp)) Then 
      'Print pp.pszText,@tt 
      Return tt ' Left(*pp.pszText,pp.cchTextMax)   End if    
End Property
Property Class_mCtrlMDItab.ItemText(Index As Integer, zText As CWStr)
   if index+1> This.GetItemCount Then Return 
   Dim pp As MC_MTITEMW
   pp.dwMask = MC_MTIF_TEXT 
   pp.pszText = zText.vptr 
   pp.cchTextMax = Len(zText)   
   SendMessage(hWndControl, MC_MTM_SETITEMW, Index, Cast(lParam, @pp)) 
End Property

Property Class_mCtrlMDItab.ItemImage(Index As Integer) As Long 
   if index+1> This.GetItemCount Then Return 0
   Dim pp As MC_MTITEMW
   pp.dwMask = MC_MTIF_IMAGE 
   if SendMessage(hWndControl, MC_MTM_GETITEMW, Index, Cast(lParam, @pp)) Then 
      Return pp.iImage
   End if    
End Property
Property Class_mCtrlMDItab.ItemImage(Index As Integer, nImg As Long)
   if index+1> This.GetItemCount Then Return 
   Dim pp As MC_MTITEMW
   pp.dwMask = MC_MTIF_IMAGE 
   pp.iImage = nimg  
   SendMessage(hWndControl, MC_MTM_SETITEMW, Index, Cast(lParam, @pp)) 
End Property

Property Class_mCtrlMDItab.ItemData(Index As Integer) As Integer 
   if index+1> This.GetItemCount Then Return 0
   Dim pp As MC_MTITEMW
   pp.dwMask = MC_MTIF_PARAM 
   if SendMessage(hWndControl, MC_MTM_GETITEMW, Index, Cast(lParam, @pp)) Then 
      Return pp.lParam
   End if    
End Property
Property Class_mCtrlMDItab.ItemData(Index As Integer, UserData As Integer)
   if index+1> This.GetItemCount Then Return 
   Dim pp As MC_MTITEMW
   pp.dwMask = MC_MTIF_PARAM 
   pp.lParam = UserData  
   SendMessage(hWndControl, MC_MTM_SETITEMW, Index, Cast(lParam, @pp)) 
End Property

Function Class_mCtrlMDItab.DeleteItem(Index As Integer) As Long
  Function = SendMessage(hWndControl, MC_MTM_DELETEITEM, Index, 0)
End Function

Function Class_mCtrlMDItab.HitTest(x As Long ,y As Long ) As Integer 
   Dim pp As MC_MTHITTESTINFO
   pp.pt.x = x
   pp.pt.y = y
  Function = SendMessage(hWndControl, MC_MTM_DELETEITEM, 0, Cast(lParam, @pp))
End Function

Property Class_mCtrlMDItab.Selection() As Integer
   Property = SendMessage(hWndControl, MC_MTM_GETCURSEL, 0, 0) 
End Property
Property Class_mCtrlMDItab.Selection(Index As Integer)
   SendMessage(hWndControl, MC_MTM_SETCURSEL, Index, 0)
End Property

Function Class_mCtrlMDItab.CloseItem(Index As Integer) As Long 
  Function = SendMessage(hWndControl, MC_MTM_CLOSEITEM, Index, 0)
End Function

Function Class_mCtrlMDItab.SetItemWidth(DefWidth As Long, MinWidth As Long) As Long
   Dim pp As MC_MTITEMWIDTH
   pp.dwDefWidth = DefWidth 
   pp.dwMinWidth = MinWidth 
  Function = SendMessage(hWndControl, MC_MTM_SETITEMWIDTH, 0,  Cast(lParam, @pp))
End Function

Function Class_mCtrlMDItab.GetItemDefWidth() As Long
   Dim pp As MC_MTITEMWIDTH
   SendMessage(hWndControl, MC_MTM_GETITEMWIDTH, 0, Cast(lParam, @pp))
   Return pp.dwDefWidth

End Function

Function Class_mCtrlMDItab.GetItemMinWidth() As Long
   Dim pp As MC_MTITEMWIDTH
   SendMessage(hWndControl, MC_MTM_GETITEMWIDTH, 0, Cast(lParam, @pp))
   Return pp.dwMinWidth
End Function

Function Class_mCtrlMDItab.InitStorage(ItemNumber As Long) As Long 

   Function = SendMessage(hWndControl, MC_MTM_INITSTORAGE, ItemNumber, 0)

End Function

Function Class_mCtrlMDItab.GetItemRect(Index As Integer,visible As Long = 0 ) As RECT
   Dim pp As RECT 
  if index+1> This.GetItemCount Then Return pp 
  SendMessage(hWndControl, MC_MTM_GETITEMRECT, MAKEWPARAM(Index, Cast(Long ,visible=0 )), Cast(lParam, @pp))
  Return pp
End Function

Function Class_mCtrlMDItab.EnsureVisible(Index As Integer) As Long  

  Function = SendMessage(hWndControl, MC_MTM_ENSUREVISIBLE, Index, 0)

End Function

Function Class_mCtrlMDItab.SetToolTips(Tips As .hWnd) As .hWnd

  Function = Cast(.hWnd , SendMessage(hWndControl, MC_MTM_SETTOOLTIPS, Cast(wParam, Tips), 0) )

End Function

Function Class_mCtrlMDItab.GetToolTips() As .HWND

  Function = Cast(.hWnd , SendMessage(hWndControl, MC_MTM_GETTOOLTIPS, 0, 0) )

End Function

Function Class_mCtrlMDItab.CancelDragItem() As Long 

  Function = SendMessage(hWndControl, MC_MTM_CANCELDRAGITEM, 0, 0) 

End Function







