﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP
Style=0 - 无边框
Icon=
Caption=miniblink写HTM界面例题
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=425
Height=608
TopMost=False
Child=False
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=300
MinHeight=300
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Miniblink]
Name=Miniblink1
Enabled=True
Visible=True
Url=about:blank
UserAgent=
HighDPI=True
MemCache=False
NewWin=False
CspCheck=True
NpapiPlus=False
Headless=False
JavaScript=True
Left=13
Top=8
Width=318
Height=526
Layout=0 - 不锚定
Tab=True
Tag=


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

Sub Form1_WM_Create(hWndForm As hWnd ,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   wkeJsBindFunction("eMsg" ,@OnMsg ,null ,5)
   wkeJsBindFunction("eShellExec" ,@OnShellExec ,NULL ,3)
   Miniblink1.LoadFile(App.Path & "view\index.html")
End Sub

Sub Form1_WM_Size(hWndForm As hWnd ,fwSizeType As Long ,nWidth As Long ,nHeight As Long)  '窗口已经改变了大小
   'fwSizeType = SIZE_MAXHIDE     SIZE_MAXIMIZED   SIZE_MAXSHOW    SIZE_MINIMIZED    SIZE_RESTORED
   ''            其他窗口最大化   窗口已最大化     其他窗口恢复    窗口已最小化      窗口已调整大小
   'nWidth nHeight  是客户区大小，不是全部窗口大小。
   if fwSizeType = SIZE_MINIMIZED Then Return
   'xxx.Move AfxScaleX(5), AfxScaleY(5), nWidth - AfxScaleX(10), nHeight - AfxScaleY(30)
   Miniblink1.Move 0 ,0 ,nWidth ,nHeight
End Sub

Function onMsg cdecl(es As jsExecState ,param As Any Ptr) as jsValue
   '注意，必须加 cdecl ，这是 Miniblink调用约定
   dim argCount As Integer = jsArgCount(es)
   if argCount < 1 Then return jsUndefined()
   Dim ntype As jsType = jsArgType(es ,0)
   if ntype <> JSTYPE_STRING Then return jsUndefined()
   Dim arg0      As jsValue = jsArg(es ,0)
   Dim msgOutput As String  = "eMsg:"
   Dim nmsg       As String  = *jsToTempString(es ,arg0)
   msgOutput = msgOutput + nmsg
   PrintA msgOutput
   Select Case nmsg
      Case "close"
         Me.Close
      Case "max"
         Me.WindowState = 2 '最大化
      Case "min"
         Me.WindowState = 1 '最小化
   End Select
   return jsUndefined()
   
End Function

Function onShellExec cdecl(es As jsExecState,param As Any Ptr) as jsValue
   '注意，必须加 cdecl ，这是 Miniblink调用约定
   if jsArgCount(es) = 0 Then return jsUndefined()
   Dim arg0      As jsValue = jsArg(es ,0)
   if jsIsString(arg0) = 0 Then return jsUndefined()
   Dim path As String = *jsToTempStringW(es, arg0)
   if path = "runEchars" Then 
'       createECharsTest()
   ElseIf path = "wkeBrowser" Then
'        wkeBrowserMain(nullptr, nullptr, nullptr, TRUE)
   End if 
   PrintA path
   return jsUndefined()
       
End Function


