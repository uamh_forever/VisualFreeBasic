﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=18
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=345
Height=276
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=主动获取MB里文本框值
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=178
Top=183
Width=138
Height=33
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Miniblink]
Name=Miniblink1
Enabled=True
Visible=True
Url=
UserAgent=
HighDPI=True
MemCache=False
NewWin=False
CspCheck=True
NpapiPlus=False
Headless=False
JavaScript=True
Left=16
Top=11
Width=279
Height=107
Layout=0 - 不锚定
Tab=True
Tag=

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=Text1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=15
Top=137
Width=305
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=Command2
Index=-1
Caption=调用JS函数
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=17
Top=185
Width=126
Height=33
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]

Sub Form1_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   wkeJsBindFunction("MyFunction", @MyFunction, null, 1)
   Miniblink1.LoadFile("tt.htm")
End Sub

Sub Form1_Command1_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd)  '单击
 
   Text1.Text ="主动获取：" & Miniblink1.GetFormIdStr("frm","input1")

End Sub

Sub Form1_Miniblink1_AlertBox(hWndForm As hWnd, hWndControl As hWnd,WebView As wkeWebView,msg As CWSTR)  '网页调用alert
    Text1.Text ="MD提示：" &  msg 
End Sub

Function MyFunction cdecl(es As Any Ptr ,param As Any Ptr) As jsValue 
   '注意，必须加 cdecl ，这是 Miniblink调用约定
    if jsArgCount(es)=0 Then return jsUndefined()
    Dim arg0 As jsValue = jsArg(es ,0)
    if jsIsString(arg0) =0 Then  return jsUndefined()
    Dim ss As WString Ptr = jsToTempStringW(es ,arg0)
    MsgBox *ss ,"来自JS的呼叫"
    Dim rr As jsValue = jsStringW(es ,"给JS回复的消息")
    Return rr 
End Function
Sub Form1_Command2_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd)  '单击
   MsgBox Miniblink1.callJSFunc("jsok" ,"ok")

End Sub























