﻿#VisualFreeBasic_Form#  Version=5.1.10
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=Form1
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Left=0
Top=0
Width=447
Height=418
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=获取网卡
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=340
Top=8
Width=73
Height=29
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[RichEdit]
Name=RichEdit1
Index=-1
Style=3 - 凹边框
TextScrollBars=3 - 垂直和水平
Text=RichEdit
Enabled=True
Visible=True
MaxLength=0
Font=微软雅黑,9
TextAlign=0 - 左对齐
Locked=False
HideSelection=False
Multiline=True
SaveSel=True
NoScroll=False
ContextMenu=True
LeftMargin=0
RightMargin=0
Left=5
Top=44
Width=416
Height=329
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。
#Include Once "win/winbase.bi"
#Include Once "win/iphlpapi.bi"
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim As ULong bufferLength
   Dim As IP_ADAPTER_INFO Ptr pAdapterInfo
   Dim As PIP_ADAPTER_INFO pAdapter
   Dim bb As String
   '根据该函数先故意失败获取,得到链表真正大小
   GetAdaptersInfo(NULL, @bufferLength)
   pAdapterInfo = Allocate(bufferLength)
   '//获取网卡信息结构体第一个节点.
   If GetAdaptersInfo(pAdapterInfo, @bufferLength) Then
      bb = "error IN GetAdaptersInfo"
   Else
      '//因为网卡之间链接成一个单链表,通过一个指针遍历该链表
      pAdapter = pAdapterInfo '->PIP_ADDR_STRING.Next
      While(pAdapter)
      bb &= "---------------------------------------------" & vbCrLf
      bb &= "网卡名:  " & pAdapter->AdapterName & vbCrLf
      bb &= "网卡描述:   " & pAdapter->Description & vbCrLf
      bb &= "网卡MAC地址:  " & pAdapter->AdapterName & vbCrLf
      For I As Integer = 0 To pAdapter->AddressLength
         If (i = (pAdapter->AddressLength - 1)) Then
            bb &= "pAdapter->Address " & pAdapter->Address(i) & vbCrLf
         Else
            bb &= "pAdapter->Address 2 " & pAdapter->Address(i) & vbCrLf
         End If
         
      Next
      bb &= "ip地址:  " & pAdapter->IpAddressList.IpAddress.String & vbCrLf
      bb &= "子网掩码:  " & pAdapter->IpAddressList.IpMask.String & vbCrLf
      bb &= "网关: " & pAdapter->GatewayList.IpAddress.String & vbCrLf
      
      If pAdapter->DhcpEnabled Then
         bb &= "DHCP服务器地址:  " & pAdapter->DhcpServer.IpAddress.String & vbCrLf
      Else
         bb &= "启用DHCP: 否" & vbCrLf
      End If
      pAdapter = pAdapter->Next
   Wend
End if
Deallocate pAdapterInfo
RichEdit1.Text = bb
End Sub


