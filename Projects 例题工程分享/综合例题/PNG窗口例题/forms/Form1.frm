﻿#VisualFreeBasic_Form#  Version=5.8.6
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=Form1
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=302
Height=208
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Timer]
Name=Timer1
Index=-1
Interval=50
Enabled=True
Left=94
Top=87
Tag=


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。
 Dim Shared 计数 as Long 
Sub Form1_Timer1_WM_Timer(hWndForm As hWnd ,wTimerID As Long) '定时器
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   ''         本控件为功能控件，就是无窗口，无显示，只有功能。如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码
   'wTimerID  定时器ID，区分不同定时器的编号
   Dim FName As CWSTR
   计数 += 1
   If 计数 <= 21 Then '因为球体光晕动画有30帧，所以把它设置为30
      FName = "Ball_" & 计数 '依次加载Png
      
   ElseIf 计数 >= 22 And 计数 <= 41 Then
      FName = "Ball_" & 计数 -11
      
   ElseIf 计数 = 42 Then
      FName = "BG_1" '动画完成后，以默认界面出现
      
   ElseIf 计数 > 80 Then
      计数 = 0 '返回变量i，重新设置变量i为0
   End If
   If 计数 < 43 Then
      'Print FName & ".png"
      PNG窗口图像显示(hWndForm ,App.Path & "Pic\" & FName & ".png" ,300 ,300)

   end if
   
End Sub

Sub Form1_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   AfxAddWindowExStyle hWndForm, WS_EX_LAYERED '必须给窗口增加叠层样式，才可以
   'PNG窗口图像显示 (hWndForm , App.Path & "Pic\Ball_1.png")
   'PNG窗口图像显示 (hWndForm , App.Path & "Pic\BG_2.png")
End Sub

Sub PNGwindowImageDisplay(hWndForm As hWnd,PNG_img As String  , m_Width As Long  ,m_Height As Long )

   Dim nDc As HDC = GetDC(hWndForm)
   Dim As HDC hMemDC =CreateCompatibleDC(nDc)

   Dim bmpinfo As BITMAPINFO
   bmpinfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER)
   bmpinfo.bmiHeader.biBitCount = 32
   bmpinfo.bmiHeader.biHeight = m_Height
   bmpinfo.bmiHeader.biWidth = m_Width
   bmpinfo.bmiHeader.biPlanes = 1
   bmpinfo.bmiHeader.biCompression = BI_RGB
   bmpinfo.bmiHeader.biXPelsPerMeter = 0
   bmpinfo.bmiHeader.biYPelsPerMeter = 0
   bmpinfo.bmiHeader.biClrUsed = 0
   bmpinfo.bmiHeader.biClrImportant = 0
   bmpinfo.bmiHeader.biSizeImage = bmpinfo.bmiHeader.biWidth * bmpinfo.bmiHeader.biHeight * bmpinfo.bmiHeader.biBitCount / 8
   Dim pp As Long Ptr
   Dim m_Bmp As HBITMAP = CreateDIBSection(hMemDC, @bmpinfo, DIB_RGB_COLORS, @pp, 0, 0)

   Dim hOldBitmap As HBITMAP = SelectObject(hMemDC, m_Bmp)
   Dim m_GpDC As GpGraphics Ptr ' 主DC 的 GDI+句柄
   GdipCreateFromHDC(hMemDC, @m_GpDC)
   Dim nGpimage As GpImage Ptr ,wFile As Wstring *260 = PNG_img
   GdipLoadImageFromFile(wFile , @nGpimage)
   GdipDrawImageRectRect m_GpDC, nGpimage, 10, 10, 186, 114, 0, 0, 186, 114, 2, NULL, NULL, NULL
   GdipDisposeImage nGpimage
   
   GdipDeleteGraphics(m_GpDC)
   
   Dim ptSrc As POINT
   Dim blf As BLENDFUNCTION
   blf.BlendOp = AC_SRC_OVER
   blf.BlendFlags = 0
   blf.SourceConstantAlpha = 255
   blf.AlphaFormat = AC_SRC_ALPHA
   Dim psize As POINT
   psize.x = m_Width
   psize.y = m_Height
   UpdateLayeredWindow(hWndForm, NULL, NULL, @psize, hMemDC, @ptSrc, 0, @blf, ULW_ALPHA)
   SelectObject(hMemDC, hOldBitmap)
   DeleteObject(m_Bmp)
   DeleteDC(hMemDC)
   ReleaseDC(hWndForm, nDc)
End Sub

Sub Form1_WM_LButtonDown(hWndForm As hWnd ,MouseFlags As Long ,xPos As Long ,yPos As Long) '按下鼠标左键
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'MouseFlags  MK_CONTROL   MK_LBUTTON     MK_MBUTTON     MK_RBUTTON    MK_SHIFT     MK_XBUTTON1       MK_XBUTTON2
   ''           CTRL键按下   鼠标左键按下   鼠标中键按下   鼠标右键按下  SHIFT键按下  第一个X按钮按下   第二个X按钮按下
   '检查什么键按下用  If (MouseFlags And MK_CONTROL)<>0 Then CTRL键按下
   'xPos yPos   当前鼠标位置，相对于窗口。就是在窗口客户区里的坐标。
   Me.MoveWin
End Sub

Sub Form1_WM_RButtonUp(hWndForm As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '释放鼠标右键
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'MouseFlags  MK_CONTROL   MK_LBUTTON     MK_MBUTTON     MK_RBUTTON    MK_SHIFT     MK_XBUTTON1       MK_XBUTTON2 
   ''           CTRL键按下   鼠标左键按下   鼠标中键按下   鼠标右键按下  SHIFT键按下  第一个X按钮按下   第二个X按钮按下
   '检查什么键按下用  If (MouseFlags And MK_CONTROL)<>0 Then CTRL键按下 
   'xPos yPos   当前鼠标位置，相对于窗口。就是在窗口客户区里的坐标。
  Me.Close   
End Sub





