﻿#VisualFreeBasic_Form#  Version=5.0.0
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
Style=3 - 常规窗口
Icon=
Caption=从系统DLL里获取图标
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Left=0
Top=0
Width=500
Height=310
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=&Pick
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=383
Top=220
Width=70
Height=28
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]



'                                                                                  
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Static wszIconPath As WString * MAX_PATH   ' // Path of the resource file containing the icons
  Static nIconIndex As Long                  ' // Icon index
  Static hIcon As HICON                      ' // Icon handle
  If Len(wszIconPath) = 0 Then wszIconPath = AfxGetSystemDllPath("Shell32.dll")
  If Len(wszIconPath) = 0 Then Exit Sub
  '// Activate the Pick Icon Common Dialog Box
  Dim hr As Long = PickIconDlg(0, wszIconPath, SizeOf(wszIconPath), @nIconIndex)
  '// If an icon has been selected...
  If hr = 1 Then
      '// Destroy previously loaded icon, if any
      If hIcon Then DestroyIcon(hIcon)
      '// Get the handle of the new selected icon
      hIcon = ExtractIcon(GetModuleHandle(Null), wszIconPath, nIconIndex)
      '// Replace the application icons
      If hIcon Then
          SendMessage(hWndForm, WM_SETICON, ICON_SMALL, Cast(lParam, hIcon))
          SendMessage(hWndForm, WM_SETICON, ICON_BIG, Cast(lParam, hIcon))
      End If
  End If

End Sub

