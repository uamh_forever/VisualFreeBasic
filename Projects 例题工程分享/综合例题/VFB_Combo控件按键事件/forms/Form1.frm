﻿#VisualFreeBasic_Form#  Version=5.8.6
Locked=0

[Form]
Name=Form1
ClassStyle=CS_DBLCLKS,CS_HREDRAW,CS_VREDRAW
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_RIGHTSCROLLBAR,WS_BORDER,WS_SYSMENU,WS_MAXIMIZEBOX,WS_MINIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CAPTION,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=测试窗口
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=True
Left=0
Top=0
Width=761
Height=534
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ComboBox]
Name=Combo1
Index=-1
Style=1 - 文本框和下拉框
Custom=
OwnDraw=0 - 系统绘制
LabelHeight=20
ItemHeight=18
HasString=False
Sorted=False
NoHeight=False
AutoHscroll=True
MaxLength=0
Uppercase=False
Lowercase=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=70
Top=248
Width=193
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Frame]
Name=Frame1
Index=-1
Caption=Frame1
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=362
Top=51
Width=247
Height=150
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=Command1
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=False
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=398
Top=144
Width=121
Height=32
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=Command2
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=84
Top=65
Width=183
Height=74
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Picture]
Name=Picture1
Index=-1
Style=2 - 半边框
Enabled=True
Visible=True
Left=49
Top=25
Width=291
Height=179
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]

Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   Dim ss As String = GetResourceStr("FONT_ICONFONT")
   Dim As Long uu
   Dim ff As Any Ptr = AddFontMemResourceEx(StrPtr(ss) ,Len(ss) ,0 ,@uu) '加载字体

End Sub

Sub Form1_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   Dim 窗口句柄 As hWnd = FindWindowEx(Combo1.hWnd ,0 ,"Edit" ,NULL)
   Dim 原Proc As Any Ptr '原处理地址 
   If 窗口句柄 Then
      原Proc = Cast(Any Ptr ,GetWindowLongPtr(窗口句柄 ,GWLP_WNDPROC)) '32位和64位软件通用，获取处理地址
      SetWindowLongPtr(窗口句柄 ,GWLP_WNDPROC ,Cast(UInteger ,@SubClassProcess)) '32位和64位软件通用，设置新处理地址
      SetPropW (窗口句柄 ,"ZOLDWNDPROC",原Proc)
   End If
End Sub

Function SubClassProcess(ByVal hWndForm As hWnd ,ByVal wMsg As ULong ,ByVal nwParam As wParam ,ByVal nlParam As lParam) As LResult
   '子类化处理函数
   Select Case wMsg
      Case WM_KEYUP
         Print "WM_KEYUP" ,nwParam
      Case WM_KEYDOWN
         Print "WM_KEYDOWN" ,nwParam
   End Select
   Function = CallWindowProc(GetPropW(hWndForm, "ZOLDWNDPROC") ,hWndForm ,wMsg ,nwParam ,nlParam) '给原处理地址处理消息 ，这是必须的，不然就吃掉消息。
End Function






























































































































































