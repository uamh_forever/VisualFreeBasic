'GDI常用封装类，其它API 可以直接用 xx.m_Dc 调用API
'为了加速GDI 画画速度，增加了 内存DC 用来缓存，不然画太多东西屏幕会闪，
'调用中不需要知道内存DC，只是其它API操作，用 xx.m_Dc 内存DC来画

'WM_PAINT 消息里用  Dim gg As yGDI = yGDI(窗口句柄,True)
'其它地方画画用    Dim gg As yGDI = 窗口句柄
'GDI分为：笔、刷、字体、位图、




Type yGDI
Private : 
    m_ps As PAINTSTRUCT    '绘画范围，就是排除被遮挡部分，系统处理，我们只要这么写就可以了。
    m_nDC As hDC           '目标控件或窗口的DC，先画到内存DC，销毁类前自动复制到这个目标DC里，为了加速画画
    m_hOldBmp As HBITMAP   ' 旧的位图句柄
    m_hBmp2 As HBITMAP     ' 内存DC的位图句柄
    m_hOldBmp2 As HBITMAP  ' 旧的位图句柄
    tFont As HFONT         '字
    nPen As HGDIOBJ        '笔
    nBrush As HBRUSH       '刷
Public : 
    nGpPen As GpPen Ptr    'GDI+ 笔 
    nGpBrush As GpBrush Ptr  'GDI+ 刷
    nGpimage As GpImage Ptr  'GDI+ 位图，用来画图，加载图片存在这里   
    Declare Sub Redraw()      '立即刷新显示画面，不然要等销毁本类时才显示
    'GDI 画图
    Declare Sub Pen(ByVal cWidth As Long, ByVal crColor As COLORREF,ByVal iStyle As Long =PS_SOLID ) '设置笔，画框线用
    Declare Sub Brush(ByVal crColor As COLORREF = -1) '设置刷子，用于填充颜色 -1为空刷子
    Declare Sub BrushHatch(fnStyle As Long , crColor As COLORREF = 0) '设置 样式 刷子，用于填充 
    Declare Sub DrawLine(ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) '画线
    Declare Sub DrawArc(ByVal X As Long, ByVal Y As Long, ByVal W As Long, ByVal H As Long, ByVal StartAngle As Single , ByVal EndAngle As Single) '画圆弧
    Declare Sub DrawPolyline cdecl(ByVal Count As Long , ...) '画一系列线段
    Declare Sub DrawFrame(ByVal X As Long, ByVal Y As Long, ByVal W As Long, ByVal H As Long) '画框
    Declare Sub DrawCircleFrame(ByVal X As Long, ByVal Y As Long, ByVal W As Long, ByVal H As Long, ByVal wC As Long, ByVal hC As Long) '描绘一个圆角矩形
    Declare Sub DrawEllipse(ByVal X As Long, ByVal Y As Long, ByVal W As Long, ByVal H As Long) '描绘一个椭圆
    'GDI 文字
    Declare Sub Font(fName As String, ByVal fSize As Long = 9, ByVal Bold As UByte = False, Italic As UByte = False, ByVal Underline As UByte = False, ByVal StrikeOut As UByte = False) '设置字体
    Declare Sub SetColor(ByVal tColor As COLORREF, ByVal bColor As COLORREF = -1) '设置文本颜色
    Declare Sub DrawString(ByVal X As Long, ByVal Y As Long, nText As String) '描绘制指定的文本字符串
    Declare Sub DrawTextS(X As Long, Y As Long, W As Long, H As Long, nText As String, uFormat As ULong = 0) '指定的矩形中绘制格式化的文本
    Declare Sub DrawFrameStr(ByVal X As Long, ByVal Y As Long, nText As String) '描绘描边字，轮廓字，轮廓先设置笔，字为正常字
    'GDI 位图
    Declare Sub InvertPixels(X As Long, Y As Long, W As Long, H As Long) '将指定的矩形中像素颜色翻转
      Declare Function GetPixel (ByVal x As Long, ByVal y As Long) As COLORREF '获取像素点颜色
      Declare Function SetPixel (ByVal x As Long, ByVal y As Long, ByVal crColor As COLORREF) As COLORREF '设置像素点颜色
    Declare Sub DrawImg( hInst As HINSTANCE,ResName As String, X As Long, Y As Long, W As Long = 0, H As Long = 0, dimPercent As Long = 0, bGrayScale As Long = False) '直接画资源里的图片
    Declare Sub DrawImg(FileName As String, X As Long, Y As Long, W As Long = 0, H As Long = 0, dimPercent As Long = 0, bGrayScale As Long = False) '直接画文件的图片
    Declare Sub LoadImg(FileName As String, dimPercent As Long = 0, bGrayScale As Long = False) '加载文件图像到 m_bDc ，便于后面重复使用，再次加载将消耗前面加载的。
    Declare Sub LoadImg( hInst As HINSTANCE, ResName As String, dimPercent As Long = 0, bGrayScale As Long = False) '加载资源图像到 m_bDc ，便于后面重复使用，再次加载将消耗前面加载的。
    Declare Sub DrawCopyImg(xDest As Long, yDest As Long, wDest As Long, hDest As Long,  xSrc As Long,  ySrc As Long,  wSrc As Long,  hSrc As Long, rop As DWord=SrcCopy) '描绘由LoadImg加载来的图,主要用来像画工具栏里的小按钮
    Declare Sub DrawCopyImgAlpha(xDest As Long, yDest As Long, wDest As Long, hDest As Long, xSrc As Long, ySrc As Long, wSrc As Long, hSrc As Long, TeMeDu As Long ) '半透明 描绘由LoadImg加载来的图
    Declare Sub DrawCopyImgColor(xDest As Long, yDest As Long, wDest As Long, hDest As Long, xSrc As Long, ySrc As Long, wSrc As Long, hSrc As Long, tColor As Long ) '扣色 描绘由LoadImg加载来的图
   'GDI+ 画图 
    Declare Sub GpPen(ByVal nWidth As REAL,ByVal nColor As ARGB=&HFF000000) 'GDI+ 设置笔，画框线用 基本笔，颜色和笔粗
    Declare Sub GpBrush(ByVal nColor As ARGB = 0) 'GDI+ 设置纯色刷子，用于填充颜色 0为空刷子
    Declare Sub GpBrush(X1 As Long ,Y1 As Long,X2 As Long,Y2 As Long,Color1 As ARGB,Color2 As ARGB,WrapMode As Long ) 'GDI+ 渐变
    Declare Sub GpDrawLine( X1 As Single,  Y1 As Single,  X2 As Single, Y2 As Single) 'GDI+ 画线
    Declare Sub GpDrawArc(X As Single, Y As Single, W As Single, H As Single, StartAngle As Single , sweepAngle As Single) 'GDI+ 画圆弧
    Declare Sub GpDrawFrame(X As Single, Y As Single, W As Single, H As Single) 'GDI+ 画框
    Declare Sub GpDrawCircleFrame(X As Single, Y As Single, W As Single, H As Single, wC As Single, hC As Single) 'GDI+ 描绘一个圆角矩形
    Declare Sub GpDrawEllipse(X As Single, Y As Single, W As Single, H As Single) 'GDI+ 描绘一个椭圆
    Declare Sub GpDrawFrameStr(X As Single, Y As Single, nText As String,hFont As String="宋体",hSize As Long =12) '描绘描边字，轮廓字，轮廓先设置笔，字为正常字
   
    Declare Function GpLoadImg(FileName As String) As Long '加载文件图像到 nGpimage ，便于后面重复使用，再次加载将消耗前面加载的。
    Declare Function GpLoadImg(hInst As HINSTANCE, ResName As String) As Long '加载资源图像到 nGpimage ，便于后面重复使用，再次加载将消耗前面加载的。
    Declare Sub GpDrawCopyImg(xDest As Single, yDest As Single, wDest As Single, hDest As Single,  xSrc As Single,  ySrc As Single,  wSrc As Single,  hSrc As Single) '描绘由LoadImg加载来的图,主要用来像画工具栏里的小按钮


    Declare Function SaveBitmap(ByRef wszFileName As WString, ByRef wszMimeType As WString = "image/bmp") As Long
    Declare Function PrintBitmap(ByVal bStretch As BOOLEAN = False, ByVal nStretchMode As Long = InterpolationModeHighQualityBicubic) As BOOLEAN
    Declare Constructor(ByVal hWndForm As HWnd = 0, clrBkg As COLORREF = -1, ByVal YN_WM_PAINT As Long = False)
    Declare Destructor
    
    m_hBmp As HBITMAP             ' 内存DC的位图句柄
    m_Dc As hDC                   ' 内存DC 兼容设备上下文句柄
    m_bDc As hDC                  ' 内存DC 用于加载文件或资源里的图像
    m_GpDC As GpGraphics Ptr 'GDI+ 的DC
    m_hWndForm As HWnd    '目标控件或窗口的句柄
    m_nWidth  As Long     '目标控件或窗口的客户区矩形的大小
    m_nHeight As Long     '目标控件或窗口的客户区矩形的大小
    m_YN As Long          '是绘图消息还是普通的画画
    m_DpiX As Single      'DPI支持，默认支持高DPI，画画时按100%写，自动转换支持高DPI，设置 =1 不支持
    m_DpiY As Single
  
End Type


'--------------------------------------------------------------------------------
Private Constructor yGDI(ByVal hWndForm As HWnd = 0, clrBkg As COLORREF = -1, ByVal YN_WM_PAINT As Long = False)
  '  Print "创建GDI"
  If IsWindow(hWndForm) Then
      m_hWndForm = hWndForm
      Dim rc As Rect
      m_YN = YN_WM_PAINT
      If YN_WM_PAINT Then
          m_nDC = BeginPaint(hWndForm, @m_ps) '获取需要绘画DC，推荐此方法，绘图效率高
      Else
          m_nDC = GetDC(hWndForm)
      End If
      GetClientRect(hWndForm, @rc)
      m_nWidth = (rc.Right - rc.Left) + 1
      m_nHeight = (rc.Bottom - rc.Top) + 1
      
      ' // 创建一个兼容的位图
      m_hBmp = CreateCompatibleBitmap(m_nDC, m_nWidth, m_nHeight)
      ' // 创建兼容的设备上下文
      m_Dc = CreateCompatibleDC(m_nDC)
      ' // 将位图选择到兼容的设备上下文中
      If m_Dc Then m_hOldBmp = SelectObject(m_Dc, m_hBmp)
      
      ' // 绘制位图的背景
      If clrBkg = -1 Then
          BitBlt m_Dc, 0, 0, m_nWidth, m_nHeight, m_nDC, 0, 0, SrcCopy '将原内容获取到内存DC
      Else
          Dim coord As RECT
          SetRect(@coord, 0, 0, m_nWidth, m_nHeight)
          
          Dim hBrush As HBRUSH = CreateSolidBrush(clrBkg)
          If hBRush Then
              FillRect(m_Dc, @coord, hBrush)
              DeleteObject hBrush
          End If
      End If
      SetBkMode m_Dc, 1  '设置这个后，画上的字是透明的
      GdipCreateFromHDC(m_Dc, @m_GpDC)
      GdipSetSmoothingMode m_GpDC,SmoothingModeAntiAlias
  End If
  m_DpiX = AfxScaleX(1)
  m_DpiY = AfxScaleY(1)
  This.Font("Microsoft YaHei")
  This.GpPen(1) '设置默认笔
'  This.Brush(-1) '默认不填充 
End Constructor
'--------------------------------------------------------------------------------
Destructor yGDI
  'Print "销毁GDI"
   This.Redraw
   If m_YN Then 
   EndPaint(m_hWndForm,@m_ps) '完成绘图
   Else
   ReleaseDC  m_hWndForm,m_nDC
   End If
   ' // 恢复原始位图
   SelectObject(m_Dc, m_hOldBmp)
   ' // 销毁位图和内存设备上下文
   If m_hBmp Then DeleteObject m_hBmp
   If m_hBmp2 Then DeleteObject m_hBmp2
   If m_hOldBmp2 Then SelectObject(m_bDc, m_hOldBmp2)
   If m_Dc Then DeleteDC m_Dc
   If m_bDc Then DeleteDC m_bDc
   If tFont Then DeleteObject tFont
   If nPen Then  DeleteObject nPen    '画笔
   If nBrush Then DeleteObject nBrush 
   If m_GpDC Then GdipDeleteGraphics(m_GpDC) 
   If nGpPen Then GdipDeletePen nGpPen
   If nGpBrush Then GdipDeleteBrush nGpBrush 'GDI+ 刷
   If nGpimage Then GdipDisposeImage nGpimage
End Destructor

'--------------------------------------------------------------------------------
Private Sub yGDI.Redraw () 
  BitBlt m_nDC,0,0,m_nWidth,m_nHeight,m_Dc,0,0,SrcCopy '将内存DC，输出到控件
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.Pen(ByVal cWidth As Long, ByVal crColor As COLORREF =0 ,ByVal iStyle As Long =PS_SOLID ) '设置笔，画框线用
'nPenStyle ------ Long，指定画笔样式，可以是下述常数之一
'PS_SOLID        画笔画出的是实线
'PS_DASH         画笔画出的是虚线（nWidth必须不大于1）
'PS_DOT          画笔画出的是点线（nWidth必须不大于1）
'PS_DASHDOT      画笔画出的是点划线（nWidth必须不大于1）
'PS_DASHDOTDOT   画笔画出的是点-点-划线（nWidth必须不大于1）
'PS_NULL         画笔不能画图
'PS_INSIDEFRAME  由椭圆、矩形、圆角矩形、饼图以及弦等生成的封闭对象框时，画线宽度向内扩展。如指定的准确RGB颜色不存在，就进行抖动处理
  Dim As HGDIOBJ hOldPen
  nPen = CreatePen(iStyle, cWidth * m_DpiX, crColor) '创建新画笔
  hOldPen = SelectObject(m_Dc, nPen)    '画笔存入DC
  DeleteObject hOldPen    '销毁旧的画笔
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.Brush(ByVal crColor As COLORREF) '设置刷子，用于填充颜色
'crColor 颜色，=-1 创建空画刷子，才能画出空心来
  Dim hOldBrush As HGDIOBJ
  If crColor <> -1 Then
      nBrush = CreateSolidBrush(crColor)
  Else
      nBrush = GetStockObject(HOLLOW_BRUSH) '创建空画刷子，才能画出空心来
  End If
  hOldBrush = SelectObject(m_Dc,nBrush )
  DeleteObject hOldBrush
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.BrushHatch(fnStyle As Long , crColor As COLORREF = 0) '设置 样式 刷子，用于填充
'fnStyle  
'HS_BDIAGONAL     从右到左45度 斜纹
'HS_CROSS         十字线
'HS_DIAGCROSS    45度十字线
'HS_FDIAGONAL     从左到右45度 斜纹
'HS_HORIZONTAL   水平纹
'HS_VERTICAL     垂直纹
'crColor 颜色
  Dim hOldBrush As HGDIOBJ
  nBrush = CreateHatchBrush(fnStyle,crColor)
  hOldBrush = SelectObject(m_Dc,nBrush )
  DeleteObject hOldBrush
End Sub



'--------------------------------------------------------------------------------
Private Sub yGDI.DrawLine(ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long)
  Dim nP(1) As Point
  '    MoveToEx m_Dc,X1,Y1,@lpPoint             '设置起点
  '    LineTo m_Dc,X2,Y2                      '画到终点
  nP(0).x = x1 * m_DpiX
  nP(0).y = y1 * m_DpiY
  nP(1).x = x2 * m_DpiX
  np(1).y = y2 * m_DpiY
  Polyline m_Dc, @nP(0), 2
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawArc(ByVal X As Long, ByVal Y As Long, ByVal W As Long, ByVal H As Long, ByVal StartAngle As Single , ByVal EndAngle As Single) '画圆弧
'x y w h 一个假象的圆，位置和宽度高度
'StartAngle  相对于x轴的起始角度（以度为单位）。0度为原的右边中部，
'EndAngle    结束角度
'
  Dim As Long nXStartArc,nYStartArc,nXEndArc,nYEndArc
  Dim pi As Double  =3.1415926535897932
  Dim As Long zx,zy
'  x=中心x + w半径 * Cos(角度)
'  y=中心y + h半径 * Sin(角度)
   zx=(x+w/2)
   zy=(y+h/2)
    nXStartArc=zx+(w/2)*Cos(StartAngle * pi/180)
    nYStartArc=zy+(h/2)*Sin(StartAngle * pi/180)
    nXEndArc=(x+w/2)+(w/2)*Cos(EndAngle * pi/180)
    nYEndArc=(y+h/2)+(h/2)*Sin(EndAngle * pi/180)
    
  Arc m_Dc, x* m_DpiX, y* m_DpiY, (x+w)* m_DpiX,(y+h)* m_DpiY, nXStartArc, nYStartArc,nXEndArc,nYEndArc
'  AngleArc m_Dc, x, y, x+w,y+h, nXStartArc, nYStartArc,nXEndArc,nYEndArc
  
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawPolyline cdecl(ByVal Count As Long,...) '画一系列线段
  Dim nP() As Point
  ReDim nP(Count-1)
  '    MoveToEx m_Dc,X1,Y1,@lpPoint             '设置起点
  '    LineTo m_Dc,X2,Y2                      '画到终点
  Dim arg As Any Ptr
  Dim i As Integer  
  arg = va_first()
  For i=0 To Count-1
    nP(i).x=va_arg(arg, Long) * m_DpiX
    arg = va_next(arg, Long)
    nP(i).y=va_arg(arg, Long) * m_DpiY
    arg = va_next(arg, Long)
  Next 
  Polyline m_Dc, @nP(0), Count
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawFrame(ByVal X As Long, ByVal Y As Long, ByVal W As Long, ByVal H As Long)
  Rectangle m_Dc, X * m_DpiX, Y * m_DpiY, (X + W) * m_DpiX, (Y + H) * m_DpiY
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawEllipse(ByVal X As Long, ByVal Y As Long, ByVal W As Long, ByVal H As Long)
  Ellipse m_Dc, X * m_DpiX, Y * m_DpiY, (X + W) * m_DpiX, (Y + H) * m_DpiY
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawCircleFrame(ByVal X As Long, ByVal Y As Long, ByVal W As Long, ByVal H As Long,ByVal wC As Long,ByVal hC As Long) '描绘一个圆角矩形
  RoundRect m_Dc, X * m_DpiX, Y * m_DpiY, (X + W) * m_DpiX, (Y + H) * m_DpiY,wC * m_DpiX,hC * m_DpiY
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.Font(fName As String,ByVal  fSize As Long = 9,ByVal Bold As UByte = False, Italic As UByte = False, ByVal Underline As UByte = False, ByVal StrikeOut As UByte = False)

  Dim oFont As HGDIOBJ
  Dim nDPI As Long, nBold As Long
  If m_DpiX = 1 Then nDPI = 96 Else nDPI = -1
  If Bold Then nBold =FW_BOLD  Else  nBold = FW_NORMAL
  tFont = AfxCreateFont(fName, fSize, nDPI, nBold, Italic, Underline, StrikeOut)
  oFont = SelectObject(m_Dc, tFont)
  DeleteObject oFont '销毁老字体
  
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawString(ByVal X As Long, ByVal Y As Long,nText As String ) '描绘制指定的文本字符串
  TextOut m_Dc, X * m_DpiX, Y * m_DpiY, StrPtr(nText),Len(nText)
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawTexts(X As Long ,Y As Long ,W As Long ,H As Long ,nText As String ,uFormat As ULong =0 ) '指定的矩形中绘制格式化的文本
'uFormat 指定格式化文本的方法。
'DT_BOTTOM  将文本对齐到矩形的底部。该值仅与DT_SINGLELINE值一起使用。
'DT_LEFT    将文本对准左侧。(默认)
'DT_RIGHT   将文本对齐到右侧。
'DT_TOP     将文本对齐到矩形的顶部。(默认)
'DT_CENTER  在矩形中居中文字。

'DT_CALCRECT 返回格式化文本的高度，但不绘制文本。
'DT_EDITCONTROL 复制多行编辑控件的文本显示特性。具体而言，以与编辑控件相同的方式计算平均字符宽度，并且该函数不显示部分可见的最后一行。
'DT_END_ELLIPSIS 如果字符串的末尾不适合矩形，则会被截断，并添加省略号。
'DT_HIDEPREFIX   忽略文本中的＆符号（&）前缀字符。
'DT_NOPREFIX     关闭前缀字符的处理。
'DT_INTERNAL     使用系统字体来计算文本度量。
'DT_MODIFYSTRING 修改指定的字符串以匹配显示的文本。
'DT_PATH_ELLIPSIS 对于显示的文本，用椭圆替换字符串中间的字符，以使结果符合指定的矩形。
'DT_RTLREADING   右向左阅读顺序布局
'DT_SINGLELINE   仅在一行上显示文字。
'DT_TABSTOP      设置制表位。
'DT_VCENTER      纵向中心文本。该值仅与DT_SINGLELINE值一起使用。
'DT_WORDBREAK    自动换行 
'DT_WORD_ELLIPSIS 截断任何不适合矩形的单词并添加省略号。

  Dim rr As rect 
  rr.Left =x *m_DpiX
  rr.top =y  * m_DpiY
  rr.Right =(x+w)*m_DpiX
  rr.bottom =(y+h)* m_DpiY
  DrawText m_Dc, StrPtr(nText),Len(nText),@rr,uFormat
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawFrameStr(ByVal X As Long, ByVal Y As Long, nText As String) '描绘描边字，轮廓字，轮廓先设置笔，字为正常字
  BeginPath(m_Dc) ';//开始轨迹
  TextOut m_Dc, X * m_DpiX, Y * m_DpiY, StrPtr(nText), Len(nText)
  'FlattenPath(m_Dc) ';//用直线封闭轨迹
  EndPath(m_Dc) ';//结束轨迹
  StrokePath(m_Dc) ' ;  //画出轨迹
  'FillPath(gg.m_Dc) ' ;  //画出轨迹
  TextOut m_Dc, X * m_DpiX, Y * m_DpiY, StrPtr(nText), Len(nText) '再画字
  
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.SetColor(ByVal tColor As COLORREF, ByVal bColor As COLORREF = -1)
  ' bColor=-1  文字背景不设置，画字是透明的。
  SetTextColor m_Dc, tColor
  If bColor = -1 Then
      SetBkMode m_Dc, TRANSPARENT  '背景保持不变,设置这个后，画上的字是透明的
  Else
      SetBkMode m_Dc, OPAQUE  '在绘制文本，阴影画笔或钢笔之前，背景填充当前的背景颜色。
      SetBkColor m_Dc, bColor
      
  End If
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.InvertPixels(X As Long, Y As Long, W As Long, H As Long) '将指定的矩形中像素颜色翻转
  Dim rr As rect 
  rr.Left =x *m_DpiX
  rr.top =y  * m_DpiY
  rr.Right =(x+w)*m_DpiX
  rr.bottom =(y+h)* m_DpiY
  InvertRect m_Dc, @rr
  
End Sub
'--------------------------------------------------------------------------------
Private Function yGDI.GetPixel (ByVal x As Long, ByVal y As Long) As COLORREF
   Function = .GetPixel(m_Dc, x, y)
End Function
'--------------------------------------------------------------------------------
Private Function yGDI.SetPixel (ByVal x As Long, ByVal y As Long, ByVal crColor As COLORREF) As COLORREF
   Function = .SetPixel(m_Dc, x, y, crColor) '返回原来像素点的颜色
End Function
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawImg(hInst As HINSTANCE,ResName As String, X As Long, Y As Long, W As Long = 0, H As Long = 0, dimPercent As Long = 0, bGrayScale As Long = False) '画资源里的图片
  'ResName  图像库中图像的名称。只有嵌入为原始数据（RCDATA类型）的图像才有效。 必须是格式为.png，.jpg，.gif，.tiff的图
  'dimPercent     [in] 调光的百分比 (1-99)
  'bGrayScale     [in] True 或 False. 转换成灰度.
  Dim nWidth As Long, nHeight As Long, bm As BITMAP
  ' 将图像加载为位图
  Dim hbmp As HICON = AfxGdipIconFromRes(hInst, ResName, dimPercent, bGrayScale)
  If hbmp Then
      DrawIconEx m_Dc, x *m_DpiX, y * m_DpiY, hbmp, w *m_DpiX, h * m_DpiY, Null, Null, DI_NORMAL
      DeleteObject hbmp
  End If
  
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawImg(FileName As String, X As Long, Y As Long, W As Long = 0, H As Long = 0, dimPercent As Long = 0, bGrayScale As Long = False) '画资源里的图片
  'FileName  文件的绝对路径 必须是格式为.png，.jpg，.gif，.tiff的图
  'dimPercent     [in] 调光的百分比 (1-99)
  'bGrayScale     [in] True 或 False. 转换成灰度.
  Dim nWidth As Long, nHeight As Long, bm As BITMAP
  ' 将图像加载为位图
  Dim hbmp As HICON = AfxGdipIconFromFile(FileName, dimPercent, bGrayScale)
  If hbmp Then
      DrawIconEx m_Dc, x *m_DpiX, y * m_DpiY, hbmp, w *m_DpiX, h * m_DpiY, Null, Null, DI_NORMAL
      DeleteObject hbmp
  End If
  
End Sub

' ========================================================================================
' 将位图保存到文件。
' 参数:
' - wszFileName = 要保存的图像的路径名称。
' - wszMimeType = MIME类型 (默认: "image/bmp").
'   "image/bmp" = Bitmap (.bmp)
'   "image/gif" = GIF (.gif)
'   "image/jpeg" = JPEG (.jpg)
'   "image/png" = PNG (.png)
'   "image/tiff" = TIFF (.tiff)
' 返回值:
' 如果方法成功，则返回Ok，它是Status枚举的一个元素。
' 如果方法失败，它将返回Status枚举的其他元素之一。
' ========================================================================================
Private Function yGDI.SaveBitmap (ByRef wszFileName As WString, ByRef wszMimeType As WString = "image/bmp") As Long
   Function = AfxGdipSaveHBITMAPToFile(m_hBmp, wszFileName, wszMimeType)
End Function
' ========================================================================================
' 在默认打印机中打印位图。
' 参数:
' - bStretch     = 拉伸图像。
' - nStretchMode = 拉伸模式。 默认值 = InterpolationModeHighQualityBicubic.
'   InterpolationModeLowQuality = 1
'   InterpolationModeHighQuality = 2
'   InterpolationModeBilinear = 3
'   InterpolationModeBicubic = 4
'   InterpolationModeNearestNeighbor = 5
'   InterpolationModeHighQualityBilinear = 6
'   InterpolationModeHighQualityBicubic = 7
' 返回值：如果位图已成功打印，则返回TRUE，否则返回FALSE。
' ========================================================================================
Private Function yGDI.PrintBitmap (ByVal bStretch As BOOLEAN = False, ByVal nStretchMode As Long = InterpolationModeHighQualityBicubic) As BOOLEAN
   Function = AfxGdipPrintHBITMAP(m_hBmp, bStretch, nStretchMode)
End Function

'--------------------------------------------------------------------------------
Private Sub yGDI.LoadImg( hInst As HINSTANCE, ResName As String, dimPercent As Long = 0, bGrayScale As Long = False) '加载图像到 m_bDc ，便于后面重复使用，再次加载将消耗前面加载的。
  'FileName  文件的绝对路径 必须是格式为.png，.jpg，.gif，.tiff的图
  'dimPercent     [in] 调光的百分比 (1-99)
  'bGrayScale     [in] True 或 False. 转换成灰度.
  Dim nWidth As Long, nHeight As Long, bm As BITMAP
  ' 将图像加载为位图
   If m_hBmp2 Then DeleteObject m_hBmp2
   If m_hOldBmp2 Then SelectObject(m_bDc, m_hOldBmp2)
   If m_bDc Then DeleteDC m_bDc
   

  Dim m_hBmp2 As HBITMAP = AfxGdipBitmapFromRes(hInst, ResName, dimPercent, bGrayScale)
   If m_hBmp2 Then
      ' 检索位图的宽度和高度
      If GetObject(m_hBmp2, SizeOf(BITMAP), @bm) Then
         nWidth = bm.bmWidth
         nHeight = bm.bmHeight
      End If
      m_bDc = CreateCompatibleDC(m_Dc)
      m_hOldBmp2 = SelectObject(m_bDc, m_hBmp2)
   End If


End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.LoadImg( FileName As String, dimPercent As Long = 0, bGrayScale As Long = False) '加载图像到 m_bDc ，便于后面重复使用，再次加载将消耗前面加载的。
  'FileName  文件的绝对路径 必须是格式为.png，.jpg，.gif，.tiff的图
  'dimPercent     [in] 调光的百分比 (1-99)
  'bGrayScale     [in] True 或 False. 转换成灰度.
  Dim nWidth As Long, nHeight As Long, bm As BITMAP
  ' 将图像加载为位图
   If m_hBmp2 Then DeleteObject m_hBmp2
   If m_hOldBmp2 Then SelectObject(m_bDc, m_hOldBmp2)
   If m_bDc Then DeleteDC m_bDc

  Dim m_hBmp2 As HBITMAP = AfxGdipBitmapFromFile( FileName, dimPercent, bGrayScale)
   If m_hBmp2 Then
      ' 检索位图的宽度和高度
      If GetObject(m_hBmp2, SizeOf(BITMAP), @bm) Then
         nWidth = bm.bmWidth
         nHeight = bm.bmHeight
      End If
      m_bDc = CreateCompatibleDC(m_Dc)
      m_hOldBmp2 = SelectObject(m_bDc, m_hBmp2)
   End If
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawCopyImg(xDest As Long, yDest As Long, wDest As Long, hDest As Long, xSrc As Long, ySrc As Long, wSrc As Long, hSrc As Long, rop As DWord = SrcCopy) '描绘由LoadImg加载来的图
  'xywh Dest 目标  xywh Src 源
  'rop 光栅操作代码
  'BLACKNESS：表示使用与物理调色板的索引0相关的色彩来填充目标矩形区域，（对缺省的物理调色板而言，该颜色为黑色）。
  'DSTINVERT：表示使目标矩形区域颜色取反。
  'MERGECOPY：表示使用布尔型的And（与）操作符将源矩形区域的颜色与特定模式组合一起。
  'MERGEPAINT：通过使用布尔型的Or（或）操作符将反向的源矩形区域的颜色与目标矩形区域的颜色合并。
  'NOTSRCCOPY：将源矩形区域颜色取反，于拷贝到目标矩形区域。
  'NOTSRCERASE：使用布尔类型的Or（或）操作符组合源和目标矩形区域的颜色值，然后将合成的颜色取反。
  'PATCOPY：将特定的模式拷贝到目标位图上。
  'PATPAINT：通过使用布尔Or（或）操作符将源矩形区域取反后的颜色值与特定模式的颜色合并。然后使用Or（或）操作符将该操作的结果与目标矩形区域内的颜色合并。
  'PATINVERT：通过使用Xor（异或）操作符将源和目标矩形区域内的颜色合并。
  'SRCAND：通过使用And（与）操作符来将源和目标矩形区域内的颜色合并。
  'SRCCOPY：将源矩形区域直接拷贝到目标矩形区域。
  'SRCERASE：通过使用And（与）操作符将目标矩形区域颜色取反后与源矩形区域的颜色值合并。
  'SRCINVERT：通过使用布尔型的Xor（异或）操作符将源和目标矩形区域的颜色合并。
  'SRCPAINT：通过使用布尔型的Or（或）操作符将源和目标矩形区域的颜色合并。
  'WHITENESS：使用与物理调色板中索引1有关的颜色填充目标矩形区域。（对于缺省物理调色板来说，这个颜色就是白色）。
  StretchBlt(m_Dc, xDest *m_DpiX, yDest *m_DpiY, wDest *m_DpiX, hDest *m_DpiY, m_bDc, xSrc , ySrc , wSrc , hSrc , rop)
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawCopyImgAlpha(xDest As Long, yDest As Long, wDest As Long, hDest As Long, xSrc As Long, ySrc As Long, wSrc As Long, hSrc As Long, TeMeDu As Long ) '半透明 描绘由LoadImg加载来的图
  'xywh Dest 目标  xywh Src 源
  'TeMeDu 透明度，百分比， 0 不透明 100 完成透明
'  Dim nAlpha As Long = (100-TeMeDu)*2.55
  Dim ftn As BLENDFUNCTION
  ftn.BlendOp =0       '这个参数必须也只能为AC_SRC_OVER(0x00),意思就是把源图片覆盖到目标之上.
  ftn.BlendFlags=0     '必须为0
  ftn.SourceConstantAlpha = (100-TeMeDu)*2.55 '简写为SCA,指定源图片的透明度,这个值是会和源图片的Alpha通道值合并计算的.，0为完全透明，255为完全不透明
  ftn.AlphaFormat =0  '可以填两种,一种是0x00,一种是AC_SRC_ALPHA(0x01).
  AlphaBlend (m_Dc, xDest *m_DpiX, yDest *m_DpiY, wDest *m_DpiX, hDest *m_DpiY, m_bDc, xSrc , ySrc , wSrc , hSrc , ftn)
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.DrawCopyImgColor(xDest As Long, yDest As Long, wDest As Long, hDest As Long, xSrc As Long, ySrc As Long, wSrc As Long, hSrc As Long, tColor As Long ) '扣色 描绘由LoadImg加载来的图
  'xywh Dest 目标  xywh Src 源
  'tColor 将源位图中的RGB颜色视为透明，扣除此色
  TransparentBlt (m_Dc, xDest *m_DpiX, yDest *m_DpiY, wDest *m_DpiX, hDest *m_DpiY, m_bDc, xSrc , ySrc , wSrc , hSrc , tColor)
End Sub

'--------------------------------------------------------------------------------
Private Sub yGDI.GpPen(ByVal nWidth As REAL,ByVal nColor As ARGB =GDIP_ARGB(255, 0, 0, 0)) 'GDI+ 设置笔，画框线用 基本笔，颜色和笔粗
  If nGpPen Then GdipDeletePen nGpPen
  GdipCreatePen1 nColor, nWidth *m_DpiX , 2, @nGpPen
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.GpBrush(ByVal nColor As ARGB = 0) 'GDI+ 设置刷子，用于填充颜色 0为空刷子
  '当nColor=0 空刷子,取消填充,就是不填充
  If nColor Then
      If nGpBrush Then
          GdipSetSolidFillColor nGpBrush, nColor
      Else
          GdipCreateSolidFill nColor, @nGpBrush
      End If
  Else
      If nGpBrush Then GdipDeleteBrush nGpBrush 'GDI+ 纯色刷
        nGpBrush=0
  End If
'  Print "填充颜色"
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.GpBrush(X1 As Long, Y1 As Long, X2 As Long, Y2 As Long, Color1 As ARGB, Color2 As ARGB, WrapMode As Long) 'GDI+ 渐变
  '从 XY1 到 XY2 渐变，颜色是 Color1 到 Color2，当不够填充目标时，WrapMode 设置平铺方式
  'WrapMode 平铺
  'WrapModeTile        =0 平铺而不翻转
  'WrapModeTileFlipX   =1 平铺水平翻转
  'WrapModeTileFlipY   =2 平铺垂直翻转
  'WrapModeTileFlipXY  =3 平铺垂直翻转，平铺水平翻转
  'WrapModeClamp       =4 不进行平铺
  If nGpBrush Then GdipDeleteBrush nGpBrush 'GDI+ 刷
  Dim As GpPointF point1, point2
  point1.x = x1 *m_DpiX
  point1.y = y1 *m_DpiY
  point2.x = x2 *m_DpiX
  point2.y = y2 *m_DpiY
  GdipCreateLineBrush  @point1, @point2, Color1, Color2, WrapMode, @nGpBrush
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.GpDrawLine(X1 As Single, Y1 As Single, X2 As Single, Y2 As Single) 'GDI+ 画线
  GdipDrawLine m_GpDC, nGpPen, X1 *m_DpiX, Y1 *m_DpiY, X2 *m_DpiX, Y2 *m_DpiY
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.GpDrawArc(X As Single, Y As Single, W As Single, H As Single, StartAngle As Single , sweepAngle As Single) 'GDI+ 画圆弧
'x y w h 一个假象的圆，位置和宽度高度
'StartAngle  相对于x轴的起始角度（以度为单位）。0度为原的右边中部，
'sweepAngle  指定弧线起始点和终点之间的角度。
'
  If nGpBrush Then
    GdipFillPie m_GpDC, nGpBrush, X *m_DpiX, Y *m_DpiY, W *m_DpiX, H *m_DpiY,StartAngle,sweepAngle
  End If
GdipDrawArc m_GpDC, nGpPen, X *m_DpiX, Y *m_DpiY, W *m_DpiX, H *m_DpiY,StartAngle,sweepAngle

End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.GpDrawFrame( X As Single,  Y As Single,  W As Single,  H As Single)
  If nGpBrush Then
    GdipFillRectangle m_GpDC, nGpBrush, X *m_DpiX, Y *m_DpiY, W *m_DpiX, H *m_DpiY
  End If
  GdipDrawRectangle  m_GpDC, nGpPen, X *m_DpiX, Y *m_DpiY, W *m_DpiX, H *m_DpiY
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.GpDrawCircleFrame(X As Single, Y As Single, W As Single, H As Single, wC As Single, hC As Single) 'GDI+ 描绘一个圆角矩形
'wC hC 是圆角半径，像素

  If nGpBrush Then
    '填充2个直角矩形  
      GdipFillRectangle m_GpDC, nGpBrush, X *m_DpiX, (Y+hC) *m_DpiY-1, W *m_DpiX, (H-hC*2) *m_DpiY+2
      GdipFillRectangle m_GpDC, nGpBrush, (X+wc) *m_DpiX-1, Y *m_DpiY, (W-wC*2) *m_DpiX+2, H *m_DpiY
    '填充四个角上的扇形区  
    '填充左上角扇形  
    GdipFillPie m_GpDC, nGpBrush, X *m_DpiX, Y *m_DpiY, Wc*2 *m_DpiX, hc*2 *m_DpiY,180,90
    '填充右下角的扇形  
    GdipFillPie m_GpDC, nGpBrush, (X+w-wC*2) *m_DpiX, (Y+h-hc*2) *m_DpiY, Wc*2 *m_DpiX, hc*2 *m_DpiY,360,90
    '填充右上角的扇形  
    GdipFillPie m_GpDC, nGpBrush, (X+w-wC*2) *m_DpiX, Y *m_DpiY, Wc*2 *m_DpiX, hc*2 *m_DpiY,270,90
    '填充左下角的扇形  
    GdipFillPie m_GpDC, nGpBrush, X *m_DpiX, (Y+h-hc*2) *m_DpiY, Wc*2 *m_DpiX, hc*2 *m_DpiY,90,90
  End If
    '画矩形上面的边  
    This.GpDrawLine(x+wc,y,w-wc+x,y)  
    '画矩形下面的边  
    This.GpDrawLine(x+wC,y+h,W-wC+x,y+h)  
    '画矩形左面的边  
    This.GpDrawLine(x,y+hC,x,y+h-hC)  
    '画矩形右面的边  
    This.GpDrawLine(x+w,y+hc,x+w,y+h-hC)  
    '画矩形左上角的圆角  
    This.GpDrawArc(x,y,wC*2,hC*2,180,90)  
    '画矩形右下角的圆角  
    This.GpDrawArc(x+w-wC*2,y+h-hC*2,wC*2,hC*2,360,90)  
    '画矩形右上角的圆角  
    This.GpDrawArc(x+w-wC*2,y,wC*2,hC*2,270,90)  
    '画矩形左下角的圆角  
    This.GpDrawArc(x,y+h-hC*2,wC*2,hC*2,90,90)  
End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.GpDrawEllipse(X As Single, Y As Single, W As Single, H As Single) 'GDI+ 描绘一个椭圆
  If nGpBrush Then
    GdipFillEllipse m_GpDC, nGpBrush, X *m_DpiX, Y *m_DpiY, W *m_DpiX, H *m_DpiY
  End If
  GdipDrawEllipse m_GpDC, nGpPen, X *m_DpiX, Y *m_DpiY, W *m_DpiX, H *m_DpiY
End Sub
'--------------------------------------------------------------------------------
Private Function yGDI.GpLoadImg(FileName As String) As Long '加载文件图像到 nGpimage ，便于后面重复使用，再次加载将消耗前面加载的。
  If nGpimage Then GdipDisposeImage nGpimage
  Dim ww As WString *1024
  ww=FileName
  Function=GdipLoadImageFromFile( @ww,@nGpimage)
  '成功 返回=0  ，失败
  Print nGpimage
End Function
'--------------------------------------------------------------------------------
Private Function yGDI.GpLoadImg(hInst As HINSTANCE, ResName As String) As Long '加载资源图像到 nGpimage ，便于后面重复使用，再次加载将消耗前面加载的。
  If nGpimage Then GdipDisposeImage nGpimage
  Dim hRes As HRSRC, pResData As HRSRC, wID As WORD, dwID As DWord, imageSize As DWord
  Dim pImageStream As IStream Ptr, hGlobal As HGLOBAL, pGlobalBuffer As LPVOID
  '找资源
  If Left(ResName, 1) = "#" Then
      wID = Val(Mid(ResName, 2))
      dwID = MAKELONG(wID, 0)
      hRes = FindResource(hInst, MAKEINTRESOURCEW(dwID), Cast(LPCSTR, RT_RCDATA))
  Else
      hRes = FindResource(hInst, StrPtr(ResName), Cast(LPCSTR, RT_RCDATA)) '查找资源
  End If
  Function = 1
  If hRes Then '检索图像的大小
      imageSize = SizeofResource(hInst, hRes) '获取长度
      If imageSize Then
          '加载资源并获取指向资源数据的指针。
          pResData = LockResource(LoadResource(hInst, hRes))
          If pResData Then
              hGlobal = GlobalAlloc(GMEM_MOVEABLE, imageSize) '分配内存以保存图像
              If hGlobal Then
                  pGlobalBuffer = GlobalLock(hGlobal) '锁定内存
                  If pGlobalBuffer Then
                      CopyMemory(pGlobalBuffer, pResData, imageSize) '将图像从二进制字符串文件复制到全局内存
                      If CreateStreamOnHGlobal(hGlobal, False, @pImageStream) = S_OK Then '在全局内存中创建一个流
                          If pImageStream Then
                              Function = GdipCreateBitmapFromStream(pImageStream, Cast(GpBitmap Ptr Ptr, @nGpimage)) '根据流中包含的数据创建一个位图
                              pImageStream->lpVtbl->Release(pImageStream)
                          End If
                      End If
                      GlobalUnlock pGlobalBuffer '解锁内存
                  End If
                  GlobalFree hGlobal '释放内存
              End If
          End If
      End If
  End If
  '成功 返回=0  ，失败
'  Print nGpimage
  
End Function
'--------------------------------------------------------------------------------
Private Sub yGDI.GpDrawCopyImg(xDest As Single, yDest As Single, wDest As Single, hDest As Single, xSrc As Single, ySrc As Single, wSrc As Single, hSrc As Single) '描绘由LoadImg加载来的图
  GdipDrawImageRectRect m_GpDC,nGpimage,_
   xDest *m_DpiX, yDest *m_DpiY, wDest *m_DpiX, hDest *m_DpiY ,_
   xSrc *m_DpiX, ySrc *m_DpiY, wSrc *m_DpiX, hSrc *m_DpiY ,_
   2,Null,Null,Null
'srcunit  [in]单位枚举的元素，用于指定图像的度量单位。默认值为UnitPixel.
'imageAttributes  [in]指向ImageAttributes结构的指针，用于指定要绘制的图像的颜色和大小属性。默认值为NULL。
' callback        [in]用于取消正在进行的绘图的回调函数。默认值为NULL。
' callbackData     [in]指向由回调参数指定的函数使用的附加数据的指针。默认值为NULL。 
  '如果函数成功，则返回Ok，它是状态枚举的一个元素 

End Sub
'--------------------------------------------------------------------------------
Private Sub yGDI.GpDrawFrameStr(X As Single, Y As Single, nText As String,hFont As String="宋体",hSize As Long =12 ) '描绘带轮廓文本字符串
  Dim nPath As GpPath Ptr
  Dim fontFamily As GpFontFamily Ptr
  Dim rc As Rect
  Dim ww As WString * 1024
  rc.Left = x *m_DpiX
  rc.top = y *m_DpiY
  rc.Right = x *m_DpiX + m_nWidth
  rc.bottom = y *m_DpiY+ m_nHeight
  ww=hFont 
  GdipCreateFontFamilyFromName @ww, Null, @fontFamily '创建一个基于指定的字体系列的FontFamily对象
'  Dim nFormat As GpStringFormat Ptr
'  GdipCreateStringFormat(0, 0, @nformat) '创建一个基于字符串的格式标志和语言的 StringFormat 对象
'  GdipSetStringFormatAlign(nformat, StringAlignmentNear)  '设置文本对齐方式
  
  GdipCreatePath FillModeAlternate, @nPath '创建路径
  ww = nText
  '把文字植入路径里
  GdipAddPathStringi(nPath, @ww, Len(nText), fontFamily, FontStyleRegular, hSize, @rc, Null)
  
  GdipDrawPath(m_GpDC, nGpPen, nPath)
  If nGpBrush Then  GdipFillPath m_GpDC, nGpBrush, nPath

'  GdipDeleteStringFormat(nFormat) '销毁指定StringFormat对象，释放资源
  GdipDeleteFontFamily(fontfamily) '销毁指定FontFamily对象，释放资源
  GdipDeletePath nPath
End Sub



