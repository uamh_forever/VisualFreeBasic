﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=GDI 类，例题
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=701
Height=347
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Picture]
Name=Picture1
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
Left=9
Top=5
Width=190
Height=253
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=圆角矩形
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=222
Top=104
Width=73
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=画框
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=310
Top=90
Width=75
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=椭圆
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=314
Top=69
Width=70
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=画线
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=224
Top=63
Width=73
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=设置线
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=225
Top=18
Width=73
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=设置填充
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=312
Top=18
Width=79
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=不填充
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=315
Top=44
Width=71
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command8
Index=-1
Caption=设置线空线
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=227
Top=41
Width=73
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command9
Index=-1
Caption=字符
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=225
Top=127
Width=73
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command10
Index=-1
Caption=黑体
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=313
Top=114
Width=70
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command11
Index=-1
Caption=文本
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=225
Top=151
Width=75
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command12
Index=-1
Caption=带背景文本
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=314
Top=138
Width=74
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Picture]
Name=Picture2
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
Left=408
Top=199
Width=265
Height=100
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command13
Index=-1
Caption=翻转像素
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=225
Top=176
Width=66
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command14
Index=-1
Caption=清空背景
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=310
Top=254
Width=76
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command15
Index=-1
Caption=画资源图片
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=411
Top=19
Width=69
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command16
Index=-1
Caption=保存为图像文件
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=16
Top=260
Width=98
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command17
Index=-1
Caption=画文件图片
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=413
Top=48
Width=76
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command18
Index=-1
Caption=画黑白图
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=412
Top=77
Width=72
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame1
Index=-1
Caption=GDI 画画速度快，功能少
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,15
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=207
Top=-2
Width=192
Height=297
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame2
Index=-1
Caption=GDI+ 速度慢点（需工程属性里选上支持）
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,15
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=403
Top=-1
Width=274
Height=190
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command20
Index=-1
Caption=像素点
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=223
Top=199
Width=66
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command21
Index=-1
Caption=局部画图
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=311
Top=185
Width=74
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command22
Index=-1
Caption=画线
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=501
Top=51
Width=73
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command23
Index=-1
Caption=设置线
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=499
Top=21
Width=73
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command24
Index=-1
Caption=椭圆
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=504
Top=77
Width=70
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command19
Index=-1
Caption=渐变
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=584
Top=76
Width=70
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command26
Index=-1
Caption=测试
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=126
Top=261
Width=78
Height=37
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command27
Index=-1
Caption=圆角矩形
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=504
Top=100
Width=73
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command28
Index=-1
Caption=画圆弧
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=221
Top=220
Width=75
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command29
Index=-1
Caption=局部画图
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=413
Top=105
Width=74
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command30
Index=-1
Caption=加载资源图片
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=415
Top=131
Width=79
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command31
Index=-1
Caption=描边字轮廓字
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=306
Top=161
Width=80
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command32
Index=-1
Caption=描边字轮廓字
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=413
Top=154
Width=80
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command33
Index=-1
Caption=局部画图透明
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=305
Top=209
Width=84
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command34
Index=-1
Caption=局部画图扣色
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=306
Top=231
Width=84
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command35
Index=-1
Caption=样式填充
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=221
Top=246
Width=75
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]
'==================================================================================================================================
'这是旧版本窗口，需要自己人工修改和调整才能正常使用，主要更改项目：
'1）窗口和控件属性：全部需要自己人工重新调整，升级程序只是简单的转换。
'2）窗口和控件事件：升级程序会自动更新到新版的格式，无法保证100%正确，更加自己需要查证。
'3）窗口和控件句柄：新版没有句柄常量，需要用类，如：HWND_FORM1  改为 Form1.hWnd ， Picture1.hwnd  改为 Form1.Picture1.hWnd 
'4）窗口和控件IDC： 新版无IDC常量，需要用类，如：IDC_FORM1_PICTURE1 改为 Form1.Picture1.IDC
'5）新弹出窗口：Form2_Show  改为 Form2.Show 
'6）与字符有关的API：以前默认为 ANSI字符，现在是 Unicode字符，因此相关API 后面要加个 A 来解决，如：PostMessage 改为 PostMessageA ，或使用宽字符变量
'7）API操作控件和窗口，有关字符的，全部为 Unicode字符 ，类型是 CWSTR，CWSTR 不可以直接用在 Len Left Right Mid InStr UCase 等一系列字符操作语句上
'必须前面加个 ** 才能当成宽字符处理（英文中文都算1个字符）如： Len(**b) ,可以赋值到ANSI变量，会自动转换编码：dim a As String = b  (b 为 CWSTR ) 
'或者 Dim b As CWSTR = a  (a 为 String) ,因此用变量过度一下，没什么问题，就是不可以直接返回就用在语句上，如：Len(FF_Control_GetText(..)) 这是错的
'8）编译后的窗口和控件是 Unicode 的，旧版是 ANSI 的在英文系统中无法显示中文。可以用API IsWindowUnicode 检查窗口和控件是不是 Unicode
'9）所有窗口和控件由 CWindow Class 类创建，旧版是直接API创建，可以参考 WinFBX 帮助来查看 CW 的众多附加功能。
'最后预祝大家编程愉快，升级顺利。有任何问题可以在编程群里提问：Basic语言编程QQ群 78458582
'==================================================================================================================================



'--------------------------------------------------------------------------------
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawCircleFrame(10, 10, 70, 90, 35, 35)
  

End Sub





'--------------------------------------------------------------------------------
Sub Form1_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawFrame(10, 10, 70, 90)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawEllipse(10, 10, 70, 90)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawLine(10, 10, 70, 90)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.Pen(2, &HFF1FE0)
  gg.DrawLine(10, 10, 70, 90)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.Brush(&H20FF20)
  gg.DrawEllipse(10, 10, 70, 90)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.Brush( -1)
  gg.DrawEllipse(10, 10, 70, 90)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command8_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.Pen(1, 0, PS_NULL)
  gg.Brush(&H20FF20)
  gg.DrawEllipse(10, 10, 70, 90)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command9_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawString(10, 10, "ww的沃尔qqq")

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command10_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.Font("黑体", 20)
  gg.DrawString(10, 10, "ww的沃尔qqq")

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command11_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.Font("宋体", 60)
 
  gg.DrawTexts(10, 10, 100, 100,WChr(&H613) )'&  "w十多万人的我或乌尔禾是电饭锅隔热例题特尔托w的沃尔qqq" & vbCrLf & "的威慑来晚了而万物人问问")
  gg.Brush( -1)
  gg.DrawFrame(10, 10, 100, 100)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command12_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.SetColor(&H00FFFF, &H0000FF)
  gg.DrawString(10, 10, "ww的沃尔qqq")

End Sub



'--------------------------------------------------------------------------------
Function Form1_Picture2_WM_Paint(hWndForm As hWnd, hWndControl As hWnd) As LResult  '重绘，系统通知控件需要重新绘画。

  Dim gg As yGDI = yGDI( Picture2.hwnd , GetSysColor(COLOR_WINDOW), True)
  gg.DrawString(10, 10, "这里是控件或窗口自绘时用的，在WM_PAINT消息里用")
  gg.Pen(3, &HFFFF00, PS_DOT)
  gg.DrawLine(10, 30, 500, 50)
  
  Function = True    ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Sub Form1_Command13_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.Brush(&H20FF20)
  gg.DrawEllipse(10, 10, 70, 90)
  gg.InvertPixels(20, 5, 30, 100)

End Sub



'--------------------------------------------------------------------------------
Sub Form1_Command14_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = yGDI(Picture1.hwnd , GetSysColor(COLOR_BTNFACE))
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command15_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawImgRes(App.HINSTANCE, "GIF_YFVBJJK", 60, 20)
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command16_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawImgRes(App.HINSTANCE, "GIF_YFVBJJK", 60, 20)
  gg.SaveBitmap(App.Path & "tt.jpg","image/jpeg" )
  AfxMsg App.Path & "tt.jpg", "保存完成"

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command17_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawImgFile(App.Path & "按钮.png", 5, 20)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command18_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawImgRes(App.HINSTANCE, "GIF_YFVBJJK", 5, 5, 150, 150, 0, True)

End Sub

'--------------------------------------------------------------------------------
Sub Form1_Command20_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  Dim i As Long
  For i = 1 To 30
      gg.SetPixel(i * 3, i * 3, &H0000FF)
  Next

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command21_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.LoadImgRes(App.HINSTANCE, "GIF_YFVBJJK")
  gg.DrawCopyImg(5, 5, 50, 100, 0, 0, 50, 100)
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command22_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.GpDrawLine(5, 5, 100, 120)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command23_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.GpPen(3, GDIP_ARGB(255, 255, 0, 0))
  gg.GpDrawLine(105, 5, 100, 120)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command24_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.GpPen(3, GDIP_ARGB(255, 255, 0, 0))
  gg.GpBrush(GDIP_ARGB(255, 0, 255, 0))
  gg.GpDrawEllipse(5, 25, 170, 100)
  gg.GpPen(3, &HFF4040FF)
  gg.GpBrush(0)
  gg.GpDrawEllipse(100, 5, 50, 170)
  

End Sub




'--------------------------------------------------------------------------------
Sub Form1_Command19_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.GpBrushGradient(5, 5, 100, 5, GDIP_ARGB(255, 255, 0, 0), GDIP_ARGB(255, 255, 255, 255), 3)
  'gg.GpPen(0,0)
  gg.GpDrawFrame(5, 5, 170, 160)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command26_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.DrawImgRes(App.HINSTANCE, "PNG_CLOCK", 60, 20)
  

End Sub



'--------------------------------------------------------------------------------
Sub Form1_Command27_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.GpBrush(&HFF20FF20)
  gg.GpDrawCircleFrame(5, 5, 150, 150, 40, 40)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command28_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  Dim As Long  i, x, y
  
  
  gg.DrawEllipse(5, 5, 150, 100)
  gg.Pen(5, &HFF00FF)
  
  gg.DrawArc 5, 5, 150, 100, 90, 0

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command29_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  Print gg.GpLoadImgFile(App.Path & "按钮.png")
  gg.GpDrawCopyImg(10, 10, 50, 50, 0, 0, 25, 25)
  gg.GpDrawCopyImg(10, 100, 50, 50, 0, 50, 25, 25)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command30_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  Print gg.GpLoadImgRes(App.HINSTANCE, "PNG_AN")
  gg.GpDrawCopyImg(10, 10, 100, 50, 0, 0, 100, 50)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command31_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.Font("黑体", 40)
  gg.Pen(5, &HFF00FF)
  gg.DrawFrameStr(10, 10, "轮廓字")
  gg.SetColor(&HFFFFFF)
  gg.DrawFrameStr(10, 100, "描边字")
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command32_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.GpPen(5)
  gg.GpBrushGradient(5, 5, 100, 5, GDIP_ARGB(255, 255, 0, 0), GDIP_ARGB(255, 255, 255, 255), 3)
  gg.GpDrawFrameStr 10, 10, "是为了", "黑体", 40

End Sub



'--------------------------------------------------------------------------------
Sub Form1_Command33_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.LoadImgRes(App.HINSTANCE, "GIF_YFVBJJK")
  gg.DrawCopyImg(5, 5, 90, 90, 0, 0, 90, 90)
  gg.DrawCopyImgAlpha(5, 105, 90, 90, 0, 0, 90, 90, 80)
  gg.DrawString(100, 20, "原图")
  gg.DrawString(100, 120, "透明 80%")

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command34_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.LoadImgRes(App.HINSTANCE, "GIF_YFVBJJK")
  gg.DrawCopyImg(5, 5, 90, 90, 0, 0, 90, 90)
  gg.DrawCopyImgColor(5, 105, 90, 90, 0, 0, 90, 90, &H3B9F00)
  gg.DrawString(100, 20, "原图")
  gg.DrawString(100, 120, "扣除绿色")

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command35_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim gg As yGDI = Picture1.hwnd 
  gg.BrushHatch(HS_BDIAGONAL, &H0000FF)
  gg.DrawFrame(10, 10, 50, 50)
  gg.BrushHatch(HS_CROSS, &H0000FF)
  gg.DrawFrame(100, 10, 50, 50)
  gg.BrushHatch(HS_DIAGCROSS, &H0000FF)
  gg.DrawFrame(10, 100, 50, 50)
  gg.BrushHatch(HS_FDIAGONAL, &H0000FF)
  gg.DrawFrame(100, 100, 50, 50)
  gg.BrushHatch(HS_HORIZONTAL, &H0000FF)
  gg.DrawFrame(10, 170, 50, 50)
  gg.BrushHatch(HS_VERTICAL, &H0000FF)
  gg.DrawFrame(100, 170, 50, 50)
            
End Sub

Sub Form1_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
 
End Sub




