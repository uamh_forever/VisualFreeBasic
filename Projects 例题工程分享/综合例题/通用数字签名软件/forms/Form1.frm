﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP
Style=3 - 常规窗口
Icon=edituser.ico|ICON_EDITUSER
Caption=勇芳_免费EXE文件数字签名软件
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=520
Height=178
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=True

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=需要被签名的文件：(EXE或DLL）
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=9
Top=6
Width=385
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=11
Top=30
Width=453
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=打开
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=469
Top=29
Width=41
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=签名人名称
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=13
Top=65
Width=75
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=29
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=89
Top=59
Width=194
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=只是黑签名，既无效签名。只是证明软件被签过名，无法被系统承认，属于假签名。
Enabled=True
Visible=True
ForeColor=&HFF00009E
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=8
Top=94
Width=536
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=黑签名也是签名，有总比没有好，哈哈！！不用花钱给自己软件签个名吧。
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=11
Top=122
Width=416
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=写入签名
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=439
Top=118
Width=70
Height=29
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=1个中文占3个，最多29个，超过会截断
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=292
Top=66
Width=218
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]

Declare Function CheckSumMappedFile Lib "Imagehlp.dll" Alias "CheckSumMappedFile"(byval BaseAddress as PVOID, byval FileLength as DWORD, byval HeaderSum as PDWORD, byval CheckSum as PDWORD) as PIMAGE_NT_HEADERS

Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim  aa As CWSTR = FF_OpenFileDialog( hWndForm, "打开制作文件","", "", "EXE和DLL的文件 |*.exe;*.dll", "",  0  , TRUE )                           
  if Len(aa) Then Text1.Text = aa

End Sub


Sub Form1_Shown(hWndForm As hWnd, UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   If Len(AfxCommand) > 0 Then Text1.Text = AfxCommand
   Text2.Enabled =False 
   Text2.Text = INI_GetKey(App.Path & "签名.ini", "QianMin","save","勇芳软件，强")
   Text2.Enabled =True 
End Sub

Sub Form1_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim As Long  a, b 
   a = 写入签名()
   If a > 0 Then MessageBox(hWndForm, "完成制作。", "自动签名和CRC写入", _
      MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1 )
End Sub
Function 写入签名() As Long '
   Dim I2 As Long, CRC As String, hFile As Long, P As String
   Dim As Long  C,U,I,PEa,PEb,q,ii
   Dim LBY() As ULong, By() As uByte
   Dim Toc(3) As ULong
   Dim As String  Pf,Pg ,QianMin
   dim Sof() As String, PID() As Dword, ok As Long
   
   
   pf = Text1.Text
   Pg = GetResourceStr("YQM_2233")
   QianMin = Text2.Text
   If Len(pf) = 0 Then
      MessageBox(Form1.hWnd, "需要一个被签名的文件", "", _
         MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1)
      Exit Function
   End If
   If Len(pg) = 0 Then
      MessageBox(Form1.hWnd, "需要一个签名档文件", "", _
         MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1)
      Exit Function
   End If
   If AfxFileExists(pf) = 0 Then
      MessageBox(Form1.hWnd, "被签名的文件不存在", "", _
         MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1)
      Exit Function
   End If
   If Len(QianMin) = 0 Then
      MessageBox(Form1.hWnd, "需要一个签名的名称", "", _
         MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1)
      Exit Function
   End If   
   GetAllPID(Sof(), PID())
   For i = 0 To UBound(pid)
      If UCase(pf) = UCase(sof(i)) Then
         ProcessKill(pid(i))
         Sleep 500
         ok = 1
         Exit For
      End If
   Next
   QianMin = CWSTRtoUTF8(Text2.Text)
   if Len(QianMin) > 29 Then 
      QianMin = Left(QianMin, 29)
   ElseIf Len(QianMin) < 29 Then 
      QianMin &= String(29 - Len(QianMin),0)
   End if    
   Mid(Pg, 454, 29) = QianMin
   Mid(Pg, 558, 29) = QianMin
        
   CRC = GetFileStr(pF)
   
   '分析PE头，为了避免软件签名内容，造成冲突
   If Left(CRC, 2) <> "MZ" Then
      MessageBox(Form1.hWnd, "没有发现DOS头文件，必须是EXE或者DLL", "", _
         MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1)
      Exit Function
   End If
   c = Peek(Long, Cast(UInteger, StrPtr(CRC)) + 60)
   If c = 0 Or c > Len(crc) -1000 Then
      MessageBox(Form1.hWnd, "没有发现PE头地址", "", _
         MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1)
      Exit Function
   End If
   u = Peek(ULong, Cast(UInteger, StrPtr(CRC)) + c)
   If u <> 17744 Then
      MessageBox(Form1.hWnd, "PE头辨识出错", "", _
         MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1)
      Exit Function
   End If
   PEa = c + 244  'PE开始位置
   u = Peek(ULong, Cast(UInteger, StrPtr(CRC)) + c + 152)
   If u > 0 And u < Len(CRC) Then
      Peb = u -1      'PE，去除签名后的 真实长度
   Else
      Peb = Len(crc)  'PE 真实长度
   End If
   
   c = Peek(ULong, Cast(UInteger, StrPtr(CRC)) + 60)
   u = Peek(ULong, Cast(UInteger, StrPtr(CRC)) + c)
   u = Peek(ULong, Cast(UInteger, StrPtr(CRC)) + c + 152)   '位置
   If u > 0 Then
      Select Case MessageBox(Form1.hWnd, "本文件已经被签名" & vbCrLf & pF & vbCrLf & "你是想要替换签名吗？", "已有签名", _
               MB_YESNO Or MB_ICONQUESTION Or MB_DEFBUTTON1)
         Case IDYES
            CRC = Left(CRC, u)
         Case IDNO
            Exit Function
      End Select
      
   End If
   
   q = len(pg)
   u = len(CRC)
   CRC &= Pg
   Mid(CRC, c + 157, 4) = MKL(q)
   Mid(CRC, c + 153, 4) = MKL(u)
   
   i = 0
   CheckSumMappedFile(StrPtr(crc), Len(crc), @i, @ii) '获取PE校验和

   Mid(CRC, c + 89, 4) = MKL(ii)
   
   SaveFileStr Pf, CRC
   Function = 1 + ok
End Function


Sub Form1_WM_DropFiles(hWndForm As hWnd, hDrop As HDROP)  '当用户将文件拖放到已启用接收的应用程序窗口中
   Dim u As Long = DragQueryFile(hDrop, -1, Null, 0) '获取文件个数
   If u Then
      Dim nFile As wString * (MAX_PATH + 1), re As Long, i As Long
      For i = 0 To u -1
         re = DragQueryFileW(hDrop, i, @nFile, MAX_PATH) '获取文件名，返回文件名的字符个数
         If re Then
            'nFile 包含路径的文件名，这里是宽字符版，要获取 A字符，使用 Dim nFile As zString 和 DragQueryFileA
            Text1.Text = nFile
            Exit For
         End If
      Next
   End If
   
End Sub

Sub Form1_Text2_EN_Change(hWndForm As hWnd, hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
  if Text2.Enabled Then 
     INI_SetKey(App.Path & "签名.ini", "QianMin","save",Text2.Text)
  End if 
End Sub











