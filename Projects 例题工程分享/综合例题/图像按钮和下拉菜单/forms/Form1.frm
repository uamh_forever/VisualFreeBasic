﻿#VisualFreeBasic_Form#  Version=5.0.0
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
Style=3 - 常规窗口
Icon=
Caption=图像按钮和下拉菜单
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Left=0
Top=0
Width=500
Height=310
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=&Shutdown
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=147
Top=110
Width=148
Height=30
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]

'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   
   Dim cx As Long = AfxScaleX(16)
   Dim hImageList As HIMAGELIST = ImageList_Create(cx, cx, ILC_COLOR32 Or ILC_MASK, 1, 0)
   '// --> Remember to change the path and name of the icon
   If hImageList Then ImageList_ReplaceIcon(hImageList, -1, AfxGdipImageFromFile("./Resources/Shutdown_48.png"))
   '// Fill a BUTTON_IMAGELIST structure and set the image list
   Dim bi As BUTTON_IMAGELIST = (hImageList, (3, 3, 3, 3), BUTTON_IMAGELIST_ALIGN_LEFT)
   Button_SetImageList(COMMAND1.hwnd, @bi)
   AfxAddWindowStyle COMMAND1.hwnd, BS_SPLITBUTTON
   
End Sub
 Const IDC_MENUCOMMAND1 = 28000
Const IDC_MENUCOMMAND2 = 28001

'                                                                                  
Function Form1_Custom(hWndForm As hWnd, wMsg As UInteger, wParam As wParam, lParam As lParam) As LResult  '自定义消息（全部消息），在其它事件后处理，返回非0，终止系统处理此消息。
   
   Select Case wMsg
         
      Case WM_NOTIFY
         '// Processs notify messages sent by the split button
         Dim pNmh As NMHDR Ptr = Cast(NMHDR Ptr, lParam)
         If pNmh->idFrom = COMMAND1.idc And pNmh->code = BCN_DROPDOWN Then
            Dim pDropDown As NMBCDROPDOWN Ptr = Cast(NMBCDROPDOWN Ptr, lParam)
            '// Get screen coordinates of the button
            Dim pt As Point = (pDropdown->rcButton.Left, pDropDown->rcButton.bottom)
            ClientToScreen(pNmh->hwndFrom, @pt)
            '// Create a menu and add items
            Dim hSplitMenu As HMENU = CreatePopupMenu
            AppendMenuW(hSplitMenu, MF_BYPOSITION, IDC_MENUCOMMAND1, "Menu item 1")
            AppendMenuW(hSplitMenu, MF_BYPOSITION, IDC_MENUCOMMAND2, "Menu item 2")
            '// Display the menu
            TrackPopupMenu(hSplitMenu, TPM_LEFTALIGN Or TPM_TOPALIGN, pt.x, pt.y, 0, hWndForm, Null)
            Function = CTRUE
            Exit Function
         End If
   End Select
   Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   
   Me.Close
   
End Sub

'                                                                                  
Sub Form1_WM_Destroy(hWndForm As hWnd)  '即将销毁窗口
   
   '// Destroy the image list
   ImageList_Destroy Cast(HIMAGELIST, Toolbar_SetImageList(GetDlgItem(hWndForm, COMMAND1.idc), 0))
   
   
   
End Sub

