﻿#VisualFreeBasic_Form#  Version=5.0.0
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
Style=3 - 常规窗口
Icon=
Caption=Form1
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Left=0
Top=0
Width=500
Height=310
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=静态调用DLL
Enabled=True
Visible=True
Font=宋体,9,0
Left=144
Top=8
Width=99
Height=28
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=Text1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=False
Multiline=False
Uppercase=False
Lowercase=False
Number=False
Left=26
Top=9
Width=102
Height=31
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=Text2
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=False
Multiline=False
Uppercase=False
Lowercase=False
Number=False
Left=14
Top=50
Width=106
Height=22
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=Command2
Index=-1
Caption=动态调用DLL
Enabled=True
Visible=True
Font=宋体,9,0
Left=144
Top=48
Width=102
Height=28
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=FB生成的DLL带会符号，可用DLL查看器看到
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=宋体,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=22
Top=90
Width=386
Height=19
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]



'--------------------------------------------------------------------------------
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   
   text1.text = Str(Add2(370037607, Rnd * 100))
   AfxMsg testdll("ddddd")
   
End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   
   Dim As Any Ptr library = DyLibLoad( "dlltest" ) '就文件名，不能带 .DLL
   If (library = 0) Then
      AfxMsg "加载DLL失败"
      Return
   End If
   Dim AddNumbers As Function(ByVal As Integer, ByVal As Integer) As Integer
   AddNumbers = DyLibSymbol(library, "ADD2")  ' 特别提醒，必须全部大写
   '从DLL查看器里可以看到全称是 ADD2@8 ，而FB用FB的DLL可以省略  @8 的符号
   
   If (AddNumbers = 0) Then
      AfxMsg "无法从开发DLL例题库中检索Add2（）函数的地址"
      Return
   End If
   text2.text = Str(AddNumbers(100, Rnd * 100))
   
   AfxMsg "使用完成"
   DyLibFree(library)  '卸载DLL ，特别注意，假如DLL还在工作，卸载会让软件崩溃
   '本例题里，加载会弹窗提示，就是在工作，必须先关弹窗在卸载
   '但本例题里同时有静态，因此关了也不崩溃，因为不在真正卸载DLL，因为还有静态加载。
   
End Sub

