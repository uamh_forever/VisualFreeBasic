﻿#VisualFreeBasic_Form#  Version=5.2.11
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=进程通信之内存共享例题
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=460
Height=397
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=发送给对方
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=44
Top=71
Width=323
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=发送
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=8
Top=74
Width=30
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=发送
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=379
Top=71
Width=56
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=接收
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=8
Top=106
Width=33
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[ListBox]
Name=List1
Index=-1
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=46
Top=106
Width=316
Height=244
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=发送ID：（对方ID号）
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=9
Top=6
Width=130
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text2
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=141
Top=6
Width=94
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=接收ID：（自己ID号）
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=8
Top=36
Width=125
Height=18
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text3
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=142
Top=36
Width=94
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Timer]
Name=Timer1
Index=-1
Interval=100
Enabled=True
Left=392
Top=250
Tag=

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=同时开2个这软件，输入对方ID，就可以给对方发消息了
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=263
Top=11
Width=163
Height=43
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]

'--------------------------------------------------------------------------------
Dim Shared hMapFile As HANDLE
Const BUF_SIZE = 1024 '定义共享最大内存 
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
  Dim bb As String = Hex(GetCurrentProcessId) 
  Text3.Text = bb  
  hMapFile = CreateFileMappingA(INVALID_HANDLE_VALUE, Null, PAGE_READWRITE, 0, BUF_SIZE, StrPtr(bb))
  

End Sub

Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  Dim bb As String = Text2.Text
  If Len(bb) = 0 Then
      MessageBox(hWndForm, "没有输入对方ID", "进程通信之内存共享例题", _
          MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Return
  End If
  Dim hMap As HANDLE
  hMap = OpenFileMappingA(FILE_MAP_ALL_ACCESS, True, StrPtr(bb))
  If hMap = 0 Then
      MessageBox(hWndForm, "对方ID无效或没开启共享", "进程通信之内存共享例题", _
          MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)      
  Else
      '映射对象的一个视图，得到指向共享内存的指针，设置里面的数据
      Dim pBuffer As LPCVOID = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, 0)
      Dim cc As ZString Ptr = Cast(ZString Ptr, pBuffer)
      Dim aa As String = Text1.Text
      If Len(aa) > BUF_SIZE -1 Then aa = Left(aa, BUF_SIZE -1) '预防超过最大共享内存 
      *cc = aa    
      UnmapViewOfFile(pBuffer)
      CloseHandle(hMap)
  End If  

End Sub
'  销毁 [事件]   hWndForm=窗体句柄
Sub Form1_WM_Destroy(hWndForm As hWnd)  '即将销毁窗口
    CloseHandle(hMapFile)


End Sub
'  定时器 [事件]   hWndForm=窗体句柄  wTimerID=计时器标识符
Sub Form1_Timer1_WM_Timer(hWndForm As hWnd, wTimerID As Long)  '定时器
      Dim pBuffer  As LPCVOID = MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0)
      Dim cc As ZString Ptr = Cast(ZString Ptr, pBuffer)
      If Len( *cc) Then 
          Dim aa As Long = List1.AddItem(NowString & " " & *cc)
          List1.ListIndex = aa  
          *cc = "" '用后清除   
      End If  
      
      UnmapViewOfFile(pBuffer)


End Sub

