﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=Form1
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=486
Height=309
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=&Close
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=387
Top=231
Width=67
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=这里演示代码创建工具栏，当然还有工具栏控件可以用
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=39
Top=60
Width=378
Height=29
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]

Const IDC_TOOLBAR = 1001
Const IDC_REBAR = 1002
Enum
   IDM_LEFTARROW = 28000
   IDM_RIGHTARROW
   IDM_HOME
   IDM_SAVEFILE
   IDM_HISTORY
   IDM_MAIL
   IDM_PRINT
   IDM_SEARCH
   IDM_VIEW
   IDM_STOP
End Enum
'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   
   Dim pWindow As CWindow Ptr = AfxCWindowPtr(hWndForm)
   
   Dim hToolBar As hWnd = pWindow->AddControl("Toolbar", , IDC_TOOLBAR)
   '// Calculate the size of the icon according the DPI
   Dim cx As Long = AfxScaleX(24)
   
   '// Create an image list for the toolbar
   Dim hImageList As HIMAGELIST
   hImageList = ImageList_Create(cx, cx, ILC_COLOR32 Or ILC_MASK, 7, 0)
   If hImageList Then
      AfxGdipAddIconFromRes(hImageList, App.hInstance, "PNG_ARROW_LEFT_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_ARROW_RIGHT_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_HOME_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_SAVE_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_HISTORY_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_MAIL_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_PRINT_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_SEARCH_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_VIEW_48")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_STOP_48")
      '// Set the normal image list
      Toolbar_SetImageList hToolBar, hImageList
      '// Set the hot image list with the same images than the normal one
      Toolbar_SetHotImageList hToolBar, hImageList
   End If
   
   '// Create a disabled image list for the toolbar
   Dim hDisabledImageList As HIMAGELIST
   hDisabledImageList = ImageList_Create(cx, cx, ILC_COLOR32 Or ILC_MASK, 4, 0)
   If hDisabledImageList Then
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_ARROW_LEFT_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_ARROW_RIGHT_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_HOME_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_SAVE_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_HISTORY_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_MAIL_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_PRINT_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_SEARCH_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.HINSTANCE, "PNG_VIEW_48", 60, True)
      AfxGdipAddIconFromRes(hDisabledImageList, App.hInstance, "PNG_STOP_48", 60, True)
      '// Set the disabled image list
      Toolbar_SetDisabledImageList hToolBar, hDisabledImageList
   End If
   '// Add buttons to the toolbar
   Toolbar_AddButton hToolBar, 0, IDM_LEFTARROW
   Toolbar_AddButton hToolBar, 1, IDM_RIGHTARROW
   Toolbar_AddButton hToolBar, 2, IDM_HOME
   Toolbar_AddButton hToolBar, 3, IDM_SAVEFILE
   Toolbar_AddButton hToolBar, 4, IDM_HISTORY
   Toolbar_AddButton hToolBar, 5, IDM_MAIL
   Toolbar_AddButton hToolBar, 6, IDM_PRINT
   Toolbar_AddButton hToolBar, 7, IDM_SEARCH
   Toolbar_AddButton hToolBar, 8, IDM_VIEW
   Toolbar_AddButton hToolBar, 9, IDM_STOP
   
   '// Disable the save file button
   Toolbar_DisableButton(hToolBar, IDM_SAVEFILE)
   
   '// Size the toolbar
   Toolbar_AutoSize hToolBar
   
   '// Create a rebar control
   Dim hRebar As hWnd = pWindow->AddControl("Rebar", , IDC_REBAR)
   
   '// Add the band containing the toolbar control to the rebar
   '// The size of the REBARBANDINFOW is different in Vista/Windows 7
   Dim rc As RECT, wszText As WString * 260
   Dim rbbi As REBARBANDINFOW
   If AfxWindowsVersion >= 600 And AfxComCtlVersion >= 600 Then
      rbbi.cbSize = REBARBANDINFO_V6_SIZE
   Else
      rbbi.cbSize = REBARBANDINFO_V3_SIZE
   End If
   '// Insert the toolbar in the rebar control
   rbbi.fMask = RBBIM_STYLE Or RBBIM_CHILD Or RBBIM_CHILDSIZE Or _
      RBBIM_SIZE Or RBBIM_ID Or RBBIM_IDEALSIZE Or RBBIM_TEXT
   rbbi.fStyle = RBBS_CHILDEDGE
   rbbi.hwndChild = hToolbar
   rbbi.cxMinChild = 270 * pWindow->rxRatio
   rbbi.cyMinChild = Toolbar_GetButtonHeight(hToolbar)
   rbbi.cx = 270 * pWindow->rxRatio
   rbbi.cxIdeal = 270 * pWindow->rxRatio
   wszText = "Toolbar"
   rbbi.lpText = @wszText
   '// Insert band into rebar
   Rebar_InsertBand(hRebar, -1, @rbbi)
   
   
End Sub


'                                                                                  
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Me.Close  

End Sub


'                                                                                  
Function Form1_Custom(hWndForm As hWnd, wMsg As UInteger, wParam As wParam, lParam As lParam) As LResult  '自定义消息（全部消息），在其它事件后处理，返回非0，终止系统处理此消息。

   Select Case wMsg

      Case WM_NOTIFY
         ' -------------------------------------------------------
         ' Notification messages are handled here.
         ' The TTN_GETDISPINFO message is sent by a ToolTip control
         ' to retrieve information needed to display a ToolTip window.
         ' ------------------------------------------------------
         Dim ptnmhdr As NMHDR Ptr              ' // Information about a notification message
         Dim ptttdi As NMTTDISPINFOW Ptr       ' // Tooltip notification message information
         Dim wszTooltipText As WString * 260   ' // Tooltip text

         ptnmhdr = Cast(NMHDR Ptr, lParam)
         Select Case ptnmhdr->code
            ' // The height of the rebar has changed
            Case RBN_HEIGHTCHANGE
               ' // Get the coordinates of the client area
               Dim rc As RECT
               GetClientRect hWndForm, @rc
               ' // Send a WM_SIZE message to resize the controls
               SendMessageW hWndForm, WM_SIZE, SIZE_RESTORED, MAKELONG(rc.Right - rc.Left, rc.Bottom - rc.Top)
            ' // Toolbar tooltips
            Case TTN_GETDISPINFOW
               ptttdi = Cast(NMTTDISPINFOW Ptr, lParam)
               ptttdi->hinst = Null
               wszTooltipText = ""
               Select Case ptttdi->hdr.hwndFrom
                  Case Toolbar_GetTooltips(GetDlgItem(GetDlgItem(hWndForm, IDC_REBAR), IDC_TOOLBAR))
                     Select Case ptttdi->hdr.idFrom
                        Case IDM_LEFTARROW  : wszTooltipText = "Left arrow"
                        Case IDM_RIGHTARROW : wszTooltipText = "Right arrow"
                        Case IDM_HOME       : wszTooltipText = "Home"
                        Case IDM_SAVEFILE   : wszTooltipText = "Save"
                        Case IDM_HISTORY    : wszTooltipText = "History"
                        Case IDM_MAIL       : wszTooltipText = "Mail"
                        Case IDM_PRINT      : wszTooltipText = "Print"
                        Case IDM_SEARCH     : wszTooltipText = "Search"
                        Case IDM_VIEW       : wszTooltipText = "View"
                        Case IDM_STOP       : wszTooltipText = "Stop"
                     End Select
                     If Len(wszTooltipText) Then ptttdi->lpszText = @wszTooltipText
               End Select
         End Select

      Case WM_SIZE
         If wParam <> SIZE_MINIMIZED Then
            ' // Update the size and position of the Rebar control
            SendMessageW GetDlgItem(hWndForm, IDC_REBAR), WM_SIZE, wParam, lParam
            Exit Function
         End If

       Case WM_DESTROY
         ' // Destroy the image lists
         ImageList_Destroy Cast(HIMAGELIST, SendMessageW(GetDlgItem(hWndForm, IDC_TOOLBAR), TB_SETIMAGELIST, 0, 0))
         ImageList_Destroy Cast(HIMAGELIST, SendMessageW(GetDlgItem(hWndForm, IDC_TOOLBAR), TB_SETHOTIMAGELIST, 0, 0))
         ImageList_Destroy Cast(HIMAGELIST, SendMessageW(GetDlgItem(hWndForm, IDC_TOOLBAR), TB_SETDISABLEDIMAGELIST, 0, 0))

   End Select

End Function

'                                                                                  
Sub Form1_WM_Command(hWndForm As hWnd, hWndControl As hWnd, wNotifyCode As Long, wID As Long)  '命令处理（处理菜单、工具栏、状态栏等）

  Me.Label1.Caption ="工具栏事件：" & wID 

End Sub

