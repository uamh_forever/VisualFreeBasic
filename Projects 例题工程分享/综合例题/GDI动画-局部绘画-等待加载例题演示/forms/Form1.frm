﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
ClassName=
WinStyle=WS_DLGFRAME,WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=GDI动画-局部绘画-等待加载例题演示
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=442
Height=308
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Image]
Name=Image1
Index=-1
Picture=startup.bmp
Stretch=0 - 自动适应
Percent=0
GrayScale=False
Enabled=True
Visible=True
Left=-1
Top=-2
Width=428
Height=276
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]

Sub ThreadYesNo(ByVal sHWnd As hWnd) '等待线程完成
  Sleep 100
  Dim gg As yGDI
  Dim As ULong t
  Dim As Long ss, w, h, x, y, qq(5), i, sd(5)
  Dim s As String
  
  t = timeGetTime
  If sHWnd > 0 Then
      FF_Control_GetClient(sHWnd, w, h)
      x = Int(w / 2) - AfxScaleX(60)
      y = Int(h / 2) - AfxScaleY(80)
      gg.Constructor sHWnd, -1, False, x, y, AfxScaleX(120), AfxScaleY(120)
      Print gg.DrawTextS(0, 0, 10, 10, "获取一个Windows线程的状态。", DT_EDITCONTROL)
  End If        
  'FIX: 8888
  
  gg.Pen 1, &HFFFFFF
  gg.Font "Microsoft YaHei", 14
  For i = 0 To 5
      qq(i) = i * 40
  Next
  
  'HACK: 5555
    
  
  Do
      'If IsThreadAlive(DohThread  )=0  Then Exit Do    '获取一个Windows线程的状态。
      Sleep(8)
      
      If sHWnd > 0 Then
          
          For i = 0 To 5
              If qq(i) < 110 Then
                  qq(i) += 4
              ElseIf qq(i) < 250 Then
                  qq(i) += 3
              ElseIf qq(i) < 300 Then
                  qq(i) += 1
              Else
                  qq(i) += 5
              End If
              If qq(i) > 360 Then  qq(i) -= 360
          Next
          ss = (timeGetTime - t) / 100
          If ss >= 600 Then
              s = Format(Int(ss / 600), "0") & "n" & Format((ss Mod 600) / 10, "0.0") & "s"
          Else
              s = Format(ss / 10, "0.0")  & "s"
          End If
          gg.Cls -1
          'gg.DrawArc 5,5,110,110,a,c
          DrawXiaoQiu @gg, qq(0), &H0000FF
          DrawXiaoQiu @gg, qq(1), &H00FF00
          DrawXiaoQiu @gg, qq(2), &HFF0000
          DrawXiaoQiu @gg, qq(3), &HFFFF00
          DrawXiaoQiu @gg, qq(4), &HFF00FF
          DrawXiaoQiu @gg, qq(5), &H00FFFF
          gg.DrawFrameTextR 10, 23, 100, 25, "联网中", &HFFFFFF, DT_SINGLELINE Or DT_CENTER Or DT_VCENTER
          gg.DrawFrameTextR 10, 49, 100, 25, "请稍等哦", &HFFFFFF, DT_SINGLELINE Or DT_CENTER Or DT_VCENTER
          gg.DrawFrameTextR 10, 73, 100, 25, s, &HFFFFFF, DT_SINGLELINE Or DT_CENTER Or DT_VCENTER
          gg.Redraw
      End If
  Loop
  If sHWnd > 0 Then FF_Redraw sHWnd
End Sub
'--------------------------------------------------------------------------
Sub DrawXiaoQiu(ggp As Any Ptr, JiaoDu As Single, nColor As COLORREF) '描绘小球
  Dim As Long x, y
  Dim pi As Double = 0.017453292519943295  ' 3.1415926535897932 /180
  'x=中心x + w半径 * Cos(角度)
  'y=中心y + h半径 * Sin(角度)
  x = 60 + 55 *Cos(JiaoDu * pi)
  y = 60 + 55 *Sin(JiaoDu * pi)
  Dim gg As yGDI Ptr = ggp
  gg->Brush nColor
  gg->DrawEllipse x -5, y -5, 10, 10
  
End Sub

Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。

  Threaddetach ThreadCreate(Cast(Any Ptr, @ThreadYesNo), hWndForm) '经典调用方法

End Sub

