#FireFly_Form#  Version=3.91
Locked=0
ClientOffset=0
ClientWidth=181
ClientHeight=197


[ControlType] Form | PropertyCount=25 | zorder=1 | tabindex=0 | 
name=Form9
classstyles=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
windowstyles=WS_POPUP, WS_THICKFRAME, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE|WS_EX_WINDOWEDGE, WS_EX_TOPMOST, WS_EX_TOOLWINDOW, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
backbitmap=
backbitmapmode=0 - 平铺
backcolor=SYS,15
caption=
export=False
height=211
icon=
left=0
mdichild=False
minwidth=195
minheight=211
maxwidth=195
maxheight=211
multilanguage=True
startupposition=0 - 手动
tabcontrolchild=False
tabcontrolchildautosize=False
tag=
tag2=
top=0
width=195
windowstate=0 - 正常

[ControlType] Timer | PropertyCount=7 | zorder=2 | tabindex=11 | 
name=Timer1
height=24
interval=100
left=9
locked=False
top=111
width=24

[ControlType] OptionButton | PropertyCount=23 | zorder=3 | tabindex=1 | 
name=Option1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHLIKE, BS_NOTIFY, BS_AUTORADIOBUTTON, BS_LEFT, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
backcolor=SYS,15
backstyle=1 - 不透明
caption=历史
checked=0 - UnChecked
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
groupname=OptionGroup0
height=22
index=1
left=4
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=4
width=31

[ControlType] OptionButton | PropertyCount=23 | zorder=4 | tabindex=2 | 
name=Option1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHLIKE, BS_NOTIFY, BS_AUTORADIOBUTTON, BS_LEFT, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
backcolor=SYS,15
backstyle=1 - 不透明
caption=系统
checked=1 - Checked
controlindex=1
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
groupname=OptionGroup0
height=22
index=3
left=39
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=4
width=31

[ControlType] OptionButton | PropertyCount=23 | zorder=5 | tabindex=3 | 
name=Option1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHLIKE, BS_NOTIFY, BS_AUTORADIOBUTTON, BS_LEFT, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
backcolor=SYS,15
backstyle=1 - 不透明
caption=色块
checked=0 - UnChecked
controlindex=2
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
groupname=OptionGroup0
height=22
index=4
left=109
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=4
width=31

[ControlType] OptionButton | PropertyCount=23 | zorder=6 | tabindex=4 | 
name=Option1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHLIKE, BS_NOTIFY, BS_AUTORADIOBUTTON, BS_LEFT, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
backcolor=SYS,15
backstyle=1 - 不透明
caption=调色
checked=0 - UnChecked
controlindex=3
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
groupname=OptionGroup0
height=22
index=5
left=144
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=4
width=31

[ControlType] ListBox | PropertyCount=20 | zorder=1 | tabindex=10 | 
name=List1
windowstyles=WS_CHILD, WS_VISIBLE, WS_VSCROLL, WS_TABSTOP, SINGLE SELECT, LBS_OWNERDRAWFIXED, LBS_NOTIFY, LBS_NOINTEGRALHEIGHT|WS_EX_CLIENTEDGE, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
custom=
backcolor=SYS,5
columns=0
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
height=78
initialselection=-1
left=4
locked=False
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=29
width=50

[ControlType] Picture | PropertyCount=15 | zorder=2 | tabindex=9 | 
name=Picture1
windowstyles=WS_CHILD, WS_BORDER, WS_VISIBLE, WS_TABSTOP, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, SS_BITMAP, SS_NOTIFY, SS_REALSIZEIMAGE, LR_DEFAULTCOLOR, LR_SHARED|
controlindex=0
height=58
iconsize=No Preference
left=64
locked=False
picture=
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=36
width=67

[ControlType] Picture | PropertyCount=15 | zorder=3 | tabindex=8 | 
name=Picture2
windowstyles=WS_CHILD, WS_BORDER, WS_VISIBLE, WS_TABSTOP, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, SS_BITMAP, SS_NOTIFY, SS_REALSIZEIMAGE, LR_DEFAULTCOLOR, LR_SHARED|
controlindex=0
height=75
iconsize=No Preference
left=77
locked=False
picture=
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=49
width=94

[ControlType] TextBox | PropertyCount=22 | zorder=1 | tabindex=6 | 
name=Text1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, ES_LEFT, ES_AUTOHSCROLL|WS_EX_CLIENTEDGE, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
backcolor=SYS,5
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
height=20
left=9
locked=False
leftmargin=0
maxlength=8
rightmargin=0
resizerules=FL,FT,FR,FT
seltext=False
text=
tag=
tag2=
tooltip=
tooltipballoon=False
top=139
width=70

[ControlType] Picture | PropertyCount=15 | zorder=2 | tabindex=7 | 
name=Picture3
windowstyles=WS_CHILD, WS_VISIBLE, WS_CLIPCHILDREN, SS_ICON, SS_NOTIFY, SS_REALSIZEIMAGE, LR_DEFAULTCOLOR, LR_SHARED|
controlindex=0
height=32
iconsize=32x32
left=127
locked=False
picture=
resizerules=
tag=
tag2=
tooltip=拖动它，获取窗口信息
tooltipballoon=False
top=134
width=32

[ControlType] OptionButton | PropertyCount=23 | zorder=1 | tabindex=5 | 
name=Option1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHLIKE, BS_NOTIFY, BS_AUTORADIOBUTTON, BS_LEFT, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
backcolor=SYS,15
backstyle=1 - 不透明
caption=参考
checked=0 - UnChecked
controlindex=4
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
groupname=OptionGroup0
height=22
index=6
left=74
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=4
width=31

[AllCode]
Type F9_colour
    colour As  Long 
    cText As  String  '显示的名词
    cTo As String  '发送给编辑器
End Type
Dim Shared F9_c() As F9_colour ,F9_Move As Long , F9_ls() As F9_colour,F9_ChkCol As Long ,F9_tempCol As Long,F9_xigai As Long,g获取窗口模式 As Long 
Dim Shared As HWnd g图标句柄1,g图标句柄2 '图标
'--------------------------------------------------------------------------------
Function FORM9_WM_CREATE ( _
                         hWndForm As HWnd, _          ' 窗体句柄
                         ByVal UserData As Integer _  ' 可选的用户定义的值
                         ) As Long
  Dim pt As Point
  GetCursorPos @pt
  SetWindowPos( hWndForm, 0,  pt.x, pt.y, 0, 0, SWP_NOZORDER Or SWP_NOSIZE )
  FORM9_OPTION1_BN_CLICKED ( 1,hWndForm,0,0)
  g图标句柄1=LoadImage(App.hInstance,"IMAGE_1", IMAGE_ICON, 0, 0, LR_SHARED)
  g图标句柄2=LoadImage(App.hInstance,"IMAGE_2", IMAGE_ICON, 0, 0, LR_SHARED)
  Picture_SetIcon( HWND_FORM9_PICTURE3,g图标句柄1 )
  SetParent HWND_FORM9_PICTURE3,HWND_FORM9_PICTURE2
  SetParent HWND_FORM9_TEXT1,HWND_FORM9_PICTURE2
  FF_Control_SetLoc HWND_FORM9_TEXT1,AfxScaleX(5),AfxScaleY(114)
  FF_Control_SetLoc HWND_FORM9_PICTURE3,AfxScaleX(127),AfxScaleY(102)
  
  
  Function = 0   ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Function FORM9_OPTION1_BN_CLICKED ( _
                                  ControlIndex     As Long, _      ' 控件数组的索引
                                  hWndForm         As HWnd, _      ' 窗体句柄
                                  hWndControl      As HWnd, _      ' 控件句柄
                                  idButtonControl  As Long   _     ' 按钮的标识符
                                  ) As Long
  Dim i As Long,u As Long   ,s As String ,t As String
  Select Case ControlIndex
      Case 0
          SetWindowPos( HWND_FORM9_PICTURE1, 0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_PICTURE2, 0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_LIST1,    0, AfxScaleX(4), AfxScaleY(29), AfxScaleX(173), AfxScaleY(163), SWP_NOZORDER  Or SWP_NOACTIVATE)
          FF_ListBox_ResetContent( HWND_FORM9_LIST1 )
          Erase F9_c
          u=UBound(F9_ls)
          If u>-1 Then
              For i=0 To u
                  t=F9_ls(i).cText
                  s=F9_ls(i).cTo
                  F9_AddShuJu F9_ls(i).colour,t,s
              Next
          End If
          u=ValLng(FF_Control_GetTag(HWND_FORM9_OPTION1(ControlIndex)))
          FF_ListBox_SetCurSel HWND_FORM9_LIST1,u
          FF_Control_SetFocus( HWND_FORM9_LIST1 )
      Case 1
          SetWindowPos( HWND_FORM9_PICTURE1, 0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_PICTURE2, 0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_LIST1,    0, AfxScaleX(4), AfxScaleY(29), AfxScaleX(173), AfxScaleY(163), SWP_NOZORDER  Or SWP_NOACTIVATE)
          FF_ListBox_ResetContent( HWND_FORM9_LIST1 )
          Erase F9_c
          F9_添加色彩
          u=ValLng(FF_Control_GetTag(HWND_FORM9_OPTION1(ControlIndex)))
          FF_ListBox_SetCurSel HWND_FORM9_LIST1,u
          FF_Control_SetFocus( HWND_FORM9_LIST1 )
      Case 2
          F9_Move=-1
          SetWindowPos( HWND_FORM9_PICTURE1, 0, AfxScaleX(4), AfxScaleY(29), AfxScaleX(173), AfxScaleY(163), SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_PICTURE2, 0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_LIST1,    0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          FF_ListBox_ResetContent( HWND_FORM9_LIST1 )
          Erase F9_c
          
          
      Case 3
          InvalidateRect (HWND_FORM9_PICTURE2 ,ByVal Null, True)
          SetWindowPos( HWND_FORM9_PICTURE1, 0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_PICTURE2, 0, AfxScaleX(4), AfxScaleY(29), AfxScaleX(173), AfxScaleY(163), SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_LIST1,    0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          FF_ListBox_ResetContent( HWND_FORM9_LIST1 )
          Erase F9_c
      Case 4
          SetWindowPos( HWND_FORM9_PICTURE1, 0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_PICTURE2, 0, 400, 29, 172, 163, SWP_NOZORDER  Or SWP_NOACTIVATE)
          SetWindowPos( HWND_FORM9_LIST1,    0, AfxScaleX(4), AfxScaleY(29), AfxScaleX(173), AfxScaleY(163), SWP_NOZORDER  Or SWP_NOACTIVATE)
          FF_ListBox_ResetContent( HWND_FORM9_LIST1 )
          Erase F9_c
          F9_添加色彩2
          u=ValLng(FF_Control_GetTag(HWND_FORM9_OPTION1(ControlIndex)))
          FF_ListBox_SetCurSel HWND_FORM9_LIST1,u
          FF_Control_SetFocus( HWND_FORM9_LIST1 )
  End Select
  FF_Control_SetTag HWND_FORM9_LIST1,Str(ControlIndex)
  
  Function = 0   ' 根据你的需要改变
End Function
'--------------------------------------------------------------------------
Sub F9_AddShuJu(c As Long ,t As String ,s As String ) '增加数据
    Dim u As Long 
    u=UBound(F9_c)+1
    ReDim Preserve F9_c(u)
    F9_c(u).colour=c
    F9_c(u).cText=t
    F9_c(u).cTo=s
    FF_ListBox_AddString( HWND_FORM9_LIST1, Str(u) )
End Sub

'--------------------------------------------------------------------------------
Function FORM9_CUSTOM ( _
                      hWndForm      As HWnd, _      ' 窗体句柄
                      wMsg          As UInteger,  _  ' 消息类型
                      wParam        As wParam, _    ' 第一个消息参数
                      lParam        As lParam   _   ' 第二个消息参数
                      ) As Long
    Dim i      As Long
    Dim c      As Long
    Dim itd    As Long
    Dim rc     As Rect
    Dim lpdis  As DRAWITEMSTRUCT Ptr
    Dim wszTxt As WString * 300
    Dim TEXTMARGIN As Long 
    TEXTMARGIN =AfxScaleX(30) ' 文本左边的距
    Select Case wMsg
     Case WM_DRAWITEM                  ' 在所有者描述的控件中绘制项目
        If wParam = IDC_FORM9_LIST1 Then    ' CBWPARAM 保留控件 ID
            lpdis =Cast(Any Ptr, lParam)            ' CBLPARAM 指向 DRAWITEMSTRUCT 结构
            If lpdis->itemID = &HFFFFFFFF Then Exit Function '如果列表为空
            rc = lpdis->rcItem

            Select Case lpdis->itemAction
            Case ODA_DRAWENTIRE, ODA_SELECT

                If (lpdis->itemState And ODS_SELECTED) = 0 Then                ' 未选中
                    FillRect lpdis->hDC, @rc, GetSysColorBrush(COLOR_WINDOW)    ' 清晰的背景
                    SetBkColor lpdis->hDC, GetSysColor(COLOR_WINDOW)           ' 文本背景
                    SetTextColor lpdis->hDC, GetSysColor(COLOR_WINDOWTEXT)     ' 文本颜色
                Else                                                            ' 处于选中状态
                    rc.Right = TEXTMARGIN - 2                                 ' 调整矩形的右边
                    FillRect lpdis->hDC, @rc, GetSysColorBrush(COLOR_3DFACE)    ' 位图背景
                    rc.Left = TEXTMARGIN - 2                                  ' 调整左边的矩形
                    rc.Right = lpdis->rcItem.Right                            ' 和右侧的矩形
                    FillRect lpdis->hDC, @rc, GetSysColorBrush(COLOR_HIGHLIGHT) ' "选定"的背景
                    SetBkColor lpdis->hDC, GetSysColor(COLOR_HIGHLIGHT)        ' 文本背景
                    SetTextColor lpdis->hDC, GetSysColor(COLOR_HIGHLIGHTTEXT)  ' 文本颜色
                End If

                ' Get/绘制当前项的文本
                ' 轻微调整文本位置
                 rc.Top +=1
                rc.Left= AfxScaleX(2)
                DrawFrame( lpdis->hDC, rc.Left, rc.Top, rc.Left+AfxScaleX(23), rc.Top+ AfxScaleY(14) ,F9_c(lpdis->itemID).colour, 0, 0 )
                rc.Left = TEXTMARGIN
                DrawText lpdis->hDC, F9_c(lpdis->itemID).cText, Len(F9_c(lpdis->itemID).cText), @rc, DT_SINGLELINE Or DT_LEFT Or DT_VCENTER

                ' 获取当前项的位图
'                ListBox Get User CB.HNDL, %IDC_LIST, @lpdis.itemID + 1 To itd

                ' 绘制当前项的位图

                Function = True
                Exit Function

            End Select
        End If

    Case WM_NCMOUSEMOVE
          Static curaa As HCURSOR
          If curaa=0 Then curaa=LoadCursor(Null, IDC_ARROW)
          SetCursor(curaa)
        Return 1
    End Select
   Function = 0   ' 根据你的需要改变
End Function

'--------------------------------------------------------------------------------
Function FORM9_PICTURE1_WM_PAINT ( _
                                 ControlIndex  As Long,  _     ' 控件数组索引
                                 hWndForm      As HWnd, _      ' 窗体句柄
                                 hWndControl   As HWnd  _      ' 控件句柄
                                 ) As Long
  Dim nDC As   hDC
  Dim ps As PAINTSTRUCT
  Dim As Long   w,h ,a,r,g,b,xx,yy,x,y,cc,i,ff
  
  nDC=BeginPaint(hWndControl,@ps)
  FF_Control_GetSize( hWndControl, w, h )
  Dim pMemBmp As CMemBmp = CMemBmp(w,h)
  DrawFrame( pMemBmp.GetMemDC, 0, 0, w, h,GetSysColor(COLOR_BTNFACE), 5, 0 )
  
  y=AfxScaleY(2)
  For i=0 To 8
      cc=(8-i)*&H20:If cc>&HFF Then cc=&HFF
      cc=BGR(cc,cc,cc)
      x=i*AfxScaleX(18)+AfxScaleX(4)
      DrawFrame( pMemBmp.GetMemDC, x, y,x+AfxScaleX(19) ,y+AfxScaleY(15),cc, 0, 0 )
      If F9_Move=yy*9+i Then
          DrawFrame( pMemBmp.GetMemDC, x+1, y+1,x+AfxScaleX(19)-1 ,y+AfxScaleY(15)-1,cc, 0, 0 )
      End If
  Next
  For yy=1 To 9
      y=yy*AfxScaleY(14)+AfxScaleY(2)
      For i=1 To 9
          cc=(8-i)*&H20
          If i=9 Then ff=&H80 Else ff=&HFF
          Select Case yy
              Case 1:cc=BGR(ff,cc,cc)
              Case 2:cc=BGR(cc,ff,cc)
              Case 3:cc=BGR(cc,cc,ff)
              Case 4:cc=BGR(cc,ff,ff)
              Case 5:cc=BGR(ff,cc,ff)
              Case 6:cc=BGR(ff,ff,cc)
              Case 7:cc=BGR(&HFF-cc,ff,cc)
              Case 8:cc=BGR(cc,&HFF-cc,ff)
              Case 9:cc=BGR(ff,cc,&HFF-cc)
              Case 5:cc=BGR(ff,cc,ff)
                  
          End Select
          x=i*AfxScaleX(18)-AfxScaleX(14)
          DrawFrame( pMemBmp.GetMemDC, x, y,x+AfxScaleX(19) ,y+AfxScaleY(15),cc, 0, 0 )
          If F9_Move=yy*9+(i-1) Then
              DrawFrame( pMemBmp.GetMemDC, x+1, y+1,x+AfxScaleX(19)-1 ,y+AfxScaleY(15)-1,cc, 0, 0 )
          End If
          
      Next
  Next
  If F9_Move<>-1 Then
      F9_tempCol=GetPixel(pMemBmp.GetMemDC,(F9_Move Mod 9)*AfxScaleX(18)+AfxScaleX(10),Int(F9_Move/9)*AfxScaleY(14)+AfxScaleY(5) )
  End If
   DrawFrame( pMemBmp.GetMemDC, AfxScaleX(4), AfxScaleY(142),AfxScaleX(167) ,AfxScaleX(160),F9_ChkCol, 0, 0 )
'  TextOut1( pMemBmp.GetMemDC,AfxScaleX(65),AfxScaleY(148),"&H" & Hex(F9_ChkCol,6) & " 双击发送")
  SetTextColor pMemBmp.GetMemDC, NotColor(F9_ChkCol)
  TextOut1( pMemBmp.GetMemDC,AfxScaleX(33),AfxScaleY(144),"&H" & Hex(F9_ChkCol,6) & " 双击发送")  
  BitBlt ndc,0,0,w,h,pMemBmp.GetMemDC,0,0,SrcCopy
  EndPaint(hWndControl,@ps)
  
  Function=True
End Function


'--------------------------------------------------------------------------------
Function FORM9_PICTURE1_CUSTOM ( _
                               ControlIndex  As Long,  _     ' 控件数组索引
                               hWndForm      As HWnd, _      ' 窗体句柄
                               hWndControl   As HWnd, _      ' 控件句柄
                               wMsg          As UInteger,  _  ' 消息类型
                               wParam        As wParam, _    ' 第一个消息参数
                               lParam        As lParam   _   ' 第二个消息参数
                               ) As Long
Dim entTrack As tagTRACKMOUSEEVENT
Select Case  wMsg
    Case WM_MOUSELEAVE   '鼠标出窗口

           F9_Move=-1
           InvalidateRect (hWndControl ,Null, True)
   Case WM_NCHITTEST        '启用鼠标出窗口检查 
        entTrack.cbSize=SizeOf(tagTRACKMOUSEEVENT)
        entTrack.dwFlags=TME_LEAVE
        entTrack.hwndTrack=hWndControl
        entTrack.dwHoverTime =HOVER_DEFAULT
        TrackMouseEvent @entTrack  
    Case  WM_ERASEBKGND 
         Function=1  '防止擦除背景
End Select

End Function

'--------------------------------------------------------------------------------
Function FORM9_PICTURE2_WM_PAINT ( _
                                 ControlIndex  As Long,  _     ' 控件数组索引
                                 hWndForm      As HWnd, _      ' 窗体句柄
                                 hWndControl   As HWnd  _      ' 控件句柄
                                 ) As Long
  
  Dim nDC As  hDC
  Dim ps As PAINTSTRUCT
  Dim  As Long w,h ,a,i,x,y,cc,yy,xx,r,g,b
  Dim RGBAptr As UByte Ptr
  nDC=BeginPaint(hWndControl,@ps)
  FF_Control_GetSize( hWndControl, w, h )
  Dim pMemBmp As CMemBmp = CMemBmp(w,h)
  DrawFrame( pMemBmp.GetMemDC, 0, 0, w, h,GetSysColor(COLOR_BTNFACE), 5, 0 )
  
  TextOut1( pMemBmp.GetMemDC,AfxScaleX(5),0,"R",0,22,"黑体")
  TextOut1( pMemBmp.GetMemDC,AfxScaleX(5),AfxScaleY(30),"G",0,22,"黑体")
  TextOut1( pMemBmp.GetMemDC,AfxScaleX(5),AfxScaleY(60),"B",0,22,"黑体")
  'TextOut1( pMemBmp.GetMemDC,5,92,"A",0,24,"黑体")
  RGBAptr=Cast(Any Ptr, @F9_ChkCol)
  r=RGBAptr[0]
  g=RGBAptr[1]
  b=RGBAptr[2]
  For yy=0 To 2
      y=yy*AfxScaleY(30)+AfxScaleY(8)

      For i=0 To 128
          x=AfxScaleX(30+i)
          xx=i*2:If xx>255 Then xx=255
          Select Case yy
              Case 0: cc=BGR(xx,g,b)
              Case 1: cc=BGR(r,xx,b)
              Case 2: cc=BGR(r,g,xx)
          End Select
          DrawLine( pMemBmp.GetMemDC, x, y, x, y+AfxScaleY(15),0, 1,cc )
          DrawLine( pMemBmp.GetMemDC, x-1, y, x-1, y+AfxScaleY(15),0, 1,cc )
      Next
      DrawFrame( pMemBmp.GetMemDC, AfxScaleX(29), y-1, AfxScaleX(160), y+AfxScaleY(15)+1,-1, 0, 0 )      
      x=AfxScaleX(30+Int(RGBAptr[yy]/2))
      y=y+AfxScaleY(15)
      DrawLine( pMemBmp.GetMemDC, x, y, x+5, y+8,0, 1,0 )
      DrawLine( pMemBmp.GetMemDC, x+5, y+8, x-5, y+8,0, 1,0 )
      DrawLine( pMemBmp.GetMemDC, x-5, y+8, x, y,0, 1,0 )
      
  Next
  DrawFrame( pMemBmp.GetMemDC, AfxScaleX(4), AfxScaleY(140),AfxScaleX(167) ,AfxScaleY(158),F9_ChkCol, 0, 0 )
 
'  TextOut1( pMemBmp.GetMemDC,AfxScaleX(65),AfxScaleY(145),"&H" & Hex(F9_ChkCol,6) & " 双击发送")
  SetTextColor pMemBmp.GetMemDC, NotColor(F9_ChkCol)
  TextOut1( pMemBmp.GetMemDC,AfxScaleX(30),AfxScaleY(142),"&H" & Hex(F9_ChkCol,6) & " 双击发送")
    
  BitBlt ndc,0,0,w,h,pMemBmp.GetMemDC,0,0,SrcCopy
  EndPaint(hWndControl,@ps)
  Function=True
  If F9_xigai=0 Then
      F9_xigai=1
      FF_Control_SetText HWND_FORM9_TEXT1,"&H" & Hex(F9_ChkCol,6)
  End If
  F9_xigai=0
End Function
'--------------------------------------------------------------------------
Sub FORM9_PICTURE2(xPos As Long ,yPos As Long ) '
  Dim RGBAptr As UByte Ptr
  Dim As Long  i,y,xx
  
  If xPos>=AfxScaleX(20) And xPos<=AfxScaleY(168) Then
      If xPos<=AfxScaleX(30) Then xx=AfxScaleX(30) Else xx= xPos
      xx=(xx-AfxScaleX(30))/AfxScaleX(1)*2
      If xx>255 Then xx=255
      RGBAptr=Cast(Any Ptr, @F9_ChkCol)
      For i=0 To 2
          y=i*AfxScaleY(30)+AfxScaleY(8)
          If yPos>=y And yPos<=y+AfxScaleY(25) Then
              RGBAptr[i]=xx
              FF_Redraw HWND_FORM9_PICTURE2
              Exit For
          End If
      Next
      
  End If
  
End Sub

'--------------------------------------------------------------------------------
Function FORM9_PICTURE1_WM_MOUSEMOVE ( _
                                     ControlIndex  As Long,  _     ' 控件数组索引
                                     hWndForm      As HWnd, _      ' 窗体句柄
                                     hWndControl   As HWnd, _      ' 控件句柄
                                     MouseFlags    As Long,  _     ' 按下的虚拟键
                                     xPos          As Long,  _     ' 光标的x坐标
                                     yPos          As Long   _     ' 光标的y坐标
                                     ) As Long
Dim x As Long ,y As Long,a As Long   
                                 
x=Int((xPos-AfxScaleX(4))/AfxScaleX(18))
y=Int((yPos-AfxScaleY(2))/AfxScaleY(14))
 
If x>=0 And x<9 And y>=0 And y<10 Then
    a=y*9+x
Else
    a=-1
End If
If a<>F9_Move Then
    F9_Move=a
    InvalidateRect (hWndControl ,Null, True)
End If
   Function = 0   ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Function FORM9_LIST1_LBN_SELCHANGE ( _
                                   ControlIndex  As Long, _     ' 控件数组的索引
                                   hWndForm      As HWnd, _     ' 窗体句柄
                                   hWndControl   As HWnd, _     ' 控件句柄
                                   idListBox     As Long  _     ' ListBox控件的标识符
                                   ) As Long
Dim a As Long ,t As String ,s As String,i As Long  
a=FF_ListBox_GetCurSel( HWND_FORM9_LIST1 )
If a>-1 Then
    F9_ChkCol=F9_c(a).colour
End If
i=ValLng(FF_Control_GetTag(HWND_FORM9_LIST1))
FF_Control_SetTag(HWND_FORM9_OPTION1(i),Str(a))

   Function = 0   ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Function FORM9_LIST1_LBN_DBLCLK ( _
                                ControlIndex  As Long, _     ' 控件数组的索引
                                hWndForm      As HWnd, _     ' 窗体句柄
                                hWndControl   As HWnd, _     ' 控件句柄
                                idListBox     As Long  _     ' ListBox控件的标识符
                                ) As Long
Dim a As Long ,t As String ,s As String 
a=FF_ListBox_GetCurSel( HWND_FORM9_LIST1 )
If a>-1 Then
    t=F9_c(a).cText
    s=F9_c(a).cTo
    F9_AddLS( F9_c(a).colour,t,s ) '增加历史已经发送的编辑器
End If
   Function = 0   ' 根据你的需要改变
End Function
'--------------------------------------------------------------------------
Sub F9_AddLS(c As Long ,t As String ,s As String ) '增加历史以及发送编辑器
  Dim u As Long ,i As Long ,a As Long,ii As Long
  u=UBound(F9_ls)
  If u>-1 Then
      For i=0 To u
          If F9_ls(i).colour=c Then
              If i<u Then
                  For ii=i To u-1
                      F9_ls(ii)=F9_ls(ii+1)
                  Next
              End If
              a=1
              Exit For
          End If
      Next
  End If
  If a=0 Then
      u=UBound(F9_ls)+1
      ReDim Preserve F9_ls(u)
  End If
  If u>0 Then
      For i=u To 1 Step -1
          F9_ls(i)=F9_ls(i-1)
      Next
  End If
  F9_ls(0).colour=c
  F9_ls(0).cText=t
  F9_ls(0).cTo=s
'  If IsWindow(F14_Hwnd)<>0 Then
'      F14_选择了颜色 c
'  Else
'      Dim ffc As HWnd =FindWindowEx(ffid,0,"CODE_WINDOW",Null)
'      Dim jeditID As HWnd =FindWindowEx(ffc,0,"JELLYEDIT_CLASS",Null)
'      If jeditID<>0 Then  JELLYEDIT_REPLACESELTEXT jeditID ,SysAllocStringByteLen(StrPtr(s),Len(s)) '传给POB的DLL字符，
'  End If
  FF_Control_ShowState( HWND_FORM9, SW_HIDE )
End Sub


'--------------------------------------------------------------------------------
Function FORM9_PICTURE1_STN_DBLCLK ( _
                                   ControlIndex     As Long,  _     ' 控件数组的索引
                                   hWndForm         As HWnd, _      ' 窗体句柄
                                   hWndControl      As HWnd, _      ' 控件句柄
                                   idStaticControl  As Long   _     ' 静态控件的标识符
                                   ) As Long
   Dim ss As String 
   ss=  Hex(F9_ChkCol,6)
F9_AddLS( F9_ChkCol,"自定义 " & ss,"&H" & ss ) '增加历史已经发送的编辑器
   Function = 0   ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Function FORM9_PICTURE2_WM_LBUTTONDOWN ( _
                                       ControlIndex  As Long,  _     ' 控件数组索引
                                       hWndForm      As HWnd, _      ' 窗体句柄
                                       hWndControl   As HWnd, _      ' 控件句柄
                                       MouseFlags    As Long,  _     ' 按下的虚拟键
                                       xPos          As Long,  _     ' 光标的x坐标
                                       yPos          As Long   _     ' 光标的y坐标
                                       ) As Long
FORM9_PICTURE2(xPos ,yPos ) '
   Function = 0   ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Function FORM9_PICTURE2_WM_MOUSEMOVE ( _
                                     ControlIndex  As Long,  _     ' 控件数组索引
                                     hWndForm      As HWnd, _      ' 窗体句柄
                                     hWndControl   As HWnd, _      ' 控件句柄
                                     MouseFlags    As Long,  _     ' 按下的虚拟键
                                     xPos          As Long,  _     ' 光标的x坐标
                                     yPos          As Long   _     ' 光标的y坐标
                                     ) As Long
If MouseFlags<>0 Then     

FORM9_PICTURE2(xPos ,yPos ) 
End If
   Function = 0   ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Function FORM9_PICTURE1_WM_LBUTTONDOWN ( _
                                       ControlIndex  As Long,  _     ' 控件数组索引
                                       hWndForm      As HWnd, _      ' 窗体句柄
                                       hWndControl   As HWnd, _      ' 控件句柄
                                       MouseFlags    As Long,  _     ' 按下的虚拟键
                                       xPos          As Long,  _     ' 光标的x坐标
                                       yPos          As Long   _     ' 光标的y坐标
                                       ) As Long
Dim x As Long ,y As Long,a As Long   
                                 
x=Int((xPos-AfxScaleX(4))/AfxScaleX(18))
y=Int((yPos-AfxScaleY(2))/AfxScaleY(14))
 
If x>=0 And x<9 And y>=0 And y<10 Then
    a=y*9+x
   If F9_Move=a Then
F9_ChkCol=F9_tempCol
InvalidateRect (hWndControl ,Null, True)

    End If
End If

   Function = 0   ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Function FORM9_PICTURE2_STN_DBLCLK ( _
                                   ControlIndex     As Long,  _     ' 控件数组的索引
                                   hWndForm         As HWnd, _      ' 窗体句柄
                                   hWndControl      As HWnd, _      ' 控件句柄
                                   idStaticControl  As Long   _     ' 静态控件的标识符
                                   ) As Long
   Dim ss As String 
   ss=  Hex(F9_ChkCol,6)
F9_AddLS( F9_ChkCol,"自定义 " & ss,"&H" & ss ) '增加历史已经发送的编辑器
   Function = 0   ' 根据你的需要改变
End Function



'--------------------------------------------------------------------------------
Function FORM9_TIMER1_WM_TIMER ( _
                               hWndForm      As HWnd, _     ' 窗体句柄
                               wTimerID      As Long  _  ' 计时器标识符
                               ) As Long
  If hWndForm <> GetForegroundWindow  Then FF_Control_ShowState( hWndForm, SW_HIDE )
  Dim  A As  Point
  Dim As  HWnd dd
  Dim As hDC zmDC
  If g获取窗口模式 Then '处于获取窗口模式中
      If GetAsyncKeyState(1)=0 Then '表示鼠标放开，终止了获取窗口模式中
          g获取窗口模式=0
          ReleaseCapture()
          SetCursor(0)
          Picture_SetIcon( HWND_FORM9_PICTURE3,g图标句柄1 )
      Else
          GetCursorPos @a
          zmDC=GetWindowDC(0)
          F9_ChkCol=GetPixel(zmDC,a.x,a.y)
          ReleaseDC  0,zmDC
          FF_Redraw HWND_FORM9_PICTURE2
          
      End If
  End If
  
  
  
  
  Function = 0   ' 根据你的需要改变
End Function



'--------------------------------------------------------------------------------
Function FORM9_TEXT1_EN_CHANGE ( _
                               ControlIndex   As Long, _      ' 控件数组的索引
                               hWndForm       As HWnd, _      ' 窗体句柄
                               hWndControl    As HWnd, _      ' 控件句柄
                               idTextControl  As Long _       ' 文本控件的标识符
                               ) As Long
If F9_xigai<>0 Then Exit Function  
Dim aa As String =FF_Control_GetText(HWND_FORM9_TEXT1)
aa=UCase(aa)
If Left(aa,2)<>"&H" Then aa= "&H" & aa
F9_ChkCol=ValInt(aa)
F9_xigai=2
FF_Redraw HWND_FORM9_PICTURE2


   Function = 0   ' 根据你的需要改变
End Function



'--------------------------------------------------------------------------------
Function FORM9_PICTURE3_WM_LBUTTONDOWN ( _
                                       ControlIndex  As Long,  _     ' 控件数组索引
                                       hWndForm      As HWnd, _      ' 窗体句柄
                                       hWndControl   As HWnd, _      ' 控件句柄
                                       MouseFlags    As Long,  _     ' 按下的虚拟键
                                       xPos          As Long,  _     ' 光标的x坐标
                                       yPos          As Long   _     ' 光标的y坐标
                                       ) As Long
  Picture_SetIcon( HWND_FORM9_PICTURE3,g图标句柄2 )
  g获取窗口模式=1 '进入获取窗口模式中
  Static aa As  HCURSOR
  Dim As  Long  x,y,w,h
  If aa=0 Then aa=LoadCursor(App.hInstance , "#500")
  '     If aa=0 Then aa=LoadImage(app.hInstance,"IMAGE_C1", IMAGE_CURSOR, 0, 0, LR_SHARED)'aa=LoadCursorFromFile("1.cur")
  SetCursor(aa)
  SetCapture(hWndControl)
  
  
  
  Function = 1   ' 根据你的需要改变
  
End Function
'--------------------------------------------------------------------------
Sub F9_添加色彩() '
      F9_AddShuJu GetSysColor(COLOR_SCROLLBAR),"滚动条","GetSysColor(COLOR_SCROLLBAR)"
      F9_AddShuJu GetSysColor(COLOR_BACKGROUND),"桌面","GetSysColor(COLOR_BACKGROUND)"
      F9_AddShuJu GetSysColor(COLOR_ACTIVECAPTION),"活动标题栏","GetSysColor(COLOR_ACTIVECAPTION)"
      F9_AddShuJu GetSysColor(COLOR_INACTIVECAPTION),"非活动标题栏","GetSysColor(COLOR_INACTIVECAPTION)"
      F9_AddShuJu GetSysColor(COLOR_MENU),"菜单条","GetSysColor(COLOR_MENU)"
      F9_AddShuJu GetSysColor(COLOR_WINDOW),"窗口背景","GetSysColor(COLOR_WINDOW)"
      F9_AddShuJu GetSysColor(COLOR_WINDOWFRAME),"窗口窗框","GetSysColor(COLOR_WINDOWFRAME)"
      F9_AddShuJu GetSysColor(COLOR_MENUTEXT),"菜单文本","GetSysColor(COLOR_MENUTEXT)"
      F9_AddShuJu GetSysColor(COLOR_WINDOWTEXT),"窗口文本","GetSysColor(COLOR_WINDOWTEXT)"
      F9_AddShuJu GetSysColor(COLOR_CAPTIONTEXT),"活动标题栏文本","GetSysColor(COLOR_CAPTIONTEXT)"
      F9_AddShuJu GetSysColor(COLOR_ACTIVEBORDER),"活动窗口的边框","GetSysColor(COLOR_ACTIVEBORDER)"
      F9_AddShuJu GetSysColor(COLOR_INACTIVEBORDER),"不活动窗口的边框","GetSysColor(COLOR_INACTIVEBORDER)"
      F9_AddShuJu GetSysColor(COLOR_APPWORKSPACE),"MDI桌面的背景","GetSysColor(COLOR_APPWORKSPACE)"
      F9_AddShuJu GetSysColor(COLOR_HIGHLIGHT),"选定项背景(突出显示)","GetSysColor(COLOR_HIGHLIGHT)"
      F9_AddShuJu GetSysColor(COLOR_HIGHLIGHTTEXT),"选定项目文字(突出文字)","GetSysColor(COLOR_HIGHLIGHTTEXT)"
      F9_AddShuJu GetSysColor(COLOR_BTNFACE),"按钮表面","GetSysColor(COLOR_BTNFACE)"
      F9_AddShuJu GetSysColor(COLOR_BTNSHADOW),"按钮的3D阴影","GetSysColor(COLOR_BTNSHADOW)"
      F9_AddShuJu GetSysColor(COLOR_GRAYTEXT),"灰色文字(无效文本)","GetSysColor(COLOR_GRAYTEXT)"
      F9_AddShuJu GetSysColor(COLOR_BTNTEXT),"按钮文字","GetSysColor(COLOR_BTNTEXT)"
      F9_AddShuJu GetSysColor(COLOR_INACTIVECAPTIONTEXT),"非活动标题栏文本","GetSysColor(COLOR_INACTIVECAPTIONTEXT)"
      F9_AddShuJu GetSysColor(COLOR_BTNHIGHLIGHT),"按钮3D加亮区","GetSysColor(COLOR_BTNHIGHLIGHT)"
      F9_AddShuJu GetSysColor(COLOR_3DDKSHADOW),"按钮3D深阴影","GetSysColor(COLOR_3DDKSHADOW)"
      F9_AddShuJu GetSysColor(COLOR_3DLIGHT),"按钮3D亮阴影","GetSysColor(COLOR_3DLIGHT)"
      F9_AddShuJu GetSysColor(COLOR_INFOTEXT),"工具提示的文本","GetSysColor(COLOR_INFOTEXT)"
      F9_AddShuJu GetSysColor(COLOR_INFOBK),"工具提示的背景色","GetSysColor(COLOR_INFOBK)"
      F9_AddShuJu GetSysColor(COLOR_HOTLIGHT),"超链接或热追踪","GetSysColor(COLOR_HOTLIGHT)"
      F9_AddShuJu GetSysColor(COLOR_GRADIENTACTIVECAPTION),"活动标题栏右侧","GetSysColor(COLOR_GRADIENTACTIVECAPTION)"
      F9_AddShuJu GetSysColor(COLOR_GRADIENTINACTIVECAPTION),"非活动标题栏右侧","GetSysColor(COLOR_GRADIENTINACTIVECAPTION)"
      F9_AddShuJu GetSysColor(COLOR_MENUHILIGHT),"平面菜单突出显示","GetSysColor(COLOR_MENUHILIGHT)"
      F9_AddShuJu GetSysColor(COLOR_MENUBAR),"平面菜单背景颜色","GetSysColor(COLOR_MENUBAR)"


End Sub
'--------------------------------------------------------------------------
Sub F9_添加色彩2() '
  F9_AddShuJu &H000000,"黑色","&H000000"
  F9_AddShuJu &H800000,"海军","&H800000"
  F9_AddShuJu &H8B0000,"深蓝","&H8B0000"
  F9_AddShuJu &HCD0000,"暗蓝","&HCD0000"
  F9_AddShuJu &HFF0000,"蓝色","&HFF0000"
  F9_AddShuJu &H006400,"深绿色","&H006400"
  F9_AddShuJu &H008000,"绿色","&H008000"
  F9_AddShuJu &H808000,"蒂尔","&H808000"
  F9_AddShuJu &H8B8B00,"深青绿","&H8B8B00"
  F9_AddShuJu &HFFBF00,"深天蓝","&HFFBF00"
  F9_AddShuJu &HD1CE00,"深粉蓝","&HD1CE00"
  F9_AddShuJu &H9AFA00,"嫩绿","&H9AFA00"
  F9_AddShuJu &H00FF00,"青柠","&H00FF00"
  F9_AddShuJu &H7FFF00,"嫩绿","&H7FFF00"
  F9_AddShuJu &HFFFF00,"水族","&HFFFF00"
  F9_AddShuJu &HFFFF00,"青色","&HFFFF00"
  F9_AddShuJu &H701919,"黑蓝","&H701919"
  F9_AddShuJu &HFF901E,"宝蓝","&HFF901E"
  F9_AddShuJu &HAAB220,"海藻绿","&HAAB220"
  F9_AddShuJu &H228B22,"森林绿","&H228B22"
  F9_AddShuJu &H578B2E,"海绿色","&H578B2E"
  F9_AddShuJu &H4F4F2F,"深青灰","&H4F4F2F"
  F9_AddShuJu &H32CD32,"暗绿","&H32CD32"
  F9_AddShuJu &H71B33C,"暗海藻","&H71B33C"
  F9_AddShuJu &HD0E040,"绿松石","&HD0E040"
  F9_AddShuJu &HE16941,"宝蓝色","&HE16941"
  F9_AddShuJu &HB48246,"钢青","&HB48246"
  F9_AddShuJu &H8B3D48,"深青蓝","&H8B3D48"
  F9_AddShuJu &HCCD148,"粉蓝","&HCCD148"
  F9_AddShuJu &H82004B,"靛青","&H82004B"
  F9_AddShuJu &H2F6B55,"深橄榄绿","&H2F6B55"
  F9_AddShuJu &HA09E5F,"藏青","&HA09E5F"
  F9_AddShuJu &HED9564,"矢菊花","&HED9564"
  F9_AddShuJu &H993366,"丽贝卡紫","&H993366"
  F9_AddShuJu &HAACD66,"草绿","&HAACD66"
  F9_AddShuJu &H696969,"暗灰","&H696969"
  F9_AddShuJu &HCD5A6A,"青蓝","&HCD5A6A"
  F9_AddShuJu &H238E6B,"淡绿褐","&H238E6B"
  F9_AddShuJu &H908070,"青灰","&H908070"
  F9_AddShuJu &H998877,"青灰","&H998877"
  F9_AddShuJu &HEE687B,"青蓝","&HEE687B"
  F9_AddShuJu &H00FC7C,"草绿","&H00FC7C"
  F9_AddShuJu &H00FF7F,"黄绿色","&H00FF7F"
  F9_AddShuJu &HD4FF7F,"蓝晶","&HD4FF7F"
  F9_AddShuJu &H000080,"栗色","&H000080"
  F9_AddShuJu &H800080,"紫色","&H800080"
  F9_AddShuJu &H008080,"橄榄","&H008080"
  F9_AddShuJu &H808080,"灰色","&H808080"
  F9_AddShuJu &H808080,"灰色","&H808080"
  F9_AddShuJu &HEBCE87,"天蓝色","&HEBCE87"
  F9_AddShuJu &HFACE87,"天蓝","&HFACE87"
  F9_AddShuJu &HE22B8A,"紫罗兰色","&HE22B8A"
  F9_AddShuJu &H00008B,"深红","&H00008B"
  F9_AddShuJu &H8B008B,"深洋红","&H8B008B"
  F9_AddShuJu &H13458B,"马鞍棕色","&H13458B"
  F9_AddShuJu &H8FBC8F,"海藻绿","&H8FBC8F"
  F9_AddShuJu &H90EE90,"浅绿","&H90EE90"
  F9_AddShuJu &HDB7093,"暗紫","&HDB7093"
  F9_AddShuJu &HD30094,"深紫","&HD30094"
  F9_AddShuJu &H98FB98,"浅绿色","&H98FB98"
  F9_AddShuJu &HCC3299,"深兰花紫","&HCC3299"
  F9_AddShuJu &H32CD9A,"黄绿色","&H32CD9A"
  F9_AddShuJu &H2D52A0,"赭色","&H2D52A0"
  F9_AddShuJu &H2A2AA5,"棕色","&H2A2AA5"
  F9_AddShuJu &HA9A9A9,"深灰色","&HA9A9A9"
  F9_AddShuJu &HA9A9A9,"深灰色","&HA9A9A9"
  F9_AddShuJu &HE6D8AD,"浅蓝","&HE6D8AD"
  F9_AddShuJu &H2FFFAD,"黄绿色","&H2FFFAD"
  F9_AddShuJu &HEEEEAF,"淡蓝绿","&HEEEEAF"
  F9_AddShuJu &HDEC4B0,"浅钢","&HDEC4B0"
  F9_AddShuJu &HE6E0B0,"粉蓝色","&HE6E0B0"
  F9_AddShuJu &H2222B2,"耐火砖","&H2222B2"
  F9_AddShuJu &H0B86B8,"深金黄","&H0B86B8"
  F9_AddShuJu &HD355BA,"暗兰花","&HD355BA"
  F9_AddShuJu &H8F8FBC,"玫瑰褐","&H8F8FBC"
  F9_AddShuJu &H6BB7BD,"深卡其色","&H6BB7BD"
  F9_AddShuJu &HC0C0C0,"银","&HC0C0C0"
  F9_AddShuJu &H8515C7,"暗紫","&H8515C7"
  F9_AddShuJu &H5C5CCD,"印地安","&H5C5CCD"
  F9_AddShuJu &H3F85CD,"秘鲁","&H3F85CD"
  F9_AddShuJu &H1E69D2,"巧克力","&H1E69D2"
  F9_AddShuJu &H8CB4D2,"黄褐色","&H8CB4D2"
  F9_AddShuJu &HD3D3D3,"浅灰","&HD3D3D3"
  F9_AddShuJu &HD3D3D3,"浅灰色","&HD3D3D3"
  F9_AddShuJu &HD8BFD8,"蓟","&HD8BFD8"
  F9_AddShuJu &HD670DA,"兰花","&HD670DA"
  F9_AddShuJu &H20A5DA,"黄花","&H20A5DA"
  F9_AddShuJu &H9370DB,"浅紫红","&H9370DB"
  F9_AddShuJu &H3C14DC,"赤红","&H3C14DC"
  F9_AddShuJu &HDCDCDC,"亮灰","&HDCDCDC"
  F9_AddShuJu &HDDA0DD,"李子","&HDDA0DD"
  F9_AddShuJu &H87B8DE,"原木色","&H87B8DE"
  F9_AddShuJu &HFFFFE0,"浅青绿","&HFFFFE0"
  F9_AddShuJu &HFAE6E6,"薰衣草","&HFAE6E6"
  F9_AddShuJu &H7A96E9,"深橙红","&H7A96E9"
  F9_AddShuJu &HEE82EE,"紫色","&HEE82EE"
  F9_AddShuJu &HAAE8EE,"淡金黄","&HAAE8EE"
  F9_AddShuJu &H8080F0,"浅珊瑚红","&H8080F0"
  F9_AddShuJu &H8CE6F0,"黄褐色","&H8CE6F0"
  F9_AddShuJu &HFFF8F0,"艾莉斯蓝","&HFFF8F0"
  F9_AddShuJu &HF0FFF0,"甘露","&HF0FFF0"
  F9_AddShuJu &HFFFFF0,"天蓝","&HFFFFF0"
  F9_AddShuJu &H60A4F4,"蒂布朗","&H60A4F4"
  F9_AddShuJu &HB3DEF5,"小麦","&HB3DEF5"
  F9_AddShuJu &HDCF5F5,"米色","&HDCF5F5"
  F9_AddShuJu &HF5F5F5,"白色的烟","&HF5F5F5"
  F9_AddShuJu &HFAFFF5,"薄荷乳白","&HFAFFF5"
  F9_AddShuJu &HFFF8F8,"鬼白色","&HFFF8F8"
  F9_AddShuJu &H7280FA,"三文鱼","&H7280FA"
  F9_AddShuJu &HD7EBFA,"古董白","&HD7EBFA"
  F9_AddShuJu &HE6F0FA,"麻布","&HE6F0FA"
  F9_AddShuJu &HD2FAFA,"浅金黄","&HD2FAFA"
  F9_AddShuJu &HE6F5FD,"旧布黄","&HE6F5FD"
  F9_AddShuJu &H0000FF,"红","&H0000FF"
  F9_AddShuJu &HFF00FF,"紫红色","&HFF00FF"
  F9_AddShuJu &HFF00FF,"品红","&HFF00FF"
  F9_AddShuJu &H9314FF,"深粉红","&H9314FF"
  F9_AddShuJu &H0045FF,"橙红色","&H0045FF"
  F9_AddShuJu &H4763FF,"番茄","&H4763FF"
  F9_AddShuJu &HB469FF,"亮粉色","&HB469FF"
  F9_AddShuJu &H507FFF,"珊瑚","&H507FFF"
  F9_AddShuJu &H008CFF,"深橙","&H008CFF"
  F9_AddShuJu &H7AA0FF,"橙红","&H7AA0FF"
  F9_AddShuJu &H00A5FF,"橙子","&H00A5FF"
  F9_AddShuJu &HC1B6FF,"浅粉红","&HC1B6FF"
  F9_AddShuJu &HCBC0FF,"粉","&HCBC0FF"
  F9_AddShuJu &H00D7FF,"金","&H00D7FF"
  F9_AddShuJu &HB9DAFF,"粉桃红","&HB9DAFF"
  F9_AddShuJu &HADDEFF,"印地安","&HADDEFF"
  F9_AddShuJu &HB5E4FF,"莫卡辛","&HB5E4FF"
  F9_AddShuJu &HC4E4FF,"浓汤","&HC4E4FF"
  F9_AddShuJu &HE1E4FF,"粉玫瑰红","&HE1E4FF"
  F9_AddShuJu &HCDEBFF,"杏仁白","&HCDEBFF"
  F9_AddShuJu &HD5EFFF,"粉木瓜橙","&HD5EFFF"
  F9_AddShuJu &HF5F0FF,"淡紫","&HF5F0FF"
  F9_AddShuJu &HEEF5FF,"贝壳","&HEEF5FF"
  F9_AddShuJu &HDCF8FF,"玉米穗黄","&HDCF8FF"
  F9_AddShuJu &HCDFAFF,"粉黄","&HCDFAFF"
  F9_AddShuJu &HF0FAFF,"花白","&HF0FAFF"
  F9_AddShuJu &HFAFAFF,"雪","&HFAFAFF"
  F9_AddShuJu &H00FFFF,"黄色","&H00FFFF"
  F9_AddShuJu &HE0FFFF,"浅黄色","&HE0FFFF"
  F9_AddShuJu &HF0FFFF,"象牙","&HF0FFFF"
  F9_AddShuJu &HFFFFFF,"白色","&HFFFFFF"
  
  
End Sub

