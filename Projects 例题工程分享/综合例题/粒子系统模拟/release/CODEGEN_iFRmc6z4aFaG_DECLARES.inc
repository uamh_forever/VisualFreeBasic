'-----------------------------------------------------------------------------
' 由 VisualFreeBasic 5.6.2 生成的源代码
' 生成时间：2021年07月11日 22时33分01秒
' 更多信息请访问 www.yfvb.com 
'-----------------------------------------------------------------------------

' 这里是全局定义源码，所有type、全局变量、函数定义 都在此处。
#include Once "Afx/CGdiPlus/CGdiPlus.inc"
#include Once "afx\CWindow.inc"
Type FormControlsPro_TYPE '保存控件的私有属性，每个控件都不同，全部在此，各取所需。
   hWndParent     As HWND       '父窗口句柄
   nName          As String     '名称\1\用来代码中识别对象的名称\Command\
   Index          As Long       '控件数组索引，小于零表示非控件数组
   IDC            As Long       '
   CtrlFocus      As HWND       ' 当前焦点在什么控件
   BigIcon        As HICON      '窗口大图标，主要用来主窗口
   SmallIcon      As HICON      '窗口小图标
   nText          as CWSTR      '
   ControlType    as Long       '控件类型，为了画控件  >=100 为虚拟控件  100=LABEL   TEXT=1 Button=2
   nData          As String     '编写控件使用，数据存放，无特定，控件根据自己需要存放任意数据。
   CtlData(99)    As Integer    '为每个控件提供 100 个数据储存(编写控件使用，控件根据自己需要存放任意数据)。
   UserData(99)   As Integer    '为每个控件提供 100 个数据储存(用户使用)。
   Style          as UInteger   '样式，每个控件都有自己的定义
   TransPer       As Long       '透明度\0\窗口透明度，百分比(0--100)，0%不透明 100%全透明\0\
   TransColor     As Long = &H197F7F7F  '透明颜色\3\透明颜色，需要用 GetCodeColorGDI 或 GetCodeColorGDIplue 转为 GDI 和 GDI+ 颜色值
   MousePointer   As Long               '指针\2\鼠标在窗口上的形状\0 - 默认,1 - 后台运行,2 - 标准箭头,3 - 十字光标,4 - 箭头和问号,5 - 文本工字光标,6 - 不可用禁止圈,7 - 移动,8 - 双箭头↙↗,9 - 双箭头↑↓,10 - 双箭头向↖↘,11 - 双箭头←→,12 - 垂直箭头,13 - 沙漏,14 - 手型
   ForeColor      As Long = &H197F7F7F  '文字色 需要用 GetCodeColorGDI 或 GetCodeColorGDIplue 转为 GDI 和 GDI+ 颜色值
   BackColor      As Long = &H197F7F7F  '背景色 需要用 GetCodeColorGDI 或 GetCodeColorGDIplue 转为 GDI 和 GDI+ 颜色值
   hBackBrush     As HBRUSH             '背景刷子，返回给窗口和控件用
   nFont          As String                   '字体\4\用于此对象的文本字体。\微软雅黑,9,0\
   Tag            As CWSTR                    ' 附加 \ 1 \ 私有自定义文本与控件关联。 \ \
   ToolTip        As CWSTR                    ' 提示 \ 1 \ 一个提示，当鼠标光标悬停在控件时显示它。 \ \
   ToolTipBalloon As Long                     ' 气球样式 \ 2 \ 一个气球样式显示工具提示。 \ False \ True, False
   ToolWnd        As HWND                     '提示窗口句柄，用来销毁
   nCursor        As HCURSOR                  '鼠标指针句柄
   nLeft          As Long                     '返回/设置相对于父窗口的 X 响应DPI数值，为 100%DPI数值（像素）
   nTop           As Long                     '返回/设置相对于父窗口的 Y 响应DPI数值，为 100%DPI数值（像素）
   nWidth         As Long                     '返回/设置控件宽度 响应DPI数值，为 100%DPI数值（像素）
   nHeight        As Long                     '返回/设置控件高度 响应DPI数值，为 100%DPI数值（像素））
   anchor         AS LONG                     '控件布局 自动调整方式
   nRight         As Long                     '控件布局 用，相对于窗口右边距离 响应DPI数值，为 100%DPI数值（像素））
   nBottom        As Long                     '控件布局 用，相对于窗口底边距离 响应DPI数值，为 100%DPI数值（像素））
   centerX        AS LONG                     '控件布局 用 中点  响应DPI数值，为 100%DPI数值（像素）
   centerY        AS LONG                     '控件布局 用
   VrControls     As FormControlsPro_TYPE ptr '如果是主窗口带 虚拟控件 ，就是下一个虚拟控件链表指针，直到为 0 表示没了
End Type




Declare Function gFLY_GetFontHandles(mFont As String) As HFONT
Declare Function GetCodeColorGDI(coColor As Long,defaultColor As Long =-1) As Long
Declare Function GetCodeColorGDIplue(coColor As Long,defaultColor As Long =0) As Long
Declare Function FF_AddTooltip(hWndForm AS HWND, strTooltipText AS wString, bBalloon AS Long, X as Long = 0, Y As Long = 0, W As Long = 0, H As Long = 0) As HWND
Declare Sub FLY_VFB_Layout_hWndForm(hWndForm As HWND) 
Declare Sub FLY_VFB_Layout_Handle(fp As FormControlsPro_TYPE ptr, pWidth AS LONG, pHeight AS LONG, nWidth AS LONG, nHeight AS LONG, ByRef x AS LONG, ByRef y AS LONG, ByRef xWidth AS LONG, ByRef yHeight AS LONG)
#include Once "yGDI.inc"
#include Once "Form\ClsForm.inc"
#include Once "ClsControl.inc"
#include Once "ClsVirtualCl.inc"

#include Once "Picture\ClsPicture.inc"

#include Once "Timer\ClsTimer.inc"


Type Form1_Class_Form Extends Class_Form
    Picture1 As Class_Picture
    Timer1 As Class_Timer
   Declare Function Show(hWndParent As .hWnd =HWND_DESKTOP,Modal As Boolean = False ,UserData As Integer = 0) As .hWnd '加载窗口并且显示, 模态显示用True {2.True.False} UserData 是传递给创建事件的参数，不保存数值
End Type
Type P_t '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|1]
X As Single '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|2]
Y As Single '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|3]
XV As Single '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|4]
YV As Single '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|5]
 '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|6]
R As Single '半径 '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|7]
M As Single '质量 '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|8]
Res As Single '反弹性 '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|9]
End Type '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|10]
Dim Shared gFLY_FontNames() As String ,gFLY_FontHandles() As HFONT
Dim Shared Form1 As Form1_Class_Form
Dim Shared Points(200) As P_t '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|12]
Const Grav As Double = 98 '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|13]
Const WallResist As Double = 0.5 '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|14]
Const PointResist As Double = 1000 '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|15]
Dim Shared SceneWidth As Single, SceneHeight As Single, DeltaTime As Double '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|16]
Dim Shared MouX As Single, MouY As Single, MouB As Boolean '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|18]
Dim Shared MouC As Long '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|19]
Const PI As Double = 3.14159265358979 '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|21]
Dim Shared Keys(255) As Boolean '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|23]
Declare Sub FF_DoEvents() '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\release\CODEGEN_iFRmc6z4aFaG_UTILITY.inc|351]
Declare Function FF_Control_GetSize( ByVal hWndControl as HWnd, _ '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\release\CODEGEN_iFRmc6z4aFaG_UTILITY.inc|372]
ByRef nWidth as Long, _ '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\release\CODEGEN_iFRmc6z4aFaG_UTILITY.inc|373]
ByRef nHeight as long _ '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\release\CODEGEN_iFRmc6z4aFaG_UTILITY.inc|374]
) as Integer '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\release\CODEGEN_iFRmc6z4aFaG_UTILITY.inc|375]
Declare Sub DrawEllipse(ByVal hDC as hDC, ByVal X as Integer, ByVal Y as Integer, ByVal hWidth as Integer, ByVal hHeight as Integer, _ '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\release\CODEGEN_iFRmc6z4aFaG_UTILITY.inc|409]
ByVal LineType as Integer ,ByVal LineWidth as Integer, ByVal bColor as  Integer , ByVal tColor as  Integer ) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\release\CODEGEN_iFRmc6z4aFaG_UTILITY.inc|410]
Declare Function FLY_DoMessagePump(pWindow AS CWindow Ptr,ByVal ShowModalFlag As Long, ByVal hWndForm As HWND, ByVal hWndParent As HWND, ByVal nFormShowState As Long, ByVal IsMDIForm As Long) As HWND
Declare Function Form1_FORMPROCEDURE(ByVal hWndForm As HWND, ByVal wMsg As uInteger, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As LResult '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|-300]
Declare Function Form1_CODEPROCEDURE(ByVal hWndControl As HWND, ByVal wMsg As uInteger, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As LRESULT '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|-78]
Declare Sub InitPoints(ByVal XL As Double, YL As Double, WL As Double, HL As Double) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|25]
Declare Sub Movement(ByVal DT As Double) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|48]
Declare Function CH_E5B9B3E696B9_(ByVal nVal As Double) As Double '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|128]
Declare Function CH_E5B084E7BABFE4B88EE59C86E79A84E79BB8E4BAA4E782B9_(ByVal CH_E8B5B7E782B9_X As Double, ByVal CH_E8B5B7E782B9_Y As Double, ByVal CH_E696B9E59091_X As Double, ByVal CH_E696B9E59091_Y As Double, ByVal CH_E8B79DE7A6BBE99990E588B6_ As Double, ByVal CH_E59C86E5BF83_X As Double, ByVal CH_E59C86E5BF83_Y As Double, ByVal CH_E58D8AE5BE84E5B9B3E696B9_ As Double, CH_E4BAA4E782B9_X As Double, CH_E4BAA4E782B9_Y As Double) As Boolean '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|133]
Declare Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|176]
Declare Sub Moni(aa As Long ) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|182]
Declare Sub Form1_WM_Size(hWndForm As hWnd, fwSizeType As Long, nWidth As Long, nHeight As Long) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|211]
Declare Sub Form1_Picture1_WM_MouseMove(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|221]
Declare Sub Form1_Picture1_WM_LButtonUp(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|230]
Declare Sub Form1_Picture1_WM_LButtonDown(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|239]
Declare Function Form1_WM_Close(hWndForm As hWnd) As LResult '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|257]
Declare Function Form1_Picture1_WM_Paint(hWndForm As hWnd, hWndControl As hWnd) As LResult '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|265]
Declare Sub Form1_Timer1_WM_Timer(hWndForm As hWnd, wTimerID As Long) '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|316]
Declare Function FF_PUMPHOOK( uMsg As Msg ) As Long '[FILE:#FF_PumpHook#|0]
Declare Function FF_WINMAIN(ByVal hInstance As HINSTANCE) As Long '[FILE:#FF_WinMain#|0]
Declare Sub FF_WINEND(ByVal hInstance As HINSTANCE) '[FILE:#FF_WinMain#|15]
