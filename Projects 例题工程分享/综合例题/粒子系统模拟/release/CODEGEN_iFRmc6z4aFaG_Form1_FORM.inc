'[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|412]
'-----------------------------------------------------------------------------
' 由 VisualFreeBasic 5.6.2 生成的源代码
' 生成时间：2021年07月11日 22时33分01秒
' 更多信息请访问 www.yfvb.com 
'-----------------------------------------------------------------------------

'------------------------------------------------------------------------------
'VisualFreeBasic 自动生成的 Form1 函数
'------------------------------------------------------------------------------
'{FORM_WS_EX_NOACTIVATE3}  '为无焦点窗口创建定义
Function Form1_Class_Form.Show(hWndParent As .hWnd = HWND_DESKTOP, Modal As Boolean = False, UserData As Integer = 0) As .hWnd  '加载窗口并且显示, 模态显示用True {2.True.False} UserData 是传递给创建事件的参数，不保存数值
   '注：hWnd 是类属性，与API的 hWnd 冲突，因此使用 .hWnd 表示API，就是前面加个小数点。
   If This.Repeat = False Then     '是不是不允许多开窗口
      If IsWindow(This.hWnd) Then 
         This.FlashWindow
         Return This.hWnd
      End if 
   End if 
'{FORM_WS_EX_NOACTIVATE}  '为无焦点窗口创建多线程代码
   Dim pWindow AS CWindow Ptr = New CWindow("粒子系统模拟_Form1")
   '创建一个窗口
   hWnd = pWindow->Create(hWndParent, "粒子系统模拟", Cast(WNDPROC, @Form1_FORMPROCEDURE), 0, 0, 705, 613, WS_POPUP Or WS_THICKFRAME Or WS_CAPTION Or WS_SYSMENU Or WS_MINIMIZEBOX Or WS_MAXIMIZEBOX Or WS_CLIPSIBLINGS Or WS_CLIPCHILDREN, WS_EX_WINDOWEDGE Or WS_EX_LEFT Or WS_EX_LTRREADING Or WS_EX_RIGHTSCROLLBAR Or WS_EX_CONTROLPARENT)
'{FORM_WS_EX_NOACTIVATE2}  '为无焦点窗口返回窗口句柄
   If IsWindow(hWnd) = 0 Then
      Delete pWindow
      Return 0
   End if
   Dim fp As FormControlsPro_TYPE ptr = new FormControlsPro_TYPE
   SetPropW(hWnd, "FCP_TYPE_PTR", Cast(Any Ptr, fp))
   vfb_Set_Control_Ptr(hWnd,fp)
   DIm rcParent AS RECT  '为控件布局属性，初始化数据用
   .GetClientRect(hWnd, @rcParent)
   rcParent.Right = AfxUnscaleX(rcParent.Right)  ' 为自动响应DPI，全部调整为 100%DPI 时的数值
   rcParent.Bottom = AfxUnscaleX(rcParent.Bottom) 
   fp->hWndParent = hWndParent
   fp->nName = "Form1"
   
   '给窗口设置其它参数
   pWindow->ClassStyle = CS_VREDRAW Or  CS_HREDRAW Or  CS_DBLCLKS
   '图标
   fp->BigIcon = LoadImage(app.hInstance, "AAAAA_APPICON", IMAGE_ICON, 48, 48, LR_SHARED)
   fp->SmallIcon = LoadImage(app.hInstance, "AAAAA_APPICON", IMAGE_ICON, 32, 32, LR_SHARED)
   pWindow->BigIcon = fp->BigIcon     '需要在销毁窗口时销毁句柄
   pWindow->SmallIcon = fp->SmallIcon
   '菜单
   '启动位置
   pWindow->Center
   '鼠标穿透 透明度 透明颜色
   
   
   This.TransColor = GetCodeColorGDI(&H197F7F7F)
   fp->TransColor = &H197F7F7F
   This.MousePointer = 0
   This.BackColor = GetCodeColorGDI(&H0F7F7F7F)
SetClassLongPtr hWnd, GCLP_HBRBACKGROUND, Cast(Integer, CreateSolidBrush(fp->BackColor))
   fp->BackColor = &H0F7F7F7F
   
   
         
'[Create main window]  创建主窗口，在此前一行插入。
    Dim hWndControl As .hWnd '控件句柄
    
'[Create control top]  顶部创建控件的代码，（主要为了顶部菜单，必须在所有控件之前创建）在此前一行插入。
        
   hWndControl = pWindow->AddControl("LABEL", hWnd, 102, "", 41, 31, 245, 207,WS_CHILD Or SS_NOTIFY Or WS_CLIPCHILDREN Or WS_CLIPSIBLINGS Or WS_VISIBLE Or WS_TABSTOP ,0 , , Cast(Any Ptr, @Form1_CODEPROCEDURE))
   If hWndControl Then 
      Dim fp As FormControlsPro_TYPE ptr = new FormControlsPro_TYPE
      vfb_Set_Control_Ptr(hWndControl,fp)
      fp->hWndParent = hWnd
      fp->Index = -1
      fp->IDC = 102
      fp->nText = ""
      This.Picture1.hWnd = hWndControl 
      This.Picture1.IDC =102
      fp->nName = "Picture1"
      fp->nLeft = 41
      fp->nTop = 31
      fp->nWidth = 245
      fp->nHeight = 207
   End IF

   fp->VrControls = new FormControlsPro_TYPE '创建虚拟控件链表
   fp = fp->VrControls
   If fp Then 
      This.Timer1.hWndForm = hWnd 
      fp->hWndParent = hWnd
      fp->Index = -1
      fp->IDC = 103
      fp->nText = ""
      This.Timer1.IDC =103
      fp->nName = "Timer1"
      This.Timer1.Interval = 100
      fp->Style Or= &H1 
      This.Timer1.Enabled = True
      fp->nLeft = 107
      fp->nTop = 364
   End IF

'[Create control]  创建控件的代码，在此前一行插入。

'[Create control end]  创建全部控件后需要处理的代码，在此前一行插入。
 FORM1_WM_CREATE(hWnd ,UserData )  '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|176]

   PostMessage(hWnd, &H1000, &H1000,CAst(lPArAm,UserData)) '触发显示事件
   
   hWndControl = hWnd '设置焦点后会发生消息循环的情况，要是同时多开同一个窗口，就发生 hWnd 被赋值为下一个窗口句柄的情况。
   This.Picture1.SetFocus
'[SET_FOCUS] '将焦点设置为选项卡顺序中的最低控件 
   Function = FLY_DoMessagePump(pWindow,Modal, hWndControl, hWndParent, SW_SHOWNORMAL, False) ' IsMDIForm 如果这是MDI窗口，请设置标志
End Function

Function Form1_FORMPROCEDURE(ByVal hWndForm As HWND, ByVal wMsg As uInteger, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As LResult 'Form1 窗口回调过程
   Dim tLResult As LResult
   Select Case wMsg
      Case WM_SIZE
         If wParam <> SIZE_MINIMIZED Then
'[CONTROL_WM_SIZE] 状态栏调整大小
            FLY_VFB_Layout_hWndForm(hWndForm) '控件布局调整大小
'[CONTROL_WM_SIZE_END] 状态栏调整大小
         End If
      Case WM_PAINT '处理虚拟控件
         Dim fp As FormControlsPro_TYPE ptr = GetPropW(hWndForm ,"FCP_TYPE_PTR")
         if fp=0 Then fp = vfb_Get_Control_Ptr(hWndForm)
         if fp Then
            Dim WinCc As Long = GetCodeColorGDI(fp->BackColor)
            If WinCc = -1 Then WinCc = GetSysColor(COLOR_BTNFACE)
            Dim gg As yGDI = yGDI(hWndForm, WinCc, True)

'[FORM_PAINT_START]  主窗口开始画画事件，在此前一行插入。
'[DRAWINGVIRTUALCONTROLS] '描绘虚拟控件，在此下一行插入（这样就最底层先画）。

'[FORM_PAINT_END] 主窗口开始画画事件，在此前一行插入。  
         end if
   End Select
   '以下CASE从表单上的控件以及用户自己处理的任何这些消息中调用通知事件。
   Select Case wMsg
      Case &H1000
         if wParam = &H1000 then
            Dim rc as Rect   '由于控件创建时间比尺寸消息晚，需要重新触发一次
            GetClientRect(hWndForm, @rc)
            SendMessage(hWndForm, WM_SIZE, SIZE_RESTORED, MAKELPARAM(rc.Right - rc.Left, rc.Bottom - rc.Top))

'[FORM_SHOWN]  主窗口第一次显示事件，在此前一行插入。
            InvalidateRect(hWndForm, Null, True)
            UpdateWindow(hWndForm)
'[Create window shadow]  创建窗口阴影，在此前一行插入。              
            Return 0
         End If
      Case WM_COMMAND

'[FORM_WM_COMMAND]  主窗口命令处理事件，在此前一行插入。
'{CONTROL_WM_COMMAND}  '控件处理初始化代码       
'[CONTROL_WM_COMMAND]  控件处理事件，在此前一行插入。
      Case WM_HSCROLL

'[FORM_WM_HSCROLL]  主窗口和一些特殊控件要的事件，在此前一行插入。 
      Case WM_VSCROLL

'[FORM_WM_VSCROLL]  主窗口和一些特殊控件要的事件，在此前一行插入。 
         
Case WM_CLOSE
         tLResult = FORM1_WM_CLOSE(hWndForm )
           If tLResult Then Return tLResult  '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|257]
Case WM_SIZE
          FORM1_WM_SIZE(hWndForm, Cast(Long, wParam), LoWord(lParam), HiWord(lParam))  '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|211]














'[Other events in main window]  主窗口其它事件，需要加 Case ，在此前一行插入。 注意每个消息值不可重复，不然后面的无法执行
'{FORM_WM_DRAWITEM}
'[FORM_WM_DRAWITEM]           
      Case WM_TIMER
          If wParam = 103 Then
             Form1.Timer1.hWndForm = hWndForm 
             KillTimer(hWndForm, 103)
             FORM1_TIMER1_WM_TIMER(hWndForm, Cast(Long, wParam))  '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|316]
             Form1.Timer1.hWndForm = hWndForm 
             If Form1.Timer1.Enabled = True Then  SetTimer(hWndForm, 103, Form1.Timer1.Interval(), NULL) 
          End if 

'[FORM_WM_TIMER]  时间控件事件
      Case WM_NOTIFY
         Dim FLY_pNotify As NMHDR Ptr = Cast(NMHDR Ptr, lParam)
'[CONTROLS_NOTIFY]  控件事件，标准控件的事件都是按照这个传递

'[FORM_WM_NOTIFY]   主窗口的事件
   End Select

'[VIRTUAL_CONTROL_EVENTS] 处理虚拟控件事件，在此前一行插入。 


'[CALL_FORM_CUSTOM] 处理任何自定义消息。

   '以下CASE处理VisualFreeBasic内部要求
   Select Case wMsg
      Case WM_GETMINMAXINFO
         '不要为MDI子窗体处理此消息，因为它会干扰子窗体的最大化。
         If (GetWindowLongPtr(hWndForm, GWL_EXSTYLE) And WS_EX_MDICHILD) <> WS_EX_MDICHILD Then
            DefWindowProcW(hWndForm, wMsg, wParam, lParam)
            Dim FLY_pMinMaxInfo As MINMAXINFO Ptr = Cast(MINMAXINFO Ptr, lParam)
            
            
            
            
            Return 0
         End If 
      Case WM_SYSCOMMAND
         If (wParam And &HFFF0) = SC_CLOSE Then
            SendMessage hWndForm, WM_CLOSE, wParam, lParam
            Exit Function
         End If
      Case WM_SETFOCUS
         '将焦点设置回正确的子控件。
         Dim fp As FormControlsPro_TYPE ptr = GetPropW(hWndForm ,"FCP_TYPE_PTR")
         if fp=0 Then fp = vfb_Get_Control_Ptr(hWndForm)
         If fp then 
            If IsWindow(fp->CtrlFocus) Then '预防控件已经关闭或没控件
               If GetAncestor(fp->CtrlFocus,GA_ROOT)=hWndForm Then '预防控件被设置其它主窗口了
                  SetFocus fp->CtrlFocus         
               End If 
            End If 
         End If 
      Case WM_SYSCOLORCHANGE, WM_THEMECHANGED '当对系统颜色设置进行更改, 在主题更改事件之后广播到每个窗口。
         '为窗体上的控件重新创建任何背景画笔。
         '将此消息转发给任何通用控件，因为它们不会自动接收此消息。
         Dim fp As FormControlsPro_TYPE ptr = GetPropW(hWndForm ,"FCP_TYPE_PTR")
         if fp=0 Then fp = vfb_Get_Control_Ptr(hWndForm)
         If fp then
            Dim cc as Long = GetCodeColorGDI(fp->BackColor)
            if cc = -1 then cc = GetSysColor(COLOR_BTNFACE)
            If fp->hBackBrush Then DeleteBrush(fp->hBackBrush)
            fp->hBackBrush = CreateSolidBrush(cc)
            dim zhWnd as HWND = GetWindow(hWndForm, GW_CHILD)
            While zhWnd
               dim zfp As FormControlsPro_TYPE ptr = GetPropW(zhWnd ,"FCP_TYPE_PTR")
               if fp=0 Then fp = vfb_Get_Control_Ptr(zhWnd)
               if zfp Then 
                  Dim zcc as Long = GetCodeColorGDI(zfp->BackColor)
                  if zcc = -1 then zcc = cc
                  If zfp->hBackBrush Then DeleteBrush(zfp->hBackBrush)
                  zfp->hBackBrush = CreateSolidBrush(zcc)
               End if 
               zhWnd = GetWindow(zhWnd, GW_HWNDNEXT)
            Wend 
         End If
         
      Case WM_SETCURSOR
         dim a as long = LOWORD(lParam)
         if a = 1 then '位于客户区
            Dim fp As FormControlsPro_TYPE ptr = GetPropW(cast(hwnd ,wParam) ,"FCP_TYPE_PTR")
            if fp=0 Then fp = vfb_Get_Control_Ptr(cast(hwnd ,wParam))
            if fp Then
               if fp->nCursor then
                  SetCursor(fp->nCursor)
                  Return True
               end if
            end if
         end if
      Case WM_PAINT
         Return True  '采用自绘，需要立即返回
      Case WM_ERASEBKGND ' 采用自绘，不需要系统画背景
         Return True   '防止擦除背景，不加这个会闪的。 与mCrtl控件冲突，加了后，mCrtl控件有透明的就不会画了。去掉这行，对窗口也无影响。
      Case WM_MOUSEMOVE
         

'[CALL_FORM_WM_MOUSEMOVE]  
      Case WM_DESTROY

'[CALL_FORM_WM_DESTROY]  窗口卸载事件
         Form1.hWndForm = hWndForm 
         Form1.Destructor 
            Form1.Picture1.hWndForm = hWndForm
            Form1.Picture1.Destructor
            Form1.Timer1.hWndForm = hWndForm
            Form1.Timer1.Destructor
'[CALL_WM_DESTROY] 通知每个控件类（包括虚拟控件），做必要的卸载工作，因为窗口类是全局的，不会因为窗口销毁而销毁。  

         KillTimer hWndForm, 103
'[DELETE_USER_TIMERS] 删除任何用户定义的Timer控件
         
         Form1.hWnd = 0
         Dim pWindow AS CWindow Ptr = AfxCWindowPtr(hWndForm)
         If pWindow Then Delete pWindow
         Dim fp As FormControlsPro_TYPE ptr = GetPropW(hWndForm ,"FCP_TYPE_PTR")
         if fp=0 Then fp = vfb_Get_Control_Ptr(hWndForm)
         While fp   '清理虚拟控件
            if fp->BigIcon then DestroyIcon fp->BigIcon
            if fp->SmallIcon then DestroyIcon fp->SmallIcon
            if fp->hBackBrush then DeleteBrush(fp->hBackBrush)
            If fp->ToolWnd Then DestroyWindow fp->ToolWnd
            if fp->nCursor then DestroyCursor fp->nCursor    
            Dim sfp As FormControlsPro_TYPE ptr = fp->VrControls
            Delete fp
            fp = sfp
         Wend 
         RemovePropW hWndForm, "FCP_TYPE_PTR"
         vfb_Remove_Control_Ptr(hWndForm)
      Case WM_CTLCOLOREDIT, WM_CTLCOLORLISTBOX, WM_CTLCOLORSTATIC, WM_CTLCOLORBTN
         '从子控件收到的消息即将显示。在此处设置颜色属性。lParam是Control的句柄。 wParam是hDC。
         Dim fp As FormControlsPro_TYPE ptr = GetPropW(Cast(HWND ,lParam) ,"FCP_TYPE_PTR")
         if fp=0 Then fp = vfb_Get_Control_Ptr(Cast(HWND ,lParam))
         If fp then
            Dim cc as Long = GetCodeColorGDI(fp->ForeColor)
            if cc <> -1 then SetTextColor(Cast(hDC, wParam), cc)
            cc = GetCodeColorGDI(fp->BackColor)
            if cc = -1 Then
               Dim wfp As FormControlsPro_TYPE ptr = GetPropW(hWndForm ,"FCP_TYPE_PTR")
               if wfp=0 Then wfp = vfb_Get_Control_Ptr(hWndForm)
               if wfp = 0 then exit select
               cc = GetCodeColorGDI(wfp->BackColor)
            End If
            if cc = -1 then cc = GetSysColor(COLOR_BTNFACE)
            SetBkColor(Cast(hDC, wParam), cc)
            If fp->hBackBrush Then DeleteBrush(fp->hBackBrush)
            fp->hBackBrush = CreateSolidBrush(cc)
'            SetBkMode Cast(hDC, wParam), TRANSPARENT
            Return Cast(LRESULT, fp->hBackBrush)
         End If
   End Select
   Function = DefWindowProcW(hWndForm, wMsg, wParam, lParam)
End Function

Function Form1_CODEPROCEDURE(ByVal hWndControl As HWND, ByVal wMsg As uInteger, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As LRESULT  ' Form1 控件回调
   'Form1 窗口上每个控件的所有消息都在此函数中处理。
   Dim fp As FormControlsPro_TYPE ptr = GetPropW(hWndControl ,"FCP_TYPE_PTR")
   if fp=0 Then fp = vfb_Get_Control_Ptr(hWndControl)
   Dim tLResult As LResult
   Dim IDC As Long = GetDlgCtrlId(hWndControl)
   '以下CASE在处理用户定义事件之前处理内部VisualFreeBasic要求（这些事件在此之后的单独CASE中处理）。
   Select Case wMsg
      Case 0 

         
      Case WM_LBUTTONDOWN ''' 
          If IDC = 102 Then  ' Picture1
              FORM1_PICTURE1_WM_LBUTTONDOWN(GetParent(hWndControl), hWndControl, Cast(Long, wParam), GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam))  '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|239]
          End If

      Case WM_LBUTTONUP ''' 
          If IDC = 102 Then  ' Picture1
              FORM1_PICTURE1_WM_LBUTTONUP(GetParent(hWndControl), hWndControl, Cast(Long, wParam), GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam))  '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|230]
          End If

      Case WM_MOUSEMOVE ''' 
          If IDC = 102 Then  ' Picture1
              FORM1_PICTURE1_WM_MOUSEMOVE(GetParent(hWndControl), hWndControl, Cast(Long, wParam), GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam))  '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|221]
          End If

      Case WM_PAINT ''' 
          If IDC = 102 Then  ' Picture1
          tLResult =  FORM1_PICTURE1_WM_PAINT(GetParent(hWndControl), hWndControl)  '[FILE:D:\VisualFreeBasic5\Projects\综合例题\粒子系统模拟\forms\Form1.frm|265]
          If tLResult Then Return tLResult
          End If

'[CONTROL_CASE_OTHER]  '控件事件。         

   End Select
   
'[CONTROL_LEAVEHOVER]  '启用鼠标出窗口检查

'[CALL_CONTROL_CUSTOM]  处理任何自定义消息。

    Select Case wMsg
       Case WM_DESTROY  '// 需要：删除控件子类
          SetWindowLongPtrW hWndControl ,GWLP_WNDPROC ,CAST(LONG_PTR ,RemovePropW(hWndControl ,"OLDWNDPROC"))
            RemovePropW hWndControl ,"FCP_TYPE_PTR"  
            vfb_Remove_Control_Ptr(hWndControl)           
            If fp Then
               if fp->hBackBrush then DeleteBrush(fp->hBackBrush)
               If fp->ToolWnd Then DestroyWindow fp->ToolWnd
               if fp->nCursor then DestroyCursor fp->nCursor
               Delete fp
            End If
   
       Case WM_SETFOCUS
         ' 如果这是一个TextBox，我们检查是否需要突出显示文本。
'         If fp Then
'            If ff->SelText Then 
'               SendMessage hWndControl, EM_SETSEL, 0, -1
'            Else
'               SendMessage hWndControl, EM_SETSEL, -1, 0
'            End If   
'         End If
         ' 将焦点控件存储在父窗体中如果此窗体是TabControl子窗体，则需要将CtrlFocus存储在此子窗体的父窗体中。
         Dim FLY_tempLong As Long = GetWindowLongPtr(GetParent(hWndControl), GWL_STYLE)
         If (FLY_tempLong And WS_CHILD) = WS_CHILD Then
            ' 必须是TabControl子对话框
            fp = GetPropW(GetParent(GetParent(hWndControl)) ,"FCP_TYPE_PTR")
            if fp=0 Then fp = vfb_Get_Control_Ptr(GetParent(GetParent(hWndControl)))
         Else   
            fp = GetPropW(GetParent(hWndControl), "FCP_TYPE_PTR")
            if fp=0 Then fp = vfb_Get_Control_Ptr(GetParent(hWndControl))
         End If   
         If fp Then fp->CtrlFocus = hWndControl          
                
   End Select
   '此控件是子类，因此我们必须将所有未处理的消息发送到原始窗口过程。
   FUNCTION = CallWindowProcW(GetPropW(hWndControl, "OLDWNDPROC"), hWndControl, wMsg, wParam, lParam)
   
End Function

' Type P_t '已经搬到定义文件处
'     X As Single '已经搬到定义文件处
'     Y As Single '已经搬到定义文件处
'     XV As Single '已经搬到定义文件处
'     YV As Single '已经搬到定义文件处
'      '已经搬到定义文件处
'     R As Single '半径 '已经搬到定义文件处
'     M As Single '质量 '已经搬到定义文件处
'     Res As Single '反弹性 '已经搬到定义文件处
' End Type '已经搬到定义文件处

' Dim Shared Points(200) As P_t '已经搬到定义文件处
' Const Grav As Double = 98 '已经搬到定义文件处
' Const WallResist As Double = 0.5 '已经搬到定义文件处
' Const PointResist As Double = 1000 '已经搬到定义文件处
' Dim Shared SceneWidth As Single, SceneHeight As Single, DeltaTime As Double '已经搬到定义文件处

' Dim Shared MouX As Single, MouY As Single, MouB As Boolean '已经搬到定义文件处
' Dim Shared MouC As Long '已经搬到定义文件处

' Const PI As Double = 3.14159265358979 '已经搬到定义文件处

' Dim Shared Keys(255) As Boolean '已经搬到定义文件处
'--------------------------------------------------------------------------------
Sub InitPoints(ByVal XL As Double, YL As Double, WL As Double, HL As Double)
  Dim I As Long
  
  For I = 0 To UBound(Points) - 1
      With Points(I)
      .R = 8 + Rnd * 15
      .M = .R * .R * .R * PI * 4 / 3
      .X = XL + Rnd * WL
      .Y = YL + Rnd * HL
      .Res = PointResist
      End With
  Next
  
  With Points(I)
  .R = 40
  .M = .R * .R * .R * PI * 4 / 3
  .X = XL + Rnd * WL
  .Y = YL + Rnd * HL
  .Res = PointResist
  End With
  
End Sub
'--------------------------------------------------------------------------------
Sub Movement(ByVal DT As Double)
  Dim As Long  I, J
  
  For I = 0 To UBound(Points)
      With Points(I)
      .YV = .YV + Grav * DT
      .X = .X + .XV * DT
      .Y = .Y + .YV * DT
      
      If Keys(87) Then .YV = .YV - 200 * DT
      If Keys(83) Then .YV = .YV + 200 * DT
      If Keys(65) Then .XV = .XV - 200 * DT
      If Keys(68) Then .XV = .XV + 200 * DT
      
      If MouB And MouC = I Then
          Points(I).XV = (MouX - Points(I).X) / DT
          Points(I).YV = (MouY - Points(I).Y) / DT
          'Points(I).X = MouX
          'Points(I).Y = MouY
      End If
      End With
  Next
  
  For I = 0 To UBound(Points)
      For J = 0 To UBound(Points)
          If I <> J Then
              Dim Hit As Boolean
              Hit = False
              
              Dim Vel As Double '速率
              Vel = Sqr(Points(I).XV * Points(I).XV + Points(I).YV * Points(I).YV) '根据速度判断相交
              Dim Dist As Double, DifX As Double, DifY As Double 'I到J的向量
              DifX = Points(J).X - Points(I).X
              DifY = Points(J).Y - Points(I).Y
              Dist = Sqr(DifX * DifX + DifY * DifY) '距离
              DifX = DifX / Dist '单位向量
              DifY = DifY / Dist
              
              Dim MinDist As Double, MinDistSq As Double
              MinDist = Points(I).R + Points(J).R
              MinDistSq = MinDist * MinDist
              
              If Dist < MinDist Then
                  Dim Force As Double
                  Force = (1 - Dist / MinDist) * (Points(I).Res + Points(J).Res) * 0.5
                  
                  Points(I).XV = Points(I).XV - DifX * Force * DT * Points(J).M / Points(I).M
                  Points(I).YV = Points(I).YV - DifY * Force * DT * Points(J).M / Points(I).M
                  
                  Points(J).XV = Points(J).XV + DifX * Force * DT * Points(I).M / Points(J).M
                  Points(J).YV = Points(J).YV + DifY * Force * DT * Points(I).M / Points(J).M
              End If
          End If
      Next
  Next
  
  For I = 0 To UBound(Points)
      With Points(I)
      If .X < .R Then
          .X = .R
          .XV = Abs(.XV * WallResist)
      End If
      If .Y < .R Then
          .Y = .R
          .YV = Abs(.YV * WallResist)
      End If
      If .X > SceneWidth - .R Then
          .X = SceneWidth - .R
          .XV = -Abs(.XV * WallResist)
      End If
      If .Y > SceneHeight - .R Then
          .Y = SceneHeight - .R
          .YV = -Abs(.YV * WallResist)
      End If
      End With
  Next
  
End Sub
'--------------------------------------------------------------------------------

Function CH_E5B9B3E696B9_(ByVal nVal As Double) As Double
  CH_E5B9B3E696B9_ = nVal * nVal
End Function
'--------------------------------------------------------------------------------

Function CH_E5B084E7BABFE4B88EE59C86E79A84E79BB8E4BAA4E782B9_(ByVal CH_E8B5B7E782B9_X As Double, ByVal CH_E8B5B7E782B9_Y As Double, ByVal CH_E696B9E59091_X As Double, ByVal CH_E696B9E59091_Y As Double, ByVal CH_E8B79DE7A6BBE99990E588B6_ As Double, ByVal CH_E59C86E5BF83_X As Double, ByVal CH_E59C86E5BF83_Y As Double, ByVal CH_E58D8AE5BE84E5B9B3E696B9_ As Double, CH_E4BAA4E782B9_X As Double, CH_E4BAA4E782B9_Y As Double) As Boolean
  '             _____
  '        _,-"~     ~"-,_
  '     ,+~               ~+,
  '   ,+"                   "+,
  '  ,"                       ",
  ' ＋                         ＋
  ' /                           \
  '丨           圆心            丨
  '|              *--------------+-------------=* 起点
  '丨              \＼_         丨        _,-"~
  ' \         直角边\  ＼半径   /    _,-"~方向（单位向量）
  ' ＋               \   ＼_  ,＋,-"~
  '  ",               \     ＼*"交点
  '   "+,          直角*,-"~,+"
  '     ~+,               ,+~
  '        ~"-,_     _,-"~
  '             ~~~~~
  Dim CH_E8B5B7E782B9E588B0E59C86E5BF83_X As Double, CH_E8B5B7E782B9E588B0E59C86E5BF83_Y As Double, CH_E8B79DE7A6BBE5B9B3E696B9_ As Double
  CH_E8B5B7E782B9E588B0E59C86E5BF83_X = CH_E59C86E5BF83_X - CH_E8B5B7E782B9_X
  CH_E8B5B7E782B9E588B0E59C86E5BF83_Y = CH_E59C86E5BF83_Y - CH_E8B5B7E782B9_Y
  If CH_E696B9E59091_X * CH_E8B5B7E782B9E588B0E59C86E5BF83_X + CH_E696B9E59091_Y * CH_E8B5B7E782B9E588B0E59C86E5BF83_Y < 0 Then Exit Function '如果射线的方向背道而驰则退出
  CH_E8B79DE7A6BBE5B9B3E696B9_ = CH_E8B5B7E782B9E588B0E59C86E5BF83_X * CH_E8B5B7E782B9E588B0E59C86E5BF83_X + CH_E8B5B7E782B9E588B0E59C86E5BF83_Y * CH_E8B5B7E782B9E588B0E59C86E5BF83_Y
  
  Dim CH_E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_ As Double, CH_E696B9E59091E68980E59CA8E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_ As Double
  CH_E696B9E59091E68980E59CA8E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_ = CH_E5B9B3E696B9_(CH_E696B9E59091_X * CH_E8B5B7E782B9E588B0E59C86E5BF83_X + CH_E696B9E59091_Y * CH_E8B5B7E782B9E588B0E59C86E5BF83_Y)
  CH_E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_ = CH_E8B79DE7A6BBE5B9B3E696B9_ - CH_E696B9E59091E68980E59CA8E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_
  If CH_E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_ < 0 Or CH_E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_ > CH_E58D8AE5BE84E5B9B3E696B9_ Then Exit Function
  
  Dim CH_E98080E59B9EE79A84E8B79DE7A6BBE5B9B3E696B9_ As Double, CH_E4BAA4E782B9E588B0E8B5B7E782B9E79A84E8B79DE7A6BB_ As Double
  CH_E98080E59B9EE79A84E8B79DE7A6BBE5B9B3E696B9_ = CH_E58D8AE5BE84E5B9B3E696B9_ - CH_E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_
  If CH_E98080E59B9EE79A84E8B79DE7A6BBE5B9B3E696B9_ < 0 Then Exit Function
  
  CH_E4BAA4E782B9E588B0E8B5B7E782B9E79A84E8B79DE7A6BB_ = Sqr(CH_E696B9E59091E68980E59CA8E79BB4E8A792E8BEB9E995BFE5BAA6E5B9B3E696B9_) - Sqr(CH_E98080E59B9EE79A84E8B79DE7A6BBE5B9B3E696B9_)
  If CH_E4BAA4E782B9E588B0E8B5B7E782B9E79A84E8B79DE7A6BB_ > CH_E8B79DE7A6BBE99990E588B6_ Then Exit Function
  
  CH_E4BAA4E782B9_X = CH_E8B5B7E782B9_X + CH_E696B9E59091_X * CH_E4BAA4E782B9E588B0E8B5B7E782B9E79A84E8B79DE7A6BB_
  CH_E4BAA4E782B9_Y = CH_E8B5B7E782B9_Y + CH_E696B9E59091_Y * CH_E4BAA4E782B9E588B0E8B5B7E782B9E79A84E8B79DE7A6BB_
  CH_E5B084E7BABFE4B88EE59C86E79A84E79BB8E4BAA4E782B9_ = True
End Function


'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
      
'  Threaddetach ThreadCreate (@Moni,0)

End Sub
'--------------------------------------------------------------------------
Sub Moni(aa As Long ) '
  Dim I As Long
  Dim TimeFreq As ULongInt
  Dim LastTime As ULongInt, ThisTime As ULongInt
  '  Dim nDC As hDC
  '  Dim ps As rect
  '  Dim nText As String
  
  QueryPerformanceFrequency @TimeFreq
  QueryPerformanceCounter @ThisTime
  LastTime = ThisTime
  
  
  InitPoints 0, 0, SceneWidth, SceneHeight
  Do
      LastTime = ThisTime
      'Do
      QueryPerformanceCounter @ThisTime
      DeltaTime = (ThisTime - LastTime) / TimeFreq
      'Loop While DeltaTime < 1 / 60
      
      Form1.Picture1.Refresh() 
      FF_DoEvents
  Loop
  
End Sub


'--------------------------------------------------------------------------------
Sub Form1_WM_Size(hWndForm As hWnd, fwSizeType As Long, nWidth As Long, nHeight As Long)  '窗口已经改变了大小

  Form1.Picture1.move   0, 0, nWidth, nHeight
  SceneWidth = nWidth
  SceneHeight = nHeight

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Picture1_WM_MouseMove(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '移动鼠标

  MouX = xPos
  MouY = yPos

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Picture1_WM_LButtonUp(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '释放鼠标左键

  MouB = False
  MouC = -1

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Picture1_WM_LButtonDown(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '按下鼠标左键

  MouB = True
  Dim I As Long
  For I = 0 To UBound(Points)
      Dim SX As Single, SY As Single
      SX = xPos - Points(I).X
      SY = yPos - Points(I).Y
      If SX * SX + SY * SY <= Points(I).R * Points(I).R Then
          MouC = I
          Exit For
      End If
  Next

End Sub


'--------------------------------------------------------------------------------
Function Form1_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭

  End
  Function = 0   ' 根据你的需要改变
End Function


'--------------------------------------------------------------------------------
Function Form1_Picture1_WM_Paint(hWndForm As hWnd, hWndControl As hWnd) As LResult  '重绘，系统通知控件需要重新绘画。

  Dim nDC As hDC
  Dim ps As PAINTSTRUCT
  Dim As Long w,h,x,a,i,r,g,b,u
  Dim nText As String,jj As Single 
  nDC=BeginPaint(hWndControl,@ps) '获取需要绘画DC，推荐此方法，绘图效率高
  FF_Control_GetSize( hWndControl, w, h )
  Dim pMemBmp As CMemBmp = CMemBmp(w,h)  '创建内存DC，先画内存DC，加速画画速度，避免产生闪耀
  '需要内存DC类，必须在 FF_AppStart 模块里加   #include Once  "afx\CMemBmp.inc"
  '自己画画
  If DeltaTime Then
      nText="FPS:" &  1 / DeltaTime '显示正确的FPS
      TextOutA pMemBmp.GetMemDC , 5,5,StrPtr(nText),Len(nText)
      If DeltaTime > 0.1 Then DeltaTime = 0.1
      Movement DeltaTime
  End If
  u=UBound(Points)+1
  jj=256/(u/7)
  For I = 0 To U-1
'      Ellipse pMemBmp.GetMemDC, Points(I).X-Points(I).R, Points(I).Y-Points(I).R,Points(I).X+ Points(I).R,Points(I).Y+ Points(I).R
    If i<u/7 Then
        r=i*jj
         g=r
          b=r
    ElseIf i<u/7*2 Then
        r=(i-u/4)*jj: g=r: b=255
    ElseIf i<u/7*3 Then
        r=(i-u/2)*jj: g=255: b=r
    ElseIf i<u/7*4 Then
        r=255: g=(i-u/4*3)*jj: b=g
    ElseIf i<u/7*5 Then
        r=(i-u/4*3)*jj: b=255:g=255
    ElseIf i<u/7*6 Then
        r=255: g=(i-u/4*3)*jj: b=255
    Else 
        r=255: g=255: b=(i-u/4*3)*jj
    End If
    
    DrawEllipse( pMemBmp.GetMemDC,  Points(I).X-Points(I).R, Points(I).Y-Points(I).R, Points(I).R*2, Points(I).R*2, PS_SOLID,1, 0, BGR(r,g,b) )
  Next
  
  
  '自己画画完成
  BitBlt ndc,0,0,w,h,pMemBmp.GetMemDC,0,0,SrcCopy '将内存DC，输出到控件
  EndPaint(hWndForm,@ps) '完成绘图
  Function=True
End Function


'--------------------------------------------------------------------------------
Sub Form1_Timer1_WM_Timer(hWndForm As hWnd, wTimerID As Long)  '定时器

  KillTimer( hWndForm,wTimerID )
  Moni(0 ) '

End Sub


