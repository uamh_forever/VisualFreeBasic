﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
ClassName=
WinStyle=WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_TOPMOST,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=zhu.ico
Caption=
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=308
Height=397
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=6
Top=75
Width=140
Height=141
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Picture]
Name=Picture1
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
Left=0
Top=0
Width=272
Height=50
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]

Dim Shared TiSi As String '提示区文本 
Type anzt '按钮状态
    zuantai As Long '状态 =0禁用 =1启用 =2选中
    x As Long 
    y As Long 
    w As Long 
    h As Long  
End Type  
Dim Shared AnNiu(6) As anzt
Dim Shared AnNiuID As Long      '按钮ID
Dim Shared AnNiuZ As Long       '按钮状态 =0 没有，=1 鼠标移动 ，=2 按下
Dim Shared 录快捷键 As Long = 121  ' 
Dim Shared 放快捷键 As Long = 121  ' 

'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   
   PostMessage(hWndForm, WM_USER + 9800, 0, 0)
   TiSi = "勇芳_鼠标精灵 (" & IIf(Len(UserData) = 4, "32", "64") & "位) " & App.FileMajor & "." & App.FileMinor & "." & App.FileRevision
   IconToSys(hWndForm, 1, "AAAAA_APPICON", TiSi)
   Me.Caption = TiSi
   Me.WindowPlacementLoad App.Path & "精灵配置.INI", "窗口"
   Me.Width = AfxScaleX(275)
   Me.Height = AfxScaleY(100)
   AnNiu(0).zuantai = 1
   AnNiu(1).zuantai = 0
   AnNiu(2).zuantai = 1
   AnNiu(3).zuantai = 0
   AnNiu(4).zuantai = 1
   AnNiu(5).zuantai = 1
   AnNiu(6).zuantai = 1
   For i As Long = 0 To 6
      AnNiu(i).x = 5
      AnNiu(i).y = 5
      AnNiu(i).w = 35
      AnNiu(i).h = 25
   Next
   AnNiu(1).x = 40
   AnNiu(2).x = 80
   AnNiu(3).x = 115
   AnNiu(4).x = 155
   AnNiu(5).x = 190
   AnNiu(6).x = 230
End Sub



'                                                                                  
Function Form1_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   
   IconToSysUn(hWndForm, 1)  '删除任务栏图标
   Me.WindowPlacementSave App.Path & "精灵配置.INI", "窗口"
   Function = 0   ' 根据你的需要改变
End Function
'                                                                                  
Function Form1_Picture1_WM_Paint(hWndForm As hWnd, hWndControl As hWnd) As LResult  '重绘，系统通知控件需要重新绘画。

   Dim gg As yGDI = yGDI(hWndControl, &HFFFFE0, True)
   Dim ss As String = GetResourceStr("FONT_ICONFONT")
   Dim As Long uu, x, y, i
   Dim ff As Any Ptr = AddFontMemResourceEx(StrPtr(ss), Len(ss), 0, @uu)
   gg.Font "iconfont", 18
   Dim wz(6) As Long = {&HE781, &HE782, &HE7D1, &HE63B, &HE7D7, &HE6F5, &HE783}
   gg.Pen 0, 0
   
   For i = 0 To 6
      Select Case AnNiu(i).zuantai
         Case 0
            gg.SetColor &HDEC4B0
         Case 1
            If AnNiuID = i + 1 Then
               If AnNiuZ = 1 Then gg.Brush &HFFBF00 Else gg.Brush &HCD0000
               gg.DrawFrame AnNiu(i).x, AnNiu(i).y, AnNiu(i).w, AnNiu(i).h
               gg.SetColor &HFFFFFF
            Else
               gg.SetColor 0
            End If
         Case 2
            If AnNiuID = i + 1 Then
               If AnNiuZ = 1 Then
                  gg.Brush &H0096FF
                  gg.SetColor 0
               Else
                  gg.Brush &H004580
                  gg.SetColor &HFFFFFF
               End If
               
            Else
               gg.Brush &H0045FF
               gg.SetColor &HFFFFFF
            End If
            gg.DrawFrame AnNiu(i).x, AnNiu(i).y, AnNiu(i).w, AnNiu(i).h
      End Select
      gg.DrawTextS AnNiu(i).x, AnNiu(i).y, AnNiu(i).w, AnNiu(i).h, WString(1, wz(i)), DT_CENTER Or DT_VCENTER Or DT_SINGLELINE
   Next
   gg.Pen 1, 0
   gg.DrawLine 77, 7, 77, 26
   gg.DrawLine 152, 7, 152, 26
   gg.DrawLine 227, 7, 227, 26
   RemoveFontMemResourceEx ff
   gg.Font
   gg.SetColor 0
   gg.DrawString 5, 32, TiSi
   Function = True
End Function

'                                                                                  
Sub Form1_Picture1_WM_MouseMove(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '移动鼠标
   
   Dim As Long  a, b
   a = GetAnID(xPos, yPos)  '获取按钮ID
   
   If MouseFlags = 1 Then
      a = AnNiuID
      b = 2
   Else
      b = 1
   End If
   Select Case a
      Case 0
         TiSi = Me.Caption
      Case 1 '文字 "点【录制】就自动记录鼠标操作,【回放】就重复你刚才的操作.."
         TiSi = "【录制 / 停止录制】鼠标操作(快捷键 F" & 录快捷键 - 111 & ")"
      Case 2
         TiSi = "【回放 / 停止回放】鼠标操作(快捷键 F" & 放快捷键 - 111 & ")"
      Case 3
         TiSi = "打开以前保存的鼠标操作脚本文件"
      Case 4
         TiSi = "保存的录制的鼠标操作到【脚本】文件"
      Case 5
         TiSi = "前台\后台模式切换，前台回放时不能使用电脑"
      Case 6
         TiSi = "修改或编辑鼠标操作的脚本，适合高手使用。"
      Case 7
         TiSi = "打开帮助网页，第一次使用时建议看完帮助。"
   End Select
   If a <> AnNiuID Or AnNiuZ <> b Then
      AnNiuID = a
      AnNiuZ = b
      Me.Picture1.Refresh
   End If
   
   
   
End Sub

Function GetAnID(ByVal xPos As Long, ByVal yPos As Long) As Long '获取按钮ID
   Dim As Long  a, i
   If yPos >= AfxScaleY(5) And yPos <= AfxScaleY(30) Then
      For i = 0 To 6
         If xPos >= AfxScaleX(AnNiu(i).x) And xPos <= AfxScaleX(AnNiu(i).x + AnNiu(i).w) Then
            Return i + 1
         End If
      Next
   End If
   Function = 0
End Function


'                                                                                  
Function Form1_Picture1_Custom(hWndForm As hWnd, hWndControl As hWnd, wMsg As UInteger, wParam As wParam, lParam As lParam) As LResult  '自定义消息（全部消息），在其它事件后处理，返回非0，终止系统处理此消息。

   Dim entTrack As tagTRACKMOUSEEVENT
   Select Case wMsg
      Case WM_MOUSELEAVE   '鼠标出窗口
         If AnNiuID <> 0 Then
            AnNiuID = 0
            AnNiuZ = 0
            TiSi = Me.Caption
            Me.Picture1.Refresh
            
         End If
      Case WM_NCHITTEST        '启用鼠标出窗口检查
         entTrack.cbSize = SizeOf(tagTRACKMOUSEEVENT)
         entTrack.dwFlags = TME_LEAVE
         entTrack.hwndTrack = Me.Picture1.hWnd
         entTrack.dwHoverTime = HOVER_DEFAULT
         TrackMouseEvent @entTrack
      Case WM_ERASEBKGND
         Return 1  '防止擦除背景
   End Select
   Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Sub Form1_Picture1_WM_LButtonDown(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '按下鼠标左键
   
   Dim As Long  w, h, a
   
   a = GetAnID(xPos, yPos)  '获取按钮ID
   
   If a <> AnNiuID Or AnNiuZ <> 2 Then
      AnNiuID = a
      AnNiuZ = 2
      Me.Picture1.Refresh
   End If
   
End Sub

'                                                                                  
Sub Form1_Picture1_WM_LButtonUp(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '释放鼠标左键
   
   Dim As Long  w, h, a, i
   
   a = GetAnID(xPos, yPos)  '获取按钮ID
   
   If a = AnNiuID Then
      If AnNiu(a -1).zuantai > 0 Then
         Select Case a
            Case 1
               If AnNiu(0).zuantai = 1 Then AnNiu(0).zuantai = 2 Else AnNiu(0).zuantai = 1
            Case 2
               If AnNiu(1).zuantai = 1 Then AnNiu(1).zuantai = 2 Else AnNiu(1).zuantai = 1
            Case 3
            Case 4
            Case 5
               If AnNiu(4).zuantai = 1 Then AnNiu(4).zuantai = 2 Else AnNiu(4).zuantai = 1
            Case 6
               If AnNiu(5).zuantai = 1 Then
                  AnNiu(5).zuantai = 2
                  Me.Height = AfxScaleY(360)
               Else
                  AnNiu(5).zuantai = 1
                  Me.Height = AfxScaleY(76)
               End If
            Case 7
         End Select
      End If
   End If
   
   If AnNiuID <> 0 Or AnNiuZ <> 0 Then
      AnNiuID = 0
      AnNiuZ = 0
      Me.Picture1.Refresh
   End If
   
End Sub


