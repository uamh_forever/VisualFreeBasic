﻿#VisualFreeBasic_Form#  Version=5.8.0
Locked=0

[Form]
Name=Form1
ClassStyle=CS_DBLCLKS,CS_HREDRAW,CS_VREDRAW
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_RIGHTSCROLLBAR,WS_BORDER,WS_CAPTION,WS_SYSMENU,WS_MAXIMIZEBOX,WS_MINIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=测试窗口
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=True
Left=0
Top=0
Width=544
Height=338
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,25
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Picture]
Name=YFproTab1
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
Left=2
Top=1
Width=525
Height=32
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]

Dim Shared YFproTabL As YFproTab
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   '加载字体图标
   Dim ss As String = GetResourceStr("FONT_ICONFONT" )
   Dim As Long uu
   Dim ff As Any Ptr = AddFontMemResourceEx(StrPtr(ss), Len(ss), 0, @uu)
End Sub
Sub Form1_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   YFproTabL.hWndForm = hWndForm 
   YFproTabL.hWndControl = YFproTab1.hWnd  '控件绑定
   '增加被控制的窗口
   YFproTabL.AddTab(Form2.Show(hWndForm),"主页","主页",&He606)
   YFproTabL.AddTab(Form2.Show(hWndForm),"222测试","222测试",&He61a)
   YFproTabL.AddTab(Form2.Show(hWndForm),"333测试标签","3333",&He600)
   YFproTabL.AddTab(Form2.Show(hWndForm),"444测试标签","4444",&He600)
   YFproTabL.AddTab(Form2.Show(hWndForm),"5测试标签","5555",&He600)
   YFproTabL.AddTab(Form2.Show(hWndForm),"6测试标签","6666",&He600)
   YFproTabL.AddTab(Form2.Show(hWndForm),"7测试标签","7777",&He600)
   YFproTabL.AddTab(Form2.Show(hWndForm),"8测试标签","8888",&He600)
    
   
End Sub
Sub Form1_WM_Size(hWndForm As hWnd ,fwSizeType As Long ,nWidth As Long ,nHeight As Long) '窗口已经改变了大小
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'fwSizeType = SIZE_MAXHIDE     SIZE_MAXIMIZED   SIZE_MAXSHOW    SIZE_MINIMIZED    SIZE_RESTORED
   ''            其他窗口最大化   窗口已最大化     其他窗口恢复    窗口已最小化      窗口已调整大小
   'nWidth nHeight  是客户区大小，不是全部窗口大小。
   If fwSizeType = SIZE_MINIMIZED Then Return
   'xxx.Move AfxScaleX(5), AfxScaleY(5), nWidth - AfxScaleX(10), nHeight - AfxScaleY(30)
   Dim y As Long =AfxScaleY(30)
   YFproTab1.Move 0 ,0 ,nWidth , y
   Dim hh As Long = nHeight - y
   If hh < 10 Then hh = 10
   YFproTabL.SetPosAndSize(0 ,y ,nWidth ,hh) '重新设定被控制的窗口位置和大小
   
End Sub

Function Form1_YFproTab1_Custom(hWndForm As hWnd ,hWndControl As hWnd ,wMsg As UInteger ,wParam As wParam ,lParam As lParam) As LResult '自定义消息（全部消息），在其它事件处理后才轮到本事件。
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   'wMsg        消息值，0--&H400 由WIN系统定义，用户自定义是 WM_USER+** 定义。例 PostMessage( 窗口句柄 , Msg , wParam , lParam )
   'wParam      主消息参数，什么作用，由发送消息者定义
   'lParam      副消息参数，什么作用，由发送消息者定义
   Select Case wMsg
      Case WM_USER + 100 '事件：删除一个TAB，lParam 是TAB的索引 ，返回 True 将阻止删除，返回零允许删除
         '控件只移除关联窗口句柄，不负责窗口句柄销毁 ，需要自己在事件中处理
         Form2.hWndForm = YFproTabL.TabData(lParam).vWnd 
         Form2.Close  
         Return 0
      Case WM_USER + 101 '即将切换标签事件，返回非零不允许切换
         'wParam =旧的标签索引 lParam=新的标签索引
         Return 0
      Case WM_USER + 102 '切换标签后事件
         'wParam =旧的标签索引 lParam=新的标签索引
         Return 0
      Case WM_USER + 103 '点击了新增按钮事件
         YFproTabL.AddTab(Form2.Show(hWndForm),"新增测试标签","新增测试标签",&He600)
         Return 0
      Case WM_USER + 104 '点击了下拉列表按钮事件，返回非零不允许显示下拉列表
         Return 0
      Case WM_USER + 105 '即将关闭标签事件，返回非零不允许关闭 ，就是点击关闭按钮后发生的事件。
         'lParam=将要被关闭的标签索引
         '当被允许关闭后，再发送一个删除TAB事件，就是 WM_USER + 100 
         Return 0
   End Select
   Function = YFproTabL.MsgProcedure(hWndControl ,wMsg ,wParam ,lParam)
End Function






























