﻿#VisualFreeBasic_Form#  Version=5.8.0
Locked=0

[Form]
Name=Form2
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_SYSMENU,WS_MAXIMIZEBOX,WS_MINIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CHILD
Style=0 - 无边框
Icon=
Caption=Form2
StartPosition=2 - 父窗口中心
WindowState=0 - 正常
Enabled=True
Repeat=True
Left=0
Top=0
Width=344
Height=245
TopMost=False
Child=True
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=&HFFCCFFCC
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=Label1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=15
Top=13
Width=314
Height=222
Layout=5 - 宽度和高度
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]


Sub Form2_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   Label1.Caption = "VFB的用户控件例题" & vbCrLf & vbCrLf & "本的窗口句柄:" & Hex(hWndForm)  & vbCrLf & vbCrLf & _
   "本例题想抛砖引玉，让大家可以制作更好的控件以及方法" & vbCrLf & _
   "VFB常规控件分为：编辑部分、编译部分、代码部分、配置部分" & vbCrLf & _
   "而用户控件只用“代码部分”，比较容易让人理解和使用" & vbCrLf & _
   "希望可以帮助到你理解VFB控件制作"
   
End Sub







