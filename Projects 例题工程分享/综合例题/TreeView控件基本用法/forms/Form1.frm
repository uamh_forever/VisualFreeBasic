﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=TreeView
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=320
Height=414
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TreeView]
Name=TreeView1
Index=-1
BStyle=3 - 凹边框
Theme=True
ImageList=无图像列表控件
HasButtons=True
HasLines=True
LinesRoot=True
EditLabels=False
DisableDragDrop=False
ShowSelAlways=True
RTLreading=False
NoToolTips=False
Check=False
TrackSelect=False
SingleExpand=False
InfoTip=False
FullRowSelect=False
NoScroll=False
NoNevenHeight=False
NoHScroll=False
Enabled=True
Visible=True
LineColor=SYS,25
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=12
Top=8
Width=283
Height=320
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=关闭
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=234
Top=339
Width=58
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]
'==================================================================================================================================
'这是旧版本窗口，需要自己人工修改和调整才能正常使用，主要更改项目：
'1）窗口和控件属性：全部需要自己人工重新调整，升级程序只是简单的转换。
'2）窗口和控件事件：升级程序会自动更新到新版的格式，无法保证100%正确，更加自己需要查证。
'3）窗口和控件句柄：新版没有句柄常量，需要用类，如：HWND_FORM1  改为 Form1.hWnd ， HWND_FORM1_PICTURE1 改为 Form1.Picture1.hWnd 
'4）窗口和控件IDC： 新版无IDC常量，需要用类，如：IDC_FORM1_PICTURE1 改为 Form1.Picture1.IDC
'5）新弹出窗口：Form2_Show  改为 Form2.Show 
'6）与字符有关的API：以前默认为 ANSI字符，现在是 Unicode字符，因此相关API 后面要加个 A 来解决，如：PostMessage 改为 PostMessageA ，或使用宽字符变量
'7）API操作控件和窗口，有关字符的，全部为 Unicode字符 ，类型是 CWSTR，CWSTR 不可以直接用在 Len Left Right Mid InStr UCase 等一系列字符操作语句上
'必须前面加个 ** 才能当成宽字符处理（英文中文都算1个字符）如： Len(**b) ,可以赋值到ANSI变量，会自动转换编码：dim a As String = b  (b 为 CWSTR ) 
'或者 Dim b As CWSTR = a  (a 为 String) ,因此用变量过度一下，没什么问题，就是不可以直接返回就用在语句上，如：Len(FF_Control_GetText(..)) 这是错的
'8）编译后的窗口和控件是 Unicode 的，旧版是 ANSI 的在英文系统中无法显示中文。可以用API IsWindowUnicode 检查窗口和控件是不是 Unicode
'9）所有窗口和控件由 CWindow Class 类创建，旧版是直接API创建，可以参考 WinFBX 帮助来查看 CW 的众多附加功能。
'最后预祝大家编程愉快，升级顺利。有任何问题可以在编程群里提问：Basic语言编程QQ群 78458582
'==================================================================================================================================
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。
'32位与64位切换，或其它方式，点击工具栏右边。
'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。

  Dim cx As Long = AfxScaleX(16)                         
   Dim hImageList As HIMAGELIST
   hImageList = ImageList_Create(cx, cx, ILC_COLOR32 Or ILC_MASK, 3, 0)
   If hImageList Then
       AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_FOLDER_CLOSED_32")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_FOLDER_OPEN_32")
      AfxGdipAddIconFromRes(hImageList, App.HINSTANCE, "PNG_ARROWRIGHT_GREEN_32")
      ' // Set the image list
'      TreeView_SetImageList(TreeView1.hWnd, hImageList, TVSIL_NORMAL)
      TreeView1.ImageList= hImageList
   End If

   ' // Add items to the TreeView
   Dim As HTREEITEM hRoot, hNode, hItem
   ' // Create the root node             控件类和直接API函数操作对应方法，
   hRoot = TreeView1.AddItem(TVI_ROOT, "Root", , 2) '  TreeView_AddRootItem(TreeView1.hWnd, "Root", , , 2)
   ' // Create a node
   hNode = TreeView1.AddItem(hRoot, "Node 1", , , 2) '  TreeView_AppendItem(TreeView1.hWnd, hRoot, "Node 1", , , 2)
   ' // Insert items in the node
   hItem = TreeView1.AddItem(hNode, "Node 1 Item 1", , , 2) ' TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 1 Item 1", , , 2)
   hItem = TreeView1.AddItem(hNode, "Node 1 Item 2", , , 2) 'TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 1 Item 2", , , 2)
   hItem = TreeView1.AddItem(hNode, "Node 1 Item 3", , , 2) 'TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 1 Item 3", , , 2)
   hItem = TreeView1.AddItem(hNode, "Node 1 Item 4", , , 2) 'TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 1 Item 4", , , 2)
   hItem = TreeView1.AddItem(hNode, "Node 1 Item 5", , , 2) 'TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 1 Item 5", , , 2)
   hItem = TreeView1.AddItem(hNode, "Node 1 Item 6", , , 2) 'TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 1 Item 6", , , 2)
   ' // Expand the node      控件类和直接API函数操作对应方法
   TreeView1.Expand(hNode)=True ' TreeView_Expand(TreeView1.hWnd, hNode, TVM_EXPAND)
   ' // Create another node
   hNode = TreeView_AppendItem(TreeView1.hWnd , hRoot, "Node 2", , , 2)
   ' // Insert items in the node
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 2 Item 1", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 2 Item 2", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 2 Item 3", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 2 Item 4", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 2 Item 5", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 2 Item 6", , , 2)
   ' // Expand the node
   TreeView_Expand(TreeView1.hWnd, hNode, TVM_EXPAND)
   ' // Create another node
   hNode = TreeView_AppendItem(TreeView1.hWnd, hRoot, "Node 3", , , 2)
   ' // Insert items in the node
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 3 Item 1", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 3 Item 2", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 3 Item 3", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 3 Item 4", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 3 Item 5", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 3 Item 6", , , 2)
   ' // Expand the node
   TreeView_Expand(TreeView1.hWnd, hNode, TVM_EXPAND)
   ' // Create another node
   hNode = TreeView_AppendItem(TreeView1.hWnd, hRoot, "Node 4", , , 2)
   ' // Insert items in the node
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 4 Item 1", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 4 Item 2", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 4 Item 3", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 4 Item 4", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 4 Item 5", , , 2)
   hItem = TreeView_AppendItem(TreeView1.hWnd, hNode, "Node 4 Item 6", , , 2)
   ' // Expand the node
   TreeView_Expand(TreeView1.hWnd, hNode, TVM_EXPAND)

   ' // Expand the root node
   TreeView_Expand(TreeView1.hWnd, hRoot, TVE_EXPAND)

End Sub


'                                                                                  
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Me.Close

End Sub

'                                                                                  
Sub Form1_WM_Destroy(hWndForm As hWnd)  '即将销毁窗口
                          
ImageList_Destroy(TreeView_SetImageList(TreeView1.hWnd, Null, TVSIL_NORMAL))

End Sub


'                                                                                  
Function Form1_Custom(hWndForm As hWnd, wMsg As UInteger, wParam As wParam, lParam As lParam) As LResult  '自定义消息（全部消息），在其它事件后处理，返回非0，终止系统处理此消息。

  Select Case wMsg
      Case WM_NOTIFY
          Dim ptnmhdr As NMHDR Ptr = Cast(NMHDR Ptr, lParam)
          Select Case ptnmhdr->idFrom
              Case TREEVIEW1.IDC
                  Select Case ptnmhdr->code
                      Case NM_DBLCLK
                          '// Retrieve the handle of the TreeView
                          Dim hTreeView As hWnd = GetDlgItem(hWndForm, TREEVIEW1.IDC)
                          '// Retrieve the selected item
                          Dim hItem As HTREEITEM = TreeView_GetSelection(hTreeView)
                          '// Retrieve the text of the selected item
                          Dim wszText As WString * 260
                          TreeView_GetItemText(hTreeView, hItem, @wszText, 260)
                          MessageBox hWndForm, wszText, "", MB_OK
                          Exit Function

                  End Select
          End Select
  End Select
  Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Sub Form1_TreeView1_TVN_ItemExpanded(hWndForm As hWnd, hWndControl As hWnd,pNMTV As NM_TREEVIEW) '某结点已被展开或收缩


  Dim tvi As TVITEM = pNMTV.itemNew
  If (pNMTV.itemNew.state And TVIS_EXPANDED) = TVIS_EXPANDED Then
      tvi.iImage = 1
  Else
      tvi.iImage = 0
  End If
  '// Set the item's new attributes
  TreeView_SetItem(pNMTV.hdr.hwndFrom, @tvi)

End Sub

