﻿#VisualFreeBasic_Form#  Version=5.2.8
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=sqlite3数据库控件 例题
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=352
Height=385
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=打开
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=24
Top=11
Width=64
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=设置密码
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=24
Top=93
Width=61
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=SQL查询
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=102
Top=74
Width=66
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=多行查询
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=97
Top=9
Width=73
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=单行查询
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=100
Top=44
Width=68
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=清除密码
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=24
Top=122
Width=62
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=新增一行
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=101
Top=110
Width=66
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command8
Index=-1
Caption=更新数据
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=105
Top=174
Width=63
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command9
Index=-1
Caption=版本
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=24
Top=49
Width=60
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command10
Index=-1
Caption=表结构
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=23
Top=276
Width=62
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command11
Index=-1
Caption=保存配置
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=188
Top=11
Width=75
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command12
Index=-1
Caption=读取配置
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=189
Top=44
Width=74
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command13
Index=-1
Caption=删除记录
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=105
Top=210
Width=62
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command14
Index=-1
Caption=统计行数
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=103
Top=144
Width=63
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command15
Index=-1
Caption=写2进制数据
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=183
Top=100
Width=80
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command16
Index=-1
Caption=读2进制数据
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=184
Top=131
Width=80
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command17
Index=-1
Caption=整理数据库
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=20
Top=311
Width=71
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command18
Index=-1
Caption=事务
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=186
Top=169
Width=76
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command19
Index=-1
Caption=创建表
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=22
Top=165
Width=65
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command20
Index=-1
Caption=添加字段
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=23
Top=194
Width=63
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command21
Index=-1
Caption=创建索引
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=22
Top=226
Width=64
Height=25
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[SQLite3]
Name=SQLite31
Mode=1 - 内置数据库引擎
Left=133
Top=276
Tag=


[AllCode]


'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  If SQLite31.Open(App.Path & "sqlite3_test.db", "") = False Then
     Print "打开数据库成功"                   
  Else
     Print "打开数据库失败 " & SQLite31.ErrMsg 
  End If 

End Sub


'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  If SQLite31.SetKey("123456")=False Then Print SQLite31.ErrMsg     
  Print "修改密码"


End Sub
                                                                                                                                                                                                                                                                                
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim rs() As String  
  Dim u As Long = SQLite31.SqlFind("SELECT * FROM features WHERE id=100", rs()) 
  Dim As Long x,y
  If u=0 Then Print SQLite31.ErrMsg  
  Print UBound(rs), UBound(rs, 2)
    For y=0 To UBound( rs)
        For x = 0 To UBound(rs, 2)
            Print rs(y, x)  ,
        Next
        Print
    Next


End Sub
                                                                                                                                                                                                                                                                                
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  Dim rs() As String  
  Dim u As Long = SQLite31.Find("features", "id>0", rs()) 
  Dim As Long x,y
  If u=0 Then Print SQLite31.ErrMsg  
  Print UBound(rs), UBound(rs, 2)
    For y=0 To UBound( rs)
        For x = 0 To UBound(rs, 2)
            Print Utf8toStr(rs(y, x)), 
        Next
        Print
    Next

  
  

  
End Sub


'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  Dim rs() As String  
  Dim u As Long = SQLite31.FindOne("features", "id=2", rs()) 
  Dim As Long x,y
  If u=0 Then Print SQLite31.ErrMsg  
  Print "字段数：" &  UBound(rs)+1

        For x = 0 To UBound(rs)
            Print Utf8toStr(rs(x))
        Next
        Print


End Sub
                                                                                                                                                                                                                                                                                
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  If SQLite31.SetKey("")=False Then Print SQLite31.ErrMsg     
   Print "清除密码"


End Sub
                                                                                                                                                                                                                                                                                
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Print SQLite31.AddItem ("features", "title=100,description='dd''d'")
    


End Sub


'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command8_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Print SQLite31.Update ("features","id=2", "description='更新内容'")
  Print SQLite31.ErrMsg 

End Sub
                                                                                                                                                                                                                                                                                
                                                                                                                                                                         
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command9_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Print "数据库引擎版本：" & SQLite31.Version 


End Sub
                                                                                                                                                                                                                                                                                
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command10_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim rs() As String  
  Dim u As Long = SQLite31.SqlFind("PRAGMA  table_info('features') ", rs())  ' features 为表名称
  Dim As Long x,y
  If u=0 Then Print SQLite31.ErrMsg  
  Print UBound(rs), UBound(rs, 2)
    For y=0 To UBound( rs)
        For x = 0 To UBound(rs, 2)
            Print Utf8toStr(rs(y, x)),   
        Next
        Print
    Next


End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command11_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim rs() As String  
  Print SQLite31.INIsetKey("软件配置", "测试项目", "测试值")  
  


End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command12_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim rs() As String  
  Print SQLite31.INIgetKey("软件配置", "测试项目", "默认值")  


End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command13_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim rs() As String
  Dim id As Long = SQLite31.MaxID("features", "id")
  
  If id = 0 Then
      Print SQLite31.ErrMsg
  Else
      Print "删除ID=" & id
      Print SQLite31.DeleteItem("features", "id=" & id)
  End If
  

End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command14_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Print SQLite31.Count("features")  


End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command15_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim id As Long = SQLite31.AddItem("[二进制]", "aaa=100")   '为了避免表名称或字段名称与SQL内部名称冲突，可以用[] 括在中间，中文更应该括起来
   If id = 0 Then 
       Print "出错：" & SQLite31.ErrMsg
   Else
       Dim by(10) As UByte 
       For i As Long = 0 To 10
           by(i) = Int(Rnd * 255)
           Print Hex(by(i), 2) & " " ; 
       Next 
       Print  
       Print SQLite31.UpdateByte("[二进制]", "ID=" & id, "[数据]", @by(0), 11)  '为了避免表名称或字段名称与SQL内部名称冲突，可以用[] 括在中间，中文更应该括起来
      Print "出错：" & SQLite31.ErrMsg
   End If   

End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command16_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  Dim rs() As String  
  Dim u As Long = SQLite31.Find("[二进制]", "", rs()) '为了避免表名称或字段名称与SQL内部名称冲突，可以用[] 括在中间，中文更应该括起来
  Dim As Long x,y ,i
  If u=0 Then Print SQLite31.ErrMsg  
  Print UBound(rs), UBound(rs, 2)
    For y=0 To UBound( rs)
        For x = 0 To UBound(rs, 2)
            If y > 0 And x = 2 Then 
                Dim ss As String 
                For i = 0 To Len(rs(y, x)) -1
                    ss &= Hex(rs(y, x)[i], 2) & " "    ' String 可以包含任意值，当然可以是2进制数据了
                Next  
                Print ss, 
            Else  
                Print rs(y, x), 
            End If  
        Next
        Print
    Next


End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command17_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Print SQLite31.Vacuum


End Sub


'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command18_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  SQLite31.TransactionBegin   ' 事务开始（用于批量执行SQL，提高SQL效率）
  Dim i As Long 
  For i = 1 To 100
      SQLite31.AddItem("features", "title=" & i & ",description='" & Rnd & "'")
  Next  
  Print SQLite31.TransactionEnd ' 事务执行并且结束
  Print "ok" 

End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command19_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  Print SQLite31.CreateTable("[创建的新表]") 
  Print "出错：" & SQLite31.ErrMsg

End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command20_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  Print SQLite31.AddField("[创建的新表]", "[新字段]", "TEXT","'dd'") 
  Print "出错：" & SQLite31.ErrMsg


End Sub
                                                                                                                                                                                                                                                                                     
'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub Form1_Command21_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

   Print SQLite31.CreateIndex("[创建的新表]", "[新字段]", True) 
  Print "出错：" & SQLite31.ErrMsg
      

End Sub



