﻿'----------------
'ComClass--------
Const CLSID_cCom = "{920DCEED-B3DD-4110-8F2F-981A554E07F4}"
'Event--------
Const IID___cCom = "{62DD544B-09E0-4340-8FF8-41CCC0FD0662}"

Type cCom___cCom Extends CIDispatch
   DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   DECLARE Sub __cCom_ObjectEvent(ByVal Info As EventInfo)
   Declare Constructor()
   Attr2Changed As SUB(ByVal var1 As Double)
End Type
'Implements----
Sub cCom___cCom. __cCom_ObjectEvent(ByVal Info As EventInfo)
   Select Case Info.EventID
      Case &H1      'Attr2Changed
         If Attr2Changed then Attr2Changed(info.EventParameters.Item(0).Value.dblVal)
   End Select
End Sub
CONSTRUCTOR cCom___cCom()
   IIDFromString(IID___cCom, @Base.EventIID)
END CONSTRUCTOR
Function cCom___cCom.Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   Dim Info as EventInfo
   Info.EventID = dispIdMember
   Info.EventParameters.Count = pDispParams->cArgs
   If pDispParams->cArgs > 0 Then
      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)
      For i As Long = 0 to pDispParams->cArgs -1
         Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]
      Next
   End If
   This. __cCom_ObjectEvent(Info)
   Function = S_OK
End Function




