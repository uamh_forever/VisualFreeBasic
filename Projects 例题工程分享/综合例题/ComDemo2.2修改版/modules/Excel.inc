﻿'----------------
'ComClass--------
Const CLSID_QueryTable = "{59191DA1-EA47-11CE-A51F-00AA0061507F}"
'Event--------
Const IID_RefreshEvents = "{0002441B-0000-0000-C000-000000000046}"
Type QueryTable_RefreshEvents Extends CIDispatch
   DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   DECLARE Sub RefreshEvents_ObjectEvent(ByVal Info As EventInfo)
   Declare Constructor()
   On_BeforeRefresh As SUB(ByRef Cancel As VARIANT_BOOL)
   On_AfterRefresh As SUB(ByVal Success As VARIANT_BOOL)
End Type
'----------------
'ComClass--------
Const CLSID_Application = "{00024500-0000-0000-C000-000000000046}"
'Event--------
Const IID_AppEvents = "{00024413-0000-0000-C000-000000000046}"
Type Application_AppEvents Extends CIDispatch
   DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   DECLARE Sub AppEvents_ObjectEvent(ByVal Info As EventInfo)
   Declare Constructor()
   On_NewWorkbook As SUB(ByRef Wb As Any Ptr)
   On_SheetSelectionChange As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr)
   On_SheetBeforeDoubleClick As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr,ByRef Cancel As VARIANT_BOOL)
   On_SheetBeforeRightClick As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr,ByRef Cancel As VARIANT_BOOL)
   On_SheetActivate As SUB(ByVal Sh As IDispatch Ptr)
   On_SheetDeactivate As SUB(ByVal Sh As IDispatch Ptr)
   On_SheetCalculate As SUB(ByVal Sh As IDispatch Ptr)
   On_SheetChange As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr)
   On_WorkbookOpen As SUB(ByRef Wb As Any Ptr)
   On_WorkbookActivate As SUB(ByRef Wb As Any Ptr)
   On_WorkbookDeactivate As SUB(ByRef Wb As Any Ptr)
   On_WorkbookBeforeClose As SUB(ByRef Wb As Any Ptr,ByRef Cancel As VARIANT_BOOL)
   On_WorkbookBeforeSave As SUB(ByRef Wb As Any Ptr,ByVal SaveAsUI As VARIANT_BOOL,ByRef Cancel As VARIANT_BOOL)
   On_WorkbookBeforePrint As SUB(ByRef Wb As Any Ptr,ByRef Cancel As VARIANT_BOOL)
   On_WorkbookNewSheet As SUB(ByRef Wb As Any Ptr,ByVal Sh As IDispatch Ptr)
   On_WorkbookAddinInstall As SUB(ByRef Wb As Any Ptr)
   On_WorkbookAddinUninstall As SUB(ByRef Wb As Any Ptr)
   On_WindowResize As SUB(ByRef Wb As Any Ptr,ByRef Wn As Any Ptr)
   On_WindowActivate As SUB(ByRef Wb As Any Ptr,ByRef Wn As Any Ptr)
   On_WindowDeactivate As SUB(ByRef Wb As Any Ptr,ByRef Wn As Any Ptr)
   On_SheetFollowHyperlink As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr)
   On_SheetPivotTableUpdate As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr)
   On_WorkbookPivotTableCloseConnection As SUB(ByRef Wb As Any Ptr,ByRef Target As Any Ptr)
   On_WorkbookPivotTableOpenConnection As SUB(ByRef Wb As Any Ptr,ByRef Target As Any Ptr)
   On_WorkbookSync As SUB(ByRef Wb As Any Ptr,ByVal SyncEventType As Any Ptr)
   On_WorkbookBeforeXmlImport As SUB(ByRef Wb As Any Ptr,ByRef Map As Any Ptr,ByVal Url As BSTR,ByVal IsRefresh As VARIANT_BOOL,ByRef Cancel As VARIANT_BOOL)
   On_WorkbookAfterXmlImport As SUB(ByRef Wb As Any Ptr,ByRef Map As Any Ptr,ByVal IsRefresh As VARIANT_BOOL,ByVal Result As Any Ptr)
   On_WorkbookBeforeXmlExport As SUB(ByRef Wb As Any Ptr,ByRef Map As Any Ptr,ByVal Url As BSTR,ByRef Cancel As VARIANT_BOOL)
   On_WorkbookAfterXmlExport As SUB(ByRef Wb As Any Ptr,ByRef Map As Any Ptr,ByVal Url As BSTR,ByVal Result As Any Ptr)
   On_WorkbookRowsetComplete As SUB(ByRef Wb As Any Ptr,ByVal Description As BSTR,ByVal Sheet As BSTR,ByVal Success As VARIANT_BOOL)
   On_AfterCalculate As SUB()
End Type
'----------------
'ComClass--------
Const CLSID_Chart = "{00020821-0000-0000-C000-000000000046}"
'Event--------
Const IID_ChartEvents = "{0002440F-0000-0000-C000-000000000046}"
Type Chart_ChartEvents Extends CIDispatch
   DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   DECLARE Sub ChartEvents_ObjectEvent(ByVal Info As EventInfo)
   Declare Constructor()
   On_Activate As SUB()
   On_Deactivate As SUB()
   On_Resize As SUB()
   On_MouseDown As SUB(ByVal Button As Long,ByVal Shift As Long,ByVal x As Long,ByVal y As Long)
   On_MouseUp As SUB(ByVal Button As Long,ByVal Shift As Long,ByVal x As Long,ByVal y As Long)
   On_MouseMove As SUB(ByVal Button As Long,ByVal Shift As Long,ByVal x As Long,ByVal y As Long)
   On_BeforeRightClick As SUB(ByRef Cancel As VARIANT_BOOL)
   On_DragPlot As SUB()
   On_DragOver As SUB()
   On_BeforeDoubleClick As SUB(ByVal ElementID As Long,ByVal Arg1 As Long,ByVal Arg2 As Long,ByRef Cancel As VARIANT_BOOL)
   On_Select As SUB(ByVal ElementID As Long,ByVal Arg1 As Long,ByVal Arg2 As Long)
   On_SeriesChange As SUB(ByVal SeriesIndex As Long,ByVal PointIndex As Long)
   On_Calculate As SUB()
End Type
'----------------
'ComClass--------
Const CLSID_Worksheet = "{00020820-0000-0000-C000-000000000046}"
'Event--------
Const IID_DocEvents = "{00024411-0000-0000-C000-000000000046}"
Type Worksheet_DocEvents Extends CIDispatch
   DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   DECLARE Sub DocEvents_ObjectEvent(ByVal Info As EventInfo)
   Declare Constructor()
   On_SelectionChange As SUB(ByRef Target As Any Ptr)
   On_BeforeDoubleClick As SUB(ByRef Target As Any Ptr,ByRef Cancel As VARIANT_BOOL)
   On_BeforeRightClick As SUB(ByRef Target As Any Ptr,ByRef Cancel As VARIANT_BOOL)
   On_Activate As SUB()
   On_Deactivate As SUB()
   On_Calculate As SUB()
   On_Change As SUB(ByRef Target As Any Ptr)
   On_FollowHyperlink As SUB(ByRef Target As Any Ptr)
   On_PivotTableUpdate As SUB(ByRef Target As Any Ptr)
End Type
'----------------
'ComClass--------
Const CLSID_Global = "{00020812-0000-0000-C000-000000000046}"
'----------------
'ComClass--------
Const CLSID_Workbook = "{00020819-0000-0000-C000-000000000046}"
'Event--------
Const IID_WorkbookEvents = "{00024412-0000-0000-C000-000000000046}"
Type Workbook_WorkbookEvents Extends CIDispatch
   DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   DECLARE Sub WorkbookEvents_ObjectEvent(ByVal Info As EventInfo)
   Declare Constructor()
   On_Open As SUB()
   On_Activate As SUB()
   On_Deactivate As SUB()
   On_BeforeClose As SUB(ByRef Cancel As VARIANT_BOOL)
   On_BeforeSave As SUB(ByVal SaveAsUI As VARIANT_BOOL,ByRef Cancel As VARIANT_BOOL)
   On_BeforePrint As SUB(ByRef Cancel As VARIANT_BOOL)
   On_NewSheet As SUB(ByVal Sh As IDispatch Ptr)
   On_AddinInstall As SUB()
   On_AddinUninstall As SUB()
   On_WindowResize As SUB(ByRef Wn As Any Ptr)
   On_WindowActivate As SUB(ByRef Wn As Any Ptr)
   On_WindowDeactivate As SUB(ByRef Wn As Any Ptr)
   On_SheetSelectionChange As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr)
   On_SheetBeforeDoubleClick As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr,ByRef Cancel As VARIANT_BOOL)
   On_SheetBeforeRightClick As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr,ByRef Cancel As VARIANT_BOOL)
   On_SheetActivate As SUB(ByVal Sh As IDispatch Ptr)
   On_SheetDeactivate As SUB(ByVal Sh As IDispatch Ptr)
   On_SheetCalculate As SUB(ByVal Sh As IDispatch Ptr)
   On_SheetChange As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr)
   On_SheetFollowHyperlink As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr)
   On_SheetPivotTableUpdate As SUB(ByVal Sh As IDispatch Ptr,ByRef Target As Any Ptr)
   On_PivotTableCloseConnection As SUB(ByRef Target As Any Ptr)
   On_PivotTableOpenConnection As SUB(ByRef Target As Any Ptr)
   On_Sync As SUB(ByVal SyncEventType As Any Ptr)
   On_BeforeXmlImport As SUB(ByRef Map As Any Ptr,ByVal Url As BSTR,ByVal IsRefresh As VARIANT_BOOL,ByRef Cancel As VARIANT_BOOL)
   On_AfterXmlImport As SUB(ByRef Map As Any Ptr,ByVal IsRefresh As VARIANT_BOOL,ByVal Result As Any Ptr)
   On_BeforeXmlExport As SUB(ByRef Map As Any Ptr,ByVal Url As BSTR,ByRef Cancel As VARIANT_BOOL)
   On_AfterXmlExport As SUB(ByRef Map As Any Ptr,ByVal Url As BSTR,ByVal Result As Any Ptr)
   On_RowsetComplete As SUB(ByVal Description As BSTR,ByVal Sheet As BSTR,ByVal Success As VARIANT_BOOL)
End Type
'----------------
'ComClass--------
Const CLSID_OLEObject = "{00020818-0000-0000-C000-000000000046}"
'Event--------
Const IID_OLEObjectEvents = "{00024410-0000-0000-C000-000000000046}"
Type OLEObject_OLEObjectEvents Extends CIDispatch
   DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   DECLARE Sub OLEObjectEvents_ObjectEvent(ByVal Info As EventInfo)
   Declare Constructor()
   On_GotFocus As SUB()
   On_LostFocus As SUB()
End Type
'Implements----
Sub QueryTable_RefreshEvents.RefreshEvents_ObjectEvent(ByVal Info As EventInfo)
   Select Case Info.EventID
      Case &H63C      'BeforeRefresh
         If On_BeforeRefresh then On_BeforeRefresh(info.EventParameters.Item(0).Value.boolVal)
      Case &H63D      'AfterRefresh
         If On_AfterRefresh then On_AfterRefresh(info.EventParameters.Item(0).Value.boolVal)
   End Select
End Sub
CONSTRUCTOR QueryTable_RefreshEvents()
   IIDFromString(IID_RefreshEvents, @Base.EventIID)
END CONSTRUCTOR
Function QueryTable_RefreshEvents.Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   Dim Info as EventInfo
   Info.EventID = dispIdMember
   Info.EventParameters.Count = pDispParams->cArgs
   If pDispParams->cArgs > 0 Then
      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)
      For i As Long = 0 to pDispParams->cArgs -1
         Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]
      Next
   End If
   This.RefreshEvents_ObjectEvent(Info)
   Function = 0
End Function
Sub Application_AppEvents.AppEvents_ObjectEvent(ByVal Info As EventInfo)
   Select Case Info.EventID
      Case &H61D      'NewWorkbook
         If On_NewWorkbook then On_NewWorkbook(info.EventParameters.Item(0).Value.byref)
      Case &H616      'SheetSelectionChange
         If On_SheetSelectionChange then On_SheetSelectionChange(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref)
      Case &H617      'SheetBeforeDoubleClick
         If On_SheetBeforeDoubleClick then On_SheetBeforeDoubleClick(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref, info.EventParameters.Item(2).Value.boolVal)
      Case &H618      'SheetBeforeRightClick
         If On_SheetBeforeRightClick then On_SheetBeforeRightClick(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref, info.EventParameters.Item(2).Value.boolVal)
      Case &H619      'SheetActivate
         If On_SheetActivate then On_SheetActivate(info.EventParameters.Item(0).Value.pdispVal)
      Case &H61A      'SheetDeactivate
         If On_SheetDeactivate then On_SheetDeactivate(info.EventParameters.Item(0).Value.pdispVal)
      Case &H61B      'SheetCalculate
         If On_SheetCalculate then On_SheetCalculate(info.EventParameters.Item(0).Value.pdispVal)
      Case &H61C      'SheetChange
         If On_SheetChange then On_SheetChange(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref)
      Case &H61F      'WorkbookOpen
         If On_WorkbookOpen then On_WorkbookOpen(info.EventParameters.Item(0).Value.byref)
      Case &H620      'WorkbookActivate
         If On_WorkbookActivate then On_WorkbookActivate(info.EventParameters.Item(0).Value.byref)
      Case &H621      'WorkbookDeactivate
         If On_WorkbookDeactivate then On_WorkbookDeactivate(info.EventParameters.Item(0).Value.byref)
      Case &H622      'WorkbookBeforeClose
         If On_WorkbookBeforeClose then On_WorkbookBeforeClose(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.boolVal)
      Case &H623      'WorkbookBeforeSave
         If On_WorkbookBeforeSave then On_WorkbookBeforeSave(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.boolVal, info.EventParameters.Item(2).Value.boolVal)
      Case &H624      'WorkbookBeforePrint
         If On_WorkbookBeforePrint then On_WorkbookBeforePrint(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.boolVal)
      Case &H625      'WorkbookNewSheet
         If On_WorkbookNewSheet then On_WorkbookNewSheet(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.pdispVal)
      Case &H626      'WorkbookAddinInstall
         If On_WorkbookAddinInstall then On_WorkbookAddinInstall(info.EventParameters.Item(0).Value.byref)
      Case &H627      'WorkbookAddinUninstall
         If On_WorkbookAddinUninstall then On_WorkbookAddinUninstall(info.EventParameters.Item(0).Value.byref)
      Case &H612      'WindowResize
         If On_WindowResize then On_WindowResize(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref)
      Case &H614      'WindowActivate
         If On_WindowActivate then On_WindowActivate(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref)
      Case &H615      'WindowDeactivate
         If On_WindowDeactivate then On_WindowDeactivate(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref)
      Case &H73E      'SheetFollowHyperlink
         If On_SheetFollowHyperlink then On_SheetFollowHyperlink(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref)
      Case &H86D      'SheetPivotTableUpdate
         If On_SheetPivotTableUpdate then On_SheetPivotTableUpdate(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref)
      Case &H870      'WorkbookPivotTableCloseConnection
         If On_WorkbookPivotTableCloseConnection then On_WorkbookPivotTableCloseConnection(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref)
      Case &H871      'WorkbookPivotTableOpenConnection
         If On_WorkbookPivotTableOpenConnection then On_WorkbookPivotTableOpenConnection(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref)
      Case &H8F1      'WorkbookSync
         If On_WorkbookSync then On_WorkbookSync(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref)
      Case &H8F2      'WorkbookBeforeXmlImport
         If On_WorkbookBeforeXmlImport then On_WorkbookBeforeXmlImport(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref, info.EventParameters.Item(2).Value.bstrVal, info.EventParameters.Item(3).Value.boolVal, info.EventParameters.Item(4).Value.boolVal)
      Case &H8F3      'WorkbookAfterXmlImport
         If On_WorkbookAfterXmlImport then On_WorkbookAfterXmlImport(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref, info.EventParameters.Item(2).Value.boolVal, info.EventParameters.Item(3).Value.byref)
      Case &H8F4      'WorkbookBeforeXmlExport
         If On_WorkbookBeforeXmlExport then On_WorkbookBeforeXmlExport(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref, info.EventParameters.Item(2).Value.bstrVal, info.EventParameters.Item(3).Value.boolVal)
      Case &H8F5      'WorkbookAfterXmlExport
         If On_WorkbookAfterXmlExport then On_WorkbookAfterXmlExport(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.byref, info.EventParameters.Item(2).Value.bstrVal, info.EventParameters.Item(3).Value.byref)
      Case &HA33      'WorkbookRowsetComplete
         If On_WorkbookRowsetComplete then On_WorkbookRowsetComplete(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.bstrVal, info.EventParameters.Item(2).Value.bstrVal, info.EventParameters.Item(3).Value.boolVal)
      Case &HA34      'AfterCalculate
         If On_AfterCalculate then On_AfterCalculate()
   End Select
End Sub
CONSTRUCTOR Application_AppEvents()
   IIDFromString(IID_AppEvents, @Base.EventIID)
END CONSTRUCTOR
Function Application_AppEvents.Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   Dim Info as EventInfo
   Info.EventID = dispIdMember
   Info.EventParameters.Count = pDispParams->cArgs
   If pDispParams->cArgs > 0 Then
      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)
      For i As Long = 0 to pDispParams->cArgs -1
         Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]
      Next
   End If
   This.AppEvents_ObjectEvent(Info)
   Function = 0
End Function
Sub Chart_ChartEvents.ChartEvents_ObjectEvent(ByVal Info As EventInfo)
   Select Case Info.EventID
      Case &H130      'Activate
         If On_Activate then On_Activate()
      Case &H5FA      'Deactivate
         If On_Deactivate then On_Deactivate()
      Case &H100      'Resize
         If On_Resize then On_Resize()
      Case &H5FB      'MouseDown
         If On_MouseDown then On_MouseDown(info.EventParameters.Item(0).Value.lVal, info.EventParameters.Item(1).Value.lVal, info.EventParameters.Item(2).Value.lVal, info.EventParameters.Item(3).Value.lVal)
      Case &H5FC      'MouseUp
         If On_MouseUp then On_MouseUp(info.EventParameters.Item(0).Value.lVal, info.EventParameters.Item(1).Value.lVal, info.EventParameters.Item(2).Value.lVal, info.EventParameters.Item(3).Value.lVal)
      Case &H5FD      'MouseMove
         If On_MouseMove then On_MouseMove(info.EventParameters.Item(0).Value.lVal, info.EventParameters.Item(1).Value.lVal, info.EventParameters.Item(2).Value.lVal, info.EventParameters.Item(3).Value.lVal)
      Case &H5FE      'BeforeRightClick
         If On_BeforeRightClick then On_BeforeRightClick(info.EventParameters.Item(0).Value.boolVal)
      Case &H5FF      'DragPlot
         If On_DragPlot then On_DragPlot()
      Case &H600      'DragOver
         If On_DragOver then On_DragOver()
      Case &H601      'BeforeDoubleClick
         If On_BeforeDoubleClick then On_BeforeDoubleClick(info.EventParameters.Item(0).Value.lVal, info.EventParameters.Item(1).Value.lVal, info.EventParameters.Item(2).Value.lVal, info.EventParameters.Item(3).Value.boolVal)
      Case &HEB      'Select
         If On_Select then On_Select(info.EventParameters.Item(0).Value.lVal, info.EventParameters.Item(1).Value.lVal, info.EventParameters.Item(2).Value.lVal)
      Case &H602      'SeriesChange
         If On_SeriesChange then On_SeriesChange(info.EventParameters.Item(0).Value.lVal, info.EventParameters.Item(1).Value.lVal)
      Case &H117      'Calculate
         If On_Calculate then On_Calculate()
   End Select
End Sub
CONSTRUCTOR Chart_ChartEvents()
   IIDFromString(IID_ChartEvents, @Base.EventIID)
END CONSTRUCTOR
Function Chart_ChartEvents.Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   Dim Info as EventInfo
   Info.EventID = dispIdMember
   Info.EventParameters.Count = pDispParams->cArgs
   If pDispParams->cArgs > 0 Then
      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)
      For i As Long = 0 to pDispParams->cArgs -1
         Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]
      Next
   End If
   This.ChartEvents_ObjectEvent(Info)
   Function = 0
End Function
Sub Worksheet_DocEvents.DocEvents_ObjectEvent(ByVal Info As EventInfo)
   Select Case Info.EventID
      Case &H607      'SelectionChange
         If On_SelectionChange then On_SelectionChange(info.EventParameters.Item(0).Value.byref)
      Case &H601      'BeforeDoubleClick
         If On_BeforeDoubleClick then On_BeforeDoubleClick(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.boolVal)
      Case &H5FE      'BeforeRightClick
         If On_BeforeRightClick then On_BeforeRightClick(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.boolVal)
      Case &H130      'Activate
         If On_Activate then On_Activate()
      Case &H5FA      'Deactivate
         If On_Deactivate then On_Deactivate()
      Case &H117      'Calculate
         If On_Calculate then On_Calculate()
      Case &H609      'Change
         If On_Change then On_Change(info.EventParameters.Item(0).Value.byref)
      Case &H5BE      'FollowHyperlink
         If On_FollowHyperlink then On_FollowHyperlink(info.EventParameters.Item(0).Value.byref)
      Case &H86C      'PivotTableUpdate
         If On_PivotTableUpdate then On_PivotTableUpdate(info.EventParameters.Item(0).Value.byref)
   End Select
End Sub
CONSTRUCTOR Worksheet_DocEvents()
   IIDFromString(IID_DocEvents, @Base.EventIID)
END CONSTRUCTOR
Function Worksheet_DocEvents.Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   Dim Info as EventInfo
   Info.EventID = dispIdMember
   Info.EventParameters.Count = pDispParams->cArgs
   If pDispParams->cArgs > 0 Then
      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)
      For i As Long = 0 to pDispParams->cArgs -1
         Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]
      Next
   End If
   This.DocEvents_ObjectEvent(Info)
   Function = 0
End Function
Sub Workbook_WorkbookEvents.WorkbookEvents_ObjectEvent(ByVal Info As EventInfo)
   Select Case Info.EventID
      Case &H783      'Open
         If On_Open then On_Open()
      Case &H130      'Activate
         If On_Activate then On_Activate()
      Case &H5FA      'Deactivate
         If On_Deactivate then On_Deactivate()
      Case &H60A      'BeforeClose
         If On_BeforeClose then On_BeforeClose(info.EventParameters.Item(0).Value.boolVal)
      Case &H60B      'BeforeSave
         If On_BeforeSave then On_BeforeSave(info.EventParameters.Item(0).Value.boolVal, info.EventParameters.Item(1).Value.boolVal)
      Case &H60D      'BeforePrint
         If On_BeforePrint then On_BeforePrint(info.EventParameters.Item(0).Value.boolVal)
      Case &H60E      'NewSheet
         If On_NewSheet then On_NewSheet(info.EventParameters.Item(0).Value.pdispVal)
      Case &H610      'AddinInstall
         If On_AddinInstall then On_AddinInstall()
      Case &H611      'AddinUninstall
         If On_AddinUninstall then On_AddinUninstall()
      Case &H612      'WindowResize
         If On_WindowResize then On_WindowResize(info.EventParameters.Item(0).Value.byref)
      Case &H614      'WindowActivate
         If On_WindowActivate then On_WindowActivate(info.EventParameters.Item(0).Value.byref)
      Case &H615      'WindowDeactivate
         If On_WindowDeactivate then On_WindowDeactivate(info.EventParameters.Item(0).Value.byref)
      Case &H616      'SheetSelectionChange
         If On_SheetSelectionChange then On_SheetSelectionChange(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref)
      Case &H617      'SheetBeforeDoubleClick
         If On_SheetBeforeDoubleClick then On_SheetBeforeDoubleClick(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref, info.EventParameters.Item(2).Value.boolVal)
      Case &H618      'SheetBeforeRightClick
         If On_SheetBeforeRightClick then On_SheetBeforeRightClick(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref, info.EventParameters.Item(2).Value.boolVal)
      Case &H619      'SheetActivate
         If On_SheetActivate then On_SheetActivate(info.EventParameters.Item(0).Value.pdispVal)
      Case &H61A      'SheetDeactivate
         If On_SheetDeactivate then On_SheetDeactivate(info.EventParameters.Item(0).Value.pdispVal)
      Case &H61B      'SheetCalculate
         If On_SheetCalculate then On_SheetCalculate(info.EventParameters.Item(0).Value.pdispVal)
      Case &H61C      'SheetChange
         If On_SheetChange then On_SheetChange(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref)
      Case &H73E      'SheetFollowHyperlink
         If On_SheetFollowHyperlink then On_SheetFollowHyperlink(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref)
      Case &H86D      'SheetPivotTableUpdate
         If On_SheetPivotTableUpdate then On_SheetPivotTableUpdate(info.EventParameters.Item(0).Value.pdispVal, info.EventParameters.Item(1).Value.byref)
      Case &H86E      'PivotTableCloseConnection
         If On_PivotTableCloseConnection then On_PivotTableCloseConnection(info.EventParameters.Item(0).Value.byref)
      Case &H86F      'PivotTableOpenConnection
         If On_PivotTableOpenConnection then On_PivotTableOpenConnection(info.EventParameters.Item(0).Value.byref)
      Case &H8DA      'Sync
         If On_Sync then On_Sync(info.EventParameters.Item(0).Value.byref)
      Case &H8EB      'BeforeXmlImport
         If On_BeforeXmlImport then On_BeforeXmlImport(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.bstrVal, info.EventParameters.Item(2).Value.boolVal, info.EventParameters.Item(3).Value.boolVal)
      Case &H8ED      'AfterXmlImport
         If On_AfterXmlImport then On_AfterXmlImport(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.boolVal, info.EventParameters.Item(2).Value.byref)
      Case &H8EF      'BeforeXmlExport
         If On_BeforeXmlExport then On_BeforeXmlExport(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.bstrVal, info.EventParameters.Item(2).Value.boolVal)
      Case &H8F0      'AfterXmlExport
         If On_AfterXmlExport then On_AfterXmlExport(info.EventParameters.Item(0).Value.byref, info.EventParameters.Item(1).Value.bstrVal, info.EventParameters.Item(2).Value.byref)
      Case &HA32      'RowsetComplete
         If On_RowsetComplete then On_RowsetComplete(info.EventParameters.Item(0).Value.bstrVal, info.EventParameters.Item(1).Value.bstrVal, info.EventParameters.Item(2).Value.boolVal)
   End Select
End Sub
CONSTRUCTOR Workbook_WorkbookEvents()
   IIDFromString(IID_WorkbookEvents, @Base.EventIID)
END CONSTRUCTOR
Function Workbook_WorkbookEvents.Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   Dim Info as EventInfo
   Info.EventID = dispIdMember
   Info.EventParameters.Count = pDispParams->cArgs
   If pDispParams->cArgs > 0 Then
      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)
      For i As Long = 0 to pDispParams->cArgs -1
         Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]
      Next
   End If
   This.WorkbookEvents_ObjectEvent(Info)
   Function = 0
End Function
Sub OLEObject_OLEObjectEvents.OLEObjectEvents_ObjectEvent(ByVal Info As EventInfo)
   Select Case Info.EventID
      Case &H605      'GotFocus
         If On_GotFocus then On_GotFocus()
      Case &H606      'LostFocus
         If On_LostFocus then On_LostFocus()
   End Select
End Sub
CONSTRUCTOR OLEObject_OLEObjectEvents()
   IIDFromString(IID_OLEObjectEvents, @Base.EventIID)
END CONSTRUCTOR
Function OLEObject_OLEObjectEvents.Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
   Dim Info as EventInfo
   Info.EventID = dispIdMember
   Info.EventParameters.Count = pDispParams->cArgs
   If pDispParams->cArgs > 0 Then
      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)
      For i As Long = 0 to pDispParams->cArgs -1
         Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]
      Next
   End If
   This.OLEObjectEvents_ObjectEvent(Info)
   Function = 0
End Function
