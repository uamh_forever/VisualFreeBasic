  '----------------
  'ComClass--------
  Const CLSID_TestControl = "{4C05FF8B-B8C9-402C-AAA3-ECA524117189}"
  'Event--------
  Const IID___TestControl = "{126EB91F-57A2-4F14-A0C6-991D6A972FCD}"
Type TestControl___TestControl Extends CIDispatch
  DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
  DECLARE Sub __TestControl_ObjectEvent(ByVal Info As EventInfo)
  Declare Constructor()
  On_MyClickEvent As SUB()
End Type
'Implements----
Sub TestControl___TestControl.__TestControl_ObjectEvent(ByVal Info As EventInfo)
    Select Case Info.EventID
    Case &H1      'MyClickEvent
      If On_MyClickEvent then On_MyClickEvent()
    End Select
End Sub
CONSTRUCTOR TestControl___TestControl()
    IIDFromString(IID___TestControl, @Base.EventIID)
END CONSTRUCTOR
Function TestControl___TestControl.Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT
  Dim Info as EventInfo
  Info.EventID = dispIdMember
  Info.EventParameters.Count = pDispParams->cArgs
  If pDispParams->cArgs > 0 Then
      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)
      For i As Long = 0 to pDispParams->cArgs -1
          Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]
      Next
  End If
  This.__TestControl_ObjectEvent(Info)
  Function = 0
End Function
