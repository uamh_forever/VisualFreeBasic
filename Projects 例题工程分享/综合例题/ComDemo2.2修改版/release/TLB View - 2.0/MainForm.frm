VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Form1 
   Caption         =   "类生成器"
   ClientHeight    =   4935
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   10440
   LinkTopic       =   "Form1"
   ScaleHeight     =   4935
   ScaleWidth      =   10440
   StartUpPosition =   3  '窗口缺省
   Begin VB.ListBox List2 
      Height          =   4200
      Left            =   5280
      TabIndex        =   3
      Top             =   480
      Width           =   5055
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5400
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command2 
      Caption         =   "导出"
      Height          =   495
      Left            =   1320
      TabIndex        =   2
      Top             =   0
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "选择类库"
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   1215
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   4170
      Left            =   0
      TabIndex        =   0
      Top             =   480
      Width           =   5295
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ObjectName As Object
Attribute ObjectName.VB_VarHelpID = -1
'   VT_EMPTY = 0
'   VT_NULL = 1
'   VT_I2 = 2
'   VT_I4 = 3
'   VT_R4 = 4
'   VT_R8 = 5
'   VT_CY = 6
'   VT_DATE = 7
'   VT_BSTR = 8
'   VT_DISPATCH = 9
'   VT_ERROR = 10
'   VT_BOOL = 11
'   VT_VARIANT = 12
'   VT_UNKNOWN = 13
'   VT_DECIMAL = 14
'   VT_I1 = 16
'   VT_UI1 = 17
'   VT_UI2 = 18
'   VT_UI4 = 19
'   VT_I8 = 20
'   VT_UI8 = 21
'   VT_INT = 22
'   VT_UINT = 23
'   VT_VOID = 24
'   VT_HRESULT = 25
'   VT_PTR = 26
'   VT_SAFEARRAY = 27
'   VT_CARRAY = 28
'   VT_USERDEFINED = 29
'   VT_LPSTR = 30
'   VT_LPWSTR = 31
'   VT_RECORD = 36
'   VT_INT_PTR = 37
'   VT_UINT_PTR = 38
'   VT_FILETIME = 64
'   VT_BLOB = 65
'   VT_STREAM = 66
'   VT_STORAGE = 67
'   VT_STREAMED_OBJECT = 68
'   VT_STORED_OBJECT = 69
'   VT_BLOB_OBJECT = 70
'   VT_CF = 71
'   VT_CLSID = 72
'   VT_VERSIONED_STREAM = 73
'   VT_BSTR_BLOB = &hfff
'   VT_VECTOR = &h1000
'   VT_ARRAY = &h2000
'   VT_BYREF = &h4000
'   VT_RESERVED = &h8000
'   VT_ILLEGAL = &hffff
'   VT_ILLEGALMASKED = &hfff
'   VT_TYPEMASK = &hfff

'    llVal As LONGLONG
'    lVal As Long
'    bVal As UBYTE
'    iVal As Short
'    fltVal As Float
'    dblVal As Double
'    boolVal As VARIANT_BOOL
'    scode As scode
'    cyVal As CY
'    date As DATE_
'    bstrVal As BSTR
'    punkVal as IUnknown ptr
'    pdispVal as IDispatch ptr
'    parray as SAFEARRAY ptr
'    pbVal as UBYTE ptr
'    piVal as SHORT ptr
'    plVal as LONG ptr
'    pllVal as LONGLONG ptr
'    pfltVal as FLOAT ptr
'    pdblVal as DOUBLE ptr
'    pboolVal as VARIANT_BOOL ptr
'    pscode as SCODE ptr
'    pcyVal as CY ptr
'    pdate as DATE_ ptr
'    pbstrVal as BSTR ptr
'    ppunkVal as IUnknown ptr ptr
'    ppdispVal as IDispatch ptr ptr
'    pparray as SAFEARRAY ptr ptr
'    pvarVal as VARIANT ptr
'    byref As PVOID
'    cVal As Char
'    uiVal As USHORT
'    ulVal As ULONG
'    ullVal As ULONGLONG
'    intVal As INT_
'    uintVal As UINT
'    pdecVal as DECIMAL ptr
'    pcVal as CHAR ptr
'    puiVal as USHORT ptr
'    pulVal as ULONG ptr
'    pullVal as ULONGLONG ptr
'    pintVal as INT_ ptr
'    puintVal as UINT ptr
Private Type CallVarType
    TName As String
    CName As String
End Type
Private Function ConvTypeName(vt As Long) As CallVarType
    Select Case vt
    Case 0 '   VT_EMPTY = 0
        ConvTypeName.TName = "Any Ptr"
        ConvTypeName.CName = "byref"
    Case vbNull    '  VT_NULL = 1
        ConvTypeName.TName = "vbNull"
        ConvTypeName.CName = "byref"
    Case vbInteger '  VT_I2 = 2
        ConvTypeName.TName = "Integer"
        ConvTypeName.CName = "iVal"
    Case vbLong    '  VT_I4 = 3
        ConvTypeName.TName = "Long"
        ConvTypeName.CName = "lVal"
    Case vbSingle ' = 4
        ConvTypeName.TName = "Single"
        ConvTypeName.CName = "iVal"
    Case vbDouble ' = 5
        ConvTypeName.TName = "Double"
        ConvTypeName.CName = "dblVal"
    Case vbCurrency ' = 6
        ConvTypeName.TName = "Currency"
        ConvTypeName.CName = "cyVal"
    Case vbDate ' =' 7
        ConvTypeName.TName = "Date"
        ConvTypeName.CName = "date"
    Case vbString ' =' 8
        ConvTypeName.TName = "BSTR"
        ConvTypeName.CName = "bstrVal"
    Case vbObject ' = 9
        ConvTypeName.TName = "IDispatch Ptr"
        ConvTypeName.CName = "pdispVal"
    Case vbError ' = 10
        ConvTypeName.TName = "vbError"
        ConvTypeName.CName = ""
    Case vbBoolean ' = 11
        ConvTypeName.TName = "VARIANT_BOOL"
        ConvTypeName.CName = "boolVal"
    Case vbVariant ' = 12
        ConvTypeName.TName = "Variant Ptr" '再用这个判断 一次
        ConvTypeName.CName = "pvarVal"
    Case vbDataObject ' = 13
        ConvTypeName.TName = "DataObject"
        ConvTypeName.CName = ""
    Case vbDecimal ' = 14
        ConvTypeName.TName = "Decimal"
        ConvTypeName.CName = "pdecVal"
    Case 16   'VT_I1 = 16
        ConvTypeName.TName = "Byte"
        ConvTypeName.CName = "bVal"
    Case vbByte ' = 17
        ConvTypeName.TName = "Byte"
        ConvTypeName.CName = "bVal"
    Case 18    '   VT_UI2 = 18
        ConvTypeName.TName = "UInteger"
        ConvTypeName.CName = "uiVal"
    Case 19    '   VT_UI4 = 19
        ConvTypeName.TName = "ULong"
        ConvTypeName.CName = "ulVal"
    Case 20    '   VT_I8 = 20
        ConvTypeName.TName = "LongLong"
        ConvTypeName.CName = "llVal"
    Case 21    '   VT_UI8 = 21
        ConvTypeName.TName = "ULongLong"
        ConvTypeName.CName = "ullVal"
    'Case 22    '   VT_INT = 22
    Case 23     '   VT_UINT = 23
        ConvTypeName.TName = "ULong"
        ConvTypeName.CName = "ulVal"
    Case 24     '   VT_VOID = 24
        ConvTypeName.TName = "Any Ptr"
        ConvTypeName.CName = "byref"
    Case 25    '   VT_HRESULT = 25
        ConvTypeName.TName = "HRESULT"
        ConvTypeName.CName = "lVal"
    Case 26    '   VT_PTR = 26
        ConvTypeName.TName = "Ptr"
        ConvTypeName.CName = "lVal"
    'Case 27    '   VT_SAFEARRAY = 27
    'Case 28    '   VT_CARRAY = 28
    'Case 29    '   VT_USERDEFINED = 29
    'Case 30    '   VT_LPSTR = 30
    'Case 31    '   VT_LPWSTR = 31
    Case vbUserDefinedType ' = 36
        ConvTypeName.TName = "Any Ptr"
        ConvTypeName.CName = "byref"
    'Case 37    '   VT_INT_PTR = 37
    'Case 38    '   VT_UINT_PTR = 38
    
    'Case 64    '   VT_FILETIME = 64
    'Case 65    '   VT_BLOB = 65
    'Case 66    '   VT_STREAM = 66
    'Case 67    '   VT_STORAGE = 67
    'Case 68    '   VT_STREAMED_OBJECT = 68
    'Case 69    '   VT_STORED_OBJECT = 69
    'Case 70    '   VT_BLOB_OBJECT = 70
    'Case 71    '   VT_CF = 71
    'Case 72    '   VT_CLSID = 72
    'Case 73    '   VT_VERSIONED_STREAM = 73
    'Case &hfff '   VT_BSTR_BLOB = &hfff
    'Case &h1000'   VT_VECTOR = &h1000
    Case vbArray  ' VT_ARRAY = &h2000
        ConvTypeName.TName = "Array"
        ConvTypeName.CName = "byref"
    'Case &h4000'   VT_BYREF = &h4000
    'Case &h8000'   VT_RESERVED = &h8000
    'Case &hffff'   VT_ILLEGAL = &hffff
    'Case &hfff '   VT_ILLEGALMASKED = &hfff
    'Case &hfff '   VT_TYPEMASK = &hfff
    Case Else
        ConvTypeName.TName = "其它类型:" & vt
        ConvTypeName.CName = "byref"
    End Select
End Function


Private Sub Command1_Click()
    Me.CommonDialog1.DialogTitle = "选择DLL EXE TLB文件"
    Me.CommonDialog1.ShowOpen
    If Me.CommonDialog1.FileName = "" Then Exit Sub
    Dim TLI As Object, Tlb As Object, oTLB As Object                                           'ComFileName或ObjectName需要提供一个即可
    Dim TlbClsid As String, Version As String, TlbName As String
    Dim ComFileName As String
    'ComFileName = "D:\VFB\Projects\ComDemo\release\ComTest.dll"
    'ComFileName = "D:\test\TLB\testexe\vb6.exe"
    'Set ObjectName = New Class1
    ComFileName = Me.CommonDialog1.FileName '"C:\Program Files (x86)\Microsoft Office\Office12\excel.exe"
    'Set ObjectName = CreateObject("excel.application")
    If TLI Is Nothing Then Set TLI = CreateObject("TLi.TLIApplication")
    If Not ObjectName Is Nothing Then
        Set oTLB = TLI.InterfaceInfoFromObject(ObjectName)
'        Set Tlb = oTLB.Parent
        ComFileName = Tlb.ContainingFile
    ElseIf ComFileName <> "" Then
        Set Tlb = TLI.TypeLibInfoFromFile(ComFileName)
    Else
        Exit Sub
    End If
    TlbClsid = Tlb.Guid
    TlbName = Tlb.Name
    Version = Tlb.MajorVersion & "." & Tlb.MinorVersion
    Dim comClass, ComClassClsid() As String, Progid() As String, i As Long
    Dim par As String, Callpar As String, param As Object, j As Integer

    Dim mem As Object
    'Me.List1.AddItem "Namespace " & TlbName
    Me.CommonDialog1.FileName = App.Path & "\" & TlbName & ".inc"
    For Each comClass In Tlb.CoClasses
        ReDim Preserve ComClassClsid(i)
        ReDim Preserve Progid(i)
        'ComClassClsid(i) = comClass.Guid
        'Progid(i) = TlbName & "." & comClass.Name
        Me.List1.AddItem "  '----------------"
        Me.List1.AddItem "  'ComClass--------"
        Me.List1.AddItem "  Const CLSID_" & comClass.Name & " = """ & comClass.Guid & """"
        
        If Not comClass.DefaultEventInterface Is Nothing Then
            Me.List1.AddItem "  'Event--------"
            Dim l As Long, IsFind As Boolean
            For l = 0 To Me.List1.ListCount - 1
                If Me.List1.List(l) = "  Const IID_" & comClass.DefaultEventInterface.Name & " = """ & comClass.DefaultEventInterface.Guid & """" Then
                    IsFind = True
                    Exit For
                Else
                    IsFind = False
                End If
            Next
            Me.List1.AddItem "  " & IIf(IsFind, "'", "") & "Const IID_" & comClass.DefaultEventInterface.Name & " = """ & comClass.DefaultEventInterface.Guid & """"
            IsFind = False
            If comClass.DefaultEventInterface.ImpliedInterfaces.Count > 0 Then
                If comClass.DefaultEventInterface.ImpliedInterfaces(1).Name = "IUnknown" Then
                    Me.List1.AddItem "  Type " & comClass.Name & "_" & comClass.DefaultEventInterface.Name & " Extends CIUnknown"
                    For Each mem In comClass.DefaultEventInterface.Members
                        If mem.Name <> "QueryInterface" And _
                            mem.Name <> "AddRef" And _
                            mem.Name <> "Release" And _
                            mem.Name <> "GetTypeInfoCount" And _
                            mem.Name <> "GetTypeInfo" And _
                            mem.Name <> "GetIDsOfNames" And _
                            mem.Name <> "Invoke" Then
                            'Me.List1.AddItem "    Case &H" & Hex(mem.MemberId) & "      '" & mem.Name
                            par = ""
                            Callpar = ""
                            j = 0
                            If mem.Parameters.Count > 0 Then
                                For Each param In mem.Parameters
                                    If param.Optional = True Then
                                        par = par & IIf(param.VarTypeInfo.PointerLevel = 0, "ByVal ", "ByRef ") & "Optional " & param.Name & " As " & ConvTypeName(param.VarTypeInfo.VarType).TName & ","
                                        Callpar = Callpar & param.Name & ","
                                    Else
                                        par = par & IIf(param.VarTypeInfo.PointerLevel = 0, "ByVal ", "ByRef ") & param.Name & " As " & ConvTypeName(param.VarTypeInfo.VarType).TName & ","
                                        Callpar = Callpar & param.Name & ","
                                    End If
                                    j = j + 1
                                Next
                                Callpar = Left(Callpar, Len(Callpar) - 1)
                                par = Left(par, Len(par) - 1)
                            End If
                            If mem.ReturnType = &H18 Then
                                Me.List1.AddItem "      Declare Virtual Sub On_" & mem.Name & "_ObjectEvent(" & par & ")"
                                Me.List2.AddItem "Sub " & comClass.Name & "_" & comClass.DefaultEventInterface.Name & ".On_" & mem.Name & "_ObjectEvent(" & par & ")"
                                Me.List1.AddItem "      On_" & mem.Name & " As SUB(" & par & ")"
                                Me.List2.AddItem "      If On_" & mem.Name & "Then On_" & mem.Name & "(" & Callpar & ")"
                                Me.List2.AddItem "End Sub"
                            Else
                                Me.List1.AddItem "      Declare Virtual Function On_" & mem.Name & "_ObjectEvent(" & par & ") As " & ConvTypeName(mem.ReturnType).TName
                                Me.List2.AddItem "Function " & comClass.Name & "_" & comClass.DefaultEventInterface.Name & ".On_" & mem.Name & "_ObjectEvent(" & par & ") As " & ConvTypeName(mem.ReturnType).TName
                                Me.List1.AddItem "      On_" & mem.Name & " As FUNCTION(" & par & ") As " & ConvTypeName(mem.ReturnType).TName
                                Me.List2.AddItem "      Return On_" & mem.Name & "(" & Callpar & ")"
                                
                                Me.List2.AddItem "End Function"
                            End If
                        End If
                        
                    Next
                    Me.List1.AddItem "     DECLARE CONSTRUCTOR()"
                    Me.List1.AddItem "  End Type"
                    Me.List2.AddItem "CONSTRUCTOR " & comClass.Name & "_" & comClass.DefaultEventInterface.Name & "()"
                    Me.List2.AddItem "    IIDFromString(IID_" & comClass.DefaultEventInterface.Name & ", @Base.EventIID)"
                    Me.List2.AddItem "END CONSTRUCTOR"
                Else
                    Me.List1.AddItem "Type " & comClass.Name & "_" & comClass.DefaultEventInterface.Name & " Extends CIDispatch"
                    Me.List1.AddItem "  DECLARE Function Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT"
                    Me.List1.AddItem "  DECLARE Sub " & comClass.DefaultEventInterface.Name & "_ObjectEvent(ByVal Info As EventInfo)"
                    Me.List1.AddItem "  Declare Constructor()"
                    
                    Me.List2.AddItem "Sub " & comClass.Name & "_" & comClass.DefaultEventInterface.Name & "." & comClass.DefaultEventInterface.Name & "_ObjectEvent(ByVal Info As EventInfo)"
                    Me.List2.AddItem "    Select Case Info.EventID"
                    For Each mem In comClass.DefaultEventInterface.Members
                
                        If mem.Name <> "QueryInterface" And _
                            mem.Name <> "AddRef" And _
                            mem.Name <> "Release" And _
                            mem.Name <> "GetTypeInfoCount" And _
                            mem.Name <> "GetTypeInfo" And _
                            mem.Name <> "GetIDsOfNames" And _
                            mem.Name <> "Invoke" Then
                            Me.List2.AddItem "    Case &H" & Hex(mem.MemberId) & "      '" & mem.Name
                            par = ""
                            Callpar = ""
                            j = 0
                            If mem.Parameters.Count > 0 Then
                                For Each param In mem.Parameters
                                    If param.Optional = True Then
                                        par = par & IIf(param.VarTypeInfo.PointerLevel = 0, "ByVal ", "ByRef ") & "Optional " & param.Name & " As " & ConvTypeName(param.VarTypeInfo.VarType).TName & ","
                                        Callpar = Callpar & "info.EventParameters.Item(" & j & ").Value." & ConvTypeName(param.VarTypeInfo.VarType).CName & ","
                                    Else
                                        par = par & IIf(param.VarTypeInfo.PointerLevel = 0, "ByVal ", "ByRef ") & param.Name & " As " & ConvTypeName(param.VarTypeInfo.VarType).TName & ","
                                        Callpar = Callpar & "info.EventParameters.Item(" & j & ").Value." & ConvTypeName(param.VarTypeInfo.VarType).CName & ","
                                    End If
                                    j = j + 1
                                Next
                                Callpar = Left(Callpar, Len(Callpar) - 1)
                                par = Left(par, Len(par) - 1)
                            End If
                            If mem.ReturnType = &H18 Then
                                Me.List1.AddItem "  On_" & mem.Name & " As SUB(" & par & ")"
                                Me.List2.AddItem "      If On_" & mem.Name & " then On_" & mem.Name & "(" & Callpar & ")"
                            Else
                                Me.List1.AddItem "  On_" & mem.Name & " As FUNCTION(" & par & ") As " & ConvTypeName(mem.ReturnType).TName
                                Me.List2.AddItem "      If On_" & mem.Name & " then On_" & mem.Name & "(" & Callpar & ")"
                            End If
                        End If
                        
                    Next
                    Me.List1.AddItem "End Type"
                    Me.List2.AddItem "    End Select"
                    Me.List2.AddItem "End Sub"
                    Me.List2.AddItem "CONSTRUCTOR " & comClass.Name & "_" & comClass.DefaultEventInterface.Name & "()"
                    Me.List2.AddItem "    IIDFromString(IID_" & comClass.DefaultEventInterface.Name & ", @Base.EventIID)"
                    Me.List2.AddItem "END CONSTRUCTOR"
                    Me.List2.AddItem "Function " & comClass.Name & "_" & comClass.DefaultEventInterface.Name & ".Invoke(ByVal dispIdMember As DISPID, ByVal riid As Const IID Const Ptr, ByVal lcid As LCID, ByVal wFlags As WORD, ByVal pDispParams As DISPPARAMS Ptr, ByVal pVarResult As VARIANT Ptr, ByVal pExcepInfo As EXCEPINFO Ptr, ByVal puArgErr As UINT Ptr) As HRESULT"
                    Me.List2.AddItem "  Dim Info as EventInfo"
                    Me.List2.AddItem "  Info.EventID = dispIdMember"
                    Me.List2.AddItem "  Info.EventParameters.Count = pDispParams->cArgs"
                    Me.List2.AddItem "  If pDispParams->cArgs > 0 Then"
                    Me.List2.AddItem "      ReDim Info.EventParameters.Item(pDispParams->cArgs -1)"
                    Me.List2.AddItem "      For i As Long = 0 to pDispParams->cArgs -1"
                    Me.List2.AddItem "          Info.EventParameters.Item(i).Value = pDispParams->rgvarg[i]"
                    Me.List2.AddItem "      Next"
                    Me.List2.AddItem "  End If"
                    Me.List2.AddItem "  This." & comClass.DefaultEventInterface.Name & "_ObjectEvent(Info)"
                    Me.List2.AddItem "  Function = 0"
                    Me.List2.AddItem "End Function"
                End If
            End If
        End If
        
'        Me.List1.AddItem "'Interface--------"
'        Me.List1.AddItem "  Const IID" & comClass.DefaultInterface.Name & " = """ & comClass.DefaultInterface.Guid & """"
'        For Each mem In comClass.DefaultInterface.Members '
'            If mem.Name <> "QueryInterface" And _
'                mem.Name <> "AddRef" And _
'                mem.Name <> "Release" And _
'                mem.Name <> "GetTypeInfoCount" And _
'                mem.Name <> "GetTypeInfo" And _
'                mem.Name <> "GetIDsOfNames" And _
'                mem.Name <> "Invoke" Then
'                Me.List1.AddItem "  Const " & comClass.Name & "_" & mem.Name & " = &H" & Hex(mem.MemberId)
'            End If
'        Next
'        Me.List1.AddItem "'--------"
        i = i + 1
    Next
    'Me.List1.AddItem "End Namespace"
    'ObjectName.quit
End Sub

Private Sub Command2_Click()
    Me.CommonDialog1.ShowSave
    If Me.CommonDialog1.FileName = "" Then Exit Sub
    
    Dim l As Long
    Open Me.CommonDialog1.FileName For Append As #1
    For l = 0 To Me.List1.ListCount - 1
        Print #1, Me.List1.List(l)
    Next
    Print #1, "'Implements----"
    For l = 0 To Me.List2.ListCount - 1
        Print #1, Me.List2.List(l)
    Next
    Close #1
    
'    Dim o As Object
'    Set o = GetObject(, "autocad.application")
'    Dim po(2) As Double
'    po(0) = 0
'    po(1) = 0
'    po(2) = 0
'    o.ActiveDocument.ModelSpace.AddCircle po, 5000
End Sub

Private Sub Form_Resize()
    Me.List1.Width = Me.Width / 2
    If Me.Height - Me.List1.Top > 0 Then Me.List1.Height = Me.Height - Me.List1.Top
    Me.List2.Left = Me.Width / 2
    If Me.Height - Me.List1.Top > 0 Then Me.List2.Height = Me.Height - Me.List1.Top
    Me.List2.Width = Me.Width / 2
End Sub
