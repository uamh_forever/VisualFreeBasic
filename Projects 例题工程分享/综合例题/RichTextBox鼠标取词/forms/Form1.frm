﻿#VisualFreeBasic_Form#  Version=5.0.0
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
Style=3 - 常规窗口
Icon=
Caption=Form1
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Left=0
Top=0
Width=624
Height=301
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[RichEdit]
Name=RichEdit1
Index=-1
Style=3 - 凹边框
TextScrollBars=3 - 垂直和水平
Text=RichEdit1
Enabled=True
Visible=True
MaxLength=0
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Locked=False
HideSelection=False
Multiline=True
SaveSel=True
NoScroll=False
ContextMenu=True
LeftMargin=0
RightMargin=0
Left=11
Top=11
Width=412
Height=246
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=鼠标位置的单词：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=439
Top=40
Width=143
Height=28
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=Label2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=435
Top=123
Width=157
Height=30
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=Label3
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=437
Top=82
Width=159
Height=25
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]

'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。

  Me.RichEdit1.Text = "Welcome to vbget" & vbCrLf & vbCrLf & _
  "This example program is provided as is with no wa rranty of any kind" & vbCrLf & vbCrLf & _
  "Send the control the EM_CHARFROMPOS message to make it return the character closest to the mouse position." & vbCrLf & vbCrLf & _
  "http://www.yfvb.com"

End Sub
Function RichWordOver(rtf As Class_RichEdit, X As Single, Y As Single) As String
  Dim pt As Point
  Dim nPos As Integer
  Dim start_pos As Integer
  Dim end_pos As Integer
  Dim ch As String
  Dim txt As String
  Dim txtlen As Integer
  '把位置坐标转换为像素.
  pt.X = X 
  pt.Y = Y 
  'Get the character number
  nPos = SendMessage(rtf.hWnd, EM_CHARFROMPOS, 0 , @pt)
  If nPos <= 0 Then Exit Function
  '查找单词的开始位置.
  txt = rtf.Text
  For start_pos = nPos To 1 Step -1
      ch = Mid(rtf.Text, start_pos, 1)
      '允许数字,字母,下划线
      If Not ( _
        (ch >= "0" And ch <= "9") Or _
        (ch >= "a" And ch <= "z") Or _
        (ch >= "A" And ch <= "Z") Or _
        ch = "_" _
        ) Then Exit For
      Next start_pos
  start_pos = start_pos + 1
  '查找单词的结尾
  txtlen = Len(txt)
  For end_pos = nPos To txtlen
      ch = Mid(txt, end_pos, 1)
      '允许数字,字母,下划线
      If Not ( _
        (ch >= "0" And ch <= "9") Or _
        (ch >= "a" And ch <= "z") Or _
        (ch >= "A" And ch <= "Z") Or _
        ch = "_" _
        ) Then Exit For
      Next end_pos
  end_pos = end_pos - 1
  If start_pos <= end_pos Then RichWordOver = Mid(txt, start_pos, end_pos - start_pos + 1)
End Function


'                                                                                  
Sub Form1_RichEdit1_WM_MouseMove(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '移动鼠标

  Dim txt As String
  Me.Label3.Caption = "x=" & xPos & ",y=" & yPos 
  txt = RichWordOver(RichEdit1, xPos, yPos)
    If Me.Label2.Caption <> txt Then   Label2.Caption = txt

End Sub

