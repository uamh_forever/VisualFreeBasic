﻿#VisualFreeBasic_Form#  Version=5.0.0
Locked=1

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
Style=3 - 常规窗口
Icon=
Caption=ADO测试
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Left=0
Top=0
Width=500
Height=483
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=记录集
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=195
Top=6
Width=96
Height=33
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[ListBox]
Name=List1
Index=-1
Style=0 - 单选
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=5
Top=8
Width=182
Height=431
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command2
Index=-1
Caption=Command对象
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=197
Top=45
Width=94
Height=30
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=写文本
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=396
Top=8
Width=74
Height=24
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=Sort属性
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=198
Top=82
Width=92
Height=25
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=页
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=197
Top=117
Width=94
Height=27
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=其它例题，看【CADODB全部例题.rar】，全部使用方法。
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=214
Top=178
Width=225
Height=51
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]

#include "Afx/CADODB/CADODB.inc"



                          
'--------------------------------------------------------------------------------
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  ' // 打开连接
  Dim pConnection As CAdoConnection Ptr = NEW CAdoConnection
  pConnection->Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=青海全省.mdb"
  
  ' // 打开记录集
  Dim pRecordset As CAdoRecordset
  ' // 将光标位置设置到客户端对于获取断开的记录集非常重要
  pRecordset.CursorLocation = adUseClient
  ' // 打开记录集
  Dim cvSource As CVAR = "SELECT * FROM bus"
  pRecordset.Open(cvSource, pConnection, adOpenKeyset, adLockOptimistic, adCmdText)
  
  ' // 通过将其活动连接设置为空来断开记录集。
  ' // 投射到Afx_ADOConnection PTR需要获得正确的重载方法;
  ' // 否则，CVAR版本将被调用并且会失败。
  pRecordset.ActiveConnection = Cast(Afx_ADOConnection Ptr, Null)
  
  ' // 关闭并释放连接
  Delete pConnection

  list1.Clear() 
  ' // 解析记录集
  Do
      ' // 虽然不在记录集的末尾...
      If pRecordset.EOF Then Exit Do
      ' // 获取“返程”列的内容
      '   DIM cvRes AS CVARIANT = pRecordset.Collect("Author")
      '   PRINT cvRes
      dim ss as String = pRecordset.Collect("返程")
      list1.AddItem(ss ) 
      ' // 取下一行
      If pRecordset.MoveNext <> S_OK Then Exit Do
  Loop

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   
   '// 打开连接
   Dim pConnection As CAdoConnection
   pConnection.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=青海全省.mdb"
   
   '// 创建一个Command对象
   Dim pCommand As CAdoCommand
   
   '// 设置活动连接
   pCommand.ActiveConnection = pConnection
   
   '// 设置CommandText属性
   pCommand.CommandText = "SELECT TOP 20 * FROM bus ORDER BY 返程"
   
   '// 通过执行查询并附加来创建记录集
   '// 将结果记录集转换为CAdoRecordset类的一个实例。
   Dim pRecordset As CAdoRecordset = pCommand.Execute

   list1.Clear() 
   '// 解析记录集
   Do
      '// While 不在记录集的末尾...
      If pRecordset.EOF Then Exit Do
      '//获取“返程”列的内容
      'DIM cvRes AS CVAR = pRecordset.Collect("Author")
      'PRINT cvRes
      dim ss as String = pRecordset.Collect("返程")
      list1.AddItem(ss) 
      '// 取下一行
      If pRecordset.MoveNext <> S_OK Then Exit Do
   Loop
   
End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  ' // 在内存中打开一个流
  Dim pStream As CAdoStream
  pStream.Type_ = adTypeText
  pStream.LineSeparator = adCRLF
  pStream.Open
  
  ' // 写一些文字给它
  pStream.WriteText "This is a test string", adWriteLine
  pStream.WriteText "This is another test string", adWriteLine
  
  ' // 将位置设置在文件的开头
  pStream.Position = 0
  ' // Read the lines
  Dim cbsText As CBSTR = pStream.ReadText(adReadLine)
  Print cbsText
  cbsText = pStream.ReadText(adReadLine)
  Print cbsText
  
  ' // 将内容保存到文件
  pStream.SaveToFile App.Path & "TestStream.txt", adSaveCreateOverWrite
  pStream.Close
  AfxMsg "保存文件完成" & vbCrLf & App.Path & "TestStream.txt"

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   
   '// 打开连接
   Dim pConnection As CAdoConnection
   pConnection.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=青海全省.mdb"
   
   '// 设置光标位置
   Dim pRecordset As CAdoRecordset
   pRecordset.CursorLocation = adUseClient
   
   '// 打开记录集
   Dim cvSource As CVAR = "bus"
   pRecordset.Open(cvSource, pConnection, adOpenKeyset, adLockOptimistic, adCmdTableDirect)
   list1.Clear() 
   '// 设置Sort属性
   pRecordset.Sort = "省 ASC, 城市 ASC"
   
   '// 解析记录集
   Do
      '// While 不在记录集的末尾...
      If pRecordset.EOF Then Exit Do
      '// 获取“城市”和“名称”列的内容
      Dim cvRes1 As CVAR = pRecordset.Collect("省")
      Dim cvRes2 As CVAR = pRecordset.Collect("城市")
      list1.AddItem(cvRes1.ToStr & " " & cvRes2.ToStr) 
      '// 取下一行
      If pRecordset.MoveNext <> S_OK Then Exit Do
   Loop
   
End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  ' // 打开连接
  Dim pConnection As CAdoConnection
  pConnection.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=青海全省.mdb"
  
  ' // 设置光标位置
  Dim pRecordset As CAdoRecordset
  pRecordset.CursorLocation = adUseClient
  
  ' // 打开记录集
  Dim cvSource As CVAR = "SELECT * FROM bus"
  pRecordset.Open(cvSource, pConnection, adOpenKeyset, adLockOptimistic, adCmdText)
  
  ' // 一次显示五条记录
  Dim nPageSize As Long = 5
  pRecordset.PageSize = nPageSize
  ' // 检索页数
  Dim nPageCount As Long = pRecordset.PageCount
  list1.Clear() 
  ' // 解析记录集
  For i As Long = 1 To nPageCount
      ' // 将光标设置在页面的开头
      pRecordset.AbsolutePage = i
      For x As Long = 1 To nPageSize
          ' // 获取“名称”列的内容
          Dim ss As String  = pRecordset.Collect("城市")
          
          list1.AddItem(ss) 
          ' // 取下一行
          pRecordset.MoveNext
          If pRecordset.EOF Then Exit For
      Next
  Next

End Sub

