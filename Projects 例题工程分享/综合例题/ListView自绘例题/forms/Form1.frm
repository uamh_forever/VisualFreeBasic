﻿#VisualFreeBasic_Form#  Version=5.0.0
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_DLGFRAME, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
Style=3 - 常规窗口
Icon=
Caption=ListView自绘
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Left=0
Top=0
Width=618
Height=424
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListView]
Name=ListView1
Index=-1
Style=1 - 报表视图
BStyle=3 - 凹边框
SingleSel=False
ShowSel=True
Sort=0 - 不排序
NoScroll=False
AutoArrange=False
EditLabels=False
OwnerData=False
AlignTop=False
AlignLeft=False
OwnerDraw=False
ColumnHeader=True
NoSortHeader=False
BorderSelect=False
Check=False
FlatScrollBars=False
FullRowSelect=True
GridLines=True
HeaderDragDrop=False
InfoTip=False
LabelTip=False
MultiWorkAreas=False
OneClickActivate=False
Regional=False
SimpleSelect=False
SubItemImages=False
TrackSelect=False
TwoClickActivate=False
UnderlineCold=False
UnderlineHot=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=14
Top=17
Width=579
Height=352
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]

Type LISTDATA
    As ZString * 16    zsIndex
    As ZString * 16    zsName
    As DWord                dwHP
    As DWord                dwMaxHP
    As DWord                dwMP
    As DWord                dwMaxMP
End Type

Dim Shared As LISTDATA g_RoleList(9)
Dim Shared As HBRUSH hBrush_LV_Back    '常规背景色
Dim Shared As HBRUSH hBrush_LV_Hig    '高亮背景色
Dim Shared As HBRUSH hBrush_HP
Dim Shared As HBRUSH hBrush_MP
Dim Shared As HBRUSH hBrush_Back
'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。

  '创建必要的刷子来画画
  hBrush_HP = CreateSolidBrush(BGR(255, 96, 96))
  hBrush_MP = CreateSolidBrush(BGR(0, 162, 232))
  hBrush_Back = CreateSolidBrush(BGR(255, 255, 255))
  hBrush_LV_Hig = CreateSolidBrush(BGR(153, 217, 234))
  '增加列表头，VFB支持2种方式，函数和类
  'FF_ListView_InsertColumn(HWND_FORM1_LISTVIEW1, 0, "序", , 100)
  'FF_ListView_InsertColumn(HWND_FORM1_LISTVIEW1, 1, "姓名", LVCFMT_CENTER, 100)
  'FF_ListView_InsertColumn(HWND_FORM1_LISTVIEW1, 2, "进度", LVCFMT_CENTER, 150)
  'FF_ListView_InsertColumn(HWND_FORM1_LISTVIEW1, 3, "MP", LVCFMT_CENTER, 150)
  Form1.ListView1.AddColumn("序", LVCFMT_LEFT, 100)
  Form1.ListView1.AddColumn("姓名", LVCFMT_CENTER, 100)
  Form1.ListView1.AddColumn("进度", LVCFMT_CENTER, 150)
  Form1.ListView1.AddColumn("MP", LVCFMT_CENTER, 150)
  
  '以LV控件背景创建刷子
  hBrush_LV_Back = CreateSolidBrush(SendMessage(LISTVIEW1.hwnd , LVM_GETBKCOLOR, 0, 0))
  '予分配内存，加速加载
  form1.ListView1 .AddItemCount (9)
  '新增
  For i As Integer = 0 To 9
      g_RoleList(i).zsIndex = Str(i)
      g_RoleList(i).zsName = "Name" + Str(i)
      g_RoleList(i).dwHP = (i + 1) * 10
      g_RoleList(i).dwMaxHP = 100
      g_RoleList(i).dwMP = (i + 1) * 10
      g_RoleList(i).dwMaxMP = 140
      form1.ListView1.AddItem(Str(i + 1))
  Next

End Sub


'--------------------------------------------------------------------------------
Function Form1_Custom(hWndForm As hWnd, wMsg As UInteger, wParam As wParam, lParam As lParam) As LResult  '自定义消息（全部消息），在其它事件后处理，返回非0，终止系统处理此消息。

  Dim lplvcd As NMLVCUSTOMDRAW Ptr
  Select Case wMsg
      Case WM_NOTIFY
          lplvcd = Cast(NMLVCUSTOMDRAW Ptr, lParam)
          
          If lplvcd->nmcd.hdr.code = NM_CUSTOMDRAW And lplvcd->nmcd.hdr.hwndFrom = LISTVIEW1.hwnd Then
              'Print lplvcd->nmcd.dwDrawStage,CDDS_ITEMPREPAINT,CDDS_SUBITEM,CDDS_PREPAINT
              Select Case lplvcd->nmcd.dwDrawStage
                  Case CDDS_PREPAINT
                      Return CDRF_NOTIFYITEMDRAW
                  Case CDDS_ITEMPREPAINT
                      Return CDRF_NOTIFYSUBITEMDRAW
                  Case CDDS_ITEMPREPAINT Or CDDS_SUBITEM '绘制单元格
                      Dim As Integer iRow = lplvcd->nmcd.dwItemSpec
                      Dim As Integer iCol = lplvcd->iSubItem
                      Dim As ZString * 64 sText
                      Dim As Integer nState
                      Dim As RECT REC
                      GetRc(iRow, iCol, @REC, LISTVIEW1.hwnd)
                      
                      '选中状态
                      nState = SendMessage(lplvcd->nmcd.hdr.hwndFrom, LVM_GETITEMSTATE, iRow, LVIS_CUT Or LVIS_SELECTED Or LVIS_FOCUSED)
                      
                      If (nState And LVIS_SELECTED) <> 0 Then
                          '选中
                          FillRect(lplvcd->nmcd.hDC, @REC, hBrush_LV_Hig)
                      Else
                          FillRect(lplvcd->nmcd.hDC, @REC, hBrush_LV_Back)
                      EndIf
                      
                      Select Case lplvcd->iSubItem
                          Case 2
                              Dim As RECT tmpREC = REC
                              
                              InflateRect(@tmpREC, -1, -1)
                              '绘制边框
                              FrameRect(lplvcd->nmcd.hDC, @tmpREC, hBrush_HP)
                              
                              '绘制底部
                              InflateRect(@tmpREC, -1, -1)
                              FillRect(lplvcd->nmcd.hDC, @tmpREC, hBrush_Back)
                              
                              'If g_RoleList(iRow).dwHP < g_RoleList(iRow).dwMaxHP Then
                              tmpREC.Right = tmpREC.Left + (g_RoleList(iRow).dwHP / g_RoleList(iRow).dwMaxHP) * (tmpREC.Right - tmpREC.Left)
                              'EndIf
                              
                              sText = Str(g_RoleList(iRow).dwHP) + "/" + Str(g_RoleList(iRow).dwMaxHP)
                              '绘制进度条
                              FillRect(lplvcd->nmcd.hDC, @tmpREC, hBrush_HP)
                              
                          Case 3
                              Dim As RECT tmpREC = REC
                              
                              '缩小矩形
                              InflateRect(@tmpREC, -1, -1)
                              '绘制边框
                              FrameRect(lplvcd->nmcd.hDC, @tmpREC, hBrush_MP)
                              
                              '缩小矩形
                              InflateRect(@tmpREC, -1, -1)
                              '绘制底部
                              FillRect(lplvcd->nmcd.hDC, @tmpREC, hBrush_Back)
                              
                              tmpREC.Right = tmpREC.Left + (g_RoleList(iRow).dwMP / g_RoleList(iRow).dwMaxMP) * (tmpREC.Right - tmpREC.Left)
                              sText = Str(g_RoleList(iRow).dwMP) + "/" + Str(g_RoleList(iRow).dwMaxMP)
                              '绘制进度条
                              FillRect(lplvcd->nmcd.hDC, @tmpREC, hBrush_MP)
                              
                          Case 0
                              sText = g_RoleList(iRow).zsIndex
                              
                          Case 1
                              sText = g_RoleList(iRow).zsName
                              
                      End Select
                      '设置字体颜色
                      'SetTextColor(lplvcd->nmcd.hdc, 0)
                      '绘制文字
                      DrawText(lplvcd->nmcd.hDC, sText, -1, @REC, DT_VCENTER Or DT_CENTER Or DT_SINGLELINE)
                      
                      Return CDRF_SKIPDEFAULT
                      
                  Case Else
                      Return CDRF_DODEFAULT
              End Select
              
          EndIf
          
  End Select
  
End Function

Sub GetRc(iRow As Integer, iCol As Integer, pRc As RECT Ptr, nhwnd As hWnd)
  Dim As Integer iColBegin
  For i As Integer = 0 To iCol - 1
      iColBegin += ListView_GetColumnWidth(nhwnd, i)
  Next
  
  SendMessage(nhwnd, LVM_GETSUBITEMRECT, iRow, Cast(lParam, pRc))
  pRc->Left = iColBegin - GetScrollPos(nhwnd, SB_HORZ)
  pRc->Right = ListView_GetColumnWidth(nhwnd, iCol) + pRc->Left
End Sub


'--------------------------------------------------------------------------------
Function Form1_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭

  DeleteObject(hBrush_HP)
  DeleteObject(hBrush_MP)
  DeleteObject(hBrush_Back)
  DeleteObject(hBrush_LV_Hig)
  DeleteObject(hBrush_LV_Back)
  Function = 0   ' 根据你的需要改变
End Function



'                                                                                  
Sub Form1_ListView1_WM_LButtonUp(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '释放鼠标左键

                 Print xpos,ypos 

End Sub

'                                                                                  
Function Form1_ListView1_Custom(hWndForm As hWnd, hWndControl As hWnd, wMsg As UInteger, wParam As wParam, lParam As lParam) As LResult  '自定义消息（全部消息），在其它事件后处理，返回非0，终止系统处理此消息。

   If wmsg=WM_LButtonUp Then
         Print WM_LButtonUp
   End If
   Function = 0   ' 根据你的需要改变
End Function

