﻿#VisualFreeBasic_Form#  Version=5.0.0
Locked=0

[Form]
Name=Form2
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
Style=3 - 常规窗口
Icon=
Caption=Form2
Enabled=True
Visible=True
StartPosition=1 - 屏幕中心
Left=0
Top=0
Width=499
Height=309
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
WindowState=0 - 正常
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=Text1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=False
Multiline=False
Uppercase=False
Lowercase=False
Number=False
Left=32
Top=16
Width=284
Height=21
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=多个窗口2控件操作
Enabled=True
Visible=True
Font=宋体,9,0
Left=32
Top=48
Width=199
Height=33
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=设Form1为父窗口
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=64
Top=104
Width=198
Height=31
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]



'--------------------------------------------------------------------------------
Sub Form2_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  '注意！！！这是个多开窗口，使用控件方法必须多此一步
  '类方式 ==================================================================
  Me.Text1.hWndForm = hWndForm
  Me.Text1.Text ="多开同个窗口，必须用此"
  '函数方式 ===================================================================
'  FF_Control_SetText   GetDlgItem(hWndForm, IDC_FORM2_TEXT1), "多开同个窗口，用 IDC_  控件句柄：" & GetDlgItem(hWndForm, IDC_FORM2_TEXT1)

End Sub


'                                                                                  
Sub Form2_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Me.hWndForm  = hWndForm
  Me.hWndParent= form1.hWnd 


End Sub


