﻿#VisualFreeBasic_Form#  Version=5.2.8
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
ClassName=
WinStyle=WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_BORDER,WS_CAPTION,WS_THICKFRAME,WS_SYSMENU,WS_MAXIMIZEBOX,WS_MINIMIZEBOX,WS_VISIBLE,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=home.ico
Caption=Form1
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=604
Height=368
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=300
MinHeight=300
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListBox]
Name=List1
Index=-1
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
Left=16
Top=32
Width=116
Height=133
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=加载List
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=宋体,9,0
Left=32
Top=168
Width=75
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=不需要DOS窗口（调试输出用）在工程属性里设置
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=宋体,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=24
Top=8
Width=317
Height=13
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=显示窗口2（普通）
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=宋体,9,0
Left=33
Top=206
Width=119
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command3
Index=-1
Caption=显示窗口2（模态）
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=宋体,9,0
Left=28
Top=243
Width=119
Height=27
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=窗口更名
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=宋体,9,0
Left=144
Top=32
Width=100
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=全局变量
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=宋体,9,0
Left=144
Top=72
Width=92
Height=31
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command7
Index=-1
Caption=弹出菜单
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=宋体,9,0
Left=144
Top=112
Width=85
Height=31
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command8
Index=-1
Caption=写代码随窗口
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=宋体,9,0
Left=432
Top=16
Width=102
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command9
Index=-1
Caption=改变list 行间距
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=宋体,9,0
Left=112
Top=168
Width=98
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[ListBox]
Name=List2
Index=-1
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=1 - 固定行高自绘
ItemHeight=15
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
Left=272
Top=72
Width=300
Height=107
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=这是自绘List
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=宋体,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=272
Top=56
Width=163
Height=14
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Picture]
Name=Picture1
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
Left=166
Top=204
Width=164
Height=72
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Timer]
Name=Timer1
Index=-1
Interval=0
Enabled=True
Left=341
Top=203
Tag=

[PopupMenu]
Name=PopupMenu1
Index=-1
Menu=
Left=424
Top=208
Tag=

[StatusBar]
Name=Status1
Status=
SizeGrip=True
ToolTips=True
Enabled=True
Visible=True
Font=微软雅黑,9,0
BackColor=SYS,15
MinHeight=0
Tag=
Tab=True

[TopMenu]
Name=TopMenu1
Menu=
Tag=

[Shape]
Name=Shape1
Index=-1
Style=2 - 圆角矩形
FillStyle=0 - 无填充
BorderWidth=1
BorderColor=SYS,8
FillColor=&HFF000000
Enabled=True
Visible=True
Left=503
Top=196
Width=59
Height=61
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]

Dim Shared 全局变量 As Long  '加个 shared 为全局变量
Dim Shared yMenu1 As HMENU '菜单
Dim Shared yMenu2 As HMENU '菜单
'--------------------------------------------------------------------------------
Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim As Long i, a
  Dim bb As String
  
  '类方式 ==================================================================
  List1.Clear      '   删除列表框中所有项目
  For i = 1 To 10
      bb = Str(Rnd)
      Print bb    '调试输出，只是看看 在DOS窗口显示，如果不要DOS，在工程属性里设置
      a = List1.AddItem(bb)    '将字符串添加到列表框控件
      List1.ItemData(a) = i  '设置与列表框中指定项目相关联的 32 位值。
  Next
  
  '函数方式 ===================================================================
'  FF_ListBox_ResetContent  HWND_FORM1_LIST1  '删除列表框中所有项目
'  For i = 1 To 10
'      bb = Str(Rnd)
'      Print bb    '调试输出，只是看看 在DOS窗口显示，如果不要DOS，在工程属性里设置
'      a = FF_ListBox_AddString(HWND_FORM1_LIST1, bb)    '将字符串添加到列表框控件
'      FF_ListBox_SetItemData HWND_FORM1_LIST1, a, i  '设置与列表框中指定项目相关联的 32 位值。
'  Next
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_List1_LBN_SelChange(hWndForm As hWnd, hWndControl As hWnd)  '选择了列表

  Dim a As Long
  '类方式 ==================================================================
  a = Me.List1.ListIndex   ' 返回一个列表框中的当前选定项的索引。多选时不能使用此函数。
  If a <> -1 Then
      Me.Label1.Caption = Me.List1.List(a)
  End If
  
  '函数方式 ===================================================================
'  a = FF_ListBox_GetCurSel(HWND_FORM1_LIST1)  ' 返回一个列表框中的当前选定项的索引。多选时不能使用此函数。
'  If a <> -1 Then
'      FF_Control_SetText HWND_FORM1_LABEL1, FF_ListBox_GetText(HWND_FORM1_LIST1, a)
'  End If
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Form2.Show(hWndForm, False) ' 普通显示窗口

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Form2.Show(hWndForm, True) '模态显示窗口

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

                              
  Me.Caption = "窗口更名"

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  全局变量 += 1
  'FF_Control_SetText HWND_FORM1_COMMAND5, Str(全局变量) '函数方式与类方式都可以用
  Me.Command5.Caption = Str(全局变量)

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  AfxMsg "自动随窗口大小改变位置" & vbCrLf & "在控件属性【ResizeRule】里设置"

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command7_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim P As Point
  GetCursorPos @p    '获取鼠标位置
  TrackPopupMenu yMenu1, 0, p.x, p.y, 0, hWndForm, Null '在某位置说弹出菜单
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。

  yMenu1 = CreatePopupMenu()      '这里演示的是代码创建菜单，当然还有 菜单控件可以使用。
  yMenu2 = CreatePopupMenu()
  
  AddMenu yMenu1, 10001, "复制选择内容", "COPY" '带图标
  AppendMenu yMenu1, MF_SEPARATOR, 0, ""      '分割线
  AppendMenu yMenu1, MF_STRING, 10002, "选择当前内存区"   '不需要图标
  AddMenu yMenu1, 10004, "选择当前内存区", ""    '不需要图标
  AddMenu yMenu1, 10005, "选择当前模块", ""
  AddMenu yMenu1, 10022, "保存选择到文件", ""
  AppendMenu yMenu1, MF_POPUP, Cast(Integer, yMenu2), "子菜单"
  AppendMenu yMenu2, MF_STRING, 10009, "我是子菜单"
   List1.AddItem("1111") 
   List1.AddItem("222") 
   List1.AddItem("总打嗝") 
   List1.AddItem("1测试1")
   list2.AddItem("")  
   list2.AddItem("")  
   list2.AddItem("")  


End Sub


'--------------------------------------------------------------------------------
Sub Form1_WM_Size(hWndForm As hWnd, fwSizeType As Long, nWidth As Long, nHeight As Long)  '窗口已经改变了大小
  Command8.move nWidth - AfxScaleX(110), AfxScaleY(16)
  '坐标以 左上角 为 0,0 开始，向右下角辐射为正值，单位为 像素
  'AfxScaleX(x),AfxScaleY(y) 是为了支持高DPI，窗口都自动支持高DPI了的
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command8_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  '在工具菜单里，用 【对话框编辑】产生API 提示框 代码
  Select Case MessageBox(hWndForm, "在主窗口大小事件里改变位置", "基础方法例题", _
              MB_OK Or MB_ICONINFORMATION Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Case IDOK
  End Select
  

End Sub


'--------------------------------------------------------------------------------
Sub Form1_Command9_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  list1.ItemHeight = AfxScaleY(15)
  list1.Refresh() 

End Sub


'--------------------------------------------------------------------------------
Function Form1_Picture1_WM_Paint(hWndForm As hWnd, hWndControl As hWnd) As LResult  '重绘，系统通知控件需要重新绘画。

  Dim nDC As hDC
  Dim ps As PAINTSTRUCT
  Dim As Long w, h, x, a
  nDC = BeginPaint(hWndControl, @ps) '获取需要绘画DC，推荐此方法，绘图效率高
  FF_Control_GetSize(hWndControl, w, h)
  Dim pMemBmp As CMemBmp = CMemBmp(w, h)  '创建内存DC，先画内存DC，加速画画速度，避免产生闪耀
  '需要内存DC类，必须在 FF_AppStart 模块里加   #include Once  "afx\CMemBmp.inc"
  '自己画画
  DrawFrame(pMemBmp.GetMemDC, 0, 0, w + 1, h + 1, &HFF80FF)
  TextOut1(pMemBmp.GetMemDC, 10, 10, "漂亮的，个性的要自绘")
  TextOut1(pMemBmp.GetMemDC, 10, 40, "自绘固定模式例题")
  '自己画画完成
  BitBlt ndc, 0, 0, w, h, pMemBmp.GetMemDC, 0, 0, SrcCopy '将内存DC，输出到控件
  EndPaint(hWndForm, @ps) '完成绘图
  Function = True
End Function




'                                                                                  
Sub Form1_WM_Command(hWndForm As hWnd, hWndControl As hWnd, wNotifyCode As Long, wID As Long)  '命令处理（处理菜单、工具栏、状态栏等）

  Select  Case wID '菜单事件
      Case 10004 '在创建菜单时设置的ID号，来区分是什么菜单点击了。
          '                                                                                                          Myaaa 
      Case 10005
  End Select

End Sub


Function Form1_List2_OwnerDraw(hWndForm As hWnd, hWndControl As hWnd,lpdis As DRAWITEMSTRUCT) As LResult '自绘控件（需要设计时选择自绘属性）
   Dim rc As Rect
   If lpdis.itemID = &HFFFFFFFF Then Return 0 '如果列表为空 =-1
   rc = lpdis.rcItem  '当前行绘画范围，多余操作，就是嫌弃 lpdis.rcItem 太长，
   Select Case lpdis.itemAction
      Case ODA_DRAWENTIRE, ODA_SELECT '要绘画消息
         Dim ki As Long = lpdis.itemID  '也是多余操作，就是嫌弃 lpdis.itemID 太长，
         Dim gg As yGDI = yGDI(lpdis.hDC, 0, rc.Left, rc.top, rc.Right - rc.Left, rc.bottom - rc.top)
         Dim As Long w=AfxUnscaleX(rc.Right - rc.Left),h= AfxUnscaleX(rc.bottom - rc.top)
         gg.Pen 0, 0
         gg.Brush GetSysColor(COLOR_WINDOW)
         gg.DrawFrame 0, 0, w + 1, h + 1
         If (lpdis.itemState And ODS_SELECTED) = 0 Then                  ' 未选中
            gg.SetColor GetSysColor(COLOR_WINDOWTEXT)
         Else                                                             ' 处于选中状态
            gg.Pen 1, GetSysColor(COLOR_HIGHLIGHT)
            gg.Brush GetSysColor(COLOR_HIGHLIGHT)
            gg.DrawFrame 0, 0, w, h -1
            gg.SetColor GetSysColor(COLOR_HIGHLIGHTTEXT)
         End If
         Dim ttt(2) As String = {"这是自绘List", "在窗口样式里设置自绘", "这里的内容是在属性里添加的"} '给数组赋值
         gg.DrawTextS 0, 0, w, h,ttt(ki) , DT_SINGLELINE Or DT_LEFT Or DT_VCENTER
         Function = True '告诉系统，表示自己画了，不需要系统处理
   End Select
End Function




