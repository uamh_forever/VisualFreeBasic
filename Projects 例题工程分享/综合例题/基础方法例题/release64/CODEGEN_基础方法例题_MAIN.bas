'------------------------------------------------------------------------------
' 生成源代码Visual Free Basic 版本：3.91
' 生成：星期日  十二月 30, 2018 at 02:27:42 下午
' 更多信息访问 www.planetsquires.com 
'------------------------------------------------------------------------------


#lang "FB"

#include Once "windows.bi"
#include Once "Afx/CGdiPlus/CGdiPlus.inc"

#include Once "afx/CWindow.inc" 'WinFBX 库，详情见WinFBX帮助
#include Once "vbcompat.bi"
#include Once "fbthread.bi"
#include Once "fbthread.bi"
#include Once  "afx\CMemBmp.inc"  '应用内存DC类，用不着时，可以不用，默认不带这个
' 这里写自定义声明、常量和#Include的包含文件。
' VisualFreeBasic 不会解析你的任何定义 #Include 文件，它只是包含在最终生成的代码中。
'#CompileFile "文件名.exe" '编译输出文件名(可带文件夹)，注：必须在此模块里才有效，单行且前部无空格

'[END_APPSTART]

Dim Shared gFLY_RichEditClass As zString * MAX_PATH
Dim Shared gFLY_AppID         As zString * MAX_PATH
Dim Shared gFLY_FontNames()   As String
Dim Shared gFLY_FontHandles() As HFONT
Dim Shared gFLY_hDlgCurrent   As HWND

#Define DQ  34     '  双引号

#if __FB_VERSION__ < "1.02"
   #define ENM_PARAGRAPHEXPANDED &h00000020 
   #define LV_COLUMNA LVCOLUMNA
   #define LV_COLUMNW LVCOLUMNW
   #define NM_LISTVIEW NMLISTVIEW
   #define LV_ITEMA LVITEMA
   #define LV_ITEMW LVITEMW
   #define GCLP_HBRBACKGROUND GCL_HBRBACKGROUND 
#endif

#Include Once "win/shlobj.bi"

Type FLY_RESCALEDATA
    FormInit As Long  ' 窗口初始宽度或高度
    FormCur  As Long  ' 窗口当前的宽度或高度
    P_Init   As Long  ' 点初始值
    P_New    As Long  ' 指出新计算的值
End Type

' 保存窗口或控件信息的结构
Type FLY_DATA                                  
   OldProc               As WNDPROC    ' 原窗口或控件程序地址 
   hWndControl           As HWND       ' 窗口或控件句柄
   hWndParent            As HWND       ' 父句柄
   IsForm                As Long       ' 标志，表示这是一个窗口
   IsModal               As Long       ' 标志，表示窗口模态
   hFont                 As HFONT      ' 字体句柄
   hAccel                As HACCEL     ' 快捷键表句柄
   hBackBitmap           As HBITMAP    ' 窗口背景位图句柄
   hStatusBarTimer       as Long       ' 状态栏定时器的句柄
   ControlIndex          As Long       ' 控件索引号（用于控件数组）
   CtrlFocus             As HWND       ' 当前焦点在什么控件
   SelText               As Long       ' 标志，突出显示编辑控件中的文本
   ProcessCtlColor       As Long       ' 标志，处理彩色信息
   IsForeSysColor        As Long       ' 表示使用系统颜色的标志
   IsBackSysColor        As Long       ' 表示使用系统颜色的标志
   hBackBrush            As HBRUSH     ' 背景颜色刷子
   nForeColor            As Long       ' 前景色
   nBackColor            As Long       ' 背景颜色
   fResizeInit           As Long       ' 标志，调整大小已被初始化
   rcForm                As Rect       ' 保存窗口的客户端尺寸的矩形（调整大小）
   rcCtrl                As Rect       ' 客户端相对维度中控件的矩形（调整大小）
   zConstraints          As zString * 30    ' 调整大小约束
End Type
                         
' 程序员可以通过共享APP变量访问的公共信息。
Type APP_TYPE
   Comments        As zString * MAX_PATH      ' 注释
   CompanyName     As zString * MAX_PATH      ' 公司名 
   EXEName         As zString * MAX_PATH      ' 程序的EXE名称 
   FileDescription As zString * MAX_PATH      ' 文件描述 
   hInstance       As HINSTANCE               ' 程序的实例句柄
   Path            As zString * MAX_PATH      ' EXE的当前路径
   ProductName     As zString * MAX_PATH      ' 产品名称 
   LegalCopyright  As zString * MAX_PATH      ' 版权所有 
   LegalTrademarks As zString * MAX_PATH      ' 商标
   ProductMajor    As Long                    ' 产品主要编号 
   ProductMinor    As Long                    ' 产品次要编号   
   ProductRevision As Long                    ' 产品修订号
   ProductBuild    As Long                    ' 产品内部编号   
   FileMajor       As Long                    ' 文件主要编号     
   FileMinor       As Long                    ' 文件次要编号     
   FileRevision    As Long                    ' 文件修订号  
   FileBuild       As Long                    ' 文件内部编号     
   ReturnValue     As Long                    ' 从FF_FormClose返回的用户值
End Type
#include Once "WinClass\ClsForm.inc"
#include Once "WinClass\ClsControl.inc"
#include Once "WinClass\ClsListBox.inc"
#include Once "WinClass\clsButton.inc"
#include Once "WinClass\ClsLabel.inc"
#include Once "WinClass\ClsTextBox.inc"
#include Once "WinClass\ClsPicture.inc"
Dim Shared Form1 As Class_Form
Dim Shared Form1_List1 As Class_ListBox
Dim Shared Form1_Command1 As Class_CommandButton
Dim Shared Form1_Label1 As Class_Label
Dim Shared Form1_Command2 As Class_CommandButton
Dim Shared Form1_Command3 As Class_CommandButton
Dim Shared Form1_Command4 As Class_CommandButton
Dim Shared Form1_Command5 As Class_CommandButton
Dim Shared Form1_Command6 As Class_CommandButton
Dim Shared Form1_Text1 As Class_TextBox
Dim Shared Form1_Command7 As Class_CommandButton
Dim Shared Form1_Command8 As Class_CommandButton
Dim Shared Form1_Command9 As Class_CommandButton
Dim Shared Form1_List2 As Class_ListBox
Dim Shared Form1_Label2 As Class_Label
Dim Shared Form1_Picture1 As Class_Picture
Dim Shared Form2 As Class_Form
Dim Shared Form2_Text1 As Class_TextBox
Dim Shared Form2_Command1 As Class_CommandButton
Dim Shared Form2_Command2 As Class_CommandButton
Dim Shared Form3 As Class_Form

Dim Shared App As APP_TYPE

Function FF_Remain_Internal( ByRef sMainString   as String, _
                    ByRef sMatchPattern as String _
                    ) as String

    Dim nLenMain as Long = Len(sMainString)
    Dim i as Long
    
    i = InStr(sMainString, sMatchPattern)
    If i Then
       Function = Mid(sMainString,i+1)
    Else
       Function = ""
    End If   
End Function



' =====================================================================================
' 获取Windows版本（基于Jose Roca的代码）
' =====================================================================================
FUNCTION AfxGetWindowsVersion () AS Single
   Dim dwVersion AS Long   
   Dim nMajorVer As Long   
   Dim nMinorVer As Long   
   dwVersion = GetVersion
   nMajorVer = LOBYTE(LOWORD(dwVersion))
   nMinorVer = HIBYTE(LOWORD(dwVersion))
   FUNCTION = nMajorVer + (nMinorVer / 100)
END FUNCTION

                    
' =====================================================================================
' 根据应用程序使用的DPI（每像素点数）来缩放水平坐标。
' =====================================================================================
Function AfxScaleX (BYVAL cx AS SINGLE) AS SINGLE
   #IfDef FF_NOSCALE 
      Function = cx
   #Else
       Static bb As Single 
       If bb=0 then  
          Dim hDC As HDC
          hDC = GetDC(Null)
          bb = GetDeviceCaps(hDC, LOGPIXELSX) / 96
          ReleaseDC Null, hDC
       End If
       Function = cx * bb
   #EndIf
End Function


' =====================================================================================
' 根据应用程序正在使用的DPI（每像素点数）缩放垂直坐标。
' =====================================================================================
Function AfxScaleY (BYVAL cy AS SINGLE) AS SINGLE
   #IfDef FF_NOSCALE
      Function = cy
   #Else   
       Static bb As Single 
       If bb=0 then  
           Dim hDC As HDC
           hDC = GetDC(Null)
           bb = GetDeviceCaps(hDC, LOGPIXELSY) / 96
           ReleaseDC Null, hDC
       End If
       Function = cy * bb
   #EndIf
End Function

' =====================================================================================
' 根据Jose Roca的代码
' 为窗口的整个客户区域创建标准工具提示。
' 参数:
' - hwnd = 窗口句柄
' - strTooltipText = 工具提示文本
' - bBalloon = 气球提示 (TRUE or FALSE)
' 返回值:
'   工具提示控件的句柄
' =====================================================================================
Function FF_AddTooltip( BYVAL hwnd AS HWND, strTooltipText AS STRING, BYVAL bBalloon AS Long ) As HWND

      IF hwnd = 0 Then Exit Function
      
      Dim hwndTT AS HWND
      Dim dwStyle As Long   

      dwStyle = WS_POPUP OR TTS_NOPREFIX OR TTS_ALWAYSTIP
      IF bBalloon THEN dwStyle = dwStyle OR TTS_BALLOON
      hwndTT = CreateWindowEx(WS_EX_TOPMOST, "tooltips_class32", "", dwStyle, 0, 0, 0, 0, 0, Cast(HMENU, Null), 0, ByVal Cast(LPVOID, Null))
 
      IF hwndTT = 0 THEN Exit Function
      SetWindowPos(hwndTT, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE OR SWP_NOSIZE OR SWP_NOACTIVATE)

      ' // Register the window with the tooltip control
      Dim tti AS TOOLINFO
      tti.cbSize = SIZEOF(tti)
      tti.uFlags = TTF_SUBCLASS
      tti.hwnd = hwnd
      tti.hinst = GetModuleHandle(BYVAL NULL)
      
      GetClientRect(hwnd, Varptr(tti.rect))
      
      ' // The length of the string must not exceed of 80 characters, including the terminating null
      IF LEN(strTooltipText) > 79 THEN strTooltipText = LEFT(strTooltipText, 79)
      tti.lpszText = STRPTR(strTooltipText)
      tti.uId = 0
      SendMessage hwndTT, TTM_ADDTOOL, 0, Cast(LPARAM, Varptr(tti))
      Function = hwndTT

End Function



' 声明/等同 项目中的所有函数，表单和控件

'  ParseModule 模块属性设置为 False


'  ParseModule 模块属性设置为 True
#Include Once "CODEGEN_基础方法例题_DECLARES.inc"
#Include Once "CODEGEN_基础方法例题_UTILITY.inc"
#Include Once "CODEGEN_基础方法例题_FORM1_FORM.inc"
#Include Once "CODEGEN_基础方法例题_FORM2_FORM.inc"
#Include Once "CODEGEN_基础方法例题_FORM3_FORM.inc"
    


'#PARSESTART#  '不要删除这一行


Function FF_WINMAIN( ByVal hInstance     As HINSTANCE, _
                     ByVal hPrevInstance As HINSTANCE, _
                     ByRef lpCmdLine     As String, _  
                     ByVal iCmdShow      As Long ) As Long


   ' 如果这个函数返回TRUE（非零），那么WinMain将退出从而结束该程序。
   ' 您可以在此函数做程序初始化。

   Function = FALSE    '如果你想让程序结束，则函数返回 TRUE 。

End Function

'[END_WINMAIN]

Function FF_PUMPHOOK( uMsg As Msg ) As Long


   ' 如果这个函数返回 FALSE(0)，那么 VisualFreeBasic 消息泵将继续进行。
   ' 返回 TRUE （非零）将绕过常规的消息泵。
   ' 

   Function = FALSE    '如果你需要绕过 VisualFreeBasic 消息泵，返回 TRUE 。

End Function

'[END_PUMPHOOK]


Sub FLY_InitializeVariables()    
   ' 所有与窗体和控件有关的FireFly变量都在这里初始化。 这包括已经定义的任何控件数组以及声明包含文件中每个列表的控件标识符。
   
    IDC_FORM1_PICTURE1 = 1000
    IDC_FORM1_LIST2 = 1001
    IDC_FORM1_LABEL2 = 1002
    IDC_FORM1_COMMAND8 = 1003
    IDC_FORM1_COMMAND9 = 1004
    IDC_FORM1_TEXT1 = 1005
    IDC_FORM1_COMMAND7 = 1006
    IDC_FORM1_COMMAND6 = 1007
    IDC_FORM1_COMMAND3 = 1008
    IDC_FORM1_COMMAND4 = 1009
    IDC_FORM1_COMMAND5 = 1010
    IDC_FORM1_COMMAND2 = 1011
    IDC_FORM1_LABEL1 = 1012
    IDC_FORM1_COMMAND1 = 1013
    IDC_FORM1_LIST1 = 1014
    IDC_FORM2_COMMAND2 = 1000
    IDC_FORM2_COMMAND1 = 1001
    IDC_FORM2_TEXT1 = 1002
          

End Sub   


Sub FLY_SetAppVariables()    
   ' 所有的FireFly App变量都在这里初始化。 这个Type变量提供了对许多常用的FireFly设置的简单访问。
   
   Dim zTemp As zString * MAX_PATH
   Dim x     As Long
   
   App.CompanyName       =  ""
   App.FileDescription   =  ""
   App.ProductName       =  ""
   App.LegalCopyright    =  ""
   App.LegalTrademarks   =  ""
   App.Comments          =  ""

   App.ProductMajor      =  1
   App.ProductMinor      =  0
   App.ProductRevision   =  0
   App.ProductBuild      =  0

   App.FileMajor         =  1
   App.FileMinor         =  0
   App.FileRevision      =  0
   App.FileBuild         =  0

   ' App.hInstance 在WinMain / LibMain中设置

   ' 检索程序完整路径和 EXE/DLL 名称
   GetModuleFileName App.hInstance, zTemp, MAX_PATH
   
   x = InStrRev( zTemp, Any ":/\")

   If x Then 
      App.Path           =  Left(zTemp, x) 
      App.EXEname        =  Mid(zTemp, x + 1)
   Else
      App.Path           =  "" 
      App.EXEname        =  zTemp
   End If


   ' 以下两个数组用于允许FireFly重用一个或多个控件通用的字体句柄。
   ' 这使我们不必为每个创建的控制字体使用GDI资源。
   ReDim gFLY_FontNames(0)   As String
   ReDim gFLY_FontHandles(0) As HFONT
   
End Sub


Function FLY_AdjustWindowRect( ByVal hWndForm As HWND, _
                               ByVal cxClient As Long   , _
                               ByVal cyClient As Long    _
                               ) As Long
                               
   Dim dwStyle As Long
   Dim hMenu   As HMENU
   Dim rc      As Rect
   
   If (cxClient <> 0) And (cyClient <> 0) Then
      dwStyle = GetWindowLongPtr( hWndForm, GWL_STYLE )
      rc.Left = 0: rc.Top = 0: rc.Right = cxClient: rc.Bottom = cyClient
      hMenu = GetMenu( hWndForm )
      AdjustWindowRectEx VarPtr(rc), dwStyle, (hMenu <> 0), GetWindowLongPtr(hWndForm, GWL_EXSTYLE)
      If (dwStyle and WS_HSCROLL) = WS_HSCROLL Then rc.Bottom = rc.Bottom + GetSystemMetrics(SM_CYHSCROLL)
      If (dwStyle and WS_VSCROLL) = WS_VSCROLL Then rc.Right  = rc.Right  + GetSystemMetrics(SM_CXVSCROLL)
      SetWindowPos hWndForm, 0, 0, 0, _
                   rc.Right-rc.Left, rc.Bottom-rc.Top, _
                   SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE
   End If

   Function = 0
   
End Function



' 枚举窗体的所有子窗口（基本上，所有控件）以处理WM_SYSCOLORCHANGE消息。
' 这将删除现有的画笔并使用新的系统颜色重新创建画笔。
Function FLY_EnumSysColorChangeProc( ByVal hWnd As HWND, lParam As LPARAM ) As Long
   Dim ff As FLY_DATA Ptr 
    
   ff = GetProp(hWnd, "FLY_PTR")
   If ff Then
      If ff->ProcessCtlColor Then
          
         ' Create the new colors/brushes if we are using System colors 
         If ff->IsForeSysColor Then ff->nForeColor = GetSysColor(ff->nForeColor)
         If ff->IsBackSysColor Then 
            If ff->hBackBrush Then DeleteObject(Cast(HGDIOBJ, ff->hBackBrush))
            ff->hBackBrush = GetSysColorBrush(ff->nBackColor)
         End If   

      End If
   End If
    
   Function = TRUE
End Function

Function FLY_SetControlData( ByVal hWndControl    As HWND, _
                             ByVal AllowSubclass  As Long   , _
                             ByVal AllowSetFont   As Long   , _
                             ByRef sFontString    As String, _
                             ByVal nControlIndex  As Long   , _
                             ByVal nProcessColor  As Long   , _
                             ByVal IsForeSysColor As Long   , _
                             ByVal IsBackSysColor As Long   , _
                             ByVal nForeColor     As Long   , _
                             ByVal nBackColor     As Long   , _
                             ByVal nTransparentBrush As HBRUSH, _
                             ByVal CodeProcedure  As WNDPROC, _
                             sResizeRules         As String, _      
                             ByVal nFontUpgrade   As Long    _   
                             ) As FLY_DATA ptr
                            
    Dim zClassName As zString * 50            
    Dim sFont      As String
    Dim ff As FLY_DATA Ptr 

    ff = HeapAlloc( GetProcessHeap(), HEAP_ZERO_MEMORY, SizeOf(*ff) )
    If ff Then
       ' 存储指针供以后使用
       SetProp hWndControl, "FLY_PTR", ff  
    
       ' 子类的控件
       If AllowSubclass = TRUE Then
          ff->OldProc = Cast(WNDPROC,SetWindowLongPtr( hWndControl, GWLP_WNDPROC, Cast(LONG_PTR, CodeProcedure) ))  
       End If
    
       ' 为此 窗体/控件 设置字体
       If AllowSetFont = TRUE Then

          ' FireFly处理两种字体字符串。 
          ' 第一个是传统的logfont字符串，第二个是简化的PB版本。 
          ' FireFly正朝着简化版本迈进，但需要处理logfont版本以实现向后兼容。

          ' 如果FontUpgrade属性处于活动状态，那么我们需要根据当前操作系统替换FontName。
          If nFontUpgrade <> 0 Then
             IF AfxGetWindowsVersion >= 6 THEN     ' Vista/Windows 7+
                sFontString = "微软雅黑," & FF_Remain_Internal(sFontString, ",") 
             Else
                sFontString = "宋体," & FF_Remain_Internal(sFontString, ",") 
             End If
          End If

          ff->hFont = FF_MakeFontEx_Internal( sFontString )

          If ff->hFont Then 
             SendMessage hWndControl, WM_SETFONT, Cast(WPARAM,ff->hFont), True
             If UCase(zClassName) = "SYSDATETIMEPICK32" Then
                SendMessage hWndControl, DTM_SETMCFONT, Cast(WPARAM,ff->hFont), True
             End If
          End If

       End If

       ff->ControlIndex = nControlIndex
       ff->IsForm       = FALSE          
       ff->hwndControl  = hWndControl 
       ff->zConstraints = sResizeRules
    
       ' Flag to process the WM_CTLCOLOR??? messages.
       ff->ProcessCtlColor = nProcessColor
       If ff->ProcessCtlColor Then
          ff->IsForeSysColor = IsForeSysColor
          ff->IsBackSysColor = IsBackSysColor
          ff->nForeColor     = nForeColor
          ff->nBackColor     = nBackColor
          ' Create a Brush for painting the background of this Control.
          If nTransparentBrush <> Cast(HBRUSH,-1) Then
             ff->hBackBrush = nTransparentBrush
          Else 
             ff->hBackBrush = IIF( ff->IsBackSysColor, GetSysColorBrush(ff->nBackColor), CreateSolidBrush(ff->nBackColor) )
          End If
       End If
    
    End If

    Function = ff     ' 返回指向数据结构的指针

End Function



Function FLY_DoMessagePump( ByVal ShowModalFlag  As Long, _
                            ByVal hWndForm       As HWND, _
                            ByVal hWndParent     As HWND, _
                            ByVal nFormShowState As Long, _
                            ByVal IsMDIForm      As Long _
                            ) As HWND
    
   Dim zTempString As zString * MAX_PATH
   Dim hWndActive  As HWND
   Dim msg         As MSG
   Dim ff          As FLY_DATA Ptr 

   ff = GetProp( hWndForm, "FLY_PTR" )
   If ff = 0 Then Exit Function

   
   ' 如果这是一个MDI子窗体，那么它不能显示为模态。
   If ( GetWindowLongPtr(hWndForm, GWL_EXSTYLE) And WS_EX_MDICHILD ) = WS_EX_MDICHILD Then ShowModalFlag = FALSE 
    

   If ShowModalFlag = TRUE Then
    
      ' 确定活动控件的顶层窗口
      WHILE (GetWindowLongPtr(hWndParent, GWL_STYLE) AND WS_CHILD) <> 0
         hWndParent = GetParent(hWndParent)
         IF (GetWindowLongPtr(hWndParent, GWL_EXSTYLE) AND WS_EX_MDICHILD) <> 0 THEN EXIT WHILE
      WEND
    
      ' 设置属性以显示这是一个模态窗体。 hWndParent存储在属性中，因为该窗口需要在模态窗体被销毁并退出模态消息循环之前重新启用。
      ff->IsModal    = TRUE
      ff->hWndParent = hWndParent
    
      ' 为父窗体禁用鼠标和键盘输入
      If hWndParent <> 0 Then EnableWindow( hWndParent, False )
    
      ShowWindow hWndForm, nFormShowState
      UpdateWindow hWndForm
   
      ' 主消息循环：
      Do While GetMessage( @Msg, 0, 0, 0 )    
           ' Exit the modal message loop if the Form was destroyed (important).
           If IsWindow(hWndForm) = FALSE   Then Exit Do
           If msg.message        = WM_QUIT Then Exit Do
           
           If FF_PUMPHOOK(Msg) = 0 Then
             If IsMDIForm = TRUE Then
                If TranslateMDISysAccel(hWndForm, @Msg) <> 0 Then Continue Do
             End If                                                          
             
             If TranslateAccelerator(hWndForm, ff->hAccel, @Msg) = 0 Then
      
                hWndActive = FLY_GetActiveHandle(gFLY_hDlgCurrent)
         
                ' 处理在多行文本框中按ESCAPE导致应用程序接收可能导致应用程序终止的WM_CLOSE的奇怪情况。 
                ' 还允许TAB键移入和移出多行文本框。
                GetClassName GetFocus, zTempString, SizeOf(zTempString)
                Select Case Ucase(zTempString)
                  Case "EDIT", Ucase(gFLY_RichEditClass)
                    IF (GetWindowLongPtr(GetFocus, GWL_STYLE) AND ES_MULTILINE) = ES_MULTILINE THEN
                       If (msg.message = WM_KEYDOWN) And (Msg.wParam = VK_ESCAPE) Then
                           msg.message = WM_COMMAND
                           msg.wParam  = MakeLong(IDCANCEL, 0)
                           msg.lParam  = 0
                       ElseIf (Msg.message = WM_CHAR) And (Msg.wParam = 9) Then
                           ' allow the Tab key to tab out of a multiline textbox
                           If (GetAsyncKeyState(VK_SHIFT) And &H8000) = 0 Then
                              SetFocus GetNextDlgTabItem( GetParent(Msg.hWnd), Msg.hWnd, FALSE )
                           Else
                              SetFocus GetNextDlgTabItem( GetParent(Msg.hWnd), Msg.hWnd, TRUE )
                           End If
                           msg.message = WM_NULL
                       End If
                    End If
                End Select
         
                If (hWndActive = 0) Or (IsDialogMessage(hWndActive, @Msg) = 0) Then
                   TranslateMessage @Msg
                   DispatchMessage @Msg                     
                End If     
         
             End If
           End If
      Loop
      
      Function = Cast(HWND, Cast(LONG_PTR,App.ReturnValue))
      App.ReturnValue = 0
      
   Else
     ShowWindow hWndForm, nFormShowState
     Function = hWndForm
   End If
    
End Function


Function FLY_GetActiveHandle( ByVal hWnd As HWND ) as HWND

  Static szClassName As zString * 100

  ' 确定活动控件的顶层窗口
  Do While (GetWindowLongPtr(hWnd, GWL_STYLE) AND WS_CHILD) <> 0
    IF (GetWindowLongPtr(hWnd, GWL_EXSTYLE) AND WS_EX_MDICHILD) <> 0 THEN EXIT DO
    GetClassName hWnd, szClassName, SizeOf(szClassName)
    If Left(szClassName, 5) = "FORM_" Then EXIT DO
    hWnd = GetParent(hWnd)
  loop

  FUNCTION = hWnd

End Function    
            

Function FLY_ResizeRuleInitEnum( ByVal hWnd As HWND, ByVal lParam As LPARAM ) As Long

   Dim ff As FLY_DATA Ptr    
   
   ff = GetProp(hWnd, "FLY_PTR")   
   If ff = 0 Then Function = TRUE: Exit Function   

   If ff->fResizeInit = TRUE Then Function = TRUE: Exit Function   

   ' 存储窗口宽度和高度以供将来参考
   GetClientRect GetParent(hWnd), Varptr(ff->rcForm)
         
   ' 获取控件的矩形并将其转换为客户端坐标
   GetWindowRect hWnd, Varptr(ff->rcCtrl)
   MapWindowPoints 0, GetParent(hWnd), Cast(LPPOINT,Varptr(ff->rcCtrl)), 2
   
   ff->fResizeInit = TRUE

   Function = TRUE

End Function


   
Function FLY_ResizeRuleEnum( ByVal hWnd As HWND, ByVal lParam As LPARAM ) As Long

   Dim buf(1 To 4)          As FLY_RESCALEDATA
   Dim i                    As Long    
   Dim Constr               As String
   Dim Constraints          As String   
   Dim ConstrRef            As String
   Dim RefPntPercentage     As Single
   Dim ControlName          As String
   Dim rc                   As Rect
   Dim ff                   As FLY_DATA Ptr    
   
   ff = GetProp(hWnd, "FLY_PTR")   
   
   If ff = 0 Then Function = TRUE: Exit Function   
   If ff->fResizeInit = FALSE Then Function = TRUE: Exit Function

   Constraints = UCase(ff->zConstraints)
   if Len(Constraints) < 7 Then Function = TRUE: Exit Function
   
    ' 控件已经初始化。 获取其初始坐标以供参考。
    ' 将控件初始坐标存储在计算缓冲区中。
    ' 控件的每一方都有一个条目。
    Buf(1).P_Init = ff->rcCtrl.Left         ' Left 控件的x坐标
    Buf(2).P_Init = ff->rcCtrl.Top          ' Top y-coordinate of the control
    Buf(3).P_Init = ff->rcCtrl.Right        ' Right x-coordinate of the control
    Buf(4).P_Init = ff->rcCtrl.Bottom       ' Bottom y-coordinate of the control      

    ' 将窗口的宽度和高度存储在计算缓冲区中。
    ' One entry for every side of the form
    Buf(1).FormInit = ff->rcForm.Right      ' For Left x-coordinate of the control
    Buf(2).FormInit = ff->rcForm.Bottom     ' For Top y-coordinate of the control
    Buf(3).FormInit = ff->rcForm.Right      ' For Right x-coordinate of the control
    Buf(4).FormInit = ff->rcForm.Bottom     ' For Bottom y-coordinate of the control
    
    
    ' -------------------------------------------------------------------------------
    ' (2) 获取窗口的当前尺寸并将坐标存储在稍后计算的缓冲区中
    ' -------------------------------------------------------------------------------
    GetClientRect GetParent(hWnd), Varptr(rc)
    Buf(1).FormCur  = rc.Right      ' Left x-coordinate
    Buf(2).FormCur  = rc.Bottom     ' Top y-coordinate
    Buf(3).FormCur  = rc.Right      ' Right x-coordinate
    Buf(4).FormCur  = rc.Bottom     ' Bottom y-coordinate


    ' -------------------------------------------------------------------------------
    ' (3)计算新的控件坐标（对于控件4个面的每一个）
    ' -------------------------------------------------------------------------------
    For i = 1 To 4      
       Constr = FF_Parse(Constraints, ",", i) 
        
       Select Case Left(Constr, 1)   
          Case "S"    ' Scaled coordinate
            Buf(i).P_New  = Buf(i).P_Init * (Buf(i).FormCur / Buf(i).FormInit)
     
          Case "F"  ' Fixed coordinate
             ConstrRef = Mid(Constr, 2)     ' Remove the "F"
             Select Case ConstrRef
                Case "L" : RefPntPercentage = 0       ' Left side
                Case "R" : RefPntPercentage = 100     ' Right side
                Case "T" : RefPntPercentage = 0       ' Top
                Case "B" : RefPntPercentage = 100     ' Bottom
                Case Else   ' Must have been a number
                   RefPntPercentage = Val(ConstrRef) 
             End Select 
             
             ' Calc the new position
             Buf(i).P_New  = Buf(i).P_Init - _
                             (Buf(i).FormInit * (RefPntPercentage / 100)) + _
                             (Buf(i).FormCur  * (RefPntPercentage / 100))

          Case Else
             'MsgBox "未知的约束参数:" & Left$(Constr, 1) & " 在 " & Constraints        
       End Select
       
    Next 


    ' -------------------------------------------------------------------------------
    ' (4) Redraw the control
    ' -------------------------------------------------------------------------------
    SetWindowPos hWnd, 0, _
                 Buf(1).P_New, Buf(2).P_New, _
                 Buf(3).P_New - Buf(1).P_New, Buf(4).P_New - Buf(2).P_New, _
                 SWP_NOZORDER    
   
   Function = TRUE ' 继续枚举子窗口  
   
End Function



Function WinMain( ByVal hInstance     As HINSTANCE, _
                  ByVal hPrevInstance As HINSTANCE, _
                  ByRef lpCmdLine     As String,  _
                  ByVal iCmdShow      As Long _
                  ) As Long

    ' 初始化公共控件。 在WinXP平台上这是必要的，以确保XP主题样式的工作。
    Dim uCC  As INITCOMMONCONTROLSEX
    Dim hLib As HINSTANCE
 
    uCC.dwSize = SizeOf(uCC)
    uCC.dwICC  = ICC_NATIVEFNTCTL_CLASS Or ICC_COOL_CLASSES Or ICC_BAR_CLASSES Or _
                 ICC_TAB_CLASSES Or ICC_USEREX_CLASSES Or ICC_WIN95_CLASSES Or _
                 ICC_STANDARD_CLASSES Or ICC_ANIMATE_CLASS Or ICC_DATE_CLASSES Or _
                 ICC_HOTKEY_CLASS Or ICC_INTERNET_CLASSES Or ICC_LISTVIEW_CLASSES Or _
                 ICC_PAGESCROLLER_CLASS Or ICC_PROGRESS_CLASS Or ICC_TREEVIEW_CLASSES Or _
                 ICC_UPDOWN_CLASS

    InitCommonControlsEx VarPtr(uCC)


    ' RichEdit 4.1 (Windows XP)
    hLib = LoadLibrary("MSFTEDIT.DLL")
    IF hLib THEN
       gFLY_RichEditClass = "RichEdit50W"
    Else
       ' Try RichEdit 2.0 or 3.0
       hLib = LoadLibrary("RICHED20.DLL")
       IF hLib THEN
          gFLY_RichEditClass = "RichEdit20A"
       Else
          ' Try RichEdit 1.0
          hLib = LoadLibrary("RICHED32.DLL")
          IF hLib THEN
             gFLY_RichEditClass = "RichEdit"
          End If
       End If
    END IF

                     
   ' 定义用于唯一标识此应用程序的变量。 这由FireFly函数FF_PrevInstance使用。
   gFLY_AppID = "FORM_基础方法例题_Form1_CLASS"


   ' 定义控件数组（如有必要）
   FLY_InitializeVariables
   

   ' 设置共享应用程序变量的值
   App.hInstance = hInstance
   FLY_SetAppVariables
    

    ' Initialize the GDI+ library
   Dim gdipToken As ULONG_PTR
   Dim gdipsi As GdiplusStartupInput
   gdipsi.GdiplusVersion = 1
   GdiplusStartup( @gdipToken, @gdipsi, Null )


   ' 调用 FLY_WinMain()函数。 如果该函数返回True，则停止执行该程序。
   If FF_WINMAIN(hInstance, hPrevInstance, lpCmdLine, iCmdShow) Then 
      Function = App.ReturnValue
      Exit Function
   End If   
   
   ' 创建启动窗体。
   Form1_Show 0, TRUE

    ' 关闭GDI+库
   GdiplusShutdown( gdipToken )


   Function = App.ReturnValue

End Function
                                          
                                                                                                            
End WinMain( GetModuleHandle( null ), null, Command(), SW_NORMAL )
