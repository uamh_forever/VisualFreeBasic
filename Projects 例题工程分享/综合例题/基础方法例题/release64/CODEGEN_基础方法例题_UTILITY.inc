'------------------------------------------------------------------------------
' 生成源代码Visual Free Basic 版本：3.91
' 生成：星期日  十二月 30, 2018 at 02:27:42 下午
' 更多信息访问 www.planetsquires.com 
'------------------------------------------------------------------------------

Function FF_ListView_GetColumnAlignment( ByVal hWndControl As HWnd, _
                                         ByVal iPosition As Long _
                                         ) As Long

    Dim tlvc As LV_COLUMN

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
  
       tlvc.mask     = LVCF_FMT 
       If SendMessage( hWndControl, LVM_GETCOLUMN, iPosition, Cast(LPARAM, Varptr(tlvc))) Then
          If (tlvc.fmt And LVCFMT_RIGHT) = LVCFMT_RIGHT Then 
              Function = LVCFMT_RIGHT:  Exit Function
          End If
          If (tlvc.fmt And LVCFMT_CENTER) = LVCFMT_CENTER Then 
              Function = LVCFMT_CENTER:  Exit Function
          End If
          If (tlvc.fmt And LVCFMT_LEFT) = LVCFMT_LEFT Then 
              Function = LVCFMT_LEFT:  Exit Function
          End If
       End If   

    End If

End Function



Function FF_ListView_GetItemImage( ByVal hWndControl as HWnd, _
                                   ByVal iRow as Long, _
                                   ByVal iColumn as Long _
                                   ) as Long

    Dim tlv_item as LV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        tlv_item.mask       = LVIF_IMAGE
        tlv_item.iItem      = iRow
        tlv_item.iSubItem   = iColumn 
       
       If SendMessage( hWndControl, LVM_GETITEM, 0, Cast(LPARAM, VarPtr(tlv_item))) Then
          Function = Cast(Integer, tlv_item.iImage)
       End If   

    End If

End Function



Function FF_ListView_GetItemlParam (ByVal hWndControl as HWnd, _
                                    ByVal iRow as Long, _
                                    ByVal iColumn as Long) as Long

    Dim tlv_item as LV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        tlv_item.mask       = LVIF_PARAM
        tlv_item.iItem      = iRow
        tlv_item.iSubItem   = iColumn 
       
       If SendMessage( hWndControl, LVM_GETITEM, 0, Cast(LPARAM, VarPtr(tlv_item))) Then
          Function = tlv_item.lParam
       End If   

    End If

End Function



Function FF_ListView_GetItemText( ByVal hWndControl as HWnd, _
                                  ByVal iRow as Long, _
                                  ByVal iColumn as Long _
                                  ) as String

    Dim tlv_item as LV_ITEM
    Dim zText    as ZString * MAX_PATH

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        tlv_item.mask       = LVIF_TEXT
        tlv_item.iItem      = iRow
        tlv_item.iSubItem   = iColumn 
        tlv_item.pszText    = VarPtr(zText)
        tlv_item.cchTextMax = SizeOf(zText)
       
       If SendMessage( hWndControl, LVM_GETITEM, 0, Cast(LPARAM, VarPtr(tlv_item))) Then
          Function = zText
       End If   

    End If

End Function



Function FF_ListView_InsertColumn( ByVal hWndControl as HWnd, _
                                   ByVal iPosition   as Long, _
                                   ByRef TheText     as String, _
                                   ByVal nAlignment  as Long = LVCFMT_LEFT, _
                                   ByVal nWidth      as Long = 100 _
                                   ) as Integer

    Dim tlvc as LV_COLUMN

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
  
       tlvc.mask     = LVCF_FMT Or LVCF_WIDTH Or LVCF_TEXT Or LVCF_SUBITEM
       tlvc.fmt      = nAlignment
       tlvc.cx       = nWidth
       tlvc.pszText  = StrPtr(TheText)
       tlvc.iSubItem = 0 
       Function = SendMessage( hWndControl, LVM_INSERTCOLUMN, iPosition, Cast(LPARAM, VarPtr(tlvc)))

    End If
    
End Function



Function FF_ListView_InsertItem( ByVal hWndControl as HWnd, _
                                 ByVal iRow        as Long, _         
                                 ByVal iColumn     as Long, _
                                 ByRef TheText     as String, _
                                 ByVal lParam as Long = 0, _
                                 ByVal iImage as Long = 0 _
                                 ) as Integer

    Dim tlv_item as LV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        tlv_item.iItem     = iRow
        tlv_item.iSubItem  = iColumn 
        tlv_item.pszText   = StrPtr(TheText)
        tlv_item.iImage    = iImage
        tlv_item.lParam    = lParam
        
        If iColumn = 0 Then
           tlv_item.mask      = LVIF_TEXT Or LVIF_PARAM Or LVIF_IMAGE 
           Function = SendMessage( hWndControl, LVM_INSERTITEM, 0, Cast(LPARAM, VarPtr(tlv_item)))
        Else 
           tlv_item.mask      = LVIF_TEXT Or LVIF_IMAGE
           Function = SendMessage( hWndControl, LVM_SETITEM, 0, Cast(LPARAM, VarPtr(tlv_item)))
        End If
    End If

    
End Function



Function FF_ListView_SetColumnAlignment( ByVal hWndControl as HWnd, _
                                         ByVal iPosition as Long, _
                                         ByVal nAlignment as Long _
                                         ) as Integer

    Dim tlvc as LV_COLUMN

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
  
       tlvc.mask     = LVCF_FMT 
       tlvc.fmt      = nAlignment
       Function = SendMessage( hWndControl, LVM_SETCOLUMN, iPosition, Cast(LPARAM, VarPtr(tlvc)))

    End If
    
End Function



Function FF_ListView_SetItemImage( ByVal hWndControl as HWnd, _
                                   ByVal iRow as Long, _
                                   ByVal iColumn as Long, _
                                   ByVal iImage as Long _
                                   ) as Integer

    Dim tlv_item as LV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        tlv_item.mask       = LVIF_IMAGE
        tlv_item.iItem      = iRow
        tlv_item.iSubItem   = iColumn 
        tlv_item.iImage     = iImage 
       
        Function = SendMessage( hWndControl, LVM_SETITEM, 0, Cast(LPARAM, VarPtr(tlv_item)))

    End If

End Function



Function FF_ListView_SetItemlParam( ByVal hWndControl as HWnd, _
                                    ByVal iRow as Long, _
                                    ByVal iColumn as Long, _
                                    ByVal lParam as Long _
                                    ) as Integer

    Dim tlv_item as LV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        tlv_item.mask       = LVIF_PARAM
        tlv_item.iItem      = iRow
        tlv_item.iSubItem   = iColumn 
        tlv_item.lParam     = lParam
       
        Function = SendMessage( hWndControl, LVM_SETITEM, 0, Cast(LPARAM, VarPtr(tlv_item)))

    End If

End Function



Function FF_ListView_SetItemText( ByVal hWndControl as HWnd, _
                                  ByVal iRow as Long, _
                                  ByVal iColumn as Long, _
                                  ByRef TheText as String _
                                  ) as Long

    Dim tlv_item as LV_ITEM

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        tlv_item.mask       = LVIF_TEXT
        tlv_item.iItem      = iRow
        tlv_item.iSubItem   = iColumn 
        tlv_item.pszText    = StrPtr(TheText)
        tlv_item.cchTextMax = Len(TheText)
       
        Function = SendMessage( hWndControl, LVM_SETITEM, 0, Cast(LPARAM, VarPtr(tlv_item)))

    End If

End Function



Function FF_ListView_SetSelectedItem( ByVal hWndControl as HWnd, _
                                      ByVal nIndex as Long _
                                      ) as Long

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       Function = SendMessage( hWndControl, LVM_SETSELECTIONMARK, 0, nIndex)

       'ensure that the item is set visually as well
       Dim tItem as LVITEM
       tItem.mask      = LVIF_STATE
       tItem.iItem     = nIndex
       tItem.iSubItem  = 0
       tItem.State     = LVIS_FOCUSED Or LVIS_SELECTED
       tItem.statemask = LVIS_FOCUSED Or LVIS_SELECTED 
       SendMessage hWndControl, LVM_SETITEMSTATE, nIndex, Cast(LPARAM, VarPtr(tItem))
    End If  

End Function



Sub AddMenu( yMenu as HMENU,cID as ULong ,n as String , T as String )
    Dim A as  String   *50
    a=n
    AppendMenu yMenu, MF_STRING, cid,a
    If Len(t)>0 Then  MenuLoadIcoImag(yMenu,cID,"IMAGE_" & t)

End Sub

Sub MenuLoadIcoImag( yMenu as HMENU,cID as UInteger  , TicoStr as String  )  '
    Dim A as  HBITMAP ,B as HANDLE ,t as ZString *50
    t= TicoStr
    b=LoadImage(app.hInstance,@t,IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR)
    a=IconToBitmap(b,GetSysColor(Color_menu))
    DeleteObject b
    SetMenuItemBitmaps( yMenu, cID, MF_BYCOMMAND,a, a)
    DeleteObject b
End Sub


Function FF_Control_GetCheck( ByVal hWndControl As HWND ) As Long
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       
       ' Get the check state
         If SendMessage( hWndControl, BM_GETCHECK, 0, 0) = BST_CHECKED Then
            Function = True
         End If   
       
    End If
       
End Function



Sub FF_Control_GetColor( ByVal hWndControl as HWnd, _
                         ByRef ForeColor as Long, _
                         ByRef BackColor as Long)

    Dim ff as FLY_DATA Ptr 
    Dim hBrush as HBRUSH
    Dim LOG_BRUSH as LOGBRUSH
     
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       
       ff = GetProp(hWndControl, "FLY_PTR")
       
       If ff Then
          ' Determine is we are dealing with a Form or Control
          If ff->IsForm Then             
             hBrush = Cast(HBRUSH, GetClassLongPtr( hWndControl, GCLP_HBRBACKGROUND))
             'Get the Color From the brush:
             GetObject hBrush, SizeOf(LOG_BRUSH), ByVal VarPtr(LOG_BRUSH)
             ForeColor = 0
             BackColor = LOG_BRUSH.lbColor
             Exit Sub
          End If
          
          If ff->ProcessCtlColor Then      
             If ff->IsForeSysColor Then
                ForeColor = GetSysColor( ff->nForeColor )
             Else
                ForeColor = ff->nForeColor
             End If   
          
             If ff->IsBackSysColor Then
                BackColor = GetSysColor( ff->nBackColor )
             Else
                BackColor = ff->nBackColor
             End If   

          Else
             ForeColor = GetSysColor( ff->nForeColor )
             BackColor = GetSysColor( ff->nBackColor )
          End If
             
       End If
       
    End If
End Sub



Function FF_Control_GetSize( ByVal hWndControl as HWnd, _
                             ByRef nWidth as Long, _
                             ByRef nHeight as long _
                             ) as Integer
    
    Dim rc as Rect
    
    ' This function works for both Forms and Controls.
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then
       
       ' Get the dimensions of the window
         Function = GetWindowRect( hWndControl, @rc)
       
       ' Return the Height and Width values back from the function
         nWidth  = (rc.Right - rc.Left)
         nHeight = (rc.Bottom - rc.Top)

    End If
    
End Function



Function FF_Control_GetTag( ByVal hWndControl As HWND ) As String
                                   
   Dim pzString As ZString Ptr
   
   'Get the pointer to the Tag string from the hWnd PROP.
   pzString = GetProp( hWndControl, "FLY_TAGPROPERTY" )
   
   If pzString = 0 Then
      'Null pointer
      Function = ""
   Else
      Function = *pzString
   End If      

End Function



Function FF_Control_GetTag2( ByVal hWndControl As HWND ) As String
                                   
   Dim pzString As ZString Ptr
   
   'Get the pointer to the Tag2 string from the hWnd PROP.
   pzString = GetProp( hWndControl, "FLY_TAGPROPERTY2" )
   
   If pzString = 0 Then
      'Null pointer
      Function = ""
   Else
      Function = *pzString
   End If      

End Function



Function FF_Control_GetText(ByVal hWndControl As HWND) As String
    
    Dim nBufferSize As Long
    Dim nBuffer     As String
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then
       
       ' 得到的文本的长度
         nBufferSize = GetWindowTextLength(hWndControl)
         If nBufferSize = 0 Then 
            Function = "": Exit Function
         End If                       
         
       ' Add an extra character for the Nul terminator
         nBufferSize = nBufferSize + 1
         
       ' Create the temporary buffer
         nBuffer = Space(nBufferSize)
         
       ' Retrieve the text
         GetWindowText hWndControl, ByVal StrPtr(nBuffer), nBufferSize

       ' Remove the Null
         nBuffer = RTrim(nBuffer, chr(0))

         Function = nBuffer
         
    End If
    
   
End Function



Sub FF_Control_SetCheck( ByVal hWndControl as HWnd, _
                         ByVal nCheckState as Integer ) 
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       
       ' Set the check state
         If nCheckState Then
            SendMessage hWndControl, BM_SETCHECK, BST_CHECKED, 0
         Else
            SendMessage hWndControl, BM_SETCHECK, BST_UNCHECKED, 0
         End If
            
    End If
       
End Sub



Sub FF_Control_SetColor( ByVal hWndControl as HWnd, _
                         ByVal ForeColor as Integer, _
                         ByVal BackColor as Integer)

    Dim ff_control  as FLY_DATA Ptr 
    Dim Redraw_Flag as Long
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       
       ff_control = GetProp(hWndControl, "FLY_PTR")
       
       If ff_control Then
          
          ' Determine is we are dealing with a Form or Control
            If ff_control->IsForm Then             
            
                ' ForeColor has no effect on a Form
                
                ' Set the Background color
                  If BackColor <> -1 Then
                     If ff_control->hBackBrush Then 
                        DeleteObject ff_control->hBackBrush
                     End If   
                     ff_control->hBackBrush = CreateSolidBrush(BackColor) 
                     ff_control->nBackColor = BackColor
                     ff_control->IsBackSysColor = False
                     SetClassLongPtr hWndControl, GCLP_HBRBACKGROUND, Cast(Integer, CreateSolidBrush(BackColor))
                     Redraw_Flag = True
                  End If   
            
            Else
            
                ' Set the Foreground color
                  If ForeColor <> -1 Then
                     ff_control->nForeColor = ForeColor
                     ff_control->IsForeSysColor = False
                     Redraw_Flag = True
                  End If   
                    
                ' Set the Background color
                  If BackColor <> -1 Then
                     If ff_control->hBackBrush Then 
                        DeleteObject ff_control->hBackBrush
                     End If   
                     ff_control->hBackBrush = CreateSolidBrush(BackColor)
                     ff_control->nBackColor = BackColor
                     ff_control->IsBackSysColor = False
                     Redraw_Flag = True
                  End If
                  
           End If
           
           If Redraw_Flag Then
              InvalidateRect hWndControl, ByVal 0, TRUE
              UpdateWindow hWndControl
           End If   
    
       End If
       
    End If
End Sub



Function FF_Control_SetLoc( ByVal hWndControl as HWnd, _
                            ByVal nLeft as Long, _
                            ByVal nTop as Long _
                            ) as Integer
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       
       ' Set the positioning of the window
         Function = SetWindowPos( hWndControl, 0, nLeft, nTop, 0, 0, _
                      SWP_NOZORDER Or SWP_NOSIZE  Or SWP_NOACTIVATE)
         
    End If
       

End Function



Sub FF_Control_SetTag( ByVal hWnd As HWND, _
                       ByRef NewTag As String )
                    
   If IsWindow(hWnd) = 0 Then Exit Sub
    
   Dim pzString As ZString Ptr
   
   ' Get the pointer to the Tag string from the hWnd PROP.
   pzString = GetProp( hWnd, "FLY_TAGPROPERTY" )

   ' Free any currently allocated memory
   If pzString Then HeapFree GetProcessHeap(), 0, ByVal pzString

   If Right(NewTag, 1) <> Chr(0) Then NewTag = NewTag & Chr(0)
   
   pzString = HeapAlloc( GetProcessHeap(), HEAP_ZERO_MEMORY, Len(NewTag) + 1 ) 
   If pzString Then *pzString = NewTag            
   
   SetProp hWnd, "FLY_TAGPROPERTY", pzString

End Sub


Sub FF_Control_SetTag2( ByVal hWnd As HWND, _
                        ByRef NewTag As String )
                    
   If IsWindow(hWnd) = 0 Then Exit Sub
    
   Dim pzString As ZString Ptr
   
   ' Get the pointer to the Tag string from the hWnd PROP.
   pzString = GetProp( hWnd, "FLY_TAGPROPERTY2" )

   ' Free any currently allocated memory
   If pzString Then HeapFree GetProcessHeap(), 0, ByVal pzString

   If Right(NewTag, 1) <> Chr(0) Then NewTag = NewTag & Chr(0)
   
   pzString = HeapAlloc( GetProcessHeap(), HEAP_ZERO_MEMORY, Len(NewTag) + 1 ) 
   If pzString Then *pzString = NewTag            
   
   SetProp hWnd, "FLY_TAGPROPERTY2", pzString

End Sub



Sub FF_Control_SetText( ByVal hWndControl As HWND, _
                        ByRef TheText As String )

    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       
       ' Set the control's text
       SetWindowText hWndControl, TheText
    
    End If
End Sub



''
''  FF_PARSE
''  Return a delimited field from a string expression.
''
''  Delimiter contains a string of one or more characters that must be fully matched to be successful.
''  If nPosition evaluates to zero or is outside of the actual field count, an empty string is returned.
''  If nPosition is negative then fields are searched from the right to left of the MainString.
''  Delimiters are case-sensitive,
''
Function FF_Parse( ByRef sMainString as String, _
                   ByRef sDelimiter  as String, _
                   ByVal nPosition   as Integer _
                   ) as String
   ' The parse must match the entire deliminter string
   Function = FF_Parse_Internal( sMainString, sDelimiter, nPosition, False, Len(sDelimiter) )
End Function





''
''  FF_PARSE_INTERNAL
''  用于 FF_Parse 和 FF_ParseAny （内部函数）
''
Function FF_Parse_Internal( ByRef sMainString   as String, _
                            ByRef sDelimiter    as String, _
                            ByRef nPosition     as Integer, _
                            ByRef nIsAny        as Integer, _
                            ByRef nLenDelimiter as Integer _
                            ) as String
                   
    ' Returns the nPosition-th substring in a string sMainString with separations sDelimiter (one or more charcters),
    ' beginning with nPosition = 1

    Dim as Integer i, j, count
    Dim s as String 
    Dim fReverse as Integer = IIf(nPosition < 0, True, False)
    
    nPosition = Abs(nPosition)
    count = 0
    i = 1
    s = sMainString 
    
    If fReverse Then 
       ' Reverse search
       ' Get the start of the token (j) by searching in reverse
       If nIsAny Then
          i = InStrRev(sMainString, Any sDelimiter)
       Else
          i = InStrRev(sMainString, sDelimiter)
       End If
       Do While i > 0        ' if not found loop will be skipped
          j = i + nLenDelimiter
          count += 1
          i = i - nLenDelimiter
          If count = nPosition Then Exit Do
          If nIsAny Then
             i = InStrRev(sMainString, Any sDelimiter, i )
          Else
             i = InStrRev(sMainString, sDelimiter, i )
          End If   
       Loop
       If i = 0 Then j = 1

       ' Now continue forward to get the end of the token
       If nIsAny Then
          i = InStr(j, sMainString, Any sDelimiter)
       Else
          i = InStr(j, sMainString, sDelimiter)
       End If
       If (i > 0) Or (count = nPosition) Then
           If i = 0 Then
               s = Mid(sMainString, j)
           Else
               s = Mid(sMainString, j, i - j)
           End If
       End If
    
    Else              
    
       ' Forward search
       Do
           j = i
           If nIsAny Then
              i = InStr(i, sMainString, Any sDelimiter)
           Else
              i = InStr(i, sMainString, sDelimiter)
           End If   
           If i > 0 Then count += 1: i += nLenDelimiter
       Loop Until (i = 0) Or (count = nPosition)

       If (i > 0) Or (count = nPosition - 1) Then
           If i = 0 Then
               s = Mid(sMainString, j)
           Else
               s = Mid(sMainString, j, i - nLenDelimiter - j)
           End If
       End If
    End If
      
    Return s

End Function



Sub DrawFrame(DC as hDC, X1 as Long, Y1 as Long, X2 as Long, Y2 as Long,C as Long, L as  Long=PS_NULL , cL as Long=0,wL as Long =1)
'画框                                                                   填充色彩  线条类型     线条色彩

    Dim hBrush as HBRUSH, hPen as HPEN
    Dim hOldBrush as HGDIOBJ, hOldPen as HGDIOBJ

If c <> -1 Then
    hBrush = CreateSolidBrush(c)
Else
    hBrush =GetStockObject(HOLLOW_BRUSH) '创建空画笔，才能画出空心来
End If
    hOldBrush = SelectObject(DC,hBrush )

    hPen = CreatePen(L, wl, cl)

    hOldPen = SelectObject(DC, hPen)

    Rectangle DC, X1, Y1, X2, Y2

    SelectObject DC, hOldPen

    DeleteObject hPen

    SelectObject DC, hOldBrush
    DeleteObject hBrush


End Sub

' ========================================================================================
' 根据Jose Roca的代码返回给定逻辑高度的字体的点大小
' ========================================================================================
Function FF_AfxGetFontPointSize( ByVal nHeight As Long ) As Long
   Dim hDC As hDC
   hDC = CreateDC("DISPLAY", ByVal Null, ByVal Null, ByVal Null)
   If hDC = Null Then Exit Function
   Dim cyPixelsPerInch As Long
   cyPixelsPerInch = GetDeviceCaps(hDC, LOGPIXELSY)
   DeleteDC hDC
   Dim nPointSize As Long
   nPointSize = MulDiv(nHeight, 72, cyPixelsPerInch)
   If nPointSize < 0 Then nPointSize = -nPointSize
   Function = nPointSize
End Function


Function FF_GetFontInfo( ByRef sCurValue  As String, _
                         ByRef sFontName  As String, _
                         ByRef nPointSize As Long, _
                         ByRef nWeight    As Long, _
                         ByRef nItalic    As Long, _
                         ByRef nUnderline As Long, _
                         ByRef nStrikeOut As Long, _
                         ByRef nFontStyle As Long _
                         ) As Long
                         
  ' FireFly处理两种类型的字体字符串。 第一个是传统的logfont字符串，第二个是简化的PB版本。 
  ' FireFly正在转向简化版本，但需要处理logfont版本以实现向后兼容性。

  '计算传入字符串中逗号的数量
  Dim nCount As Integer
  Dim i      As Integer 
  
  For i = 0 To Len(sCurValue) - 1
     If sCurValue[i] = 44 Then nCount = nCount + 1
  Next
  
  If nCount > 10 Then
     ' 这必须是旧样式的logfont结构
     ' "Tahoma,-11,0,0,0,400,0,0,0,0,3,2,1,34"        ' 400 = normal, 700 = bold
     sFontName  = FF_Parse(sCurValue, ",", 1)
     nPointSize = FF_AfxGetFontPointSize(Val(FF_Parse(sCurValue, ",", 2)))
     nWeight    = Val(FF_Parse(sCurValue, ",", 6))
     nItalic    = Val(FF_Parse(sCurValue, ",", 7)) 
     nUnderline = Val(FF_Parse(sCurValue, ",", 8))
     nStrikeOut = Val(FF_Parse(sCurValue, ",", 9)) 
     If nWeight = FW_BOLD Then nFontStyle = nFontStyle + 1   ' normal/bold
     If nItalic           Then nFontStyle = nFontStyle + 2   ' italic
     If nUnderline        Then nFontStyle = nFontStyle + 4   ' underline
     If nStrikeOut        Then nFontStyle = nFontStyle + 8   ' strikeout
  Else
     ' This is the new style (3 parts)
     sFontName  = FF_Parse(sCurValue, ",", 1)
     nPointSize = Val(FF_Parse(sCurValue, ",", 2))
     nFontStyle = Val(FF_Parse(sCurValue, ",", 3))
     nWeight = FW_NORMAL
     If (nFontStyle And 1) Then nWeight    = FW_BOLD 
     If (nFontStyle And 2) Then nItalic    = True
     If (nFontStyle And 4) Then nUnderline = True
     If (nFontStyle And 8) Then nStrikeOut = True
  End If

  Function = 0
     
End Function


Function FF_EnumCharSet( _
                       elf            As ENUMLOGFONT,   _
                       ntm            As NEWTEXTMETRIC, _
                       ByVal FontType As Long, _
                       CharSet        As Long  _
                       ) As Long
                       
    CharSet = elf.elfLogFont.lfCharSet 
    Function = True
End Function


Function FF_MakeFontEX( sFont            As String, _
                        ByVal PointSize  As Long,   _
                        ByVal fBold      As Long,   _ 
                        ByVal fItalic    As Long,   _
                        ByVal fUnderline As Long,   _
                        ByVal StrikeThru As Long    _
                        ) As HFONT

    Dim tlf      As LOGFONT
    Dim hDC      As hDC
    Dim CharSet  As Integer
    Dim m_DPI    As Integer
    
    ' 创建高dpi识别字体
    If Len(sFont) = 0 Then Exit Function

    hDC = GetDC(0)

    EnumFontFamilies hDC, ByVal StrPtr(sFont), Cast(FONTENUMPROC,@FF_EnumCharSet), ByVal Cast(lParam, VarPtr(CharSet))
    
    m_DPI = GetDeviceCaps(hDC, LOGPIXELSX)
    PointSize = (PointSize * m_DPI) \ GetDeviceCaps(hDC, LOGPIXELSY)

    tlf.lfHeight         = -MulDiv(PointSize, GetDeviceCaps(hDC, LOGPIXELSY), 72)   ' 逻辑字体高度
    tlf.lfWidth          =  0                                                       ' 平均字符宽度
    tlf.lfEscapement     =  0                                                       ' escapement
    tlf.lfOrientation    =  0                                                       ' orientation angles
    tlf.lfWeight         =  fBold                                                   ' font weight
    tlf.lfItalic         =  fItalic                                                 ' italic(TRUE/FALSE)
    tlf.lfUnderline      =  fUnderline                                              ' underline(TRUE/FALSE)
    tlf.lfStrikeOut      =  StrikeThru                                              ' strikeout(TRUE/FALSE)
    tlf.lfCharSet        =  Charset                                                 ' character set
    tlf.lfOutPrecision   =  OUT_TT_PRECIS                                           ' output precision
    tlf.lfClipPrecision  =  CLIP_DEFAULT_PRECIS                                     ' clipping precision
    tlf.lfQuality        =  DEFAULT_QUALITY                                         ' output quality
    tlf.lfPitchAndFamily =  FF_DONTCARE                                             ' pitch and family
    tlf.lfFaceName       =  sFont                                                   ' typeface name

    ReleaseDC 0, hDC

    Function = CreateFontIndirect(@tlf)

End Function               



Function FF_MakeFontEx_Internal( sFont As String ) As HFONT

   Dim hFont      As HFONT
   Dim sFontName  As String
   Dim nPointSize As Long
   Dim nWeight    As Long
   Dim nItalic    As Long
   Dim nUnderline As Long
   Dim nStrikeOut As Long 
   Dim nFontStyle As Long 
      
   FF_GetFontInfo sFont, sFontName, nPointSize, nWeight, nItalic, nUnderline, nStrikeOut, nFontStyle 
'   hFont = FF_MakeFontEX( sFontName, nPointSize, nWeight, nItalic, nUnderline, nStrikeOut )
#ifdef FF_NOSCALE
   hFont = AfxCreateFont ( sFontName, nPointSize,96, nWeight, nItalic, nUnderline, nStrikeOut )    
#else  
   hFont = AfxCreateFont ( sFontName, nPointSize,-1, nWeight, nItalic, nUnderline, nStrikeOut )    
#endif                 
   Function = hFont
   
End Function



Function IconToBitmap(ByVal HICON as HICON ,ByVal Background as ULong ) as HBITMAP 
    Dim  ret as  Integer  
    Dim as ULong  hBmp ,w,h  
    Dim  pif as ICONINFO,rt as Rect,hBrush as HBRUSH 
    Dim  DC as hDC  ,hDib as HBITMAP,bi as BITMAPINFO,lpBits as  Integer  ,oldobj as HGDIOBJ 
    
   GetIconInfo(HICON,@pif)
    w=pif.xHotspot * 2 
    h=pif.yHotspot * 2
    bi.bmiHeader.biSize=SizeOf(bi.bmiHeader)
    bi.bmiHeader.biPlanes = 1
    bi.bmiHeader.biBitCount =24
    bi.bmiHeader.biWidth = w
    bi.bmiHeader.biHeight = h
    bi.bmiHeader.biSizeImage = 4 * ((w * bi.bmiHeader.biBitCount + 31) \ 32 ) * h
    
    DC=CreateCompatibleDC(0)
    
    hdib=CreateDIBSection(DC,@bi,DIB_RGB_COLORS,CPtr( any ptr , @lpBits )   ,0,0)
    oldobj=SelectObject(DC,hDib)
    rt.Right=w
    rt.Bottom=h
    hBrush=CreateSolidBrush(Background)
    FillRect(DC,@rt,hBrush)
    DrawIconEx(DC,0,0,HICON,0,0,0,0,DI_NORMAL)
    
    hDib=SelectObject(DC,oldobj)
    
     DeleteObject(hBrush)
     DeleteDC(DC)
    Function=hDib

End Function

Function TextOut1(ByVal hDC as hDC, ByVal X as Long, ByVal Y as Long, ByVal nText as String _
                                      , ByVal hAlign as Long =0 _ 
                                      , ByVal hSize as Long =9_ 
                                      , ByVal hFont as String ="宋体" _ 
                                      , ByVal Bold as Long =400  _ 
                                      , ByVal Italic as Byte =0 _
                                      , ByVal Underline as Byte =0 _ 
                                      , ByVal StrikeOut as Byte =0 _ 
                                      , ByVal OutSIZE as SIZE Ptr =0 _ 
                                      ) as Long

   Dim  tFont as  HFONT,s as SIZE
   Dim oFont as HGDIOBJ

   SetBkMode hDC,1  '设置这个后，画上的字是透明的
    tFont =AfxCreateFont(hFont,hSize,-1,Bold, Italic,Underline,StrikeOut)
    oFont=SelectObject(hDC, tFont)
    GetTextExtentPoint32a hDC,StrPtr(nText),Len(nText),@s
    If hAlign>10000 Then' 对齐方式，=0 左对齐 >0 是显示范围的宽度，中对齐 >1万 右对齐，多出的是显示范围的宽度
        hAlign=hAlign-10000
        x=hAlign-s.cx+X
        TextOut hDC, X,Y,StrPtr(nText),Len(nText)
        Function=x
    ElseIf hAlign>0 Then
        x=(hAlign-s.cx)/2+X
        TextOut hDC, X,Y, StrPtr(nText),Len(nText)
        Function=x
    Else     
        TextOut hDC, X,Y,StrPtr(nText),Len(nText)
        Function=X
    End If
  DeleteObject tFont
  DeleteObject oFont
  If OutSIZE Then *OutSIZE=s
'文本绘图函数?也请参考SetTextAlign
'返回值Long，非零表示成功，零表示失败。会设置GetLastError
'参数 类型及说明
'hdc Long，设备场景的句柄
'x,y Long，绘图的起点，采用逻辑坐标
'lpString String，欲描绘的字串
'nCount Long，字串中要描绘的字符数量
'注解
'在一个路径中，如绘图背景模式是“不透明”（opaque）——请参考SetBkMode函数，那么创建的轮廓将由字符单元格减去字符构成。如背景模式为透明，轮廓就由字符的字样本身构成

End Function


Sub FF_CloseForm( hwndForm As HWND, _
                  ByVal ReturnValue As Integer = 0 ) 
    
    If IsWindow( hWndForm ) = False Then Exit Sub
    
    Dim hWndParent As HWND

    Dim ff As FLY_DATA Ptr 
    ff = GetProp(hwndForm, "FLY_PTR")

    If ff Then
      If ff->IsModal Then  
         ' Reset the focus back to the parent of the modal form.
         ' Enable Mouse and keyboard input for the Parent Form 
         hWndParent = Cast(HWND, GetWindowLongPtr( hWndForm, GWLP_HWNDPARENT ))
         If IsWindow(hWndParent) Then
            EnableWindow hWndParent, True
            SetActiveWindow hWndParent
         End If   
      End If                
    End If
    
    'Destroy the window. This will delete any font objects and release
    'the memory held by the type structure.
    DestroyWindow hWndForm 
    
    'Set the value that will be returned to the calling program.
    App.ReturnValue = ReturnValue
End Sub        



Sub FF_Redraw(HWND_F as HWnd )
   InvalidateRect (HWND_F , Null, True)
   UpdateWindow (HWND_F)

End Sub

