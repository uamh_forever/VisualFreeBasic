FreeBASIC Compiler - Version 1.06.0 (11-03-2017), built for win64 (64bit)
Copyright (C) 2004-2016 The FreeBASIC development team.
 --- 由勇芳软件工作室(www.yfvb.com)汉化及增强 ---
开始编译时间：2018-12-30 14:27:42，花费时间：5.958秒
目标：win64, x86-64, 64bit
编译：CODEGEN_基础方法例题_MAIN.bas -o CODEGEN_基础方法例题_MAIN.c (主模块)
C编译：D:\VisualFreeBasic_399\Compile\FreeBASICwin64\bin\win64\gcc.exe -m64 -march=x86-64 -S -nostdlib -nostdinc -Wall -Wno-unused-label -Wno-unused-function -Wno-unused-variable -Wno-unused-but-set-variable -Wno-main -Werror-implicit-function-declaration -O0 -fno-strict-aliasing -frounding-math -fno-math-errno -fwrapv -fno-exceptions -fno-unwind-tables -fno-asynchronous-unwind-tables -masm=intel "CODEGEN_基础方法例题_MAIN.c" -o "CODEGEN_基础方法例题_MAIN.asm"
组件：D:\VisualFreeBasic_399\Compile\FreeBASICwin64\bin\win64\as.exe --64 --strip-local-absolute "CODEGEN_基础方法例题_MAIN.asm" -o "CODEGEN_基础方法例题_MAIN.o"
资源：D:\VisualFreeBasic_399\Compile\FreeBASICwin64\bin\win64\GoRC.exe /ni /nw /o /machine X64 /fo "CODEGEN_BBF9B4A_RESOURCE.obj" "CODEGEN_BBF9B4A_RESOURCE.rc"
链接：D:\VisualFreeBasic_399\Compile\FreeBASICwin64\bin\win64\ld.exe -m i386pep -o "基础方法例题.exe" -subsystem console "D:\VisualFreeBasic_399\Compile\FreeBASICwin64\lib\win64\fbextra.x" --stack 1048576,1048576 -s -L "D:\VisualFreeBasic_399\Compile\lib\64" -L "D:\VisualFreeBasic_399\Compile\FreeBASICwin64\lib\win64" -L "." "D:\VisualFreeBasic_399\Compile\FreeBASICwin64\lib\win64\crt2.o" "D:\VisualFreeBasic_399\Compile\FreeBASICwin64\lib\win64\crtbegin.o" "D:\VisualFreeBasic_399\Compile\FreeBASICwin64\lib\win64\fbrt0.o" "CODEGEN_基础方法例题_MAIN.o" "CODEGEN_BBF9B4A_RESOURCE.obj" "-(" -lkernel32 -lgdi32 -lmsimg32 -luser32 -lversion -ladvapi32 -limm32 -lddraw -ldxguid -luuid -lole32 -loleaut32 -lgdiplus -lwinspool -lcomctl32 -lpsapi -lshell32 -lcomdlg32 -lshlwapi -luxtheme -lfb -lgcc -lmsvcrt -lmingw32 -lmingwex -lmoldname -lgcc_eh "-)" "D:\VisualFreeBasic_399\Compile\FreeBASICwin64\lib\win64\crtend.o" 
