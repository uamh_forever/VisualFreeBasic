'------------------------------------------------------------------------------
' 生成源代码Visual Free Basic 版本：3.91
' 生成：星期日  十二月 30, 2018 at 02:27:42 下午
' 更多信息访问 www.planetsquires.com 
'------------------------------------------------------------------------------



'------------------------------------------------------------------------------
' FireFly 自动生成的 FORM2 函数
'------------------------------------------------------------------------------

Function FORM2_Show( ByVal hWndParent As HWND = HWND_DESKTOP, _
                             ByVal ShowModalFlag As Long = 0, _
                             ByVal UserData As Long = 0 _
                             )  As HWND

    Dim szClassName     As zString * MAX_PATH
    Dim wce             As WndClassEx

    Dim FLY_nLeft       As Long    
    Dim FLY_nTop        As Long   
    Dim FLY_nWidth      As Long   
    Dim FLY_nHeight     As Long              
    
    Dim hWndForm        As HWND
    Dim IsMDIForm       as Long   
          
    Static IsInitialized  As Long   
   

    

    '确定类是否已经存在。 如果没有，则创建该类。
    
    szClassName       = "FORM_基础方法例题_FORM2_CLASS"
    
    wce.cbSize        = SizeOf(wce)

    IsInitialized = GetClassInfoEx( App.hInstance, szClassName, varptr(wce) )
	
    If IsInitialized = 0 Then
        wce.cbSize        = SizeOf(wce)
        wce.STYLE         = CS_VREDRAW Or CS_HREDRAW Or CS_DBLCLKS
        wce.lpfnWndProc   = Cast(WNDPROC, @FORM2_FORMPROCEDURE)
        wce.cbClsExtra    = 0
        wce.cbWndExtra    = 0
        wce.hInstance     = App.hInstance
        wce.hIcon         = LoadIcon( App.hInstance, ByVal IDI_APPLICATION ) 
        wce.hCursor       = LoadCursor( 0, ByVal IDC_ARROW )
        wce.hbrBackground = Cast(HBRUSH, COLOR_BTNFACE + 1)
        wce.lpszMenuName  = 0
        wce.lpszClassName = VarPtr( szClassName )
        wce.hIconSm       = LoadIcon( App.hInstance, ByVal IDI_APPLICATION )
    
        If RegisterClassEx(varptr(wce)) Then IsInitialized = TRUE         
    End If  
    

    ' // 如果这是我们正在创建的MDI表单，请设置标志
    IsMDIForm = FALSE

    ' // 将可选的UserData保存在窗口过程CREATE消息中
    App.ReturnValue = UserData

    ' 设置即将创建的窗口的大小/位置
    FLY_nLeft  = afxScaleX(0):  FLY_nTop    = afxScaleY(0)
    FLY_nWidth = afxScaleX(500): FLY_nHeight = afxScaleY(310)

    ' Form is to be centered, therefore make the necessary adjustment
    FLY_nLeft = (GetSystemMetrics( SM_CXSCREEN ) - FLY_nWidth) \ 2
    FLY_nTop  = (GetSystemMetrics( SM_CYSCREEN ) - FLY_nHeight) \ 2

    ' 使用注册的类创建一个窗口
    hWndForm = CreateWindowEx( WS_EX_WINDOWEDGE Or WS_EX_CONTROLPARENT _
                                  Or WS_EX_LEFT Or WS_EX_LTRREADING Or WS_EX_RIGHTSCROLLBAR _
                                 , _
                               szClassName, _       ' window class name
                               "Form2", _       ' window caption
                               WS_POPUP Or WS_THICKFRAME Or WS_CAPTION Or WS_SYSMENU _
                                  Or WS_MINIMIZEBOX Or WS_MAXIMIZEBOX Or WS_CLIPSIBLINGS _
                                  Or WS_CLIPCHILDREN , _
                               FLY_nLeft, FLY_nTop, FLY_nWidth, FLY_nHeight, _ 
                               hwndParent, _                  ' parent window handle
                               Cast(HMENU, Cast(LONG_PTR, Null)), _    ' window menu handle/child identifier
                               App.hInstance, _               ' program instance handle
                               ByVal Cast(LPVOID, Cast(LONG_PTR, UserData)) )                      
    
    If IsWindow(hWndForm) = 0 Then 
       Function = 0: Exit Function
    End If
    
   
    
    
    
    Function = FLY_DoMessagePump( ShowModalFlag, hWndForm, hWndParent, SW_SHOWNORMAL, IsMDIForm )

    
    
End Function



'------------------------------------------------------------------------------
' 创建所有的子控件 FORM2 
'------------------------------------------------------------------------------
Sub FORM2_CreateControls( ByVal hWndForm As HWND ) 
                                                    
   ' All of the child controls for the FORM2 form are
   ' created in this subroutine. This subroutine is called
   ' from the WM_CREATE message of the form.
    
    Dim FLY_zTempString As zString * MAX_PATH

    Dim FLY_hPicture    As HANDLE
    Dim hWndControl     As HWND
    Dim FLY_ScrollInfo  As ScrollInfo
    Dim FLY_TBstring    As String
    Dim FLY_Notify      As NMHDR    
    Dim FLY_ImageListNormal As HANDLE, FLY_ImageListDisabled As HANDLE, FLY_ImageListHot As HANDLE

    Dim FLY_hStatusBar   As HWND, FLY_hRebar  As HWND, FLY_hToolBar As HWND
    Dim FLY_tempLong     As Long, FLY_hFont   As HFONT, FLY_hIcon    As HICON
    Dim FLY_hMemDC       As HDC, FLY_ImageIndex  As Long   
    Dim FLY_ClientOffset As Long

    Dim FLY_RECT         As Rect  
    Dim FLY_TabRect      As Rect  
    
    Dim FLY_TCITEM       As TC_ITEM
    Dim uVersion         As OSVERSIONINFO
    Dim DateRange(1)     As SYSTEMTIME
    
         
    Dim ff               As FLY_DATA Ptr 
    Dim FLY_child        As FLY_DATA Ptr 
    Dim FLY_parent       As FLY_DATA Ptr 

    FLY_parent = GetProp( hWndForm, "FLY_PTR" )

                                  

    '------------------------------------------------------------------------------
    ' 创建 COMMAND2  [CommandButton] 控件.
    '------------------------------------------------------------------------------
    hWndControl = CreateWindowEx( WS_EX_LEFT Or WS_EX_LTRREADING, _
                                  "Button", _  
                                  "设Form1为父窗口", _ 
                                  WS_CHILD Or WS_VISIBLE Or WS_TABSTOP Or BS_TEXT _
                                  Or BS_PUSHBUTTON Or BS_NOTIFY Or BS_CENTER Or BS_VCENTER _
                                 , _
                                  afxScaleX(32), afxScaleY(FLY_ClientOffset + 96), _
                                  afxScaleX(198), afxScaleY(31), _ 
                                  hWndForm, Cast(HMENU, Cast(LONG_PTR,IDC_FORM2_COMMAND2)), _
                                  App.hInstance, ByVal 0 )
    
    ff = FLY_SetControlData( hWndControl, TRUE, TRUE, _
                      "微软雅黑,9,0", 0, FALSE, _
                      FALSE, FALSE, 0, _
                      0, Cast(HBRUSH,-1), Cast(WNDPROC, @FORM2_CODEPROCEDURE), _
                      "", FALSE )

    ' 如果此控件具有指定的工具提示，则立即进行设置
    FLY_zTempString = ""
    If Len(FLY_zTempString) then
       FF_AddToolTip hWndControl, FLY_zTempString, FALSE
    End If   

    ' 设置Control的Tag属性
    FF_Control_SetTag hWndControl, ""
    FF_Control_SetTag2 hWndControl, ""
     
    HWND_FORM2_COMMAND2 = hWndControl
    FORM2_COMMAND2.Hwnd = hWndControl 

 

                                  

    '------------------------------------------------------------------------------
    ' 创建 COMMAND1  [CommandButton] 控件.
    '------------------------------------------------------------------------------
    hWndControl = CreateWindowEx( WS_EX_LEFT Or WS_EX_LTRREADING, _
                                  "Button", _  
                                  "多个窗口2控件操作", _ 
                                  WS_CHILD Or WS_VISIBLE Or WS_TABSTOP Or BS_TEXT _
                                  Or BS_PUSHBUTTON Or BS_NOTIFY Or BS_CENTER Or BS_VCENTER _
                                 , _
                                  afxScaleX(32), afxScaleY(FLY_ClientOffset + 48), _
                                  afxScaleX(199), afxScaleY(33), _ 
                                  hWndForm, Cast(HMENU, Cast(LONG_PTR,IDC_FORM2_COMMAND1)), _
                                  App.hInstance, ByVal 0 )
    
    ff = FLY_SetControlData( hWndControl, TRUE, TRUE, _
                      "宋体,9,0", 0, FALSE, _
                      FALSE, FALSE, 0, _
                      0, Cast(HBRUSH,-1), Cast(WNDPROC, @FORM2_CODEPROCEDURE), _
                      "", FALSE )

    ' 如果此控件具有指定的工具提示，则立即进行设置
    FLY_zTempString = ""
    If Len(FLY_zTempString) then
       FF_AddToolTip hWndControl, FLY_zTempString, FALSE
    End If   

    ' 设置Control的Tag属性
    FF_Control_SetTag hWndControl, ""
    FF_Control_SetTag2 hWndControl, ""
     
    HWND_FORM2_COMMAND1 = hWndControl
    FORM2_COMMAND1.Hwnd = hWndControl 

 

                                  

    '------------------------------------------------------------------------------
    ' 创建 TEXT1  [TextBox] 控件.
    '------------------------------------------------------------------------------
    hWndControl = CreateWindowEx( WS_EX_CLIENTEDGE Or WS_EX_LEFT Or WS_EX_LTRREADING _
                                  Or WS_EX_RIGHTSCROLLBAR, _
                                  "Edit", _  
                                  "", _ 
                                  WS_CHILD Or WS_VISIBLE Or WS_TABSTOP Or ES_LEFT _
                                  Or ES_AUTOHSCROLL, _
                                  afxScaleX(32), afxScaleY(FLY_ClientOffset + 16), _
                                  afxScaleX(284), afxScaleY(21), _ 
                                  hWndForm, Cast(HMENU, Cast(LONG_PTR,IDC_FORM2_TEXT1)), _
                                  App.hInstance, ByVal 0 )
    
    ff = FLY_SetControlData( hWndControl, TRUE, TRUE, _
                      "宋体,9,0", 0, TRUE, _
                      TRUE, TRUE, COLOR_WINDOWTEXT, _
                      COLOR_WINDOW, Cast(HBRUSH,-1), Cast(WNDPROC, @FORM2_CODEPROCEDURE), _
                      "", FALSE )

    ' 如果此控件具有指定的工具提示，则立即进行设置
    FLY_zTempString = ""
    If Len(FLY_zTempString) then
       FF_AddToolTip hWndControl, FLY_zTempString, FALSE
    End If   

    ' 设置Control的Tag属性
    FF_Control_SetTag hWndControl, ""
    FF_Control_SetTag2 hWndControl, ""
     
    HWND_FORM2_TEXT1 = hWndControl
    FORM2_TEXT1.Hwnd = hWndControl 
    SetWindowText hWndControl, "Text1"
    SendMessage hWndControl, EM_SETLIMITTEXT, 0, 0
    SendMessage hWndControl, EM_SETMARGINS, EC_LEFTMARGIN Or EC_RIGHTMARGIN, MAKELONG( 0, 0)

 
    ' 设置标志，当TextBox控件选择所有文本时将获得焦点。
    ff->SelText = FALSE
                    

    ' 将焦点设置为Tab顺序中的最低控件
    SetFocus HWND_FORM2_COMMAND2    
    
End Sub                                                    




'------------------------------------------------------------------------------
' FORM2_FORMPROCEDURE
'------------------------------------------------------------------------------
Function FORM2_FORMPROCEDURE( ByVal hWndForm As HWND, _
                                      ByVal wMsg   As uInteger, _ 
                                      ByVal wParam As WPARAM, _
                                      ByVal lParam As LPARAM _
                                      ) As LRESULT
  
    Dim FLY_UserData        As Long Ptr
    Dim FLY_pNotify         As NMHDR Ptr
    Dim FLY_pTBN            As TBNOTIFY Ptr
    Dim FLY_lpToolTip       As TOOLTIPTEXT Ptr
    Dim FLY_zTempString     As zString * MAX_PATH
    Dim FLY_TCITEM          As TC_ITEM
    Dim FLY_bm              As Bitmap  
    Dim FLY_Rect            As Rect
    Dim FLY_TabRect         As Rect
    Dim FLY_StatusRect      As Rect 
    Dim FLY_ToolbarRect     As Rect 
    Dim FLY_ProcAddress     As Long
    Dim FLY_hFont           As HFONT
    Dim FLY_hDC             As HDC
    Dim FLY_hForeColor      As Long
    Dim FLY_hBackBrush      As Long
    Dim FLY_hwndParent      As HWND
    Dim FLY_nResult         As HANDLE 
    Dim FLY_tempLong        As Long   
    Dim FLY_ControlIndex    As Long
    Dim FLY_StatusBarHeight As Long
    Dim FLY_ToolBarHeight   As Long
    
    Dim ff As FLY_DATA Ptr 
    Dim FLY_control As FLY_DATA Ptr
    If IsWindow(hWndForm) And (wMsg <> WM_CREATE) Then ff = GetProp(hWndForm, "FLY_PTR")
    

    ' 处理任何工具栏/状态栏的大小
    If wMsg = WM_SIZE Then                      


   
       ' 初始化调整大小规则数据（如果需要）
       EnumChildWindows hWndForm, Cast(WNDENUMPROC, @FLY_ResizeRuleInitEnum), 0
       If wParam <> SIZE_MINIMIZED Then
          EnumChildWindows hWndForm, Cast(WNDENUMPROC, @FLY_ResizeRuleEnum), 0
       End If
    End If       


    If wMsg = WM_ACTIVATE then
       If wParam = FALSE then
          gFLY_hDlgCurrent = 0
       Else
          gFLY_hDlgCurrent = hWndForm
       End If
    End If


    ' 获取控件Index（如果有）
    Select Case wMsg
       Case WM_COMMAND, WM_HSCROLL, WM_VSCROLL
          If lParam <> 0 Then FLY_nResult = Cast(HANDLE, lParam)
       Case WM_NOTIFY
          FLY_pNotify = Cast(NMHDR Ptr,lParam)
          If FLY_pNotify->hWndFrom <> 0 Then FLY_nResult = FLY_pNotify->hWndFrom
    End Select    
    If FLY_nResult Then FLY_control = GetProp( Cast(HWND,FLY_nResult), "FLY_PTR" )
    If FLY_control Then FLY_ControlIndex = FLY_control->ControlIndex Else FLY_ControlIndex = -1
       


    ' 以下CASE从表单上的控件以及用户自己处理的任何这些消息中调用通知事件。
    Select Case wMsg
       
       Case WM_COMMAND
          If (LoWord(wParam) = IDC_FORM2_COMMAND1) And (HiWord(wParam) = BN_CLICKED) Then
             FLY_tempLong = FORM2_COMMAND1_BN_CLICKED (FLY_ControlIndex, hWndForm, Cast(HWND, lParam), LoWord(wParam))
             If FLY_tempLong Then Function = FLY_tempLong: Exit Function
          End If
          If (LoWord(wParam) = IDC_FORM2_COMMAND2) And (HiWord(wParam) = BN_CLICKED) Then
             FLY_tempLong = FORM2_COMMAND2_BN_CLICKED (FLY_ControlIndex, hWndForm, Cast(HWND, lParam), LoWord(wParam))
             If FLY_tempLong Then Function = FLY_tempLong: Exit Function
          End If

        
       Case WM_HSCROLL

       
       Case WM_VSCROLL

       
       Case WM_NOTIFY

    

    
    End Select

    ' 如有必要，处理任何自定义消息。
          

    

    ' 以下CASE处理FireFly内部要求
    Select Case wMsg  
      
       Case WM_GETMINMAXINFO
          ' 不要为MDI子窗体处理此消息，因为它会干扰子窗体的最大化。
          If ( GetWindowLongPtr(hWndForm, GWL_EXSTYLE) And WS_EX_MDICHILD ) <> WS_EX_MDICHILD Then 
             DefWindowProc hWndForm, wMsg, wParam, lParam 
             Dim FLY_pMinMaxInfo As MINMAXINFO Ptr
             FLY_pMinMaxInfo = Cast(MINMAXINFO Ptr,lParam)
             FLY_pMinMaxInfo->ptMinTrackSize.x = FLY_pMinMaxInfo->ptMinTrackSize.x
             FLY_pMinMaxInfo->ptMinTrackSize.y = FLY_pMinMaxInfo->ptMinTrackSize.y
             FLY_pMinMaxInfo->ptMaxTrackSize.x = FLY_pMinMaxInfo->ptMaxTrackSize.x
             FLY_pMinMaxInfo->ptMaxTrackSize.y = FLY_pMinMaxInfo->ptMaxTrackSize.y
             Function = 0: Exit Function
          End If	 
   
      Case WM_SYSCOMMAND
         If (wParam And &HFFF0) = SC_CLOSE Then
            SendMessage hWndForm, WM_CLOSE, wParam, lParam
            Exit Function
         End If
            
      Case WM_SETFOCUS
         ' 将焦点设置回正确的子控件。
         If ff Then SetFocus ff->CtrlFocus
         
      Case WM_SYSCOLORCHANGE, WM_THEMECHANGED
         ' 为窗体上的控件重新创建任何背景画笔。
         EnumChildWindows hWndForm, Cast(WNDENUMPROC, @FLY_EnumSysColorChangeProc), 0
         
         ' 将此消息转发给任何通用控件，因为它们不会自动接收此消息。

         
           
      Case WM_NOTIFY

           

      Case WM_CLOSE
          ' 如果我们正在处理模态表单，那么重新启用父表单并在此表单被销毁之前使其处于活动状态非常重要。
          If ff Then
             If ff->IsModal Then  
                 ' 将焦点重置回模态窗体的父级。
                 ' 为父窗体启用鼠标和键盘输入
                 EnableWindow ff->hWndParent, TRUE
                 SetActiveWindow ff->hWndParent
                 ShowWindow hWndForm, SW_HIDE
             End If
          End If

         


      Case WM_DESTROY 
         
         
         ' 删除任何用户定义的Timer控件
         

         HWND_FORM2 = 0
         
    
         ' 删除用于此表单/控件的字体/画笔及其关联的属性
         If ff Then
             If ff->hBackBrush Then DeleteObject(Cast(HGDIOBJ, ff->hBackBrush))
             

             ' 如果已创建，则销毁加速器表
             If ff->hAccel Then DestroyAcceleratorTable ff->hAccel
             If ff->hStatusBarTimer Then KillTimer ByVal 0, ff->hStatusBarTimer
             
             ' 重新声明类型结构使用的内存
             HeapFree GetProcessHeap(), 0, ByVal Cast(PVOID,ff)

             ' 删除包含Tag属性字符串的属性。
             FLY_nResult = GetProp( hWndForm, "FLY_TAGPROPERTY" )
             If FLY_nResult Then 
                HeapFree GetProcessHeap(), 0, ByVal Cast(PVOID,FLY_nResult)
                RemoveProp hWndForm, "FLY_TAGPROPERTY"
             End If   
             FLY_nResult = GetProp( hWndForm, "FLY_TAGPROPERTY2" )
             If FLY_nResult Then 
                HeapFree GetProcessHeap(), 0, ByVal Cast(PVOID,FLY_nResult)
                RemoveProp hWndForm, "FLY_TAGPROPERTY2"
             End If   

         End If
         
         
         
         RemoveProp hWndForm, "FLY_PTR"
         
         
         Function = 0: Exit Function
  

      Case WM_CREATE       
          
         ff = HeapAlloc( GetProcessHeap(), HEAP_ZERO_MEMORY, SizeOf(*ff) )
         If ff Then
            SetProp hWndForm, "FLY_PTR", ff  ' 存储指针以供以后使用
            ff->IsForm = TRUE                ' 这是一个表单，而不是一个控件。
         Else
            Function = -1 : Exit Function   ' 返回-1以中断动作
         End If

         ' 检索通过Show函数传递的任何用户定义值。
         FLY_UserData = Cast(Long Ptr,lParam)
         

         ' 为此表单设置共享变量。
         HWND_FORM2 = hWndForm  
         
         FORM2.HWnd = hWndForm   
         
         

         

         ' 设置表单的标记属性
         FF_Control_SetTag hWndForm, ""
         FF_Control_SetTag2 hWndForm, ""

         FORM2_CreateControls hWndForm

         ' 必要时创建键盘加速器
         

         ' 正在创建MDIClient（如果可应用），但尚未显示。 允许用户在显示命令之前处理它们。
         
    
         ' 表单正在创建，但尚未显示。 允许用户在显示表单控件之前处理其命令。
         
         
         ' 创建任何用户定义的Timer控件
         

      Case WM_ERASEBKGND
         


      Case WM_TIMER


        
      
      Case WM_CTLCOLOREDIT, _
           WM_CTLCOLORLISTBOX, _
           WM_CTLCOLORSTATIC, _
           WM_CTLCOLORBTN
           
         ' 从子控件收到的消息即将显示。
         ' 在此处设置颜色属性。
         ' lParam是Control的句柄。 wParam是hDC。
         FLY_hDC = Cast(HDC,wParam)          
         
         FLY_control = GetProp(Cast(HWND,lParam), "FLY_PTR")
         If FLY_control = 0 Then
            FLY_control = GetProp( GetFocus(), "FLY_PTR") 
            If FLY_control = 0 Then Exit Select
         End If
         
         If FLY_control->ProcessCtlColor Then
  
             SetTextColor FLY_hDC, IIF( FLY_control->IsForeSysColor, GetSysColor( FLY_control->nForeColor ), FLY_control->nForeColor )
             SetBkColor   FLY_hDC, IIF( FLY_control->IsBackSysColor, GetSysColor( FLY_control->nBackColor ), FLY_control->nBackColor )
             
             ' 如果这是一个TextBox，那么我们必须使用OPAQUE STYLE，否则在滚动时我们会在控件中获得大量的屏幕垃圾。
             GetClassName FLY_control->hWndControl, FLY_zTempString, SizeOf(FLY_zTempString)
             
             ' 返回用于绘制背景的画笔的句柄
             Function = Cast(LRESULT,FLY_control->hBackBrush)

             If UCase(FLY_zTempString) = "EDIT" Then     
                SetBkMode FLY_hDC, OPAQUE                                     

                ' 如果禁用该控件，则尝试使用禁用的颜色
                If IsWindowEnabled( Cast(HWND,lParam) ) = FALSE Then
                   SetBkColor FLY_hDC, GetSysColor( COLOR_BTNFACE )
                   Function = Cast(LRESULT,GetSysColorBrush( COLOR_BTNFACE ))
                End If
             
             Else
                SetBkMode FLY_hDC, TRANSPARENT
             End If
              
             Exit Function
             
         End If
      
    End Select
    
  
    Function = DefWindowProc( hWndForm, wMsg, wParam, lParam )

End Function

    
    
'------------------------------------------------------------------------------
' FORM2_CODEPROCEDURE
'------------------------------------------------------------------------------
Function FORM2_CODEPROCEDURE( ByVal hWndControl As HWND, _
                                      ByVal wMsg   As uInteger, _ 
                                      ByVal wParam As WPARAM, _
                                      ByVal lParam As LPARAM _
                                      ) As LRESULT
  
    ' FORM2 表单上每个控件的所有消息都在此函数中处理。
    
    Dim FLY_ProcAddress  As WNDPROC
    Dim FLY_hFont        As HFONT
    Dim FLY_hBackBrush   As Long   
    Dim FLY_nResult      As HANDLE   
    Dim FLY_tempLong     As Long   
    Dim FLY_TopForm      As Long   
    Dim FLY_ControlIndex As Long   
    Dim FLY_TCITEM       As TC_ITEM
    Dim uVersion         As OSVERSIONINFO
    Dim FLY_zTempString  As zString * MAX_PATH
    Dim FLY_Rect         As Rect

    Dim ff As FLY_DATA Ptr 
    Dim FLY_parent As FLY_DATA Ptr 
    If hWndControl Then ff = GetProp(hWndControl, "FLY_PTR")
    If ff Then FLY_ControlIndex = ff->ControlIndex Else FLY_ControlIndex = -1
    

    ' 以下CASE在处理用户定义事件之前处理内部FireFly要求（这些事件在此之后的单独CASE中处理）。
    Select Case wMsg
      Case WM_DESTROY 
         
         
         ' 销毁控件
         If ff Then
          
             ' 删除用于此Control的画笔
             If ff->hBackBrush Then DeleteObject(Cast(HGDIOBJ, ff->hBackBrush))
             
             FLY_ProcAddress = ff->OldProc 

             ' 重新声明类型结构使用的内存
             If ff Then HeapFree(GetProcessHeap(), 0, ByVal Cast(PVOID,ff))
             RemoveProp hWndControl, "FLY_PTR"
             
             ' 删除包含Tag属性字符串的属性。
             FLY_nResult = GetProp(hWndControl, "FLY_TAGPROPERTY")
             If FLY_nResult Then
                HeapFree GetProcessHeap(), 0, ByVal Cast(PVOID,FLY_nResult)
                RemoveProp hWndControl, "FLY_TAGPROPERTY"
             End If
             FLY_nResult = GetProp(hWndControl, "FLY_TAGPROPERTY2")
             If FLY_nResult Then
                HeapFree GetProcessHeap(), 0, ByVal Cast(PVOID,FLY_nResult)
                RemoveProp hWndControl, "FLY_TAGPROPERTY2"
             End If

             DeleteObject ff->hFont
             
             ' 允许控件处理WM_DESTROY消息。
             SetWindowLongPtr hWndControl, GWLP_WNDPROC, Cast(LONG_PTR,FLY_ProcAddress)
             CallWindowProc Cast(WNDPROC,FLY_ProcAddress), hWndControl, wMsg, wParam, lParam

         End If
         
         Function = 0: Exit Function
      




      Case WM_SETFOCUS                                                          
         ' 如果这是一个TextBox，我们检查是否需要突出显示文本。
         If ff Then
            If ff->SelText Then 
               SendMessage hWndControl, EM_SETSEL, 0, -1
            Else
               SendMessage hWndControl, EM_SETSEL, -1, 0
            End If   
         End If
         
         ' 将焦点控件存储在父窗体中如果此窗体是TabControl子窗体，则需要将CtrlFocus存储在此子窗体的父窗体中。
         FLY_tempLong = GetWindowLongPtr(GetParent(hWndControl), GWL_STYLE)
         If (FLY_tempLong And WS_CHILD) = WS_CHILD Then
            ' 必须是TabControl子对话框
            FLY_parent = GetProp(GetParent(GetParent(hWndControl)), "FLY_PTR")
         Else   
            FLY_parent = GetProp(GetParent(hWndControl), "FLY_PTR")
         End If   
         If FLY_parent Then FLY_parent->CtrlFocus = hWndControl
        

      Case WM_SIZE, WM_MOVE
         ' 处理自动调整大小的任何TabControl子页面。 如果TabControl更改大小，则会在此处捕获。 我们只需要处理TabControlChildAutoSize表单，因为其他表单保持固定大小。  

       
    End Select
       
  
    ' 以下情况调用每个用户定义的函数。
    Select Case wMsg

       Case 0
      
    End Select
  
    ' 如有必要，处理任何自定义消息。

  
    ' 此控件是子类，因此我们必须将所有未处理的消息发送到原始窗口过程。
    Function = CallWindowProc( ff->OldProc, hWndControl, wMsg, wParam, lParam )

End Function




'--------------------------------------------------------------------------------
Function FORM2_COMMAND1_BN_CLICKED( _
                                  ControlIndex     As Long, _      ' 控件数组的索引
                                  hWndForm         As HWnd, _      ' 窗体句柄
                                  hWndControl      As HWnd, _      ' 控件句柄
                                  idButtonControl  As Long   _     ' 按钮的标识符
                                  ) As Long
  '注意！！！这是个多开窗口，使用控件方法必须多此一步
  '类方式 ==================================================================
  FORM2_TEXT1.hWndForm = hWndForm
  FORM2_TEXT1.Text ="多开同个窗口，必须用此"
  '函数方式 ===================================================================
'  FF_Control_SetText   GetDlgItem(hWndForm, IDC_FORM2_TEXT1), "多开同个窗口，用 IDC_  控件句柄：" & GetDlgItem(hWndForm, IDC_FORM2_TEXT1)
  Function = 0   ' 根据你的需要改变
End Function


'                                                                                  
Function Form2_Command2_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
  FORM2.hWndForm  = hWndForm
  FORM2.hWndParent= form1.hWnd 

   Function = 0   ' 根据你的需要改变
End Function

