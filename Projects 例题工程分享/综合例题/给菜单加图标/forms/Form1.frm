﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=这里演示用代码创建菜单和加图标， 当然还有菜单控件，直接用控件操作
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=500
Height=310
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=这里演示用代码创建菜单和加图标， 当然还有菜单控件，直接用控件操作
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=35
Top=91
Width=428
Height=41
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]
'==================================================================================================================================
Enum
   IDM_UNDO = 1001   ' Undo
   IDM_REDO          ' Redo
   IDM_HOME          ' Home
   IDM_SAVE          ' Save
   IDM_EXIT          ' Exit
End Enum
Function BuildMenu () As HMENU

   Dim hMenu As HMENU
   Dim hPopUpMenu As HMENU

   hMenu = CreateMenu
   hPopUpMenu = CreatePopupMenu
      AppendMenuW hMenu, MF_POPUP Or MF_ENABLED, Cast(UINT_PTR, hPopUpMenu), "&File"
         AppendMenuW hPopUpMenu, MF_ENABLED, IDM_UNDO, "&Undo" & Chr(9) & "Ctrl+U"
         AppendMenuW hPopUpMenu, MF_ENABLED, IDM_REDO, "&Redo" & Chr(9) & "Ctrl+R"
         AppendMenuW hPopUpMenu, MF_ENABLED, IDM_HOME, "&Home" & Chr(9) & "Ctrl+H"
         AppendMenuW hPopUpMenu, MF_ENABLED, IDM_SAVE, "&Save" & Chr(9) & "Ctrl+S"
         AppendMenuW hPopUpMenu, MF_ENABLED, IDM_EXIT, "E&xit" & Chr(9) & "Alt+F4"
   Function = hMenu

End Function
'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。

   Dim hMenu As HMENU = BuildMenu
   SetMenu hWndForm, hMenu

   ' // Add icons to the items of the File menu
   Dim hSubMenu As HMENU = GetSubMenu(hMenu, 0)
   AfxAddIconToMenuItem(hSubMenu, 0, True, AfxGdipIconFromRes(App.HINSTANCE, "PNG_ARROW_LEFT_32"))
   AfxAddIconToMenuItem(hSubMenu, 1, True, AfxGdipIconFromRes(App.HINSTANCE, "PNG_ARROW_RIGHT_32"))
   AfxAddIconToMenuItem(hSubMenu, 2, True, AfxGdipIconFromRes(App.HINSTANCE, "PNG_HOME_32"))
   AfxAddIconToMenuItem(hSubMenu, 3, True, AfxGdipIconFromRes(App.HINSTANCE, "PNG_SAVE_32"))

End Sub


'                                                                                  
Sub Form1_WM_Command(hWndForm As hWnd, hWndControl As hWnd, wNotifyCode As Long, wID As Long)  '命令处理（处理菜单、工具栏、状态栏等）

  Select Case wID
      Case IDM_UNDO
          MessageBox hWndForm, "Undo option clicked", "Menu", MB_OK
          Exit Sub
      Case IDM_REDO
          MessageBox hWndForm, "Redo option clicked", "Menu", MB_OK
          Exit Sub
      Case IDM_HOME
          MessageBox hWndForm, "Home option clicked", "Menu", MB_OK
          Exit Sub
      Case IDM_SAVE
          MessageBox hWndForm, "Save option clicked", "Menu", MB_OK
          Exit Sub
      Case IDM_EXIT
          SendMessageW hWndForm, WM_CLOSE, 0, 0
          Exit Sub
  End Select

End Sub

