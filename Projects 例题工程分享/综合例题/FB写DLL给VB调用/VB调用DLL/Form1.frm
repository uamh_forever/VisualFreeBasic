VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3030
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3030
   ScaleWidth      =   4560
   StartUpPosition =   2  '屏幕中心
   Begin VB.CommandButton Command6 
      Caption         =   "字符串数组"
      Height          =   495
      Left            =   240
      TabIndex        =   6
      Top             =   2160
      Width           =   1575
   End
   Begin VB.CommandButton Command5 
      Caption         =   "数字数组"
      Height          =   375
      Left            =   2520
      TabIndex        =   5
      Top             =   1320
      Width           =   1575
   End
   Begin VB.CommandButton Command4 
      Caption         =   "地址参数"
      Height          =   375
      Left            =   2640
      TabIndex        =   4
      Top             =   840
      Width           =   1335
   End
   Begin VB.CommandButton Command3 
      Caption         =   "字符串参数"
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   1560
      Width           =   1575
   End
   Begin VB.CommandButton Command2 
      Caption         =   "返回字符"
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   960
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "数字调用"
      Height          =   495
      Left            =   2520
      TabIndex        =   0
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   1815
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'==============处理数字变量===================
Private Declare Function JiaFa Lib "FBtoVB.dll" Alias "JIAFA@8" (ByVal a As Long, ByVal b As Long) As Long '数字例题
'Alias 里的全部是大写，不管FB里如何，一律大写
'@8 标准符号，是为了兼容C语言默认带的， 表示有2个参数，无参数是 @0 ,1个参数为 4 ，几个就 几个*4

Private Declare Sub Dizi Lib "FBtoVB.dll" Alias "DIZI@4" (ByRef a As Long) '传地址数字
'以地址方式传数字参数给FB，FB那里可以修改参数值了

Private Declare Sub LongShuZhu Lib "FBtoVB.dll" Alias "LONGSHUZHU@8" (ByVal 数组地址 As Long, ByVal 数组长度 As Long) '数组例题
'FB 和 VB 之间，处理数组的方式不同，因此无法直接传数组，数组长度是为了FB那边知道长度，不然超边界了软件会崩溃的。
'其它数字和Long 一样处理，对应FB那边类型名是
'VB类型：Byte  Integer Single Double
'FB类型：UByte Short   Single Double

'==============处理字符串变量===================
Private Declare Function GetStr Lib "FBtoVB.dll" Alias "GETSTR@0" () As String '返回字符串
'从FB里传来的字符，VB这边直接使用，无障碍。
Private Declare Function LenDuoStr Lib "FBtoVB.dll" Alias "LENDUOSTR@16" (ByVal a As String, ByVal b As String, ByVal c As String, ByVal d As String) As Long '传字符串给FB
'传VB的字符到FB那边使用。注意，FB那边中文算2个，英文算1个。
Private Declare Sub StrShuZhu Lib "FBtoVB.dll" Alias "STRSHUZHU@8" (ByVal 数组地址 As Long, ByVal 数组长度 As Long) '数组例题
'FB 和 VB 之间，处理数组的方式不同，因此无法直接传数组，数组长度是为了FB那边知道长度，不然超边界了软件会崩溃的。
'


Private Sub Command4_Click()
    Dim a As Long
    a = 100
    Dizi a
    Debug.Print a
End Sub

Private Sub Command5_Click()
 Dim a(10) As Long, i As Long

 LongShuZhu VarPtr(a(0)), UBound(a) + 1
 For i = 0 To 10
    Debug.Print a(i),
 Next
Debug.Print

End Sub

Private Sub Command6_Click()
 Dim a(10) As String, i As Long
 a(0) = "dd"
 a(2) = ""
 StrShuZhu VarPtr(a(0)), UBound(a) + 1
 For i = 0 To 10
    Debug.Print a(i),
 Next
Debug.Print
End Sub

Private Sub Form_Load()
    ChDrive App.Path '必须先设置软件文件夹，否则，万一不在EXE文件夹开启软件，会发生找不到DLL文件的错误
    ChDir App.Path

End Sub

Private Sub Command1_Click()
    Label1 = JiaFa(10, 20)
End Sub
Private Sub Command2_Click()
    Dim b As String
    b = GetStr
    Label1 = b
    Debug.Print b, Len(b)

End Sub
Private Sub Command3_Click()
    Dim a As String
    a = "w我o"
    Label1 = LenDuoStr(a, "p单位c", "", "ddd")
End Sub

