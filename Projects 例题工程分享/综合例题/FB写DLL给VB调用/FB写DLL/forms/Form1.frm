﻿#VisualFreeBasic_Form#  Version=5.0.0
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
Style=3 - 常规窗口
Icon=
Caption=Form1
Enabled=True
Visible=True
StartPosition=1 - 屏幕中心
Left=0
Top=0
Width=500
Height=310
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
WindowState=0 - 正常
MousePass=False
TransPer=0
TransColor=SYS,25
MousePointer=0 - 默认
BackColor=SYS,15
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
'==================================================================================================================================
'这是旧版本窗口，需要自己人工修改和调整才能正常使用，主要更改项目：
'1）窗口和控件属性：全部需要自己人工重新调整，升级程序只是简单的转换。
'2）窗口和控件事件：升级程序会自动更新到新版的格式，无法保证100%正确，更加自己需要查证。
'3）窗口和控件句柄：新版没有句柄常量，需要用类，如：HWND_FORM1  改为 Form1.hWnd ， HWND_FORM1_PICTURE1 改为 Form1.Picture1.hWnd 
'4）窗口和控件IDC： 新版无IDC常量，需要用类，如：IDC_FORM1_PICTURE1 改为 Form1.Picture1.IDC
'5）新弹出窗口：Form2_Show  改为 Form2.Show 
'6）与字符有关的API：以前默认为 ANSI字符，现在是 Unicode字符，因此相关API 后面要加个 A 来解决，如：PostMessage 改为 PostMessageA ，或使用宽字符变量
'7）API操作控件和窗口，有关字符的，全部为 Unicode字符 ，类型是 CWSTR，CWSTR 不可以直接用在 Len Left Right Mid InStr UCase 等一系列字符操作语句上
'必须前面加个 ** 才能当成宽字符处理（英文中文都算1个字符）如： Len(**b) ,可以赋值到ANSI变量，会自动转换编码：dim a As String = b  (b 为 CWSTR ) 
'或者 Dim b As CWSTR = a  (a 为 String) ,因此用变量过度一下，没什么问题，就是不可以直接返回就用在语句上，如：Len(FF_Control_GetText(..)) 这是错的
'8）编译后的窗口和控件是 Unicode 的，旧版是 ANSI 的在英文系统中无法显示中文。可以用API IsWindowUnicode 检查窗口和控件是不是 Unicode
'9）所有窗口和控件由 CWindow Class 类创建，旧版是直接API创建，可以参考 WinFBX 帮助来查看 CW 的众多附加功能。
'最后预祝大家编程愉快，升级顺利。有任何问题可以在编程群里提问：Basic语言编程QQ群 78458582
'==================================================================================================================================


 '这里为空，也可以写代码，窗口必须保留1个，空窗口，不用管


