﻿#FireFly_Form#  Version=4.0.1
Locked=0
ClientOffset=0
ClientWidth=484
ClientHeight=272


[ControlType] Form | PropertyCount=25 | zorder=1 | tabindex=0 | 
name=NullForm
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
backbitmap=
backbitmapmode=0 - 平铺
backcolor=SYS,15
caption=Form1
export=False
height=310
icon=
left=0
mdichild=False
minwidth=0
minheight=0
maxwidth=0
maxheight=0
multilanguage=True
startupposition=1 - 中心
tabcontrolchild=False
tabcontrolchildautosize=False
tag=
tag2=
top=0
width=500
windowstate=0 - 正常

[ControlType] Label | PropertyCount=21 | zorder=2 | tabindex=1 | 
name=Label1
WinStyle=WS_CHILD, WS_VISIBLE, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, SS_LEFT, SS_NOTIFY,WS_EX_LEFT, WS_EX_LTRREADING
backcolor=SYS,15
backstyle=1 - 不透明
caption=必须保留1个窗口，DLL用不着，就空着
controlindex=0
font=宋体,9,0
fontupgrade=False
forecolor=SYS,8
height=22
identifier=True
left=48
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=40
width=376

[ControlType] Label | PropertyCount=21 | zorder=0 | tabindex=0 | 
name=Label2
WinStyle=WS_CHILD, WS_VISIBLE, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, SS_LEFT, SS_NOTIFY,WS_EX_LEFT, WS_EX_LTRREADING
backcolor=SYS,15
backstyle=1 - 不透明
caption=这里是空窗口，编译时不会把任何有关此窗口和代码编译到EXE中的
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
height=27
identifier=True
left=48
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=88
width=371

[AllCode]
'==================================================================================================================================
'这是旧版本窗口，需要自己人工修改和调整才能正常使用，主要更改项目：
'1）窗口和控件属性：全部需要自己人工重新调整，升级程序只是简单的转换。
'2）窗口和控件事件：升级程序会自动更新到新版的格式，无法保证100%正确，更加自己需要查证。
'3）窗口和控件句柄：新版没有句柄常量，需要用类，如：HWND_FORM1  改为 Form1.hWnd ， HWND_FORM1_PICTURE1 改为 Form1.Picture1.hWnd 
'4）窗口和控件IDC： 新版无IDC常量，需要用类，如：IDC_FORM1_PICTURE1 改为 Form1.Picture1.IDC
'5）新弹出窗口：Form2_Show  改为 Form2.Show 
'6）与字符有关的API：以前默认为 ANSI字符，现在是 Unicode字符，因此相关API 后面要加个 A 来解决，如：PostMessage 改为 PostMessageA ，或使用宽字符变量
'7）API操作控件和窗口，有关字符的，全部为 Unicode字符 ，类型是 CWSTR，CWSTR 不可以直接用在 Len Left Right Mid InStr UCase 等一系列字符操作语句上
'必须前面加个 ** 才能当成宽字符处理（英文中文都算1个字符）如： Len(**b) ,可以赋值到ANSI变量，会自动转换编码：dim a As String = b  (b 为 CWSTR ) 
'或者 Dim b As CWSTR = a  (a 为 String) ,因此用变量过度一下，没什么问题，就是不可以直接返回就用在语句上，如：Len(FF_Control_GetText(..)) 这是错的
'8）编译后的窗口和控件是 Unicode 的，旧版是 ANSI 的在英文系统中无法显示中文。可以用API IsWindowUnicode 检查窗口和控件是不是 Unicode
'9）所有窗口和控件由 CWindow Class 类创建，旧版是直接API创建，可以参考 WinFBX 帮助来查看 CW 的众多附加功能。
'最后预祝大家编程愉快，升级顺利。有任何问题可以在编程群里提问：Basic语言编程QQ群 78458582
'==================================================================================================================================
'
'
'窗口名为： NullForm  时，表示空窗口 
'
'这里是空窗口，编译时不会把任何有关此窗口和代码编译到EXE中的
'
'这里一切代码，都不会被编译到EXE里。
'
'
'原因是VFB必须有个窗口才可以正常编译，
'因此需要个空窗口，这里只是形式，编译时自动剔除本代码和窗口。
'
'
'

