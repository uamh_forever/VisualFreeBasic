{visualfreebasic_lib} 'VFB函数库插入的位置(全部小写,在引用必要的基础库之后),这是让BAS工程支持使用VFB里的【VFB代码库】和【我的代码库】等,去掉后就无法支持。

#include"windows.bi"


type LCUI_Widget as any pointer

Declare Sub LCUI_Init cdecl Lib "LCUI" Alias "LCUI_Init" ()
declare function LCUI_Main cdecl Lib "LCUI" alias "LCUI_Main" () as integer
Declare Function LCUIWidget_GetRoot cdecl Lib "LCUI" Alias "LCUIWidget_GetRoot" () As LCUI_Widget
declare function LCUIBuilder_LoadFile cdecl Lib "LCUI" alias "LCUIBuilder_LoadFile" (filepath as zstring ptr) as LCUI_Widget
declare function Widget_Append cdecl Lib "LCUI" alias "Widget_Append" (container as LCUI_Widget, widget as LCUI_Widget) as integer
Declare Function Widget_Unwrap cdecl Lib "LCUI" Alias "Widget_Unwrap" (widget As LCUI_Widget) As Integer
Declare Function LCUIWidget_GetById cdecl Lib "LCUI" Alias "LCUIWidget_GetById" (idstr As ZString Ptr) As LCUI_Widget
Declare Function Widget_SetTitleW cdecl Lib "LCUI" Alias "Widget_SetTitleW" (As LCUI_Widget,idstr As Wstring Ptr) As LCUI_Widget

Dim As LCUI_Widget root, text,button,pack,INPUT_

Dim As Wstring * 255 caption = "GUI 测试"

LCUI_Init()
root = LCUIWidget_GetRoot()
Widget_SetTitleW(root,@caption)

pack = LCUIBuilder_LoadFile("hello.xml")

Widget_Append(root, pack)
Widget_Unwrap(pack)
button = LCUIWidget_GetById("btn-hello")
text = LCUIWidget_GetById("text-hello")
INPUT_ = LCUIWidget_GetById("input-hello")

LCUI_Main()
