﻿#VisualFreeBasic_Form#  Version=5.6.2
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=Form1
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=500
Height=310
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command2
Index=-1
Caption=Command2
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=290
Top=51
Width=114
Height=35
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]

 '这里演示全代码操作，建议用控件       
'--------------------------------------------------------------------------------
Sub Form1_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击

  Dim As Long i, x, y
  Print StrToWStr("")    '如果你代码中没用到此函数，就需要引用下函数，不然会报错
  Print wStrToUtf8("", 0) '如果你代码中没用到此函数，需要引用下函数，不然会报错
  Dim db As SQLite3 Ptr 'DB 为后面使用数据用
  
  If SQLiteOpen(db, App.Path & "test.db") Then
      Print "失败" 'SQLiteErrMsg(hDB)
      
      Exit Sub
  End If
  
  Dim sql As String, rs() As String
  Sql = "select * from features where id>0  LIMIT 20"
  Print  SQLiteSelect(db, sql, rs())
  Print "---------- 列名显示 --------------"
  
  
  For x = 0 To UBound(rs, 2)
      Print rs(0, x) & " , " ;
  Next
  Print
  
  Print "---------- 内容显示 --------------------"
  
  For y = 1 To UBound(rs, 1)
      For x = 0 To UBound(rs, 2)
          Print rs(y, x) & " , " ;
      Next
      Print
  Next
  
  SQLiteClose db

End Sub

