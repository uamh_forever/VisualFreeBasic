﻿#FireFly_Form#  Version=4.0.1
Locked=0
ClientOffset=0
ClientWidth=484
ClientHeight=272


[ControlType] Form | PropertyCount=25 | zorder=1 | tabindex=0 | 
name=Form1
ClassStyle=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
WinStyle=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE,WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
backbitmap=
backbitmapmode=0 - 平铺
backcolor=SYS,15
caption=Form1
export=False
height=310
icon=
left=0
mdichild=False
minwidth=0
minheight=0
maxwidth=0
maxheight=0
multilanguage=True
startupposition=1 - 中心
tabcontrolchild=False
tabcontrolchildautosize=False
tag=
tag2=
top=0
width=500
windowstate=0 - 正常

[AllCode]
'==================================================================================================================================
'这是旧版本窗口，需要自己人工修改和调整才能正常使用，主要更改项目：
'1）窗口和控件属性：全部需要自己人工重新调整，升级程序只是简单的转换。
'2）窗口和控件事件：升级程序会自动更新到新版的格式，无法保证100%正确，更加自己需要查证。
'3）窗口和控件句柄：新版没有句柄常量，需要用类，如：HWND_FORM1  改为 Form1.hWnd ， HWND_FORM1_PICTURE1 改为 Form1.Picture1.hWnd 
'4）窗口和控件IDC： 新版无IDC常量，需要用类，如：IDC_FORM1_PICTURE1 改为 Form1.Picture1.IDC
'5）新弹出窗口：Form2_Show  改为 Form2.Show 
'6）与字符有关的API：以前默认为 ANSI字符，现在是 Unicode字符，因此相关API 后面要加个 A 来解决，如：PostMessage 改为 PostMessageA ，或使用宽字符变量
'7）API操作控件和窗口，有关字符的，全部为 Unicode字符 ，类型是 CWSTR，CWSTR 不可以直接用在 Len Left Right Mid InStr UCase 等一系列字符操作语句上
'必须前面加个 ** 才能当成宽字符处理（英文中文都算1个字符）如： Len(**b) ,可以赋值到ANSI变量，会自动转换编码：dim a As String = b  (b 为 CWSTR ) 
'或者 Dim b As CWSTR = a  (a 为 String) ,因此用变量过度一下，没什么问题，就是不可以直接返回就用在语句上，如：Len(FF_Control_GetText(..)) 这是错的
'8）编译后的窗口和控件是 Unicode 的，旧版是 ANSI 的在英文系统中无法显示中文。可以用API IsWindowUnicode 检查窗口和控件是不是 Unicode
'9）所有窗口和控件由 CWindow Class 类创建，旧版是直接API创建，可以参考 WinFBX 帮助来查看 CW 的众多附加功能。
'最后预祝大家编程愉快，升级顺利。有任何问题可以在编程群里提问：Basic语言编程QQ群 78458582
'==================================================================================================================================
'==================================================================================================================================
'这是旧版本窗口，需要自己人工修改和调整才能正常使用，主要更改项目：
'1）窗口和控件属性：全部需要自己人工重新调整，升级程序只是简单的转换。
'2）窗口和控件事件：升级程序会自动更新到新版的格式，无法保证100%正确，更加自己需要查证。
'3）窗口和控件句柄：新版没有句柄常量，需要用类，如：HWND_FORM1  改为 Form1.hWnd ， HWND_FORM1_PICTURE1 改为 Form1.Picture1.hWnd 
'4）窗口和控件IDC： 新版无IDC常量，需要用类，如：IDC_FORM1_PICTURE1 改为 Form1.Picture1.IDC
'5）新弹出窗口：Form2_Show  改为 Form2.Show 
'6）与字符有关的API：以前默认为 ANSI字符，现在是 Unicode字符，因此相关API 后面要加个 A 来解决，如：PostMessage 改为 PostMessageA ，或使用宽字符变量
'7）API操作控件和窗口，有关字符的，全部为 Unicode字符 ，类型是 CWSTR，CWSTR 不可以直接用在 Len Left Right Mid InStr UCase 等一系列字符操作语句上
'必须前面加个 ** 才能当成宽字符处理（英文中文都算1个字符）如： Len(**b) ,可以赋值到ANSI变量，会自动转换编码：dim a As String = b  (b 为 CWSTR ) 
'或者 Dim b As CWSTR = a  (a 为 String) ,因此用变量过度一下，没什么问题，就是不可以直接返回就用在语句上，如：Len(FF_Control_GetText(..)) 这是错的
'8）编译后的窗口和控件是 Unicode 的，旧版是 ANSI 的在英文系统中无法显示中文。可以用API IsWindowUnicode 检查窗口和控件是不是 Unicode
'9）所有窗口和控件由 CWindow Class 类创建，旧版是直接API创建，可以参考 WinFBX 帮助来查看 CW 的众多附加功能。
'最后预祝大家编程愉快，升级顺利。有任何问题可以在编程群里提问：Basic语言编程QQ群 78458582
'==================================================================================================================================
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。
'32位与64位切换，或其它方式，点击工具栏右边。
'--------------------------------------------------------------------------------
Sub Form1_WM_Create(hWndForm As hWnd,UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
                         hWndForm As hWnd, _          ' 窗体句柄
                         ByVal UserData As Integer _  ' 可选的用户定义的值
                         ) As Long


End Sub

