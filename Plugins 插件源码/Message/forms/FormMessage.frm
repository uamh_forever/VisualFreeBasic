﻿#VisualFreeBasic_Form#  Version=5.8.2
Locked=0

[Form]
Name=FormMessage
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_SYSMENU,WS_VISIBLE,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_EX_CONTROLPARENT,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CHILD
Style=0 - 无边框
Icon=
Caption=消息框编辑器
StartPosition=0 - 手动
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=478
Height=445
TopMost=False
Child=True
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=消息标题：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=12
Top=12
Width=103
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=消息文本：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=11
Top=60
Width=103
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=（Windows 2000 及以后）
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=30
Top=385
Width=154
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=TextlpCaption
Index=-1
Style=3 - 凹边框
TextScrollBars=3 - 垂直和水平
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=True
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=11
Top=80
Width=343
Height=101
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Picture]
Name=Picture1
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
Left=291
Top=255
Width=51
Height=175
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=False
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=使用工程属性里的产品名称当标题
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=148
Top=13
Width=203
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=TextLp
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=False
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=11
Top=31
Width=343
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=测试消息(&T)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=365
Top=28
Width=104
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandCopy
Index=-1
Caption=预览代码(&V)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=365
Top=59
Width=104
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandInsert
Index=-1
Caption=插入到编辑器(&I)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=365
Top=90
Width=104
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandEsc
Index=-1
Caption=取 消(&C)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=365
Top=121
Width=104
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame1
Index=-1
Caption=消息框按钮
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,15
Enabled=True
Visible=True
Font=微软雅黑,9
Left=10
Top=190
Width=178
Height=245
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=OptionButton
Index=0
Style=0 - 标准
Caption=确定
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=21
Top=217
Width=105
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionButton
Index=1
Style=0 - 标准
Caption=确定 取消
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=21
Top=241
Width=105
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionButton
Index=2
Style=0 - 标准
Caption=是 否 取消
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=21
Top=265
Width=105
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionButton
Index=3
Style=0 - 标准
Caption=是 否
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=21
Top=289
Width=105
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionButton
Index=4
Style=0 - 标准
Caption=重试 取消
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=21
Top=313
Width=105
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionButton
Index=5
Style=0 - 标准
Caption=中止 重试 忽略
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=21
Top=337
Width=105
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionButton
Index=6
Style=0 - 标准
Caption=取消 重试 继续
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=21
Top=362
Width=105
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=CheckHelp
Index=-1
Style=0 - 标准
Caption=增加帮助按钮
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=22
Top=406
Width=115
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=将帮助按钮添加到消息框中。
ToolTipBalloon=False
AcceptFiles=False

[Frame]
Name=Frame2
Index=-1
Caption=消息框图标
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,15
Enabled=True
Visible=True
Font=微软雅黑,9
Left=195
Top=190
Width=155
Height=245
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=OptionIcon
Index=0
Style=0 - 标准
Caption=没有图标
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=208
Top=217
Width=86
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionIcon
Index=1
Style=0 - 标准
Caption=严重/错误
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=208
Top=260
Width=86
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionIcon
Index=2
Style=0 - 标准
Caption=问题
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=208
Top=305
Width=86
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionIcon
Index=3
Style=0 - 标准
Caption=警告
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=208
Top=350
Width=86
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=OptionIcon
Index=4
Style=0 - 标准
Caption=通知
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=2
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=208
Top=395
Width=86
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=CheckCase
Index=-1
Style=0 - 标准
Caption=生成选择案例
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
Left=370
Top=154
Width=96
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check3
Index=-1
Style=0 - 标准
Caption=设为前台窗口
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=370
Top=175
Width=98
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check4
Index=-1
Style=0 - 标准
Caption=设为顶层窗口
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=370
Top=196
Width=95
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check2
Index=-1
Style=0 - 标准
Caption=文本右对齐
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=370
Top=217
Width=95
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Frame]
Name=Frame4
Index=-1
Caption=默认按钮
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9
Left=362
Top=239
Width=105
Height=106
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option2
Index=0
Style=0 - 标准
Caption=第1个
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=4
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=374
Top=262
Width=77
Height=13
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option2
Index=1
Style=0 - 标准
Caption=第2个
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=4
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=374
Top=283
Width=77
Height=13
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option2
Index=2
Style=0 - 标准
Caption=第3个
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=4
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=374
Top=304
Width=77
Height=13
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option2
Index=3
Style=0 - 标准
Caption=第4个
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=4
Enabled=False
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=374
Top=325
Width=77
Height=13
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Frame]
Name=Frame3
Index=-1
Caption=模态对话框
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9
Left=362
Top=352
Width=104
Height=82
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Option]
Name=Option1
Index=0
Style=0 - 标准
Caption=应用模式
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=1 - 选择
Multiline=True
GroupName=3
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=371
Top=373
Width=91
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=应用模式用户必须先对消息框作出响应，然后才能在hWnd参数标识的窗口中继续工作。但是，用户可以移至其他线程的窗口并在这些窗口中工作。
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=1
Style=0 - 标准
Caption=系统模式
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=3
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=371
Top=393
Width=88
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=系统模式与应用模式相同，除了消息框具有顶层样式。使用系统模式消息框通知用户需要立即引起注意的严重，潜在破坏性错误（例如，内存不足）。除了与hWnd关联的窗口外，此标志对用户与窗口交互的能力没有影响。
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=2
Style=0 - 标准
Caption=任务模式
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=3
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9
Left=371
Top=413
Width=94
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=任务模式与应用模式相同，不同之处在于，如果hWnd参数为NULL，则禁用属于当前线程的所有顶级窗口。当调用的应用程序或库没有可用的窗口句柄，但仍需要在不暂停其他线程的情况下阻止输入到调用线程中的其他窗口时，请使用此标志。
ToolTipBalloon=False
AcceptFiles=False


[AllCode]


'创建 [事件]   hWndForm=窗体句柄  UserData=可选的用户定义的值
Sub FormMessage_WM_Create(hWndForm As hWnd, UserData As Integer)  '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   Dim r As ZString * 100
   GetProRunFileEx(0,5,@r)
   TextLp.Text = r
   TextlpCaption.SetFocus 
   'Dim aa As Integer = SendMessage(GetMainWinHandle(), WM_GETICON, ICON_BIG, 0)
   'SendMessage(hWndForm ,WM_SETICON ,ICON_BIG ,aa)
   'aa=SendMessage(GetMainWinHandle(), WM_GETICON, ICON_SMALL, 0)
   'SendMessage(hWndForm, WM_SETICON, ICON_SMALL, aa) 
End Sub

'单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub FormMessage_CommandCopy_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   FormCopy.Show(hWndForm, True)
End Sub

Function CopyMessage() As String
   Dim tca As String = YF_Replace(CWSTRtoString(Textlp.Text), """", """""")   '标题
   Dim tcap As String = YF_Replace(CWSTRtoString(TextlpCaption.Text), """", """""") '内容
   Dim tt As String = YF_Replace(tcap, vbCrLf, """ & vbCrLf & _" & vbCrLf & Space(IIf(CheckCase.Value, 33, 31)) & """")
   Dim mm As String = IIf(CheckCase.Value, "Select Case", "Dim nResult As Long" & vbCrLf & "nResult =") & " MsgBox( hWndForm,""" & tt
   mm &= """, _" & vbCrLf & Space(IIf(CheckCase.Value, 12, 10))  
   if Check1.Value=False Then mm &= """" & tca & ""","
   Dim As Long  i,aa
   Dim xian(6) As String = {"MB_OK", "MB_OKCANCEL", "MB_YESNOCANCEL", "MB_YESNO", "MB_RETRYCANCEL", "MB_ABORTRETRYIGNORE", "MB_CANCELTRYCONTINUE"}
   Dim xianok(6) As String = {"IDOK", "IDOK,IDCANCEL", "IDYES,IDNO,IDCANCEL", "IDYES,IDNO", "IDRETRY,IDCANCEL", "IDABORT,IDRETRY,IDIGNORE", "IDCANCEL,IDTRYAGAIN,IDCONTINUE"}
   Dim xianoks As String
   For i = 0 To 6
      If OptionButton(i).Value Then
         mm &= xian(i)
         xianoks = xianok(i)
         Exit For
      End If
   Next
   Dim tubiao(1 To 4) As String = {"MB_ICONERROR", "MB_ICONQUESTION", "MB_ICONWARNING", "MB_ICONINFORMATION"}
   For i = 1 To 4
      If OptionIcon(i).Value Then
         mm &= " Or " & tubiao(i)
         Exit For
      End If
   Next
   For i = 0 To 3
      if Option2(i).Value Then
         aa = i
         Exit for
      End if
   Next
   mm &= " Or MB_DEFBUTTON" & aa+ 1
   Dim mosi(2) As String = {"MB_APPLMODAL", "MB_SYSTEMMODAL", "MB_TASKMODAL"}
   aa=0
   for i = 0 To 2
      if Option1(i).Value Then
         aa = i
         Exit for
      End if
   Next   
   mm &= " Or " & mosi(aa)
   If CheckHelp.Value Then mm &= " Or MB_HELP"
   If Check2.Value Then mm &= " Or MB_RIGHT"  
   If Check3.Value Then mm &= " Or MB_SETFOREGROUND"  
   If Check4.Value Then mm &= " Or MB_TOPMOST"  
   mm &=")"
   If CheckCase.Value Then
      Dim ee() As String
      vbSplit xianoks, ",", ee()
      For i = 0 To UBound(ee)
         mm &= vbCrLf & "    Case " & ee(i)
      Next
      If CheckHelp.Value Then mm &= vbCrLf & "    Case IDHELP"
      mm &= vbCrLf & "End Select"
   End If
   Function = mm
End Function

'单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub FormMessage_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim As Long  i,aa
   Dim uType As UINT
   Dim xian(6) As UINT = {MB_OK, MB_OKCANCEL, MB_YESNOCANCEL, MB_YESNO, MB_RETRYCANCEL, MB_ABORTRETRYIGNORE, MB_CANCELTRYCONTINUE}
   For i = 0 To 6
      If OptionButton(i).Value Then
         uType = xian(i)
         Exit For
      End If
   Next
   Dim tubiao(1 To 4) As UINT = {MB_ICONERROR, MB_ICONQUESTION, MB_ICONWARNING, MB_ICONINFORMATION}
   For i = 1 To 4
      If OptionIcon(i).Value Then
         uType Or= tubiao(i)
         Exit For
      End If
   Next
   Dim an(3) As UINT = {MB_DEFBUTTON1, MB_DEFBUTTON2, MB_DEFBUTTON3, MB_DEFBUTTON4}
   for i = 0 To 3
      if Option2(i).Value Then
         aa = i
         Exit for
      End if
   Next   
   uType Or= an(aa)
   Dim mosi(2) As UINT = {MB_APPLMODAL, MB_SYSTEMMODAL, MB_TASKMODAL}
   Dim As CWSTR cc=TextlpCaption.Text,tt =TextLp.Text
   for i = 0 To 1
      if Option1(i).Value Then
         aa = i
         Exit for
      End if
   Next     
   uType Or= mosi(aa)
   
   If CheckHelp.Value Then uType Or= MB_HELP
   If Check2.Value Then uType Or= MB_RIGHT
   If Check3.Value Then uType Or= MB_SETFOREGROUND
   If Check4.Value Then uType Or= MB_TOPMOST
   
   MsgBox hWndForm,cc,tt, uType
   
End Sub

'单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub FormMessage_CommandEsc_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Form主.Close
   
End Sub

'重绘 [事件]   ControlIndex=控件数组索引  hWndForm=窗体句柄  hWndControl=控件句柄
Function FormMessage_Picture1_WM_Paint(hWndForm As hWnd, hWndControl As hWnd) As LResult  '重绘，系统通知控件需要重新绘画。
   Dim gg As yGDI = yGDI(hWndControl, GetSysColor(COLOR_BTNFACE), True)
   Dim aa As HICON, y As Long, idi(3) As Any Ptr = {IDI_ERROR, IDI_QUESTION, IDI_WARNING, IDI_INFORMATION}
   For i As Long = 0 To 3
      aa = LoadIcon(Null, idi(i))
      DrawIcon gg.m_Dc, 0, y, aa
      DeleteObject aa ' 用完要销毁
      y += AfxScaleY(45)
   Next
   Function = True  '最后一行，必须的
End Function
   

'单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub FormMessage_CommandInsert_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
    InsertText( vbCrLf & wStrToUtf8(CopyMessage))
   Form主.Close
  
   
End Sub

'关闭 [事件]   hWndForm=窗体句柄
Function FormMessage_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭
   
'   PostMessage(Form1.hWnd, &H501, &H501, &H502)  '让VFB前台
'   PostMessage(FORM1.HWND, &H501,&H501, &H505)   '使代码编辑器获取焦点
   Function = 0
End Function

Sub FormMessage_OptionButton_BN_Clicked(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd)  '单击
   If OptionButton(ControlIndex).Value = True Then
      Dim aa(6) As Long = {1, 2, 3, 2, 2, 3, 3}
      Dim bb As Long, i As Long
      for i = 0 To 3
         if Option2(i).Value Then
            bb = i
            Exit for
         End if
      Next
      if CheckHelp.Value Then aa(ControlIndex) += 1
      
      if bb + 1 > aa(ControlIndex) Then
         Option2(bb).Value = False
         bb = aa(ControlIndex) -1
         Option2(bb).Value = True
      end if
      bb = aa(ControlIndex)
      for i = 0 To 3
         Option2(i).Enabled = i < bb
      Next
   End if
End Sub

Sub FormMessage_CheckHelp_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Dim aa(6) As Long = {1, 2, 3, 2, 2, 3, 3}
   Dim bb As Long, i As Long
   for i = 0 To 6
      if OptionButton(i).Value Then
         bb = i
         Exit for
      End if
   Next
   if CheckHelp.Value Then aa(bb) += 1 
   bb = aa(bb) 
   for i = 0 To 3
      Option2(i).Enabled = i < bb 
   Next     
End Sub

Sub FormMessage_Check1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   TextLp.Enabled = Check1.Value =False 
End Sub














