﻿#VisualFreeBasic_Form#  Version=5.8.2
Locked=0

[Form]
Name=Form打开
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CHILD
Style=0 - 无边框
Icon=
Caption=
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=606
Height=469
TopMost=False
Child=True
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text标题
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=13
Top=27
Width=343
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=打开对话框的标题文字：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=13
Top=8
Width=341
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=打开的文件名:（初始文件名）
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=13
Top=57
Width=418
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text文件名
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=13
Top=75
Width=228
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=初始目录：(若没指定，将使用当前目录)
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=13
Top=164
Width=341
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text扩展名
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=13
Top=126
Width=105
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text目录
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=13
Top=183
Width=585
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=默认扩展名：（若用户没键入扩展名，则使用它)
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=13
Top=106
Width=333
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=过滤器：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=13
Top=221
Width=341
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text过滤器
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=全部(*.ffp;*.bas)|*.ffp;*.bas|工程(*.ffp)|*.ffp|文件(*.bas)|*.bas|所有文件(*.*)|*.*
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=13
Top=240
Width=585
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label6
Index=-1
Style=0 - 无边框
Caption=选项：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=13
Top=274
Width=341
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[YFList]
Name=YFList1
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
ForeColor=&H000000
BackColor=&HFFFFFF
MoveColor=&HFFE5CC
SelFore=&HFFFFFF
SelBack=&HE57300
ScrollFore=&H878787
ScrollBack=&HE1E1E1
ScrollMove=&H4B4B4B
LinesColor=&HA5A5A5
ItemHeight=25
Check=True
GridLines=True
FocusLine=True
Left=13
Top=294
Width=579
Height=165
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=测试(&T)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=443
Top=11
Width=155
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandCopy
Index=-1
Caption=预览代码(&V)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=443
Top=42
Width=155
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandInsert
Index=-1
Caption=插入到编辑器(&I)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=443
Top=73
Width=155
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandEsc
Index=-1
Caption=取 消(&C)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=443
Top=104
Width=155
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=单行函数调用
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=445
Top=137
Width=154
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

'全部(*.ffp;*.bas)|*.ffp;*.bas|工程(*.ffp)|*.ffp|文件(*.bas)|*.bas|所有文件(*.*)|*.*
'nFilename = FF_OpenFileDialog(hWndForm, 标题, 默认文件名,初始目录,筛选器, 默认扩展名,FLAGS,中心)
'hWnd:       （如果有） 的父窗口的句柄 
'Caption:      标题将显示为对话框的标题。
'FileSpec:     要显示的默认文件名。默认文件夹最后不能带 \ 字符
'InitialDir:  对话框中将显示初始目录。如果此值为 null，则使用当前目录。
'Filter:      要在对话框中显示的文件的筛选器： 说明。这是一个分隔的字符串，将在"文件类型"组合框中产生可用的选项。
'例： "图像文件(*.bmp;*.jpg;*.jpeg;*.gif;*.png)|*.bmp;*.jpg;*.jpeg;*.gif;*.png"
'DefExtension: 默认文件扩展名适用于文件名，如果用户不提供一个。
'Flags:          Win32 帮助下"OpenFileName"文件中所述的一个或多个值的组合。
'E.g.: OFN_ALLOWMULTISELECT Or OFN_FILEMUSTEXIST Or OFN_NOCHANGEDIR 
'CenterFlag:   TRUE  对中心对话框上的桌面屏幕。

'OFN_ENABLEHOOK         激活在lpfnHook成员中指定的钩子函数。
'OFN_ENABLETEMPLATE 当用户打开一个文件夹时，引起对话框发送CDN_INCLUDEITEM通知消息到你的OFNHookProc程序。
'OFN_ENABLETEMPLATEHANDLE  指出hInstance成员能识别的包含预载对话框模板的数据块。如果这个标记被指定的，系统忽略lpTemplateName。 

Sub Form打开_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   Dim xc(...) As Wstring *128 ={ _
    vfb_LangString("对话框在桌面屏幕的中心"), _
    vfb_LangString("新的Explorer风格的用户化模块"), _  '  OFN_EXPLORER
    vfb_LangString("如果当搜索时用户改变了目录，则恢复当前目录到初始值"), _  ' OFN_NOCHANGEDIR
    vfb_LangString("如果用户指定了一个不存在的文件，提示用户是否新建这个文件"), _  ' OFN_CREATEPROMPT
    vfb_LangString("允许多选，选择多个文件"), _  '  OFN_ALLOWMULTISELECT
    vfb_LangString("用户仅可以输入已存在的文件的名字"), _  '  OFN_FILEMUSTEXIST
    vfb_LangString("用户只能选择有效的路径和文件名"), _    ' OFN_PATHMUSTEXIST
    vfb_LangString("用户输入的扩展名可以和默认扩展名不同"), _  ' OFN_EXTENSIONDIFFERENT
    vfb_LangString("选择的是快捷方式（.LNK）返回路径和文件名"), _   ' OFN_NODEREFERENCELINKS
    vfb_LangString("返回的文件不能有只读，也不是在写保护的目录中"), _   ' OFN_NOREADONLYRETURN
    vfb_LangString("指定文件不是在对话框关闭前建立的"), _    ' OFN_NOTESTFILECREATE
    vfb_LangString("允许返回的文件名中的无效字符"), _    ' OFN_NOVALIDATE
    vfb_LangString("隐藏和禁用 “网络 ”按钮。"), _    ' OFN_NONETWORKBUTTON
    vfb_LangString("隐藏只读复选框"), _     ' OFN_HIDEREADONLY
    vfb_LangString("显示帮助按钮"), _     '  OFN_SHOWHELP
    vfb_LangString("显示只读文件"), _     ' OFN_READONLY
    vfb_LangString("对于旧风格对话框，使用长文件名"), _  '  OFN_LONGNAMES
    vfb_LangString("对于旧风格对话框，使用短文件名（8.3格式）"), _   ' OFN_NOLONGNAMES
    vfb_LangString("如果与网络共享冲突而，也要返回选择的文件名")  }   ' OFN_SHAREAWARE
    Dim i As Long 
    For i=0 To UBound(xc)
       YFList1.AddItem(xc(i))
    Next  
    For i=1 To 3
       YFList1.ItemSel(i) = True 
    Next 
    YFList1.ListIndex =-1
End Sub
Function Copy打开() As String
   Dim 标题      As String = YF_Replace(CWSTRtoString(Text标题.Text) ,"""" ,"""""")   '标题
   Dim 文件名    As String = YF_Replace(CWSTRtoString(Text文件名.Text) ,"""" ,"""""") '
   Dim 扩展名    As String = YF_Replace(CWSTRtoString(Text扩展名.Text) ,"""" ,"""""") '
   Dim 目录      As String = YF_Replace(CWSTRtoString(Text目录.Text) ,"""" ,"""""")   '
   Dim 过滤器    As String = YF_Replace(CWSTRtoString(Text过滤器.Text) ,"""" ,"""""") '
   Dim 中心      As Long   = YFList1.ItemSel(0)
   Dim i         As Long
   Dim 选项(...) As ZString * 30 = {"OFN_EXPLORER" ,"OFN_NOCHANGEDIR" ,"OFN_CREATEPROMPT" ,"OFN_ALLOWMULTISELECT" ,"OFN_FILEMUSTEXIST" ,"OFN_PATHMUSTEXIST" ,"OFN_EXTENSIONDIFFERENT" _
      ,"OFN_NODEREFERENCELINKS" ,"OFN_NOREADONLYRETURN" ,"OFN_NOTESTFILECREATE" ,"OFN_NOVALIDATE" ,"OFN_NONETWORKBUTTON" ,"OFN_HIDEREADONLY" ,"OFN_SHOWHELP" _
      ,"OFN_READONLY"           ,"OFN_LONGNAMES" ,"OFN_NOLONGNAMES"             ,"OFN_SHAREAWARE"}
   Dim g选项 As String
   For i = 0 To UBound(选项)
      If YFList1.ItemSel(i + 1) Then
         If Len(g选项) Then g选项 &= " Or " & 选项(i) Else g选项 = 选项(i)
      End If
   Next
   Dim m As String = "   Dim 文件 As CWSTR = FF_OpenFileDialog(hWndForm,《行符号》""" & 标题 & """,《行符号》""" & 文件名 & """,《行符号》""" & _
      目录 & """,《行符号》""" & 过滤器 & """,《行符号》""" & 扩展名 & """,《行符号》" & g选项 & ",《行符号》" & IIf(中心 ,"True" ,"False") & ")"
   If Check1.Value then
      m = YF_Replace(m ,"《行符号》" ,"")
   Else
      m = YF_Replace(m ,"《行符号》" ,"_" & vbCrLf & "      ")
   End If
   If InStr(m ,"OFN_ALLOWMULTISELECT") Then
      m &= vbCrLf & "   Dim i As Long ,文件列表() As CWSTR " & vbCrLf & "   If vbSplitW(文件 ,""|"" ,文件列表()) Then " & vbCrLf & "      Print ""文件路径 : "" & 文件列表(0)" & vbCrLf & _
         "      For i = 1 To UBound(文件列表)" & vbCrLf & "         Print 文件列表(0) & ""\"" & 文件列表(i)" & vbCrLf & "      Next " & vbCrLf & "   End If"
   Else
      m &= vbCrLf & "   If Len(文件) Then " & vbCrLf & "      Print 文件" & vbCrLf & "   End If "
   End If
   Function = m
End Function

Sub Form打开_CommandCopy_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   FormCopy.Show(hWndForm ,True)
End Sub

Sub Form打开_Command1_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   Dim 标题      As String = CWSTRtoString(Text标题.Text)    '标题
   Dim 文件名    As String = CWSTRtoString(Text文件名.Text)  '
   Dim 扩展名    As String = CWSTRtoString(Text扩展名.Text)  '
   Dim 目录      As String = CWSTRtoString(Text目录.Text)    '
   Dim 过滤器    As String = CWSTRtoString(Text过滤器.Text)  '
   Dim 中心      As Long   = YFList1.ItemSel(0)
   Dim i         As Long
   Dim 选项(...) As Long = {OFN_EXPLORER ,OFN_NOCHANGEDIR ,OFN_CREATEPROMPT ,OFN_ALLOWMULTISELECT ,OFN_FILEMUSTEXIST ,OFN_PATHMUSTEXIST ,OFN_EXTENSIONDIFFERENT _
   ,OFN_NODEREFERENCELINKS ,OFN_NOREADONLYRETURN ,OFN_NOTESTFILECREATE ,OFN_NOVALIDATE ,OFN_NONETWORKBUTTON ,OFN_HIDEREADONLY ,OFN_SHOWHELP ,OFN_READONLY ,OFN_LONGNAMES ,OFN_NOLONGNAMES ,OFN_SHAREAWARE}
   Dim g选项 As Long 
   For i = 0 To UBound(选项)
      If YFList1.ItemSel(i + 1) Then
         g选项 Or= 选项(i) 
      End If
   Next
   Dim 文件 As CWSTR = FF_OpenFileDialog(hWndForm ,标题 ,文件名 ,目录 ,过滤器 ,扩展名 ,g选项 ,IIf(中心 ,True ,False))
   MsgBox hWndForm,文件 ,vfb_LangString("返回信息") 
   If (g选项 And OFN_ALLOWMULTISELECT)<>0 Then
   Else
   End If
End Sub

Sub Form打开_CommandInsert_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   InsertText(vbCrLf & wStrToUtf8(Copy打开))
   Form主.Close
End Sub

Sub Form打开_CommandEsc_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   Form主.Close
End Sub




