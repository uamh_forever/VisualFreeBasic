﻿#VisualFreeBasic_Form#  Version=5.8.2
Locked=0

[Form]
Name=Form输入
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CHILD
Style=0 - 无边框
Icon=
Caption=Form1
StartPosition=0 - 手动
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=477
Height=308
TopMost=False
Child=True
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text标题
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=14
Top=29
Width=269
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text提示
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=14
Top=77
Width=450
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text文本
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=14
Top=128
Width=448
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text长度
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=260
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=14
Top=185
Width=68
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text位置
Index=0
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=0
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=37
Top=239
Width=51
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text位置
Index=1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=0
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=127
Top=238
Width=51
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=将所有字符显示为星号(*)
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=17
Top=278
Width=414
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=测试
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=304
Top=6
Width=57
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandEsc
Index=-1
Caption=取 消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=305
Top=40
Width=57
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandCopy
Index=-1
Caption=预览代码
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=367
Top=6
Width=95
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandInsert
Index=-1
Caption=插入到编辑器
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=367
Top=40
Width=94
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label6
Index=0
Style=0 - 无边框
Caption=x=
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=16
Top=244
Width=21
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label4
Index=-1
Style=0 - 无边框
Caption=要编辑的文本：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=14
Top=108
Width=333
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=要编辑的字符串的最大长度。默认长度为260个字符。最大长度为2048个字符。
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=14
Top=166
Width=512
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=对话框中显示的提示:
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=14
Top=59
Width=418
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=对话框的标题文字：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=14
Top=8
Width=341
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label5
Index=-1
Style=0 - 无边框
Caption=对话框的XY位置。如果两者均为0,则对话框居中。
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=16
Top=220
Width=436
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label6
Index=1
Style=0 - 无边框
Caption=y=
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=106
Top=243
Width=21
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

Sub Form输入_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)

End Sub
Function Copy输入() As String
   Dim 标题      As String = YF_Replace(CWSTRtoString(Text标题.Text) ,"""" ,"""""") '标题
   Dim 提示      As String = YF_Replace(CWSTRtoString(Text提示.Text) ,"""" ,"""""") '
   Dim 文本 As String = YF_Replace(CWSTRtoString(Text文本.Text) ,"""" ,"""""") '
   Dim 长度 As String  = Text长度.Text
   Dim x As String = Text位置(0).text 
   Dim y As String = Text位置(1).text 
   If x = "0" Then x = ""
   If y="0" Then y=""
   Dim m As String = "   Dim 文本 As CWSTR = AfxInputBox(hWndForm," & x & "," & y & ",""" & 标题 & """,""" & 提示 & """,""" & 文本 & """," & 长度 & IIf(Check1.Value ,",True" ,"") & ")"
   Function = m
End Function

Sub Form输入_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   Dim 标题      As String = CWSTRtoString(Text标题.Text) '标题
   Dim 提示      As String = CWSTRtoString(Text提示.Text)  '
   Dim 文本 As String = CWSTRtoString(Text文本.Text)  '
   Dim 长度 As Long   = Text长度.Text.ValLong 
   Dim x As Long  = Text位置(0).Text.ValLong
   Dim y As Long = Text位置(1).text.ValLong 
   Dim 返回 As CWSTR = AfxInputBox(hWndForm, x , y , 标题 , 提示 , 文本 , 长度, Check1.Value ) 

End Sub

Sub Form输入_CommandCopy_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
       FormCopy.Show(hWndForm ,True)
End Sub

Sub Form输入_CommandEsc_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   Form主.Close
End Sub

Sub Form输入_CommandInsert_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   InsertText(vbCrLf & wStrToUtf8(Copy输入))
   Form主.Close
End Sub




