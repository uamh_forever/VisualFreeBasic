﻿#VisualFreeBasic_Form#  Version=5.8.2
Locked=0

[Form]
Name=FormCopy
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_BORDER,WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=Default.ico
Caption=对话框代码预览
StartPosition=2 - 父窗口中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=500
Height=319
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=300
MinHeight=200
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=TextCopy
Index=-1
Style=3 - 凹边框
TextScrollBars=3 - 垂直和水平
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=False
Multiline=True
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=6
Top=6
Width=472
Height=236
Layout=5 - 宽度和高度
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=CommandCopy
Index=-1
Caption=复制到剪贴板(&A)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=259
Top=248
Width=115
Height=28
Layout=9 - 右边和底部
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandEsc
Index=-1
Caption=取 消(&C)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=380
Top=248
Width=97
Height=28
Layout=9 - 右边和底部
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]

'  创建 [事件]   hWndForm=窗体句柄  UserData=可选的用户定义的值
Sub FormCopy_WM_Create(hWndForm As hWnd ,UserData As Integer) '完成创建窗口及所有的控件后，此时窗口还未显示。注：自定义消息里 WM_Create 此时还未创建控件和初始赋值。
   select Case Form主.YFproTab1.Selected
      Case 0
         TextCopy.Text = CopyMessage
      Case 1
         TextCopy.Text = Copy打开
      Case 2
         TextCopy.Text = Copy保存
      Case 3
         TextCopy.Text = Copy目录
      Case 4
      Case 5
         TextCopy.Text = Copy输入
      Case 6
         TextCopy.Text =Copy打印
   End Select
   'Dim aa As Integer = SendMessage(GetMainWinHandle(), WM_GETICON, ICON_BIG, 0)
   'SendMessage(hWndForm ,WM_SETICON ,ICON_BIG ,aa)
   'aa=SendMessage(GetMainWinHandle(), WM_GETICON, ICON_SMALL, 0)
   'SendMessage(hWndForm, WM_SETICON, ICON_SMALL, aa)
   
End Sub


'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub FormCopy_CommandEsc_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  
  Me.Close

End Sub


'  单击 [事件]   ControlIndex=控件数组的索引  hWndForm=窗体句柄  hWndControl=控件句柄  idButtonControl=按钮的标识符
Sub FormCopy_CommandCopy_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
  
  FF_ClipboardSetText CWSTRtoString(TextCopy.Text)
  Me.Close
  

End Sub




