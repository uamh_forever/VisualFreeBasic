﻿#VisualFreeBasic_Form#  Version=5.8.2
Locked=0

[Form]
Name=Form目录
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CHILD
Style=0 - 无边框
Icon=
Caption=Form1
StartPosition=0 - 手动
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=604
Height=511
TopMost=False
Child=True
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text标题
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=9
Top=27
Width=343
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=保存对话框的标题文字：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=9
Top=8
Width=341
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label3
Index=-1
Style=0 - 无边框
Caption=初始目录：(若没指定，将使用当前目录)
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=10
Top=58
Width=341
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text目录
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=宋体,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=10
Top=77
Width=585
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=测试(&T)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=360
Top=9
Width=113
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandCopy
Index=-1
Caption=预览代码(&V)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=360
Top=40
Width=113
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandInsert
Index=-1
Caption=插入到编辑器(&I)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=480
Top=9
Width=113
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandEsc
Index=-1
Caption=取 消(&C)
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=480
Top=40
Width=113
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label6
Index=-1
Style=0 - 无边框
Caption=选项：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=10
Top=107
Width=341
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[YFList]
Name=YFList1
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
ForeColor=&H000000
BackColor=&HFFFFFF
MoveColor=&HFFE5CC
SelFore=&HFFFFFF
SelBack=&HE57300
ScrollFore=&H878787
ScrollBack=&HE1E1E1
ScrollMove=&H4B4B4B
LinesColor=&HA5A5A5
ItemHeight=25
Check=True
GridLines=True
FocusLine=True
Left=9
Top=128
Width=584
Height=374
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=单行函数调用
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=365
Top=102
Width=154
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

'hwnd   [in]对话框父窗口的句柄。该值可以为零。
'pwszTitle   [in]可选。表示“浏览”对话框中显示的标题的字符串值。
'pwszStartFolder   [in]可选。 对话框将显示的初始文件夹。
'nFlags   [in]可选。包含方法选项的LONG值。这可以是零或是BROWSEINFO结构中的ulFlags 成员下列出的值的组合。

Sub Form目录_Shown(hWndForm As hWnd ,UserData As Integer) '窗口完全显示后。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   Dim xc(...) As Wstring * 128 = {_
      vfb_LangString("只返回文件系统目录。如果用户选择不属于文件系统的文件夹，则OK按钮呈灰色。") , _ 'BIF_RETURNONLYFSDIRS
      vfb_LangString("请勿在对话框的树视图控件中包含域级别下的网络文件夹")                       , _ '  BIF_DONTGOBELOWDOMAIN
      vfb_LangString("只返回电脑。如果用户选择了除计算机以外的任何内容，则OK按钮会变灰。")       , _ ' BIF_BROWSEFORCOMPUTER
      vfb_LangString("请勿在浏览对话框中包含“新建文件夹”按钮。")                               , _ ' BIF_NONEWFOLDERBUTTON
      vfb_LangString("当所选项目是快捷方式时，返回快捷方式本身而不是其目标。")                   , _ '  BIF_NOTRANSLATETARGETS
      vfb_LangString("在对话框中包含状态区域")                                                   , _ ' BIF_STATUSTEXT
      vfb_LangString("只返回文件系统的根文件夹下的子文件夹")                                     , _ ' BIF_RETURNFSANCESTORS
      vfb_LangString("在浏览对话框中包含一个编辑控件，允许用户输入项目的名称。")                 , _ '  BIF_EDITBOX
      vfb_LangString("使用新的用户界面。提供了可以调整大小的较大对话框")                         , _ ' BIF_NEWDIALOGSTYLE
      vfb_LangString("使用新的用户界面，包括一个编辑框。")                                       , _ 'BIF_USENEWUI
      vfb_LangString("会在对话框中添加使用提示，以代替编辑框")                                   , _ '  BIF_UAHINT
      vfb_LangString("只允许选择打印机。如果用户选择打印机以外的任何其他设备，则OK按钮会变灰。") , _ ' BIF_BROWSEFORPRINTER
      vfb_LangString("浏览对话框显示文件以及文件夹。")             , _ ' BIF_BROWSEINCLUDEFILES
      vfb_LangString("浏览对话框可以显示远程系统上的可共享资源。") , _ '  BIF_SHAREABLE
      vfb_LangString("允许浏览文件夹连接，例如库或带.zip文件扩展名的压缩文件。") _ ' BIF_BROWSEFILEJUNCTIONS
      } '
   Dim i As Long
   For i = 0 To UBound(xc)
      YFList1.AddItem(xc(i))
   Next
   For i = 0 To 3
      YFList1.ItemSel(i) = True
   Next
   YFList1.ListIndex = -1
End Sub
Function Copy目录() As String
   Dim 标题      As String = YF_Replace(CWSTRtoString(Text标题.Text) ,"""" ,"""""") '标题
   Dim 目录      As String = YF_Replace(CWSTRtoString(Text目录.Text) ,"""" ,"""""") '
   Dim i         As Long
   Dim 选项(...) As ZString * 30 = {"BIF_RETURNONLYFSDIRS" ,"BIF_DONTGOBELOWDOMAIN" ,"BIF_BROWSEFORCOMPUTER" ,"BIF_NONEWFOLDERBUTTON" ,"BIF_NOTRANSLATETARGETS" , _
      "BIF_STATUSTEXT"         ,"BIF_RETURNFSANCESTORS" ,"BIF_EDITBOX" ,"BIF_NEWDIALOGSTYLE" ,"BIF_USENEWUI" ,"BIF_UAHINT" ,"BIF_BROWSEFORPRINTER" , _
      "BIF_BROWSEINCLUDEFILES" ,"BIF_SHAREABLE" ,"BIF_BROWSEFILEJUNCTIONS"}
   Dim g选项 As String
   For i = 0 To UBound(选项)
      If YFList1.ItemSel(i ) Then
         If Len(g选项) Then g选项 &= " Or " & 选项(i) Else g选项 = 选项(i)
      End If
   Next
   Dim m As String = "   Dim 文件夹 As CWSTR = AfxBrowseForFolder(hWndForm,《行符号》""" & 标题 & """,《行符号》""" & 目录 & """,《行符号》" & g选项 & ")"
   If Check1.Value Then
      m = YF_Replace(m ,"《行符号》" ,"")
   Else
      m = YF_Replace(m ,"《行符号》" ,"_" & vbCrLf & "      ")
   End If
   m &= vbCrLf & "   If Len(文件夹) Then " & vbCrLf & "      Print 文件夹" & vbCrLf & "   End If "
   Function = m
End Function

Sub Form目录_CommandCopy_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   FormCopy.Show(hWndForm ,True)
End Sub

Sub Form目录_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   Dim 标题      As String = CWSTRtoString(Text标题.Text)  '标题
   Dim 目录      As String = CWSTRtoString(Text目录.Text)  '
   Dim i         As Long
   Dim 选项(...) As ZString * 30 = {"BIF_RETURNONLYFSDIRS" ,"BIF_DONTGOBELOWDOMAIN" ,"BIF_BROWSEFORCOMPUTER" ,"BIF_NONEWFOLDERBUTTON" ,"BIF_NOTRANSLATETARGETS" , _
      "BIF_STATUSTEXT"         ,"BIF_RETURNFSANCESTORS" ,"BIF_EDITBOX" ,"BIF_NEWDIALOGSTYLE" ,"BIF_USENEWUI" ,"BIF_UAHINT" ,"BIF_BROWSEFORPRINTER" , _
      "BIF_BROWSEINCLUDEFILES" ,"BIF_SHAREABLE" ,"BIF_BROWSEFILEJUNCTIONS"}
   Dim g选项 As String
   For i = 0 To UBound(选项)
      If YFList1.ItemSel(i ) Then
         If Len(g选项) Then g选项 &= " Or " & 选项(i) Else g选项 = 选项(i)
      End If
   Next
   Dim m As String = "   Dim 文件夹 As CWSTR = AfxBrowseForFolder(hWndForm,《行符号》""" & 标题 & """,《行符号》""" & 目录 & """,《行符号》" & g选项 & ")"
   If Check1.Value Then
      m = YF_Replace(m ,"《行符号》" ,"")
   Else
      m = YF_Replace(m ,"《行符号》" ,"_" & vbCrLf & "      ")
   End If
   m &= vbCrLf & "   If Len(文件夹) Then " & vbCrLf & "      Print 文件夹" & vbCrLf & "   End If "
End Sub

Sub Form目录_CommandInsert_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   InsertText(vbCrLf & wStrToUtf8(Copy目录))
   Form主.Close
End Sub

Sub Form目录_CommandEsc_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   Form主.Close
End Sub




