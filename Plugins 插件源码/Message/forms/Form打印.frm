﻿#VisualFreeBasic_Form#  Version=5.8.2
Locked=0

[Form]
Name=Form打印
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CHILD
Style=0 - 无边框
Icon=
Caption=Form2
StartPosition=0 - 手动
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=478
Height=354
TopMost=False
Child=True
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=1
Enabled=True
Visible=True
MaxLength=3
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=12
Top=26
Width=51
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[YFList]
Name=YFList1
Index=-1
Style=0 - 无边框
Enabled=True
Visible=True
ForeColor=&H000000
BackColor=&HFFFFFF
MoveColor=&HFFE5CC
SelFore=&HFFFFFF
SelBack=&HE57300
ScrollFore=&H878787
ScrollBack=&HE1E1E1
ScrollMove=&H4B4B4B
LinesColor=&HA5A5A5
ItemHeight=20
Check=True
GridLines=True
FocusLine=True
Left=9
Top=164
Width=459
Height=181
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=打印份数：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=12
Top=7
Width=212
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=0
Style=0 - 无边框
Caption=初始起始页面:
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=12
Top=58
Width=152
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandInsert
Index=-1
Caption=插入到编辑器
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=376
Top=89
Width=94
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandCopy
Index=-1
Caption=预览代码
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=376
Top=49
Width=94
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=CommandEsc
Index=-1
Caption=取 消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=376
Top=128
Width=94
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=测试
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9
Left=376
Top=9
Width=94
Height=26
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[UpDown]
Name=UpDown1
Index=-1
Style=1 - 在伙伴右边
Buddy=Text1
Horz=False
Setbuddyint=True
Base=0 - 10进制
HotTrack=False
Thousands=False
Wrap=False
Max=255
Min=1
Value=20
Enabled=True
Visible=True
Left=72
Top=28
Width=14
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[UpDown]
Name=UpDown2
Index=2
Style=1 - 在伙伴右边
Buddy=Text2(0)
Horz=False
Setbuddyint=True
Base=0 - 10进制
HotTrack=False
Thousands=False
Wrap=False
Max=100
Min=1
Value=0
Enabled=True
Visible=True
Left=77
Top=81
Width=18
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[UpDown]
Name=UpDown2
Index=3
Style=1 - 在伙伴右边
Buddy=Text2(1)
Horz=False
Setbuddyint=True
Base=0 - 10进制
HotTrack=False
Thousands=False
Wrap=False
Max=255
Min=1
Value=0
Enabled=True
Visible=True
Left=76
Top=133
Width=18
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text2
Index=0
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=1
Enabled=True
Visible=True
MaxLength=8
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=12
Top=78
Width=51
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text2
Index=1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=1
Enabled=True
Visible=True
MaxLength=8
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=12
Top=131
Width=51
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label3
Index=0
Style=0 - 无边框
Caption=初始结束页面:
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=13
Top=111
Width=156
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label3
Index=1
Style=0 - 无边框
Caption=最大页面:
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=180
Top=110
Width=206
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text2
Index=2
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=1
Enabled=True
Visible=True
MaxLength=8
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=178
Top=78
Width=51
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[TextBox]
Name=Text2
Index=3
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=1
Enabled=True
Visible=True
MaxLength=8
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=True
AutoHScroll=True
AutoVScroll=False
Left=178
Top=129
Width=51
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[UpDown]
Name=UpDown2
Index=4
Style=1 - 在伙伴右边
Buddy=Text2(3)
Horz=False
Setbuddyint=True
Base=0 - 10进制
HotTrack=False
Thousands=False
Wrap=False
Max=255
Min=1
Value=0
Enabled=True
Visible=True
Left=243
Top=132
Width=18
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[UpDown]
Name=UpDown2
Index=5
Style=1 - 在伙伴右边
Buddy=Text2(2)
Horz=False
Setbuddyint=True
Base=0 - 10进制
HotTrack=False
Thousands=False
Wrap=False
Max=100
Min=1
Value=0
Enabled=True
Visible=True
Left=244
Top=80
Width=18
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label2
Index=1
Style=0 - 无边框
Caption=最小页面:
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=179
Top=57
Width=206
Height=16
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

#include Once "Afx/AfxPrinter.inc" 'ln 103 
Sub Form打印_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   Dim xc(...) As Wstring * 128 = {_
      vfb_LangString("显示“打印设置”对话框，而不是“打印”对话框") , _ ' PD_PRINTSETUP
      vfb_LangString("禁用“打印到文件”复选框") , _ 'PD_DISABLEPRINTTOFILE
      vfb_LangString("隐藏“打印到文件”复选框") , _ ' PD_HIDEPRINTTOFILE
      vfb_LangString("隐藏和禁用“网络”按钮") , _ 'PD_NONETWORKBUTTON
      vfb_LangString("禁用“页”单选按钮和关联的编辑控件") , _ ' PD_NOPAGENUMS
      vfb_LangString("选择“页”单选按钮") , _ ' PD_PAGENUMS
      vfb_LangString("禁用“选择”单选按钮") , _ ' PD_NOSELECTION
      vfb_LangString("选择“选择”单选按钮") , _ ' PD_SELECTION
      vfb_LangString("防止在没有默认打印机时显示警告消息")  _ ' PD_NOWARNING
       } '
   Dim i As Long
   For i = 0 To UBound(xc)
      YFList1.AddItem(xc(i))
   Next
   YFList1.ListIndex = -1
End Sub
Function Copy打印() As String
   Dim 打印份数 As Long = Text1.Text.ValLong
   Dim 起始页面 As Long = Text2(0).Text.ValLong
   Dim 结束页面 As Long = Text2(1).Text.ValLong
   Dim 最小页面 As Long = Text2(2).Text.ValLong
   Dim 最大页面 As Long = Text2(3).Text.ValLong
   
   Dim i         As Long
   Dim 选项(...) As ZString * 30 = {"PD_PRINTSETUP" ,"PD_DISABLEPRINTTOFILE" ,"PD_HIDEPRINTTOFILE" ,"PD_NONETWORKBUTTON" ,"PD_NOPAGENUMS" , _
      "PD_PAGENUMS" ,"PD_NOSELECTION" ,"PD_SELECTION" ,"PD_NOWARNING" ,"OFN_SHOWHELP"}
   Dim g选项 As String
   For i = 0 To UBound(选项)
      If YFList1.ItemSel(i) Then
         If Len(g选项) Then g选项 &= " Or " & 选项(i) Else g选项 = 选项(i)
      End If
   Next
   Dim m As String = "'#include Once ""Afx / AfxPrinter.inc"" '必须把此行放到起始模块 " & vbCrLf & _
      "   Dim As WORD nCopies ="          & 打印份数 & " ,nFromPage  =" & 起始页面 & " ,nToPage =" & 结束页面 & vbCrLf & _
      "   Dim nDc As HDC '打印机DC"    & vbCrLf   & _
      "   If AfxPrinterDialog(hWndForm ," & g选项    & " ,nDc ,nCopies ,nFromPage ,nToPage ," & 最小页面 & " ," & 最大页面 & ") Then" & vbCrLf & _
      "      '开始打印  返回用户选择：nCopies=打印份数 nFromPage=起始页面 nToPage=结束页面" & vbCrLf & _
      "      If nDc Then"        & vbCrLf & _
      "         StartPage nDc '打印开始了，开始新的一页"        & vbCrLf & _
      "         '执行绘画函数。"        & vbCrLf & _
      "         EndPage nDc  '结束该页，立即打印"        & vbCrLf & _
      "         DeleteDC nDc '清理句柄"        & vbCrLf & _
      "      End If"        & vbCrLf & _
      "   Else "        & vbCrLf & _
      "      '取消打印" & vbCrLf & _
      "   End If"       & vbCrLf
   Function = m
End Function

Sub Form打印_Command1_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   Dim 打印份数 As WORD = Text1.Text.ValLong
   Dim 起始页面 As WORD = Text2(0).Text.ValLong
   Dim 结束页面 As WORD = Text2(1).Text.ValLong
   Dim 最小页面 As WORD = Text2(2).Text.ValLong
   Dim 最大页面 As WORD = Text2(3).Text.ValLong
   
   Dim i         As Long
   Dim 选项(...) As Long = {PD_PRINTSETUP ,PD_DISABLEPRINTTOFILE ,PD_HIDEPRINTTOFILE ,PD_NONETWORKBUTTON ,PD_NOPAGENUMS , _
      PD_PAGENUMS ,PD_NOSELECTION ,PD_SELECTION ,PD_NOWARNING ,OFN_SHOWHELP}
   Dim g选项 As Long
   For i = 0 To UBound(选项)
      If YFList1.ItemSel(i) Then g选项 Or= 选项(i)
   Next
   Dim As WORD nCopies = 打印份数 ,nFromPage = 起始页面 ,nToPage = 结束页面
   Dim nDc As HDC '打印机DC
   If AfxPrinterDialog(hWndForm ,g选项 ,nDc ,nCopies ,nFromPage ,nToPage ,最小页面 ,最大页面) Then
      '开始打印  返回用户选择：nCopies=打印份数 nFromPage=起始页面 nToPage=结束页面
      If nDc Then DeleteDC nDc
   Else
      '取消打印"
   End If
   
End Sub

Sub Form打印_CommandCopy_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
       FormCopy.Show(hWndForm ,True)
End Sub

Sub Form打印_CommandInsert_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   InsertText(vbCrLf & wStrToUtf8(Copy打印))
   Form主.Close
End Sub

Sub Form打印_CommandEsc_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   Form主.Close
End Sub





