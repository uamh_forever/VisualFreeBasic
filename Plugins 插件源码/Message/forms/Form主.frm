﻿#VisualFreeBasic_Form#  Version=5.8.2
Locked=0

[Form]
Name=Form主
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP
Style=3 - 常规窗口
Icon=Default.ico
Caption=可视化对话框编辑器
StartPosition=2 - 父窗口中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=650
Height=473
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Timer]
Name=Timer1
Index=-1
Interval=50
Enabled=True
Left=46
Top=102
Tag=

[YFproTab]
Name=YFproTab1
Index=-1
Style=0 - 无边框
SelStyle=0 - 反圆角
Enabled=True
Visible=True
TabH=28
AddButton=False
DownButton=False
Word=SYS,8
Back=SYS,15
Move=SYS,3
SelBack=SYS,13
SelWord=SYS,14
LeftIndent=10
Left=1
Top=1
Width=644
Height=35
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

Sub Form主_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   YFproTab1.AddTab(FormMessage.Show(hWndForm),"消息","MsgBox()",&HEF9F,2 ) 
   YFproTab1.AddTab(Form打开.Show(hWndForm),"打开","FF_OpenFileDialog()",&HE66C,2 ) 
   YFproTab1.AddTab(Form保存.Show(hWndForm),"保存","FF_SaveFileDialog()",&HEFFB ,2) 
   YFproTab1.AddTab(Form目录.Show(hWndForm),"目录","AfxBrowseForFolder()",&HE6CD ,2) 
   YFproTab1.AddTab(Form色彩.Show(hWndForm),"色彩","AfxChooseColorDialog()",&HE601,2 ) 
   YFproTab1.AddTab(Form输入.Show(hWndForm),"输入","AfxInputBox()",&HF06A,2 ) 
   YFproTab1.AddTab(Form打印.Show(hWndForm) ,"打印" ,"AfxPrinterDialog()" ,&HE62D,2) 
   YFproTab1.Selected = 0
   Form主_YFproTab1_SelChange(hWndForm,0,0,0)
End Sub

Sub Form主_WM_Size(hWndForm As hWnd, fwSizeType As Long, nWidth As Long, nHeight As Long)  '窗口已经改变了大小
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'fwSizeType = SIZE_MAXHIDE     SIZE_MAXIMIZED   SIZE_MAXSHOW    SIZE_MINIMIZED    SIZE_RESTORED  
   ''            其他窗口最大化   窗口已最大化     其他窗口恢复    窗口已最小化      窗口已调整大小
   'nWidth nHeight  是客户区大小，不是全部窗口大小。
   If fwSizeType = SIZE_MINIMIZED Then Return 
   'xxx.Move AfxScaleX(5), AfxScaleY(5), nWidth - AfxScaleX(10), nHeight - AfxScaleY(30)
   YFproTab1.Move 0 ,0 ,nWidth ,AfxScaleY(35)
   YFproTab1.SetPosAndSize 0,AfxScaleY(35),nWidth ,nHeight - AfxScaleY(35)
   
End Sub

Sub Form主_YFproTab1_SelChange(hWndForm As hWnd ,hWndControl As hWnd ,oldIndex As Long ,newIndex As Long) '用户切换标签后事件
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   '只有用户点击标签后才有本事件，写代码操作不会发生本事件（避免发生死循环问题）
   'oldIndex =旧的标签索引 newIndex=新的标签索引
   Dim sw As Long
   Dim sh As Long
   
   Select Case newIndex
      Case 0
         sw = 478
         sh = 445 + 35
      Case 1 
         sw = 606
         sh = 469 + 35
      Case 2
         sw = 606
         sh = 469 + 35
      Case 3
         sw = 604
         sh = 511 + 35
      Case 4
         sw = 478
         sh = 121 + 35
      Case 5
         sw = 478
         sh = 308 + 35
      Case 6
         sw = 478
         sh = 354 + 35
   End Select
   sw        = AfxScaleX(sw) + (Me.Width - Me.ScaleWidth)
   sh        = AfxScaleX(sh) + (Me.Height - Me.ScaleHeight)
   Me.Width  = sw
   Me.Height = sh
End Sub

Sub Form主_Timer1_WM_Timer(hWndForm As hWnd ,wTimerID As Long) '定时器
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   ''         本控件为功能控件，就是无窗口，无显示，只有功能。如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码
   'wTimerID  定时器ID，区分不同定时器的编号
   Timer1.Enabled = False
   Select Case YFproTab1.Selected
      Case 0
         FormMessage.TextlpCaption.SetFocus
      Case 1
         Form打开.Text标题.SetFocus
      Case 2
         Form保存.Text标题.SetFocus
      Case 3
         Form目录.Text标题.SetFocus
      Case 4
         Form色彩.Text1.SetFocus
      Case 5
         Form输入.Text标题.SetFocus
      Case 6
         Form打印.Text1.SetFocus
   End Select
End Sub

Sub Form主_YFproTab1_WM_LButtonUp(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '释放鼠标左键
   'MouseFlags  MK_CONTROL   MK_LBUTTON     MK_MBUTTON     MK_RBUTTON    MK_SHIFT     MK_XBUTTON1       MK_XBUTTON2 
   ''           CTRL键按下   鼠标左键按下   鼠标中键按下   鼠标右键按下  SHIFT键按下  第一个X按钮按下   第二个X按钮按下
   '检查什么键按下用  If (MouseFlags And MK_CONTROL)<>0 Then CTRL键按下 
   Timer1.Enabled =True 
End Sub




             