﻿#VisualFreeBasic_Form#  Version=5.5.6
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_SYSMENU,WS_MAXIMIZEBOX,WS_MINIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CHILD
Style=0 - 无边框
Icon=
Caption=Form1
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=440
Height=335
TopMost=False
Child=True
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[RichEdit]
Name=RichEdit1
Index=-1
Style=0 - 无边框
TextScrollBars=1 - 垂直滚动条
Text=RichEdit
Enabled=True
Visible=True
MaxLength=0
Font=微软雅黑,9
TextAlign=0 - 左对齐
Locked=False
HideSelection=False
Multiline=True
SaveSel=True
NoScroll=False
AutoHScroll=True
AutoVScroll=False
ContextMenu=True
LeftMargin=5
RightMargin=0
Left=23
Top=50
Width=366
Height=156
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

Sub Form1_Shown(hWndForm As hWnd ,UserData As Integer) '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   Dim vfbAPP As APP_TYPE Ptr = GetExeAPP()
   Dim ss     As String       = GetFileStrW(vfbAPP->Path & "Settings\Note.txt")
   If Len(ss) = 0 Then ss = vfb_LangString("这里编程笔记，关VFB后自动保存。")
   RichEdit1.BackColor = HSBtoRGB_Gdi(ThemeWin.colors(Themes_窗口).nBg )
   Dim cf As CHARFORMAT
   cf.cbSize      = SizeOf(cf)
   cf.dwMask      = CFM_COLOR
   cf.crTextColor = 色差绝对HSBtoGDI(ThemeWin.colors(Themes_窗口).nBg)
   SendMessage(RichEdit1.hWnd ,EM_SETCHARFORMAT ,SCF_ALL ,@cf)   
   RichEdit1.Text = ss
   
End Sub

Sub Form1_WM_Size(hWndForm As hWnd, fwSizeType As Long, nWidth As Long, nHeight As Long)  '窗口已经改变了大小
   'fwSizeType = SIZE_MAXHIDE     SIZE_MAXIMIZED   SIZE_MAXSHOW    SIZE_MINIMIZED    SIZE_RESTORED
   ''            其他窗口最大化   窗口已最大化     其他窗口恢复    窗口已最小化      窗口已调整大小
   'nWidth nHeight  是客户区大小，不是全部窗口大小。
   if fwSizeType = SIZE_MINIMIZED Then Return
   'xxx.Move AfxScaleX(5), AfxScaleY(5), nWidth - AfxScaleX(10), nHeight - AfxScaleY(30)
   RichEdit1.Move 0, 0, nWidth, nHeight
End Sub

Function Form1_WM_Close(hWndForm As hWnd) As LResult  '即将关闭窗口，返回非0可阻止关闭

   Function = FALSE ' 如果想阻止窗口关闭，则应返回 TRUE 。
End Function




