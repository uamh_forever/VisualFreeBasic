﻿
'在 VisualFreeBasic 选项里，可以选择是不是启用本插件，禁用后，除了 initialization 其它全部失效，也就是 initialization 不受选项控制，必须要执行的。
'VisualFreeBasicStart 函数，除了【启用】选项，还受到 选项里【启动执行】 控制，不选择启动时是不会被执行的。

'由于VFb是32位的，因此编译的 DLL 也必须是32位，不可以是64位DLL
'编译完成后，若无出错，可以按提示选择自动重启VFB生效（选项里已启用此插件才行），重启后自动恢复先前开启的工程。
'假如插件造成VFB无法使用，可以把 VFB\Settings\StartupReBak.txt 文件更名为 VFB\Settings\StartupReplace.txt 后重开软件还原。


Function initialization(xName As String ,xExplain As String) As Long Export '初始化，VFB启动后加载插件时，调用此函数，返回插件名称 （不可执行其它代码）
                        '多个插件按优先度依次执行（优先度在VFB选项里设置，每个插件都能执行到）
                        '注意：由于此时VFB在做各种初始化工作，加载窗口还在提示中，请勿执行和调用EXE里各个输出函数，避免引发崩溃
                        '后面还有 VisualFreeBasicStart 为初始化完成后调用，弹窗及显示功能窗口，在初始化结束后使用。
                        '推荐在此做插件的初始化工作，非有必要请勿写在程序入口函数里。
   SetFunctionAddress() '设置函数地址 ，必须要做的
   xName    = vfb_LangString("启动页")              '插件名，在 标签 上显示的名称
   xExplain = vfb_LangString("启动VFB时显示的页面") '插件的说明解释，主要时在VFB插件管理里显示，让大家知道插件是干嘛的。
   Function = 3 '返回协议版本号，不同协议（发生接口变化）不能通用，会发生崩溃等问题，因此VFB主软件会判断此版本号，不匹配就不使用本插件。
End Function
Dim Shared SatrtupIDC As Long 
Sub MenuAdd(zMenu As HMENU, nPos As Long, tFont As HFONT, ByRef IDC As Long) Export  ' 创建VFB主菜单时调用，在这里可以添加菜单（不可执行其它代码）
   'zMenu   菜单句柄
   'nPos    当前位置，0-7 代表：文件 编辑 搜索  视图 工程 工具 帮助 插件
   'tFont   字体句柄，就是符号字体的句柄，需要传递给加载菜单画字体图标用
   'IDC     控件唯一识辨码，在这里作为菜单ID，必须是唯一的，使用后必须 IDC +=1 ，避免产生重复（请勿随便修改，造成VFb混乱）
   ''       自己添加菜单后，自己必须记住自己的 IDC ，好在事件中处理识辨是自己的菜单。
   
   'VFB自己添加完菜单后，再调用这里，这里---{只做添加菜单用，请勿执行其它东西，避免引发崩溃}---
   '注意：由于此时VFB在做各种初始化工作，加载窗口还在提示中，请勿执行和调用EXE里各个输出函数，避免引发崩溃
   ''     只可以用 Menu_Add_Jilu 函数添加，不要用API添加或修改，VFB无法自动处理，发生混乱
   
   '以下为添加菜单例题
   'Menu_Add_Jilu(zMenu, tFont, 图标的符号值,图标样式,IDC, 有效值, 菜单文字, 快捷键)
   '有效值(自动根据状态禁用或启用菜单功能):  =0始终有效 =1代码编辑器时有效 =2窗口区有效 =3有工程时有效 =4窗口和代码
   Select Case nPos
      Case 0 '文件
      Case 1 '编辑
      Case 2 '搜索
      Case 3 '视图
      Case 4 '工程
      Case 5 '工具
      Case 6 '帮助
      Case 7 '插件专用菜单，一般需要什么功能，可以增加到这里
         Menu_Add_Jilu(zMenu, tFont, 0, &H0,IDC, 0, vfb_LangString("启动页"), "")
         SatrtupIDC =IDC 
         IDC += 1 '唯一的，使用后必须 IDC +=1 
      Case 8 '代码编辑时，右键菜单， 字符转换 里的子菜单
      Case 9 '代码编辑时，右键菜单， 代码插入 里的子菜单
      Case 10 '代码编辑时，右键菜单， 代码格式 里的子菜单
      Case 11 '工程设置菜单，用在工具栏最后面工程类型按钮
      Case 12 '标签菜单 ，用在 TAB0 右键菜单
      Case 13 '代码编辑器，右键菜单
      Case 14 '工程列表，右键菜单
      Case 15 '窗口编辑器，右键菜单
   End Select
   
   'Menu_Add_Jilu(zMenu, tFont, &HE657, &H0,IDC, 0, ("新建(&N)..."), "Ctrl+N")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, &HE64D, &H0,IDC, 0, ("打开(&O)..."), "Ctrl+O")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, &H0,&H0,IDC, 3, ("关闭当前工程(&R)"), "Ctrl+Q")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, &HE6B2, &H0,IDC, 4, ("保存全部(&E)"), "Ctrl+S")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, &H0,&H0,IDC, 3, ("另存为模版"), "")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, 0, 0, 0, 0, "", "")   '分割符号
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, &HE65B, &H0,IDC, 0, ("最近文件"), "Ctrl+T")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, 0, 0, 0, 0, "", "")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, &HE637, &H0,IDC, 0, ("命令提示符"), "")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, 0, 0, 0, 0, "", "")
   'IDC +=1
   'Menu_Add_Jilu(zMenu, tFont, &HE7C2, &H0,IDC, 0, ("退出(&X)"), "")
End Sub

Sub ToolAdd(ByRef IDC As Long) Export '在工具栏 增加按钮（不可执行其它代码）
   '******注意：由于此时VFB在做各种初始化工作，加载窗口还在提示中，请勿执行和调用EXE里各个输出函数，避免引发崩溃*****
   'IDC     控件唯一识辨码，在这里作为菜单ID，必须是唯一的，使用后必须 IDC +=1 ，避免产生重复（请勿随便修改，造成VFb混乱）
   
   '用 InsertTool(position As Long, IcoWchr As Long, Colour As Long, CommandID As Long, xTyep As Long, xTips As String,FileImg As String="") '插入工具栏按钮，此函数只在这里有效果，其它地方无效。
   'position As Long '在此位置插入按钮，从1 开始 到 ** ，分割线也算1个位置，多个插件插入按钮后引起位置改变，注意插件优先度，最后看运行结果。
   '位置 = 0 就不操作，只设置位置，CommandID = 0 表示是分割符
   'IcoWchr   As Long   '宽字符符号值
   'Colour    As Long   '显示的颜色
   'CommandID As Long   '菜单的命令ID号
   'xTyep     As Long   ' 属性  =0始终有效 =1代码编辑器时有效 =2窗口区有效 =3有工程时有效 =4窗口和代码
   'xTips     As String ' 提示
   'FileImg   As String  ' 图标文件名，不包含路径，文件必须在 VFB\Settings\img\tool  里 ，如 "1.ico"
   
End Sub

Sub VisualFreeBasicStart(nThemes As TYPE_THEMES, nThemesWin As TYPE_THEMES) Export 'VFB 初始结束，可以开始执行其它操作
   '初始化完成后调用，可以在这里弹窗及显示功能窗口，
   '多个插件按优先度依次执行（优先度在VFB选项里设置，每个插件都能执行到）
   ThemeCode = nThemes        '代码主题
   ThemeWin = nThemesWin      '窗口主题
   Form1.Show GetMainWinHandle()  '必须是显示在VFB主窗口上。
   CreateTab(0, vfb_LangString("启动页"), vfb_LangString("普通启动页面"), Form1.hWnd)  '注意，创建在标签里的，窗口属性必须是子窗口
   'nPos  位置，=0 增加在大窗口里，=1 在 库、属性这小窗口里 =2 在底部  (注：只有在大窗口里可以关闭，其它地方增加后不可以被关闭)
   'nText 名称
   'xTips 提示
   'nHwnd 窗口句柄，需要自己加载个新窗口给它，窗口要带 子窗口属性，才可以。
   'wIco  图标，字体图标的数值
End Sub



Function MenuCommand(wID As Long) As Long Export '点击VFB菜单后调用，返回非零后，VFB或优先度低的就不再被执行。等所有插件执行完成后，VFB才执行自己的代码。  wID 是菜单命令ID
   '这里可以根据 wID 执行自己添加的菜单命令，也可以执行别的插件和VFB的命令。
   Select Case wID
      case SatrtupIDC
         Form1.Show GetMainWinHandle()  '必须是显示在VFB主窗口上。
         CreateTab(0, vfb_LangString("启动页"), vfb_LangString("普通启动页面"), Form1.hWnd) '注意，创建在标签里的，窗口属性必须是子窗口
         '(注：只有在大窗口里可以关闭，其它地方增加后不可以被关闭)
   End Select
   Function = 0
End Function

Function CompileStart(ProFile As String, nFile As String) As Long Export  '编译前调用此函数，此时还未生成临时编译文件。返回非零就退出，停止编译，优先度低的就无法执行。
   'ProFile   工程文件，包含文件夹
   'nFile      输出文件，包含文件夹，如软件为 EXE扩展名，DLL为DLL扩展名，64位为 在 release64 路径里。
   
   Function = 0 
End Function

Function CompileEnd(ProFile As String, nFile As String) As Long Export  '编译后调用此函数，返回非零就退出，后续操作，优先度低的就无法执行。
   'ProFile   工程文件，包含文件夹
   'nFile      输出文件，包含文件夹，如软件为 EXE扩展名，DLL为DLL扩展名，64位为 在 release64 路径里。
   
   Function = 0 
End Function

Sub WinThemeChange(nThemes As TYPE_THEMES) Export '窗口主题已经修改
   ThemeWin = nThemes
   
End Sub

Sub CodeThemeChange(nThemes As TYPE_THEMES) Export '代码主题已经修改
   ThemeCode = nThemes
   
End Sub

Function VisualFreeBasicClose(hWndForm As hWnd) As LResult Export
   'hWndForm   VFB 主窗口，来自 VFB_WM_Close 事件
   '即将关闭窗口，返回非0可阻止关闭  Return True
   
   Function =0 
End Function

