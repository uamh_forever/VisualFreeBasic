﻿#VisualFreeBasic_Form#  Version=5.8.3
Locked=0

[Form]
Name=Form2
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP
Style=3 - 常规窗口
Icon=tianjia.ico
Caption=控件属性
StartPosition=2 - 父窗口中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=609
Height=402
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=0
Style=0 - 无边框
Caption=模板：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=5
Top=10
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=0
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=True
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=43
Top=8
Width=555
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=False
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=1
Style=0 - 无边框
Caption=名称：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=5
Top=38
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=43
Top=36
Width=145
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=2
Style=0 - 无边框
Caption=必须是英文字母，而且不能带符号
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=201
Top=42
Width=355
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=3
Style=0 - 无边框
Caption=提示：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=5
Top=68
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=2
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=43
Top=66
Width=360
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=4
Style=0 - 无边框
Caption=鼠标停留在控件图标上提示的内容
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=410
Top=71
Width=355
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=5
Style=0 - 无边框
Caption=图标：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=5
Top=96
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=3
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=43
Top=94
Width=60
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=6
Style=0 - 无边框
Caption=字体图标的字符值，任意1个 UNICODE 字符值。
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=151
Top=98
Width=270
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=7
Style=0 - 无边框
Caption=特征：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=5
Top=131
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[ComboBox]
Name=Combo1
Index=-1
Style=0 - 标签和下拉框
Custom=
OwnDraw=0 - 系统绘制
LabelHeight=20
ItemHeight=15
HasString=False
Sorted=False
NoHeight=False
AutoHscroll=True
MaxLength=0
Uppercase=False
Lowercase=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=43
Top=128
Width=246
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=8
Style=0 - 无边框
Caption=唯一：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=3
Top=160
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=一个窗口只能有1个此控件
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=41
Top=159
Width=347
Height=22
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=9
Style=0 - 无边框
Caption=配置：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=3
Top=189
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=4
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=41
Top=187
Width=175
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=10
Style=0 - 无边框
Caption=文件夹名称，控件属性和事件的配置所在的子文件夹名称。
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=226
Top=193
Width=362
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=11
Style=0 - 无边框
Caption=类名：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=4
Top=216
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=5
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=41
Top=214
Width=175
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=12
Style=0 - 无边框
Caption=控件类文件名，用来提供给编译的程序使用。
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=227
Top=222
Width=362
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=13
Style=0 - 无边框
Caption=DLL：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=3
Top=242
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=6
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=41
Top=241
Width=175
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=14
Style=0 - 无边框
Caption=生成的DLL名称，处理编辑和编译用的DLL
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=226
Top=247
Width=362
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=15
Style=0 - 无边框
Caption=分组：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=3
Top=269
Width=54
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=7
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=41
Top=267
Width=175
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Label]
Name=Label1
Index=16
Style=0 - 无边框
Caption=分组名称，2个中文到4个最佳，属于多个分组，用英文豆号分割
Enabled=True
Visible=True
ForeColor=SYS,16
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=225
Top=274
Width=355
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=8
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,15
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=True
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=6
Top=296
Width=588
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=Command1
Index=-1
Caption=创建控件
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=421
Top=336
Width=109
Height=31
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=取消
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=537
Top=336
Width=59
Height=31
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line1
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无箭头
ArrowStartH=0 - 无箭头
ArrowEndW=0 - 无箭头
ArrowEndH=0 - 无箭头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=7
Top=329
Width=590
Height=4
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=9
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=110
Top=94
Width=28
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。
Type ColToolType '控件工具
   sName  As String   '名称，大小写
   uName As String    '名称，大写，用来不区分大小写对比查找
   sTips As String   '鼠标提示，在控件显示区提示用
   Folder As String  '控件配置文件夹名，不带路径。路径固定为：app.path + Control
   ClassFile As String '控件类文件名，在 Folder 文件夹里的类声明文件名
   ProLib    As String '处理编译和编辑的DLL文件名，在 Folder 文件夹里
   Group     As String '分组
   sVale As Long   '字体图标值，在控件显示区显示用
   Feature As Long '特征 =0 不使用 =1 主窗口 =2 普通控件  =3 虚拟控件有界面 =4 虚拟控件无界面
   Only As Long      '是否是唯一的，就是一个窗口只能有1个此控件
End Type
Dim Shared ctt() As ColToolType
Sub Form2_Shown(hWndForm As hWnd, UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   Me.UserData(0)=UserData
   Dim pp As String Ptr = Cast(Any Ptr, UserData)
   Dim hItem As YFTreeViewData Ptr = Form1.YFTreeView1.Selection
   If hItem = 0 Then Return
   Dim aa    As Long   = hItem->DataValue
   Dim vfbAPP As APP_TYPE Ptr = GetExeAPP()
   Text1(0).Text = PathActualToRelative(gcmb(aa), vfbAPP->Path)
   Text1(1).Text = *pp
   Text1(2).Text = vfb_LangString("这是个控件")
   Text1(3).Text = "&H65B0" '新
   Combo1.AddItem vfb_LangString("不使用")
   Combo1.AddItem vfb_LangString("普通控件（有窗口句柄）")
   Combo1.AddItem vfb_LangString("虚拟控件有界面（无句柄）")
   Combo1.AddItem vfb_LangString("虚拟控件无界面（组件）" )
   Dim ff As Long = InStr(gcmb(aa), "\Code\")
   if ff = 0 Then
      MsgBox(hWndForm, vfb_LangString("模板文件出错"), _
         MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Return
   End if
   Dim kp As String = Left(gcmb(aa), ff)
   Dim kk As String = DirW(kp & "*.inc")
   if len(kk) = 0 Then 
      MsgBox(hWndForm, vfb_LangString("模板出错，非控件结构模板"), _
         MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Return
   End if
   Dim ks As String = GetFileStrW(kp & kk)  '读取控件类，判断是什么类型
   Dim gg As String 
   if InStr(ks, " Class_Control") Then 
      Combo1.ListIndex = 1
      gg=vfb_LangString("其它")
   ElseIf InStr(ks, " Class_Virtual") Then 
      Combo1.ListIndex = 2
      gg=vfb_LangString("其它,虚拟控件")
   Else
      Combo1.ListIndex = 3
      gg=vfb_LangString("其它,组件")
   End if 
   Text1(4).Text = *pp
   Text1(5).Text ="Cls" & *pp  & ".inc"
   Text1(6).Text ="Pro"  & *pp  & ".dll"
   Text1(7).Text = gg 
   
   '读取先有控件
   Dim lName() As WIN32_FIND_DATAW, uu As Long
   Dim ki As Long  
   Dim i As Long 
   '获取控件里子文件夹
   if GetDIR(vfbAPP->Path & "Control\*.*", lName()) Then
      ReDim ctt(UBound(lname))
      For i = 0 To UBound(lname)
         If (lname(i).dwFileAttributes And fbDirectory) <> 0 Then
            dim pp As String = vfbAPP->Path & "Control\" & CWSTRtoString(lname(i).cFileName) & "\Settings.ini"
            if AfxFileExists(pp) Then '控件属性
               ctt(ki).Folder = UCase(CWSTRtoString(lname(i).cFileName))
               获取控件配置(pp, @ctt(ki))
               ki += 1
            End if
         End if
      Next
   End if   
   if ki Then ReDim Preserve ctt(ki -1)

      
   '提取出使用过的组名称
   gg = ""
   Dim bb() As String 
   if UBound(ctt)>0  Then 
      
      For i = 0 To UBound(ctt)
         gg &= ctt(i).Group & ","
      Next 
      if vbSplit(gg, ",", bb()) Then 
         ks =","
         For i = 0 To UBound(bb)
            gg = Trim(bb(i))
            if Len(gg) Then 
               if InStr(ks,"," & gg & ",") = 0 Then  ks &= gg & ","  '去除重复的
            End if 
         Next 
         Text1(8).Text = Mid(ks ,2,Len(ks)-2)
      end if 
   end if 
   
   
End Sub

Sub Form2_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
   Me.Close 
   
End Sub

Sub Form2_Command1_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   Dim tt(7) As String
   Dim i     As Long
   For i = 0 To 7
      tt(i) = Trim(Text1(i).Text)
   Next
   if Len(tt(1)) = 0 Then
      MsgBox(hWndForm ,vfb_LangString("没有控件名，请重新输入") , _
         MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Text1(0).SetFocus
      Return
   End if
   Dim rtt As String = UCase(tt(1))
   For i = 0 To Len(rtt) -1
      if rtt[i] < 65 Or rtt[i] > 90 Then
         MsgBox(hWndForm ,vfb_LangString("控件名里不可以有符号，请重新输入") , _
            MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
         Text1(0).SetFocus
         Return
      End if
   Next
   if InStr(tt(4) ,Any "/\:*""<>|? ") Then
      MsgBox(hWndForm ,vfb_LangString("配置是文件夹名称，不可以包含特殊字符，也不能有空格，请修改。") & vbCrLf & vfb_LangString("特殊字符") & "  /\:*""<>|? " & vfb_LangString("以及空格") , _
         "VisualFreeBasic" ,MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Text1(4).SetFocus
      Return
   end if
   if InStr(tt(5) ,Any "/\:*""<>|? ") Then
      MsgBox(hWndForm ,vfb_LangString("类名是文件名称，不可以包含特殊字符，也不能有空格，请修改。") & vbCrLf & vfb_LangString("特殊字符") & "  /\:*""<>|? " & vfb_LangString("以及空格") , _
         "VisualFreeBasic" ,MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Text1(5).SetFocus
      Return
   End If
   if InStr(tt(6) ,Any "/\:*""<>|? ") Then
      MsgBox(hWndForm ,vfb_LangString("DLL是文件名称，不可以包含特殊字符，也不能有空格，请修改。") & vbCrLf & vfb_LangString("特殊字符") & "  /\:*""<>|? " & vfb_LangString("以及空格") , _
         "VisualFreeBasic" ,MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Text1(6).SetFocus
      Return
   end if
   Dim vfbAPP As APP_TYPE Ptr = GetExeAPP()
   
   Dim ks as String
   if UBound(ctt) > 0 Then
      ks = UCase(tt(1))
      For i = 0 To UBound(ctt)
         if ctt(i).uName = ks Then
            MsgBox(hWndForm ,tt(1) & vfb_LangString(" 控件名已经存在，请重新输入") , _
               MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
            Text1(0).SetFocus
            Return
         End if
      Next
      ks = UCase(tt(4))
      For i = 0 To UBound(ctt)
         If ctt(i).Folder = ks Then
            MsgBox(hWndForm ,tt(4) & vbCrLf & vfb_LangString("配置文件夹已经存在，请改用其它名称") , _
               "VisualFreeBasic" ,MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
            Text1(4).SetFocus
            Return
         End if
      Next
      ks = UCase(tt(5))
      For i = 0 To UBound(ctt)
         if ctt(i).ClassFile = ks Then
            MsgBox(hWndForm ,tt(5) & vbCrLf & vfb_LangString("控件类文件名已经存在，请改用其它名称") , _
               "VisualFreeBasic" ,MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
            Text1(5).SetFocus
            Return
         End if
      Next
      ks = UCase(tt(6))
      For i = 0 To UBound(ctt)
         if ctt(i).ProLib = ks Then
            MsgBox(hWndForm ,tt(6) & vbCrLf & vfb_LangString("生成的DLL名称已经存在，请改用其它名称") , _
               "VisualFreeBasic" ,MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
            Text1(6).SetFocus
            Return
         End if
      Next
   End if
   
   tt(4) = vfbAPP->Path & "Control\" & tt(4)
   AfxMkDir tt(4)
   tt(4) &= "\"
   Dim hItem As YFTreeViewData Ptr = Form1.YFTreeView1.Selection
   If hItem = 0 Then Return
   Dim aa    As Long   = hItem->DataValue
   Dim ff As Long   = InStr(gcmb(aa) ,"\Code\") '模板
   Dim kp As String = Left(gcmb(aa) ,ff)        '模板路径
   '复制模板到目标文件夹 ==================================================================
   If ffCopyDir(hWndForm ,kp ,tt(4)) <> 1 Then
      MsgBox(hWndForm ,vfb_LangString("创建控件工程失败，无法建立文件。") , _
         "VisualFreeBasic" ,MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Return
   End if
   '设置控件类 ==================================================================
   Dim kk As String = DirW(tt(4) & "*.inc") '控件类
   AfxName tt(4) & kk ,tt(4) & tt(5)
   ks = GetFileStrW(tt(4) & tt(5)) '读模板的控件类，把我们自己的类名换上去。
   ks = YF_Replace(ks ,"Class_Template" ,"Class_" & tt(1))
   SaveFileStr tt(4) & tt(5) ,ks
   kk = DirW(tt(4) & "Code\*.ffp") '控件工程
   if Len(kk) = 0 Then
      MsgBox(hWndForm ,vfb_LangString("无法找到控件工程文件。") , _
         "VisualFreeBasic" ,MB_OK Or MB_ICONERROR Or MB_DEFBUTTON1 Or MB_APPLMODAL)
      Return
   End if
   '设置控件工程 ==================================================================
   AfxName tt(4) & "Code\" & kk ,tt(4) & "Code\" & tt(1) & "_dll.ffp"
   ks = GetFileStrW(tt(4) & "Code\" & tt(1) & "_dll.ffp") '读模板的控件类，把我们自己的类名换上去。
   ks = YF_Replace(ks ,"ProjectName=Template" ,"ProjectName=" & tt(1))
   ks = YF_Replace(ks ,"LastRunFilename=ProTemplate.dll" ,"LastRunFilename=" & tt(6))
   ks = YF_Replace(ks ,"\ClsTemplate.inc|" ,"\" & tt(5) & "|")
   SaveFileStr tt(4) & "Code\" & tt(1) & "_dll.ffp" ,Chr(&HEF ,&HBB ,&HBF) & StrToUtf8(ks)
   '设置控件属性，改默认控件名 =============================================================
   ks = GetFileStrW(tt(4) & "Attribute.ini")
   Dim ksl() As String
   if vbSplit(ks ,vbCrLf ,ksl()) Then
      For i = 0 To UBound(ksl)
         ksl(i) = Trim(ksl(i))
         Dim ff As Long = InStr(ksl(i) ,"=")
         if ff > 0 Then
            Dim jj As String = Trim(Mid(ksl(i) ,ff + 1))
            if UCase(Trim(Left(ksl(i) ,ff -1))) = "DEFAULT" Then
               ksl(i) = "Default=" & tt(1)
               Exit For
            End if
         End if
      Next
      SaveFileStrW tt(4) & "Attribute.ini" ,FF_Join(ksl() ,vbCrLf)
   End If
   
   '控件配置中，==================================================================
   ks = GetTextFileStrUTF8(tt(4) & "Code\modules\Module1.inc")
   ks = YF_Replace(ks ,"{ColTool.sName}" ,StrToUtf8(tt(1)))
   ks = YF_Replace(ks ,"{ColTool.sTips}" ,StrToUtf8(tt(2)))
   ks = YF_Replace(ks ,"{ColTool.sVale}" ,Hex(ValInt(tt(3))))
   i  = Combo1.ListIndex
   If i > 0 Then i += 1
   ks = YF_Replace(ks ,"{ColTool.Feature}" ,Str(i))
   ks = YF_Replace(ks ,"{ColTool.Only}" ,IIf(Check1.Value ,"1" ,"0"))
   ks = YF_Replace(ks ,"{ColTool.ClassFile}" ,StrToUtf8(tt(5)))
   ks = YF_Replace(ks ,"{ColTool.GROUP}" ,StrToUtf8(tt(7)))
   ks = YF_Replace(ks ,"{ColTool.Sort}" ,Str(UBound(ctt) + 1))
   
   SaveFileStr tt(4) & "Code\modules\Module1.inc" ,Chr(&HEF ,&HBB ,&HBF) & ks
   
   '成功 =======
   OpenGongCen(tt(4) & "Code\" & tt(1) & "_dll.ffp")
   Dim pp As String Ptr = Cast(Any Ptr ,Me.UserData(0))
    *pp = "" '配置空，表示成功
   Me.Close
End Sub

Sub Form2_Text1_EN_Change(ControlIndex As Long ,hWndForm As hWnd ,hWndControl As hWnd)  '文本已经被修改（修改前用 EN_UPDATE
   Select Case ControlIndex
      Case 9
         Dim cc As WString * 3 = Text1(9).Text
         Dim bb As String = Text1(3).Text
         Dim ee As String = "&H" & Hex(Asc(cc ,1))
         if bb <> ee Then Text1(3).Text = ee
      Case 3
         Dim cc As WString * 3 = Text1(9).Text
         Dim bb As String = Text1(3).Text
         Dim kk As Long   = ValInt(bb)
         Dim ee As Long   = Asc(cc ,1)
         if kk <> ee Then Text1(9).Text = WChr(kk)
   End Select
   
End Sub
Sub 获取控件配置(pp As String ,nColTool As ColToolType Ptr)
   
   Dim bb() As String
   Dim uu   As Long = GetFileStrArray(pp ,bb())
   if uu = 0 Then Return
   Dim i As Long ,ff As Long ,jj As String ,gg As Long ,mm As String ,gai As Long ,sk As Long
   For i = 0 To uu -1
      ff = InStr(bb(i) ,"=")
      if ff > 0 Then
         jj = Trim(Mid(bb(i) ,ff + 1))
         Select Case UCase(Trim(Left(bb(i) ,ff -1)))
            Case "ICO"
               nColTool->sVale = ValInt(jj)
            Case "TIPS"
               nColTool->sTips = jj
            Case "FEATURE"
               nColTool->Feature = ValInt(jj)
            Case "CLASSFILE"
               nColTool->ClassFile = UCase(jj)
            Case "PROLIB"
               nColTool->ProLib = UCase(jj)
            Case "ONLY"
               nColTool->Only = ValInt(jj)
            Case "GROUP"
               nColTool->Group = jj
            Case "NAME"
               nColTool->sName = jj
               nColTool->uName = UCase(jj)
            Case "SORT"
               'nColTool->Sort =ValInt(jj)
         End Select
      End if
   Next
   
End Sub






