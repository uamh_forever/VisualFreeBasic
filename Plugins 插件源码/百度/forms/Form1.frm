﻿#VisualFreeBasic_Form#  Version=5.5.5
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_SYSMENU,WS_MAXIMIZEBOX,WS_MINIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_CHILD
Style=0 - 无边框
Icon=
Caption=
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=804
Height=453
TopMost=False
Child=True
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=True
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Miniblink]
Name=Miniblink1
Enabled=True
Visible=True
Url=about:blank
UserAgent=
HighDPI=True
MemCache=False
NewWin=False
CspCheck=True
NpapiPlus=False
Headless=False
JavaScript=True
Left=1
Top=25
Width=1024
Height=509
Layout=0 - 不锚定
Tab=True
Tag=

[Timer]
Name=Timer1
Index=-1
Interval=1000
Enabled=True
Left=358
Top=67
Tag=

[Button]
Name=Command1
Index=-1
Caption=暂停
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=3
Top=3
Width=34
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=43
Top=6
Width=988
Height=15
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False


[AllCode]
Dim Shared 计时 As Long 
Sub Form1_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。

   'Form1.WindowsZ=HWND_BOTTOM
End Sub

Sub Form1_WM_Size(hWndForm As hWnd ,fwSizeType As Long ,nWidth As Long ,nHeight As Long) '窗口已经改变了大小
   'fwSizeType = SIZE_MAXHIDE     SIZE_MAXIMIZED   SIZE_MAXSHOW    SIZE_MINIMIZED    SIZE_RESTORED
   ''            其他窗口最大化   窗口已最大化     其他窗口恢复    窗口已最小化      窗口已调整大小
   'nWidth nHeight  是客户区大小，不是全部窗口大小。
   If fwSizeType = SIZE_MINIMIZED Then Return
   'xxx.Move AfxScaleX(5), AfxScaleY(5), nWidth - AfxScaleX(10), nHeight - AfxScaleY(30)
   'Me.Miniblink1.Width = nWidth
   Dim kk As Ulong 
   Dim mk As Ulong = Get_Pro_Mo_Ad(GetCurrentProcessId() ,"node.dll",kk)
  API_FunTihuan(GetCurrentProcessId() ,mk ,DLL_Fun_Add("user32" ,"SetForegroundWindow") ,@oSetForegroundWindow)
  API_FunTihuan(GetCurrentProcessId() ,mk ,DLL_Fun_Add("user32" ,"SetFocus") ,@oSetForegroundWindow)
   
   
End Sub

Function oSetForegroundWindow(ByVal hWnd As hWnd) As WINBOOL
   Function = -1
End Function
Function oSetFocus(ByVal hWnd As hWnd) As hWnd
   Function = 0
End Function


Sub Form1_Timer1_WM_Timer(hWndForm As hWnd ,wTimerID As Long) '定时器
   计时           += 1
   Label1.Caption = vfb_LangString("刷 VisualFreeBasic 关键词权重，") & 300 - 计时 & vfb_LangString("秒后自动关闭。")
   Select Case 计时
      Case 1 ,60 ,155
         Miniblink1.URL = "https://www.baidu.com/"
      Case 5 ,63 ,158
         Dim mm As String = "$(""#kw"").val(""VisualFreeBasic 编程 勇芳软件"");"
         Miniblink1.RunJS(mm)
      Case 6 ,64 ,159
         Dim mm As String = "document.getElementById(""form"").submit();"
         Miniblink1.RunJS(mm)
         'Miniblink1.FireMouseClick AfxScaleX(616) ,AfxScaleY(35)
         'Form1.WindowsZ=HWND_BOTTOM
      Case 9 ,67 ,162
         Dim mm As String = Miniblink1.GetSource
         mm             = GetStrCenter(mm ,"class=""result c-container new-pmd""" ,"VisualFreeBasic")
         mm             = GetStrCenter(mm ,"href=""" ,"""")
         Miniblink1.URL = mm
         'Miniblink1.FireMouseClick AfxScaleX(227) ,AfxScaleY(149)
         'Form1.WindowsZ=HWND_BOTTOM
      Case 15 To 35 ,68 To 88 ,110 To 130
         Miniblink1.FireMouseWheelEvent(100 ,10 , -120)
      case 36 to 56 ,89 To 109 ,131 To 151
         Miniblink1.FireMouseWheelEvent(100 ,10 ,120)
      Case 300
         Timer1.Enabled = False
         CloseTab(2 ,hWndForm)
   End Select
   
End Sub

Sub Form1_Command1_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '单击
   If Command1.Caption = vfb_LangString("暂停") Then
      Command1.Caption = vfb_LangString("继续")
      Timer1.Enabled   = False
   Else
      Command1.Caption = vfb_LangString("暂停")
      Timer1.Enabled   = True
   End If
End Sub


Function Form1_Miniblink1_CreateView(hWndForm As hWnd, hWndControl As hWnd,WebView As wkeWebView,navigationType As Integer,url As CWSTR,windowFeatures as wkeWindowFeatures)As wkeWebView  '网页点击a标签创建新窗口时
   'NavigationType 表示浏览触发的原因。可以取的值有：WKE_NAVIGATION_TYPE_LINKCLICK：点击a标签触发。WKE_NAVIGATION_TYPE_FORMSUBMITTE：点击form触发。WKE_NAVIGATION_TYPE_BACKFORWARD：前进后退触发。WKE_NAVIGATION_TYPE_RELOAD：重新加载触发
    Miniblink1.URL =url 
    Function = WebView
End Function

 




