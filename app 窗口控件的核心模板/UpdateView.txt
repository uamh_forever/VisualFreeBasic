﻿Sub FLY_yfvb_Shadow_window_delay_display(hWndForm As HWND)
   Dim vi as OSVERSIONINFO, tt As Long
   vi.dwOsVersionInfoSize = SizeOf(OSVERSIONINFO)
   GetVersionEx @vi
   if vi.dwMajorVersion < 6 Then
      tt = 50
   ElseIf vi.dwMinorVersion > 1 Then
      tt = 150
   Else
      tt = 250
   End if
   Sleep tt
   ShowWindow hWndForm, SW_SHOWNOACTIVATE
End Sub

Sub FLY_yfvb_UpdateView(hWndForm As hWnd, zWndForm As HWND, f As Long, nAlpha As Long = 255)  '阴影窗口绘画过程
   dim rc As Rect
   GetWindowRect(zWndForm, @rc)
   Dim pptDst As POINT, psize as tagSIZE
   pptDst.x = rc.left - AfxScaleX(f)
   pptDst.y = rc.top - AfxScaleY(f)
   psize.cx = rc.right - rc.left + AfxScaleX(f * 2.4)
   psize.cy = rc.bottom - rc.top + AfxScaleY(f * 2.4)
   if IsIconic(zWndForm) OrElse IsZoomed(zWndForm) Then Return
   
   Dim nDc As HDC = GetDC(hWndForm)
   dim As HDC hMemDC =CreateCompatibleDC(nDc)
   
   Dim bmpinfo As BITMAPINFO
   bmpinfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER)
   bmpinfo.bmiHeader.biBitCount = 32
   bmpinfo.bmiHeader.biHeight = psize.cy
   bmpinfo.bmiHeader.biWidth = psize.cx
   bmpinfo.bmiHeader.biPlanes = 1
   bmpinfo.bmiHeader.biCompression = BI_RGB
   bmpinfo.bmiHeader.biXPelsPerMeter = 0
   bmpinfo.bmiHeader.biYPelsPerMeter = 0
   bmpinfo.bmiHeader.biClrUsed = 0
   bmpinfo.bmiHeader.biClrImportant = 0
   bmpinfo.bmiHeader.biSizeImage = bmpinfo.bmiHeader.biWidth * bmpinfo.bmiHeader.biHeight * bmpinfo.bmiHeader.biBitCount / 8
   Dim pp As Long Ptr
   Dim m_Bmp As HBITMAP = CreateDIBSection(hMemDC, @bmpinfo, DIB_RGB_COLORS, @pp, 0, 0)
   'MsgBox Hex(pp)
   Dim hOldBitmap As HBITMAP = SelectObject(hMemDC, m_Bmp)
   Dim m_GpDC As GpGraphics Ptr ' 主DC 的 GDI+句柄
   GdipCreateFromHDC(hMemDC, @m_GpDC)
   'GdipSetSmoothingMode m_GpDC, SmoothingModeAntiAlias
   Dim nGpBrush As GpBrush Ptr
   Dim nGpPen As GpPen Ptr
   Static fda(402) As Long = {_   'PNG图像数据
      &H474E5089, &HA1A0A0D, &HD000000, &H52444849,&H31000000,&H31000000,&H608,&H419C7300, &H1C, &H47527301,&HCEAE0042, &HE91C, &H41670400,&H414D, &HFC0B8FB1, &H561,&H48700900,&H7359,&HC30E, &HC701C30E, _
      &H64A86F, &H49E10500, &H68544144,&HE9D9D543, &H1453EEAB, &H9E63F007, &H2508845D, &H32895743,&H257B3247, &H2DCA2929, &H42F0DE19, &H324942A6, &HCA292494, &H10C94250, &HEB42FE99, &HD5ADF3A3, &HF739FB3E, &HCF4CF7B9, &HB9DBEBB3, &HBEB5EF7B, _
      &HD7BDAD6B, &HCE7DBF5A, &H71A3D8DD, &HE95836C2, &H3B787331, &H3998C4AC, &H812E2706, &HDBEC1E99, &HD8E71998, &H97938549,&H14585904,&H998D9ED0, &H29F8EEB1, &H71375385,&H9F4E15A, &H7C8647AC, &HDEE46A0F, &HF79231D5, &H1CE38F53, &H6670A33C, _
      &HD9C2ACE1, &HB0267385, &H9D1C9F6E, &HEDD9A031, &H327478EC, &HB8CF27E8, &H9C7CE593, &HBF38543E, &HC2E14170, &H27DBAC09, &HF4405E47, &H1EE14373, &HA347666F, &H99A13913, &H939C71F3, &H2D2E1717, &HBCB8565C, &HB9834570, &HC8E4FB75, &H80B73BD3, _
      &HC7660C67, &H7B95D237, &HD2030FA, &H39E3209C, &HF4718E71, &H85D5C2CA, &HB870B6B, &H5B9835D7, &H3C8E4FB7, &H780A8BBD, &H9A4DE1F0, &H3B8E46E5, &H634AE98, &HCEEFD390, &HE4E0555F, &HB1B85F5, &H2D6E1537, &H6E60D6DC, &HF2393EDD, &H3E0C12F4, _
      &H40C7F8BC, &H8F4B3FBA, &H518A0059, &HE9014B1E, &H289C3520, &HDC286E67, &H70A3B852, &HC2DEE177, &HA878583, &H34FE6F0F, &H1C9F6EB7, &HF1F47A79, &H9F8BC3E0, &H8166F61D, &H166352C, &H801E9938, &H4E1693D3, &H16773394,&HF8702BEE, &H8563C291, _
      &H344F0BC7, &H4FB75B98, &H9BBD3C8E, &H29AE780B, &H2C81EDE0, &H476D91BA, &H5AC45284, &H7A8525F, &HE29DB720, &HA7B85ED, &HC0B47A9C, &HCF0BA7D9, &HBC2B9E14, &HDD6E60D0, &HF4F2393E, &H7C374BE8, &H49207B78, &H5EE0B4AD, &H4470B1F, &H14080B72, _
      &H1E8579DB, &H1296BC80, &H2C9C17F7, &H9E148EA7, &H7858BC2F, &HE15AF0B9, &H9B1BC2F5, &HDBADCD3F, &H1E9E4727, &HF0F83C7D, &H4DD840F6, &H1B752FB1, &HA90A04D9, &HD1435D03, &HEE5759C9, &HA07C18C4, &H2C9F34E0, &HA9785B3C, &HAB7D1C0, &H2BDE14EF, _
      &H6E60DFBC, &HF2393EDD, &HC1E3E8F4, &H3B3F1787, &H7ECBB1EC, &HE7EE8FAF, &HD10D9196, &HFED234F6, &H27C50F74, &H9390A577, &H82A9F863, &H6FC27154, &H7C2DDE16, &HF0B8F854, &H6B3E169, &HC9F6EB73, &H1F47A791, &HF8BC3E0F, &H5D8F61D9, &H6D5A7BF6, _
      &H53161B7B, &HB82DE30B, &H11445D5E, &H902961CB, &HBC295E00, &HF0A0F85D, &HC2F3E149, &HAAF8517, &HD9BE175F, &HDD6E69FC, &HF4F2393E, &HBC1027E8, &H8F61D9F8, &HA46DD9DD, &H1E8CB636, &H71416E44, &HDCFD0D39, &H5055DF35, &H2A59728C, &H7C8E4938, _
      &H9CD27058, &H8577F4E4, &H363F0BEF, &H4FB75B98, &HFA3D3C8E, &HC5E1F078, &HEC7B0ECF, &H7C8BDFB2, &H20161061,&HA009E4A9, &HA0B78DC5, &H4576BA2D, &H84A5A729, &H97D0E513, &H3F0B6F85, &HFC2A7E14, &HC1A5F85C, &H727DBADC, &HC7D1E9E4, &H7E2F0F83, _
      &HE0DBC676, &HA616BF0F, &HA9220F54, &H43A9EBE4, &H5D1416A4, &H5177416E, &H6A5B729C, &HCE270024, &HBF0B5FD9, &HF68FE26F, &HC9F6EB6F, &H78FA1025, &H6DCDE1F0, &HE0DA97B0, &H52927F07, &HF5E8830B, &H9525ADA0, &H3EB4D47C, &HAB395A5F, &HBA936BCF, _
      &HE395228C, &H7309C452, &HC702B7F0, &HD5FC2CFF, &H3EDD6E60, &HE8F4F239, &H1787C1E3, &HB1EC3B3F, &H8A527ECB, &H48BAD35F, &H4BC20944, &H9B495D32, &H79BDB7D3, &H717C193C, &HB0E4B07D, &H38DBA976, &H552EB94D, &HF802749C, &H834FF0BB, &H47210279, _
      &H3C7D1E9E, &H67E2F0F8, &HD9763D87, &H14BA47E7, &HFF107AFF, &HA1063207, &H9E0F5278, &HE3D2F2CF, &HD3C7DBCD, &HD5F797C1, &HA72FBE82, &HEB451D75, &HF4CA574, &H1E86DFE0, &HF4F23908, &H87C1E3E8, &HEC3B3F17, &H3F3ECBB1, &H71631752,&H4508351F, _
      &H59ED19E5, &HED07A90, &HC07D817D, &H53ED7FF4, &HCE5E2B8E, &H80319D49, &H727D881E, &HC7D1E9E4, &HFE2F0F83, &HECBB05D4, &HB5DEE2F7, &H12253C20, &H104EBC84, &H20996552,&HFA0825C8, &HBEEF0209, &H3756B105, &HB135B6B1, &HE8C9DD96, &H77E5AC41, _
      &H6B4448C2, &H1236C5FD, &H63E19E44, &HDBD41C5D, &HFE87AC69, &H2C32DA76, &H4A48BAF6, &HC838525D, &H49FB2C57, &HB61EB1A5, &HBA908B0C, &HA55C62F0, &H93F827DF, &H5BAF887A, &H52923646,&HAC294BBD, &HBDDE86D4, &HE1D9BF97, &HDBB02D4F, &H67C2D2BB, _
      &H20364678,&H7290A1B7, &HCADCDA1B, &H646D8EFF, &H1414B453, &H9B5C5C92, &H111876BF, &H53A78148, &HD5C54EB9, &H2B627103, &HCA777F9, &HD70DB942, &HEB8B4888, &H5FD41C81, &H891E9700, &H2F2207B6, &H5672207B, &H4E22E9F1, &HC2523051, &HC193D697, _
      &HFEC0E7DB, &H61446B16, &HE5A9240C, &H74E8A9EB, &H9FA7A70F, &HCA89C673, &H8F1E6F6B, &H98126937,&H8E4FB75B, &H78FA3D3C, &HA4DE1F0, &H638002CD, &H8792511A, &H4D8A2063, &H7AFED0D7, &H9A544E30, &H7A4E6DC9, &HD6E60746, &HF38B93ED, &H13FFF2C7, _
      &H9CA235C7, &H9E881260, &H5B93E3ED, &H91820291,&H1A931C0E, &HD1C0B9E, &H27DBADCC, &HE71E9E47, &H9BC3E0F1, &HB0F3BA00, &H7289D1A3, &HE460B323, &HDCA0271, &H9F6EB08D, &H3F27791C, &H4A3B0F3A, &H64E8D9A1, &HEB4BD3D0, &H700A5C63, &H2C723146, _
      &H6EB08D41, &H6D25C9F, &HF4C742D2, &H9D1F5D81, &H24CDFA1C, &H9CB27020, &HEF13911C, &H91F9C78E, &H68C74F7B, &H2CEDFA0C, &H53B3B203, &HCC6DF60F, &H8EC7038C, &H1C1E9986, &HB53998F8, &HCC7E561D, &H79760E9C, &H7FC6C6C, &HFC898C16, &HB950B9EA, _
      &H0,&H444E4549, &H826042AE}
   Static yXimage As GpImage Ptr
   if yXimage = 0 Then '把PNG图像内存数据，转换为GDI+图像句柄。
      DIM pImageStream AS IStream PTR, hGlobal AS HGLOBAL, pGlobalBuffer AS LPVOID
      hGlobal = GlobalAlloc(GMEM_MOVEABLE, UBound(fda) * 4 + 4)
      IF hGlobal THEN
         pGlobalBuffer = GlobalLock(hGlobal)
         IF pGlobalBuffer THEN
            CopyMemory(pGlobalBuffer, @fda(0), UBound(fda) * 4 + 4)
            IF CreateStreamOnHGlobal(hGlobal, FALSE, @pImageStream) = S_OK THEN
               IF pImageStream THEN
                  GdipCreateBitmapFromStream(pImageStream, CAST(GpBitmap PTR PTR, @yXimage))
               End if
            End if
            GlobalUnlock pGlobalBuffer
         End if
         GlobalFree hGlobal
      End if
   End if
   
   Dim dpi As Single = AfxScaleX(1)
   Dim as Single gYn=24 *dpi,gW=psize.cx -48 *dpi,gH = psize.cy -24 *dpi ,pH =psize.cy -48 *dpi
   '画矩形上面的边
   GdipDrawImageRectRect m_GpDC, yXimage, gYn, 0, gW, gYn, 24, 0, 1, 24, 2, Null, Null, Null
   '画矩形下面的边
   GdipDrawImageRectRect m_GpDC, yXimage, gYn, gH, gW, gYn, 24, 25, 1, 24, 2, Null, Null, Null
   '画矩形左面的边
   GdipDrawImageRectRect m_GpDC, yXimage, 0, gYn, gYn,pH , 0, 25, 24, 1, 2, Null, Null, Null
   '画矩形右面的边
   GdipDrawImageRectRect m_GpDC, yXimage, psize.cx -gYn, gYn, gYn,pH, 25, 25, 24, 1, 2, Null, Null, Null
   '画矩形左上角的圆角
   GdipDrawImageRectRect m_GpDC, yXimage, 0, 0, gYn, gYn, 0, 0, 24, 24, 2, Null, Null, Null
   '画矩形右下角的圆角
   GdipDrawImageRectRect m_GpDC, yXimage, psize.cx -gYn, gH, gYn, gYn, 25, 25, 24, 24, 2, Null, Null, Null
   '画矩形右上角的圆角
   GdipDrawImageRectRect m_GpDC, yXimage, psize.cx -gYn, 0, gYn, gYn, 25, 0, 24, 24, 2, Null, Null, Null
   '画矩形左下角的圆角
   GdipDrawImageRectRect m_GpDC, yXimage, 0, gH, gYn, gYn, 0, 25, 24, 24, 2, Null, Null, Null
   '填充中间
   GdipDrawImageRectRect m_GpDC, yXimage, gYn, gYn, gW, pH, 25, 25, 1, 1, 2, Null, Null, Null
   
   GdipDeleteGraphics(m_GpDC)
   '扣除主窗口范围
   
   MapWindowPoints HWND_DESKTOP, hWndForm, Cast(LPPOINT, VarPtr(rc)), 2
   Dim hBrush As HBRUSH = CreateSolidBrush(0)
   If hBRush Then
      Dim hOldBrush as HGDIOBJ, hOldPen as HGDIOBJ
      dim hPen as HPEN = CreatePen(PS_NULL, 0, 0)
      hOldBrush = SelectObject(hMemDC, hBrush)
      hOldPen = SelectObject(hMemDC, hPen)
      RoundRect hMemDC, rc.left + 1, rc.top + 1, rc.right + 2, rc.bottom + 2, AfxScaleX(16) ,AfxScaleY(16)
      SelectObject(hMemDC, hOldBrush)
      SelectObject(hMemDC, hOldPen)
      DeleteObject hBrush
      DeleteObject hPen
   End If
   
   Dim ptSrc As POINT
   Dim blf As BLENDFUNCTION
   blf.BlendOp = AC_SRC_OVER
   blf.BlendFlags = 0
   blf.SourceConstantAlpha = nAlpha
   blf.AlphaFormat = AC_SRC_ALPHA
   UpdateLayeredWindow(hWndForm, NULL, @pptDst, Cast(Any Ptr, @psize), hMemDC, @ptSrc, 0, @Blf, ULW_ALPHA)
   SelectObject(hMemDC, hOldBitmap)
   DeleteObject(m_Bmp)
   DeleteDC(hMemDC)
   ReleaseDC(hWndForm, nDc)
   if IsWindowVisible(zWndForm) Then 
      if IsWindowVisible(hWndForm) = 0 Then
         SetWindowPos(hWndForm, zWndForm, 0,0 , 0, 0,SWP_NOMOVE Or SWP_NOSIZE Or SWP_NOACTIVATE Or SWP_SHOWWINDOW) 
'         ShowWindow(hWndForm, SW_SHOWNOACTIVATE)
      end if 
   End if 
End Sub
Function FLY_yfvb_FORMPROCEDURE(ByVal hWndForm As HWND, ByVal wMsg As uInteger, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As LResult '阴影窗口回调过程
   Select Case wMsg
      Case WM_DESTROY
         Dim pWindow AS CWindow Ptr = AfxCWindowPtr(hWndForm)
         If pWindow Then Delete pWindow
   End Select
   Function = DefWindowProcW(hWndForm, wMsg, wParam, lParam)
End Function

Sub FLY_yfvb_Shadow_window_delay(hWndForm As HWND)
   Dim vi as OSVERSIONINFO, tt As Long
   vi.dwOsVersionInfoSize = SizeOf(OSVERSIONINFO)
   GetVersionEx @vi
   if vi.dwMajorVersion < 6 Then
      tt = 50
   ElseIf vi.dwMinorVersion > 1 Then
      tt = 150
   Else
      tt = 250
   End if
   Sleep tt
   Dim sWnd As HWND = GetPropW(hWndForm, "FCP_SHADOW_HWND")
   if sWnd Then
      ShowWindow(sWnd, SW_SHOWNOACTIVATE)
      Dim rc as Rect
      GetClientRect(hWndForm, @rc)
      PostMessage(hWndForm, WM_MOVE, 0, 0)
      PostMessage(hWndForm, WM_Size, 0, MAKELPARAM((rc.Right - rc.Left),(rc.Bottom - rc.Top) ))
      
   End if
   'PostMessage( hWndForm , WM_NcActivate , 0 , 0 ) '更新阴影窗口
   'PostMessage( hWndForm , WM_NcActivate , 1 , 0 ) '更新阴影窗口 需要更新2次，才能确保正常显示
End Sub


Sub FLY_yfvb_CREATEWINDOWSHADOW(ByVal hWndForm As HWND) '创建阴影窗口
   Dim sWindow AS CWindow Ptr = New CWindow("VisualFreeBasic_Shadow_window")
   sWindow->Create(0 ,"" ,Cast(WNDPROC ,@FLY_yfvb_FORMPROCEDURE) ,0 ,0 ,100 ,100 ,WS_POPUPWINDOW ,WS_EX_TRANSPARENT Or WS_EX_LAYERED Or WS_EX_NOACTIVATE)
   If sWindow->hWindow then
      SetPropW(hWndForm ,"FCP_SHADOW_HWND" ,Cast(Any Ptr ,sWindow->hWindow))
      Threaddetach ThreadCreate(Cast(Any Ptr ,@FLY_yfvb_Shadow_window_delay) ,hWndForm) '经典调用方法
   end if
End Sub


