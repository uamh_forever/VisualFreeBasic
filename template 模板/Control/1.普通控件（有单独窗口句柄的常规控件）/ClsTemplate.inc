'控件类
'注意： hWnd 成员存在 Class_Control 类里，因此使用 API的类型 hWnd 需要前面加个点 .hWnd

Type Class_Template Extends Class_Control
   Private : '私有区
   kMouse As Long '鼠标在那个按钮上，0表示不在按钮上，1开始在按钮上。
   kPress As Long '鼠标是不是按下
   Declare Function GetAn(xPos As Long ,yPos As Long) As Long '根据鼠标位置，返回按钮，0表示不在按钮上，1开始在按钮上。
   Declare Sub TabPainting(vhWnd As .hWnd) ' 绘画（控件自用）
   Public : '公共区
    Declare Property Icon() As hIcon         '返回/设置 关联的图标句柄，必须具有SS_ICON样式控件才有效。
    Declare Property Icon(nIcon As HICON)
    Declare Property Image() As HBITMAP         '返回/设置 关联的位图BMP句柄，必须具有SS_BITMAP样式控件才有效。
    Declare Property Image(nBitmap As HBITMAP)
    Declare Sub DeleteIcon()   '删除与静态控件关联的图标
    Declare Sub DeleteImage() '删除与静态控件关联的位图
   '{代码不提示}
   Declare Function MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult '消息处理（控件自用）
   Declare Constructor '创建时
   Declare Sub UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   '{代码可提示}    
End Type
Constructor Class_Template
   '注意：由于窗口类是永久的，全局变量，因此这里从开软件到关软件只执行1次。
End Constructor
Sub Class_Template.UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp Then
      '1，每次窗口销毁  就执行1次
   End If
End Sub   

Function Class_Template.MsgProcedure(vhWnd As .hWnd ,wMsg As UInteger ,nwParam As wParam ,nlParam As lParam) As LResult
   Dim fp As FormControlsPro_TYPE Ptr
   Select Case wMsg
      Case WM_PAINT
         This.TabPainting(vhWnd)
         Return True
      Case WM_USER + 200 '预防多线程操作控件，需要发消息处理
         
         'Return jj
      Case WM_MOUSEMOVE
         '移出控件侦察
         Dim entTrack As tagTRACKMOUSEEVENT
         entTrack.cbSize      = SizeOf(tagTRACKMOUSEEVENT)
         entTrack.dwFlags     = TME_LEAVE
         entTrack.hwndTrack   = vhWnd
         entTrack.dwHoverTime = HOVER_DEFAULT
         TrackMouseEvent @entTrack
         '处理移动
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(xPos ,yPos)
         Dim bb As Long = MouseFlags = 1
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            FF_Redraw vhWnd
         End If
         
      Case WM_MOUSELEAVE '处理移出
         kMouse = 0
         kPress = 0
         FF_Redraw vhWnd
      Case WM_LBUTTONDOWN
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(xPos ,yPos)
         Dim bb As Long = True         
         ReleaseCapture '取消鼠标绑定控件
         SetCapture vhWnd '绑定鼠标控件，让鼠标按下拖动到控件外面也能有鼠标移动消息
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb

            FF_Redraw vhWnd
         End If
      Case WM_LBUTTONUP
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         ReleaseCapture '取消鼠标绑定控件
         Dim As Long xPos = GET_X_LPARAM(nlParam) ,yPos = GET_Y_LPARAM(nlParam) ,MouseFlags = nwParam
         Dim aa As Long = GetAn(xPos ,yPos)
         Dim bb As Long
         If aa = kMouse Then
            Select Case aa
               Case 1  '触发按钮事件
               Case 2
            End Select
         End If
         If aa <> kMouse Or kPress <> bb Then
            kMouse = aa
            kPress = bb
            
            FF_Redraw vhWnd
         End If
         
      Case WM_NOTIFY
         '为了工具提示时，鼠标指针挡住了提示文字，因此需要我们自己调整显示位置。
         fp = vfb_Get_Control_Ptr(vhWnd)
         If fp = 0 Then Return 0
         If fp->ToolTipBalloon = 0 Then
            Dim nn As NMHDR Ptr = Cast(Any Ptr ,nlParam)
            Select Case nn->code
               Case TTN_SHOW '工具提示时，设置显示位置
                  Dim A As Point
                  GetCursorPos @a
                  SetWindowPos(nn->hwndFrom ,NULL ,A.x ,A.y + GetSystemMetrics(SM_CYCURSOR) ,0 ,0 ,SWP_NOSIZE Or SWP_NOZORDER Or SWP_NOACTIVATE)
                  Return True
            End Select
         End If
   End Select
   Function = 0
End Function
Sub Class_Template.TabPainting(vhWnd As .hWnd) ' 绘画

   Dim fp As FormControlsPro_TYPE Ptr = vfb_Get_Control_Ptr(hWndControl)
   If fp = 0 Then Return
   Dim BColor As Long = GetCodeColorGDI(fp->BackColor) '背景色
   Dim fColor As Long = GetCodeColorGDI(fp->ForeColor) '前景色
   Dim gg  As yGDI   = yGDI(vhWnd ,BColor ,True) 'WM_PAINT事件里必须用这行初始化 yGDI
   Dim ww  As Long   = gg.m_Width ,hh As Long = gg.m_Height
   Dim dpi As Single = gg.Dpi     
   
End Sub    
   
Property Class_Template.Icon() As HICON         '返回/设置 关联的图标句柄，必须具有SS_ICON样式控件才有效。
  Return Cast(hIcon, SendMessage(hWndControl, STM_GETICON, 0, 0)) 
End Property
Property Class_Template.Icon(nIcon As HICON)
  PostMessage(hWndControl, STM_SETICON, CPtr(wParam, nIcon), 0)
End Property
Property Class_Template.Image() As hBitmap         '返回/设置 关联的位图BMP句柄，必须具有SS_BITMAP样式控件才有效。
  Return Cast(hBitmap, SendMessage(hWndControl, STM_GETIMAGE, 0, 0))
End Property
Property Class_Template.Image(nBitmap As HBITMAP)
  PostMessage(hWndControl, STM_SETIMAGE, CPtr(wParam, nBitmap), 0)
End Property
Sub Class_Template.DeleteIcon()
  Static_DeleteIcon hWndControl
End Sub
Sub Class_Template.DeleteImage()
  Static_DeleteImage hWndControl,IMAGE_BITMAP
End Sub
Function Class_Template.GetAn(xPos As Long ,yPos As Long) As Long
   '获取鼠标在什么按钮
   
   
End Function 



