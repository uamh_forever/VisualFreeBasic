Type Class_Template
Private : 
   m_hWndForm As HWnd '控件句柄
   m_IDC As Long     '控件IDC
   Declare Function GetFP() As FormControlsPro_TYPE ptr '返回控件保存数据的指针（一般是类自己用）
Public : 
   Declare Property hWndForm() As.hWnd         '返回/设置控件所在的窗口句柄，主要用于多开同一个窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
   Declare Property hWndForm(ByVal hWndParent As.hWnd) '获取控件所在的窗口句柄（父窗口）
   Declare Property Enabled() As Boolean                 '返回/设置控件是否允许操作。{=.True.False}
   Declare Property Enabled(ByVal bValue As Boolean)
   Declare Property Interval() As Long                  '返回/设置两次调用 Timer 控件的 Timer 事件间隔的毫秒数。
   Declare Property Interval(ByVal wTimer As Long)
   Declare Property IDC() As Long             '返回/设置控件IDC，控件标识符，1个窗口里每个控件IDC都是唯一的，包括控件数组。（不可随意修改，系统自动处理）
   Declare Property IDC(NewIDC As Long)
   Declare Property Tag() As CWSTR                       '存储程序所需的附加数据。
   Declare Property Tag(ByVal sText As CWSTR)
   Declare Property UserData(idx AS LONG) As Integer      '返回/设置用户数据，idx索引号，范围为0至99。就是1个控件可以存放100个数值。
   Declare Property UserData(idx As Long ,bValue As Integer)   
   '{代码不提示}
   Declare Constructor
   Declare Sub UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
End Type
Constructor Class_Template
   '注意：由于窗口类是永久的，全局变量，因此这里从开软件到关软件只执行1次。
End Constructor
Sub Class_Template.UnLoadControl() ' 当窗口被销毁时，最后需要清理的工作，由控件内部调用。
   Dim fp As FormControlsPro_TYPE Ptr = GetFP()
   If fp Then
      
   End If    
End Sub

Function Class_Template.GetFP() As FormControlsPro_TYPE Ptr '返回自己控件的指针
   Dim fp As FormControlsPro_TYPE ptr = GetPropW(m_hWndForm, "FCP_TYPE_PTR")
   if fp=0 Then fp = vfb_Get_Control_Ptr(m_hWndForm)
   While fp
      if fp->IDC = m_IDC Then Return fp      
      fp = fp->VrControls
   Wend

End Function
Property Class_Template.hWndForm() As .hWnd         '返回/设置控件所在的窗口句柄
      Return m_hWndForm
End Property
Property Class_Template.hWndForm(ByVal hWndParent As .hWnd)
   m_hWndForm = hWndParent
End Property
Property Class_Template.Enabled() As Boolean
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then  ' 虚拟控件通用样式 样式结构：&H01020304  04 选项 &H1=允许 &H2=显示
      Return (fp->Style and &H1 )<>0
   End If
End Property
Property Class_Template.Enabled(ByVal bValue As Boolean)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      If bValue Then fp->Style Or= &H1 Else fp->Style And= Not &H1
      dim m_Value as long = HiWord(fp->Style)
      KillTimer(m_hWndForm, m_IDC)
      If (fp->Style and &H1) <> 0 and m_Value > 0 Then
         SetTimer(m_hWndForm, m_IDC, m_Value, Null)
      End If
   End If
End Property

Property Class_Template.Interval() As Long                  '返回/设置两次调用 Timer 控件的 Timer 事件间隔的毫秒数。
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return HiWord(fp->Style)
   End If   
End Property
Property Class_Template.Interval(ByVal wTimer As Long)
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      dim bValue as long = (fp->Style and &H1)
      dim m_Value as long = HiWord(fp->Style)
      If bValue <> 0 and m_Value > 0 Then KillTimer(m_hWndForm, m_IDC)
      fp->Style = LoWord(fp->Style) or (Cast(UInteger, wTimer) shl 16)
      If bValue <> 0 and wTimer > 0 Then
         SetTimer(m_hWndForm, m_IDC, wTimer, Null)
      End If
   End If
End Property
Property Class_Template.IDC() As Long                  '返回/设置两次调用 Timer 控件的 Timer 事件间隔的毫秒数。
   Return m_IDC
End Property
Property Class_Template.IDC(ByVal NewIDC As Long)
   m_IDC  =NewIDC
End Property
Property Class_Template.Tag() As CWSTR 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      Return fp->Tag
   End If
End Property
Property Class_Template.Tag(ByVal sText As CWSTR )
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   if fp then
      fp->Tag = sText
   End If
End Property
Property Class_Template.UserData(idx AS LONG) As Integer      '返回/设置用户数据，就是1个控件可以存放100个数值。
   If idx < 0 Or idx > 99 Then Return 0
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      Return fp->UserData(idx)
   End If   

End Property
Property Class_Template.UserData(idx AS LONG, bValue As Integer)
   If idx < 0 Or idx > 99 Then Return 
   Dim fp As FormControlsPro_TYPE ptr = GetFP()
   If fp  Then
      fp->UserData(idx) = bValue
   End If    
End Property



