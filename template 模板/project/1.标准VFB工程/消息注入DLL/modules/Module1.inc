﻿


Sub SetHook(nHwnd As HWND) Export  '这个给外挂调用，注入DLL用（这里是运行在外挂进程中）
   '外挂加载本DLL，然后调用本函数，nHwnd 为游戏窗口句柄，那这个DLL就注入到游戏里了。
   '必须在DLL中执行以下函数才可以，当外挂关闭，本DLL会自动卸载，不需要写卸载代码。
   '外挂里  Declare Sub SetHook Lib "DLL文件名不包含扩展名" Alias "SETHOOK"(nHwnd As HWND) 
   '        SetHook FindWindow("游戏窗口类名", "游戏窗口名称")   
   Dim As ULong Tid, RetPid 
   Tid = GetWindowThreadProcessId(nHwnd, @RetPid)  '获取游戏窗口线程，注入到这线程
   
   SetWindowsHookEx(WH_CALLWNDPROCRET, @CallWndRetProc, App.hInstance, Tid)  '游戏处理完消息后
   '是 SendMessage 发来的消息，主要是 WM_MOVE WM_SIZE WM_Close 等窗口常规消息，
   
   SetWindowsHookEx(WH_GETMESSAGE, @GetMsgProc, App.hInstance, Tid)   '游戏的消息队列的
   '是 PostMessage 发来的消息，主要是 WM_KEYDOWN WM_RBUTTONDOWN WM_CHAR 等键盘鼠标消息。

End Sub
Function CallWndRetProc(code As Integer, ParamW As wParam, ParamL As LPARAM) As LRESULT
   '是 PostMessage 发来的消息，主要是 WM_MOVE WM_SIZE WM_Close 等窗口常规消息，
   '这里在游戏进程中运行
   if code = 0 Then
      Dim wMsg As CWPRETSTRUCT Ptr = Cast(Any Ptr, ParamL)
      Select Case wMsg->message
         Case 0

      End Select
   end if
   Function = CallNextHookEx(0, code, ParamW, ParamL)
End Function
Function GetMsgProc(code As Integer, ParamW As wParam, ParamL As LPARAM) As LRESULT
   '是 SendMessage 发来的消息，主要是 WM_KEYDOWN WM_RBUTTONDOWN WM_CHAR 等键盘鼠标消息。
   '这里在游戏进程中运行
   if code = 0 Then
      Dim wMsg As MSG Ptr = Cast(Any Ptr, ParamL)
      Select Case wMsg->message
         Case 0

            
      End Select
      
   end if
   Function = CallNextHookEx(0, code, ParamW, ParamL)
End Function