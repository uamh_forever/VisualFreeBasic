﻿#VisualFreeBasic_Form#  Version=5.5.4
Locked=0

[Form]
Name=Form虚拟指针
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_POPUP
Style=0 - 无边框
Icon=
Caption=
StartPosition=0 - 手动
WindowState=3 - 隐藏
Enabled=True
Repeat=True
Left=0
Top=0
Width=75
Height=70
TopMost=False
Child=False
MdiChild=False
TitleBar=False
SizeBox=False
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=True
MousePass=True
TransPer=0
TransColor=&HFFFFFFFF
Shadow=0 - 无阴影
BackColor=&HFFFFFFFF
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Timer]
Name=Timer1
Index=-1
Interval=50
Enabled=True
Left=29
Top=86
Tag=


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

Sub Form虚拟指针_Timer1_WM_Timer(hWndForm As hWnd ,wTimerID As Long) '定时器
   Me.hWndForm    = hWndForm
   Me.UserData(1) = Me.UserData(1) + 1
   If Me.UserData(1) > 40 Then Me.UserData(1) = 0
   Me.Refresh
End Sub

Sub Form虚拟指针_FormPaintEnd(hWndForm As hWnd ,gg As yGDI ,nBackColor As Long) '重绘最后，已经画好虚拟控件，用 gg 来画。
   Dim Po(6) As Point
   Po(0).x     = 0  : Po(0).y = 0
   Po(1).x     = 0  : Po(1).y = 27
   Po(2).x     = 6  : Po(2).y = 21
   Po(3).x     = 11 : Po(3).y = 29
   Po(4).x     = 14 : Po(4).y = 27
   Po(5).x     = 10 : Po(5).y = 19
   Po(6).x     = 19 : Po(6).y = 19
   Me.hWndForm = hWndForm
   Dim ss As Long = Me.UserData(1)
   gg.gpPen 1                     ,&HFF000000
   GdipSetPenDashOffset gg.nGpPen ,ss
   GdipSetPenDashStyle gg.nGpPen  ,3
   gg.GpBrushGradient ss          ,ss ,20 + ss ,20 + ss ,&HFFFEFEFE ,&HFFC0C0C0 ,WrApMoDETilEFlipXY
   gg.GpDrawPolygon Po()

   
End Sub




