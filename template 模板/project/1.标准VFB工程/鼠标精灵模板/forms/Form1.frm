﻿#VisualFreeBasic_Form#  Version=5.5.5
Locked=0

[Form]
Name=主窗口
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_CAPTION,WS_CLIPCHILDREN,WS_CLIPSIBLINGS,WS_SYSMENU,WS_VISIBLE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_EX_WINDOWEDGE,WS_POPUP
Style=3 - 常规窗口
Icon=Default.ico|ICON_DEFAULT
Caption=鼠标精灵
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=278
Height=106
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=False
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=0
Style=0 - 无边框
Caption=次数：无限
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=11
Top=35
Width=115
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[HScroll]
Name=HScroll1
Index=0
Max=1000
Min=0
Value=0
Page=0
SmallChange=1
LargeChange=10
Enabled=True
Visible=True
Left=9
Top=54
Width=113
Height=16
ALING=False
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[HScroll]
Name=HScroll1
Index=1
Max=200
Min=-9
Value=1
Page=0
SmallChange=1
LargeChange=10
Enabled=True
Visible=True
Left=152
Top=54
Width=116
Height=16
ALING=False
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=1
Style=0 - 无边框
Caption=速度：正常
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=164
Top=35
Width=99
Height=17
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=开启
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=9
Top=3
Width=42
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=停止
TextAlign=1 - 居中
Ico=
Enabled=False
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=57
Top=3
Width=42
Height=23
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Label]
Name=Label2
Index=-1
Style=0 - 无边框
Caption=状态提示：停止中
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=106
Top=7
Width=160
Height=19
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Line]
Name=Line1
Index=-1
Style=0 - 顶部
BorderWidth=1
ArrowStartW=0 - 无箭头
ArrowStartH=0 - 无箭头
ArrowEndW=0 - 无箭头
ArrowEndH=0 - 无箭头
BorderColor=&HFF000000
Enabled=True
Visible=True
Left=5
Top=30
Width=266
Height=1
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Timer]
Name=Timer1
Index=-1
Interval=100
Enabled=True
Left=11
Top=145
Tag=

[WinHook]
Name=WinHook1
Left=76
Top=145
Tag=


[AllCode]
'这里是主窗口，负责控制脚本执行。

Sub 主窗口_Command1_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '开始按钮
   Command1.Enabled = False
   Command2.Enabled = True
   配置.回放次数 = 0
   配置.停止执行 = 0
   配置.等待时间 = 0
   配置.目标窗口 = 0
   配置.回放位置 = 0
   配置.虚拟指针 = Form虚拟指针.Show()   
   Threaddetach ThreadCreate(Cast(Any Ptr,@脚本解释执行线程),0) '经典调用方法
End Sub
Sub 脚本解释执行线程(脚本 As String Ptr)
   Dim i As Long ,ii As Long ,bb As String
   '必要的设置
   Dim po As Point
   GetCursorPos @po
   SetWindowPos(配置.虚拟指针 ,HWND_TOPMOST ,po.x + 1 ,po.y + 1 ,0 ,0 ,SWP_NOSIZE Or SWP_NOACTIVATE)
   If 配置.虚拟鼠标 Then
      If 配置.前台模式 = 0 Or 配置.移动鼠标 = 0 Then FF_Control_ShowState 配置.虚拟指针 ,SW_SHOWNOACTIVATE
   End If
   '命令执行 ---------------------------------------------
   Do
      配置.回放次数 += 1
      If 配置.执行次数 > 0 Then
         If 配置.回放次数 > 配置.执行次数 Then Exit Do
      End If
      脚本执行()
      If 配置.停止执行 Then Exit Do
      Sleep 100
   Loop
   CloseForm 配置.虚拟指针
   Command1.Enabled = True
   Command2.Enabled = False
   配置.停止执行    = 1
   Label2.Caption   = "执行完毕，共执行 " & 配置.回放次数 -1 & " 次"
End Sub
Sub 主窗口_Command2_BN_Clicked(hWndForm As hWnd ,hWndControl As hWnd) '结束按钮
   Command1.Enabled = True
   Command2.Enabled = False
   配置.停止执行    = 1
   'Label2.Caption   = "状态提示：停止中"
End Sub

Sub 主窗口_Shown(hWndForm As hWnd ,UserData As Integer) '窗口完全显示后。UserData 来自显示窗口最后1个参数。
   配置.停止执行 = 1
   脚本配置()
   HScroll1(0).Value = 配置.执行次数
   HScrollSet(0)
   If 配置.执行速度 = 0 Or 配置.执行速度 = 1 Then
      HScroll1(1).Value = 0
   ElseIf 配置.执行速度 >= 4 Then
      HScroll1(1).Value = 配置.执行速度 + 26
   ElseIf 配置.执行速度 < 1 Then
      HScroll1(1).Value = 配置.执行速度 * 10 - 10
   Else
      HScroll1(1).Value = 配置.执行速度 * 10 + 10
   End If
   HScrollSet(1)
   For i As Long = 0 To 7
      F4_Form(i).jj       = 99
      F4_Form(i).hWndForm = Form点击效果.Show
   Next
End Sub

Sub 主窗口_HScroll1_Change(ControlIndex As Long, hWndForm As hWnd, hWndControl As hWnd, hNewPos As Long, nScrollCode As Long )  '左右滚动条的值发生了改变
   'nScrollCode = SB_LINELEFT  SB_LINERIGHT  SB_PAGELEFT  SB_PAGERIGHT  SB_THUMBPOSITION SB_THUMBTRACK  SB_ENDSCROLL   SB_LEFT     SB_RIGHT  
   ''             点击向左按钮 点击向右按钮  点击滑块左边 点击滑块右边  托动滑块结束     托动滑块进行中 结束滚动条操作 滚动到左边  滚动到右边
   'hNewPos       滑块位置（注意：因为操作系统传来是16位数，最大为65535）
   HScrollSet(ControlIndex) '设置要显示的 回放次数： 和  回放速度：
End Sub
Sub HScrollSet(ControlIndex As Long) '设置要显示的 回放次数： 和  回放速度：
   Dim v As Single
   v = HScroll1(ControlIndex).Value
   If ControlIndex = 0 Then
      Label1(0).Caption = "次数：" & IIf(v = 0 ,"无限" ,v & "次")
      配置.执行次数 = v
   Else
      If v = 0 Then
         Label1(1).Caption = "速度：" & "正常"
         v                 = 1
      ElseIf v > 29 Then
         v                 -= 26
         Label1(1).Caption = "速度：" & v & "倍"
      ElseIf v < 0 Then
         v                 = (10 + v) / 10
         Label1(1).Caption = "速度：" & v & "倍"
      Else
         v                 = (v + 10) / 10
         Label1(1).Caption = "速度：" & v & "倍"
      End If
      配置.执行速度 = v
   End If
End Sub

Sub 主窗口_Timer1_WM_Timer(hWndForm As hWnd ,wTimerID As Long) '定时器
   Static zttiu As Long
   zttiu += 1
   If zttiu > 7         Then zttiu = 0
   If 配置.停止执行 = 0 Then '播放
      Label2.Caption = "第" & 配置.回放次数 & "次，按 Esc 键停止." & Mid("↖↑↗→↘↓↙←" ,zttiu * 2 + 1 ,2)
   End If
   Dim i As Long
   For i = 0 To 7
      If F4_Form(i).jj < 20 Then
         Dim gg As yGDI = F4_Form(i).hWndForm
         Dim jj As Long = F4_Form(i).jj
         gg.Cls &HFFFFFF
         Select Case F4_Form(i).an
            Case WM_LBUTTONDOWN
               gg.GpPen 1 ,&HFF000000
            Case WM_RBUTTONDOWN
               gg.GpPen 1 ,&HFF0000FF
            Case 519
               gg.GpPen 1 ,&HFFFF00FF
         End Select
         gg.GpDrawEllipse 20 - jj ,20 - jj ,jj * 2 ,jj * 2
         F4_Form(i).jj += 2 'F4_Form(i).jt
      ElseIf F4_Form(i).jj < 50 Then
         Dim gg As yGDI = F4_Form(i).hWndForm
         gg.Cls &HFFFFFF
         F4_Form(i).jj = 99
      End If
   Next
End Sub

Function 主窗口_WinHook1_LowLevelKeyboardProc(ByVal nCode As Integer ,ByVal KeyMsg As UInteger ,ByRef lKey As KBDLLHOOKSTRUCT) As LResult '底层键盘钩子(全系统有效)
   '每当新的键盘输入事件即将发布到线程输入队列中时，系统都会调用此函数。
   'nCode   挂钩过程用来确定如何处理消息的代码。如果nCode小于零，必须向下传递。nCode=HC_ACTION  按键消息
   'KeyMsg  键盘消息的标识符。此参数是以下消息之一：WM_KEYDOWN WM_KEYUP WM_SYSKEYDOWN WM_SYSKEYUP
   'lKey    lKey.vkCode   虚拟键代码。详细看微软的 https://docs.microsoft.com/zh-cn/windows/win32/inputdev/virtual-key-codes
   ''       lKey.scanCode 硬件扫描代码。
   ''       lKey.flags    扩展标志 = (lKey.flags And 1)  : 指示该键是否为扩展键，例如出现在增强型101键或102键键盘上的右侧ALT和CTRL键。如果是扩展键，则值为1；否则为0
   ''                     完整级别 = (lKey.flags And 2)  : 是否从较低完整性级别运行的进程注入事件。如果是这种情况，则该值为2；否则为0
   ''                     注入事件 = (lKey.flags And 16) : 是否注入事件。如果是这种情况，则该值为16；否则为0。
   ''                     上下文   = (lKey.flags And 32) : 如果ALT键按下，则值为32；否则，值为0。
   ''                     过渡状态 = (lKey.flags And 64) : 如果按下键，则值为0；如果释放键，则值为64。
   if nCode = HC_ACTION Then '=HC_ACTION 才是用户处理
      '这里是用户代码， 如果想屏蔽（俗称吃掉）这消息，则最后写：Return TRUE
      If lKey.vkCode = VK_ESCAPE Then
         If Command1.Enabled = False Then
            Command2.Click
         End If
      End If
   End If
   Function = CallNextHookEx(0 ,nCode ,KeyMsg ,Cast(lParam ,@lKey)) '传递到下一个挂钩过程。
End Function











